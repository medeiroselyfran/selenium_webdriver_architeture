package com.lge.gpri.tests.usermanagement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsUserManagement;
import com.lge.gpri.screens.usermanagement.ScreenUsersManagement;
import com.lge.gpri.tests.AbstractTestScenary;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestBlockedUsers extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-3087
	 * @summary Block User
	 */
	@Test
	public void test_1821_BlockUser() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();
		screenUsersManagement.clickOnBlockedUsers();
		screenUsersManagement.selectUserToBlock(ConstantsUserManagement.ALBERTO_DECACERES);
		screenUsersManagement.verifyIfUserBlock(ConstantsUserManagement.ALBERTO_DECACERES);
		int blockUser = screenUsersManagement.searchUserBlocked(ConstantsUserManagement.ALBERTO_DECACERES);
		screenUsersManagement.clickOnCloseBlockedUsers();
		assertEquals("1", String.valueOf(blockUser));
		assertFalse(screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.ALBERTO_DECACERES));
	}

	/**
	 * @testLinkId T-GPRI-3088
	 * @summary Block User
	 */
	@Test
	public void test_1822_UnblockUser() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();
		screenUsersManagement.clickOnBlockedUsers();
		screenUsersManagement.checkUserOnListBlock(ConstantsUserManagement.ALBERTO_DECACERES);
		screenUsersManagement.clickOnUnblockSelectedUsers();
		int blockUser = screenUsersManagement.searchUserBlocked(ConstantsUserManagement.ALBERTO_DECACERES);
		screenUsersManagement.clickOnCloseBlockedUsers();
		assertEquals("0", String.valueOf(blockUser));
		assertTrue(screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.ALBERTO_DECACERES));
	}

	/**
	 * @testLinkId T-GPRI-3089
	 * @summary Block User
	 */
	@Test
	public void test_1823_UnblockMoreOneUser() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();
		screenUsersManagement.clickOnBlockedUsers();
		screenUsersManagement.checkUserOnListBlock(ConstantsUserManagement.ALBERTO_DECACERES);
		screenUsersManagement.clickOnUnblockSelectedUsers();
		screenUsersManagement.checkUserOnListBlock(ConstantsUserManagement.BEN_CHU);
		screenUsersManagement.clickOnUnblockSelectedUsers();
		screenUsersManagement.clickOnCloseBlockedUsers();
		assertTrue(screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.ALBERTO_DECACERES));
		assertTrue(screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.BEN_CHU));
	}
	
	/**
	 * @testLinkId T-GPRI-3090
	 * @summary Block User
	 */
	@Test
	public void test_1824_Filters() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();
		screenUsersManagement.clickOnBlockedUsers();
		screenUsersManagement.selectUserToBlock(ConstantsUserManagement.BEN_CHU);
		screenUsersManagement.clickOnBlock();
		assertTrue(screenUsersManagement.selectUserToBlock(ConstantsUserManagement.ALBERTO_DECACERES));
		assertTrue(screenUsersManagement.selectUserToBlock("Alberto De Caceres"));
		assertFalse(screenUsersManagement.selectUserToBlock(ConstantsUserManagement.USER_INVALID));
		assertFalse(screenUsersManagement.selectUserToBlock(ConstantsUserManagement.USER_INVALID));
		assertEquals("1", String.valueOf(screenUsersManagement.searchUserBlocked(ConstantsUserManagement.BEN_CHU)));
		assertEquals("0", String.valueOf(screenUsersManagement.searchUserBlocked(ConstantsUserManagement.USER_INVALID)));
	}
	
	/**
	 * @testLinkId T-GPRI-3091
	 * @summary Block User
	 */
	@Test
	public void test_1825_BlockUserEreased() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();
		screenUsersManagement.clickOnBlockedUsers();
		screenUsersManagement.selectUserToBlock(ConstantsUserManagement.ALBERTO_DECACERES);
		screenUsersManagement.clearUserFieldToBlock();
		screenUsersManagement.clickOnBlock();
		assertEquals("Please, select one User.", screenUsersManagement.getErrorMsg("Invalid User"));
	}

	/**
	 * @testLinkId T-GPRI-3092
	 * @summary Block User
	 */
	@Test
	public void test_1826_CheckTheXButton() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();
		screenUsersManagement.clickOnBlockedUsers();
		assertTrue(screenUsersManagement.clickOnXCloseBlockedUsers());
	}
}
