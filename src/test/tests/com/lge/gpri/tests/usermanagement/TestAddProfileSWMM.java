package com.lge.gpri.tests.usermanagement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsUserManagement;
import com.lge.gpri.screens.usermanagement.ScreenUsersManagement;
import com.lge.gpri.tests.AbstractTestScenary;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAddProfileSWMM extends AbstractTestScenary {

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3070
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1804_ForUserWithoutProfile() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.SWMM);
		screenUsersManagement.associateUserToProfile(ConstantsUserManagement.ALBERTO_DECACERES, profiles);
		screenUsersManagement.clickOnGPRICarrer();
		assertTrue(screenUsersManagement.verifyGPRICarrerForAssociateUserWithProfile(ConstantsUserManagement.SWMM));
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3071
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1805_ForUserWhoAlreadyhasAProfile() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.SWMM);
		screenUsersManagement.associateUserToProfile(ConstantsUserManagement.ALBERTO_DECACERES, profiles);
		screenUsersManagement.clickOnGPRICarrer();
		assertTrue(screenUsersManagement.verifyGPRICarrerForAssociateUserWithProfile(ConstantsUserManagement.SWMM));
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3072
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1806_ForAnInvalidUser() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.SWMM);
		assertEquals("Please, select one user and one role for this user.", screenUsersManagement
				.verifyAssociateUserWithError(ConstantsUserManagement.USER_INVALID, profiles, "Invalid User/Role"));
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3073
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1807_ForAUserWhoAlreadyHasThisProfile() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.SWMM);
		assertEquals("The user already belongs to the role", screenUsersManagement
				.verifyAssociateUserWithError(ConstantsUserManagement.ALBERTO_DECACERES, profiles, "Error"));
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3074
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1808_WithoutAnyUserSelected() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.SWMM);
		assertEquals("Please, select one user and one role for this user.",
				screenUsersManagement.verifyAssociateUserWithError("", profiles, "Invalid User/Role"));
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3075
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1809_WithAuserErased() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.SWMM);
		screenUsersManagement.fillNewUserField(ConstantsUserManagement.ALBERTO_DECACERES);
		screenUsersManagement.clearUser();
		screenUsersManagement.setProfileAddNewUser(ConstantsUserManagement.SWMM);

		assertEquals("Please, select one user and one role for this user.",
				screenUsersManagement.getErrorMsg("Invalid User/Role"));
	}

}
