package com.lge.gpri.tests.usermanagement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsUserManagement;
import com.lge.gpri.screens.usermanagement.ScreenUsersManagement;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery SCENARY NAME.
 * 
 * @author AUTHOR NAME (can exist more than one)
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAddProfileNVG_PVG extends AbstractTestScenary {

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3064
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1798_ForUserWithoutProfile() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.associateUserToProfile(ConstantsUserManagement.ALBERTO_DECACERES, profiles);
		screenUsersManagement.clickOnGPRICarrer();
		assertTrue(screenUsersManagement.verifyGPRICarrerForAssociateUserWithProfile(ConstantsUserManagement.NVG_PVG));
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3065
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1799_ForUserWhoAlreadyhasAProfile() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.associateUserToProfile(ConstantsUserManagement.ALBERTO_DECACERES, profiles);
		screenUsersManagement.clickOnGPRICarrer();
		assertTrue(screenUsersManagement.verifyGPRICarrerForAssociateUserWithProfile(ConstantsUserManagement.NVG_PVG));
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3066
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1800_ForAnInvalidUser() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		assertEquals("Please, select one user and one role for this user.", screenUsersManagement
				.verifyAssociateUserWithError(ConstantsUserManagement.USER_INVALID, profiles, "Invalid User/Role"));
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3067
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1801_ForAUserWhoAlreadyHasThisProfile() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		assertEquals("The user already belongs to the role", screenUsersManagement
				.verifyAssociateUserWithError(ConstantsUserManagement.ALBERTO_DECACERES, profiles, "Error"));
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3068
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1802_WithoutAnyUserSelected() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		assertEquals("Please, select one user and one role for this user.",
				screenUsersManagement.verifyAssociateUserWithError("", profiles, "Invalid User/Role"));
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3069
	 * @summary Add Profile NVG/PVG For User Without Profile
	 */
	@Test
	public void test_1803_WithAuserErased() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.fillNewUserField(ConstantsUserManagement.ALBERTO_DECACERES);
		screenUsersManagement.clearUser();
		screenUsersManagement.setProfileAddNewUser(ConstantsUserManagement.NVG_PVG);

		assertEquals("Please, select one user and one role for this user.",
				screenUsersManagement.getErrorMsg("Invalid User/Role"));
	}
}
