package com.lge.gpri.tests.usermanagement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsCompare;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.helpers.constants.ConstantsItemInspector;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.helpers.constants.ConstantsUserManagement;
import com.lge.gpri.helpers.constants.ConstantsView;
import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.dashboard.ScreenPerformDashboard;
import com.lge.gpri.screens.usermanagement.ScreenUsersManagement;
import com.lge.gpri.tests.AbstractTestScenary;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAccessUserNVG_PVG extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-3093
	 * @summary User Access Pages
	 */
	@Test
	public void test_1827_UserAccessPages() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformDashboard screenPerformDashboard = new ScreenPerformDashboard(getDriver());
		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.associateUserToProfile(ConstantsUserManagement.ALBERTO_DECACERES, profiles);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.ALBERTO_DECACERES);
		screenUsersManagement.removeAllRolesExcept(profiles.get(0));
		screenUsersManagement.clicOnLoginAs();
		screenPerformDashboard.clickOnCloseInWhatsNewPopUp();

		screenPerformDashboard.clickOnActivityStream();
		boolean isShowActivityStreamDashboard = screenPerformDashboard.verifyShowActivityStreamDashboard();

		sectionPerformMenu.clickOnMenuView();
		String urlViewPage = sectionPerformMenu.getCurrentURL();

		sectionPerformMenu.clickOnMenuItemInspector();
		String itemInspectorURL = sectionPerformMenu.getCurrentURL();

		sectionPerformMenu.clickOnMenuCompare();
		String menuCompareURL = sectionPerformMenu.getCurrentURL();

		sectionPerformMenu.clickOnExpandSideMenuButton();
		sectionPerformMenu.clickOnFollows();
		String FollowsURL = sectionPerformMenu.getCurrentURL();
		Thread.sleep(2000);

		sectionPerformMenu.clickOnMenuHelp();
		String helpURL = sectionPerformMenu.getCurrentURL();

		sectionPerformMenu.clickOnMenuPackage();
		String packageURL = sectionPerformMenu.getCurrentURL();

		assertTrue(isShowActivityStreamDashboard);
		assertEquals(ConstantsView.URL, urlViewPage);
		assertEquals(ConstantsItemInspector.URL, itemInspectorURL);
		assertEquals(ConstantsCompare.URL, menuCompareURL);
		assertEquals(ConstantsGeneric.URL_FOLLOWS, FollowsURL);
		assertEquals(ConstantsGeneric.URL_HELP, helpURL);
		assertEquals(ConstantsPackages.URL, packageURL);
	}

}
