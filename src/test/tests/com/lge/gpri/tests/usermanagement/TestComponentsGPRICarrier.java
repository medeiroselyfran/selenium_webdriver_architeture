package com.lge.gpri.tests.usermanagement;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.Util;
import com.lge.gpri.helpers.constants.ConstantsUserManagement;
import com.lge.gpri.screens.usermanagement.ScreenUsersManagement;
import com.lge.gpri.tests.AbstractTestScenary;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestComponentsGPRICarrier extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-3094
	 * @summary Add Custom Moment
	 */
	@Test
	public void test_1828_AddCustomMoment() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		
		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		screenUsersManagement.waitLoading();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clickOnGPRICarrer();
		screenUsersManagement.clickOnAddCustomMoment();
		screenUsersManagement.fillCustomMomentCarrer(Util.currentDay(),
				ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST);
		screenUsersManagement.clickOnSaveCustomMoment();
		assertTrue(screenUsersManagement
				.verifyGPRICarrerForCustomMoment(ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST));

	}

	/**
	 * @testLinkId T-GPRI-3095
	 * @summary Add Custom Moment
	 */
	@Test
	public void test_1829_CancelAddCustomMoment() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		screenUsersManagement.waitLoading();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clickOnGPRICarrer();
		screenUsersManagement.clickOnAddCustomMoment();
		screenUsersManagement.fillCustomMomentCarrer(ConstantsUserManagement.DATE_26_06_16,
				ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST);
		screenUsersManagement.clickOnCancelCustomMoment();
		assertTrue(screenUsersManagement.verifyCustomMomentisClosed());
	}

	/**
	 * @testLinkId T-GPRI-3096
	 * @summary Add Custom Moment
	 */
	@Test
	public void test_1830_CancelAddCustomMomentWithX() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		screenUsersManagement.waitLoading();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clickOnGPRICarrer();
		screenUsersManagement.clickOnAddCustomMoment();
		screenUsersManagement.fillCustomMomentCarrer(ConstantsUserManagement.DATE_26_06_16,
				ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST);
		screenUsersManagement.clickOnCloseCustomMoment();
		assertTrue(screenUsersManagement.verifyCustomMomentisClosed());
	}

	/**
	 * @testLinkId T-GPRI-3097
	 * @summary Add Custom Moment
	 */
	@Test
	public void test_1831_AddCustomMomentWithoutSelectingTheDate() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		screenUsersManagement.waitLoading();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clickOnGPRICarrer();
		screenUsersManagement.clickOnAddCustomMoment();
		screenUsersManagement.clickOnCancelCustomMoment();
		screenUsersManagement.clickOnAddCustomMoment();
		screenUsersManagement.clickOnSaveCustomMoment();
		assertEquals("Please, select one Date and give some Description.",
				(screenUsersManagement.getErrorMsg("Invalid Career Moment")));
	}

	/**
	 * @testLinkId T-GPRI-3098
	 * @summary Add Custom Moment
	 */
	@Test
	public void test_1832_AddCustomMomentWithoutTypingDescription() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		screenUsersManagement.waitLoading();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clickOnGPRICarrer();
		screenUsersManagement.clickOnAddCustomMoment();
		screenUsersManagement.fillCustomMomentCarrer(ConstantsUserManagement.DATE_26_06_16, "");
		screenUsersManagement.clickOnSaveCustomMoment();
		assertEquals("Please, select one Date and give some Description.",
				(screenUsersManagement.getErrorMsg("Invalid Career Moment")));
	}

	/**
	 * @testLinkId T-GPRI-3099
	 * @summary Add Custom Moment
	 */
	@Test
	public void test_1833_AddCustomMomentWithoutDateAndDescription() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		screenUsersManagement.waitLoading();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clickOnGPRICarrer();
		screenUsersManagement.clickOnAddCustomMoment();
		screenUsersManagement.clickOnCancelCustomMoment();
		screenUsersManagement.clickOnAddCustomMoment();
		screenUsersManagement.clickOnSaveCustomMoment();
		assertEquals("Please, select one Date and give some Description.",
				(screenUsersManagement.getErrorMsg("Invalid Career Moment")));
	}

	/**
	 * @testLinkId T-GPRI-3100
	 * @summary Add Custom Moment
	 */
	@Test
	public void test_1834_RemoveCustomMoment() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		screenUsersManagement.waitLoading();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clickOnGPRICarrer();
		while(screenUsersManagement.clickOnRemoveCustomMoment(ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST)){
			screenUsersManagement.clickOnConfirmRemoveCustomMoment();
		}
		screenUsersManagement.clickOnAddCustomMoment();
		screenUsersManagement.fillCustomMomentCarrer(Util.currentDay(),
				ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST);
		screenUsersManagement.clickOnSaveCustomMoment();
		screenUsersManagement.clickOnRemoveCustomMoment(ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST);
		screenUsersManagement.clickOnConfirmRemoveCustomMoment();
		assertFalse(screenUsersManagement
		.verifyGPRICarrerForCustomMoment(ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST));
	}
	
	/**
	 * @testLinkId T-GPRI-3101
	 * @summary Add Custom Moment
	 */
	@Test
	public void test_1835_CancelRemoveCustomMoment() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		screenUsersManagement.waitLoading();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clickOnGPRICarrer();
		screenUsersManagement.clickOnAddCustomMoment();
		screenUsersManagement.fillCustomMomentCarrer(Util.currentDay(),
				ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST);
		screenUsersManagement.clickOnSaveCustomMoment();
		screenUsersManagement.clickOnRemoveCustomMoment(ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST);
		screenUsersManagement.clickOnCancelRemoveCustomMoment();
		assertTrue(screenUsersManagement
		.verifyGPRICarrerForCustomMoment(ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST));
	}
	
	/**
	 * @testLinkId T-GPRI-3102
	 * @summary Add Custom Moment
	 */
	@Test
	public void test_1836_FilterOfGPRICareerScreen() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		screenUsersManagement.waitLoading();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clickOnGPRICarrer();
		screenUsersManagement.searchGPRICarrer(ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST);
		assertTrue(screenUsersManagement
		.verifyGPRICarrerForCustomMoment(ConstantsUserManagement.CREATE_REASONS_ONLY_FOR_TEST));
	}
	
	/**
	 * @testLinkId T-GPRI-3103
	 * @summary Add Custom Moment
	 */
	@Test
	public void test_1837_CloseTheGPRICareerScreen() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		screenUsersManagement.waitLoading();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clickOnGPRICarrer();
		screenUsersManagement.clickOnCloseGPRICarrerButton();
		assertTrue(screenUsersManagement.verifyGPRICareerisClosed());
	}
	
	/**
	 * @testLinkId T-GPRI-3104
	 * @summary Add Custom Moment
	 */
	@Test
	public void test_1838_CloseTheGPRICareerScreenWithX() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		screenUsersManagement.waitLoading();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clickOnGPRICarrer();
		screenUsersManagement.closeGPRICarrer();
		assertTrue(screenUsersManagement.verifyGPRICareerisClosed());
	}

}
