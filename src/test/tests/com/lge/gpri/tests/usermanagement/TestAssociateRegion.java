package com.lge.gpri.tests.usermanagement;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsUserManagement;
import com.lge.gpri.screens.usermanagement.ScreenPerformRegions;
import com.lge.gpri.screens.usermanagement.ScreenUsersManagement;
import com.lge.gpri.tests.AbstractTestScenary;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAssociateRegion extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-3076
	 * @summary
	 */
	@Test
	public void test_1810_UserWithTheOneRegion() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();

		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.associateUserToProfile(ConstantsUserManagement.ALBERTO_DECACERES, profiles);

		screenUsersManagement.addRegionOnNVGPVG(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);

		assertTrue(screenUsersManagement.verifyIfRegionIsAssociated(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA));
	}

	/**
	 * @testLinkId T-GPRI-3077
	 * @summary
	 */
	@Test
	public void test_1811_UserWithMoreOneRegion() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();

		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.associateUserToProfile(ConstantsUserManagement.ALBERTO_DECACERES, profiles);

		screenUsersManagement.addRegionOnNVGPVG(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenUsersManagement.addRegionOnNVGPVG(ConstantsUserManagement.EUROPE);
		screenUsersManagement.addRegionOnNVGPVG(ConstantsUserManagement.CHINA_SOUTHEAST_ASIA);

		assertTrue(screenUsersManagement.verifyIfRegionIsAssociated(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA));
		assertTrue(screenUsersManagement.verifyIfRegionIsAssociated(ConstantsUserManagement.EUROPE));
		assertTrue(screenUsersManagement.verifyIfRegionIsAssociated(ConstantsUserManagement.CHINA_SOUTHEAST_ASIA));
	}

	/**
	 * @testLinkId T-GPRI-3078
	 * @summary
	 */
	@Test
	public void test_1812_CancelRemovalTheRegionAssociatedWithUser() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();

		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.associateUserToProfile(ConstantsUserManagement.ALBERTO_DECACERES, profiles);

		screenUsersManagement.addRegionOnNVGPVG(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenUsersManagement.removeRegionNVGPVG(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		assertTrue(screenUsersManagement.cancelRemoveRegion());
		assertTrue(screenUsersManagement.verifyIfRegionIsAssociated(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA));
	}

	/**
	 * @testLinkId T-GPRI-3079
	 * @summary
	 */
	@Test
	public void test_1813_RemoveOneRegionAssociatedWithUser() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();

		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.associateUserToProfile(ConstantsUserManagement.ALBERTO_DECACERES, profiles);

		screenUsersManagement.addRegionOnNVGPVG(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenUsersManagement.removeRegionNVGPVG(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		assertTrue(screenUsersManagement.confirmRemove());
	}

	/**
	 * @testLinkId T-GPRI-3080
	 * @summary
	 */
	@Test
	public void test_1814_RemoveMoreOneRegionAssociatedWithUser() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.openPage();

		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		screenUsersManagement.associateUserToProfile(ConstantsUserManagement.ALBERTO_DECACERES, profiles);

		screenUsersManagement.addRegionOnNVGPVG(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenUsersManagement.addRegionOnNVGPVG(ConstantsUserManagement.EUROPE);
		screenUsersManagement.addRegionOnNVGPVG(ConstantsUserManagement.CHINA_SOUTHEAST_ASIA);

		screenUsersManagement.removeRegionNVGPVG(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		boolean confirmRemoveRegion1 = screenUsersManagement.confirmRemove();
		screenUsersManagement.removeRegionNVGPVG(ConstantsUserManagement.EUROPE);
		boolean confirmRemoveRegion2 = screenUsersManagement.confirmRemove();
		screenUsersManagement.removeRegionNVGPVG(ConstantsUserManagement.CHINA_SOUTHEAST_ASIA);
		boolean confirmRemoveRegion3 = screenUsersManagement.confirmRemove();

		assertTrue(confirmRemoveRegion1);
		assertTrue(confirmRemoveRegion2);
		assertTrue(confirmRemoveRegion3);
	}

	/**
	 * @testLinkId T-GPRI-3081
	 * @summary One user who have the profile NVG/PVG a region by Regions screen
	 */
	@Test
	public void test_1815_OneUserWhoHaveTheProfileNVGPVGRegionByRegionsScreen() throws Exception {
		ScreenPerformRegions screenPerformRegions = new ScreenPerformRegions(getDriver());

		screenPerformRegions.openPage();
		screenPerformRegions.searchRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnTabNVGPVG();
		screenPerformRegions.selectUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		screenPerformRegions.addUserNVGPVGButton();
		screenPerformRegions.searchUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		assertTrue(screenPerformRegions.verifyIfUserIsAssociatedWithRegion(ConstantsUserManagement.ALBERTO_DECACERES));
	}

	/**
	 * @testLinkId T-GPRI-3082
	 * @summary One user who have the profile NVG/PVG a region by Regions screen
	 */
	@Test
	public void test_1816_MoreOneUserWhoHaveTheProfileNVGPVGRegionByRegionsScreen() throws Exception {
		ScreenPerformRegions screenPerformRegions = new ScreenPerformRegions(getDriver());

		screenPerformRegions.openPage();
		screenPerformRegions.searchRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnTabNVGPVG();
		screenPerformRegions.selectUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		screenPerformRegions.moveToRight();
		screenPerformRegions.addUserNVGPVGButton();
		screenPerformRegions.selectUserNVGPVG("abhai.singh");
		screenPerformRegions.moveToRight();
		screenPerformRegions.addUserNVGPVGButton();
		screenPerformRegions.searchUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		boolean user1 = screenPerformRegions
				.verifyIfUserIsAssociatedWithRegion(ConstantsUserManagement.ALBERTO_DECACERES);
		screenPerformRegions.searchUserNVGPVG("abhai.singh");
		boolean user2 = screenPerformRegions.verifyIfUserIsAssociatedWithRegion("abhai.singh");

		assertTrue(user1);
		assertTrue(user2);
	}

	/**
	 * @testLinkId T-GPRI-3083
	 * @summary One user who have the profile NVG/PVG a region by Regions screen
	 */
	@Test
	public void test_1817_CancelTheAssociateOneUserWhoHaveTheProfileVNGPVGARegionByRegionsScreen() throws Exception {
		ScreenPerformRegions screenPerformRegions = new ScreenPerformRegions(getDriver());

		screenPerformRegions.openPage();
		screenPerformRegions.searchRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnTabNVGPVG();
		screenPerformRegions.selectUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		screenPerformRegions.moveToRight();
		screenPerformRegions.addUserNVGPVGButton();

		screenPerformRegions.refreshPage();
		screenPerformRegions.clickOnTabNVGPVG();
		screenPerformRegions.searchUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		assertTrue(screenPerformRegions.verifyIfUserIsAssociatedWithRegion(ConstantsUserManagement.NO_PVG_AVAILABLE));
	}

	/**
	 * @testLinkId T-GPRI-3084
	 * @summary One user who have the profile NVG/PVG a region by Regions screen
	 */
	@Test
	public void test_1818_CancelTheRemovalOfAUserWhoIsAddedToTheProfileRegionNVGPVGRegionsScreen() throws Exception {
		ScreenPerformRegions screenPerformRegions = new ScreenPerformRegions(getDriver());
		ScreenUsersManagement usersManagement = new ScreenUsersManagement(getDriver());

		screenPerformRegions.openPage();
		screenPerformRegions.searchRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnTabNVGPVG();
		screenPerformRegions.selectUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		screenPerformRegions.addUserNVGPVGButton();
		screenPerformRegions.searchUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		screenPerformRegions.removeUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		screenPerformRegions.cancelRemove();
		screenPerformRegions.clickOnSaveRegion();

		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		usersManagement.openPage();
		usersManagement.searchAndSelectUser(ConstantsUserManagement.ALBERTO_DECACERES);

		assertTrue(usersManagement.verifyIfProfileIsAssociatedWithUser());

	}

	/**
	 * @testLinkId T-GPRI-3085
	 * @summary One user who have the profile NVG/PVG a region by Regions screen
	 */
	@Test
	public void test_1819_RemoveOneUserWhoIsAddedTheProfileRegionListNVGPVG() throws Exception {
		ScreenPerformRegions screenPerformRegions = new ScreenPerformRegions(getDriver());
		ScreenUsersManagement usersManagement = new ScreenUsersManagement(getDriver());

		screenPerformRegions.openPage();
		screenPerformRegions.searchRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnTabNVGPVG();
		screenPerformRegions.selectUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		screenPerformRegions.moveToRight();
		screenPerformRegions.addUserNVGPVGButton();
		screenPerformRegions.searchUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		screenPerformRegions.removeUserNVGPVG(ConstantsUserManagement.ALBERTO_DECACERES);
		screenPerformRegions.confirmRemove();
		screenPerformRegions.clickOnSaveRegion();
		screenPerformRegions.clickOkSavedRegionButton();

		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		usersManagement.openPage();
		usersManagement.searchAndSelectUser(ConstantsUserManagement.ALBERTO_DECACERES);

		assertTrue(verifyIfUserNotAssociatedWithRegion(usersManagement, ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA,
				ConstantsUserManagement.ALBERTO_DECACERES));

	}
	
	/**
	 * @testLinkId T-GPRI-3086
	 * @summary One user who have the profile NVG/PVG a region by Regions screen
	 */
	@Test
	public void test_1820_RemoveMoreOneUserWhoIsAddedTheProfileRegionListNVGPVG() throws Exception {
		ScreenPerformRegions screenPerformRegions = new ScreenPerformRegions(getDriver());
		ScreenUsersManagement usersManagement = new ScreenUsersManagement(getDriver());

		screenPerformRegions.openPage();
		screenPerformRegions.searchRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnRegion(ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA);
		screenPerformRegions.clickOnTabNVGPVG();
		
		List<String> users = new ArrayList<String>();
		users.add(ConstantsUserManagement.ALBERTO_DECACERES);
		users.add(ConstantsUserManagement.BEN_CHU);
		
		for(int i = 0; i < users.size(); i++){
			screenPerformRegions.selectUserNVGPVG(users.get(i));
			screenPerformRegions.moveToRight();
			screenPerformRegions.addUserNVGPVGButton();
			screenPerformRegions.searchUserNVGPVG(users.get(i));
			screenPerformRegions.removeUserNVGPVG(users.get(i));
			screenPerformRegions.confirmRemove();
		}
		screenPerformRegions.clickOnSaveRegion();
		screenPerformRegions.clickOkSavedRegionButton();

		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.NVG_PVG);
		usersManagement.openPage();
		usersManagement.searchAndSelectUser(ConstantsUserManagement.ALBERTO_DECACERES);
		boolean user1 = verifyIfUserNotAssociatedWithRegion(usersManagement, ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA,
				ConstantsUserManagement.ALBERTO_DECACERES);
		usersManagement.searchAndSelectUser(ConstantsUserManagement.BEN_CHU);
		assertTrue(verifyIfUserNotAssociatedWithRegion(usersManagement, ConstantsUserManagement.ASIA_MIDDLE_EAST_AFRICA,
				ConstantsUserManagement.BEN_CHU));
		assertTrue(user1);
	}

	private boolean verifyIfUserNotAssociatedWithRegion(ScreenUsersManagement usersManagement, String region,
			String profile) throws Exception {
		boolean isOk = true;
		ArrayList<String> itemsOfRole = usersManagement.getItemsOfRole(profile, 1);
		for (int i = 0; i < itemsOfRole.size(); i++) {
			if (region.equals(itemsOfRole.get(i))) {
				isOk = false;
				break;
			}
		}
		return isOk;
	}
}
