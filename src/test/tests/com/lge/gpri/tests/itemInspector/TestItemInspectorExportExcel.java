package com.lge.gpri.tests.itemInspector;

import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsItemInspector;
import com.lge.gpri.screens.itemInspector.ScreenPerformItemInspector;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Export Excel.
*  
* @author Gabriel Costa do Nascimento
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestItemInspectorExportExcel extends AbstractTestScenary{

	/**
	* @testLinkId T-GPRI-2673
	* @summary Generation with one item
	*/
	@Test
	public void test_1251_GenerationWithOneItem() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_IAAPN_PROTOCOL_TYPE);
		screenPerformItemInspector.selectContryOptions(ConstantsItemInspector.BRAZIL);
		screenPerformItemInspector.clickOnShow();
		screenPerformItemInspector.exportExcel();
		String fileName = ConstantsItemInspector.IAAPN_PROTOCOL_TYPE;
		String content = screenPerformItemInspector.getContentExcel(fileName);
		screenPerformItemInspector.deleteFileDownloaded(fileName);
	      
		assertTrue(content.contains(ConstantsItemInspector.IAAPN_PROTOCOL_TYPE));
	}
	
	/**
	* @testLinkId T-GPRI-2674
	* @summary Generation with multi item
	*/
	@Test
	public void test_1252_GenerationWithMultiItem() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_IAAPN_PROTOCOL_TYPE);
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_ITEM_NAME);
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_ITEM_HOME_NETWORK);
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		screenPerformItemInspector.selectContryOptions(ConstantsItemInspector.BRAZIL);
		screenPerformItemInspector.clickOnShow();
		screenPerformItemInspector.exportExcel();
		String fileName = ConstantsItemInspector.IAAPN_PROTOCOL_TYPE+", "+ConstantsItemInspector.NAME+", "+ConstantsItemInspector.HOME_NETWORK+" and other";
		String content = screenPerformItemInspector.getContentExcel(fileName);
		screenPerformItemInspector.deleteFileDownloaded(fileName);
	      
		assertTrue(content.contains(ConstantsItemInspector.IAAPN_PROTOCOL_TYPE));
		assertTrue(content.contains(ConstantsItemInspector.NAME));
		assertTrue(content.contains(ConstantsItemInspector.HOME_NETWORK));
		assertTrue(content.contains(ConstantsItemInspector.ITEM_LTE_MODE));
	}
	
}
