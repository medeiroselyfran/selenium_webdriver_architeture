package com.lge.gpri.tests.itemInspector;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsItemInspector;
import com.lge.gpri.screens.itemInspector.ScreenPerformItemInspector;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Information Item.
*  
* @author Gabriel Costa do Nascimento
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestItemInspectorInformation extends AbstractTestScenary{

	/**
	* @testLinkId T-GPRI-2664
	* @summary Show Item
	*/
	@Test
	public void test_1233_ShowItem() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_IAAPN_PROTOCOL_TYPE);
		screenPerformItemInspector.selectAllContriesOptions();
		screenPerformItemInspector.selectAllOperatorsOptions();
		screenPerformItemInspector.selectAllModelsOptions();
		screenPerformItemInspector.selectAllGTMsLTMsOptions();
		screenPerformItemInspector.clickOnShow();
		boolean tableIsVisible = screenPerformItemInspector.valuesTableIsVisible();
		assertTrue(tableIsVisible);
	}
	
	/**
	* @testLinkId T-GPRI-2665
	* @summary Show only country Item
	*/
	@Test
	public void test_1234_ShowOnlyCountryItem() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_IAAPN_PROTOCOL_TYPE);
		screenPerformItemInspector.selectContryOptions(ConstantsItemInspector.BRAZIL);
		screenPerformItemInspector.clickOnShow();
		boolean tableIsVisible = screenPerformItemInspector.valuesTableIsVisible();
		assertTrue(tableIsVisible);
	}
	
	/**
	* @testLinkId T-GPRI-2666
	* @summary Show only operator Item
	*/
	@Test
	public void test_1235_ShowOnlyOperatorItem() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_IAAPN_PROTOCOL_TYPE);
		screenPerformItemInspector.selectOperatorOptions(ConstantsItemInspector.OPERATOR_012MOBILE);
		screenPerformItemInspector.clickOnShow();
		boolean tableIsVisible = screenPerformItemInspector.valuesTableIsVisible();
		assertTrue(tableIsVisible);
	}
	
	/**
	* @testLinkId T-GPRI-2667
	* @summary Show only model Item
	*/
	@Test
	public void test_1237_ShowOnlyModelItem() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_IAAPN_PROTOCOL_TYPE);
		screenPerformItemInspector.selectModelOptions(ConstantsItemInspector.LGP970);
		screenPerformItemInspector.clickOnShow();
		boolean tableIsVisible = screenPerformItemInspector.valuesTableIsVisible();
		assertTrue(tableIsVisible);
	}
	
	/**
	* @testLinkId T-GPRI-2668
	* @summary Show only GTMs/LTMs Item
	*/
	@Test
	public void test_1240_ShowOnlyGTMsLTMsItem() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_IAAPN_PROTOCOL_TYPE);
		screenPerformItemInspector.selectGTMLTMOptions(ConstantsItemInspector.ABHAI_SINGH);
		screenPerformItemInspector.clickOnShow();
		boolean tableIsVisible = screenPerformItemInspector.valuesTableIsVisible();
		assertTrue(tableIsVisible);
	}
	
	/**
	* @testLinkId T-GPRI-2669
	* @summary Try show item without select options
	*/
	@Test
	public void test_1241_TryShowItemWithoutSelectOptions() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_IAAPN_PROTOCOL_TYPE);
		screenPerformItemInspector.clickOnShow();
		String message = screenPerformItemInspector.getMessageAlert();
		assertEquals(ConstantsItemInspector.ALERT_ITEM_INSPECTOR,message);
	}
	
	/**
	* @testLinkId T-GPRI-2670
	* @summary Try show item without select options and without select item
	*/
	@Test
	public void test_1242_TryShowItemWithoutSelectOptionsAndWithoutSelectItem() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.clickOnShow();
		String message = screenPerformItemInspector.getMessageAlert();
		assertEquals(ConstantsItemInspector.ALERT_ITEM_INSPECTOR,message);
	}
	
	/**
	* @testLinkId T-GPRI-2671
	* @summary Show multi items
	*/
	@Test
	public void test_1244_ShowMultiItems() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree("");
		screenPerformItemInspector.selectContryOptions(ConstantsItemInspector.BRAZIL);
		screenPerformItemInspector.clickOnShow();
		boolean tableIsVisible = screenPerformItemInspector.valuesTableIsVisible();
		assertTrue(tableIsVisible);
	}
	
	//This test breaks the system
//	/**
//	* @testLinkId T-GPRI-2672
//	* @summary Show all items
//	*/
//	@Test
//	public void test_2672_ShowAllItems() throws Exception {
//		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
//		
//		screenPerformItemInspector.openPage();
//		screenPerformItemInspector.selectItemInTree("");
//		screenPerformItemInspector.selectAllContriesOptions();
//		screenPerformItemInspector.selectAllOperatorsOptions();
//		screenPerformItemInspector.selectAllModelsOptions();
//		screenPerformItemInspector.selectAllGTMsLTMsOptions();
//		screenPerformItemInspector.clickOnShow();
//		boolean tableIsVisible = screenPerformItemInspector.valuesTableIsVisible();
//		assertTrue(tableIsVisible);
//	}
}
