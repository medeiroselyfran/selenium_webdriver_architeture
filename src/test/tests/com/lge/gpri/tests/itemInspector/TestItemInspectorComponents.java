package com.lge.gpri.tests.itemInspector;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsItemInspector;
import com.lge.gpri.screens.itemInspector.ScreenPerformItemInspector;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Components.
*  
* @author AUTHOR NAME (can exist more than one) 
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestItemInspectorComponents extends AbstractTestScenary{

	/**
	* @testLinkId T-GPRI-2675
	* @summary Hidden Options
	*/
	@Test
	public void test_1298_HiddenOptions() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.hideOptions();
		boolean isOpen = screenPerformItemInspector.optionsPanelIsOpen();
		
		assertFalse(isOpen);
	}
	
	/**
	* @testLinkId T-GPRI-2676
	* @summary Show Options
	*/
	@Test
	public void test_1299_ShowOptions() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.hideOptions();
		screenPerformItemInspector.showOptions();
		boolean isOpen = screenPerformItemInspector.optionsPanelIsOpen();
		
		assertTrue(isOpen);
	}
	
	/**
	* @testLinkId T-GPRI-2677
	* @summary Hidden Filters
	*/
	@Test
	public void test_1300_HiddenFilters() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.hideFilters();
		boolean isOpen = screenPerformItemInspector.filtersPanelIsOpen();
		
		assertFalse(isOpen);
	}
	
	/**
	* @testLinkId T-GPRI-2678
	* @summary Show Filters
	*/
	@Test
	public void test_1301_ShowFilters() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.hideFilters();
		screenPerformItemInspector.showFilters();
		boolean isOpen = screenPerformItemInspector.filtersPanelIsOpen();
		
		assertTrue(isOpen);
	}
	
	/**
	* @testLinkId T-GPRI-2679
	* @summary Show Entries
	*/
	@Test
	public void test_1355_ShowEntries() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		boolean result_10 = true, result_25 = true, result_50 = true, result_100 = true;
		int lineNumbers, end;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_ITEM_HOME_NETWORK);
		screenPerformItemInspector.selectContryOptions(ConstantsItemInspector.BRAZIL);
		screenPerformItemInspector.clickOnShow();
		screenPerformItemInspector.showEntries("10");
		lineNumbers = screenPerformItemInspector.getLineNumberEntriesShow();
		end = screenPerformItemInspector.getEndEntrieShowingInfo();
		if(end>10||lineNumbers!=end){
			result_10 = false;
		}
		screenPerformItemInspector.showEntries("25");
		lineNumbers = screenPerformItemInspector.getLineNumberEntriesShow();
		end = screenPerformItemInspector.getEndEntrieShowingInfo();
		if(end>25||lineNumbers!=end){
			result_25 = false;
		}
		screenPerformItemInspector.showEntries("50");
		lineNumbers = screenPerformItemInspector.getLineNumberEntriesShow();
		end = screenPerformItemInspector.getEndEntrieShowingInfo();
		if(end>50||lineNumbers!=end){
			result_50 = false;
		}
		screenPerformItemInspector.showEntries("100");
		lineNumbers = screenPerformItemInspector.getLineNumberEntriesShow();
		end = screenPerformItemInspector.getEndEntrieShowingInfo();
		if(end>100||lineNumbers!=end){
			result_100 = false;
		}
		
		assertTrue(result_10);
		assertTrue(result_25);
		assertTrue(result_50);
		assertTrue(result_100);
	}
	
	/**
	* @testLinkId T-GPRI-2680
	* @summary Navigation
	*/
	@Test
	public void test_1357_Navigation() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		int initialPage, actualPage, numberOfPages;
		boolean nextOk = false, previousOk = false, lastOk = false, firstOk = false, selectPageOk = false;
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree(ConstantsItemInspector.PATH_ITEM_HOME_NETWORK);
		screenPerformItemInspector.selectContryOptions(ConstantsItemInspector.BRAZIL);
		screenPerformItemInspector.clickOnShow();
		screenPerformItemInspector.showEntries("10");
		numberOfPages = screenPerformItemInspector.getNumberOfPages();
		initialPage = screenPerformItemInspector.getActivePage();
		screenPerformItemInspector.paginateNext();
		actualPage = screenPerformItemInspector.getActivePage();
		if(initialPage<actualPage||actualPage==numberOfPages){
			nextOk = true;
		}
		initialPage = screenPerformItemInspector.getActivePage();
		screenPerformItemInspector.paginatePrevious();
		actualPage = screenPerformItemInspector.getActivePage();
		if(initialPage>actualPage||actualPage==1){
			previousOk = true;
		}
		screenPerformItemInspector.goToLastPage();
		actualPage = screenPerformItemInspector.getActivePage();
		if(actualPage==numberOfPages){
			lastOk = true;
		}
		screenPerformItemInspector.goToFirstPage();
		actualPage = screenPerformItemInspector.getActivePage();
		if(actualPage==1){
			firstOk = true;
		}
		screenPerformItemInspector.clickOnVisiblePage("3");
		actualPage = screenPerformItemInspector.getActivePage();
		if(actualPage==3){
			selectPageOk = true;
		}
		
		assertTrue(nextOk);
		assertTrue(previousOk);
		assertTrue(lastOk);
		assertTrue(firstOk);
		assertTrue(selectPageOk);
	}
	
	/**
	* @testLinkId T-GPRI-2681
	* @summary Search
	*/
	@Test
	public void _1359_testSearch() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		boolean resultValid = false, resultIvalid = false;
		int lines;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectItemInTree("");
		screenPerformItemInspector.selectModelOptions(ConstantsItemInspector.LGH815);
		screenPerformItemInspector.clickOnShow();
		screenPerformItemInspector.searchInResult(ConstantsItemInspector.BLC);
		lines = screenPerformItemInspector.getLineNumberEntriesShow();
		if (lines!=0){
			resultValid = true;
		}
		screenPerformItemInspector.searchInResult(ConstantsItemInspector.BLCG);
		lines = screenPerformItemInspector.getLineNumberEntriesShow();
		if (lines==0){
			resultIvalid = true;
		}
		
		assertTrue(resultValid);
		assertTrue(resultIvalid);
	}
}
