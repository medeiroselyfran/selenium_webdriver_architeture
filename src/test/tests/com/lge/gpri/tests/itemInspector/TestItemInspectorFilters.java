package com.lge.gpri.tests.itemInspector;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsItemInspector;
import com.lge.gpri.screens.itemInspector.ScreenPerformItemInspector;
import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Filters.
*  
* @author Gabriel Costa do Nascimento
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestItemInspectorFilters extends AbstractTestScenary{

	/**
	* @testLinkId T-GPRI-2652
	* @summary Check Filter Auto Commit (assert the first item of each parent directory)
	*/
	@Test
	public void test_1274_AutoCommit() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_AUTO_COMMIT);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_AUTO_COMMIT, ConstantsItemInspector.YES);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.AUTO_COMMIT);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformItemInspector.openPage();
			screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_AUTO_COMMIT);
			screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_AUTO_COMMIT, ConstantsItemInspector.NO);
			pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformItemInspector.newTab("");
				screenPerformItemInspector.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.AUTO_COMMIT);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformItemInspector.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2653
	* @summary Check Filter Default Hide on Mother (assert the first item of each parent directory)
	*/
	@Test
	public void test_1275_DefaultHideOnMother() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_DEFAULT_HIDE_ON_MOTHER, ConstantsItemInspector.YES);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.DEFAULT_HIDE_ON_MOTHER);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformItemInspector.openPage();
			screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_DEFAULT_HIDDEN_ON_MOTHER);
			screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_DEFAULT_HIDE_ON_MOTHER, ConstantsItemInspector.NO);
			pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformItemInspector.newTab("");
				screenPerformItemInspector.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.DEFAULT_HIDE_ON_MOTHER);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformItemInspector.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2654
	* @summary Check Filter Mandatory (assert the first item of each parent directory)
	*/
	@Test
	public void test_1276_Mandatory() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_MANDATORY);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_MANDATORY, ConstantsItemInspector.YES);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformItemInspector.openPage();
			screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_MANDATORY);
			screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_MANDATORY, ConstantsItemInspector.NO);
			pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformItemInspector.newTab("");
				screenPerformItemInspector.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformItemInspector.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2655
	* @summary Check Filter Mandatory for Buyer Child (assert the first item of each parent directory)
	*/
	@Test
	public void test_1277_MandatoryForBuyerChild() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_MANDATORY_FOR_BUYER_CHILD, ConstantsItemInspector.YES);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY_FOR_BUYER_CHILD);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformItemInspector.openPage();
			screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_MANDATORY);
			screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_MANDATORY, ConstantsItemInspector.NO);
			pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformItemInspector.newTab("");
				screenPerformItemInspector.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformItemInspector.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2656
	* @summary Check Filter Multiple (assert the first item of each parent directory)
	*/
	@Test
	public void test_1278_Multiple() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_MULTIPLE);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_MULTIPLE, ConstantsItemInspector.YES);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MULTIPLE);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformItemInspector.openPage();
			screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_MULTIPLE);
			screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_MULTIPLE, ConstantsItemInspector.NO);
			pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformItemInspector.newTab("");
				screenPerformItemInspector.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MULTIPLE);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformItemInspector.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2657
	* @summary Check Filter Name (assert the first item of each parent directory)
	*/
	@Test
	public void test_1279_Name() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_NAME);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_NAME, ConstantsItemInspector.HOME);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_NAME);
			resultOk = value.contains(ConstantsItemInspector.HOME);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2658
	* @summary Check Filter SmartCA Types (assert the first item of each parent directory)
	*/
	@Test
	public void test_1285_SmartCATypes() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_SMARTCA_TYPES);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_SMARTCA_TYPES, ConstantsItemInspector.CUSTOMIZER);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_SMARTCA_TYPES);
			resultOk = value.equals(ConstantsItemInspector.CUSTOMIZER);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2659
	* @summary Check Filter Tag Types (assert the first item of each parent directory)
	*/
	@Test
	public void test_1286_TagTypes() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_TAG_TYPES);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_TAG_TYPES, ConstantsItemInspector.ANDROID_VERSION);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_TAG_TYPES);
			resultOk = value.contains(ConstantsItemInspector.ANDROID_VERSION);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2660
	* @summary Check Filter Tag Values (assert the first item of each parent directory)
	*/
	@Test
	public void test_1287_TagValues() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_TAG_VALUES);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_TAG_VALUES, ConstantsItemInspector.ANDROID_VERSION, ConstantsItemInspector.ANDROID_VERSION);
		ArrayList<String> allVersions = screenPerformItemInspector.getAllValuesOfTagValues(ConstantsItemInspector.ANDROID_VERSION);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_TAG_VALUES);
			String[] versions = value.split(",");
			boolean hasSomeOne = false;
			for(int i=0;i<versions.length&&resultOk;i++){
				if(versions[i].contains(ConstantsItemInspector.ANDROID_VERSION)){
					hasSomeOne = true;
					resultOk = allVersions.contains(versions[i].replace(ConstantsItemInspector.ANDROID_VERSION+" ",""));
				}
			}
			if(!hasSomeOne){resultOk = false;}
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2661
	* @summary Check Filter Type (assert the first item of each parent directory)
	*/
	@Test
	public void test_1288_Type() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_TYPE);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_TYPE, ConstantsItemInspector.RESOURCE);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_TYPE);
			resultOk = value.contains(ConstantsItemInspector.RESOURCE);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2662
	* @summary Check Filter Warnings (assert the first item of each parent directory)
	*/
	@Test
	public void test_1289_Warnings() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_WARNINGS);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_WARNINGS, ConstantsItemInspector.COTA);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_WARNINGS);
			resultOk = value.contains(ConstantsItemInspector.COTA);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2663
	* @summary Check Filter Has Links (assert the first item of each parent directory)
	*/
	@Test
	public void test_1292_HasLinks() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_HAS_LINK);
		screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_HAS_LINK, ConstantsItemInspector.YES);
		pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformItemInspector.newTab("");
			screenPerformItemInspector.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_HAS_LINK);
			resultOk = value=="true";
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformItemInspector.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformItemInspector.openPage();
			screenPerformItemInspector.selectSomeFilter(ConstantsItemInspector.FILTER_HAS_LINK);
			screenPerformItemInspector.setInFilterValue(ConstantsItemInspector.FILTER_HAS_LINK, ConstantsItemInspector.NO);
			pathItems = screenPerformItemInspector.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformItemInspector.newTab("");
				screenPerformItemInspector.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_HAS_LINK);
				resultOk = value=="false";
				yesProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformItemInspector.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
}
