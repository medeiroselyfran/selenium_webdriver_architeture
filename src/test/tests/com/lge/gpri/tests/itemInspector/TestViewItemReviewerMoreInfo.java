package com.lge.gpri.tests.itemInspector;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsItemInspector;
import com.lge.gpri.helpers.constants.ConstantsUserManagement;
import com.lge.gpri.screens.itemInspector.ScreenPerformItemInspector;
import com.lge.gpri.screens.usermanagement.ScreenUsersManagement;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery View Item Reviewer of the More Info.
*  
* @author AUTHOR NAME (can exist more than one) 
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestViewItemReviewerMoreInfo extends AbstractTestScenary{
	
	/**
	* @testLinkId T-GPRI-2682
	* @summary Item Review to one Item and one Region
	*/
	@Test
	public void test_1377_AssociateUserWithOneRegion() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = false, addSuccess;
		
		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.ITEM_REVIEWER);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsItemInspector.ALBERTO_DECACERES);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsItemInspector.ALBERTO_DECACERES);
		
		addSuccess = screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		if(!addSuccess){
			screenUsersManagement.removeRegionOnItemReviewer(ConstantsItemInspector.CANADA);
			screenUsersManagement.confirmRemoveRegion();
			screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		}
		addSuccess = screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		if(!addSuccess){
			screenUsersManagement.removeItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
			screenUsersManagement.confirmRemoveItem();
			screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		}
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.moreInfoRightClickItem(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		String reviewers = screenPerformItemInspector.getReviewersFromMoreInfo(ConstantsItemInspector.CANADA);
		if(reviewers.contains(ConstantsItemInspector.ALBERTO_DECACERES)){
			resultOk = true;
		}
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-2683
	* @summary More than one Item Reviewer for same region
	*/
	@Test
	public void test_1412_MoreThanOneItemReviewerForSameRegion() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultCanadaOk = false, addSuccess;
		
		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.ITEM_REVIEWER);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ABHAI_SINGH, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsItemInspector.ABHAI_SINGH);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ABHAI_SINGH, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsItemInspector.ABHAI_SINGH);
		
		addSuccess = screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		if(!addSuccess){
			screenUsersManagement.removeRegionOnItemReviewer(ConstantsItemInspector.CANADA);
			screenUsersManagement.confirmRemoveRegion();
			screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		}
		addSuccess = screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		if(!addSuccess){
			screenUsersManagement.removeItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
			screenUsersManagement.confirmRemoveItem();
			screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		}
		msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsItemInspector.ALBERTO_DECACERES);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsItemInspector.ALBERTO_DECACERES);
		
		addSuccess = screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		if(!addSuccess){
			screenUsersManagement.removeRegionOnItemReviewer(ConstantsItemInspector.CANADA);
			screenUsersManagement.confirmRemoveRegion();
			screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		}
		addSuccess = screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		if(!addSuccess){
			screenUsersManagement.removeItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
			screenUsersManagement.confirmRemoveItem();
			screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		}
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.moreInfoRightClickItem(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		String reviewersCanada = screenPerformItemInspector.getReviewersFromMoreInfo(ConstantsItemInspector.CANADA);
		if(reviewersCanada.contains(ConstantsItemInspector.ABHAI_SINGH)&&reviewersCanada.contains(ConstantsItemInspector.ALBERTO_DECACERES)){
			resultCanadaOk = true;
		}
		
		assertTrue(resultCanadaOk);
	}
	
	/**
	* @testLinkId T-GPRI-2684
	* @summary More than one Item Reviewer for different regions
	*/
	@Test
	public void test_1417_MoreThanOneItemReviewerForDifferentRegions() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultEuropeOk = false, resultCanadaOk = false, addSuccess;
		
		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.ITEM_REVIEWER);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ABHAI_SINGH, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsItemInspector.ABHAI_SINGH);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ABHAI_SINGH, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsItemInspector.ABHAI_SINGH);
		
		addSuccess = screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.EUROPE);
		if(!addSuccess){
			screenUsersManagement.removeRegionOnItemReviewer(ConstantsItemInspector.EUROPE);
			screenUsersManagement.confirmRemoveRegion();
			screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.EUROPE);
		}
		addSuccess = screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		if(!addSuccess){
			screenUsersManagement.removeItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
			screenUsersManagement.confirmRemoveItem();
			screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		}
		msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsItemInspector.ALBERTO_DECACERES);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsItemInspector.ALBERTO_DECACERES);
		
		addSuccess = screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		if(!addSuccess){
			screenUsersManagement.removeRegionOnItemReviewer(ConstantsItemInspector.CANADA);
			screenUsersManagement.confirmRemoveRegion();
			screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		}
		addSuccess = screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		if(!addSuccess){
			screenUsersManagement.removeItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
			screenUsersManagement.confirmRemoveItem();
			screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		}
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.moreInfoRightClickItem(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		String reviewersEurope = screenPerformItemInspector.getReviewersFromMoreInfo(ConstantsItemInspector.EUROPE);
		String reviewersCanada = screenPerformItemInspector.getReviewersFromMoreInfo(ConstantsItemInspector.CANADA);
		if(reviewersEurope.contains(ConstantsItemInspector.ABHAI_SINGH)){
			resultEuropeOk = true;
		}
		if(reviewersCanada.contains(ConstantsItemInspector.ALBERTO_DECACERES)){
			resultCanadaOk = true;
		}
		
		assertTrue(resultEuropeOk);
		assertTrue(resultCanadaOk);
	}
	
	/**
	* @testLinkId T-GPRI-2685
	* @summary More than region
	*/
	@Test
	public void test_1423_MoreThanRegion() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultEuropeOk = false, resultCanadaOk = false, addSuccess;
		
		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.ITEM_REVIEWER);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsItemInspector.ALBERTO_DECACERES);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsItemInspector.ALBERTO_DECACERES);
		
		addSuccess = screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		if(!addSuccess){
			screenUsersManagement.removeRegionOnItemReviewer(ConstantsItemInspector.CANADA);
			screenUsersManagement.confirmRemoveRegion();
			screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		}
		addSuccess = screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.EUROPE);
		if(!addSuccess){
			screenUsersManagement.removeRegionOnItemReviewer(ConstantsItemInspector.EUROPE);
			screenUsersManagement.confirmRemoveRegion();
			screenUsersManagement.addRegionOnItemReviewer(ConstantsItemInspector.EUROPE);
		}
		addSuccess = screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		if(!addSuccess){
			screenUsersManagement.removeItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
			screenUsersManagement.confirmRemoveItem();
			screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		}
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.moreInfoRightClickItem(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		String reviewersEurope = screenPerformItemInspector.getReviewersFromMoreInfo(ConstantsItemInspector.EUROPE);
		String reviewersCanada = screenPerformItemInspector.getReviewersFromMoreInfo(ConstantsItemInspector.CANADA);
		if(reviewersEurope.contains(ConstantsItemInspector.ALBERTO_DECACERES)){
			resultEuropeOk = true;
		}
		if(reviewersCanada.contains(ConstantsItemInspector.ALBERTO_DECACERES)){
			resultCanadaOk = true;
		}
		
		assertTrue(resultEuropeOk);
		assertTrue(resultCanadaOk);
	}
	
	/**
	* @testLinkId T-GPRI-2686
	* @summary Remove the Item Reviewer of the region when the Item Has more than one Item Reviewer
	*/
	@Test
	public void test_1434_RemoveRegionItemMoreThanOneItemReviewer() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultEuropeOk = false, resultCanadaOk = false;
		
		test_1417_MoreThanOneItemReviewerForDifferentRegions();
		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.ITEM_REVIEWER);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsItemInspector.ALBERTO_DECACERES);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsItemInspector.ALBERTO_DECACERES);
		
		screenUsersManagement.removeRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		screenUsersManagement.confirmRemoveRegion();
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.moreInfoRightClickItem(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		String reviewersEurope = screenPerformItemInspector.getReviewersFromMoreInfo(ConstantsItemInspector.EUROPE);
		String reviewersCanada = screenPerformItemInspector.getReviewersFromMoreInfo(ConstantsItemInspector.CANADA);
		if(reviewersEurope.contains(ConstantsItemInspector.ABHAI_SINGH)){
			resultEuropeOk = true;
		}
		if(!reviewersCanada.contains(ConstantsItemInspector.ALBERTO_DECACERES)){
			resultCanadaOk = true;
		}
		
		assertTrue(resultEuropeOk);
		assertTrue(resultCanadaOk);
	}
	
	/**
	* @testLinkId T-GPRI-2687
	* @summary Remove the Item Reviewer of the region when the Item Has only one Item Reviewer
	*/
	@Test
	public void test_1438_RemoveRegionItemOnlyOneItemReviewer() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultCanadaOk = false;
		
		test_1377_AssociateUserWithOneRegion();
		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.ITEM_REVIEWER);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsItemInspector.ALBERTO_DECACERES);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsItemInspector.ALBERTO_DECACERES);
		
		screenUsersManagement.removeRegionOnItemReviewer(ConstantsItemInspector.CANADA);
		screenUsersManagement.confirmRemoveRegion();
		
		screenPerformItemInspector.openPage();
		screenPerformItemInspector.moreInfoRightClickItem(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		String reviewersCanada = screenPerformItemInspector.getReviewersFromMoreInfo(ConstantsItemInspector.CANADA);

		if(!reviewersCanada.contains(ConstantsItemInspector.ALBERTO_DECACERES)){
			resultCanadaOk = true;
		}
		
		assertTrue(resultCanadaOk);
	}
	
	/**
	* @testLinkId T-GPRI-2688
	* @summaryOnly item to Item Reviewer when the Item Has any Item Reviewer
	*/
	@Test
	public void test_1426_OnlyItemItemReviewerItemHasAnyItemReviewer() throws Exception {
		ScreenPerformItemInspector screenPerformItemInspector = new ScreenPerformItemInspector(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = false, addSuccess;

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.ITEM_REVIEWER);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsItemInspector.ALBERTO_DECACERES);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsItemInspector.ALBERTO_DECACERES, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsItemInspector.ALBERTO_DECACERES);
		
		screenUsersManagement.removeAllRegionsOnItemReviewer();
		
		addSuccess = screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		if(!addSuccess){
			screenUsersManagement.removeItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
			screenUsersManagement.confirmRemoveItem();
			screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		}

		screenPerformItemInspector.openPage();
		screenPerformItemInspector.moreInfoRightClickItem(ConstantsItemInspector.PATH_ITEM_LTE_MODE);
		String reviewers = screenPerformItemInspector.getReviewersFromMoreInfo("");
		if(!reviewers.contains(ConstantsItemInspector.ALBERTO_DECACERES)){
			resultOk = true;
		}

		assertTrue(resultOk);
	}
}