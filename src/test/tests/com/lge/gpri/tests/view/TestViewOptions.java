package com.lge.gpri.tests.view;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsView;
import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.view.ScreenPerformFollow;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Options in View
 * 
 * @author Gabriel Costa do Nascimento
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestViewOptions extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2617
	 * @summary Follow
	 */
	@Test
	public void test_1104_Follow() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformFollow screenPerformFollow = new ScreenPerformFollow(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch("abcd");
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnOptionsMenu();
		boolean success = screenPerformViewValues.selectOption("Follow Packages");
		if(!success){
			screenPerformViewValues.selectOption("Unfollow Packages");
			screenPerformViewValues.clickOnOptionsMenu();
			screenPerformViewValues.selectOption("Follow Packages");
		}
		sectionPerformMenu.clickOnExpandSideMenuButton();
		sectionPerformMenu.clickOnFollows();
		screenPerformFollow.clickOnPackagesTab();
		String nameItem = screenPerformFollow.getNameItemOnPackagesTab(ConstantsView.PACKAGE_ADS_16GB2);
		assertEquals(ConstantsView.PACKAGE_ADS_16GB2, nameItem);
	}
	
	/**
	 * @testLinkId T-GPRI-2618
	 * @summary Permanent Link
	 */
	@Test
	public void test_1105_PermanentLink() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch("abcd");
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnOptionsMenu();
		screenPerformViewValues.selectOption("Permanent Link");
		screenPerformViewValues.switchTabs(1);
		boolean isShow = screenPerformViewValues.packageItemsTableIsShowing();
		screenPerformViewValues.getDriver().close();
		screenPerformViewValues.switchTabs(0);
		assertTrue(isShow);
	}
	
	/**
	 * @testLinkId T-GPRI-2619
	 * @summary Revision Comments
	 */
	@Test
	public void test_1106_RevisionComments() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch("abcd");
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnOptionsMenu();
		screenPerformViewValues.selectOption("Revision Comments");
		boolean isShow = screenPerformViewValues.isRevisionCommentsVisible();
		
		assertTrue(isShow);
	}
	
	/**
	 * @testLinkId T-GPRI-2620
	 * @summary Download as spreadsheet
	 */
	@Test
	public void test_1107_DownloadAsSpreadsheet() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch("abcd");
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnOptionsMenu();
		screenPerformViewValues.selectOption("Download as Spreadsheet");
		screenPerformViewValues.clickOnExportButton();
		boolean success = screenPerformViewValues.waitExport();
		
		assertTrue(success);
	}
	
	/**
	 * @testLinkId T-GPRI-2621
	 * @summary Download as CSV
	 */
	@Test
	public void test_1108_DownloadAsCSV() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PACKAGE_NAME_PKG3_N_MS1);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnOptionsMenu();
		screenPerformViewValues.selectOption("Download as CSV");
		screenPerformViewValues.moveToPageBottom();
		screenPerformViewValues.clickOnExportButton();
		boolean success = screenPerformViewValues.waitExport();
		assertTrue(success);
	}
	
	/**
	 * @testLinkId T-GPRI-2622
	 * @summary Download change notes
	 */
	@Test
	public void test_1109_DownloadChangeNotes() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PACKAGE_NAME_PKG3_N_MS1);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnOptionsMenu();
		screenPerformViewValues.selectOption("Download Change Notes");
		boolean success = screenPerformViewValues.waitExport();
		assertTrue(success);
	}
	
	/**
	 * @testLinkId T-GPRI-2624
	 * @summary Model/Package Info
	 */
	@Test
	public void test_1111_ModelPackageInfo() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PACKAGE_NAME_PKG3_N_MS1);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnOptionsMenu();
		screenPerformViewValues.selectOption("Model/Package Info");
		boolean isShow = screenPerformViewValues.isModelPackageInfoVisible();
		
		assertTrue(isShow);
	}
	
	/**
	 * @testLinkId T-GPRI-2625
	 * @summary App Counter
	 */
	@Test
	public void test_1112_AppCounter() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PACKAGE_NAME_PKG3_N_MS1);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnOptionsMenu();
		screenPerformViewValues.selectOption("App Counter");
		boolean isShow = screenPerformViewValues.isAppCounterVisible();
		
		assertTrue(isShow);
	}
	
	/**
	 * @testLinkId T-GPRI-2626
	 * @summary Download as spreadsheet without item selected
	 */
	@Test
	public void test_1113_DownloadAsSpreadsheetWithoutItemSelected() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PACKAGE_NAME_PKG3_N_MS1);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnOptionsMenu();
		screenPerformViewValues.selectOption("Download as Spreadsheet");
		screenPerformViewValues.clickOnItemLibraryCheck();
		screenPerformViewValues.clickOnExportButton();
		assertEquals(ConstantsView.ERROR_MESSAGE_EXPORT_WITHOUT_ITEM, screenPerformViewValues.getErrorMsg(" Error"));
	}
	
	/**
	 * @testLinkId T-GPRI-2627
	 * @summary Download with success on search view
	 */
	@Test
	public void test_1114_DownloadWithSuccessOnSearchView() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PACKAGE_NAME_PKG3_N_MS1);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnOptionsMenu();
		screenPerformViewValues.selectOption("Download XML");
		screenPerformViewValues.moveToPageBottom();
		screenPerformViewValues.clickOnExportButton();
		boolean success = screenPerformViewValues.waitExport();
		
		assertTrue(success);
	}
}
