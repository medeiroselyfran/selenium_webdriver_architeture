package com.lge.gpri.tests.view;

import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.helpers.constants.ConstantsView;
import com.lge.gpri.screens.buyers.ScreenPerformBuyerApproval;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Values For Buyers View Confirmation.
*  
* @author Gabriel Costa do Nascimento
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestValuesForBuyersViewConfirmation extends AbstractTestScenary{
	
	/**
	* @testLinkId T-GPRI-2615
	* @summary Buyer Not Approved
	*/
	@Test
	public void test_1101_BuyerNotApproved() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.TLF_GROUP_EUROPE_6TL);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.moveToPageHeader();
		String txtApproved = screenPerformViewValues.getTextFrom("txtAboveTheLabelBuyer");
		boolean tableShow = screenPerformViewValues.packageItemsTableIsShowing();
		if(!txtApproved.contains("Not approved yet")){
			screenPerformView.newTab(ConstantsGeneric.LINK_SITE_BUYERS_PAGE);
			screenPerformView.switchTabs(1);
			screenPerformListBuyers.addSuffixFilter();
			screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_6TL);
			screenPerformListBuyers.clickOnEditProfile(ConstantsView.SUFFIX_6TL);
			screenPerformEditBuyerProfile.clickOnSelectItemsButton();
			screenPerformEditBuyerProfile.checkItemLibrary("chkBandBlockItem");
			screenPerformEditBuyerProfile.moveToPageBottom();
			screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
			String value = screenPerformEditBuyerProfile.getValue("fieldBandBlockOnEditProfilePackage");
			String newValue = "50";
			if(value.equals("50")){
				newValue = "51";
			}
			screenPerformEditBuyerProfile.setValue("fieldBandBlockOnEditProfilePackage", newValue);
			screenPerformEditBuyerProfile.clickOnItem("bandBlockNote");
			screenPerformEditBuyerProfile.setItemValueChangeNote("fieldNote", ConstantsView.MESSAGE_ONLY_FOR_TESTING);
			screenPerformEditBuyerProfile.clickOnSaveNote();
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsView.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.checkNormalChange();
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformListBuyers.getDriver().close();
			screenPerformListBuyers.switchTabs(0);
			
			screenPerformView.openPage();
			screenPerformView.addSearch(ConstantsView.TLF_GROUP_EUROPE_6TL);
			screenPerformView.selectFirstItemResultSearch();
			screenPerformViewValues.clickOnRevisionButton();
			screenPerformViewValues.clickOnLastRevisionDropdown();
			screenPerformViewValues.clickOnViewPackageButton();
			txtApproved = screenPerformViewValues.getTextFrom("txtAboveTheLabelBuyer");
			tableShow = screenPerformViewValues.packageItemsTableIsShowing();
		}
		
		assertTrue(txtApproved.contains("Not approved yet"));
		assertTrue(tableShow);
	}
	
	/**
	* @testLinkId T-GPRI-2616
	* @summary Buyer Approved
	*/
	@Test
	public void test_1103_BuyerApproved() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.TLF_GROUP_EUROPE_6TL);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.moveToPageHeader();
		String txtApproved = screenPerformViewValues.getTextFrom("txtAboveTheLabelBuyer");
		boolean tableShow = screenPerformViewValues.packageItemsTableIsShowing();
		if(txtApproved.contains("Not approved yet")){
			screenPerformView.newTab(ConstantsGeneric.LINK_SITE_BUYERS_PAGE);
			screenPerformView.switchTabs(1);
			screenPerformListBuyers.addSuffixFilter();
			screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_6TL);
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsView.SUFFIX_6TL);
			screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsView.SUFFIX_6TL);
			screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
			screenPerformBuyerApproval.clickOnApproveAll();
			screenPerformBuyerApproval.clickOnSaveButton();
			screenPerformBuyerApproval.clickOnConfirmSave();
			screenPerformBuyerApproval.waitLoadingOverlay();
			screenPerformListBuyers.getDriver().close();
			screenPerformListBuyers.switchTabs(0);
			
			screenPerformView.openPage();
			screenPerformView.addSearch(ConstantsView.TLF_GROUP_EUROPE_6TL);
			screenPerformView.selectFirstItemResultSearch();
			screenPerformViewValues.clickOnRevisionButton();
			screenPerformViewValues.clickOnLastRevisionDropdown();
			screenPerformViewValues.selectAllAutoProfiles();
			screenPerformViewValues.clickOnViewPackageButton();
			screenPerformViewValues.moveToPageHeader();
			txtApproved = screenPerformViewValues.getTextFrom("txtAboveTheLabelBuyer");
			tableShow = screenPerformViewValues.packageItemsTableIsShowing();
		}
		
		assertTrue(!txtApproved.contains("Not approved yet"));
		assertTrue(tableShow);
	}
}
