package com.lge.gpri.tests.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsCompare;
import com.lge.gpri.helpers.constants.ConstantsView;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Values For Packages Components.
*  
* @author Gabriel Costa do Nascimento 
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestValuesForPackagesComponents extends AbstractTestScenary{
	
	/**
	* @testLinkId T-GPRI-2601
	* @summary Change Package
	*/
	@Test
	public void test_1046_ChangePackage() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PKG_3M_VOLTE_VOWIFI_MS1_NTP_LGH815);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.setPackage(ConstantsView.PKG3_M_VOLTE_VOWIFI);
		screenPerformViewValues.clickOnViewPackageButton();
		
		assertTrue(screenPerformViewValues.packageItemsTableIsShowing());
	}
	
	/**
	* @testLinkId T-GPRI-2602
	* @summary Change Auto Profile Code
	*/
	@Test
	public void test_1047_ChangeAutoProfileCode() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PKG_3M_VOLTE_VOWIFI_MS1_NTP_LGH815);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.deselectAllAutoProfiles();
		boolean deselectAllResult = screenPerformViewValues.packageItemsTableIsShowing();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		boolean selectAllResult = screenPerformViewValues.packageItemsTableIsShowing();
		
		assertFalse(deselectAllResult);
		assertTrue(selectAllResult);
	}
	
	/**
	* @testLinkId T-GPRI-2603
	* @summary Search name of item
	*/
	@Test
	public void test_1048_SearchNameOfItem() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PKG_3M_VOLTE_VOWIFI_MS1_NTP_LGH815);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.setNameFilter(ConstantsView.HOME_NETWORK);
		String pathToItem = "Parameters::Network::Home Network::Home Network #1";
		String nameItem = screenPerformViewValues.getNameItemOnTreeTable(pathToItem);
		String valueItem = screenPerformViewValues.getValueItemOnTreeTable(pathToItem);
		
		assertEquals(ConstantsView.HOME_NETWORK_1,nameItem);
		assertEquals(ConstantsView.HOME_NETWORK_VALUE_716_17,valueItem);
	}
	
	/**
	* @testLinkId T-GPRI-2604
	* @summary Search value of item
	*/
	@Test
	public void test_1049_SearchValueOfItem() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PKG_3M_VOLTE_VOWIFI_MS1_NTP_LGH815);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.setValueFilter(ConstantsView.HOME_NETWORK_VALUE_716_17);
		String pathToItem = "Parameters::Network::Home Network::Home Network #1";
		String nameItem = screenPerformViewValues.getNameItemOnTreeTable(pathToItem);
		String valueItem = screenPerformViewValues.getValueItemOnTreeTable(pathToItem);
		
		assertEquals(ConstantsView.HOME_NETWORK_1,nameItem);
		assertEquals(ConstantsView.HOME_NETWORK_VALUE_716_17,valueItem);
	}
	
	/**
	* @testLinkId T-GPRI-2605
	* @summary Search with name valid and value invalid
	*/
	@Test
	public void test_1050_SearchWithNameValidAndValueInvalid() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PKG_3M_VOLTE_VOWIFI_MS1_NTP_LGH815);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.setNameFilter(ConstantsView.HOME_NETWORK);
		String pathToItem = "Parameters::Network::Home Network::Home Network #1";
		String nameItem = screenPerformViewValues.getNameItemOnTreeTable(pathToItem);
		String valueItem = screenPerformViewValues.getValueItemOnTreeTable(pathToItem);
		screenPerformViewValues.setValueFilter(ConstantsView.HOME_NETWORK_INVALID_VALUE_716_16);
		
		assertEquals(ConstantsView.HOME_NETWORK_1,nameItem);
		assertEquals(ConstantsView.HOME_NETWORK_VALUE_716_17,valueItem);
		assertFalse(screenPerformViewValues.packageItemsTableIsShowing());
	}
	
	/**
	* @testLinkId T-GPRI-2606
	* @summary Pagination
	*/
	@Test
	public void test_1051_Pagination() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PKG_3M_VOLTE_VOWIFI_MS1_NTP_LGH815);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnCollapseAllButton();
		boolean collapsedAll = screenPerformViewValues.isParametersCollapsed();
		screenPerformViewValues.clickOnExpandAllButton();
		boolean expandAll = !screenPerformViewValues.isParametersCollapsed();
		screenPerformViewValues.clickOnCenterValuesButton();
		Thread.sleep(1000);
		boolean isVisible = screenPerformViewValues.openViewBuyerChildrenIsVisible();
		
		assertTrue(collapsedAll);
		assertTrue(expandAll);
		assertFalse(isVisible);
	}
	
	/**
	* @testLinkId T-GPRI-2607
	* @summary Show only changed after creation
	*/
	@Test
	public void test_1052_ShowOnlyChangedAfterCreation() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.PKG_3M_VOLTE_VOWIFI_MS1_NTP_LGH815);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.setPackage(ConstantsView.PKG5_M_VOLTE_VOWIFI_MS1);
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.setEnableShowOnlyChangedAfterCreationButton();
		String pathToItem = "Parameters::Network::Band Block";
		String nameItem = screenPerformViewValues.getNameItemOnTreeTable(pathToItem);
		String valueItem = screenPerformViewValues.getValueItemOnTreeTable(pathToItem);
		
		assertEquals(ConstantsCompare.BAND_BLOCK,nameItem);
		assertEquals(ConstantsCompare.BAND_BLOCK_VALUE_50,valueItem);
	}
}
