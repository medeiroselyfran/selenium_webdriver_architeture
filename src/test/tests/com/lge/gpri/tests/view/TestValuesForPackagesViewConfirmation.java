package com.lge.gpri.tests.view;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsView;
import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Values For Packages View Confirmation.
*  
* @author Gabriel Costa do Nascimento
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestValuesForPackagesViewConfirmation extends AbstractTestScenary{
	
	
	/**
	* @testLinkId T-GPRI-2595
	* @summary All buyers of package not confirmed
	*/
	@Test
	public void test_1040_AllBuyersOfPackageNotConfirmed() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		HashMap<String,Boolean> listConfirmationBuyerChildren = null;
		boolean confirmationMotherBuyer=true;
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsView.LGH815);
		screenPerformListPackages.setBuyer(ConstantsView.TLF_GROUP_EUROPE_6TL);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String modelName = screenPerformListPackages.getModelName();
		String buyerName = screenPerformListPackages.getBuyerName().split("\\(")[1].split("\\)")[0];
		screenPerformListPackages.setBuyerUnconfirmed(ConstantsView.TLF_GROUP_EUROPE_6TL);
		screenPerformListPackages.setBuyerUnconfirmed(ConstantsView.O2_UNITED_KINGDOM_O2U);
		screenPerformListPackages.setBuyerUnconfirmed(ConstantsView.O2D_GERMANY_O2D);
		screenPerformListPackages.setBuyerUnconfirmed(ConstantsView.TELEFONICA_SPAIN_TLF);
	
		sectionPerformMenu.clickOnMenuView();
 		screenPerformView.addSearch(packageName + " " + buyerName + " " + modelName);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.openViewBuyerChildren();
		confirmationMotherBuyer = screenPerformViewValues.getConfirmationMotherBuyer();
		listConfirmationBuyerChildren = screenPerformViewValues.getConfirmationListBuyerChildren();
		
		assertEquals(false, confirmationMotherBuyer);
		assertEquals(false, listConfirmationBuyerChildren.get(ConstantsView.O2_UNITED_KINGDOM_O2U));
		assertEquals(false, listConfirmationBuyerChildren.get(ConstantsView.O2D_GERMANY_O2D));
		assertEquals(false, listConfirmationBuyerChildren.get(ConstantsView.TELEFONICA_SPAIN_TLF));
	}
	
	/**
	* @testLinkId T-GPRI-2596
	* @summary Only buyer mother confirmed
	*/
	@Test
	public void test_1041_OnlyBuyerMotherConfirmed() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		HashMap<String,Boolean> listConfirmationBuyerChildren = null;
		boolean confirmationMotherBuyer=true;
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsView.LGH815);
		screenPerformListPackages.setBuyer(ConstantsView.TLF_GROUP_EUROPE_6TL);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.TLF_GROUP_EUROPE_6TL);
		screenPerformListPackages.setBuyerUnconfirmed(ConstantsView.O2_UNITED_KINGDOM_O2U);
		screenPerformListPackages.setBuyerUnconfirmed(ConstantsView.O2D_GERMANY_O2D);
		screenPerformListPackages.setBuyerUnconfirmed(ConstantsView.TELEFONICA_SPAIN_TLF);
		
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(packageName);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.openViewBuyerChildren();
		confirmationMotherBuyer = screenPerformViewValues.getConfirmationMotherBuyer();
		listConfirmationBuyerChildren = screenPerformViewValues.getConfirmationListBuyerChildren();
		
		assertEquals(true, confirmationMotherBuyer);
		assertEquals(false, listConfirmationBuyerChildren.get(ConstantsView.O2_UNITED_KINGDOM_O2U));
		assertEquals(false, listConfirmationBuyerChildren.get(ConstantsView.O2D_GERMANY_O2D));
		assertEquals(false, listConfirmationBuyerChildren.get(ConstantsView.TELEFONICA_SPAIN_TLF));
	}
	
	/**
	* @testLinkId T-GPRI-2597
	* @summary Buyer and one child confirmed
	*/
	@Test
	public void test_1042_BuyerAndOneChildConfirmed() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		HashMap<String,Boolean> listConfirmationBuyerChildren = null;
		boolean confirmationMotherBuyer=true;
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsView.LGH815);
		screenPerformListPackages.setBuyer(ConstantsView.TLF_GROUP_EUROPE_6TL);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.TLF_GROUP_EUROPE_6TL);
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.O2_UNITED_KINGDOM_O2U);
		screenPerformListPackages.setBuyerUnconfirmed(ConstantsView.O2D_GERMANY_O2D);
		screenPerformListPackages.setBuyerUnconfirmed(ConstantsView.TELEFONICA_SPAIN_TLF);
		
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(packageName);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.openViewBuyerChildren();
		confirmationMotherBuyer = screenPerformViewValues.getConfirmationMotherBuyer();
		listConfirmationBuyerChildren = screenPerformViewValues.getConfirmationListBuyerChildren();
		
		assertEquals(true, confirmationMotherBuyer);
		assertEquals(true, listConfirmationBuyerChildren.get(ConstantsView.O2_UNITED_KINGDOM_O2U));
		assertEquals(false, listConfirmationBuyerChildren.get(ConstantsView.O2D_GERMANY_O2D));
		assertEquals(false, listConfirmationBuyerChildren.get(ConstantsView.TELEFONICA_SPAIN_TLF));
	}
	
	/**
	* @testLinkId T-GPRI-2598
	* @summary Buyer and two child confirmed
	*/
	@Test
	public void test_1043_BuyerAndTwoChildConfirmed() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		HashMap<String,Boolean> listConfirmationBuyerChildren = null;
		boolean confirmationMotherBuyer=true;
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsView.LGH815);
		screenPerformListPackages.setBuyer(ConstantsView.TLF_GROUP_EUROPE_6TL);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.TLF_GROUP_EUROPE_6TL);
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.O2_UNITED_KINGDOM_O2U);
		screenPerformListPackages.setBuyerUnconfirmed(ConstantsView.O2D_GERMANY_O2D);
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.TELEFONICA_SPAIN_TLF);
		
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(packageName);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.openViewBuyerChildren();
		confirmationMotherBuyer = screenPerformViewValues.getConfirmationMotherBuyer();
		listConfirmationBuyerChildren = screenPerformViewValues.getConfirmationListBuyerChildren();
		
		assertEquals(true, confirmationMotherBuyer);
		assertEquals(true, listConfirmationBuyerChildren.get(ConstantsView.O2_UNITED_KINGDOM_O2U));
		assertEquals(false, listConfirmationBuyerChildren.get(ConstantsView.O2D_GERMANY_O2D));
		assertEquals(true, listConfirmationBuyerChildren.get(ConstantsView.TELEFONICA_SPAIN_TLF));
	}
	
	/**
	* @testLinkId T-GPRI-2599
	* @summary All Buyers of package confirmed
	*/
	@Test
	public void test_1044_AllBuyersOfPackageConfirmed() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		HashMap<String,Boolean> listConfirmationBuyerChildren = null;
		boolean confirmationMotherBuyer=true;
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsView.LGH815);
		screenPerformListPackages.setBuyer(ConstantsView.TLF_GROUP_EUROPE_6TL);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.TLF_GROUP_EUROPE_6TL);
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.O2_UNITED_KINGDOM_O2U);
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.O2D_GERMANY_O2D);
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.TELEFONICA_SPAIN_TLF);
		
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(packageName);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.openViewBuyerChildren();
		confirmationMotherBuyer = screenPerformViewValues.getConfirmationMotherBuyer();
		listConfirmationBuyerChildren = screenPerformViewValues.getConfirmationListBuyerChildren();
		
		assertEquals(true, confirmationMotherBuyer);
		assertEquals(true, listConfirmationBuyerChildren.get(ConstantsView.O2_UNITED_KINGDOM_O2U));
		assertEquals(true, listConfirmationBuyerChildren.get(ConstantsView.O2D_GERMANY_O2D));
		assertEquals(true, listConfirmationBuyerChildren.get(ConstantsView.TELEFONICA_SPAIN_TLF));
	}
	
	/**
	* @testLinkId T-GPRI-2600
	* @summary Only mother not confirmed
	*/
	@Test
	public void test_1045_OnlyMotherNotConfirmed() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		HashMap<String,Boolean> listConfirmationBuyerChildren = null;
		boolean confirmationMotherBuyer=true;
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsView.LGH815);
		screenPerformListPackages.setBuyer(ConstantsView.TLF_GROUP_EUROPE_6TL);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		screenPerformListPackages.setBuyerUnconfirmed(ConstantsView.TLF_GROUP_EUROPE_6TL);
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.O2_UNITED_KINGDOM_O2U);
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.O2D_GERMANY_O2D);
		screenPerformListPackages.setBuyerConfirmed(ConstantsView.TELEFONICA_SPAIN_TLF);
		
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(packageName);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.openViewBuyerChildren();
		confirmationMotherBuyer = screenPerformViewValues.getConfirmationMotherBuyer();
		listConfirmationBuyerChildren = screenPerformViewValues.getConfirmationListBuyerChildren();
		
		assertEquals(false, confirmationMotherBuyer);
		assertEquals(true, listConfirmationBuyerChildren.get(ConstantsView.O2_UNITED_KINGDOM_O2U));
		assertEquals(true, listConfirmationBuyerChildren.get(ConstantsView.O2D_GERMANY_O2D));
		assertEquals(true, listConfirmationBuyerChildren.get(ConstantsView.TELEFONICA_SPAIN_TLF));
	}
}
