package com.lge.gpri.tests.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.helpers.constants.ConstantsView;
import com.lge.gpri.screens.buyers.ScreenPerformBuyerApproval;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Values For Buyers Components.
 * 
 * @author Gabriel Costa do Nascimento
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestValuesForBuyersComponents extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2608
	 * @summary Change Revision
	 */
	@Test
	public void test_1053_ChangeRevision() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.BUYER_ENTEL_PERU_NTP);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnRevisionDropdown("60");
		screenPerformViewValues.clickOnViewPackageButton();
		
		assertTrue(screenPerformViewValues.packageItemsTableIsShowing());
	}
	
	/**
	* @testLinkId T-GPRI-2609
	* @summary Change Auto Profile Code
	*/
	@Test
	public void test_1054_ChangeAutoProfileCode() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.BUYER_ENTEL_PERU_NTP);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.deselectAllAutoProfiles();
		boolean deselectAllResult = screenPerformViewValues.packageItemsTableIsShowing();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		boolean selectAllResult = screenPerformViewValues.packageItemsTableIsShowing();
		
		assertFalse(deselectAllResult);
		assertTrue(selectAllResult);
	}
	
	/**
	* @testLinkId T-GPRI-2610
	* @summary Search name of item
	*/
	@Test
	public void test_1055_SearchNameOfItem() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_NTP);
		screenPerformListBuyers.clickOnEditProfile(ConstantsView.SUFFIX_NTP);
		screenPerformEditBuyerProfile.clickOnSelectItemsButton();
		screenPerformEditBuyerProfile.addItemByPath("");
		screenPerformEditBuyerProfile.removeItemByPath("");
		screenPerformEditBuyerProfile.addItemByPath(ConstantsPackages.PATH_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsView.HOME_NETWORK_1, ConstantsView.HOME_NETWORK_VALUE_716_17);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		if(!screenPerformListBuyers.getStatusApproval(ConstantsView.SUFFIX_NTP).equals(ConstantsBuyers.VALIDATED)){
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsView.SUFFIX_NTP);
			screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsView.SUFFIX_NTP);
			screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
			screenPerformBuyerApproval.clickOnApproveAll();
			screenPerformBuyerApproval.moveToPageBottom();
			screenPerformBuyerApproval.clickOnSaveButton();
			screenPerformBuyerApproval.clickOnConfirmSave();
		}
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.BUYER_ENTEL_PERU_NTP);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.setNameFilter(ConstantsView.HOME_NETWORK);
		String pathToItem = "Parameters::Network::Home Network::Home Network #1";
		String nameItem = screenPerformViewValues.getNameItemOnTreeTable(pathToItem);
		String valueItem = screenPerformViewValues.getValueItemOnTreeTable(pathToItem);
		
		assertEquals(ConstantsView.HOME_NETWORK_1,nameItem);
		assertEquals(ConstantsView.HOME_NETWORK_VALUE_716_17,valueItem);
	}
	
	/**
	* @testLinkId T-GPRI-2611
	* @summary Search value of item
	*/
	@Test
	public void test_1056_SearchValueOfItem() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_NTP);
		screenPerformListBuyers.clickOnEditProfile(ConstantsView.SUFFIX_NTP);
		screenPerformEditBuyerProfile.clickOnSelectItemsButton();
		screenPerformEditBuyerProfile.addItemByPath("");
		screenPerformEditBuyerProfile.removeItemByPath("");
		screenPerformEditBuyerProfile.addItemByPath(ConstantsPackages.PATH_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsView.HOME_NETWORK_1, ConstantsView.HOME_NETWORK_VALUE_716_17);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		if(!screenPerformListBuyers.getStatusApproval(ConstantsView.SUFFIX_NTP).equals(ConstantsBuyers.VALIDATED)){
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsView.SUFFIX_NTP);
			screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsView.SUFFIX_NTP);
			screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
			screenPerformBuyerApproval.clickOnApproveAll();
			screenPerformBuyerApproval.moveToPageBottom();
			screenPerformBuyerApproval.clickOnSaveButton();
			screenPerformBuyerApproval.clickOnConfirmSave();
		}
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.BUYER_ENTEL_PERU_NTP);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.setValueFilter(ConstantsView.HOME_NETWORK_VALUE_716_17);
		String pathToItem = "Parameters::Network::Home Network::Home Network #1";
		String nameItem = screenPerformViewValues.getNameItemOnTreeTable(pathToItem);
		String valueItem = screenPerformViewValues.getValueItemOnTreeTable(pathToItem);
		
		assertEquals(ConstantsView.HOME_NETWORK_1,nameItem);
		assertEquals(ConstantsView.HOME_NETWORK_VALUE_716_17,valueItem);
	}
	
	/**
	* @testLinkId T-GPRI-2612
	* @summary Search with name valid and value invalid
	*/
	@Test
	public void test_1057_SearchWithNameValidAndValueInvalid() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_NTP);
		screenPerformListBuyers.clickOnEditProfile(ConstantsView.SUFFIX_NTP);
		screenPerformEditBuyerProfile.clickOnSelectItemsButton();
		screenPerformEditBuyerProfile.addItemByPath("");
		screenPerformEditBuyerProfile.removeItemByPath("");
		screenPerformEditBuyerProfile.addItemByPath(ConstantsPackages.PATH_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsView.HOME_NETWORK_1, ConstantsBuyers.VALUE_714_06);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		if(!screenPerformListBuyers.getStatusApproval(ConstantsView.SUFFIX_NTP).equals(ConstantsBuyers.VALIDATED)){
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsView.SUFFIX_NTP);
			screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsView.SUFFIX_NTP);
			screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
			screenPerformBuyerApproval.clickOnApproveAll();
			screenPerformBuyerApproval.moveToPageBottom();
			screenPerformBuyerApproval.clickOnSaveButton();
			screenPerformBuyerApproval.clickOnConfirmSave();
		}
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.BUYER_ENTEL_PERU_NTP);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.setNameFilter(ConstantsView.HOME_NETWORK);
		String pathToItem = "Parameters::Network::Home Network::Home Network #1";
		String nameItem = screenPerformViewValues.getNameItemOnTreeTable(pathToItem);
		String valueItem = screenPerformViewValues.getValueItemOnTreeTable(pathToItem);
		screenPerformViewValues.setValueFilter(ConstantsView.HOME_NETWORK_INVALID_VALUE_716_16);
		
		assertEquals(ConstantsView.HOME_NETWORK_1,nameItem);
		assertEquals(ConstantsBuyers.VALUE_714_06,valueItem);
		assertFalse(screenPerformViewValues.packageItemsTableIsShowing());
	}
	
	/**
	* @testLinkId T-GPRI-2613
	* @summary Pagination
	*/
	@Test
	public void test_1058_Pagination() throws Exception {
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.BUYER_ENTEL_PERU_NTP);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.clickOnCollapseAllButton();
		boolean collapsedAll = screenPerformViewValues.isParametersCollapsed();
		screenPerformViewValues.clickOnExpandAllButton();
		boolean expandAll = !screenPerformViewValues.isParametersCollapsed();
		screenPerformViewValues.clickOnCenterValuesButton();
		Thread.sleep(1000);
		boolean isVisible = screenPerformViewValues.openViewBuyerChildrenIsVisible();
		
		assertTrue(collapsedAll);
		assertTrue(expandAll);
		assertFalse(isVisible);
	}
}
