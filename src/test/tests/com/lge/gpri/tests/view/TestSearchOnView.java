package com.lge.gpri.tests.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.helpers.constants.ConstantsModels;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.helpers.constants.ConstantsView;
import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.buyers.AutoProfile;
import com.lge.gpri.screens.buyers.ScreenPerformBuyer;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.models.ScreenPerformListModel;
import com.lge.gpri.screens.models.ScreenPerformModel;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.packages.ScreenPerformNormalPackage;
import com.lge.gpri.screens.usermanagement.ScreenUsersManagement;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Search On
 * View.
 * 
 * @author Gabriel Costa do Nascimento
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSearchOnView extends AbstractTestScenary {
	
	
	/**
	 * @testLinkId T-GPRI-2566
	 * @summary Only Model
	 */
	@Test
	public void test_980_OnlyModel() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.MODEL_16GB2);
		String packageNameCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Name");
		String packageStatusCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Status");
		String packageBuyerNameCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Buyer");

		sectionPerformMenu.clickOnMenuPackage();
		screenPerformListPackages.setModel(ConstantsView.MODEL_16GB2);
		screenPerformListPackages.setBuyer(packageBuyerNameCard);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String packageStatus = screenPerformListPackages.getStatusPackageConfirmation();

		assertEquals(packageName, packageNameCard);
		if (packageStatusCard.equals(ConstantsView.NOT_CONFIRMED)) {
			assertFalse(packageStatus.contains(ConstantsView.CONFIRMED));
		} else {
			assertTrue(packageStatus.contains(ConstantsView.CONFIRMED));
		}
	}

	/**
	 * @testLinkId T-GPRI-2567
	 * @summary Only Buyer
	 */
	@Test
	public void test_981_OnlyBuyer() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		String name = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Name");
		String packageStatusCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Status");
		
		sectionPerformMenu.clickOnMenuPackage();
		screenPerformListPackages.setModel(ConstantsView.MODEL_16GB2);
		screenPerformListPackages.setBuyer(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String packageStatus = screenPerformListPackages.getStatusPackageConfirmation();

		sectionPerformMenu.clickOnMenuBuyer();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_ADS);
		String regionName = screenPerformListBuyers.getRegionConfirmation();
		String buyerName = screenPerformListBuyers.getBuyerConfirmation();

		assertEquals(name, packageName);
		assertEquals(ConstantsView.ADVINNE_SOUTH_AFRICA, buyerName);
		assertEquals(ConstantsView.ASIA_MIDDLE_EAST_AFRICA, regionName);
		if (packageStatusCard.equals(ConstantsView.NOT_CONFIRMED)) {
			assertFalse(packageStatus.contains(ConstantsView.CONFIRMED));
		} else {
			assertTrue(packageStatus.contains(ConstantsView.CONFIRMED));
		}
	}

	/**
	 * @testLinkId T-GPRI-2568
	 * @summary Only Country
	 */
	@Test
	public void test_984_OnlyCountry() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.SOUTH_AFRICA);
		screenPerformView.addSearch(ConstantsView.MODEL_16GB2);
		String name = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Name");
		String packageStatusCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Status");

		sectionPerformMenu.clickOnMenuPackage();
		screenPerformListPackages.setModel(ConstantsView.MODEL_16GB2);
		screenPerformListPackages.setBuyer(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String packageStatus = screenPerformListPackages.getStatusPackageConfirmation();

		sectionPerformMenu.clickOnMenuBuyer();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_ADS);
		String regionName = screenPerformListBuyers.getRegionConfirmation();
		String buyerName = screenPerformListBuyers.getBuyerConfirmation();

		assertEquals(name, packageName);
		assertEquals(ConstantsView.ADVINNE_SOUTH_AFRICA, buyerName);
		assertEquals(ConstantsView.ASIA_MIDDLE_EAST_AFRICA, regionName);
		if (packageStatusCard.equals(ConstantsView.NOT_CONFIRMED)) {
			assertFalse(packageStatus.contains(ConstantsView.CONFIRMED));
		} else {
			assertTrue(packageStatus.contains(ConstantsView.CONFIRMED));
		}
	}

	/**
	 * @testLinkId T-GPRI-2569
	 * @summary Only Operator
	 */
	@Test
	public void test_986_OnlyOperator() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.ADVINNE);
		String name = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Name");
		String packageStatusCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Status");

		sectionPerformMenu.clickOnMenuPackage();
		screenPerformListPackages.setModel(ConstantsView.MODEL_16GB2);
		screenPerformListPackages.setBuyer(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String packageStatus = screenPerformListPackages.getStatusPackageConfirmation();

		sectionPerformMenu.clickOnMenuBuyer();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_ADS);
		String regionName = screenPerformListBuyers.getRegionConfirmation();
		String buyerName = screenPerformListBuyers.getBuyerConfirmation();

		assertEquals(name, packageName);
		assertEquals(ConstantsView.ADVINNE_SOUTH_AFRICA, buyerName);
		assertEquals(ConstantsView.ASIA_MIDDLE_EAST_AFRICA, regionName);
		if (packageStatusCard.equals(ConstantsView.NOT_CONFIRMED)) {
			assertFalse(packageStatus.contains(ConstantsView.CONFIRMED));
		} else {
			assertTrue(packageStatus.contains(ConstantsView.CONFIRMED));
		}
	}

	/**
	 * @testLinkId T-GPRI-2570
	 * @summary Only Suffix
	 */
	@Test
	public void test_989_OnlySuffix() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.SUFFIX_ADS);
		String packageNameCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Name");
		String packageStatusCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Status");

		sectionPerformMenu.clickOnMenuPackage();
		screenPerformListPackages.setModel(ConstantsView.MODEL_16GB2);
		screenPerformListPackages.setBuyer(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String packageStatus = screenPerformListPackages.getStatusPackageConfirmation();

		sectionPerformMenu.clickOnMenuBuyer();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_ADS);
		String regionName = screenPerformListBuyers.getRegionConfirmation();
		String buyerName = screenPerformListBuyers.getBuyerConfirmation();

		assertEquals(packageNameCard, packageName);
		assertEquals(ConstantsView.ADVINNE_SOUTH_AFRICA, buyerName);
		assertEquals(ConstantsView.ASIA_MIDDLE_EAST_AFRICA, regionName);
		if (packageStatusCard.equals(ConstantsView.NOT_CONFIRMED)) {
			assertFalse(packageStatus.contains(ConstantsView.CONFIRMED));
		} else {
			assertTrue(packageStatus.contains(ConstantsView.CONFIRMED));
		}
	}

	/**
	 * @testLinkId T-GPRI-2571
	 * @summary Only Package
	 */
	@Test
	public void test_991_OnlyPackage() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch("abcd");
		String packageNameCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Name");
		String packageStatusCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Status");

		sectionPerformMenu.clickOnMenuPackage();
		screenPerformListPackages.setModel(ConstantsView.MODEL_16GB2);
		screenPerformListPackages.setBuyer(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage("abcd");
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String packageStatus = screenPerformListPackages.getStatusPackageConfirmation();

		assertEquals(packageName, packageNameCard);
		assertEquals("abcd", packageName);
		if (packageStatusCard.equals(ConstantsView.NOT_CONFIRMED)) {
			assertFalse(packageStatus.contains(ConstantsView.CONFIRMED));
		} else {
			assertTrue(packageStatus.contains(ConstantsView.CONFIRMED));
		}
	}

	/**
	 * @testLinkId T-GPRI-2572
	 * @summary Search without returning results
	 */
	@Test
	public void test_993_SearchWithoutReturningResults() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.SEARCH_INVALID_BOI_BRASIL);

		assertFalse(screenPerformView.selectFirstItemResultSearch());
	}

	/**
	 * @testLinkId T-GPRI-2573
	 * @summary Search empty
	 */
	@Test
	public void test_995_SearchEmpty() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch("");

		assertFalse(screenPerformView.selectFirstItemResultSearch());
	}

	/**
	 * @testLinkId T-GPRI-2574
	 * @summary Only Model and Buyer
	 */
	@Test
	public void test_997_OnlyModelOnlyAndBuyer() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.MODEL_16GB2);
		screenPerformView.addSearch(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		String packageNameCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Name");
		String packageStatusCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Status");

		sectionPerformMenu.clickOnMenuPackage();
		screenPerformListPackages.setModel(ConstantsView.MODEL_16GB2);
		screenPerformListPackages.setBuyer(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String packageStatus = screenPerformListPackages.getStatusPackageConfirmation();

		assertEquals(packageName, packageNameCard);
		if (packageStatusCard.equals(ConstantsView.NOT_CONFIRMED)) {
			assertFalse(packageStatus.contains(ConstantsView.CONFIRMED));
		} else {
			assertTrue(packageStatus.contains(ConstantsView.CONFIRMED));
		}
	}

	/**
	 * @testLinkId T-GPRI-2575
	 * @summary Only Country and Operator
	 */
	@Test
	public void test_1000_OnlyCountryAndOperator() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.SOUTH_AFRICA);
		screenPerformView.addSearch(ConstantsView.ADVINNE);
		String packageNameCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Name");
		String packageStatusCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Status");
		
		sectionPerformMenu.clickOnMenuPackage();
		screenPerformListPackages.setModel(ConstantsView.MODEL_16GB2);
		screenPerformListPackages.setBuyer(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String packageStatus = screenPerformListPackages.getStatusPackageConfirmation();

		sectionPerformMenu.clickOnMenuBuyer();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_ADS);
		String regionName = screenPerformListBuyers.getRegionConfirmation();
		String buyerName = screenPerformListBuyers.getBuyerConfirmation();

		assertEquals(packageNameCard, packageName);
		assertEquals(ConstantsView.ADVINNE_SOUTH_AFRICA, buyerName);
		assertEquals(ConstantsView.ASIA_MIDDLE_EAST_AFRICA, regionName);
		if (packageStatusCard.equals(ConstantsView.NOT_CONFIRMED)) {
			assertFalse(packageStatus.contains(ConstantsView.CONFIRMED));
		} else {
			assertTrue(packageStatus.contains(ConstantsView.CONFIRMED));
		}
	}

	/**
	 * @testLinkId T-GPRI-2576
	 * @summary Only Suffix and Package
	 */
	@Test
	public void test_1002_OnlySuffixAndPackage() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.SUFFIX_ADS);
		screenPerformView.addSearch("abcd");
		String packageNameCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Name");
		String packageStatusCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Status");

		sectionPerformMenu.clickOnMenuPackage();
		screenPerformListPackages.setModel(ConstantsView.MODEL_16GB2);
		screenPerformListPackages.setBuyer(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String packageStatus = screenPerformListPackages.getStatusPackageConfirmation();

		assertEquals(packageName, packageNameCard);
		if (packageStatusCard.equals(ConstantsView.NOT_CONFIRMED)) {
			assertFalse(packageStatus.contains(ConstantsView.CONFIRMED));
		} else {
			assertTrue(packageStatus.contains(ConstantsView.CONFIRMED));
		}
	}

	/**
	 * @testLinkId T-GPRI-2577
	 * @summary Only Model and Country
	 */
	@Test
	public void test_1003_OnlyModelAndCountry() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.MODEL_16GB2);
		screenPerformView.addSearch(ConstantsView.SOUTH_AFRICA);
		String packageNameCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Name");
		String packageStatusCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Status");

		sectionPerformMenu.clickOnMenuPackage();
		screenPerformListPackages.setModel(ConstantsView.MODEL_16GB2);
		screenPerformListPackages.setBuyer(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String packageStatus = screenPerformListPackages.getStatusPackageConfirmation();

		assertEquals(packageName, packageNameCard);
		if (packageStatusCard.equals(ConstantsView.NOT_CONFIRMED)) {
			assertFalse(packageStatus.contains(ConstantsView.CONFIRMED));
		} else {
			assertTrue(packageStatus.contains(ConstantsView.CONFIRMED));
		}
	}

	/**
	 * @testLinkId T-GPRI-2578
	 * @summary Only Model, Suffix and Package
	 */
	@Test
	public void test_1005_OnlyModelSuffixAndPackage() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.MODEL_16GB2);
		screenPerformView.addSearch(ConstantsView.SUFFIX_ADS);
		screenPerformView.addSearch("abcd");
		String packageNameCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Name");
		String packageStatusCard = screenPerformView.getValuePerFieldOfCardBasedOnModel(ConstantsView.MODEL_16GB2, "Status");

		sectionPerformMenu.clickOnMenuPackage();
		screenPerformListPackages.setModel(ConstantsView.MODEL_16GB2);
		screenPerformListPackages.setBuyer(ConstantsView.ADVINNE_SOUTH_AFRICA_ADS);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage("abcd");
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		String packageStatus = screenPerformListPackages.getStatusPackageConfirmation();

		assertEquals(packageName, packageNameCard);
		assertEquals("abcd", packageName);
		if (packageStatusCard.equals(ConstantsView.NOT_CONFIRMED)) {
			assertFalse(packageStatus.contains(ConstantsView.CONFIRMED));
		} else {
			assertTrue(packageStatus.contains(ConstantsView.CONFIRMED));
		}

	}

	/**
	 * @testLinkId T-GPRI-2579
	 * @summary Create buyer and search. The creation choose the first buyer of
	 *          list always
	 */
	@Test
	public void test_1010_CreateBuyerAndSearch() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.newTab(ConstantsGeneric.LINK_SITE_VIEW_PAGE);
		screenPerformListBuyers.switchTabs(0);
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile("MCC/MNC", "730 30"));
		screenPerformBuyer.fillFieldsChildBuyer("FIRST", "", autoProfilesSaved,
				Arrays.asList("ajay.cheeckoli"), Arrays.asList(""));
		String buyer = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix = buyer.split("\\(")[1].replace(")", "");
		String region = screenPerformBuyer.getRegionFirstDropdownBuyer();
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsView.ERROR);
		if (msg.equals(ConstantsView.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer = oldNickNameOperator + " " + suffix + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffix);
		String regionConfirmation = screenPerformListBuyers.getRegionConfirmation();
		screenPerformListBuyers.getDriver().close();

		screenPerformView.switchTabs(0);
		screenPerformView.addSearch(buyer);
		String buyerName = screenPerformView.getFieldValueCard("Name");
		String regionName = screenPerformView.getFieldValueCard("Region");
		String buyerWithoutSuffix = buyer.split("\\(")[0];

		assertEquals(region, regionConfirmation);
		assertEquals(buyerWithoutSuffix.replace(" ", ""), buyerName.replace(" ", ""));
		assertEquals(region, regionName);
	}

	/**
	 * @testLinkId T-GPRI-2580
	 * @summary Remove Buyer and search.
	 */
	@Test
	public void test_1021_RemoveBuyerAndSearch() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.newTab(ConstantsGeneric.LINK_SITE_VIEW_PAGE);
		screenPerformListBuyers.switchTabs(0);
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_IDT);
		boolean removed = screenPerformListBuyers.clickOnRemoveBuyer(ConstantsView.SUFFIX_IDT);
		screenPerformListBuyers.confirmRemoveBuyer();
		if (!removed) {
			screenPerformListBuyers.clickOnShowDeletedBuyersButton();
			screenPerformListBuyers.clickOnRestoreBuyer(ConstantsView.SUFFIX_IDT);
			screenPerformListBuyers.confirmRestoreBuyer();
			screenPerformListBuyers.clickOnRemoveBuyer(ConstantsView.SUFFIX_IDT);
			screenPerformListBuyers.confirmRemoveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_IDT);
		screenPerformListBuyers.getDriver().close();

		screenPerformView.switchTabs(0);
		screenPerformView.addSearch(ConstantsView.TATA_INDIA);

		assertFalse(screenPerformView.selectFirstItemResultSearch());
	}

	/**
	 * @testLinkId T-GPRI-2581
	 * @summary Create package and search.
	 */
	@Test
	public void test_1022_CreatePackageAndSearch() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.newTab(ConstantsGeneric.LINK_SITE_VIEW_PAGE);
		screenPerformListPackages.switchTabs(0);
		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", "FIRST", "PackageTest",
				ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "2",
				ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
				ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2,
				ConstantsPackages.VOICE_OVER_NONE);
		String buyer = screenPerformNormalPackage.getBuyerFirstDropdownBuyer();
		String suffix = buyer.split("\\(")[1].replace(")", "");
		screenPerformNormalPackage.setPackageName("Test " + suffix);
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		String txtPackageCreateSuccess = screenPerformNormalPackage.getTextFrom("txtPackageCreatedSuccess");
		screenPerformNormalPackage.clickOnClosePopup();
		screenPerformListPackages.getDriver().close();

		screenPerformView.switchTabs(0);
		screenPerformView.addSearch("Test " + suffix);
		String buyerName = screenPerformView.getFieldValueCard("Buyer");
		String model = screenPerformView.getFieldValueCard("Model");
		String name = screenPerformView.getFieldValueCard("Name");
		
		String buyerWithoutSuffix = buyer.split("\\(")[0].substring(0, buyer.split("\\(")[0].length() - 1);

		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
		assertEquals(buyerWithoutSuffix, buyerName);
		assertEquals(ConstantsPackages.LGH815, model);
		assertEquals(name, "Test " + suffix);
	}

	/**
	 * @testLinkId T-GPRI-2582
	 * @summary Remove Package and search.
	 */
	// BLOCKED
	@Test
	public void test_1025_RemovePackageAndSearch() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.newTab(ConstantsGeneric.LINK_SITE_VIEW_PAGE);
		screenPerformListPackages.switchTabs(0);
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsView.OI_BRAZIL_BOI);
		String testName = screenPerformListPackages.getNamePackageConfirmation();
		boolean removed = screenPerformListPackages.clickOnRemovePackageButton();
		screenPerformListPackages.confirmRemovePackage();
		if (!removed) {
			screenPerformListPackages.clickOnCreatePackageButton();
			testName = "Test Remove and Search";
			screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.FIRST,
					testName, ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
					ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
			screenPerformNormalPackage.clickOnSaveCreatedButton();
			screenPerformNormalPackage.clickOnOkPackageSuccessCreated();

			screenPerformListPackages.clickOnRemovePackageButton();
			screenPerformListPackages.confirmRemovePackage();
		}
		screenPerformListPackages.getDriver().close();

		screenPerformView.switchTabs(0);
		String[] result = screenPerformView.addSearch(testName);

		assertEquals(0, result.length);
	}

	/**
	 * @testLinkId T-GPRI-2583
	 * @summary Create Model Create Package and search.
	 */
	@Test
	public void test_1027_CreateModelCreatePackageAndSearch() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.newTab(ConstantsGeneric.LINK_SITE_VIEW_PAGE);
		screenPerformListModel.switchTabs(0);
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT, Arrays.asList(ConstantsModels.SWPL_HUGO_KANG),
				Arrays.asList());
		String model = screenPerformModel.getModelFirst();
		screenPerformModel.clickOnSaveModel();
		String txtModelCreateSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.fillFieldsPackage(model, "", ConstantsModels.FIRST, "", ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1", ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.ANDROID_VERSION_5, ConstantsPackages.GRID_ROW_COLUMN_5X5,
				ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
		String buyer = screenPerformNormalPackage.getBuyerFirstDropdownBuyer();
		String suffix = buyer.split("\\(")[1].replace(")", "");
		screenPerformNormalPackage.setPackageName("Test " + suffix);
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		String txtPackageCreateSuccess = screenPerformNormalPackage.getTextFrom("txtPackageCreatedSuccess");
		screenPerformNormalPackage.clickOnClosePopup();
		screenPerformListPackages.getDriver().close();

		screenPerformView.switchTabs(0);
		screenPerformView.addSearch("Test " + suffix);
		String buyerName = screenPerformView.getFieldValueCard("Buyer");
		String modelName = screenPerformView.getFieldValueCard("Model");
		String name = screenPerformView.getFieldValueCard("Name");
		String buyerWithoutSuffix = buyer.split("\\(")[0].substring(0, buyer.split("\\(")[0].length() - 1);

		assertEquals(ConstantsModels.MODEL_CREATED_SUCCESS, txtModelCreateSuccess);
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
		assertEquals(buyerWithoutSuffix, buyerName);
		assertEquals(model, modelName);
		assertEquals(name, "Test " + suffix);
	}

	/**
	 * @testLinkId T-GPRI-2584
	 * @summary Remove Model and search package of model removed
	 */
	@Test
	public void test_1028_RemoveModelAndSearchPackageOfModelRemoved() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.newTab(ConstantsGeneric.LINK_SITE_VIEW_PAGE);
		screenPerformListModel.switchTabs(0);
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsView.LGX240YK);
		boolean removed = screenPerformListModel.clickOnRemoveModel(ConstantsView.LGX240YK);
		screenPerformListModel.confirmRemoveModel();
		if (!removed) {
			screenPerformListModel.clickOnShowDeletedModelsButton();
			screenPerformListModel.clickOnRestoreModel(ConstantsView.LGX240YK);
			screenPerformListModel.confirmRestoreModel();
			screenPerformListModel.clickOnRemoveModel(ConstantsView.LGX240YK);
			screenPerformListModel.confirmRemoveModel();
		}
		screenPerformListModel.getDriver().close();

		screenPerformView.switchTabs(0);
		screenPerformView.addSearch(ConstantsView.PACKAGE_LGX240YK);

		assertFalse(screenPerformView.selectFirstItemResultSearch());
	}

	/**
	 * @testLinkId T-GPRI-2585
	 * @summary Enable Hide on View and Search Buyer
	 */
	@Test
	public void test_1029_EnableHideOnViewAndSearchBuyer() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		boolean sucessfullSaving;
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_ADS);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsView.SUFFIX_ADS);
		screenPerformBuyer.uncheckMVNO();
		screenPerformBuyer.checkOnHideOnView();
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(ConstantsView.SUFFIX_ADS);
		boolean result = screenPerformView.selectFirstItemResultSearch();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnShowHiddenBuyersButton();
		screenPerformListBuyers.clickOnEditBuyer(ConstantsView.SUFFIX_ADS);
		screenPerformBuyer.uncheckMVNO();
		screenPerformBuyer.uncheckOnHideOnView();
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);

		assertFalse(result);
	}

	/**
	 * @testLinkId T-GPRI-2586
	 * @summary Disable Hide on View and Search Buyer
	 */
	@Test
	public void test_1030_DisableHideOnViewAndSearchBuyer() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		boolean sucessfullSaving;
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsView.SUFFIX_ADS);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsView.SUFFIX_ADS);
		screenPerformBuyer.uncheckMVNO();
		screenPerformBuyer.uncheckOnHideOnView();
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(ConstantsView.SUFFIX_ADS);
		String regionName = screenPerformView.getFieldValueCard("Region");

		assertEquals(ConstantsView.ASIA_MIDDLE_EAST_AFRICA, regionName);
	}

	/**
	 * @testLinkId T-GPRI-2587
	 * @summary Search Model associated of the SWPL logged
	 */
	@Test
	public void test_1032_SearchModelAssociatedOfTheSWPLLogged() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		boolean resultUser;
		String resultSearch = "";

		screenUsersManagement.openPage();
		resultUser = screenUsersManagement.searchAndSelectUser(ConstantsView.CHANGHWAN_LEE);
		if (resultUser) {
			screenUsersManagement.clicOnLoginAs();
			screenPerformView.openPage();
			resultSearch = screenPerformView.addSearch(ConstantsView.LGH220)[0];
		}

		assertTrue(resultUser);
		assertEquals(ConstantsView.LGH220, resultSearch);
	}

	/**
	 * @testLinkId T-GPRI-2588
	 * @summary Search Model associated of the SWMM logged
	 */
	@Test
	public void test_1033_SearchModelAssociatedOfTheSWMMLogged() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		boolean resultUser;
		String resultSearch = "";

		screenUsersManagement.openPage();
		resultUser = screenUsersManagement.searchAndSelectUser(ConstantsView.ALVIN_SHIN);
		if (resultUser) {
			screenUsersManagement.clicOnLoginAs();
			screenPerformView.openPage();
			resultSearch = screenPerformView.addSearch(ConstantsView.LGH220)[0];
		}

		assertTrue(resultUser);
		assertEquals(ConstantsView.LGH220, resultSearch);
	}

	/**
	 * @testLinkId T-GPRI-2589
	 * @summary Search Buyer associated of the LTM logged
	 */
	@Test
	public void test_1034_SearchBuyerAssociatedOfTheLTMogged() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		boolean resultUser;
		String resultSearch = "";

		screenUsersManagement.openPage();
		resultUser = screenUsersManagement.searchAndSelectUser(ConstantsView.ALEX_RECHTMAN);
		if (resultUser) {
			screenUsersManagement.clicOnLoginAs();
			screenPerformView.openPage();
			resultSearch = screenPerformView.addSearch(ConstantsView.SEARCH_ZEALAND)[0];
		}
		assertEquals(ConstantsView.RESULT_NEW_ZEALAND, resultSearch);
	}

	/**
	 * @testLinkId T-GPRI-2590
	 * @summary Search Buyer associated of the GTAM logged
	 */
	@Test
	public void test_1035_SearchBuyerAssociatedOfTheGTAMogged() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		boolean resultUser;
		String resultSearch = "";

		screenUsersManagement.openPage();
		resultUser = screenUsersManagement.searchAndSelectUser(ConstantsView.KJ_SHIN);
		if (resultUser) {
			screenUsersManagement.clicOnLoginAs();
			screenPerformView.openPage();
			resultSearch = screenPerformView.addSearch(ConstantsView.SEARCH_ZEALAND)[0];
		}
		assertEquals(ConstantsView.RESULT_NEW_ZEALAND, resultSearch);
	}

	/**
	 * @testLinkId T-GPRI-2591
	 * @summary Search Buyer associated of the GTM logged
	 */
	@Test
	public void test_1036_SearchBuyerAssociatedOfTheGTMogged() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		boolean resultUser;
		String resultSearch = "";

		screenUsersManagement.openPage();
		resultUser = screenUsersManagement.searchAndSelectUser(ConstantsView.DONATO_ANTONUCCI);
		if (resultUser) {
			screenUsersManagement.clicOnLoginAs();
			screenPerformView.openPage();
			resultSearch = screenPerformView.addSearch(ConstantsView.SEARCH_BYT)[0];
		}
		assertEquals(ConstantsView.RESULT_BOUYGUES_FRANCE_BYT, resultSearch);
	}

	/**
	 * @testLinkId T-GPRI-2592
	 * @summary Clear Search
	 */
	@Test
	public void test_1037_ClearSearch() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsView.OI_BRAZIL_BOI);
		screenPerformView.removeSearch(ConstantsView.OI_BRAZIL_BOI);

		assertTrue(screenPerformView.searchIsClean());
	}

	/**
	 * @testLinkId T-GPRI-2593
	 * @summary Return of the searched list by model with only a model
	 */
	@Test
	public void test_1038_ReturnOfTheSearchedListByModelWithOnlyAModel() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());

		screenPerformView.openPage();
		String[] results = screenPerformView.addSearch("815");

		for (int i = 0; i < results.length; i++) {
			assertTrue(results[i].contains("815"));
		}
	}

	/**
	 * @testLinkId T-GPRI-2594
	 * @summary Return of the searched list by model with two models
	 */
	@Test
	public void test_1039_ReturnOfTheSearchedListByModelWithTwoModels() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());

		screenPerformView.openPage();
		String[] results1 = screenPerformView.addSearch("815");
		String[] results2 = screenPerformView.addSearch("722");

		for (int i = 0; i < results1.length; i++) {
			assertTrue(results1[i].contains("815"));
		}
		for (int i = 0; i < results2.length; i++) {
			assertTrue(results2[i].contains("722"));
		}
	}
}
