package com.lge.gpri.tests.compare;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsCompare;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.compare.ScreenPerformCompare;
import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Compare.
*  
* @author Gabriel Costa do Nascimento
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCompare extends AbstractTestScenary{
	
	/**
	* @testLinkId T-GPRI-2628
	* @summary Compare Two normal packages
	*/
	@Test
	public void test_1023_TwoNormalPackages() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList( ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList( ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		String valueOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		
		assertEquals(ConstantsCompare.BAND_BLOCK_VALUE_50,valueOneItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_32,valueTwoItem);
	}
	
	/**
	* @testLinkId T-GPRI-2629
	* @summary Compare Three normal packages
	*/
	@Test
	public void test_1060_ThreeNormalPackages() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, true);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.DJUICE_UKRAINE_DJU, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_35, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriThree(ConstantsCompare.DJUICE_UKRAINE_DJU, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		String valueOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		String valueThreeItem = screenPerformCompare.getValuePriThreeOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		
		assertEquals(ConstantsCompare.BAND_BLOCK_VALUE_50,valueOneItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_32,valueTwoItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_35,valueThreeItem);
	}
	
	/**
	* @testLinkId T-GPRI-2630
	* @summary Compare Two package without items
	*/
	@Test
	public void test_1061_TwoPackageWithoutItems() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());

		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.ALL, ConstantsCompare.EMPTY, ConstantsCompare.EMPTY , false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.ALL, ConstantsCompare.EMPTY, ConstantsCompare.EMPTY, false);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();

		assertTrue(screenPerformCompare.isResultParametersDifEmpty());
	}
	
	/**
	* @testLinkId T-GPRI-2631
	* @summary Verify component ">" to Copy Parameters of PRI One for PRI Two
	*/
	@Test
	public void test_1062_VerifyComponentCopyParametersOfPRIOneForPRITwo() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.copyParametersOneTwo();
		screenPerformCompare.clickOnCompare();
		String valueOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		
		assertEquals(ConstantsCompare.BAND_BLOCK_VALUE_50,valueOneItem);
		assertEquals(ConstantsCompare.BAND_BLOCK_VALUE_50,valueTwoItem);
	}
	
	/**
	* @testLinkId T-GPRI-2632
	* @summary Verify component ">" to Copy Parameters of PRI Two for PRI Three
	*/
	@Test
	public void test_1063_VerifyComponentCopyParametersOfPRITwoForPRIThree() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.copyParametersTwoThree();
		screenPerformCompare.clickOnCompare();
		String valueOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		String valueThreeItem = screenPerformCompare.getValuePriThreeOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		
		assertEquals(ConstantsCompare.BAND_BLOCK_VALUE_50,valueOneItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_32,valueTwoItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_32,valueThreeItem);
	}
	
	/**
	* @testLinkId T-GPRI-2633
	* @summary Verify component "<" to Copy Parameters of PRI Three for PRI Two
	*/
	@Test
	public void test_1064_VerifyComponentCopyParametersOfPRIThreeForPRITwo() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.MODEL_16GB2, ConstantsCompare.ADVINNE_SOUTH_AFRICA_ADS, ConstantsCompare.NAME_0, ConstantsCompare.NAME_ADVINNE, ConstantsCompare.PATH_ITEM_NAME_0, false);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriThree(ConstantsCompare.ADVINNE_SOUTH_AFRICA_ADS, ConstantsCompare.MODEL_16GB2, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.copyParametersThreeTwo();
		boolean resultOk = screenPerformCompare.clickOnCompare();
		
		assertFalse(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-2634
	* @summary Verify component "<" to Copy Parameters of PRI Two for PRI One
	*/
	@Test
	public void test_1068_VerifyComponentCopyParametersOfPRITwoForPRIOne() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.MODEL_16GB2, ConstantsCompare.ADVINNE_SOUTH_AFRICA_ADS, ConstantsCompare.NAME_0, ConstantsCompare.NAME_ADVINNE, ConstantsCompare.PATH_ITEM_NAME_0, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriThree(ConstantsCompare.ADVINNE_SOUTH_AFRICA_ADS, ConstantsCompare.MODEL_16GB2, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.copyParametersTwoOne();
		screenPerformCompare.clickOnCompare();
		String valueOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		String valueTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		String valueThreeItem = screenPerformCompare.getValuePriThreeOnResult(ConstantsCompare.PATH_ITEM_NAME_0);
		
		assertEquals(ConstantsCompare.HOME_NETWORK_724_32,valueOneItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_32,valueTwoItem);
		assertEquals(ConstantsCompare.NAME_ADVINNE,valueThreeItem);
	}
	
	/**
	* @testLinkId T-GPRI-2635
	* @summary Check "Only Auto Commit" option
	*/
	@Test
	public void test_1143_CheckOnlyAutoCommitOption() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		
		screenItemLibrary.preconditionItem(ConstantsCompare.PATH_ITEM_BAND_BLOCK, ConstantsCompare.AUTO_COMMIT, true);
		screenItemLibrary.preconditionItem(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, ConstantsCompare.AUTO_COMMIT, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, true);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.clickOnOnlyAutoCommit();
		String valueOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		
		assertEquals(ConstantsCompare.BAND_BLOCK_VALUE_50,valueOneItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_32,valueTwoItem);
	}
	
	/**
	* @testLinkId T-GPRI-2636
	* @summary Check "Show only diffs" with values differents
	*/
	@Test
	public void test_1146_CheckShowOnlyDiffsWithValuesDifferents() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_35, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);
		
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.clickOnOnlyDiffs();
		String valueBandBlockOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueBandBlockTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueHomeNetworkOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		String valueHomeNetworkTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		
		assertEquals(ConstantsCompare.EMPTY,valueBandBlockOneItem);
		assertEquals(ConstantsCompare.EMPTY,valueBandBlockTwoItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_32,valueHomeNetworkOneItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_35,valueHomeNetworkTwoItem);
	}
	
	/**
	* @testLinkId T-GPRI-2637
	* @summary Check "Show only diffs" with only same values
	*/
	@Test
	public void test_1152_CheckShowOnlyDiffsWithOnlySameValues() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.copyParametersOneTwo();
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.clickOnOnlyDiffs();
		
		assertTrue(screenPerformCompare.isResultParametersDifEmpty());
	}
	
	/**
	* @testLinkId T-GPRI-2638
	* @summary Check "Only Auto Commit" and 'Only diffs' with the different items the type Auto Commit
	*/
	@Test
	public void test_1156_CheckOnlyAutoCommitAndOnlyDiffsWithTheDifferentItemsTheTypeAutoCommit() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		
		screenItemLibrary.preconditionItem(ConstantsCompare.PATH_ITEM_BAND_BLOCK, ConstantsCompare.AUTO_COMMIT, false);
		screenItemLibrary.preconditionItem(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, ConstantsCompare.AUTO_COMMIT, true);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_35, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);
		
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.clickOnOnlyDiffs();
		screenPerformCompare.clickOnOnlyAutoCommit();
		String valueBandBlockOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueBandBlockTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueHomeNetworkOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		String valueHomeNetworkTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		
		assertEquals(ConstantsCompare.EMPTY,valueBandBlockOneItem);
		assertEquals(ConstantsCompare.EMPTY,valueBandBlockTwoItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_32,valueHomeNetworkOneItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_35,valueHomeNetworkTwoItem);
	}
	
	/**
	* @testLinkId T-GPRI-2639
	* @summary Check "Only Auto Commit" and 'Only diffs' with the different items are not type Auto Commit
	*/
	@Test
	public void test_1158_CheckOnlyAutoCommitAndOnlyDiffsWithTheDifferentItemsAreNotTypeAutoCommit() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		
		screenItemLibrary.preconditionItem(ConstantsCompare.PATH_ITEM_BAND_BLOCK, ConstantsCompare.AUTO_COMMIT, true);
		screenItemLibrary.preconditionItem(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, ConstantsCompare.AUTO_COMMIT, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_35, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);
		
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.clickOnOnlyDiffs();
		screenPerformCompare.clickOnOnlyAutoCommit();
		String valueBandBlockOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueBandBlockTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueHomeNetworkOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		String valueHomeNetworkTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		
		assertEquals(ConstantsCompare.EMPTY,valueBandBlockOneItem);
		assertEquals(ConstantsCompare.EMPTY,valueBandBlockTwoItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_32,valueHomeNetworkOneItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_35,valueHomeNetworkTwoItem);
	}
	
	/**
	* @testLinkId T-GPRI-2640
	* @summary Check Clear fields of PRI's
	*/
	@Test
	public void test_1159_ClearFields() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnClear();

		assertTrue(screenPerformCompare.isResultParametersDifEmpty());
	}
	
	/**
	* @testLinkId T-GPRI-2641
	* @summary Check "Permanent Link"
	*/
	@Test
	public void test_1160_CheckPermanentLink() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.openOptions();
		screenPerformCompare.clickOnPermanentLinkOption();
		screenPerformCompare.switchTabs(1);
		boolean resultOk = screenPerformCompare.isResultParametersDifEmpty();
		screenPerformCompare.getDriver().close();
		screenPerformCompare.switchTabs(0);
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-2642
	* @summary Download as Spreadsheet
	*/
	@Test
	public void test_1161_DownloadAsSpreadsheet() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		
		screenItemLibrary.preconditionItem(ConstantsCompare.PATH_ITEM_BAND_BLOCK, ConstantsCompare.AUTO_COMMIT, true);
		screenItemLibrary.preconditionItem(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, ConstantsCompare.AUTO_COMMIT, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGH815, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditPackageProfile.preconditionItem(ConstantsCompare.LGA270, ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.openOptions();
		boolean success = screenPerformCompare.downloadAsSpreadsheet();
		
		assertTrue(success);
	}
	
	/**
	* @testLinkId T-GPRI-2643
	* @summary Check Collapse All
	*/
	@Test
	public void test_1162_CollapseAll() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.clickOnCollapseAll();
		boolean success = screenPerformCompare.isParametersCollapsed();
		
		assertTrue(success);
	}
	
	/**
	* @testLinkId T-GPRI-2643
	* @summary Check Expand All
	*/
	@Test
	public void test_1163_ExpandAll() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.clickOnExpandAll();
		boolean success = screenPerformCompare.isParametersCollapsed();
		
		assertFalse(success);
	}
	
	/**
	* @testLinkId T-GPRI-2643
	* @summary Check Center Values
	*/
	@Test
	public void test_1164_CenterValues() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGH815, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.BUYER_ENTEL_PERU_NTP, ConstantsCompare.LGA270, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.clickOnCenterValues();
		boolean success = screenPerformCompare.isCompareButtonClickable();
		
		assertFalse(success);
	}
	
	/**
	* @testLinkId T-GPRI-2646
	* @summary Compare without model
	*/
	@Test
	public void test_1165_CompareWithoutModel() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_TIM, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.OI_BRAZIL_BOI, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.TIM_ITALY_TIM, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		String valueOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		String valueTwoItem = screenPerformCompare.getValuePriTwoOnResult(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1);
		
		assertEquals(ConstantsCompare.BAND_BLOCK_VALUE_50,valueOneItem);
		assertEquals(ConstantsCompare.HOME_NETWORK_724_32,valueTwoItem);
	}
	
	/**
	* @testLinkId T-GPRI-2647
	* @summary Search by Items and Values
	*/
	@Test
	public void test_1166_SearchByItemsAndValues() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_TIM, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.OI_BRAZIL_BOI, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.TIM_ITALY_TIM, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.setNameFilter(ConstantsCompare.BAND_BLOCK);
		screenPerformCompare.setValueFilter(ConstantsCompare.BAND_BLOCK_VALUE_50);
		String valueOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		
		assertEquals(ConstantsCompare.BAND_BLOCK_VALUE_50,valueOneItem);
	}
	
	/**
	* @testLinkId T-GPRI-2648
	* @summary Search on Only Items by Value valid
	*/
	@Test
	public void test_1167_SearchOnOnlyItemsByValueValid() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_TIM, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.OI_BRAZIL_BOI, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.TIM_ITALY_TIM, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.setNameFilter(ConstantsCompare.BAND_BLOCK);
		String valueOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		
		assertEquals(ConstantsCompare.BAND_BLOCK_VALUE_50,valueOneItem);
	}
	
	/**
	* @testLinkId T-GPRI-2649
	* @summary Search on Only Items by Value invalid
	*/
	@Test
	public void test_1168_SearchOnOnlyItemsByValueInvalid() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_TIM, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.OI_BRAZIL_BOI, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.TIM_ITALY_TIM, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.setNameFilter(ConstantsCompare.BAND_BLOCK_VALUE_50);
		boolean resultOk = screenPerformCompare.isResultParametersDifEmpty();
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-2650
	* @summary Search on Only Values by Value valid
	*/
	@Test
	public void test_1169_SearchOnOnlyValuesByValueValid() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_TIM, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.OI_BRAZIL_BOI, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.TIM_ITALY_TIM, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.setValueFilter(ConstantsCompare.BAND_BLOCK_VALUE_50);
		String valueOneItem = screenPerformCompare.getValuePriOneOnResult(ConstantsCompare.PATH_ITEM_BAND_BLOCK);
		
		assertEquals(ConstantsCompare.BAND_BLOCK_VALUE_50,valueOneItem);
	}
	
	/**
	* @testLinkId T-GPRI-2651
	* @summary Search on Only Values by Value invalid
	*/
	@Test
	public void test_1170_SearchOnOnlyValuesByValueInvalid() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformCompare screenPerformCompare = new ScreenPerformCompare(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.BAND_BLOCK, ConstantsCompare.BAND_BLOCK_VALUE_50, ConstantsCompare.PATH_ITEM_BAND_BLOCK, false);
		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_TIM, ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, true);
		screenPerformCompare.openPage();
		screenPerformCompare.fillPriOne(ConstantsCompare.OI_BRAZIL_BOI, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.fillPriTwo(ConstantsCompare.TIM_ITALY_TIM, ConstantsCompare.EMPTY, ConstantsCompare.LAST, ConstantsCompare.LAST, Arrays.asList(ConstantsCompare.ALL));
		screenPerformCompare.clickOnCompare();
		screenPerformCompare.setValueFilter(ConstantsCompare.BAND_BLOCK);
		boolean resultOk = screenPerformCompare.isResultParametersDifEmpty();
		
		assertTrue(resultOk);
	}
}
