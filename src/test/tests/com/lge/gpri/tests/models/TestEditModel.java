package com.lge.gpri.tests.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsModels;
import com.lge.gpri.screens.models.ScreenPerformListModel;
import com.lge.gpri.screens.models.ScreenPerformModel;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery SCENARY NAME.
 * 
 * @author AUTHOR NAME (can exist more than one)
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestEditModel extends AbstractTestScenary {
	
	/**
	 * @testLinkId T-GPRI-2939
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_985_ChangeIconSize() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		screenPerformModel.setIconSize(ConstantsModels.ICON_SIZE_72x72);
		screenPerformModel.clickOnSaveModel();

		String txtModelEditeSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnMoreInfo(ConstantsModels.MODEL_16GB2);

		assertEquals(ConstantsModels.ICON_SIZE_72x72, screenPerformListModel.getValueTagFromMoreInfo("Icon Size"));
		assertEquals(ConstantsModels.MODEL_EDITED_SUCCESS, txtModelEditeSuccess);
	}

	/**
	 * @testLinkId T-GPRI-2940
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_987_AddOneSWMM() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		screenPerformModel.addSWMM(ConstantsModels.SWMM_ABHAI_SINGH_ALL);
		screenPerformModel.clickOnSaveModel();

		String txtModelEditeSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnMoreInfo(ConstantsModels.MODEL_16GB2);

		assertEquals(ConstantsModels.MODEL_EDITED_SUCCESS, txtModelEditeSuccess);
		assertTrue(screenPerformListModel.getSWMMFromMoreInfo(Arrays.asList(ConstantsModels.SWMM_ABHAI_SINGH)));
	}

	/**
	 * @testLinkId T-GPRI-2941
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_990_AddMoreOneSWPL() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		screenPerformModel.addSWPL(ConstantsModels.SWPL_WOOJAE_LEE_ALL);
		screenPerformModel.addSWPL(ConstantsModels.SWPL_HUGO_KANG);
		screenPerformModel.addSWPL(ConstantsModels.SWPL_JAESUK_LEE);
		screenPerformModel.clickOnSaveModel();

		String txtModelEditeSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnMoreInfo(ConstantsModels.MODEL_16GB2);

		assertEquals(ConstantsModels.MODEL_EDITED_SUCCESS, txtModelEditeSuccess);
		assertTrue(screenPerformListModel.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_WOOJAE_LEE,
				ConstantsModels.SWPL_MANSOO_KIM, ConstantsModels.SWPL_CHANGSOO_KIM)));
	}

	/**
	 * @testLinkId T-GPRI-2942
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_992_DeleteOneSWPL() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		screenPerformModel.removeSWPL(ConstantsModels.SWPL_WOOJAE_LEE_ALL);
		screenPerformModel.clickOnSaveModel();

		String txtModelEditeSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnMoreInfo(ConstantsModels.MODEL_16GB2);

		assertEquals(ConstantsModels.MODEL_EDITED_SUCCESS, txtModelEditeSuccess);
		assertFalse(screenPerformListModel.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_WOOJAE_LEE)));
	}

	/**
	 * @testLinkId T-GPRI-2943
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_994_DeleteMoreOneSWPL() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		screenPerformModel.removeSWPL(ConstantsModels.SWPL_WOOJAE_LEE_ALL);
		screenPerformModel.removeSWPL(ConstantsModels.SWPL_MANSOO_KIM_ALL);
		screenPerformModel.clickOnSaveModel();

		String txtModelEditeSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnMoreInfo(ConstantsModels.MODEL_16GB2);

		assertEquals(ConstantsModels.MODEL_EDITED_SUCCESS, txtModelEditeSuccess);
		assertTrue(screenPerformListModel.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_CHANGSOO_KIM)));
	}

	/**
	 * @testLinkId T-GPRI-2944
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_992_DeleteOneSWMM() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		screenPerformModel.removeSWMM(ConstantsModels.SWMM_ABHAI_SINGH_ALL);
		screenPerformModel.clickOnSaveModel();

		String txtModelEditeSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnMoreInfo(ConstantsModels.MODEL_16GB2);

		assertEquals(ConstantsModels.MODEL_EDITED_SUCCESS, txtModelEditeSuccess);
		assertTrue(screenPerformListModel.getSWMMFromMoreInfo(Arrays.asList()));
	}

	/**
	 * @testLinkId T-GPRI-2945
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_998_AssociateOneUserSWPLToAnExistingModel() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		screenPerformModel.addSWPL(ConstantsModels.SWPL_MANSOO_KIM_ALL);
		screenPerformModel.clickOnSaveModel();

		String txtModelEditeSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnMoreInfo(ConstantsModels.MODEL_16GB2);

		assertEquals(ConstantsModels.MODEL_EDITED_SUCCESS, txtModelEditeSuccess);
		assertTrue(screenPerformListModel.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_MANSOO_KIM)));
	}

	/**
	 * @testLinkId T-GPRI-2946
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1001_AddMoreOneSWMM() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		screenPerformModel.addSWMM(ConstantsModels.SWMM_ABHAI_SINGH_ALL);
		screenPerformModel.addSWMM(ConstantsModels.SWMM_ALVIN_SHIN_ALL);
		screenPerformModel.clickOnSaveModel();

		String txtModelEditeSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnMoreInfo(ConstantsModels.MODEL_16GB2);

		assertEquals(ConstantsModels.MODEL_EDITED_SUCCESS, txtModelEditeSuccess);
		assertTrue(screenPerformListModel
				.getSWMMFromMoreInfo(Arrays.asList(ConstantsModels.SWMM_ABHAI_SINGH, ConstantsModels.SWMM_ALVIN_SHIN)));
	}
	
	/**
	 * @testLinkId T-GPRI-2947
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1004_DeleteOnlySWPL() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		//alterar implementação para deletar todos os SWPL, pois no modelo pode existir mais de 1 adicionado
		screenPerformModel.removeSWPL(ConstantsModels.SWPL_CHANGSOO_KIM_ALL);
		screenPerformModel.clickOnSaveModel();

		assertEquals(ConstantsModels.MODEL_EMPTY_FIELD, screenPerformModel.getTextFrom("txtMsgModelLeavingFieldEmpty"));
	}

}
