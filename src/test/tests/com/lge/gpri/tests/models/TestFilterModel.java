package com.lge.gpri.tests.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.constants.ConstantsModels;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.models.ScreenPerformListModel;
import com.lge.gpri.screens.models.ScreenPerformModel;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.packages.ScreenPerformNormalPackage;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery SCENARY NAME.
 * 
 * @author AUTHOR NAME (can exist more than one)
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestFilterModel extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2919
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1090_FilterOnlyName() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);

		WebElement desiredLineOfTable = screenPerformListModel.getDesiredLineOfTable("Name");
		String[] expectedModel = desiredLineOfTable.getText().split(" ");
		assertEquals(expectedModel[0].toString(), ConstantsModels.MODEL_16GB2);
	}

	/**
	 * @testLinkId T-GPRI-2920
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1091_FilterOnlyRepresentativeName() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		String representativeName = screenPerformModel.getRepresentativeName();
		
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRepresentativeNameFilter();
		screenPerformListModel.setRepresentativeNameFilter(representativeName);
		assertEquals(screenPerformListModel.getModelNameBasedOnRepresentativeNameInModelTable(representativeName),
				ConstantsModels.MODEL_16GB2);
	}

	/**
	 * @testLinkId T-GPRI-2921
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1092_FilterOnlyTargetDevice() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		String targetDevice = screenPerformModel.getTargetDevice();
		
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addTargetDeviceFilter();
		screenPerformListModel.setTargetDeviceFilter(targetDevice);
		
		assertTrue(screenPerformListModel.filterOnlySameTargetDevice(targetDevice));
	}

	/**
	 * @testLinkId T-GPRI-2922
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1093_FilterOnlyGridRowColumnTag() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addTagsFilter();
		screenPerformListModel.setTagFilter(ConstantsModels.TAG_GRID_ROW_X_COLUMN, ConstantsModels.GRID_ROW_COLUMN_3X2);

		assertTrue(screenPerformListModel.verifyTag(ConstantsModels.TAG_GRID_ROW_X_COLUMN,
				ConstantsModels.GRID_ROW_COLUMN_3X2));
	}

	/**
	 * @testLinkId T-GPRI-2923
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1094_FilterOnlyHotseatIconRangeTag() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addTagsFilter();
		screenPerformListModel.setTagFilter(ConstantsModels.TAG_HOTSEAT_ICON_RANGE,
				ConstantsModels.HOTSEAT_ICON_RANGE_7);

		assertTrue(screenPerformListModel.verifyTag(ConstantsModels.TAG_HOTSEAT_ICON_RANGE,
				ConstantsModels.HOTSEAT_ICON_RANGE_7));
	}

	/**
	 * @testLinkId T-GPRI-2924
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1096_FilterOnlyIconSizeTag() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addTagsFilter();
		screenPerformListModel.setTagFilter(ConstantsModels.TAG_ICON_SIZE, ConstantsModels.ICON_SIZE_136x136);

		assertTrue(screenPerformListModel.verifyTag(ConstantsModels.TAG_ICON_SIZE, ConstantsModels.ICON_SIZE_136x136));
	}

	/**
	 * @testLinkId T-GPRI-2925
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1097_FilterOnlyScreenSizeTag() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addTagsFilter();
		screenPerformListModel.setTagFilter(ConstantsModels.TAG_SCREEN_SIZE, ConstantsModels.SCREEN_SIZE_800x480);

		assertTrue(
				screenPerformListModel.verifyTag(ConstantsModels.TAG_SCREEN_SIZE, ConstantsModels.SCREEN_SIZE_800x480));
	}

	/**
	 * @testLinkId T-GPRI-2926
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1099_FilterByOneRegionRelatedToModel() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRegionFilter();
		screenPerformListModel.setRegionFilter(ConstantsModels.REGION_ASIA_MIDDLE_EAST_AFRICA);
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);

		String[] showQuantityOfModelsOnTheFootTable = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals("1", showQuantityOfModelsOnTheFootTable[0].trim());
	}

	/**
	 * @testLinkId T-GPRI-2927
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1296_FilterByMoreThanOneRegionRelatedToModel() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRegionFilter();
		screenPerformListModel.setRegionFilter(ConstantsModels.REGION_LATIN_AMERICA);
		screenPerformListModel.setRegionFilter(ConstantsModels.REGION_ASIA_MIDDLE_EAST_AFRICA);
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);

		String[] showQuantityOfModelsOnTheFootTable = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals("1", showQuantityOfModelsOnTheFootTable[0].trim());
	}

	/**
	 * @testLinkId T-GPRI-2928
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1302_FilterByAllRegionsRelatedToModel() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRegionFilter();
		screenPerformListModel.setRegionFilter(ConstantsModels.REGION_LATIN_AMERICA);
		screenPerformListModel.setRegionFilter(ConstantsModels.REGION_ASIA_MIDDLE_EAST_AFRICA);
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);

		String[] showQuantityOfModelsOnTheFootTable = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals("1", showQuantityOfModelsOnTheFootTable[0].trim());
	}

	/**
	 * @testLinkId T-GPRI-2929
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1304_FilterByAllRegionsOptions() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRegionFilter();
		screenPerformListModel.selectAllRegionFilter();
		screenPerformListModel.showTableElements("All");

		int allModels = screenPerformListModel.getModelsQuantity();
		String[] modelsQuantity = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals(modelsQuantity[0].toString().trim(), modelsQuantity[1].toString().trim());
		assertEquals(modelsQuantity[1].toString().trim(), (String.valueOf(allModels)));
	}

	/**
	 * @testLinkId T-GPRI-2930
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1308_FilterByAnyRegionsOptions() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRegionFilter();
		screenPerformListModel.deselectAllRegionFilter();
		screenPerformListModel.showTableElements("All");

		int allModels = screenPerformListModel.getModelsQuantity();
		String[] modelsQuantity = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals(modelsQuantity[0].toString().trim(), modelsQuantity[1].toString().trim());
		assertEquals(modelsQuantity[1].toString().trim(), (String.valueOf(allModels)));
	}

	/**
	 * @testLinkId T-GPRI-2931
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1309_FilterByAllRegionOptionsCheckingEachOneOfThem() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRegionFilter();
		screenPerformListModel.selectAllRegionFilterOneByOne();
		screenPerformListModel.showTableElements("All");

		int allModels = screenPerformListModel.getModelsQuantity();
		String[] modelsQuantity = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals(modelsQuantity[0].toString().trim(), modelsQuantity[1].toString().trim());
		assertEquals(modelsQuantity[1].toString().trim(), (String.valueOf(allModels)));
	}

	/**
	 * @testLinkId T-GPRI-2932
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1342_FilterByAnyRegionOptionsUncheckingEachOneOfThem() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRegionFilter();
		screenPerformListModel.selectAllRegionFilter();
		screenPerformListModel.deselectAllRegionFilterOneByOne();
		;
		screenPerformListModel.showTableElements("All");

		int allModels = screenPerformListModel.getModelsQuantity();
		String[] modelsQuantity = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals(modelsQuantity[0].toString().trim(), modelsQuantity[1].toString().trim());
		assertEquals(modelsQuantity[1].toString().trim(), (String.valueOf(allModels)));
	}

	/**
	 * @testLinkId T-GPRI-2933
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1352_SearchRegionName() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRegionFilter();
		assertEquals("1", String.valueOf(
				screenPerformListModel.verifyDisplayRegionInFilter(ConstantsModels.REGION_ASIA_MIDDLE_EAST_AFRICA)));
	}

	/**
	 * @testLinkId T-GPRI-2934
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1353_SearchIvalidValueRegionName() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRegionFilter();
		String messageForInvalidSearch = ConstantsModels.INVALID_SEARCH.replace("REGION",
				ConstantsModels.INVALID_REGION);
		assertEquals(messageForInvalidSearch,
				screenPerformListModel.verifySearchForInvalidRegion(ConstantsModels.INVALID_REGION));
	}

	/**
	 * @testLinkId T-GPRI-2935
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1354_FilterByRegionNotRelatedToModel() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRegionFilter();
		screenPerformListModel.setRegionFilter(ConstantsModels.REGION_LATIN_AMERICA);
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_LG_G350);

		assertEquals(ConstantsModels.NO_MODELS_AVAILABLE, (screenPerformListModel.getMessageForNoModelsAvailable()));
	}

	/**
	 * @testLinkId T-GPRI-2936
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1356_FilterByNoPackage() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.deselectAllModelFilter();
		screenPerformListModel.addRegionFilter();
		screenPerformListModel.setRegionFilter(ConstantsModels.NO_PACKAGE);

		assertTrue(screenPerformListModel.verifyNoPackageRelatedToPackage(screenPerformListModel));
	}

	/**
	 * @testLinkId T-GPRI-2937
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1358_FilterByNoPackageAfterInsertFirstPackageToModel() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_64GB2);
		screenPerformListModel.clickOnViewPackages(ConstantsModels.MODEL_64GB2);
		String txtPackageCreateSuccess = createPackage();
		screenPerformListModel.openPage();
		screenPerformListModel.addRegionFilter();
		screenPerformListModel.setRegionFilter(ConstantsModels.NO_PACKAGE);
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_64GB2);
		assertEquals(ConstantsModels.NO_MODELS_AVAILABLE, (screenPerformListModel.getMessageForNoModelsAvailable()));
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
	}

	/**
	 * @testLinkId T-GPRI-2938
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1360_FilterByNoPackageAfterDeletingPackageOfModel() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_64GB2);
		Thread.sleep(500);
		screenPerformListModel.elementFocus();
		screenPerformListModel.clickOnViewPackages(ConstantsModels.MODEL_64GB2);

		boolean exists = screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		if (!exists) {
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformNormalPackage.fillFieldsPackage(ConstantsModels.MODEL_64GB2, "",
					ConstantsPackages.OI_BRAZIL_BOI, ConstantsPackages.PACKAGE_NAME_TEST, ConstantsPackages.TODAY,
					ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "2",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
					ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
			screenPerformNormalPackage.clickOnSaveCreatedButton();
			screenPerformListModel.openPage();
			screenPerformListModel.addNameFilter();
			screenPerformListModel.setNameFilter(ConstantsModels.MODEL_64GB2);
			screenPerformListModel.elementFocus();
			screenPerformListModel.clickOnViewPackages(ConstantsModels.MODEL_64GB2);

			screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		}
		String buyerName = screenPerformListPackages.getBuyerName();
		String[] split = buyerName.split(" ");
		String[] split2 = split[split.length - 1].split("\\(");
		String[] suffix = split2[1].split("\\)");

		screenPerformListPackages.clickOnRemovePackageButton();
		screenPerformListPackages.confirmRemovePackage();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffix[0]);
		String regionOfBuyer = screenPerformListBuyers.getRegionOfBuyer();

		screenPerformListModel.openPage();
		screenPerformListModel.addRegionFilter();
		screenPerformListModel.setRegionFilter(regionOfBuyer);

		assertEquals(ConstantsModels.NO_MODELS_AVAILABLE, (screenPerformListModel.getMessageForNoModelsAvailable()));
	}

	/**
	 * Method for create package
	 * 
	 * @return message success
	 * @throws Exception
	 */
	private String createPackage() throws Exception {
		preconditions.preconditionRestoreModel16GB2();
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.fillFieldsPackage(ConstantsModels.MODEL_64GB2, "", ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TEST, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "3", ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.ANDROID_VERSION_5, ConstantsPackages.GRID_ROW_COLUMN_5X5,
				ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		return screenPerformNormalPackage.getTextFrom("txtPackageCreatedSuccess");
	}
}
