package com.lge.gpri.tests.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsModels;
import com.lge.gpri.helpers.constants.ConstantsUserManagement;
import com.lge.gpri.screens.models.ScreenPerformListModel;
import com.lge.gpri.screens.models.ScreenPerformModel;
import com.lge.gpri.screens.usermanagement.ScreenUsersManagement;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery SCENARY NAME.
 * 
 * @author AUTHOR NAME (can exist more than one)
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCreateModel extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2948
	 * @summary THE Suite Test related T-GPRI-2948
	 */
	@Test
	public void test_1066_NewModelWithOneSWPL() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT,
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL), Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL));
		String model = screenPerformModel.getModelName();
		screenPerformModel.clickOnSaveModel();
		String txtModelCreateSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(model);
		screenPerformListModel.clickOnMoreInfo(model);
		assertEquals(ConstantsModels.MODEL_CREATED_SUCCESS, txtModelCreateSuccess);
	}

	/**
	 * @testLinkId T-GPRI-2949
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1067_NewModelWithMoreThanOneSWPL() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();

		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.MESSAGE_ONLY_FOR_TESTING, ConstantsModels.MESSAGE_ONLY_FOR_TESTING,
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL, ConstantsModels.SWPL_HUGO_KANG_ALL),
				Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL, ConstantsModels.SWMM_FOX_LEE_ALL));
		String model = screenPerformModel.getModelName();
		screenPerformModel.clickOnSaveModel();
		String txtModelCreateSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(model);
		screenPerformListModel.clickOnMoreInfo(model);
		assertEquals(ConstantsModels.MODEL_CREATED_SUCCESS, txtModelCreateSuccess);
	}

	/**
	 * @testLinkId T-GPRI-2950
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1071_NewModelLeavingOneEmpyField() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();

		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				"", ConstantsModels.MESSAGE_ONLY_FOR_TESTING,
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL, ConstantsModels.SWPL_HUGO_KANG_ALL),
				Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL, ConstantsModels.SWMM_FOX_LEE_ALL));
		screenPerformModel.clickOnSaveModel();
		assertEquals(ConstantsModels.MODEL_EMPTY_FIELD, screenPerformModel.getTextFrom("txtMsgModelLeavingFieldEmpty"));
	}

	/**
	 * @testLinkId T-GPRI-2951
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1072_NewModelLeavingMoreEmpyField() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();

		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				"", "",
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL), Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL));
		screenPerformModel.clickOnSaveModel();
		assertEquals(ConstantsModels.MODEL_EMPTY_FIELD, screenPerformModel.getTextFrom("txtMsgModelLeavingFieldEmpty"));
	}

	/**
	 * @testLinkId T-GPRI-2952
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1019_NewModelWithoutModelName() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();

		screenPerformModel.fillFieldsModel("", ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT,
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL), Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL));
		screenPerformModel.clickOnSaveModel();
		assertEquals(ConstantsModels.MODEL_EMPTY_FIELD, screenPerformModel.getTextFrom("txtMsgModelLeavingFieldEmpty"));
	}

	/**
	 * @testLinkId T-GPRI-2953
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1073_NewModelWithoutSWPL() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();

		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT, Arrays.asList(),
				Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL));
		screenPerformModel.clickOnSaveModel();
		assertEquals(ConstantsModels.MODEL_EMPTY_FIELD, screenPerformModel.getTextFrom("txtMsgModelLeavingFieldEmpty"));
	}

	/**
	 * @testLinkId T-GPRI-2954
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1075_NewModelSelectingOnlySWPL() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.addSWPL(ConstantsModels.SWPL_HUGO_KANG);
		screenPerformModel.clickOnSaveModel();
		assertEquals(ConstantsModels.MODEL_EMPTY_FIELD, screenPerformModel.getTextFrom("txtMsgModelLeavingFieldEmpty"));
	}

	/**
	 * @testLinkId T-GPRI-2955
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1076_NewModelSelectingTwoSWPL() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel("", "", "", "",
				Arrays.asList(ConstantsModels.SWPL_HUGO_KANG_ALL, ConstantsModels.SWPL_JAESUK_LEE_ALL),
				Arrays.asList());
		screenPerformModel.clickOnSaveModel();
		assertEquals(ConstantsModels.MODEL_EMPTY_FIELD, screenPerformModel.getTextFrom("txtMsgModelLeavingFieldEmpty"));
	}

	/**
	 * @testLinkId T-GPRI-2956
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1077_NewModelLeavingAllEmptyFields() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.clickOnSaveModel();
		assertEquals(ConstantsModels.MODEL_EMPTY_FIELD, screenPerformModel.getTextFrom("txtMsgModelLeavingFieldEmpty"));
	}

	/**
	 * @testLinkId T-GPRI-2957
	 * @summary THE Suite Test related T-GPRI-2957
	 */
	@Test
	public void test_1006_NewModelWithOnlySWPLAndWithoutSWMM() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT,
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL), Arrays.asList());
		String modelName = screenPerformModel.getModelName();
		screenPerformModel.clickOnSaveModel();
		String txtModelCreateSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(modelName);
		screenPerformListModel.clickOnMoreInfo(modelName);
		boolean isSWPLOK = screenPerformListModel.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE));
		screenUsersManagement.openPage();
		screenUsersManagement.waitLoading();
		boolean resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_JAESUK_LEE);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWPL_JAESUK_LEE);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_JAESUK_LEE);
		}
		ArrayList<String> models = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWPL, 2);
		boolean isModelOk = false;
		if (models.contains(modelName)) {
			isModelOk = true;
		}
		assertEquals(ConstantsModels.MODEL_CREATED_SUCCESS, txtModelCreateSuccess);
		assertTrue(isSWPLOK);
		assertTrue(isModelOk);
	}

	/**
	 * @testLinkId T-GPRI-2958
	 * @summary THE Suite Test related T-GPRI-2958
	 */
	@Test
	public void test_1008_NewModelWithMoreThanSWPLAndWithoutSWMM() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT,
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL, ConstantsModels.SWPL_HUGO_KANG_ALL),
				Arrays.asList());
		String modelName = screenPerformModel.getModelName();
		screenPerformModel.clickOnSaveModel();
		String txtModelCreateSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(modelName);
		screenPerformListModel.clickOnMoreInfo(modelName);
		boolean isSWPLOK = screenPerformListModel
				.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE, ConstantsModels.SWPL_HUGO_KANG));
		screenUsersManagement.openPage();
		screenUsersManagement.waitLoading();
		boolean resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_HUGO_KANG);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWPL_HUGO_KANG);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_HUGO_KANG);
		}
		ArrayList<String> models = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWPL, 2);
		boolean isModelOk = false;
		if (models.contains(modelName)) {
			isModelOk = true;
		}
		assertEquals(ConstantsModels.MODEL_CREATED_SUCCESS, txtModelCreateSuccess);
		assertTrue(isSWPLOK);
		assertTrue(isModelOk);
	}

	/**
	 * @testLinkId T-GPRI-2959
	 * @summary THE Suite Test related T-GPRI-2959
	 */
	@Test
	public void test_1012_NewModelWithOnlyOneSWMMAndWithoutSWPL() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT, Arrays.asList(),
				Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL));
		screenPerformModel.clickOnSaveModel();
		assertEquals(ConstantsModels.MODEL_EMPTY_FIELD, screenPerformModel.getTextFrom("txtMsgModelLeavingFieldEmpty"));
	}

	/**
	 * @testLinkId T-GPRI-2960
	 * @summary THE Suite Test related T-GPRI-2960
	 */
	@Test
	public void test_1013_NewModelWithMoreThanOneSWMMAndWithoutSWPL() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT, Arrays.asList(),
				Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL, ConstantsModels.SWMM_FOX_LEE_ALL));
		screenPerformModel.clickOnSaveModel();
		assertEquals(ConstantsModels.MODEL_EMPTY_FIELD, screenPerformModel.getTextFrom("txtMsgModelLeavingFieldEmpty"));
	}

	/**
	 * @testLinkId T-GPRI-2961
	 * @summary THE Suite Test related T-GPRI-2961
	 */
	@Test
	public void test_1014_NewModelWithOneSWPLAndOneSWMM() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT,
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL), Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL));
		String modelName = screenPerformModel.getModelName();
		screenPerformModel.clickOnSaveModel();
		String txtModelCreateSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(modelName);
		screenPerformListModel.clickOnMoreInfo(modelName);
		boolean isSWPLOK = screenPerformListModel.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE));
		boolean isSWMMOK = screenPerformListModel.getSWMMsFromMoreInfo(Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN));
		screenUsersManagement.openPage();
		screenUsersManagement.waitLoading();
		boolean resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_JAESUK_LEE);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWPL_JAESUK_LEE);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_JAESUK_LEE);
		}
		ArrayList<String> modelSWPL = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWPL, 2);
		boolean isModelSWPLOk = false;
		if (modelSWPL.contains(modelName)) {
			isModelSWPLOk = true;
		}
		resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_ALVIN_SHIN);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWMM_ALVIN_SHIN);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_ALVIN_SHIN);
		}
		ArrayList<String> modelSWMM = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWMM, 1);
		boolean isModelSWMMOk = false;
		if (modelSWMM.contains(modelName)) {
			isModelSWMMOk = true;
		}
		assertEquals(ConstantsModels.MODEL_CREATED_SUCCESS, txtModelCreateSuccess);
		assertTrue(isSWPLOK);
		assertTrue(isSWMMOK);
		assertTrue(isModelSWPLOk);
		assertTrue(isModelSWMMOk);
	}

	/**
	 * @testLinkId T-GPRI-2962
	 * @summary THE Suite Test related T-GPRI-2962
	 */
	@Test
	public void test_1015_NewModelWithTwoSWPLAndTwoSWMM() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT,
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL, ConstantsModels.SWPL_HUGO_KANG_ALL),
				Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL, ConstantsModels.SWMM_FOX_LEE_ALL));
		String modelName = screenPerformModel.getModelName();
		screenPerformModel.clickOnSaveModel();
		String txtModelCreateSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(modelName);
		screenPerformListModel.clickOnMoreInfo(modelName);
		boolean isSWPLOK1 = screenPerformListModel.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE));
		boolean isSWPLOK2 = screenPerformListModel.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_HUGO_KANG));
		boolean isSWMMOK1 = screenPerformListModel.getSWMMsFromMoreInfo(Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN));
		boolean isSWMMOK2 = screenPerformListModel.getSWMMsFromMoreInfo(Arrays.asList(ConstantsModels.SWMM_FOX_LEE));
		screenUsersManagement.openPage();
		screenUsersManagement.waitLoading();
		boolean resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_JAESUK_LEE);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWPL_JAESUK_LEE);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_JAESUK_LEE);
		}
		ArrayList<String> modelSWPL1 = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWPL, 2);
		boolean isModelSWPLOk1 = false;
		if (modelSWPL1.contains(modelName)) {
			isModelSWPLOk1 = true;
		}
		resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_HUGO_KANG);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWPL_HUGO_KANG);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_HUGO_KANG);
		}
		ArrayList<String> modelSWPL2 = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWPL, 2);
		boolean isModelSWPLOk2 = false;
		if (modelSWPL2.contains(modelName)) {
			isModelSWPLOk2 = true;
		}
		resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_ALVIN_SHIN);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWMM_ALVIN_SHIN);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_ALVIN_SHIN);
		}
		ArrayList<String> modelSWMM1 = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWMM, 1);
		boolean isModelSWMMOk1 = false;
		if (modelSWMM1.contains(modelName)) {
			isModelSWMMOk1 = true;
		}
		resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_FOX_LEE);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWMM_FOX_LEE);
			screenUsersManagement.moveToPageHeader();
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_FOX_LEE);
		}
		ArrayList<String> modelSWMM2 = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWMM, 1);
		boolean isModelSWMMOk2 = false;
		if (modelSWMM2.contains(modelName)) {
			isModelSWMMOk2 = true;
		}
		assertEquals(ConstantsModels.MODEL_CREATED_SUCCESS, txtModelCreateSuccess);
		assertTrue(isSWPLOK1);
		assertTrue(isSWPLOK2);
		assertTrue(isSWMMOK1);
		assertTrue(isSWMMOK2);
		assertTrue(isModelSWPLOk1);
		assertTrue(isModelSWPLOk2);
		assertTrue(isModelSWMMOk1);
		assertTrue(isModelSWMMOk2);
	}

	/**
	 * @testLinkId T-GPRI-2963
	 * @summary THE Suite Test related T-GPRI-2963
	 */
	@Test
	public void test_1016_NewModelWithOneSWPLAndTwoSWMM() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT,
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL),
				Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL, ConstantsModels.SWMM_FOX_LEE_ALL));
		String modelName = screenPerformModel.getModelName();
		screenPerformModel.clickOnSaveModel();
		String txtModelCreateSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(modelName);
		screenPerformListModel.clickOnMoreInfo(modelName);
		boolean isSWPLOK1 = screenPerformListModel.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE));
		boolean isSWMMOK1 = screenPerformListModel.getSWMMsFromMoreInfo(Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN));
		boolean isSWMMOK2 = screenPerformListModel.getSWMMsFromMoreInfo(Arrays.asList(ConstantsModels.SWMM_FOX_LEE));
		screenUsersManagement.openPage();
		screenUsersManagement.waitLoading();
		boolean resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_JAESUK_LEE);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWPL_JAESUK_LEE);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_JAESUK_LEE);
		}
		ArrayList<String> modelSWPL1 = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWPL, 2);
		boolean isModelSWPLOk1 = false;
		if (modelSWPL1.contains(modelName)) {
			isModelSWPLOk1 = true;
		}
		resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_ALVIN_SHIN);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWMM_ALVIN_SHIN);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_ALVIN_SHIN);
		}
		ArrayList<String> modelSWMM1 = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWMM, 1);
		boolean isModelSWMMOk1 = false;
		if (modelSWMM1.contains(modelName)) {
			isModelSWMMOk1 = true;
		}
		resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_FOX_LEE);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWMM_FOX_LEE);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_FOX_LEE);
		}
		ArrayList<String> modelSWMM2 = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWMM, 1);
		boolean isModelSWMMOk2 = false;
		if (modelSWMM2.contains(modelName)) {
			isModelSWMMOk2 = true;
		}
		assertEquals(ConstantsModels.MODEL_CREATED_SUCCESS, txtModelCreateSuccess);
		assertTrue(isSWPLOK1);
		assertTrue(isSWMMOK1);
		assertTrue(isSWMMOK2);
		assertTrue(isModelSWPLOk1);
		assertTrue(isModelSWMMOk1);
		assertTrue(isModelSWMMOk2);
	}

	/**
	 * @testLinkId T-GPRI-2964
	 * @summary THE Suite Test related T-GPRI-2964
	 */
	@Test
	public void test_1017_NewModelWithTwoSWPLAndOneSWMM() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT,
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL, ConstantsModels.SWPL_HUGO_KANG_ALL),
				Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL));
		String modelName = screenPerformModel.getModelName();
		screenPerformModel.clickOnSaveModel();
		String txtModelCreateSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(modelName);
		screenPerformListModel.clickOnMoreInfo(modelName);
		boolean isSWPLOK1 = screenPerformListModel.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE));
		boolean isSWPLOK2 = screenPerformListModel.getSWPLsFromMoreInfo(Arrays.asList(ConstantsModels.SWPL_HUGO_KANG));
		boolean isSWMMOK1 = screenPerformListModel.getSWMMsFromMoreInfo(Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN));
		screenUsersManagement.openPage();
		screenUsersManagement.waitLoading();
		boolean resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_JAESUK_LEE);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWPL_JAESUK_LEE);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_JAESUK_LEE);
		}
		ArrayList<String> modelSWPL1 = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWPL, 2);
		boolean isModelSWPLOk1 = false;
		if (modelSWPL1.contains(modelName)) {
			isModelSWPLOk1 = true;
		}
		resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_HUGO_KANG);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWPL_HUGO_KANG);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWPL_HUGO_KANG);
		}
		ArrayList<String> modelSWPL2 = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWPL, 2);
		boolean isModelSWPLOk2 = false;
		if (modelSWPL2.contains(modelName)) {
			isModelSWPLOk2 = true;
		}
		resultUserOk = screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_ALVIN_SHIN);
		if (!resultUserOk) {
			unblockUser(ConstantsModels.SWMM_ALVIN_SHIN);
			screenUsersManagement.searchAndSelectUser(ConstantsModels.SWMM_ALVIN_SHIN);
		}
		ArrayList<String> modelSWMM1 = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.SWMM, 1);
		boolean isModelSWMMOk1 = false;
		if (modelSWMM1.contains(modelName)) {
			isModelSWMMOk1 = true;
		}
		assertEquals(ConstantsModels.MODEL_CREATED_SUCCESS, txtModelCreateSuccess);
		assertTrue(isSWPLOK1);
		assertTrue(isSWPLOK2);
		assertTrue(isSWMMOK1);
		assertTrue(isModelSWPLOk1);
		assertTrue(isModelSWPLOk2);
		assertTrue(isModelSWMMOk1);

	}

	/**
	 * @testLinkId T-GPRI-2965
	 * @summary THE Suite Test related T-GPRI-2965
	 */
	@Test
	public void test_1018_NewModelWithoutSWPLAndWithoutSWMM() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT, Arrays.asList(), Arrays.asList());
		screenPerformModel.clickOnSaveModel();
		assertEquals(ConstantsModels.MODEL_EMPTY_FIELD, screenPerformModel.getTextFrom("txtMsgModelLeavingFieldEmpty"));
	}
	
	private void unblockUser(String userName) throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		screenUsersManagement.clickOnBlockedUsers();
		screenUsersManagement.checkUserOnListBlock(ConstantsModels.SWMM_FOX_LEE);
		screenUsersManagement.clickOnUnblockSelectedUsers();
		screenUsersManagement.clickOnCloseBlockedUsers();
		screenUsersManagement.moveToPageHeader();
	}

}
