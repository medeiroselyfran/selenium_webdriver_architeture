package com.lge.gpri.tests.models;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;

import com.lge.gpri.helpers.constants.ConstantsModels;
import com.lge.gpri.screens.models.ScreenPerformListModel;
import com.lge.gpri.screens.models.ScreenPerformModel;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery SCENARY NAME.
 * 
 * @author AUTHOR NAME (can exist more than one)
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestComponentsModel extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2966
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1078_RemoveModel() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		String modelInfo = "";
		screenPerformListModel.openPage();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_LG_G350);
		if (ConstantsModels.NO_MODELS_AVAILABLE.equals(screenPerformListModel.getMessageForNoModelsAvailable())) {
			screenPerformListModel.clickOnShowDeletedModelsButton();
			screenPerformListModel.clickOnRestoreModel(ConstantsModels.MODEL_LG_G350);
			screenPerformListModel.confirmRestoreModel();
			screenPerformListModel.clickOnShowDeletedModelsButton();
		}
		modelInfo = screenPerformListModel.getDesiredLineOfTable("Name").getText();
		String[] modelName = modelInfo.split(" ");
		screenPerformListModel.clickOnRemoveModel(ConstantsModels.MODEL_LG_G350);
		screenPerformListModel.confirmRemoveModel();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_LG_G350);
		assertEquals(ConstantsModels.MODEL_LG_G350, modelName[0]);
		assertEquals(ConstantsModels.NO_MODELS_AVAILABLE, screenPerformListModel.getMessageForNoModelsAvailable());
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-2967
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1079_ShowDeletedModels() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnShowDeletedModelsButton();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_LG_G350);
		String modelInfo = "";
		modelInfo = screenPerformListModel.getDesiredLineOfTable("Name").getText();
		String[] modelName = modelInfo.split(" ");
		assertEquals(ConstantsModels.MODEL_LG_G350, modelName[0]);
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-2968
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1080_RestoreModel() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnShowDeletedModelsButton();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_LG_G350);
		screenPerformListModel.clickOnRestoreModel(ConstantsModels.MODEL_LG_G350);
		screenPerformListModel.confirmRestoreModel();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_LG_G350);
		String modelInfo = "";
		modelInfo = screenPerformListModel.getDesiredLineOfTable("Name").getText();
		String[] modelName = modelInfo.split(" ");
		assertEquals(ConstantsModels.MODEL_LG_G350, modelName[0]);
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-2969
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1081_ShowOnlyMyModels() throws Exception {

		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		preConditionForTCShowOnlyMyModels(screenPerformListModel);

		screenPerformListModel.clickOnShowOnlyMyModelsButton();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_LG_G350);
		screenPerformListModel.clickOnRestoreModel(ConstantsModels.MODEL_LG_G350);
		screenPerformListModel.confirmRestoreModel();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_LG_G350);
		String modelInfo = "";
		modelInfo = screenPerformListModel.getDesiredLineOfTable("Name").getText();
		String[] modelName = modelInfo.split(" ");
		assertEquals(ConstantsModels.MODEL_LG_G350, modelName[0]);
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-2970
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1082_SearchAnInvalidModel() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.setNameFilter(ConstantsModels.INVALID_MODEL);
		assertEquals(ConstantsModels.NO_MODELS_AVAILABLE, (screenPerformListModel.getMessageForNoModelsAvailable()));
	}

	/**
	 * @testLinkId T-GPRI-2976
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1088_Next10Models() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.showTableElements("10");
		screenPerformListModel.paginateNext();
		screenPerformListModel.waitLoading();
		String[] modelsQuantity = getDriver().findElement(By.id("tblModels_info")).getText().split("Showing ");
		String[] quantityModelsDisplayInTable = modelsQuantity[1].split(" - ");
		assertEquals("11", quantityModelsDisplayInTable[0].toString().trim());
	}

	/**
	 * @testLinkId T-GPRI-2977
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1089_Previous10Models() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.showTableElements("10");
		screenPerformListModel.paginateNext();
		screenPerformListModel.paginatePrevious();
		screenPerformListModel.waitLoading();
		String[] modelsQuantity = getDriver().findElement(By.id("tblModels_info")).getText().split("Showing ");
		String[] quantityModelsDisplayInTable = modelsQuantity[1].split(" - ");
		assertEquals("1", quantityModelsDisplayInTable[0].toString().trim());
	}

	/**
	 * @testLinkId T-GPRI-2971
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1083_Show10Models() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.showTableElements("10");

		int allModels = screenPerformListModel.getModelsQuantity();
		String[] modelsQuantity = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals("10", modelsQuantity[0].toString().trim());
		assertEquals("10", (String.valueOf(allModels)));

	}

	/**
	 * @testLinkId T-GPRI-2972
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1084_Show25Models() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.showTableElements("25");
		int allModels = screenPerformListModel.getModelsQuantity();
		String[] modelsQuantity = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals("25", modelsQuantity[0].toString().trim());
		assertEquals("25", (String.valueOf(allModels)));
	}

	/**
	 * @testLinkId T-GPRI-2973
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1085_Show50Models() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.showTableElements("50");
		int allModels = screenPerformListModel.getModelsQuantity();
		String[] modelsQuantity = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals("50", modelsQuantity[0].toString().trim());
		assertEquals("50", (String.valueOf(allModels)));
	}

	/**
	 * @testLinkId T-GPRI-2974
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1086_Show100Models() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.showTableElements("100");
		int allModels = screenPerformListModel.getModelsQuantity();
		String[] modelsQuantity = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals("100", modelsQuantity[0].toString().trim());
		assertEquals("100", (String.valueOf(allModels)));
	}

	/**
	 * @testLinkId T-GPRI-2975
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_1087_ShowALLModels() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.showTableElements("All");
		int allModels = screenPerformListModel.getModelsQuantity();
		String[] modelsQuantity = screenPerformListModel.getShowQuantityOfModelsOnTheFootTable();
		assertEquals(modelsQuantity[0].toString().trim(), modelsQuantity[1].toString().trim());
		assertEquals(modelsQuantity[1].toString().trim(), (String.valueOf(allModels)));
	}

	private void preConditionForTCShowOnlyMyModels(ScreenPerformListModel screenPerformListModel) throws Exception {
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		screenPerformListModel.openPage();
		screenPerformListModel.clickOnNewModelButton();
		screenPerformModel.fillFieldsModel(ConstantsModels.FIRST, ConstantsModels.REPRESENTATIVE_NAME_TEST_MODEL,
				ConstantsModels.TARGET_DEVICE, ConstantsModels.PRODUCT,
				Arrays.asList(ConstantsModels.SWPL_JAESUK_LEE_ALL), Arrays.asList(ConstantsModels.SWMM_ALVIN_SHIN_ALL));
		screenPerformModel.clickOnSaveModel();
		String txtModelCreateSuccess = screenPerformModel.getTextFrom("txtMsgModelAfterClickSave");
		screenPerformModel.clickOnCreateModelOkButton();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_4GB3);
		screenPerformListModel.clickOnMoreInfo(ConstantsModels.MODEL_4GB3);
		assertEquals(ConstantsModels.MODEL_CREATED_SUCCESS, txtModelCreateSuccess);
	}
}
