package com.lge.gpri.tests.itemLibrary;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsItemLibrary;
import com.lge.gpri.screens.itemLibrary.ScreenChangeLog;
import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Change Log.
 * 
 * @author AUTHOR NAME (can exist more than one)
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestChangeLog extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-3029
	 * @summary When check item for multiple. Should be considered that the
	 *          IT-1311 was runned
	 */
	@Test
	public void test_1331_WhenCheckItemForMultiple() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.unselectOption("Multiple");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.selectOption("Multiple");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();
		// TODO necessita alterar o @DATE_FORMAT para dd/MM/yyyy, pois a data do
		// sistema o formato é esse.
		// TODO Aplica-se a todos as outras suites de teste, essa alteração.
		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);
		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];
		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Multiple: No --> Yes"));
	}

	/**
	 * @testLinkId T-GPRI-3030
	 * @summary When uncheck item for multiple
	 */
	@Test
	public void test_1332_WhenUncheckItemForMultiple() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOption("Multiple");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.unselectOption("Multiple");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);
		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];
		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Multiple: Yes --> No"));
	}

	/**
	 * @testLinkId T-GPRI-3031
	 * @summary When check item for From App Pool
	 */
	@Test
	public void test_1333_WhenCheckItemForFromAppPool() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.unselectOption("From App Pool");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.selectOption("From App Pool");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);
		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];
		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("From APP pool: No --> Yes"));
	}

	/**
	 * @testLinkId T-GPRI-3032
	 * @summary When uncheck item for From App Pool
	 */
	@Test
	public void test_1334_WhenUncheckItemForFromAppPool() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOption("From App Pool");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.unselectOption("From App Pool");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);
		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];
		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("From APP pool: Yes --> No"));
	}

	/**
	 * @testLinkId T-GPRI-3033
	 * @summary When add Tag Type on Item
	 */
	@Test
	public void test_1335_WhenAddTagTypeOnItem() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.removeTagOrRegion("Payment");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.fillTagType("Payment");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Tag Types:"));
		description = description.split("-->")[1];
		assertTrue(description.contains("Payment"));
	}

	/**
	 * @testLinkId T-GPRI-3034
	 * @summary When remove Tag Type on Item
	 */
	@Test
	public void test_1336_WhenRemoveTagTypeOnItem() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.fillTagType("Payment");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.removeTagOrRegion("Payment");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Tag Types:"));
		description = description.split("-->")[0];
		assertTrue(description.contains("Payment"));
	}

	/**
	 * @testLinkId T-GPRI-3035
	 * @summary When add Tag Value on Item
	 */
	@Test
	public void test_1337_WhenAddTagValueOnItem() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.removeTagValue("Android Version 5.0");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.fillTagValue("Android Version 5.0");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Tag Values:"));
		description = description.split("-->")[1];
		assertTrue(description.contains("5.0"));
	}

	/**
	 * @testLinkId T-GPRI-3036
	 * @summary When remove Tag Value on Item
	 */
	@Test
	public void test_1338_WhenRemoveTagValueOnItem() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.fillTagValue("Android Version 5.0");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.removeTagValue("Android Version 5.0");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Tag Values:"));
		description = description.split("-->")[0];
		assertTrue(description.contains("5.0"));
	}

	/**
	 * @testLinkId T-GPRI-3037
	 * @summary When add Region on Item
	 */
	@Test
	public void test_1339_WhenAddRegionOnItem() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.removeTagOrRegion("USA");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.fillRegion("USA");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Regions:"));
		description = description.split("-->")[1];
		assertTrue(description.contains("USA"));
	}

	/**
	 * @testLinkId T-GPRI-3038
	 * @summary When remove Region on Item
	 */
	@Test
	public void test_1340_WhenRemoveRegionOnItem() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.fillRegion("USA");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.removeTagOrRegion("USA");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Regions:"));
		String descriptionBefore = description = description.split("-->")[0];
		assertTrue(descriptionBefore.contains("USA"));
	}

	/**
	 * @testLinkId T-GPRI-3039
	 * @summary When add Check Procedure on Item
	 */
	@Test
	public void test_1341_WhenAddCheckProcedureOnItem() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectCheckProcedure();
		screenItemLibrary.fillCheckProcedure("Test");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Check Procedure Changed:"));
		description = description.split("-->")[1];
		assertTrue(description.contains("New"));
	}

	/**
	 * @testLinkId T-GPRI-3040
	 * @summary When edit Check Procedure on Item
	 */
	@Test
	public void test_1343_WhenEditCheckProcedureOnItem() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectCheckProcedure();
		screenItemLibrary.fillCheckProcedure("New Test");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Check Procedure Changed: Old --> New"));
	}

	/**
	 * @testLinkId T-GPRI-3041
	 * @summary When add Item Reviewer on Item
	 */
	@Test
	public void test_1344_WhenAddItemReviewerOnItem() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.clickOnTabReviewers();
		screenItemLibrary.clickOnSelectReviewers();
		screenItemLibrary.selectReviewerItem("junho.roh");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String description = screenChangeLog.getDescriptionFirstLog();
		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertFalse(description.contains("Reviewrs"));
	}

	/**
	 * @testLinkId T-GPRI-3042
	 * @summary When remove Item Reviewer on Item
	 */
	@Test
	public void test_1345_WhenRemoveItemReviewerOnItem() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.clickOnTabReviewers();
		screenItemLibrary.removeReviewer("junho.roh");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_IAAPN_PROTOCOL_TYPE);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String description = screenChangeLog.getDescriptionFirstLog();
		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertFalse(description.contains("Reviewrs"));
	}

	/**
	 * @testLinkId T-GPRI-3043
	 * @summary When check group for multipe
	 */
	@Test
	public void test_1346_WhenCheckGroupForMultipe() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Roaming Icon");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.unselectOptionFolder("Multiple");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.selectOptionFolder("Multiple");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Multiple: No --> Yes"));
	}

	/**
	 * @testLinkId T-GPRI-3044
	 * @summary When uncheck group for multipe
	 */
	@Test
	public void test_1347_WhenUncheckGroupForMultipe() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Roaming Icon");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.selectOptionFolder("Multiple");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.unselectOptionFolder("Multiple");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Multiple: Yes --> No"));
	}

	/**
	 * @testLinkId T-GPRI-3045
	 * @summary When add Tag Type on group
	 */
	@Test
	public void test_1348_WhenAddTagTypeOnGroup() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Roaming Icon");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.removeTagOrRegion("Payment");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.fillTagType("Payment");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Tag Types:"));
		description = description.split("Tag Types:")[1];
		description = description.split("-->")[1];
		assertTrue(description.contains("Payment"));
	}

	/**
	 * @testLinkId T-GPRI-3046
	 * @summary When remove Tag Type on group
	 */
	@Test
	public void test_1349_WhenRemoveTagTypeOnGroup() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Roaming Icon");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.fillTagType("Payment");
		screenItemLibrary.clickOnButtonSave();
		screenItemLibrary.removeTagOrRegion("Payment");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Tag Types:"));
		description = description.split("Tag Types:")[1];
		String descriptionBefore = description.split("-->")[0];
		String descriptionAfter = description.split("-->")[1];
		assertTrue(descriptionBefore.contains("Payment"));
		assertFalse(descriptionAfter.contains("Payment"));
	}

	/**
	 * @testLinkId T-GPRI-3047
	 * @summary When add Check Procedure on group
	 */
	@Test
	public void test_1350_WhenAddCheckProcedureOnGroup() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Roaming Icon");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.selectCheckProcedure();
		screenItemLibrary.fillCheckProcedure("Test");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);
		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();

		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Check Procedure Changed:"));
		description = description.split("-->")[1];
		assertTrue(description.contains("New"));
	}

	/**
	 * @testLinkId T-GPRI-3048
	 * @summary When remove Check Procedure on group
	 */
	@Test
	public void test_1351_WhenRemoveCheckProcedureOnGroup() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenChangeLog screenChangeLog = new ScreenChangeLog(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Roaming Icon");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.selectCheckProcedure();
		screenItemLibrary.fillCheckProcedure("New Test");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_ROAMING_ICON);
		screenItemLibrary.selectOptionRightClick("Change Log");
		screenItemLibrary.switchTabs(1);

		String date = screenChangeLog.getDateFirstLog();
		String login = screenChangeLog.getLoginFirstLog();
		String description = screenChangeLog.getDescriptionFirstLog();
		Date currentDate = new Date();
		String DATE_FORMAT = ConstantsItemLibrary.DD_MM_YYYY_HH_MM_SS_A;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String format = formatter.format(currentDate);

		String formatedDate;
		if (format.contains("AM")) {
			String day = format.split("/")[0];
			int today = Integer.valueOf(day) - 1;
			format = format.replaceAll(format.split("/")[0], String.valueOf(today));
			String hour = format.split(" ")[1];
			int pmHour = Integer.valueOf(hour.split(":")[0]) + 12;
			hour = hour.replaceAll(hour.split(":")[0], String.valueOf(pmHour));
			format = format.split(" ")[0] + " " + hour;
		}
		formatedDate = format.split(" ")[0];

		getDriver().close();
		screenItemLibrary.switchTabs(0);
		assertTrue(date.contains(formatedDate));
		assertTrue(login.contains("yara.martins (LGESP R&D MC SW)"));
		assertTrue(description.contains("Check Procedure Changed: Old --> New"));
	}

}