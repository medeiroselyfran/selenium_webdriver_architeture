package com.lge.gpri.tests.itemLibrary;

import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;

import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Filter in Item Library.
*  
* @author AUTHOR NAME (can exist more than one) 
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestFilter extends AbstractTestScenary{

	/**
	 * @testLinkId T-GPRI-3049
	 * @summary Add All Filters
	 */
	@Test
	public void test_1398_AddAllFilters() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		assertTrue(screenItemLibrary.selecOptionAll("Select All"));
	}
	
	/**
	 * @testLinkId T-GPRI-3050
	 * @summary Add All Filters
	 */
	@Test
	public void test_1408_RemoveAllFilters() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		assertFalse(screenItemLibrary.selecOptionAll("Deselect All"));
	}
	
	/**
	* @testLinkId T-GPRI-3051
	* @summary With Default Hide on Mother
	*/
	@Test
	public void test_1409_WithDefaultHideOnMother() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.selectSomeFilter("Default hidden on Mother");
		assertTrue(screenItemLibrary.setInFilterValue("Default Hide on Mother","Yes"));
	}
	
	/**
	* @testLinkId T-GPRI-3052
	* @summary Without Default Hide on Mother
	*/
	@Test
	public void test_1411_WithoutDefaultHideOnMother() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.selectSomeFilter("Default hidden on Mother");
		assertTrue(screenItemLibrary.setInFilterValue("Default Hide on Mother","No"));
	}
	
	/**
	* @testLinkId T-GPRI-3053
	* @summary Filter SmartCA Types with option None checked
	*/
	@Test
	public void test_1414_FilterSmartCATypesWithOptionNoneChecked() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.selectSomeFilter("SmartCA Types");
		assertTrue(screenItemLibrary.setInFilterValue("SmartCA Types","None"));
	}
	
	/**
	* @testLinkId T-GPRI-3054
	* @summary Filter SmartCA Types with option Enabler checked
	*/
	@Test
	public void test_1416_FilterSmartCATypesWithOptionEnablerChecked() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.selectSomeFilter("SmartCA Types");
		assertTrue(screenItemLibrary.setInFilterValue("SmartCA Types","Enabler"));
	}
	
	/**
	* @testLinkId T-GPRI-3055
	* @summary Filter SmartCA Types with option Customizer checked
	*/
	@Test
	public void test_1419_FilterSmartCATypesWithOptionCustomizerChecked() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.selectSomeFilter("SmartCA Types");
		assertTrue(screenItemLibrary.setInFilterValue("SmartCA Types","Customizer"));
	}
	
	/**
	* @testLinkId T-GPRI-3056
	* @summary Filter SmartCA Types with all options selected
	*/
	@Test
	public void test_1420_FilterSmartCATypesWithAllOptionsSelected() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.selectSomeFilter("SmartCA Types");
		assertTrue(screenItemLibrary.selectAllOrDeselectOnFilter("SmartCA Types","Select All"));
	}
	
	/**
	* @testLinkId T-GPRI-3057
	* @summary Filter Smart CA Types with Customizer and Enabler selected
	*/
	@Test
	public void test_1421_FilterSmartCATypesWithCustomizerAndEnablerSelected() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.selectSomeFilter("SmartCA Types");
		assertTrue(screenItemLibrary.setInFilterValue("SmartCA Types","Enabler"));
		assertTrue(screenItemLibrary.setInFilterValue("SmartCA Types","Customizer"));
		
	}
	
	/**
	* @testLinkId T-GPRI-3058
	* @summary Validate filter
	*/
	@Test
	public void test_1424_ValidateFilter() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.selectSomeFilter("Auto Commit");
		screenItemLibrary.selectSomeFilter("Tag Values");
		assertTrue(screenItemLibrary.verifyOptionInTree("Auto Commit"));
		assertTrue(screenItemLibrary.verifyOptionInTree("Tag Values"));
	}
	
}
