package com.lge.gpri.tests.itemLibrary;

import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;

import com.lge.gpri.helpers.constants.ConstantsItemLibrary;
import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Property SmartCA in Item Library.
*  
* @author AUTHOR NAME (can exist more than one) 
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPropertySmartCA extends AbstractTestScenary{
	/**
	* @testLinkId T-GPRI-3059
	* @summary Added property option Customizer in SmartCA Types
	*/
	@Test
	public void test_1428_AddedPropertyOptionCustomizerInSmartCATypes() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_ITEM_NAME);
		screenItemLibrary.selectSmartCAType("Customizer");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-3060
	* @summary Added property option Enabler in SmartCA Types
	*/
	@Test
	public void test_1429_AddedPropertyOptionEnablerInSmartCATypes() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Name");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_ITEM_NAME);
		screenItemLibrary.selectSmartCAType("Enabler");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-3061
	* @summary Added property option None in SmartCA Types
	*/
	@Test
	public void test_1430_AddedPropertyOptionNoneInSmartCATypes() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Name");
		screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_ITEM_NAME);
		screenItemLibrary.selectSmartCAType("None");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
}