package com.lge.gpri.tests.itemLibrary;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsItemLibrary;
import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Search Of Tree in Item Library.
*  
* @author AUTHOR NAME (can exist more than one) 
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSearchOfTree extends AbstractTestScenary{
	/**
	* @testLinkId T-GPRI-3062
	* @summary Valid search
	*/
	@Test
	public void test_1471_ValidSearch() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch(ConstantsItemLibrary.ITEM_LTE_MODE);
		assertTrue(screenItemLibrary.itemIsHighlightedInTree(ConstantsItemLibrary.PATH_ITEM_LTE_MODE));
	}
	
	/**
	* @testLinkId T-GPRI-3063
	* @summary Invalid search
	*/
	@Test
	public void test_1472_InvalidSearch() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Testtttttt");
		assertFalse(screenItemLibrary.itemIsHighlightedInTree("Parameters::Network::Testtttttt"));
	}
}
