package com.lge.gpri.tests.itemLibrary;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsCompare;
import com.lge.gpri.helpers.constants.ConstantsItemLibrary;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;
import com.lge.gpri.screens.itemLibrary.ScreenTags;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Management Item.
*  
* @author AUTHOR NAME (can exist more than one) 
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestManagementItem extends AbstractTestScenary{
	/**
	* @testLinkId T-GPRI-2978
	* @summary CREATE A NEW ITEM
	*/
	@Test
	public void test_1448_CreateNewItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.rightClickOnNetwork();
		screenItemLibrary.verifyOptionsInScreen("New Group");
		screenItemLibrary.verifyOptionsInScreen("Rename");
		screenItemLibrary.verifyOptionsInScreen("Delete");
		screenItemLibrary.verifyOptionsInScreen("Copy");
		screenItemLibrary.verifyOptionsInScreen("Paste");
		screenItemLibrary.verifyOptionsInScreen("Copy Path");
		screenItemLibrary.verifyOptionsInScreen("Export Excel");
		screenItemLibrary.verifyOptionsInScreen("Change log");
		screenItemLibrary.verifyOptionsInScreen("New Item");
		assertTrue(screenItemLibrary.clickOnNewItem());
	}
	
	/**
	* @testLinkId T-GPRI-2979
	* @summary Check item with Multiple
	*/
	@Test
	public void test_1311_CheckItemWithMultiple() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch(ConstantsPackages.SHOW_PLMN);
		screenItemLibrary.selectItemInTree(ConstantsPackages.PATH_SHOW_PLMN);
		screenItemLibrary.selectOption("Multiple");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.openPage();
		sectionPerformMenu.clickOnMenuPackage();
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		screenPerformListPackages.setModel(ConstantsItemLibrary.LGH815);
		screenPerformListPackages.setBuyer(ConstantsItemLibrary.TIM_BRAZIL_BTM);
		
		boolean resultOk = screenPerformListPackages.clickOnEditProfilePackageButton();
		if(!resultOk){
			screenPerformListPackages.rejectGeneratingAPK(ConstantsItemLibrary.TIM_BRAZIL_BTM);
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.SHOW_PLMN);
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		assertTrue(screenPerformEditPackageProfile.verifyExistsElementInParameters(ConstantsPackages.SHOW_PLMN));
	}
	
	/**
	* @testLinkId T-GPRI-2980
	* @summary Uncheck item with Multipe
	*/
	@Test
	public void test_1312_UncheckItemWithMultipe() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch(ConstantsPackages.SHOW_PLMN);
		screenItemLibrary.selectItemInTree(ConstantsPackages.PATH_SHOW_PLMN);
		screenItemLibrary.unselectOption("Multiple");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.openPage();
		sectionPerformMenu.clickOnMenuPackage();
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		screenPerformListPackages.setModel(ConstantsItemLibrary.LGH815);
		screenPerformListPackages.setBuyer(ConstantsItemLibrary.TIM_BRAZIL_BTM);
		
		boolean resultOk = screenPerformListPackages.clickOnEditProfilePackageButton();
		if(!resultOk){
			screenPerformListPackages.rejectGeneratingAPK(ConstantsItemLibrary.TIM_BRAZIL_BTM);
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.SHOW_PLMN);
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		assertTrue(screenPerformEditPackageProfile.verifyExistsElementInParameters(ConstantsPackages.SHOW_PLMN));
	}
	
	/**
	* @testLinkId T-GPRI-2981
	* @summary Check item with From app Pool
	*/
	@Test
	public void test_1313_CheckItemWithFromAppPool() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch(ConstantsPackages.LTE_MODE);
		screenItemLibrary.selectItemInTree("Parameters::Network::LTE Mode");
		screenItemLibrary.selectOption("From App Pool");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.openPage();
		sectionPerformMenu.clickOnMenuPackage();
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		screenPerformListPackages.setModel(ConstantsItemLibrary.LGH815);
		screenPerformListPackages.setBuyer(ConstantsItemLibrary.TIM_BRAZIL_BTM);

		boolean resultOk = screenPerformListPackages.clickOnEditProfilePackageButton();
		if(!resultOk){
			screenPerformListPackages.rejectGeneratingAPK(ConstantsItemLibrary.TIM_BRAZIL_BTM);
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.LTE_MODE);
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		screenPerformEditPackageProfile.clickOnAndroid(ConstantsPackages.LTE_MODE);
		screenPerformEditPackageProfile.searchApp("");
		screenPerformEditPackageProfile.searchApp("Instagram");
		screenPerformEditPackageProfile.clickOnInstagramApp();
		screenPerformEditPackageProfile.clickOnSaveAppsButton();
		screenPerformEditPackageProfile.waitLoading();
	}

	/**
	* @testLinkId T-GPRI-2982
	* @summary Uncheck item with From app Pool
	*/
	@Test
	public void test_1314_UncheckItemWithFromAppPool() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch(ConstantsPackages.LTE_MODE);
		screenItemLibrary.selectItemInTree("Parameters::Network::LTE Mode");
		screenItemLibrary.unselectOption("From App Pool");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.openPage();
		sectionPerformMenu.clickOnMenuPackage();
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		screenPerformListPackages.setModel(ConstantsItemLibrary.LGH815);
		screenPerformListPackages.setBuyer(ConstantsItemLibrary.TIM_BRAZIL_BTM);
		
		boolean resultOk = screenPerformListPackages.clickOnEditProfilePackageButton();
		if(!resultOk){
			screenPerformListPackages.rejectGeneratingAPK(ConstantsItemLibrary.TIM_BRAZIL_BTM);
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.LTE_MODE);
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		assertTrue(screenPerformEditPackageProfile.verifyExistsElementInParameters(ConstantsPackages.LTE_MODE));
	}
	
	/**
	* @testLinkId T-GPRI-2983
	* @summary Check Mandatory for Group
	*/
	@Test
	public void test_1449_CheckMandatoryForGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch(ConstantsPackages.HOME_NETWORK);
		screenItemLibrary.selectItemInTree(ConstantsPackages.PATH_HOME_NETWORK);
		screenItemLibrary.selectOption("Mandatory for Group");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.openPage();
		sectionPerformMenu.clickOnMenuPackage();
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		screenPerformListPackages.setModel(ConstantsItemLibrary.LGH815);
		screenPerformListPackages.setBuyer(ConstantsItemLibrary.OI_BRAZIL_BOI);
		boolean resultOk = screenPerformListPackages.clickOnEditProfilePackageButton();
		if(!resultOk){
			screenPerformListPackages.rejectGeneratingAPK(ConstantsItemLibrary.OI_BRAZIL_BOI);
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.removeItem("idItemHomeNetWork");
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch(ConstantsPackages.HOME_NETWORK);
		screenItemLibrary.selectItemInTree(ConstantsPackages.PATH_HOME_NETWORK);
		screenItemLibrary.unselectOption("Mandatory for Group");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-2984
	* @summary Uncheck Mandatory for Group
	*/
	@Test
	public void test_1451_UncheckMandatoryForGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch(ConstantsPackages.HOME_NETWORK);
		screenItemLibrary.selectItemInTree(ConstantsPackages.PATH_HOME_NETWORK);
		screenItemLibrary.unselectOption("Mandatory for Group");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		screenItemLibrary.openPage();
		sectionPerformMenu.clickOnMenuPackage();
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		screenPerformListPackages.setModel(ConstantsItemLibrary.LGH815);
		screenPerformListPackages.setBuyer(ConstantsItemLibrary.TIM_BRAZIL_BTM);
		
		boolean resultOk = screenPerformListPackages.clickOnEditProfilePackageButton();
		if(!resultOk){
			screenPerformListPackages.rejectGeneratingAPK(ConstantsItemLibrary.TIM_BRAZIL_BTM);
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.removeItem("idItemHomeNetWork");
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
	}
	
	/**
	* @testLinkId T-GPRI-2985
	* @summary Check Mandatory for Buyer Child
	*/
	@Test
	public void test_1452_CheckMandatoryForBuyerChild() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch(ConstantsPackages.HOME_NETWORK);
		screenItemLibrary.selectItemInTree(ConstantsPackages.PATH_HOME_NETWORK);
		screenItemLibrary.selectOption("Mandatory for Buyer Child");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	    sectionPerformMenu.clickOnItemLibrary();
	    sectionPerformMenu.clickOnMenuBuyer();
	    ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
	    ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
	    screenPerformListBuyers.addTypeFilter();
	    screenPerformListBuyers.setTypeFilter("Child");
	    screenPerformListBuyers.clickOnEditProfile("3AE");
	    String msg = screenPerformEditBuyerProfile.getErrorMsg("Missing Buyer Child Mandatory items");
	    String exitsElement;
	    if(!(msg.equals("This buyer has buyer child mandatory items missing. Missing entries will be inserted automatically."))){
	    	exitsElement = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
	    	assertNotEquals(exitsElement,"");
	    	screenPerformEditBuyerProfile.clickOnSelectItemsButton();
	    	screenPerformEditBuyerProfile.addItemByPath("Parameters::Network::Home Network");
	    	screenPerformEditBuyerProfile.removeItemByPath("Parameters::Network::Home Network");
	    	screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
	    	if(!screenPerformEditBuyerProfile.clickOnSaveProfile())
	    		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
	    	msg = screenPerformEditBuyerProfile.getErrorMsg("Missing Buyer Child Mandatory items");
	    	assertEquals(msg,"This buyer has buyer child mandatory items missing. Missing entries will be inserted automatically.");
	    	screenPerformEditBuyerProfile.closeErrorMsg("Missing Buyer Child Mandatory items");
	    	assertTrue(screenPerformEditBuyerProfile.verifyExistsElementInParameters(ConstantsCompare.HOME_NETWORK));
	    }else{
	    	assertEquals(msg,"This buyer has buyer child mandatory items missing. Missing entries will be inserted automatically.");
	    	screenPerformEditBuyerProfile.closeErrorMsg("Missing Buyer Child Mandatory items");
	    	screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, ConstantsCompare.HOME_NETWORK_724_32);
	    	screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
	    	assertTrue(screenPerformEditBuyerProfile.verifyExistsElementInParameters(ConstantsCompare.HOME_NETWORK));
	    }
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch(ConstantsPackages.HOME_NETWORK);
		screenItemLibrary.selectItemInTree(ConstantsPackages.PATH_HOME_NETWORK);
		screenItemLibrary.unselectOption("Mandatory for Buyer Child");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-2986
	* @summary Uncheck Mandatory for Buyer Child
	*/
	@Test
	public void test_1454_UncheckMandatoryForBuyerChild() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch(ConstantsPackages.HOME_NETWORK);
		screenItemLibrary.selectItemInTree(ConstantsPackages.PATH_HOME_NETWORK);
		screenItemLibrary.unselectOption("Mandatory for Buyer Child");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	    sectionPerformMenu.clickOnItemLibrary();
	    sectionPerformMenu.clickOnMenuBuyer();
	    ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
	    ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
	    screenPerformListBuyers.addTypeFilter();
	    screenPerformListBuyers.setTypeFilter("Child");
	    screenPerformListBuyers.clickOnEditProfile("3AE");
	    
	    String msg;
	    
    	screenPerformEditBuyerProfile.verifyIfPackageHasItemsAndClickToSelectItems();
    	screenPerformEditBuyerProfile.addItemByPath("Parameters::Network::Home Network");
    	screenPerformEditBuyerProfile.removeItemByPath("Parameters::Network::Home Network");
    	screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
    	if(!screenPerformEditBuyerProfile.clickOnSaveProfile())
    		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
    	msg = screenPerformEditBuyerProfile.getErrorMsg("Missing Buyer Child Mandatory items");
    	assertNotEquals(msg,"This buyer has buyer child mandatory items missing. Missing entries will be inserted automatically.");
    	screenPerformEditBuyerProfile.closeErrorMsg("Missing Buyer Child Mandatory items");
    	assertFalse(screenPerformEditBuyerProfile.verifyExistsElementInParameters(ConstantsCompare.HOME_NETWORK));
	}
	
	/**
	* @testLinkId T-GPRI-2987
	* @summary Check Default Hide on Mother
	*/
	@Test
	public void test_1455_CheckDefaultHideOnMother() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree("Parameters::Network::iaAPN ProtocolType");
		screenItemLibrary.selectOption("Default Hide On Mother");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	    sectionPerformMenu.clickOnMenuBuyer();
	    ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
	    ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
	    
	    screenPerformListBuyers.clickOnEditProfile("3AE");
	    screenPerformEditBuyerProfile.verifyIfPackageHasItemsAndClickToSelectItems();
	    screenPerformEditBuyerProfile.addItemByPath("Parameters::Network::iaAPN ProtocolType");
	    screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
	    screenPerformEditBuyerProfile.clickOnHideOnMother("iaAPN ProtocolType");
	    assertTrue(screenPerformEditBuyerProfile.verifyCheckBoxInHideOnMother("iaAPN ProtocolType"));
	    
	}
	
	/**
	* @testLinkId T-GPRI-2988
	* @summary Uncheck Default Hide on Mother
	*/
	@Test
	public void test_1456_UncheckDefaultHideOnMother() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree("Parameters::Network::iaAPN ProtocolType");
		screenItemLibrary.unselectOption("Default Hide On Mother");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	    sectionPerformMenu.clickOnMenuBuyer();
	    ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
	    ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
	    
	    screenPerformListBuyers.clickOnEditProfile("3AE");
	    screenPerformEditBuyerProfile.verifyIfPackageHasItemsAndClickToSelectItems();
	    screenPerformEditBuyerProfile.addItemByPath("Parameters::Network::iaAPN ProtocolType");
	    screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
	    screenPerformEditBuyerProfile.clickOnHideOnMother("iaAPN ProtocolType");
	    assertFalse(screenPerformEditBuyerProfile.verifyCheckBoxInHideOnMother("iaAPN ProtocolType"));
	    
	}
	
	/**
	* @testLinkId T-GPRI-2989
	* @summary Uncheck Allow Multiple Files
	*/
	@Test
	public void test_1458_UncheckAllowMultipleFiles() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Country Code");
		screenItemLibrary.selectItemInTree("Parameters::Network::Country Code");
		screenItemLibrary.unselectOption("Allow Multiple Files");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	    sectionPerformMenu.clickOnMenuBuyer();
	    ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
	    ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
	    
	    screenPerformListBuyers.clickOnEditProfile("3AE");
	    screenPerformEditBuyerProfile.verifyIfPackageHasItemsAndClickToSelectItems();
	    screenPerformEditBuyerProfile.addItemByPath("Parameters::Network::Country Code");
	    screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
	    assertTrue(screenPerformEditBuyerProfile.verifyPresenceUploadFile("Country Code"));
	}
	
	/**
	* @testLinkId T-GPRI-2990
	* @summary check Allow Multiple Files
	*/
	@Test
	public void test_1457_CheckAllowMultipleFiles() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Country Code");
		screenItemLibrary.selectItemInTree("Parameters::Network::Country Code");
		screenItemLibrary.selectOption("Allow Multiple Files");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	    sectionPerformMenu.clickOnMenuBuyer();
	    ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
	    ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
	    
	    screenPerformListBuyers.clickOnEditProfile("3AE");
	    screenPerformEditBuyerProfile.verifyIfPackageHasItemsAndClickToSelectItems();
	    screenPerformEditBuyerProfile.addItemByPath("Parameters::Network::Country Code");
	    screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
	    assertTrue(screenPerformEditBuyerProfile.verifyPresenceUploadFile("Country Code"));
	    //alterando o assert para o teste ser válido.
	}
	
	/**
	* @testLinkId T-GPRI-2991
	* @summary Add A Valid Tag Type On Item
	*/
	@Test
	public void test_1315_AddAValidTagTypeOnItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree("Parameters::Network::iaAPN ProtocolType");
		screenItemLibrary.fillTagType("Payment");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	    sectionPerformMenu.clickOnMenuBuyer();
	    ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
	    ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
	    
	    screenPerformListBuyers.clickOnEditProfile("3AE");
	    screenPerformEditBuyerProfile.verifyIfPackageHasItemsAndClickToSelectItems();
	    screenPerformEditBuyerProfile.addItemByPath("Parameters::Network::iaAPN ProtocolType");
	    screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
	    assertTrue(screenPerformEditBuyerProfile.verifyOptionsTagType("iaAPN ProtocolType","Payment"));
	}
	
	/**
	* @testLinkId T-GPRI-2992
	* @summary Add A Invalid Tag Type On Item
	*/
	@Test
	public void test_1465_AddAInvalidTagTypeOnItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree("Parameters::Network::iaAPN ProtocolType");
		screenItemLibrary.fillTagType("Paymenty");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	    sectionPerformMenu.clickOnMenuBuyer();
	    ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
	    ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
	    
	    screenPerformListBuyers.clickOnEditProfile("3AE");
	    screenPerformEditBuyerProfile.verifyIfPackageHasItemsAndClickToSelectItems();
	    screenPerformEditBuyerProfile.addItemByPath("Parameters::Network::iaAPN ProtocolType");
	    screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
	    assertFalse(screenPerformEditBuyerProfile.verifyOptionsTagType("iaAPN ProtocolType","Paymenty"));
	}
	
	/**
	* @testLinkId T-GPRI-2993
	* @summary Remove Tag Type on item
	*/
	@Test
	public void test_1316_RemoveTagTypeOnItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());

		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("iaAPN ProtocolType");
		screenItemLibrary.selectItemInTree("Parameters::Network::iaAPN ProtocolType");
		screenItemLibrary.removeTagOrRegion("Payment");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		sectionPerformMenu.clickOnMenuBuyer();
		
	    ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
	    ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
	    
	    screenPerformListBuyers.clickOnEditProfile("3AE");
	    screenPerformEditBuyerProfile.verifyIfPackageHasItemsAndClickToSelectItems();
	    screenPerformEditBuyerProfile.addItemByPath("Parameters::Network::iaAPN ProtocolType");
	    screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
	    assertFalse(screenPerformEditBuyerProfile.verifyOptionsTagType("iaAPN ProtocolType","Payment"));
	}
	
	/**
	* @testLinkId T-GPRI-2994
	* @summary Add New Tag on item
	*/
	@Test
	public void test_1464_AddNewTagOnItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		sectionPerformMenu.clickOnItemLibrary();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.fillTagType("Payment");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		sectionPerformMenu.clickOnTags();
		ScreenTags screenTags = new ScreenTags(getDriver());
		screenTags.addNewTagType("testb");
		screenTags.clickOnElementInList("testb");
		screenTags.addValueInNewTag("test2","test1");
		screenTags.clickOnSave();
		sectionPerformMenu.clickOnItemLibrary();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.fillTagType("test2");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}

	/**
	* @testLinkId T-GPRI-2995
	* @summary Add a valid Tag Value
	*/
	@Test
	public void test_1317_AddAValidTagValue() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		screenItemLibrary.openPage();
		sectionPerformMenu.clickOnItemLibrary();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.fillTagValue("Android Version 5.0");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-2996
	* @summary Add a invalid Tag Value
	*/
	@Test
	public void test_1466_AddAInvalidTagValue() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.fillTagValue("Android Version 9.0");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-2997
	* @summary Remove Tag Value
	*/
	@Test
	public void test_1318_RemoveTagValue() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.removeTagOrRegion("Android Version 5.0");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-2998
	* @summary Add Region on item
	*/
	@Test
	public void test_1319_AddRegionOnItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.fillRegion("CANADA");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-2999
	* @summary Add invalid Region on item
	*/
	@Test
	public void test_1467_AddInvalidRegionOnItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.fillRegion("KANADA");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-3000
	* @summary Remove Region on item
	*/
	@Test
	public void test_1320_RemoveRegionOnItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.removeTagOrRegion("CANADA");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}	
	
	
	/**
	* @testLinkId T-GPRI-3001
	* @summary Add Check Procedure on item
	*/
	@Test
	public void test_1321_AddCheckProcedureOnItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.selectCheckProcedure();
		screenItemLibrary.fillCheckProcedure("Test");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}

	/**
	* @testLinkId T-GPRI-3002
	* @summary Edit Check Procedure on item
	*/
	@Test
	public void test_1322_EditCheckProcedureOnItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.selectCheckProcedure();
		screenItemLibrary.fillCheckProcedure("New Test");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-3003
	* @summary Add Reviewer valid
	*/
	@Test
	public void test_1323_AddReviewerValid() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.clickOnTabReviewers();
		screenItemLibrary.clickOnSelectReviewers();
		screenItemLibrary.selectReviewerItem("junho.roh");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-3004
	* @summary Search invalid Reviewer
	*/
	@Test
	public void test_1468_SearchInvalidReviewer() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.clickOnTabReviewers();
		screenItemLibrary.clickOnSelectReviewers();
		screenItemLibrary.selectReviewerItem("junho.rohs");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-3005
	* @summary Remove Reviewer valid
	*/
	@Test
	public void test_1324_RemoveReviewerValid() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Home Network");
		screenItemLibrary.selectItemInTree("Parameters::Network::Home Network");
		screenItemLibrary.clickOnTabReviewers();
		screenItemLibrary.removeReviewer("junho.roh");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}

	/**
	* @testLinkId T-GPRI-3006
	* @summary Rename Item
	*/
	@Test
	public void test_1459_RenameItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("SVN Increment");
		if(screenItemLibrary.clickRightOnItem("Parameters::Buyer Specific Requirements::SVN Increment")){
			screenItemLibrary.selectOptionRightClick("Rename");
			assertTrue(screenItemLibrary.renameItemForAnExistingName("SVN Increment New Name"));
		}else{
			screenItemLibrary.putTextOnFieldSearch("SVN Increment New Name");
			screenItemLibrary.clickRightOnItem("Parameters::Buyer Specific Requirements::SVN Increment New Name");
			screenItemLibrary.selectOptionRightClick("Rename");
			assertTrue(screenItemLibrary.renameItemForAnExistingName("SVN Increment"));
		}
	}
	
	/**
	* @testLinkId T-GPRI-3007
	* @summary Rename Item with same item for another item
	*/
	@Test
	public void test_1460_RenameItemWithSameItemForAnotherItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		boolean renameItemForAnExistingName = false;
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("UMTS");
		if(screenItemLibrary.clickRightOnItem("Parameters::Network::Frequency Bands::UMTS")){
			screenItemLibrary.selectOptionRightClick("Rename");
			renameItemForAnExistingName = screenItemLibrary.renameItemUsingAnExistingName("LTE", " Invalid Name");
		}else{
			screenItemLibrary.clickRightOnItem("Parameters::Network::Frequency Bands::UMTS New Name");
			screenItemLibrary.selectOptionRightClick("Rename");
			renameItemForAnExistingName = screenItemLibrary.renameItemUsingAnExistingName("LTE", " Invalid Name");
		}
		assertTrue(renameItemForAnExistingName);
	}
	
	
	/**
	* @testLinkId T-GPRI-3008
	* @summary Remove Item
	*/
	@Test
	public void test_1461_RemoveItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.clickRightOnItem("Parameters::Network");
		screenItemLibrary.selectOptionRightClick("New Item");
		screenItemLibrary.renameItemForAnExistingName("aaaaaaa");
		screenItemLibrary.clickRightOnItem("Parameters::Network::aaaaaaa");
		screenItemLibrary.selectOptionRightClick("Delete");
		assertTrue(screenItemLibrary.confirmDelete());
	}
	
	/**
	* @testLinkId T-GPRI-3009
	* @summary Copy Path of Item
	*/
	@Test
	public void test_1462_CopyPathOfItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("HPLMN Timer Time");
		screenItemLibrary.clickRightOnItem("Parameters::Network::HPLMN Timer Time");
		screenItemLibrary.selectOptionRightClick("Copy Path");
		assertTrue(screenItemLibrary.copyPath("Parameters::Network::HPLMN Timer Time"));
	}
	
	/**
	* @testLinkId T-GPRI-3010
	* @summary Add valid link on an item
	*/
	@Test
	public void test_1469_AddValidLinkOnAnItem() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("HPLMN Timer Time");
		screenItemLibrary.selectItemInTree("Parameters::Network::HPLMN Timer Time");
		screenItemLibrary.clickOnAbaLink();
		screenItemLibrary.addLinkInAbaLink("Customization::Applications and Games::Games::Feature Phone::Language");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId T-GPRI-3011
	* @summary Remove a Link
	*/
	@Test
	public void test_1470_RemoveALink() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("HPLMN Timer Time");
		screenItemLibrary.selectItemInTree("Parameters::Network::HPLMN Timer Time");
		screenItemLibrary.clickOnAbaLink();
		if(!screenItemLibrary.removeLinks("Customization::Applications and Games::Games::Feature Phone::Language")){
			screenItemLibrary.addLinkInAbaLink("Customization::Applications and Games::Games::Feature Phone::Language");
			screenItemLibrary.removeLinks("Customization::Applications and Games::Games::Feature Phone::Language");
		}
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
}
