package com.lge.gpri.tests.itemLibrary;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsItemLibrary;
import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* <pre>
* Author: great
* Purpose: <Type here the purpose of the class> - great - 28 de set de 2017
* History: Class creation - great - 28 de set de 2017
*
*</pre>
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestManagementGroup extends AbstractTestScenary{
	/**
	* @testLinkId  T-GPRI-3012
	* @summary Create New Group with setting
	*/
	@Test
	public void test_1361_CreateNewGroupWithSettings() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.rightClickOnNetwork();
		screenItemLibrary.selectOptionOnFolderRightClick("New Group");
		screenItemLibrary.renameItemForAnExistingName("TestGroup");
		screenItemLibrary.selectItemInTree("Parameters::Network::TestGroup");
		screenItemLibrary.selectOptionFolder("Multiple");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId  T-GPRI-3013
	* @summary Create New Group without setting
	*/
	@Test
	public void test_1362_CreateNewGroupWithoutSetting() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.rightClickOnNetwork();
		screenItemLibrary.selectOptionOnFolderRightClick("New Group");
		if(!screenItemLibrary.renameItemForAnExistingName("TestGroupB")){
			screenItemLibrary.selectItemInTree("Parameters::Network::TestGroupB");
			screenItemLibrary.selectOptionOnFolderRightClick("Delete");
			screenItemLibrary.confirmDelete();
			screenItemLibrary.rightClickOnNetwork();
			screenItemLibrary.selectOptionOnFolderRightClick("New Group");
			screenItemLibrary.renameItemForAnExistingName("TestGroupB");
		}else{
			screenItemLibrary.clickRightOnItem("Parameters::Network::TestGroupB");
			screenItemLibrary.selectOptionOnFolderRightClick("Delete");
			screenItemLibrary.confirmDelete();
			screenItemLibrary.rightClickOnNetwork();
			screenItemLibrary.selectOptionOnFolderRightClick("New Group");
			screenItemLibrary.renameItemForAnExistingName("TestGroupB");
		}
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId  T-GPRI-3014
	* @summary Setting of group
	*/
	@Test
	public void test_1363_SettingOfGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("New Group");
		if(!screenItemLibrary.selectItemInTree("Parameters::Network::New Group Test")){
			screenItemLibrary.rightClickOnNetwork();
			screenItemLibrary.selectOptionOnFolderRightClick("New Group");
			screenItemLibrary.renameItemForAnExistingName("New Group Test");
			screenItemLibrary.clickOnButtonSave();
			screenItemLibrary.selectItemInTree("Parameters::Network::New Group Test");
		}
		screenItemLibrary.selectOptionFolder("Multiple");
		screenItemLibrary.selectOptionFolder("COTA");
		screenItemLibrary.fillTagType("Android Version");
		screenItemLibrary.fillTagValue("Android Version");
		screenItemLibrary.fillRegion("USA");
		screenItemLibrary.selectCheckProcedure();
		screenItemLibrary.fillCheckProcedure("Teste");
		screenItemLibrary.clickOnTabReviewers();
		screenItemLibrary.clickOnSelectReviewers();
		screenItemLibrary.selectReviewerFolder("Alex.ahn");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId  T-GPRI-3015
	* @summary Check group with Multipe
	*/
	@Test
	public void test_1325_CheckGroupWithMultipe() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Frequency Bands");
		screenItemLibrary.selectItemInTree("Parameters::Network::Frequency Bands");
		screenItemLibrary.selectOptionFolder("Multiple");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		sectionPerformMenu.clickOnMenuPackage();

		screenPerformListPackages.setModel(ConstantsItemLibrary.LGH815);
		screenPerformListPackages.setBuyer(ConstantsItemLibrary.TIM_BRAZIL_BTM);
		if(screenPerformListPackages.verifyIsStatusUnderEdition()){
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.selectItemInSelectItemsTree("Frequency Bands");
			assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		}else{
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.selectItemInSelectItemsTree("Frequency Bands");
			assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		}
	}
	
	/**
	* @testLinkId  T-GPRI-3016
	* @summary Uncheck group with Multipe
	*/
	@Test
	public void test_1326_UncheckGroupWithMultipe() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Frequency Bands");
		screenItemLibrary.selectItemInTree("Parameters::Network::Frequency Bands");
		screenItemLibrary.unselectOptionFolder("Multiple");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		sectionPerformMenu.clickOnMenuPackage();

		screenPerformListPackages.setModel(ConstantsItemLibrary.LGH815);
		screenPerformListPackages.setBuyer(ConstantsItemLibrary.TIM_BRAZIL_BTM);
		if(screenPerformListPackages.verifyIsStatusUnderEdition()){
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.unselectItemInSelectItemsTree("Frequency Bands");
			assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		}else{
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.unselectItemInSelectItemsTree("Frequency Bands");
			assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		}
	}
	
	/**
	* @testLinkId  T-GPRI-3017
	* @summary Add a valid Tag Type on group
	*/
	@Test
	public void test_1327_AddAValidTagTypeOnGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Carrier Aggregation");
		screenItemLibrary.selectItemInTree("Parameters::Network::Carrier Aggregation");
		screenItemLibrary.fillTagType("Payment");
		assertTrue(screenItemLibrary.clickOnButtonSave());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		sectionPerformMenu.clickOnMenuPackage();

		screenPerformListPackages.setModel(ConstantsItemLibrary.LGH815);
		screenPerformListPackages.setBuyer(ConstantsItemLibrary.TIM_BRAZIL_BTM);
		if(screenPerformListPackages.verifyIsStatusUnderEdition()){
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.selectItemInSelectItemsTree("Carrier Aggregation");
			assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		}else{
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.selectItemInSelectItemsTree("Carrier Aggregation");
			assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		}
	}
	
	/**
	* @testLinkId  T-GPRI-3018
	* @summary Remove Add Tag Type on group
	*/
	@Test
	public void test_1328_RemoveTagTypeOnGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Carrier Aggregation");
		screenItemLibrary.selectItemInTree("Parameters::Network::Carrier Aggregation");
		if(screenItemLibrary.verifyExistTagType("Payment"))
			screenItemLibrary.removeTagOrRegion("Payment");
		else{
			screenItemLibrary.fillTagType("Payment");
			screenItemLibrary.clickOnButtonSave();
			screenItemLibrary.removeTagOrRegion("Payment");
		}
		assertTrue(screenItemLibrary.clickOnButtonSave());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		sectionPerformMenu.clickOnMenuPackage();

		screenPerformListPackages.setModel(ConstantsItemLibrary.LGH815);
		screenPerformListPackages.setBuyer(ConstantsItemLibrary.TIM_BRAZIL_BTM);
		if(screenPerformListPackages.verifyIsStatusUnderEdition()){
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.selectItemInSelectItemsTree("Carrier Aggregation");
			assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		}else{
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.selectItemInSelectItemsTree("Carrier Aggregation");
			assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		}
	}
	
	/**
	* @testLinkId  T-GPRI-3019
	* @summary Add Check Procedure on groupp
	*/
	@Test
	public void test_1329_AddCheckProcedureOnGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Carrier Aggregation");
		screenItemLibrary.selectItemInTree("Parameters::Network::Carrier Aggregation");
		screenItemLibrary.selectCheckProcedure();
		screenItemLibrary.fillCheckProcedure("Test");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId  T-GPRI-3020
	* @summary Remove Check Procedure on group
	*/
	@Test
	public void test_1330_RemoveCheckProcedureOnGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Carrier Aggregation");
		screenItemLibrary.selectItemInTree("Parameters::Network::Carrier Aggregation");
		screenItemLibrary.selectCheckProcedure();
		screenItemLibrary.removeCheckProcedure();
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId  T-GPRI-3021
	* @summary Copy of group in paste in other group
	*/
	@Test
	public void test_1369_CopyOfGroupInPasteInOtherGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		if(!screenItemLibrary.selectItemInTree("Parameters::Network::New GroupB")){
			screenItemLibrary.rightClickOnNetwork();
			screenItemLibrary.selectOptionOnFolderRightClick("New Group");
			screenItemLibrary.renameItemForAnExistingName("New GroupB");
		}
		screenItemLibrary.putTextOnFieldSearch("New GroupB");
		screenItemLibrary.clickRightOnItem("Parameters::Network::New GroupB");
		screenItemLibrary.selectOptionOnFolderRightClick("Copy");
		if(!screenItemLibrary.selectItemInTree("Parameters::Network::New GroupA")){
			screenItemLibrary.rightClickOnNetwork();
			screenItemLibrary.selectOptionOnFolderRightClick("New Group");
			screenItemLibrary.renameItemForAnExistingName("New GroupA");
		}else if(screenItemLibrary.selectItemInTree("Parameters::Network::New GroupA::New GroupB")){
			screenItemLibrary.clickRightOnItem("Parameters::Network::New GroupA::New GroupB");
			screenItemLibrary.selectOptionOnFolderRightClick("Delete");
			screenItemLibrary.confirmDelete();
		}
		screenItemLibrary.putTextOnFieldSearch("New GroupA");
		screenItemLibrary.clickRightOnItem("Parameters::Network::New GroupA");
		screenItemLibrary.selectOptionOnFolderRightClick("Paste");
		assertTrue(screenItemLibrary.clickOnButtonSave());
	}
	
	/**
	* @testLinkId  T-GPRI-3022
	* @summary Copy the group and paste in the same group
	*/
	//TODO: Should be verify if exits already the Group "Carrier Aggregation" inside in the Group
	@Test
	public void test_1372_CopyTheGroupAndPasteInTheSameGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Carrier Aggregation");
		screenItemLibrary.clickRightOnItem("Parameters::Network::Carrier Aggregation");
		screenItemLibrary.selectOptionOnFolderRightClick("Copy");
		screenItemLibrary.clickRightOnItem("Parameters::Network::Carrier Aggregation");
		screenItemLibrary.selectOptionOnFolderRightClick("Paste");
		String msg = screenItemLibrary.getErrorMsg(" Error");
		if(!msg.isEmpty()){
			assertEquals(msg,"There is already another Group with the same name on destiny group, but it is marked as deleted. Please create a new one with the same name.");
		}else{
			assertTrue(screenItemLibrary.clickOnButtonSave());
		}
	}
	
	/**
	* @testLinkId  T-GPRI-3023
	* @summary Copy a subgroup and paste in the group
	*/
	@Test
	public void test_1373_CopyASubgroupAndPasteInTheGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Special Lock Sequence");
		screenItemLibrary.clickRightOnItem("Parameters::Network::SIM Lock::Special Lock Sequence");
		screenItemLibrary.selectOptionOnFolderRightClick("Copy");
		screenItemLibrary.putTextOnFieldSearch("Clear Codes");
		screenItemLibrary.clickRightOnItem("Parameters::Network::Clear Codes");
		screenItemLibrary.selectOptionOnFolderRightClick("Paste");
		String msg = screenItemLibrary.getErrorMsg(" Error");
		if(!msg.isEmpty()){
			assertEquals(msg,"Cannot paste a multiple group or a group with multiple descendants into a multiple group.");
		}else{
			assertTrue(screenItemLibrary.clickOnButtonSave());
		}
	}
	
	/**
	* @testLinkId  T-GPRI-3024
	* @summary Rename new group
	*/
	@Test
	public void test_1378_RenameNewGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("SKD Block");
		screenItemLibrary.clickRightOnItem("Parameters::Network::SIM Lock::SKD Block");
		screenItemLibrary.selectOptionRightClick("Rename");
		screenItemLibrary.renameItem("New Name");
	}
	
	/**
	* @throws Exception 
	* @testLinkId  T-GPRI-3025
	* @summary Rename group with the same name
	*/
	@Test
	public void test_1380_RenameGroupWithTheSameName() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("International Roaming");
		screenItemLibrary.clickRightOnItem("Parameters::Network::Data Connection::International Roaming");
		screenItemLibrary.selectOptionRightClick("Rename");
		screenItemLibrary.renameItem("Data Warning Message");
		String msgError = screenItemLibrary.getErrorMsg(" Invalid Name");
		assertEquals("There is one Group with name 'Data Warning Message' in this group (Parameters::Network::Data Connection::Data Warning Message). Please choose another name.", msgError);
	}
	
	/**
	* @throws Exception 
	* @testLinkId  T-GPRI-3026
	* @summary Remove group
	*/
	@Test
	public void test_1385_RemoveGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		if(!screenItemLibrary.selectItemInTree("Parameters::Network::New Group Remove")){
			screenItemLibrary.rightClickOnNetwork();
			screenItemLibrary.selectOptionOnFolderRightClick("New Group");
			screenItemLibrary.renameItemForAnExistingName("New Group Remove");
		}
		screenItemLibrary.clickRightOnItem("Parameters::Network::New Group Remove");
		screenItemLibrary.selectOptionOnFolderRightClick("Delete");
		assertTrue(screenItemLibrary.confirmDelete());
	}
	
	/**
	* @throws Exception 
	* @testLinkId  T-GPRI-3027
	* @summary Copy Path of Group
	*/
	@Test
	public void test_1387_CopyPathOfGroup() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		screenItemLibrary.openPage();
		screenItemLibrary.putTextOnFieldSearch("Emergency Numbers");
		screenItemLibrary.clickRightOnItem("Parameters::Network::Emergency Numbers");
		screenItemLibrary.selectOptionRightClick("Copy Path");
		assertTrue(screenItemLibrary.copyPath("Parameters::Network::Emergency Numbers"));
		
	}
}
