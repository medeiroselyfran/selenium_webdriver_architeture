package com.lge.gpri.tests.packages.editProfile;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Change Folder Name By Profile.
*  
* @author Gabriel Costa
* @author Elyfran
*/

import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestChangeFolderNameByProfile extends AbstractTestScenary {

	/**
	* @testLinkId T-GPRI-3141
	* @summary Added a new value for a folder per Per Profile
	*/
	@Test
	public void test_2326_AddedANewValueForAFolderPerProfile() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();

		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();

		screenPerformEditPackageProfile.addItemByPath(ConstantsPackages.PATH_CUSTOMIZATION_HOTSEAT_ANDROID_ITEM_FOLDER);
		screenPerformEditPackageProfile.addItemByPath(ConstantsPackages.PATH_CUSTOMIZATION_HOTSEAT_ANDROID_FOLDER_NAME);
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();

		screenPerformEditPackageProfile.setValueItemPerProfile(ConstantsPackages.PATH_FOLDER_1_NAME, ConstantsPackages.FOLDER_TEST_2);
		screenPerformEditPackageProfile.setValueItemPerProfile(ConstantsPackages.PATH_FOLDER_1_NAME, ConstantsPackages.FOLDER_TEST);
		screenPerformEditPackageProfile.setValueItemPerProfile(ConstantsPackages.PATH_ITEM_1_FOLDER, ConstantsPackages.FOLDER_TEST);
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsPackages.LGH815);
		screenPerformView.addSearch(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformView.selectFirstItemResultSearch();

		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();

		String valueFolder = screenPerformViewValues
				.getValueItemOnTreeTable(ConstantsPackages.PATH_FOLDER_1_NAME);
		String valueItem = screenPerformViewValues
				.getValueItemOnTreeTable(ConstantsPackages.PATH_ITEM_1_FOLDER);

		assertEquals(ConstantsPackages.FOLDER_TEST, valueFolder);
		assertEquals(ConstantsPackages.FOLDER_TEST, valueItem);
	}

	/**
	* @testLinkId T-GPRI-3142
	* @summary Update the folder with item associated with folder
	*/
	@Test
	public void test_2371_UpdateTheFolderWithItemAssociatedWithFolder() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();

		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();

		screenPerformEditPackageProfile.addItemByPath(ConstantsPackages.PATH_CUSTOMIZATION_HOTSEAT_ANDROID_ITEM_FOLDER);
		screenPerformEditPackageProfile.addItemByPath(ConstantsPackages.PATH_CUSTOMIZATION_HOTSEAT_ANDROID_FOLDER_NAME);
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();

		screenPerformEditPackageProfile.setValueItemPerProfile(ConstantsPackages.PATH_FOLDER_1_NAME, ConstantsPackages.FOLDER_TEST_2);
		screenPerformEditPackageProfile.setValueItemPerProfile(ConstantsPackages.PATH_FOLDER_1_NAME, ConstantsPackages.FOLDER_TEST);
		screenPerformEditPackageProfile.setValueItemValue(ConstantsPackages.PATH_ITEM_1_FOLDER, ConstantsPackages.FOLDER_TEST);
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();

		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsPackages.LGH815);
		screenPerformView.addSearch(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformView.selectFirstItemResultSearch();

		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();

		String valueFolder = screenPerformViewValues.getValueItemOnTreeTable(ConstantsPackages.PATH_FOLDER_1_NAME);
		String valueItem = screenPerformViewValues.getValueItemOnTreeTable(ConstantsPackages.PATH_ITEM_1_FOLDER);

		assertEquals(ConstantsPackages.FOLDER_TEST, valueFolder);
		assertEquals(ConstantsPackages.FOLDER_TEST, valueItem);
	}
	
	/**
	* @testLinkId T-GPRI-3143
	* @summary Add new folder
	*/
	@Test
	public void test_2372_AddANewFolder() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		
		screenPerformListPackages.selectTabCustomizationOrParameters(ConstantsPackages.CUSTOMIZATION);
		
		String addItemToFolder = screenPerformEditPackageProfile.addItemToFolder(ConstantsPackages.PATH_CUSTOMIZATION_HOTSEAT_ANDROID_FOLDER);
		screenPerformEditPackageProfile.setValueItemPerProfile(ConstantsPackages.PATH_FOLDER_VAR_NAME.replace("%%s", addItemToFolder), ConstantsPackages.FOLDER_TEST+addItemToFolder);
		screenPerformEditPackageProfile.setValueItemValue(ConstantsPackages.PATH_ITEM_1_FOLDER, ConstantsPackages.FOLDER_TEST+addItemToFolder);
		
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsPackages.LGH815);
		screenPerformView.addSearch(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformView.selectFirstItemResultSearch();

		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		
		String pathToFolder = ConstantsPackages.PATH_FOLDER_VAR_NAME.replace("%%s", addItemToFolder);
		String valueFolder = screenPerformViewValues.getValueItemOnTreeTable(pathToFolder);
		String valueItem = screenPerformViewValues.getValueItemOnTreeTable(ConstantsPackages.PATH_ITEM_1_FOLDER);
		
		assertEquals(ConstantsPackages.FOLDER_TEST+addItemToFolder, valueFolder);
		assertEquals(ConstantsPackages.FOLDER_TEST+addItemToFolder, valueItem);
	}
	
	/**
	* @testLinkId T-GPRI-3144
	* @summary New Folder without value
	*/
	@Test
	public void test_2373_NewFolderWithoutValue() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		String addItemToFolder = screenPerformEditPackageProfile.addItemToFolder(ConstantsPackages.PATH_CUSTOMIZATION_HOTSEAT_ANDROID_FOLDER);
		screenPerformEditPackageProfile.clickOnSaveProfile();
		String msgErrorMarkedFields = screenPerformEditPackageProfile.getMSgWaringMarkedFields();
		screenPerformEditPackageProfile.clickOkWarningFields();
		String msgFieldEmpty = screenPerformEditPackageProfile.getMsgWarningItem(ConstantsPackages.PATH_FOLDER_VAR_NAME.replace("%%s", addItemToFolder));
		assertEquals(ConstantsPackages.MSG_WARNING_CANNOT_SAVE_PACKAGE,msgErrorMarkedFields);
		assertEquals(ConstantsPackages.MSG_WARNING_FIELD_EMPTY,msgFieldEmpty);
	}
	
	/**
	* @testLinkId T-GPRI-3145
	* @summary Remove folder
	*/
	@Test
	public void test_2374_RemoveFolder() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		
		String addItemToFolder = screenPerformEditPackageProfile.addItemToFolder(ConstantsPackages.PATH_CUSTOMIZATION_HOTSEAT_ANDROID_FOLDER);
		screenPerformEditPackageProfile.setValueItemPerProfile(ConstantsPackages.PATH_FOLDER_VAR_NAME.replace("%%s", addItemToFolder), ConstantsPackages.FOLDER_TEST+addItemToFolder);
		
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.removeItemInFolder(ConstantsPackages.PATH_FOLDER_VAR.replace("%%s", addItemToFolder));
		String previousNameFolder = screenPerformEditPackageProfile.getValueItem(ConstantsPackages.PATH_FOLDER_VAR_NAME.replace("%%s", (Integer.parseInt(addItemToFolder)-1)+""));
		screenPerformEditPackageProfile.setValueItemValue(ConstantsPackages.PATH_ITEM_1_FOLDER, previousNameFolder);
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsPackages.LGH815);
		screenPerformView.addSearch(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformView.selectFirstItemResultSearch();

		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		
		String pathToFolderRemoved = ConstantsPackages.PATH_FOLDER_VAR_NAME.replace("%%s", addItemToFolder);
		String valueFolderRemoved = screenPerformViewValues.getValueItemOnTreeTable(pathToFolderRemoved);
		String valueItem = screenPerformViewValues.getValueItemOnTreeTable(ConstantsPackages.PATH_ITEM_1_FOLDER);
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.sortFolder(ConstantsPackages.PATH_FOLDER);
		
		assertEquals(null, valueFolderRemoved);
		assertEquals(previousNameFolder, valueItem);
	}
	
	/**
	* @testLinkId T-GPRI-3146
	* @summary Add new folder with do not apply
	*/
	@Test
	public void test_2375_AddNewFolderWithDoNotApply() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		
		String addItemToFolder = screenPerformEditPackageProfile.addItemToFolder(ConstantsPackages.PATH_CUSTOMIZATION_HOTSEAT_ANDROID_FOLDER);
		screenPerformEditPackageProfile.setValueItemPerProfile(ConstantsPackages.PATH_FOLDER_VAR_NAME.replace("%%s", addItemToFolder), ConstantsPackages.FOLDER_TEST+addItemToFolder);
		screenPerformEditPackageProfile.setValueItemValue(ConstantsPackages.PATH_ITEM_1_FOLDER, ConstantsPackages.FOLDER_TEST+addItemToFolder);
		
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.checkItemsAsDoNotApplyButton(ConstantsPackages.PATH_FOLDER_VAR_NAME.replace("%%s", addItemToFolder));
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		
		screenPerformView.openPage();
		screenPerformView.addSearch(ConstantsPackages.LGH815);
		screenPerformView.addSearch(ConstantsPackages.MOBILE_ISRAEL_ISM);
		screenPerformView.selectFirstItemResultSearch();

		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		
		String pathToFolderDoNotApply = ConstantsPackages.PATH_FOLDER_VAR_NAME.replace("%%s", addItemToFolder);
		String valueFolderRemoved = screenPerformViewValues.getValueItemOnTreeTable(pathToFolderDoNotApply);
		assertEquals(null, valueFolderRemoved);
	}
	
}
