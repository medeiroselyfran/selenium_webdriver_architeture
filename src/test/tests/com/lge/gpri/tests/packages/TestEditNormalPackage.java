package com.lge.gpri.tests.packages;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.packages.ScreenPerformNormalPackage;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Edit Normal
 * Package.
 * 
 * @author Lucas Teixeira
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestEditNormalPackage extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2770
	 * @summary Edit Normal Package Change Name
	 */
	@Test
	public void test_1129_ChangeName() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		String constructorNamePackage = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		constructorNamePackage = screenPerformListPackages.constructorNamePackage();
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		if ("Normal".equalsIgnoreCase(screenPerformListPackages.getPackageType())) {
			if (screenPerformListPackages.clickOnEditPackageButton()) {
				screenPerformNormalPackage.setPackageName(constructorNamePackage);
				screenPerformNormalPackage.setTargetDate(ConstantsPackages.TARGET_DATE_26_11_18);
			} else {
				screenPerformListPackages.reopenPackage("");
				if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
					screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
				}
				screenPerformListPackages.clickOnEditPackageButton();
				screenPerformNormalPackage.setPackageName(constructorNamePackage);
				screenPerformNormalPackage.setTargetDate(ConstantsPackages.TARGET_DATE_26_11_18);
			}
		} else {
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH840, "",
						ConstantsPackages.CLARO_BRAZIL_CLR, constructorNamePackage, ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH840, "",
						ConstantsPackages.CLARO_BRAZIL_CLR, constructorNamePackage, ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
				screenPerformNormalPackage.clickOnOkPackageSuccessCreated();
				constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			}
		}
			if (screenPerformListPackages.clickOnEditPackageButton()) {
				screenPerformNormalPackage.setPackageName(constructorNamePackage);
				screenPerformNormalPackage.setTargetDate(ConstantsPackages.TARGET_DATE_26_11_18);
			} else {
				screenPerformListPackages.reopenPackage("");
				if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
					screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
				}
				screenPerformListPackages.clickOnEditPackageButton();
				screenPerformNormalPackage.setPackageName(constructorNamePackage);
				screenPerformNormalPackage.setTargetDate(ConstantsPackages.TARGET_DATE_26_11_18);
			}
		
		screenPerformNormalPackage.clickOnSaveEditedButton();
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		assertEquals(constructorNamePackage, packageName);
	}

	/**
	 * @testLinkId T-GPRI-2771
	 * @summary Edit Normal Package Change UI Simulator
	 */
	@Test
	public void test_1130_ChangeUISimulator() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformNormalPackage.setUISimulator(ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB);
		} else {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformNormalPackage.setUISimulator(ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB);
		}
		screenPerformNormalPackage.clickOnSaveEditedButton();
		screenPerformListPackages.clickOnEditPackageButton();
		String txtUISimulator = screenPerformNormalPackage.getTextFrom("txtUISimulator");
		assertEquals(ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, txtUISimulator);
	}

	/**
	 * @testLinkId T-GPRI-2772
	 * @summary Edit Normal Package Change AndroidVersion
	 */
	@Test
	public void test_1131_ChangeAndroidVersion() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformNormalPackage.setAndroidVersion(ConstantsPackages.ANDROID_VERSION_6);
		} else {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformNormalPackage.setAndroidVersion(ConstantsPackages.ANDROID_VERSION_6);
		}
		screenPerformNormalPackage.clickOnSaveEditedButton();
		screenPerformListPackages.clickOnEditPackageButton();
		String txtAndroidVersion = screenPerformNormalPackage.getTextFrom("txtAndroidVersion");
		assertEquals(ConstantsPackages.ANDROID_VERSION_6, txtAndroidVersion);
	}

	/**
	 * @testLinkId T-GPRI-2773
	 * @summary Edit Normal Package Leave Name Empty
	 */
	@Test
	public void test_1132_LeaveNameEmpty() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformNormalPackage.setPackageName("");
		} else {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformNormalPackage.setPackageName("");
		}
		screenPerformNormalPackage.clickOnSaveEditedButton();
		String txtPackageEditionValidationName = screenPerformNormalPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationName);
	}

	/**
	 * @testLinkId T-GPRI-2774
	 * @summary Edit Normal Package Leave Reasons Empty
	 */
	@Test
	public void test_1133_LeaveReasonsEmpty() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformNormalPackage.setCreationReasons("");
		} else {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformNormalPackage.setCreationReasons("");
		}
		screenPerformNormalPackage.clickOnSaveEditedButton();
		String txtPackageEditionValidationReasons = screenPerformNormalPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationReasons);
	}

	/**
	 * @testLinkId T-GPRI-2775
	 * @summary Edit Normal Package Target Date For After Date Than Current Day For
	 *          Buyer Child
	 */
	@Test
	public void test_1134_EditNormalPackageTargetDateForAfterDateThanCurrentDayForBuyerChild() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformNormalPackage.setTargetDate(ConstantsPackages.TARGET_DATE_26_11_18);
		} else {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformNormalPackage.setTargetDate(ConstantsPackages.TARGET_DATE_26_11_18);
		}
		screenPerformNormalPackage.clickOnSaveEditedButton();
		screenPerformListPackages.clickOnEditPackageButton();
		String txtTargetDate = screenPerformNormalPackage.getAttributeTextFrom("fieldTargetDate");
		assertEquals(ConstantsPackages.TARGET_DATE_26_11_18, txtTargetDate);
	}

	/**
	 * @testLinkId T-GPRI-2776
	 * @summary Edit Normal Package Target Date For After Date Than Current Day For
	 *          Buyer Mother
	 */
	@Test
	public void test_1142_EditNormalPackageTargetDateForAfterDateThanCurrentDayForBuyerMother() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.TLF_GROUP_EUROPE_6TL);
		if (!screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformListPackages.selectReopenPackage(ConstantsPackages.TARGET_DATE_26_11_18_OTHER_FORMAT);
		}
		screenPerformListPackages.clickOnEditPackageButton();
		String txtTargetDate = screenPerformNormalPackage.getAttributeTextFrom("fieldTargetDate");
		String date = txtTargetDate.replace("/20", "/");
		assertEquals(ConstantsPackages.TARGET_DATE_26_11_18_OTHER_FORMAT, date);
	}

	/**
	 * @testLinkId T-GPRI-2777
	 * @summary Edit Normal Package Target Date For Before Date Than Current Day
	 */
	@Test
	public void test_1135_EditNormalPackageTargetDateForBeforeDateThanCurrentDay() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformNormalPackage.setTargetDate(ConstantsPackages.TARGET_DATE_01_10_2016);
		} else {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformNormalPackage.setTargetDate(ConstantsPackages.TARGET_DATE_01_10_2016);
		}
		screenPerformNormalPackage.clickOnSaveEditedButton();
		String txtPackageEditionValidationDate = screenPerformNormalPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationDate);
	}

	/**
	 * @testLinkId T-GPRI-2778
	 * @summary Edit Normal Package With Name Already Existing Buyer Question
	 */
	@Test
	public void test_1136_WithNameAlreadyExistingBuyerQuestion() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		screenPerformListPackages.getNamePackageConfirmation();
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformNormalPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_DUPLICATE);
		} else {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformNormalPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_DUPLICATE);
		}
		screenPerformNormalPackage.clickOnSaveEditedButton();
		String txtPackageEditionValidationDate = screenPerformNormalPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationDate);
	}

	/**
	 * @testLinkId T-GPRI-2779
	 * @summary Edit Normal Package With Name With Name Double Quote
	 */
	@Test
	public void test_1137_EditNormalPackageWithNameWhithNameDoubleQuote() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		screenPerformListPackages.getNamePackageConfirmation();
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformNormalPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_DOBLE_QUOTE);
		} else {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformNormalPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_DOBLE_QUOTE);
		}
		screenPerformNormalPackage.clickOnSaveEditedButton();
		String txtPackageEditionValidationDate = screenPerformNormalPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationDate);
	}

	/**
	 * @testLinkId T-GPRI-2780
	 * @summary Edit Normal Package With Name With Name Double Quote
	 */
	@Test
	public void test_1138_EditNormalPackageWithNameWithNameSimpleQuote() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		screenPerformListPackages.getNamePackageConfirmation();
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformNormalPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_SIMPLE_QUOTE);
		} else {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformNormalPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_SIMPLE_QUOTE);
		}
		screenPerformNormalPackage.clickOnSaveEditedButton();
		String txtPackageEditionValidationDate = screenPerformNormalPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationDate);
	}

	/**
	 * @testLinkId T-GPRI-2781
	 * @summary Edit Normal Package With Name With Name Backslash
	 */
	@Test
	public void test_1139_EditNormalPackageWithNameWithNameBackslash() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		screenPerformListPackages.getNamePackageConfirmation();
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformNormalPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_BACKSLASH);
		} else {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformNormalPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_BACKSLASH);
		}
		screenPerformNormalPackage.clickOnSaveEditedButton();
		String txtPackageEditionValidationDate = screenPerformNormalPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationDate);
	}

	/**
	 * @testLinkId T-GPRI-2782
	 * @summary Edit Normal Package Change Country Shortcut
	 */
	@Test
	public void test_1140_EditNormalPackageChangeCountryShortcut() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH840);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		screenPerformListPackages.getNamePackageConfirmation();
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformNormalPackage.setCountryShortcut(ConstantsPackages.PACKAGE_COUNTRY_SHORTCUT);
		} else {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformNormalPackage.setCountryShortcut(ConstantsPackages.PACKAGE_COUNTRY_SHORTCUT);
		}
		screenPerformNormalPackage.clickOnSaveEditedButton();
		screenPerformListPackages.clickOnEditPackageButton();
		String txtPackageEditionValidationShortcut = screenPerformNormalPackage
				.getAttributeTextFrom("fieldCountryShortcut");
		assertEquals(ConstantsPackages.PACKAGE_COUNTRY_SHORTCUT, txtPackageEditionValidationShortcut);
	}
}
