package com.lge.gpri.tests.packages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.packages.ScreenAutoCommit;
import com.lge.gpri.screens.packages.ScreenPackageHistory;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.packages.ScreenPerformNormalPackage;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Normal
 * Package History.
 * 
 * @author Gabriel Costa
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestNormalPackageStatusHistory extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2799
	 * @summary Status change from Under Edition to Confirmed
	 */
	@Test
	public void test_889_StatusChangeFromUnderEditionToConfirmed() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		//screenPerformListPackages.clickOnLoadOnlyTheLatestPackageButton();
		//screenPerformListPackages.searchPackage(ConstantsPackages.PACKAGE_NAME_TEST2);
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		String txtSecondOldStatus = screenPackageHistory.getSecondOldStatus();
		String txtSecondNewStatus = screenPackageHistory.getSecondNewStatus();
		String txtFirstOldStatus = screenPackageHistory.getFirstOldStatus();
		String txtFirstNewStatus = screenPackageHistory.getFirstNewStatus();
		getDriver().close();
		screenPackageHistory.switchTabs(0);
		assertEquals(ConstantsPackages.UNDER_EDITION, txtSecondOldStatus);
		assertEquals(ConstantsPackages.COMMITING, txtSecondNewStatus);
		assertEquals(ConstantsPackages.COMMITING, txtFirstOldStatus);
		assertEquals(ConstantsPackages.CONFIRMED, txtFirstNewStatus);
	}

	/**
	 * @testLinkId T-GPRI-2800
	 * @summary Status change from Commiting to Under Edition
	 */
	@Test
	public void test_890_StatusChangeFromCommitingToUnderEdition() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		createNormalPackage(ConstantsPackages.LGH815, ConstantsPackages.CLARO_BRAZIL_CLR);
		screenPerformListPackages.clickOnConfirmPackageButton();
		screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
		screenPerformListPackages.clickOnReopenPackageButton();
		screenPerformListPackages.clickOnOKButtonAfterReopenPackage();
		screenPerformListPackages.selectExtendedDateToday();
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		String txtFirstOldStatus = screenPackageHistory.getFirstOldStatus();
		String txtFirstNewStatus = screenPackageHistory.getFirstNewStatus();
		getDriver().close();
		screenPackageHistory.switchTabs(0);
		assertEquals(ConstantsPackages.COMMITING, txtFirstOldStatus);
		assertEquals(ConstantsPackages.UNDER_EDITION, txtFirstNewStatus);
	}

	/**
	 * @testLinkId T-GPRI-2801
	 * @summary Status change from Confirmed to Under Edition
	 */
	@Test
	public void test_891_StatusChangeFromConfirmedToUnderEdition() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());
		String txtFirstOldStatus = "", txtFirstNewStatus = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OPEN_EUROPE_EUU);
		boolean isNotConfirmed = screenPerformListPackages.clickOnConfirmPackageButton();
		if (isNotConfirmed) {
			screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
			screenPerformListPackages.checkStatus();
		}
		if (screenPerformListPackages.getStatusPackageConfirmation().contains(ConstantsPackages.CONFIRMED)) {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnOKButtonAfterReopenPackage();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnPackageHistoryButton();
			screenPerformListPackages.switchTabs(1);
			txtFirstOldStatus = screenPackageHistory.getFirstOldStatus();
			txtFirstNewStatus = screenPackageHistory.getFirstNewStatus();
			getDriver().close();
			screenPackageHistory.switchTabs(0);
		}
		assertEquals(ConstantsPackages.CONFIRMED, txtFirstOldStatus);
		assertEquals(ConstantsPackages.UNDER_EDITION, txtFirstNewStatus);
	}

	/**
	 * @testLinkId T-GPRI-2802
	 * @summary Status change from Under Edition to Commit Error
	 */
	@Test
	public void test_892_StatusChangeFromUnderEditionToCommitError() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.VIVO_BRAZIL_VIV);
		screenPerformListPackages.searchPackage("PKG4-M-MS1");
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		String txtSecondOldStatus = screenPackageHistory.getSecondOldStatus();
		String txtSecondNewStatus = screenPackageHistory.getSecondNewStatus();
		String txtFirstOldStatus = screenPackageHistory.getFirstOldStatus();
		String txtFirstNewStatus = screenPackageHistory.getFirstNewStatus();
		getDriver().close();
		screenPackageHistory.switchTabs(0);
		assertEquals(ConstantsPackages.UNDER_EDITION, txtSecondOldStatus);
		assertEquals(ConstantsPackages.COMMITING, txtSecondNewStatus);
		assertEquals(ConstantsPackages.COMMITING, txtFirstOldStatus);
		assertEquals(ConstantsPackages.COMMIT_ERROR, txtFirstNewStatus);
	}

	/**
	 * @testLinkId T-GPRI-2803
	 * @summary Permanlinks when status is Commiting
	 */
	@Test
	public void test_920_PermanlinksWhenStatusIsCommiting() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage(ConstantsPackages.TEST_43);
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.clickOnFirstPermalinks();
		String txtNoPermalinksFound = screenPackageHistory.getMsgPackagePermalinks();
		getDriver().close();
		screenPackageHistory.switchTabs(0);
		assertEquals(ConstantsPackages.TXT_NO_PERMALINKS_FOUND, txtNoPermalinksFound);
	}

	/**
	 * @testLinkId T-GPRI-2804
	 * @summary Permanlinks when package is confirmed with success
	 */
	@Test
	public void test_921_PermanlinksWhenPackageIsConfirmedWithSuccess() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_BRAZIL_CLR);
		/*screenPerformListPackages.clickOnLoadOnlyTheLatestPackageButton();
		screenPerformListPackages.searchPackage(ConstantsPackages.PACKAGE_NAME_TEST2);*/
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		String txtFirstNewStatus = screenPackageHistory.getFirstNewStatus();
		screenPackageHistory.clickOnFirstPermalinks();
		String txtNoPermalinksFound = screenPackageHistory.getMsgPackagePermalinks();
		getDriver().close();
		screenPackageHistory.switchTabs(0);
		assertEquals(ConstantsPackages.CONFIRMED, txtFirstNewStatus);
		assertTrue(txtNoPermalinksFound.contains("Autocommit"));
	}

	/**
	 * @testLinkId T-GPRI-2805
	 * @summary Error Validating
	 */
	@Test
	public void test_922_ErrorValidating() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		createNormalPackageInvalid(ConstantsPackages.LGH815, ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnConfirmPackageButton();
		screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
		screenPerformListPackages.checkStatus();
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.clickOnFirstDetails();
		boolean allValidatingError = screenPackageHistory.allValidatingErrorAutocommit();
		getDriver().close();
		screenPackageHistory.switchTabs(0);
		assertTrue(allValidatingError);
	}

	/**
	 * @testLinkId T-GPRI-2806
	 * @summary Error Commiting
	 */
	@Test
	public void test_923_ErrorCommiting() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenAutoCommit screenAutoCommit = new ScreenAutoCommit(getDriver());

		screenPerformListPackages.openPage();
		sectionPerformMenu.clickOnMenuConfiguration();
		sectionPerformMenu.clickOnMenuAutocommits();
		String urlSave = screenAutoCommit.getCommitUrlFromRestricitons(
				Arrays.asList(ConstantsPackages.RESTRICTION_OS_M, ConstantsPackages.RESTRICTION_UI_4_2_5_0));
		if (!urlSave.startsWith("a")) {
			screenAutoCommit.setCommitUrlFromRestricitons("a" + urlSave,
					Arrays.asList(ConstantsPackages.RESTRICTION_OS_M, ConstantsPackages.RESTRICTION_UI_4_2_5_0));
			screenAutoCommit.saveChanges();
			screenAutoCommit.confirmSaveChanges();
			screenAutoCommit.waitLoadingOverlay();
		}
		createNormalPackage(ConstantsPackages.LGH815, ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnConfirmPackageButton();
		screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
		screenPerformListPackages.checkStatus();
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.clickOnFirstDetails();
		screenPackageHistory.viewRequestDetailsFromAutocommit(ConstantsPackages.AUTOCOMMIT_GPRI_M);
		String txtErrorCommiting = screenPackageHistory.getDetailsAutocommitFromStatus(ConstantsPackages.COMMITING);
		getDriver().close();
		screenPackageHistory.switchTabs(0);
		sectionPerformMenu.clickOnMenuConfiguration();
		sectionPerformMenu.clickOnMenuAutocommits();
		if (urlSave.startsWith("a")) {
			screenAutoCommit.setCommitUrlFromRestricitons(urlSave.substring(1, urlSave.length()),
					Arrays.asList(ConstantsPackages.RESTRICTION_OS_M, ConstantsPackages.RESTRICTION_UI_4_2_5_0));
		} else {
			screenAutoCommit.setCommitUrlFromRestricitons(urlSave,
					Arrays.asList(ConstantsPackages.RESTRICTION_OS_M, ConstantsPackages.RESTRICTION_UI_4_2_5_0));
		}
		screenAutoCommit.saveChanges();
		screenAutoCommit.confirmSaveChanges();
		screenAutoCommit.waitLoadingOverlay();
		assertEquals(ConstantsPackages.TXT_ERROR_COMMITING, txtErrorCommiting);
	}

	/**
	 * @testLinkId T-GPRI-2807
	 * @summary Error Building
	 */
	@Test
	public void test_924_ErrorBuilding() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage(ConstantsPackages.TEST_41);
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		String txtFirstNewStatus = screenPackageHistory.getFirstNewStatus();
		screenPackageHistory.clickOnFirstDetails();
		screenPackageHistory.viewRequestDetailsFromAutocommit(ConstantsPackages.AUTOCOMMIT_APK_OVERLAY2_M);
		String txtErrorBuilding = screenPackageHistory.getDetailsAutocommitFromStatus(ConstantsPackages.BUILDING);
		getDriver().close();
		screenPackageHistory.switchTabs(0);
		assertEquals(ConstantsPackages.COMMIT_ERROR, txtFirstNewStatus);
		assertEquals(ConstantsPackages.TXT_ERROR_ENGINE, txtErrorBuilding);
	}

	public void createNormalPackage(String model, String buyer) throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(model);
		screenPerformListPackages.setBuyer(buyer);
		boolean isUnderEdition = screenPerformListPackages.clickOnRemovePackageButton();
		if (isUnderEdition) {
			screenPerformListPackages.confirmRemovePackage();
		}

		String namePackage = screenPerformListPackages.getExistingPackage();

		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F_)) {
			int numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(model, "", buyer,
						ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {

				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(model, "", buyer,
						ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}

		} else {
			screenPerformListPackages.waitLoading();
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformListPackages.waitLoading();
			screenPerformNormalPackage.fillFieldsPackage(model, "", buyer, ConstantsPackages.PACKAGE_NAME_TEST_F_1,
					ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
					ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
			screenPerformNormalPackage.clickOnSaveCreatedButton();
		}
		screenPerformNormalPackage.clickOnKeepConfirmationButton();
		screenPerformNormalPackage.clickOnOkPackageSuccessCreated();
	}

	public void createNormalPackageInvalid(String model, String buyer) throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(model);
		screenPerformListPackages.setBuyer(buyer);
		boolean isUnderEdition = screenPerformListPackages.clickOnRemovePackageButton();
		if (isUnderEdition) {
			screenPerformListPackages.confirmRemovePackage();
		}

		String namePackage = screenPerformListPackages.getExistingPackage();

		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F_)) {
			int numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(model, "", buyer,
						ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_O,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
			} else {

				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(model, "", buyer,
						ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_O,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
			}

		} else {
			screenPerformListPackages.waitLoading();
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformListPackages.waitLoading();
			screenPerformNormalPackage.fillFieldsPackage(model, "", buyer, ConstantsPackages.PACKAGE_NAME_TEST_F_1,
					ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_O,
					ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
		}
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnKeepConfirmationButton();
		screenPerformNormalPackage.clickOnOkPackageSuccessCreated();
	}
}
