package com.lge.gpri.tests.packages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.packages.ScreenPerformNormalPackage;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Remove Normal
 * Package.
 * 
 * @author Gabriel Costa
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestRemoveNormalPackage extends AbstractTestScenary {
	/**
	 * @testLinkId T-GPRI-2784
	 * @summary Cancel Removal
	 */
	@Test
	public void test_1195_CancelRemoval() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		createNormalPackage(ConstantsPackages.LGH815, ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		screenPerformListPackages.clickOnRemovePackageButton();
		screenPerformListPackages.cancelRemovePackage();
		screenPerformListPackages.waitLoadingOverlay();
		String packageNameNotRemoved = screenPerformListPackages.getNamePackageConfirmation();

		assertEquals(packageName, packageNameNotRemoved);
	}

	/**
	 * @testLinkId T-GPRI-2785
	 * @summary Confirm removal
	 */
	@Test
	public void test_1196_ConfirmRemoval() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		createNormalPackage(ConstantsPackages.LGH815, ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		screenPerformListPackages.clickOnRemovePackageButton();
		screenPerformListPackages.confirmRemovePackage();
		screenPerformListPackages.waitLoadingOverlay();
		String packageNameRemoved = screenPerformListPackages.getNamePackageConfirmation();

		assertFalse(packageName.equals(packageNameRemoved));
	}

	/**
	 * @testLinkId T-GPRI-2786
	 * @summary Remove Package Confirmed
	 */
	@Test
	public void test_1197_RemovePackageConfirmed() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		createNormalPackage(ConstantsPackages.LGH815, ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		String status = screenPerformListPackages.getStatusPackageConfirmation();
		if (status.contains(ConstantsPackages.UNDER_EDITION)) {
			screenPerformListPackages.clickOnConfirmPackageButton();
			screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
		}
		boolean isUnderEdition = screenPerformListPackages.clickOnRemovePackageButton();
		assertFalse(isUnderEdition);
	}

	public void createNormalPackage(String model, String buyer) throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(model);
		screenPerformListPackages.setBuyer(buyer);
		boolean isUnderEdition = screenPerformListPackages.clickOnRemovePackageButton();
		if (isUnderEdition) {
			screenPerformListPackages.confirmRemovePackage();
		}

		String namePackage = screenPerformListPackages.getExistingPackage();

		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F_)) {
			int numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(model, "", buyer,
						ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {

				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(model, "", buyer,
						ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}

		} else {
			screenPerformListPackages.waitLoading();
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformListPackages.waitLoading();
			screenPerformNormalPackage.fillFieldsPackage(model, "", buyer, ConstantsPackages.PACKAGE_NAME_TEST_F_1,
					ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
					ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
			screenPerformNormalPackage.clickOnSaveCreatedButton();
		}
		screenPerformNormalPackage.clickOnKeepConfirmationButton();
		String txtPackageCreateSuccess = screenPerformNormalPackage.getTextFrom("txtPackageCreatedSuccess");
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
	}
}
