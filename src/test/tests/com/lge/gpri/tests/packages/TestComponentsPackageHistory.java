package com.lge.gpri.tests.packages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPackageHistory;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Components
 * Package History.
 * 
 * @author Gabriel Costa
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestComponentsPackageHistory extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2808
	 * @summary Show All Lines
	 */
	@Test
	public void test_893_ShowAllLines() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.showTableElements("All");
		int allLines = screenPackageHistory.getLinesQuantity();
		String[] linesQuantity = screenPackageHistory.getShowQuantityOfLinesOnTheFootTable();
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertEquals(linesQuantity[0], linesQuantity[1]);
		assertEquals(linesQuantity[0], (String.valueOf(allLines)));
	}

	/**
	 * @testLinkId T-GPRI-2809
	 * @summary Show 100 Lines
	 */
	@Test
	public void test_894_Show100Lines() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.showTableElements("100");
		int allLines = screenPackageHistory.getLinesQuantity();
		String[] linesQuantity = screenPackageHistory.getShowQuantityOfLinesOnTheFootTable();
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertEquals("100", linesQuantity[0]);
		assertEquals("100", (String.valueOf(allLines)));
	}

	/**
	 * @testLinkId T-GPRI-2810
	 * @summary Show 50 Lines
	 */
	@Test
	public void test_895_Show50Lines() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.showTableElements("50");
		int allLines = screenPackageHistory.getLinesQuantity();
		String[] linesQuantity = screenPackageHistory.getShowQuantityOfLinesOnTheFootTable();
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertEquals("50", linesQuantity[0]);
		assertEquals("50", (String.valueOf(allLines)));
	}

	/**
	 * @testLinkId T-GPRI-2811
	 * @summary Show 25 Lines
	 */
	@Test
	public void test_896_Show25Lines() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.showTableElements("25");
		int allLines = screenPackageHistory.getLinesQuantity();
		String[] linesQuantity = screenPackageHistory.getShowQuantityOfLinesOnTheFootTable();
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertEquals("25", linesQuantity[0]);
		assertEquals("25", (String.valueOf(allLines)));
	}

	/**
	 * @testLinkId T-GPRI-2812
	 * @summary Show 10 Lines
	 */
	@Test
	public void test_897_Show10Lines() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.showTableElements("10");
		int allLines = screenPackageHistory.getLinesQuantity();
		String[] linesQuantity = screenPackageHistory.getShowQuantityOfLinesOnTheFootTable();
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertEquals("10", linesQuantity[0]);
		assertEquals("10", (String.valueOf(allLines)));
	}

	/**
	 * @testLinkId T-GPRI-2813
	 * @summary Search invalid
	 */
	@Test
	public void test_898_SearchValid() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		
		screenPerformListPackages.searchPackage(screenPerformListPackages.getNameSecondPackage());
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.showTableElements("All");
		screenPackageHistory.setFilter(ConstantsPackages.VALID_SEARCH);
		int allLines = screenPackageHistory.getLinesQuantity();
		int linesContainsSearch = Integer.parseInt(screenPackageHistory.getShowQuantityOfLinesOnTheFootTable()[0]);
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertEquals(allLines, linesContainsSearch);
	}

	/**
	 * @testLinkId T-GPRI-2814
	 * @summary Search invalid
	 */
	@Test
	public void test_899_SearchInvalid() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage(screenPerformListPackages.getNameSecondPackage());
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.setFilter(ConstantsPackages.INVALID_SEARCH);
		String msg = screenPackageHistory.getMessageForNoHistoriesAvailable();
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertEquals(ConstantsPackages.NO_HISTORIES_AVAILABLE, msg);
	}

	/**
	 * @testLinkId T-GPRI-2815
	 * @summary Last Page
	 */
	@Test
	public void test_900_LastPage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());
		String numberOfPages;
		int actualPage;
		boolean resultOk = false;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage(screenPerformListPackages.getNameSecondPackage());
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.goToLastPage();
		numberOfPages = screenPackageHistory.getNumberOfLastPage();
		actualPage = screenPackageHistory.getActivePage();
		if (String.valueOf(actualPage).equals(numberOfPages)) {
			resultOk = true;
		}
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2816
	 * @summary First Page
	 */
	@Test
	public void test_901_FirstPage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());
		int actualPage;
		boolean resultOk = false;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage(screenPerformListPackages.getNameSecondPackage());
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.goToFirstPage();
		actualPage = screenPackageHistory.getActivePage();
		if (actualPage == 1) {
			resultOk = true;
		}
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2817
	 * @summary Next Page
	 */
	@Test
	public void test_902_NextPage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());
		int initialPage, actualPage, numberOfPages;
		boolean resultOk = false;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage(screenPerformListPackages.getNameSecondPackage());
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		numberOfPages = screenPackageHistory.getNumberOfPages();
		initialPage = screenPackageHistory.getActivePage();
		screenPackageHistory.paginateNext();
		actualPage = screenPackageHistory.getActivePage();
		if (initialPage < actualPage || actualPage == numberOfPages) {
			resultOk = true;
		}
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2818
	 * @summary Previous Page
	 */
	@Test
	public void test_903_PreviousPage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());
		int initialPage, actualPage;
		boolean resultOk = false;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage(screenPerformListPackages.getNameSecondPackage());
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		initialPage = screenPackageHistory.getActivePage();
		screenPackageHistory.paginatePrevious();
		actualPage = screenPackageHistory.getActivePage();
		if (initialPage > actualPage || actualPage == 1) {
			resultOk = true;
		}
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2819
	 * @summary Select Page
	 */
	@Test
	public void test_904_SelectPage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPackageHistory screenPackageHistory = new ScreenPackageHistory(getDriver());
		int actualPage;
		boolean resultOk = false;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage(screenPerformListPackages.getNameSecondPackage());
		screenPerformListPackages.clickOnPackageHistoryButton();
		screenPerformListPackages.switchTabs(1);
		screenPackageHistory.goToChoosedPage("3");
		actualPage = screenPackageHistory.getActivePage();
		if (actualPage == 3) {
			resultOk = true;
		}
		getDriver().close();
		screenPackageHistory.switchTabs(0);

		assertTrue(resultOk);
	}
}
