package com.lge.gpri.tests.packages.editProfile;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsCompare;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.buyers.ScreenPerformBuyerApproval;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Update To on
 * Edit Package Profile.
 * 
 * @author Gabriel Costa
 * @author Cayk Lima Barreto
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestUpdateTo extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-3162
	 * @summary Buyer Values
	 */
	@Test
	public void test_2343_BuyerValues() throws Exception {
		String homeNetwork = "";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsPackages.SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(ConstantsPackages.SUFFIX_BOI);
		if (screenPerformEditBuyerProfile.verifyIfBuyerWithoutItems()) {
			screenPerformEditBuyerProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		} else {
			screenPerformEditBuyerProfile.clickOnSelectItemsButton();
		}
		screenPerformEditBuyerProfile.addItemByPath("");
		screenPerformEditBuyerProfile.removeItemByPath("");
		screenPerformEditBuyerProfile.addItemByPath(ConstantsPackages.PATH_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
		if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
			item = ConstantsBuyers.HOME_NETWORK_724_32;
		} else {
			item = ConstantsBuyers.HOME_NETWORK_724_99;
		}
		screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
				ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		if (screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI).contains(ConstantsBuyers.VALIDATED)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsPackages.SUFFIX_BOI);
			item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
			if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
				item = ConstantsBuyers.HOME_NETWORK_724_32;
			} else {
				item = ConstantsBuyers.HOME_NETWORK_724_99;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		}
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.moveToPageHeader();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.moveToPageHeader();
		screenPerformEditPackageProfile.clickOnUpdateToButton();
		String valueSync = screenPerformEditPackageProfile.getBuyerValuesFromSyncPage();
		screenPerformEditPackageProfile.clickOnUseBuyerValuesAllButton();
		screenPerformEditPackageProfile.clickOnApplyButton();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		homeNetwork = screenPerformEditPackageProfile.getValueFromFieldText("fieldHomeNetwork");

		assertEquals(valueSync, homeNetwork);
	}

	/**
	 * @testLinkId T-GPRI-3163
	 * @summary Package Values
	 */
	@Test
	public void test_2344_PackageValues() throws Exception {
		String homeNetwork = "";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsPackages.SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(ConstantsPackages.SUFFIX_BOI);
		screenPerformEditBuyerProfile.clickOnSelectItemsButton();
		screenPerformEditBuyerProfile.addItemByPath("");
		screenPerformEditBuyerProfile.removeItemByPath("");
		screenPerformEditBuyerProfile.addItemByPath(ConstantsPackages.PATH_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
		if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
			item = ConstantsBuyers.HOME_NETWORK_724_32;
		} else {
			item = ConstantsBuyers.HOME_NETWORK_724_99;
		}
		screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
				ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		if (screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI).contains(ConstantsBuyers.VALIDATED)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsPackages.SUFFIX_BOI);
			item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
			if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
				item = ConstantsBuyers.HOME_NETWORK_724_32;
			} else {
				item = ConstantsBuyers.HOME_NETWORK_724_99;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		}
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.moveToPageHeader();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.moveToPageHeader();
		screenPerformEditPackageProfile.clickOnUpdateToButton();
		screenPerformEditPackageProfile.clickOkOkPopUpUpdatePackage();
		screenPerformEditPackageProfile.moveToPageBottom();
		String valueSync = screenPerformEditPackageProfile.getHomeNetworkValuesFromPackageSyncPage();
		screenPerformEditPackageProfile.clickOnUsePackageValuesAllButton();
		screenPerformEditPackageProfile.clickOnApplyButton();
		screenPerformEditPackageProfile.clickOkOkPopUpUpdatePackage();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		homeNetwork = screenPerformEditPackageProfile.getValueFromFieldText("fieldHomeNetwork");

		assertEquals(valueSync, homeNetwork);
	}

	/**
	 * @testLinkId T-GPRI-3164
	 * @summary Values of buyers and packages
	 */
	@Test
	public void test_2346_ValuesOfBuyersAndPackages() throws Exception {
		String homeNetwork = "";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsPackages.SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(ConstantsPackages.SUFFIX_BOI);
		screenPerformEditBuyerProfile.clickOnSelectItemsButton();
		screenPerformEditBuyerProfile.addItemByPath("");
		screenPerformEditBuyerProfile.removeItemByPath("");
		screenPerformEditBuyerProfile.addItemByPath(ConstantsPackages.PATH_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
		if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
			item = ConstantsBuyers.HOME_NETWORK_724_32;
		} else {
			item = ConstantsBuyers.HOME_NETWORK_724_99;
		}
		screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
				ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		if (screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI).contains(ConstantsBuyers.VALIDATED)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsPackages.SUFFIX_BOI);
			item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
			if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
				item = ConstantsBuyers.HOME_NETWORK_724_32;
			} else {
				item = ConstantsBuyers.HOME_NETWORK_724_99;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		}
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.moveToPageHeader();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.moveToPageHeader();
		screenPerformEditPackageProfile.clickOnUpdateToButton();
		String valueSync = screenPerformEditPackageProfile.getBuyerValuesFromSyncPage();
		screenPerformEditPackageProfile.clickOnUseBuyerValuesParametersButton();
		screenPerformEditPackageProfile.clickOnUsePackageValuesCustomizationButton();
		screenPerformEditPackageProfile.clickOnApplyButton();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		homeNetwork = screenPerformEditPackageProfile.getValueFromFieldText("fieldHomeNetwork");

		assertEquals(valueSync, homeNetwork);
	}
}
