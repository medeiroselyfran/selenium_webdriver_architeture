package com.lge.gpri.tests.packages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.packages.ScreenPerformSmartCAPackage;
import com.lge.gpri.tests.AbstractTestScenary;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestRemoveSmartCAPackage extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2883
	 * @summary Remove Package Confirmed
	 */
	@Test
	public void test_1200_RemovePackageConfirmed() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.NEXTEL_BRAZIL);
		screenPerformListPackages.searchPackage("Smart CA");
		String status = screenPerformListPackages.getStatusPackageConfirmation();
		if (status.contains(ConstantsPackages.UNDER_EDITION)) {
			screenPerformListPackages.clickOnConfirmPackageButton();
			screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
		}
		boolean isUnderEdition = screenPerformListPackages.clickOnRemovePackageButton();
		assertFalse(isUnderEdition);
	}

	/**
	 * @testLinkId T-GPRI-2884
	 * @summary Remove Package Confirmed
	 */
	@Test
	public void test_1198_CancelRemoval() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		screenPerformListPackages.openPage();
		CreateSmartCAPackage();

		screenPerformListPackages.searchPackage("Smart CA");
		String status = screenPerformListPackages.getStatusPackageConfirmation();
		if (status.contains(ConstantsPackages.UNDER_EDITION)) {
			screenPerformListPackages.clickOnConfirmPackageButton();
			screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
			screenPerformListPackages.clickOnOkConfirmSmartCA();
		}
		boolean isUnderEdition = screenPerformListPackages.clickOnRemovePackageButton();
		assertFalse(isUnderEdition);
	}

	/**
	 * @testLinkId T-GPRI-2885
	 * @summary Remove Package Confirmed
	 */
	@Test
	public void test_1199_ConfirmRemoval() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		screenPerformListPackages.openPage();
		ArrayList<String> packageCreated = CreateSmartCAPackage();

		screenPerformListPackages.searchPackage("Smart CA");
		String status = screenPerformListPackages.getStatusPackageConfirmation();
		if (status.contains(ConstantsPackages.UNDER_EDITION)) {
			screenPerformListPackages.clickOnRemovePackageButton();
			screenPerformListPackages.confirmRemovePackage();
		}
		boolean resultok = screenPerformListPackages.setBuyer(packageCreated.get(0));
		if (resultok) {
			screenPerformListPackages.searchPackage(packageCreated.get(1));
			assertEquals(ConstantsPackages.NO_PACKAGE_AVAILABLE,
					screenPerformListPackages.getMessageForNoPackagesAvailable());
		} else {
			assertFalse(resultok);
		}

	}

	/**
	 * Method for create smart ca package
	 */
	private ArrayList<String> CreateSmartCAPackage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		ArrayList<String> infoPackage = new ArrayList<String>();

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TESTSMARTCA, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.NORMAL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF,
				ConstantsPackages.SW_VERSION_FROM, ConstantsPackages.SW_VERSION_TO);
		screenPerformSmartCAPackage.moveToPageHeader();
		String buyerName = screenPerformSmartCAPackage.getBuyer();
		String packageName = screenPerformSmartCAPackage.getPackageName();
		infoPackage.add(buyerName);
		infoPackage.add(packageName);
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		screenPerformSmartCAPackage.clickOnOkPackageSuccessCreated();
		return infoPackage;
	}

}
