package com.lge.gpri.tests.packages.editProfile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static com.lge.gpri.helpers.constants.ConstantsPackages.*;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.view.ScreenPerformViewValidator;
import com.lge.gpri.tests.AbstractTestScenary;


/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Edit Package Profile.
*  
* @author Cayk Lima Barreto
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestInsertValueByPerProfile extends AbstractTestScenary{
	
	/**
	* @testLinkId T-GPRI-3177
	* @summary Insert values invalid associated with 2 per profile
	*/
	@Test
	public void test_2363_InsertValuesInvalidAssociatedWithTwoPerProfile() throws Exception{
		String txtWarning = "";
		String option24hourProfile1 = "";
		String option24hourProfile2 = "";
		String option24hourProfile3 = "";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformViewValidator screenPerformViewValidator = new ScreenPerformViewValidator(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.openValidatorsPageOnAnotherTab();
		screenPerformViewValidator.searchValidator(TIME_FORMAT_VALIDATOR);
		screenPerformViewValidator.clickOnTimeFormatValidatorButton();
		screenPerformViewValidator.setNewValue(TIME_FORMAT_24_HOUR);
		screenPerformViewValidator.clickOnAddNewValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformViewValidator.switchTabs(0);
		screenPerformListPackages.setModel(LGH815);
		screenPerformListPackages.setBuyer(TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.searchItemSelectItems("Time Format");
		screenPerformEditPackageProfile.clickOnTimeFormatButton();
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.switchTabs(1);
		screenPerformViewValidator.clickOnDeleteTimeFormatButton();
		screenPerformViewValidator.clickOnConfirmDeletedValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformEditPackageProfile.closeTab();
		screenPerformViewValidator.switchTabs(0);
		screenPerformEditPackageProfile.clickOnTimeFormatTypeButton();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfileOption();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72402Options();
		screenPerformEditPackageProfile.clickOnOption24HourPerProfile72402();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72403Options();
		screenPerformEditPackageProfile.clickOnOption24HourPerProfile72403();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72404Options();
		screenPerformEditPackageProfile.clickOnOption24HourPerProfile72404();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		txtWarning = screenPerformEditPackageProfile.getText("spanTimeFormat");
		screenPerformEditPackageProfile.clickOnCancelSaveButton();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		screenPerformEditPackageProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		option24hourProfile1 = screenPerformEditPackageProfile.getSelectedOption("btnTimeFormatPerProfile72402");
		option24hourProfile2 = screenPerformEditPackageProfile.getSelectedOption("btnTimeFormatPerProfile72403");
		option24hourProfile3 = screenPerformEditPackageProfile.getSelectedOption("btnTimeFormatPerProfile72404");
		
		assertEquals(VALUE_NO_LONGER_ACCEPTABLE, txtWarning);
		assertEquals(TIME_FORMAT_24_HOUR, option24hourProfile1);
		assertEquals(TIME_FORMAT_24_HOUR, option24hourProfile2);
		assertEquals(TIME_FORMAT_24_HOUR, option24hourProfile3);
	}
	
	/**
	* @testLinkId T-GPRI-3178
	* @summary Insert value invalid for only one per profile
	*/
	@Test
	public void test_2366_InsertValueInvalidForOnlyOnePerProfile() throws Exception{
		String txtWarning = "";
		String option12hoursProfile1 = "";
		String option24hourProfile2 = "";
		String option24hourProfile3 = "";
		boolean profile2Options = false;
		boolean profile3Options = false;
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformViewValidator screenPerformViewValidator = new ScreenPerformViewValidator(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.openValidatorsPageOnAnotherTab();
		screenPerformViewValidator.searchValidator(TIME_FORMAT_VALIDATOR);
		screenPerformViewValidator.clickOnTimeFormatValidatorButton();
		screenPerformViewValidator.setNewValue(TIME_FORMAT_24_HOUR);
		screenPerformViewValidator.clickOnAddNewValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformViewValidator.switchTabs(0);
		screenPerformListPackages.setModel(LGH815);
		screenPerformListPackages.setBuyer(TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.searchItemSelectItems("Time Format");
		screenPerformEditPackageProfile.clickOnTimeFormatButton();
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.switchTabs(1);
		screenPerformViewValidator.clickOnDeleteTimeFormatButton();
		screenPerformViewValidator.clickOnConfirmDeletedValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformEditPackageProfile.closeTab();
		screenPerformViewValidator.switchTabs(0);
		screenPerformEditPackageProfile.clickOnTimeFormatTypeButton();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfileOption();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72402Options();
		screenPerformEditPackageProfile.clickOnOption12HoursPerProfile72402();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72403Options();
		screenPerformEditPackageProfile.clickOnOption24HourPerProfile72403();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72404Options();
		screenPerformEditPackageProfile.clickOnOption24HourPerProfile72404();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		txtWarning = screenPerformEditPackageProfile.getText("spanTimeFormat");
		screenPerformEditPackageProfile.clickOnCancelSaveButton();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		screenPerformEditPackageProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		option12hoursProfile1 = screenPerformEditPackageProfile.getSelectedOption("btnTimeFormatPerProfile72402");
		option24hourProfile2 = screenPerformEditPackageProfile.getSelectedOption("btnTimeFormatPerProfile72403");
		option24hourProfile3 = screenPerformEditPackageProfile.getSelectedOption("btnTimeFormatPerProfile72404");
		profile2Options = screenPerformEditPackageProfile.verifyOptions("btnTimeFormatPerProfile72403");
		profile3Options = screenPerformEditPackageProfile.verifyOptions("btnTimeFormatPerProfile72404");
		
		assertEquals(VALUE_NO_LONGER_ACCEPTABLE, txtWarning);
		assertEquals(TIME_FORMAT_12_HOURS, option12hoursProfile1);
		assertEquals(TIME_FORMAT_24_HOUR, option24hourProfile2);
		assertEquals(TIME_FORMAT_24_HOUR, option24hourProfile3);
		assertTrue(profile2Options);
		assertTrue(profile3Options);
	}
	
	/**
	* @testLinkId T-GPRI-3179
	* @summary Insert value invalid for 1 per profile not applied
	*/
	@Test
	public void test_2367_InsertValueInvalidForOnePerProfileNotApplied() throws Exception{
		String txtWarning = "";
		String option24hoursProfile1 = "";
		String option24hourProfile2 = "";
		String option24hourProfile3 = "";
		boolean profile1Options = true;
		boolean profile2Options = false;
		boolean profile3Options = false;
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformViewValidator screenPerformViewValidator = new ScreenPerformViewValidator(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.openValidatorsPageOnAnotherTab();
		screenPerformViewValidator.searchValidator(TIME_FORMAT_VALIDATOR);
		screenPerformViewValidator.clickOnTimeFormatValidatorButton();
		screenPerformViewValidator.setNewValue(TIME_FORMAT_24_HOUR);
		screenPerformViewValidator.clickOnAddNewValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformViewValidator.switchTabs(0);
		screenPerformListPackages.setModel(LGH815);
		screenPerformListPackages.setBuyer(TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.searchItemSelectItems("Time Format");
		screenPerformEditPackageProfile.clickOnTimeFormatButton();
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.switchTabs(1);
		screenPerformViewValidator.clickOnDeleteTimeFormatButton();
		screenPerformViewValidator.clickOnConfirmDeletedValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformEditPackageProfile.closeTab();
		screenPerformViewValidator.switchTabs(0);
		screenPerformEditPackageProfile.clickOnTimeFormatTypeButton();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfileOption();
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72402("Enable");
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72403("Enable");
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72404("Enable");
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72402Options();
		screenPerformEditPackageProfile.clickOnOption24HourPerProfile72402();
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72402("Disable");
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72403Options();
		screenPerformEditPackageProfile.clickOnOption24HourPerProfile72403();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72404Options();
		screenPerformEditPackageProfile.clickOnOption24HourPerProfile72404();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		txtWarning = screenPerformEditPackageProfile.getText("spanTimeFormat");
		screenPerformEditPackageProfile.clickOnCancelSaveButton();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		screenPerformEditPackageProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		option24hoursProfile1 = screenPerformEditPackageProfile.getSelectedOption("btnTimeFormatPerProfile72402");
		option24hourProfile2 = screenPerformEditPackageProfile.getSelectedOption("btnTimeFormatPerProfile72403");
		option24hourProfile3 = screenPerformEditPackageProfile.getSelectedOption("btnTimeFormatPerProfile72404");
		profile1Options = screenPerformEditPackageProfile.verifyOptions("btnTimeFormatPerProfile72402");
		profile2Options = screenPerformEditPackageProfile.verifyOptions("btnTimeFormatPerProfile72403");
		profile3Options = screenPerformEditPackageProfile.verifyOptions("btnTimeFormatPerProfile72404");
		
		assertEquals(VALUE_NO_LONGER_ACCEPTABLE, txtWarning);
		assertEquals(TIME_FORMAT_24_HOURS, option24hoursProfile1);
		assertEquals(TIME_FORMAT_24_HOUR, option24hourProfile2);
		assertEquals(TIME_FORMAT_24_HOUR, option24hourProfile3);
		assertFalse(profile1Options);
		assertTrue(profile2Options);
		assertTrue(profile3Options);
	}
	
	/**
	* @testLinkId T-GPRI-3180
	* @summary Insert value invalid for 2 per profile not applied
	*/
	@Test
	public void test_2368_InsertValueInvalidForTwoPerProfileNotApplied() throws Exception{
		
		boolean existTimeFormatPerProfile1 = true;
		boolean existTimeFormatPerProfile2 = true;
		boolean existTimeFormatPerProfile3 = true;
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformViewValidator screenPerformViewValidator = new ScreenPerformViewValidator(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.openValidatorsPageOnAnotherTab();
		screenPerformViewValidator.searchValidator(TIME_FORMAT_VALIDATOR);
		screenPerformViewValidator.clickOnTimeFormatValidatorButton();
		screenPerformViewValidator.setNewValue(TIME_FORMAT_24_HOUR);
		screenPerformViewValidator.clickOnAddNewValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformViewValidator.switchTabs(0);
		screenPerformListPackages.setModel(LGH815);
		screenPerformListPackages.setBuyer(TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.searchItemSelectItems("Time Format");
		screenPerformEditPackageProfile.clickOnTimeFormatButton();
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.switchTabs(1);
		screenPerformViewValidator.clickOnDeleteTimeFormatButton();
		screenPerformViewValidator.clickOnConfirmDeletedValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformEditPackageProfile.closeTab();
		screenPerformViewValidator.switchTabs(0);
		screenPerformEditPackageProfile.clickOnTimeFormatTypeButton();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfileOption();
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72402("Enable");
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72403("Enable");
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72404("Enable");
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72402Options();
		screenPerformEditPackageProfile.clickOnOption24HourPerProfile72402();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72403Options();
		screenPerformEditPackageProfile.clickOnOption24HourPerProfile72403();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72404Options();
		screenPerformEditPackageProfile.clickOnOption24HourPerProfile72404();
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72402("Disable");
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72403("Disable");
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72404("Disable");
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		existTimeFormatPerProfile1 = screenPerformEditPackageProfile.existsElement("btnTimeFormatPerProfile72402");
		existTimeFormatPerProfile2 = screenPerformEditPackageProfile.existsElement("btnTimeFormatPerProfile72403");
		existTimeFormatPerProfile3 = screenPerformEditPackageProfile.existsElement("btnTimeFormatPerProfile72404");
		
		assertFalse(existTimeFormatPerProfile1);
		assertFalse(existTimeFormatPerProfile2);
		assertFalse(existTimeFormatPerProfile3);
	}
	
	/**
	* @testLinkId T-GPRI-3181
	* @summary Insert value empty for 2 per profile not applied
	*/
	@Test
	public void test_2369_InsertValueEmptyForTwoPerProfileNotApplied() throws Exception{
		
		boolean existTimeFormatPerProfile1 = true;
		boolean existTimeFormatPerProfile2 = true;
		boolean existTimeFormatPerProfile3 = true;
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformViewValidator screenPerformViewValidator = new ScreenPerformViewValidator(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.openValidatorsPageOnAnotherTab();
		screenPerformViewValidator.searchValidator(TIME_FORMAT_VALIDATOR);
		screenPerformViewValidator.clickOnTimeFormatValidatorButton();
		screenPerformViewValidator.setNewValue(TIME_FORMAT_24_HOUR);
		screenPerformViewValidator.clickOnAddNewValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformViewValidator.switchTabs(0);
		screenPerformListPackages.setModel(LGH815);
		screenPerformListPackages.setBuyer(TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.searchItemSelectItems("Time Format");
		screenPerformEditPackageProfile.clickOnTimeFormatButton();
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.switchTabs(1);
		screenPerformViewValidator.clickOnDeleteTimeFormatButton();
		screenPerformViewValidator.clickOnConfirmDeletedValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformEditPackageProfile.closeTab();
		screenPerformViewValidator.switchTabs(0);
		screenPerformEditPackageProfile.clickOnTimeFormatTypeButton();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfileOption();
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72402("Enable");
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72403("Enable");
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72402Options();
		screenPerformEditPackageProfile.clickOnEmptyOptionPerProfile72402();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72403Options();
		screenPerformEditPackageProfile.clickOnEmptyOptionPerProfile72403();
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72402("Disable");
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72403("Disable");
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72404("Disable");
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		existTimeFormatPerProfile1 = screenPerformEditPackageProfile.existsElement("btnTimeFormatPerProfile72402");
		existTimeFormatPerProfile2 = screenPerformEditPackageProfile.existsElement("btnTimeFormatPerProfile72403");
		existTimeFormatPerProfile3 = screenPerformEditPackageProfile.existsElement("btnTimeFormatPerProfile72404");
		
		assertFalse(existTimeFormatPerProfile1);
		assertFalse(existTimeFormatPerProfile2);
		assertFalse(existTimeFormatPerProfile3);
	}
	
	/**
	* @testLinkId T-GPRI-3182
	* @summary Insert value empty for 2 per profile applied
	*/
	@Test
	public void test_2370_InsertValueEmptyForTwoPerProfileApplied() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformViewValidator screenPerformViewValidator = new ScreenPerformViewValidator(getDriver());
		String txtWarning = "";
		String fieldEmpty = "";
		
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.openValidatorsPageOnAnotherTab();
		screenPerformViewValidator.searchValidator(TIME_FORMAT_VALIDATOR);
		screenPerformViewValidator.clickOnTimeFormatValidatorButton();
		screenPerformViewValidator.setNewValue(TIME_FORMAT_24_HOUR);
		screenPerformViewValidator.clickOnAddNewValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformViewValidator.switchTabs(0);
		screenPerformListPackages.setModel(LGH815);
		screenPerformListPackages.setBuyer(TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.verifyIfPackageHasItemsAndClickToSelectItems();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.searchItemSelectItems("Time Format");
		screenPerformEditPackageProfile.clickOnTimeFormatButton();
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.switchTabs(1);
		screenPerformViewValidator.clickOnDeleteTimeFormatButton();
		screenPerformViewValidator.clickOnConfirmDeletedValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformEditPackageProfile.closeTab();
		screenPerformViewValidator.switchTabs(0);
		screenPerformEditPackageProfile.clickOnTimeFormatTypeButton();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfileOption();
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72402("Enable");
		screenPerformEditPackageProfile.clickOnCheckBoxProfile72403("Enable");
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72402Options();
		screenPerformEditPackageProfile.clickOnEmptyOptionPerProfile72402();
		screenPerformEditPackageProfile.clickOnTimeFormatPerProfile72403Options();
		screenPerformEditPackageProfile.clickOnEmptyOptionPerProfile72403();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		txtWarning = screenPerformEditPackageProfile.getText("warningCannotSavePackage");
		screenPerformEditPackageProfile.clickOnOkButtonWarningCannotSavePackage();
		fieldEmpty = screenPerformEditPackageProfile.getText("msgFieldEmpty");
		
		assertEquals(MSG_WARNING_CANNOT_SAVE_PACKAGE, txtWarning);
		assertEquals(MSG_WARNING_FIELD_EMPTY, fieldEmpty);
	}
}

