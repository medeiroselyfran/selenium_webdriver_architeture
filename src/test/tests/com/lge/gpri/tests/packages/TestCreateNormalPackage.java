package com.lge.gpri.tests.packages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.packages.ScreenPerformNormalPackage;
import com.lge.gpri.screens.packages.ScreenPerformSmartCAPackage;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Create Normal
 * Package.
 * 
 * @author Fernando Pinheiro Refactor methods and create new metods - Fernando
 *         Pinheiro - 03/11/17 Refactor methods and create new metods - Fernando
 *         Pinheiro - 07/11/17
 * @author Gabriel Costa
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCreateNormalPackage extends AbstractTestScenary {
	/**
	 * @testLinkId T-GPRI-2756
	 * @summary Create Normal Package With All Fields Requireds
	 */
	@Test
	public void test_1116_CreateNormalPackageWithAllFieldsRequireds() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		int numberPackage = 0;
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);

		String namePackage = screenPerformListPackages.getExistingPackage();

		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F_)) {
			numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {

				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.FIRST,
						ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}

		} else if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F)) {
			numberPackage = Integer.parseInt(namePackage.split("(?<=\\D)(?=\\d)")[1]);
			screenPerformListPackages.waitLoading();
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST_F + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST_F + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}
		} else {
			screenPerformListPackages.waitLoading();
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}
			screenPerformNormalPackage.clickOnKeepConfirmationButton();
			String txtPackageCreateSuccess = screenPerformNormalPackage.getTextFrom("txtPackageCreatedSuccess");
			assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
		}
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-2757
	 * @summary Create Normal Package With More Than One Buyer
	 */
	@Test
	public void test_1115_CreateNormalPackageWithMoreThanOneBuyer() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		if (screenPerformListPackages.verifyPackageStatus(ConstantsPackages.OI_BRAZIL_BOI, "Under")) {
			screenPerformListPackages.clickOnConfirmPackageButton();
			screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
			screenPerformListPackages.waitLoading();
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.OI_BRAZIL_BOI,
					ConstantsPackages.PACKAGE_NAME_TEST_MORE_THAN_ONE_BUYER, ConstantsPackages.TODAY,
					ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_6,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_N,
					ConstantsPackages.UI_VERSION_5_1, ConstantsPackages.VOICE_OVER_NONE);
			screenPerformNormalPackage.setBuyerValues(ConstantsPackages.TIM_BRAZIL_BTM);
		} else {
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.OI_BRAZIL_BOI,
					ConstantsPackages.PACKAGE_NAME_TEST_MORE_THAN_ONE_BUYER, ConstantsPackages.TODAY,
					ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_6,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_N,
					ConstantsPackages.UI_VERSION_5_1, ConstantsPackages.VOICE_OVER_NONE);
			screenPerformNormalPackage.setBuyerValues(ConstantsPackages.TIM_BRAZIL_BTM);
		}
		screenPerformNormalPackage.clickOnKeepConfirmationButton();
		String secondeBuyer = screenPerformNormalPackage.getBuyer();
		assertEquals(ConstantsPackages.TIM_BRAZIL_BTM, secondeBuyer);
	}

	/**
	 * @testLinkId T-GPRI-2758
	 * @summary Create Normal Package From Another Package
	 */
	@Test
	public void test_1117_CreateNormalPackageFromAnotherPackage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);

		String namePackage = screenPerformListPackages.getExistingPackage();

		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F)) {
			int numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOkConfirmSmartCA();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsFromAnotherPackage(ConstantsPackages.LGH815,
						ConstantsPackages.FIRST, ConstantsPackages.FIRST, ConstantsPackages.FIRST,
						ConstantsPackages.PACKAGE_NAME_TEST_F + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.CARRIER_AGGREGATION, ConstantsPackages.GRID_ROW_COLUMN_5X5,
						ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2,
						ConstantsPackages.VOICE_OVER_NONE);
			} else {
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsFromAnotherPackage(ConstantsPackages.LGH815,
						ConstantsPackages.FIRST, ConstantsPackages.SECOND, ConstantsPackages.FIRST,
						ConstantsPackages.PACKAGE_NAME_TEST_F + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.CARRIER_AGGREGATION, ConstantsPackages.GRID_ROW_COLUMN_5X5,
						ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2,
						ConstantsPackages.VOICE_OVER_NONE);
			}
		} else if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F_)) {
			int numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {

				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.FIRST,
						ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}
		} else {
			int numberPackage = Integer.parseInt(namePackage.split("(?<=\\D)(?=\\d)")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.FIRST,
						ConstantsPackages.PACKAGE_NAME_TEST + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}
		}
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnKeepConfirmationButton();
		String txtPackageCreateSuccess = screenPerformNormalPackage.getTextFrom("txtPackageCreatedSuccess");
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
	}

	/**
	 * @testLinkId T-GPRI-2759
	 * @summary Create Normal Package With Keep Confirmation
	 */
	@Test
	public void test_1118_CreateNormalPackageWithKeepConfirmation() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TLF_GROUP_EUROPE_6TL);
		if ("Under".equals(screenPerformListPackages.getTextFrom("status").split(" ")[0])) {
			screenPerformListPackages.ToogleAll();
			if (screenPerformListPackages.isDisplayedPopUpConfirmSmartCA()) {
				screenPerformListPackages.clickOnOkConfirmSmartCA();
			}
		}
		String namePackage = screenPerformListPackages.getExistingPackage();

		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F)) {
			int numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
					ConstantsPackages.TLF_GROUP_EUROPE_6TL, ConstantsPackages.PACKAGE_NAME_TEST_F + (numberPackage + 1),
					ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_6,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_N,
					ConstantsPackages.UI_VERSION_5_1, ConstantsPackages.VOICE_OVER_NONE);

		} else {
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
					ConstantsPackages.TLF_GROUP_EUROPE_6TL, ConstantsPackages.PACKAGE_NAME_TEST_F_1,
					ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_6,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_N,
					ConstantsPackages.UI_VERSION_5_1, ConstantsPackages.VOICE_OVER_NONE);
		}
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnKeepConfirmationButton();
		screenPerformNormalPackage.clickOnConfirmationSucessOkButton();
		String statusMother = screenPerformListPackages.getPackageStatusMother();
		String statusChildOne = screenPerformListPackages.getPackageStatusChildOne();
		String statusChildTwo = screenPerformListPackages.getPackageStatusChildTwo();
		String st1[] = statusChildOne.split(" ");
		String st2[] = statusChildTwo.split(" ");
		assertEquals(ConstantsPackages.UNDER_EDITION, statusMother);
		assertEquals(ConstantsPackages.CONFIRMED_BY, st1[0]);
		assertEquals(ConstantsPackages.CONFIRMED_BY, st2[0]);
	}

	/**
	 * @testLinkId T-GPRI-2760
	 * @summary Create Normal Package Open All Buyers
	 */
	@Test
	public void test_1119_CreateNormalPackageOpenAllBuyers() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TLF_GROUP_EUROPE_6TL);
		screenPerformListPackages.ToogleAll();

		String namePackage = screenPerformListPackages.getExistingPackage();

		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F_)) {
			int numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.FIRST,
						ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}

		} else if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F)) {
			int numberPackage = Integer.parseInt(namePackage.split("(?<=\\D)(?=\\d)")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TLF_GROUP_EUROPE_6TL,
						ConstantsPackages.PACKAGE_NAME_TEST_F + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_6,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_N,
						ConstantsPackages.UI_VERSION_5_1, ConstantsPackages.VOICE_OVER_NONE);
			} else {
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.FIRST,
						ConstantsPackages.PACKAGE_NAME_TEST_F + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}
		} else {
			int numberPackage = Integer.parseInt(namePackage.split("(?<=\\D)(?=\\d)")[1]);
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
					ConstantsPackages.TLF_GROUP_EUROPE_6TL, ConstantsPackages.PACKAGE_NAME_TEST + (numberPackage + 1),
					ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_6,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_N,
					ConstantsPackages.UI_VERSION_5_1, ConstantsPackages.VOICE_OVER_NONE);
		}
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnOpenAllBuyers();
		screenPerformNormalPackage.clickOnConfirmationSucessOkButton();
		ArrayList<String> status = screenPerformListPackages.getAllStatus();
		for (int i = 0; i < status.size(); i++) {
			assertTrue(status.get(i).contains(ConstantsPackages.UNDER_EDITION));
		}
	}

	/**
	 * @testLinkId T-GPRI-2761
	 * @summary Create Normal Package Open only buyers that changed
	 */
	@Test
	public void test_1120_CreateNormalPackageOpenOnlyBuyersThatChanged() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TLF_GROUP_EUROPE_6TL);
		screenPerformListPackages.upgradeRevisonOfBuyer(ConstantsBuyers.SUFFIX_TLF);
		screenPerformListPackages.ToogleAll();

		String namePackage = screenPerformListPackages.getExistingPackage();

		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F)) {
			int numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
					ConstantsPackages.TLF_GROUP_EUROPE_6TL, ConstantsPackages.PACKAGE_NAME_TEST_F + (numberPackage + 1),
					ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_6,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_N,
					ConstantsPackages.UI_VERSION_5_1, ConstantsPackages.VOICE_OVER_NONE);

		} else {
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
					ConstantsPackages.TLF_GROUP_EUROPE_6TL, ConstantsPackages.PACKAGE_NAME_TEST_F_1,
					ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_6,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_N,
					ConstantsPackages.UI_VERSION_5_1, ConstantsPackages.VOICE_OVER_NONE);
		}
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnOpenOnlyBuyersThatChanged();
		screenPerformNormalPackage.clickOnConfirmationSucessOkButton();
		ArrayList<String> status = screenPerformListPackages.getAllStatus();
		ArrayList<String[]> revisions = screenPerformListPackages.getAllRevison();
		assertTrue(status.get(0).contains(ConstantsPackages.UNDER_EDITION));
		for (int i = 1; i < status.size(); i++) {
			boolean revisionUpdated = revisions.get(i)[0].equals(revisions.get(i)[1]);
			assertTrue((revisionUpdated && status.get(i).contains(ConstantsPackages.CONFIRMED))
					|| (!revisionUpdated && status.get(i).contains(ConstantsPackages.UNDER_EDITION)));
		}
	}

	/**
	 * @testLinkId T-GPRI-2762
	 * @summary Create Normal Package With Last Package Under Edition
	 */
	@Test
	public void test_1121_CreateNormalPackageWithLastPackageUnderEdition() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.WIND_ITALY);
		Thread.sleep(1000);

		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		if (nameStatus.equals("Under")) {
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.WIND_ITALY,
					ConstantsPackages.PACKAGE_NAME_TEST, ConstantsPackages.TODAY,
					ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
					ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
			screenPerformNormalPackage.clickOnSaveCreatedButton();
			screenPerformNormalPackage.clickOnClosePopup();
			String errorBuyer = screenPerformNormalPackage.getTextFrom("comboboxBuyer");
			assertEquals(ConstantsPackages.SELECT_BUYER, errorBuyer);
		} else {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnOKButtonAfterReopenPackage();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.waitLoading();
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.WIND_ITALY,
					ConstantsPackages.PACKAGE_NAME_TEST, ConstantsPackages.TODAY,
					ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
					ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
			screenPerformNormalPackage.clickOnSaveCreatedButton();
			screenPerformNormalPackage.clickOnClosePopup();
			String errorBuyer = screenPerformNormalPackage.getTextFrom("comboboxBuyer");
			assertEquals(ConstantsPackages.SELECT_BUYER, errorBuyer);
		}
	}

	/**
	 * @testLinkId T-GPRI-2763
	 * @summary Create Normal Package Without Name
	 */
	@Test
	public void test_1122_CreateNormalPackageWithoutName() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
				ConstantsPackages.OPEN_UNIFIED_LATIN_AMERICA_SCA, "", ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1", ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.ANDROID_VERSION_5, ConstantsPackages.GRID_ROW_COLUMN_5X5,
				ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnClosePopup();
		String txtMessageError = screenPerformNormalPackage.getTextFrom("errorNamePackage");
		assertEquals(ConstantsPackages.WITHOUT_NAME, txtMessageError);
	}

	/**
	 * @testLinkId T-GPRI-2764
	 * @summary Create Normal Package With Double Quote
	 */
	@Test
	public void test_1123_CreateNormalPackageWithDoubleQuote() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
				ConstantsPackages.OPEN_UNIFIED_LATIN_AMERICA_SCA, ConstantsPackages.PACKAGE_NAME_WITH_DOUBLE_QUOTE,
				ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
				ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
				ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2,
				ConstantsPackages.VOICE_OVER_NONE);
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnClosePopup();
		String txtMessageError = screenPerformNormalPackage.getTextFrom("errorNamePackage");
		assertEquals(ConstantsPackages.PACKAGE_NAME_ERROR, txtMessageError);
	}

	/**
	 * @testLinkId T-GPRI-2765
	 * @summary Create Normal Package With Single Quote
	 */
	@Test
	public void test_1124_CreateNormalPackageWithSingleQuote() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
				ConstantsPackages.OPEN_UNIFIED_LATIN_AMERICA_SCA, ConstantsPackages.PACKAGE_NAME_WITH_SINGLE_QUOTE,
				ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
				ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
				ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2,
				ConstantsPackages.VOICE_OVER_NONE);
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnClosePopup();
		String txtMessageError = screenPerformNormalPackage.getTextFrom("errorNamePackage");
		assertEquals(ConstantsPackages.PACKAGE_NAME_ERROR, txtMessageError);
	}

	/**
	 * @testLinkId T-GPRI-2766
	 * @summary Create Normal Package With Back Slash
	 */
	@Test
	public void test_1125_CreateNormalPackageWithBackSlash() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
				ConstantsPackages.OPEN_UNIFIED_LATIN_AMERICA_SCA, ConstantsPackages.PACKAGE_NAME_WITH_BACKSLASH,
				ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
				ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
				ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2,
				ConstantsPackages.VOICE_OVER_NONE);
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnClosePopup();
		String txtMessageError = screenPerformNormalPackage.getTextFrom("errorNamePackage");
		assertEquals(ConstantsPackages.PACKAGE_NAME_ERROR, txtMessageError);
	}

	/**
	 * @testLinkId T-GPRI-2767
	 * @summary Create Normal Package From Smart CA
	 */
	@Test
	public void test_1126_CreateNormalPackageFromSmartCA() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);

		String namePackage = screenPerformListPackages.getExistingPackage();

		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F)) {
			int numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOkConfirmSmartCA();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreateSmartCAButton();
				screenPerformSmartCAPackage.setTypeNormalPackage();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST_F + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {

				screenPerformListPackages.clickOnCreateSmartCAButton();
				screenPerformSmartCAPackage.setTypeNormalPackage();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST_F + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}

		} else {
			screenPerformListPackages.waitLoading();
			int numberPackage = Integer.parseInt(namePackage.split("(?<=\\D)(?=\\d)")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreateSmartCAButton();
				screenPerformSmartCAPackage.setTypeNormalPackage();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST_F + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {
				screenPerformListPackages.clickOnCreateSmartCAButton();
				screenPerformSmartCAPackage.setTypeNormalPackage();
				screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "",
						ConstantsPackages.TIM_BRAZIL_BTM, ConstantsPackages.PACKAGE_NAME_TEST + (numberPackage + 1),
						ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}
		}
		String txtPackageCreateSuccess = screenPerformNormalPackage.getTextFrom("txtPackageCreatedSuccess");
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);

	}

	/**
	 * @testLinkId T-GPRI-2768
	 * @summary Create Normal Package With Cancel
	 */
	@Test
	public void test_1127_CreateNormalPackageWithCancel() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.TIM_BRAZIL_BTM,
				ConstantsPackages.PACKAGE_NAME_TEST, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1", ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.ANDROID_VERSION_5, ConstantsPackages.GRID_ROW_COLUMN_5X5,
				ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
		screenPerformNormalPackage.clickOnCancelPackage();
		screenPerformNormalPackage.clickOnConfirmCancelPackage();
		boolean LoadOnlyLatestPackage = getDriver().getPageSource().contains("Load only the latest package");
		assertTrue(LoadOnlyLatestPackage);
	}

	/**
	 * @testLinkId T-GPRI-2769
	 * @summary Create Normal Package With Name Already Existing
	 */
	@Test
	public void test_1128_CreateNormalPackageWithNameAlreadyExisting() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.ETB_COLOMBIA);
		String getNameOfPackage = screenPerformListPackages.getTextFrom("getNamePackage");
		String namePackage[] = getNameOfPackage.split(" ");
		getNameOfPackage = namePackage[1] + " ".concat(namePackage[2]);
		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.ETB_COLOMBIA,
				getNameOfPackage, ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
				ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
				ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2,
				ConstantsPackages.VOICE_OVER_NONE);
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnClosePopup();
		String txtMessageError = screenPerformNormalPackage.getTextFrom("errorNamePackage");
		assertEquals(ConstantsPackages.PACKAGE_ALREADY_EXISTING + " ".concat("\"" + getNameOfPackage + "\"" + "."),
				txtMessageError);
	}
}
