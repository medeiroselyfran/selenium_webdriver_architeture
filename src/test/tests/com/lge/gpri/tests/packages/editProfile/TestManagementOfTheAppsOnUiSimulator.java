package com.lge.gpri.tests.packages.editProfile;

import static com.lge.gpri.helpers.constants.ConstantsPackages.APP_1_ROW;
import static com.lge.gpri.helpers.constants.ConstantsPackages.APP_1_ROW_EMPTY;
import static com.lge.gpri.helpers.constants.ConstantsPackages.APP_2_COLUMN;
import static com.lge.gpri.helpers.constants.ConstantsPackages.APP_2_ROW;
import static com.lge.gpri.helpers.constants.ConstantsPackages.APP_FACEBOOK;
import static com.lge.gpri.helpers.constants.ConstantsPackages.APP_INSTAGRAM;
import static com.lge.gpri.helpers.constants.ConstantsPackages.CLARO_BRAZIL_CLR;
import static com.lge.gpri.helpers.constants.ConstantsPackages.EMPTY;
import static com.lge.gpri.helpers.constants.ConstantsPackages.FACEBOOK;
import static com.lge.gpri.helpers.constants.ConstantsPackages.INSTAGRAM;
import static com.lge.gpri.helpers.constants.ConstantsPackages.LGH815;
import static com.lge.gpri.helpers.constants.ConstantsPackages.MESSAGE_ONLY_FOR_TESTING;
import static com.lge.gpri.helpers.constants.ConstantsPackages.MSG_WARNING_CANNOT_SAVE_PACKAGE;
import static com.lge.gpri.helpers.constants.ConstantsPackages.MSG_WARNING_FIELD_EMPTY;
import static com.lge.gpri.helpers.constants.ConstantsPackages.VALUE_1;
import static com.lge.gpri.helpers.constants.ConstantsPackages.VALUE_2;
import static com.lge.gpri.helpers.constants.ConstantsPackages.WARNING_CANNOT_SAVE_PACKAGE;
import static com.lge.gpri.helpers.constants.ConstantsView.APP_FACEBOOK_VIEW;
import static com.lge.gpri.helpers.constants.ConstantsView.APP_INSTAGRAM_VIEW;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Management of
 * the Apps on UI Simulator to Edit Package Profile.
 * 
 * @author Cayk Lima Barreto
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestManagementOfTheAppsOnUiSimulator extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-3153
	 * @summary Add apps in different positions from package
	 */
	@Test
	public void test_2333_AddAppsInDifferentPositionsFromBuyer() throws Exception {
		boolean resultApp1 = false;
		boolean resultApp2 = false;
		boolean resultApp1OnView = false;
		boolean resultApp2OnView = false;
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(LGH815);
		screenPerformListPackages.setBuyer(CLARO_BRAZIL_CLR);
		if (!"Under Edition".equals(screenPerformListPackages.getPackageStatus())) {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
				screenPerformListPackages.waitLoadingOverlay();
			}
		}
		String namePackage = screenPerformListPackages.getNamePackageConfirmation();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		screenPerformEditPackageProfile.clickOnEditorModeButton();
		Thread.sleep(2000);
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.waitLoadingOverlay();
		if (!screenPerformEditPackageProfile.verifyAllItemsUnchecked()) {
			screenPerformEditPackageProfile.doubleClickOnElementInTree("Item Library");
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		}
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.addItemByPath("Customization::Menu::Android::Item::App");
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.waitLoadingOverlay();
		screenPerformEditPackageProfile.removeAllItemsOnProfilePackages();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		screenPerformEditPackageProfile.clickOnEditorModeButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickAppPositionOneButton();
		screenPerformEditPackageProfile.addApp(INSTAGRAM);
		screenPerformEditPackageProfile.clickOnSaveAppsButton();
		screenPerformEditPackageProfile.waitLoading();

		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		screenPerformEditPackageProfile.clickOnEditorModeButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickAppPositionTwoButton();
		screenPerformEditPackageProfile.addApp(FACEBOOK);
		screenPerformEditPackageProfile.clickOnSaveAppsButton();
		screenPerformEditPackageProfile.waitLoadingOverlay();

		screenPerformEditPackageProfile.clickOnSaveProfile();
		if (screenPerformEditPackageProfile.existsElement("warningModal")) {
			screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		}
		screenPerformEditPackageProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnCustomizationButton();
		screenPerformEditPackageProfile.waitLoadingOverlay();
		

		resultApp1 = screenPerformEditPackageProfile.checkApp1GridValues();
		resultApp2 = screenPerformEditPackageProfile.checkApp2GridValues();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(namePackage);
		screenPerformView.selectFirstItemResultSearch();
		Thread.sleep(2000);
		screenPerformView.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnCustomizationButton();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		
		resultApp1OnView = screenPerformViewValues.checkApp1GridValues();
		resultApp2OnView = screenPerformViewValues.checkApp2GridValues();

		assertTrue(resultApp1);
		assertTrue(resultApp2);
		assertTrue(resultApp1OnView);
		assertTrue(resultApp2OnView);
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3285
	 * @summary Add apps in same position from package
	 */
	@Test
	public void test_2336_AddAppsInSamePositionFromBuyer() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		boolean collision = false;
		boolean collisionOnView = false;

		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.onlyOneItemOnLibrary();
		screenPerformListPackages.setModel(LGH815);
		screenPerformListPackages.setBuyer(CLARO_BRAZIL_CLR);
		String namePackageConfirmation = screenPerformListPackages.getNamePackageConfirmation();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnEditorModeButton();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		screenPerformEditPackageProfile.clickAppPositionOneButton();
		screenPerformEditPackageProfile.addApp(INSTAGRAM);
		screenPerformEditPackageProfile.clickOnSaveAppsButton();
		screenPerformEditPackageProfile.clickOnEditorModeButton();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		screenPerformEditPackageProfile.clickAppPositionTwoButton();
		screenPerformEditPackageProfile.addApp(FACEBOOK);
		screenPerformEditPackageProfile.clickOnSaveAppsButton();
		screenPerformEditPackageProfile.setAppGridRow(APP_2_ROW, VALUE_1);
		screenPerformEditPackageProfile.setAppGridColumn(APP_2_COLUMN, VALUE_1);
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		collision = screenPerformEditPackageProfile.checkCollisionBetweenApps(APP_INSTAGRAM, APP_FACEBOOK);
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(namePackageConfirmation);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.waitLoading();
		screenPerformViewValues.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformViewValues.clickOnLockUnlockButton();
		collisionOnView = screenPerformViewValues.checkCollisionBetweenApps(APP_INSTAGRAM_VIEW, APP_FACEBOOK_VIEW);

		assertTrue(collision);
		assertTrue(collisionOnView);
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3157
	 * @summary Add apps without position from package
	 */
	@Test
	public void test_2337_AddAppsWithoutPositionFromBuyer() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		String warningMsg = "";
		String fieldEmptyMsg = "";
		boolean warning = false;

		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.onlyOneItemOnLibrary();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnEditorModeButton();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		screenPerformEditPackageProfile.clickAppPositionOneButton();
		screenPerformEditPackageProfile.addApp(INSTAGRAM);
		screenPerformEditPackageProfile.clickOnSaveAppsButton();
		screenPerformEditPackageProfile.clickOnCustomizationButton();
		screenPerformEditPackageProfile.setAppGridRow(APP_1_ROW, EMPTY);
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		warningMsg = screenPerformEditPackageProfile.getTextFrom(WARNING_CANNOT_SAVE_PACKAGE);
		warning = screenPerformEditPackageProfile.clickOnCannotSaveBuyerOkButton();
		fieldEmptyMsg = screenPerformEditPackageProfile.getTextFrom(APP_1_ROW_EMPTY);

		assertTrue(warning);
		assertEquals(MSG_WARNING_CANNOT_SAVE_PACKAGE, warningMsg);
		assertEquals(MSG_WARNING_FIELD_EMPTY, fieldEmptyMsg);
	}

	/**
	 * @throws Exception
	 * @testLinkId T-GPRI-3155
	 * @summary Edit positions of the apps to not overlap from package
	 */
	@Test
	public void test_2335_EditPositionsOfTheAppsToNotOverpalFromBuyer() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		boolean collision = false;
		boolean collisionAfterEdit = true;
		boolean collisionOnViewPage = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(LGH815);
		screenPerformListPackages.setBuyer(CLARO_BRAZIL_CLR);
		if (!"Under Edition".equals(screenPerformListPackages.getPackageStatus())) {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
				screenPerformListPackages.waitLoadingOverlay();
			}
		}
		String namePackage = screenPerformListPackages.getNamePackageConfirmation();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		screenPerformEditPackageProfile.clickOnEditorModeButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.waitLoadingOverlay();
		if (!screenPerformEditPackageProfile.verifyAllItemsUnchecked()) {
			screenPerformEditPackageProfile.doubleClickOnElementInTree("Item Library");
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		}
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.addItemByPath("Customization::Menu::Android::Item::App");
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.waitLoadingOverlay();
		screenPerformEditPackageProfile.removeAllItemsOnProfilePackages();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		screenPerformEditPackageProfile.clickOnEditorModeButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickAppPositionOneButton();
		screenPerformEditPackageProfile.addApp(INSTAGRAM);
		screenPerformEditPackageProfile.clickOnSaveAppsButton();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		screenPerformEditPackageProfile.clickOnEditorModeButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickAppPositionTwoButton();
		screenPerformEditPackageProfile.addApp(FACEBOOK);
		screenPerformEditPackageProfile.clickOnSaveAppsButton();
		if (screenPerformEditPackageProfile.existsElement("warningModal")) {
			screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		}
		
		screenPerformEditPackageProfile.setAppGridRow(APP_2_ROW, VALUE_1);
		screenPerformEditPackageProfile.setAppGridColumn(APP_2_COLUMN, VALUE_1);
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		collision = screenPerformEditPackageProfile.checkCollisionBetweenApps(APP_INSTAGRAM, APP_FACEBOOK);
		screenPerformEditPackageProfile.clickOnCustomizationButton();
		screenPerformEditPackageProfile.setAppGridRow(APP_2_ROW, VALUE_2);
		screenPerformEditPackageProfile.setAppGridColumn(APP_2_COLUMN, VALUE_1);
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnLockUnlockButton();
		collisionAfterEdit = screenPerformEditPackageProfile.checkCollisionBetweenApps(APP_INSTAGRAM, APP_FACEBOOK);
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(namePackage);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnUiStartButton();
		Thread.sleep(1000);
		screenPerformViewValues.clickOnLockUnlockButton();
		collisionOnViewPage = screenPerformViewValues.checkCollisionBetweenApps(APP_INSTAGRAM_VIEW, APP_FACEBOOK_VIEW);

		assertTrue(collision);
		assertFalse(collisionAfterEdit);
		assertFalse(collisionOnViewPage);
	}
}
