package com.lge.gpri.tests.packages.editProfile;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Sync on Edit Package Profile.
*  
* @author Gabriel Costa
* @author Cayk Lima Barreto
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSync extends AbstractTestScenary{

	/**
	* @testLinkId T-GPRI-3158
	* @summary Buyer Values
	*/
	@Test
	public void test_2338_BuyerValues() throws Exception{
		String homeNetwork = "";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		
		if(!screenPerformEditPackageProfile.existsElement("fieldHomeNetwork")){
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.addItem(ConstantsPackages.ITEM_HOME_NETWORK);
			screenPerformEditPackageProfile.moveToPageBottom();
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		}
		if(screenPerformEditPackageProfile.verifyIfValuesAreEquals("fieldHomeNetwork", ConstantsPackages.VALUE_72490)){
			screenPerformEditPackageProfile.setValue("fieldHomeNetwork", ConstantsPackages.HOME_NETWORK_VALUE_716_17);
		}else{
			screenPerformEditPackageProfile.setValue("fieldHomeNetwork", ConstantsPackages.VALUE_72490);
		}
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnItem("homeNetworkNote");
		screenPerformEditPackageProfile.setItemValueChangeNote("fieldNote", ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnSaveNote();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		Thread.sleep(1000);
		if(screenPerformEditPackageProfile.existsElement("warningModal")){
			screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		}
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.moveToPageHeader();
		screenPerformEditPackageProfile.clickOnSyncButton();
		String valueSync = screenPerformEditPackageProfile.getBuyerValuesFromSyncPage();
		screenPerformEditPackageProfile.clickOnUseBuyerValuesAllButton();
		screenPerformEditPackageProfile.clickOnApplyButton();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		homeNetwork = screenPerformEditPackageProfile.getValueFromFieldText("fieldHomeNetwork");
		
		assertEquals(valueSync, homeNetwork);
	}
	
	/**
	* @testLinkId T-GPRI-3159
	* @summary Package Values
	*/
	@Test
	public void test_2339_PackageValues() throws Exception{
		String homeNetwork = "";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		
		if(!screenPerformEditPackageProfile.existsElement("fieldHomeNetwork")){
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.addItem(ConstantsPackages.ITEM_HOME_NETWORK);
			screenPerformEditPackageProfile.moveToPageBottom();
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		}
		if(screenPerformEditPackageProfile.verifyIfValuesAreEquals("fieldHomeNetwork", ConstantsPackages.VALUE_724_89)){
			screenPerformEditPackageProfile.setValue("fieldHomeNetwork", ConstantsPackages.HOME_NETWORK_VALUE_716_17);
		}else{
			screenPerformEditPackageProfile.setValue("fieldHomeNetwork", ConstantsPackages.VALUE_724_89);
		}
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnItem("homeNetworkNote");
		screenPerformEditPackageProfile.setItemValueChangeNote("fieldNote", ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnSaveNote();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		Thread.sleep(1000);
		if(screenPerformEditPackageProfile.existsElement("warningModal")){
			screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		}
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.moveToPageHeader();
		screenPerformEditPackageProfile.clickOnSyncButton();
		String valueSync = screenPerformEditPackageProfile.getPackageValuesFromSyncPage();
		screenPerformEditPackageProfile.clickOnUsePackageValuesAllButton();
		screenPerformEditPackageProfile.clickOnApplyButton();
		screenPerformEditPackageProfile.clickOkOkPopUpSynchronizePackageWithoutChange();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.waitLoadingOverlay();
		if(screenPerformEditPackageProfile.existsElement("warningModal")){
			screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		}
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		homeNetwork = screenPerformEditPackageProfile.getValueFromFieldText("fieldHomeNetwork");
		
		assertEquals(valueSync, homeNetwork);
	}
	
	/**
	* @testLinkId T-GPRI-3160
	* @summary Nothing changed
	*/
	@Test
	public void test_2340_NothingChanged() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		test_2338_BuyerValues();
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.moveToPageHeader();
		screenPerformEditPackageProfile.clickOnSyncButton();
		String txtMsg = screenPerformEditPackageProfile.getMsgSynchronizePackage();
		assertEquals(ConstantsPackages.NOTHING_CHANGE, txtMsg);
	}
	
	/**
	* @testLinkId T-GPRI-3161
	* @summary Values of buyers and packages
	*/
	@Test
	public void test_2342_ValuesOfBuyersAndPackages() throws Exception{
		String homeNetwork = "";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		
		if(!screenPerformEditPackageProfile.existsElement("fieldHomeNetwork")){
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.addItem(ConstantsPackages.ITEM_HOME_NETWORK);
			screenPerformEditPackageProfile.moveToPageBottom();
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		}
		if(screenPerformEditPackageProfile.verifyIfValuesAreEquals("fieldHomeNetwork", ConstantsPackages.VALUE_724_89)){
			screenPerformEditPackageProfile.setValue("fieldHomeNetwork", ConstantsPackages.HOME_NETWORK_VALUE_716_17);
		}else{
			screenPerformEditPackageProfile.setValue("fieldHomeNetwork", ConstantsPackages.VALUE_724_89);
		}
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnItem("homeNetworkNote");
		screenPerformEditPackageProfile.setItemValueChangeNote("fieldNote", ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnSaveNote();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		Thread.sleep(1000);
		if(screenPerformEditPackageProfile.existsElement("warningModal")){
			screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		}
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.moveToPageHeader();
		screenPerformEditPackageProfile.clickOnSyncButton();
		String valueSync = screenPerformEditPackageProfile.getBuyerValuesFromSyncPage();
		screenPerformEditPackageProfile.clickOnUseBuyerValuesParametersButton();
		screenPerformEditPackageProfile.clickOnUsePackageValuesCustomizationButton();
		screenPerformEditPackageProfile.clickOnApplyButton();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		homeNetwork = screenPerformEditPackageProfile.getValueFromFieldText("fieldHomeNetwork");
		
		assertEquals(valueSync, homeNetwork);
	}
}
