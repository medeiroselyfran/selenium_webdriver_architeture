package com.lge.gpri.tests.packages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.packages.ScreenPerformNormalPackage;
import com.lge.gpri.screens.packages.ScreenPerformSmartCAPackage;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Create Smart
 * CA Package.
 * 
 * @author Fernando Pinheiro
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCreateSmartCAPackage extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2886
	 * @summary Create Smart CA Package With All Fields Requireds
	 */
	@Test
	public void test_1144_CreateSmartCAPackageWithAllFieldsRequireds() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TESTSMARTCA, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.NORMAL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF, ConstantsPackages.SW_VERSION_FROM,
				ConstantsPackages.SW_VERSION_TO);
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String txtPackageCreateSuccess = screenPerformSmartCAPackage.getTextFrom("txtPackageCreatedSuccess");
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
	}

	/**
	 * @testLinkId T-GPRI-2887
	 * @summary Create Smart CA Package With More Than One Buyer
	 */
	@Test
	public void test_1147_CreateSmartCAPackageWithMoreThanOneBuyer() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TESTSMARTCA, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.SMALL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF, ConstantsPackages.SW_VERSION_FROM,
				ConstantsPackages.SW_VERSION_TO);
		screenPerformListPackages.moveToPageHeader();
		screenPerformSmartCAPackage.setBuyerValues(ConstantsPackages.MEO_PORTUGAL);
		String txtNameSecondeBuyer = screenPerformSmartCAPackage.getTextFrom("buyerName");
		assertEquals(ConstantsPackages.MEO_PORTUGAL, txtNameSecondeBuyer.trim());
	}

	/**
	 * @testLinkId T-GPRI-2888
	 * @summary Create Smart CA Package From Another Package
	 */
	@Test
	public void test_1145_CreateSmartCAFromAnotherPackage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.setModel(ConstantsPackages.LGH815);
		screenPerformSmartCAPackage.setValueFromAnotherPackage();
		screenPerformSmartCAPackage.setModelValuesFromAnotherPackage(ConstantsPackages.LGH840);
		screenPerformSmartCAPackage.setBuyerValuesFromAnotherPackage(ConstantsPackages.FIRST);
		screenPerformSmartCAPackage.setPackageValuesFromAnotherPackage(ConstantsPackages.FIRST);
		screenPerformSmartCAPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_TEST);
		screenPerformSmartCAPackage.setTargetDate(ConstantsPackages.TODAY);
		screenPerformSmartCAPackage.setUISimulator(ConstantsPackages.UI_SIMULATOR_DEFAULT_FP_KEY);
		screenPerformSmartCAPackage.setCreationReasons(ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST);
		screenPerformSmartCAPackage.setSmartCAType(ConstantsPackages.SMALL_SMARTCA,
				ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI);
		screenPerformSmartCAPackage.setOpenSW(ConstantsPackages.OPEN_SW_YES);
		screenPerformSmartCAPackage.setNTCodeList(ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF);
		screenPerformListPackages.moveToPageHeader();
		screenPerformSmartCAPackage.setDinamicTagValues();
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String txtPackageCreateSuccess = screenPerformSmartCAPackage.getTextFrom("txtPackageCreatedSuccess");
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
	}

	/**
	 * @testLinkId T-GPRI-2889
	 * @summary Create Smart CA Package With Invalid NT Code
	 */
	@Test
	public void test_1148_CreateSmartCAWithInvalidNTCode() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TESTSMARTCA, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.SMALL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_INVALID_FORMAT,
				ConstantsPackages.SW_VERSION_FROM, ConstantsPackages.SW_VERSION_TO);
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String errorMsg = screenPerformSmartCAPackage.getErrorMsg(" Package Creation Validation");
		screenPerformSmartCAPackage.clickOnClosePopup();
		String txtMessageError = screenPerformSmartCAPackage.getTextFrom("ntCodeError");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, errorMsg);
		assertEquals(ConstantsPackages.NT_CODE_MESSAGE_ERROR, txtMessageError);
	}

	/**
	 * @testLinkId T-GPRI-2890
	 * @summary Create Smart CA Package With NT Code Larger The Maximum Supported
	 */
	@Test
	public void test_1149_CreateSmartCAWithNTCodeLargerTheMaximumSupported() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TESTSMARTCA, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.SMALL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_MAXIMUN_SUPPORTED,
				ConstantsPackages.SW_VERSION_FROM, ConstantsPackages.SW_VERSION_TO);
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String errorMsg = screenPerformSmartCAPackage.getErrorMsg(" Package Creation Validation");
		screenPerformSmartCAPackage.clickOnClosePopup();
		String txtMessageError = screenPerformSmartCAPackage.getTextFrom("ntCodeError");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, errorMsg);
		assertEquals(ConstantsPackages.NT_CODE_MESSAGE_MAXIMUN_SUPPORTED, txtMessageError);
	}

	/**
	 * @testLinkId T-GPRI-2891
	 * @summary Create Smart CA Package With Double Quote
	 */
	@Test
	public void test_1150_CreateSmartCAWithDoubleQuote() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_WITH_DOUBLE_QUOTE, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.SMALL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF, ConstantsPackages.SW_VERSION_FROM,
				ConstantsPackages.SW_VERSION_TO);
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String errorMsg = screenPerformSmartCAPackage.getErrorMsg(" Package Creation Validation");
		screenPerformSmartCAPackage.clickOnClosePopup();
		String txtMessageError = screenPerformSmartCAPackage.getTextFrom("errorNamePackage");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, errorMsg);
		assertEquals(ConstantsPackages.PACKAGE_NAME_ERROR, txtMessageError);
	}

	/**
	 * @testLinkId T-GPRI-2892
	 * @summary Create Smart CA Package With Single Quote
	 */
	@Test
	public void test_1151_CreateSmartCAWithSingleQuote() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_WITH_SINGLE_QUOTE, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.SMALL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF, ConstantsPackages.SW_VERSION_FROM,
				ConstantsPackages.SW_VERSION_TO);
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String errorMsg = screenPerformSmartCAPackage.getErrorMsg(" Package Creation Validation");
		screenPerformSmartCAPackage.clickOnClosePopup();
		String txtMessageError = screenPerformSmartCAPackage.getTextFrom("errorNamePackage");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, errorMsg);
		assertEquals(ConstantsPackages.PACKAGE_NAME_ERROR, txtMessageError);
	}

	/**
	 * @testLinkId T-GPRI-2893
	 * @summary Create Smart CA Package With Back Slash
	 */
	@Test
	public void test_1153_CreateSmartCAWithBackSlash() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_WITH_BACKSLASH, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.SMALL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF, ConstantsPackages.SW_VERSION_FROM,
				ConstantsPackages.SW_VERSION_TO);
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String errorMsg = screenPerformSmartCAPackage.getErrorMsg(" Package Creation Validation");
		screenPerformSmartCAPackage.clickOnClosePopup();
		String txtMessageError = screenPerformSmartCAPackage.getTextFrom("errorNamePackage");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, errorMsg);
		assertEquals(ConstantsPackages.PACKAGE_NAME_ERROR, txtMessageError);
	}

	/**
	 * @testLinkId T-GPRI-2894
	 * @summary Create Smart CA Package From Normal Package
	 */
	@Test
	public void test_1154_CreateSmartCAFromNormalPackage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.setTypeSmartCAPackage();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TESTSMARTCA, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.SMALL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF, ConstantsPackages.SW_VERSION_FROM,
				ConstantsPackages.SW_VERSION_TO);
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		String txtPackageCreateSuccess = screenPerformNormalPackage.getTextFrom("txtPackageCreatedSuccess");
		screenPerformNormalPackage.clickOnOkPackageSuccessCreated();
		String packageStatus = screenPerformListPackages.getPackageStatus();
		assertEquals(ConstantsPackages.UNDER_EDITION, packageStatus);
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
	}

	/**
	 * @testLinkId T-GPRI-2895
	 * @summary Create Smart CA Package For Buyer Mother
	 */
	@Test
	public void test_1155_CreateSmartCAPackageForBuyerMother() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.setModel(ConstantsPackages.LGH815);
		screenPerformSmartCAPackage.setBuyerValues(ConstantsPackages.TLF_GROUP_EUROPE_6TL);
		if (ConstantsPackages.SELECT_BUYER.equals(screenPerformSmartCAPackage.getBuyer())) {
			screenPerformListPackages.rejectChildBuyers();
		}
		String namePackage = screenPerformListPackages.verifyExistingPackageToGetPackageName(ConstantsPackages.LGH815,
				ConstantsPackages.TLF_GROUP_EUROPE_6TL, ConstantsPackages.PACKAGE_NAME_TEST_F_);
		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F_)) {
			int numberPackage = Integer.parseInt(namePackage.split("] ")[1].split(" ")[1]);
			namePackage = ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1);
		} else {
			namePackage = ConstantsPackages.PACKAGE_NAME_TEST_F_1;
		}
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.setModel(ConstantsPackages.LGH815);
		screenPerformSmartCAPackage.setBuyerValues(ConstantsPackages.TLF_GROUP_EUROPE_6TL);
		screenPerformSmartCAPackage.setPackageName(namePackage);
		screenPerformSmartCAPackage.setTargetDate(ConstantsPackages.TODAY);
		screenPerformSmartCAPackage.setUISimulator(ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB);
		screenPerformSmartCAPackage.setCreationReasons(ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST);
		screenPerformSmartCAPackage.setDinamicTagValues();
		screenPerformSmartCAPackage.moveToPageBottom();
		screenPerformSmartCAPackage.setSmartCAType(ConstantsPackages.SMALL_SMARTCA,
				ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI);
		screenPerformSmartCAPackage.setOpenSW(ConstantsPackages.OPEN_SW_YES);
		screenPerformSmartCAPackage.setNTCodeList(ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF);

		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String txtPackageCreateSuccess = screenPerformSmartCAPackage.getTextFrom("txtPackageCreatedSuccess");
		assertEquals(namePackage, screenPerformListPackages.getNamePackageConfirmation());
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
	}

	/**
	 * @testLinkId T-GPRI-3407
	 * @summary Create Smart CA Package With Double Quote
	 */
	@Test
	public void test_3407_CreateSmartCAWithoutSmartCAType() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TEST, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, "",
				ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI, ConstantsPackages.OPEN_SW_YES,
				ConstantsPackages.NT_CODE_LIST_1_FFF, ConstantsPackages.SW_VERSION_FROM,
				ConstantsPackages.SW_VERSION_TO);
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String errorMsg = screenPerformSmartCAPackage.getErrorMsg(" Package Creation Validation");
		screenPerformSmartCAPackage.clickOnClosePopup();
		assertTrue(screenPerformSmartCAPackage.verifyErrorOnSmartCATypeField());
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, errorMsg);
	}

	/**
	 * @testLinkId T-GPRI-3410
	 * @summary Create Smart CA Package With Double Quote
	 */
	@Test
	public void test_3410_CreateSmartCAWithoutSmartCATypeAndAnotherItem() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TEST, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, "",
				ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI, ConstantsPackages.OPEN_SW_YES,
				ConstantsPackages.NT_CODE_LIST_1_FFF, ConstantsPackages.SW_VERSION_FROM,
				ConstantsPackages.SW_VERSION_TO);
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String errorMsg = screenPerformSmartCAPackage.getErrorMsg(" Package Creation Validation");
		screenPerformSmartCAPackage.clickOnClosePopup();
		assertTrue(screenPerformSmartCAPackage.verifyErrorOnOsVersionField());
		assertTrue(screenPerformSmartCAPackage.verifyErrorOnSmartCATypeField());
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, errorMsg);
	}

	/**
	 * @testLinkId T-GPRI-3411
	 * @summary Create Smart CA Package With All Fields Requireds
	 */
	@Test
	public void test_3411_CreateSmartCAPackageWithNormalSmartCAType() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TESTSMARTCA, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.NORMAL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF, ConstantsPackages.SW_VERSION_FROM,
				ConstantsPackages.SW_VERSION_TO);
		String model = screenPerformSmartCAPackage.getModel();
		String buyer = screenPerformSmartCAPackage.getBuyer();
		String packageName = screenPerformSmartCAPackage.getPackageName();
		String uiSimulator = screenPerformSmartCAPackage.getUISimulator();
		String androidVersion = screenPerformSmartCAPackage.getAndroidVersion();
		String gridColumn = screenPerformSmartCAPackage.getGridRowAndColumn();
		String osVersion = screenPerformSmartCAPackage.getOSVersion();
		String smartCAType = screenPerformSmartCAPackage.getSmartCAType();
		String providingType = screenPerformSmartCAPackage.getProvidingType();

		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String txtPackageCreateSuccess = screenPerformSmartCAPackage.getTextFrom("txtPackageCreatedSuccess");
		screenPerformSmartCAPackage.clickOnOkPackageSuccessCreated();
		screenPerformListPackages.clickOnEditPackageButton();
		assertTrue(screenPerformSmartCAPackage.verifyPackageCreatedWithSuccess(model, buyer, packageName, uiSimulator,
				androidVersion, gridColumn, osVersion, smartCAType, providingType));
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
	}

	/**
	 * @testLinkId T-GPRI-3414
	 * @summary Create Smart CA Package With All Fields Requireds
	 */
	@Test
	public void test_3414_CreateSmartCAPackageWithSmallSmartCAType() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TESTSMARTCA, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.SMALL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF, ConstantsPackages.SW_VERSION_FROM,
				ConstantsPackages.SW_VERSION_TO);
		String model = screenPerformSmartCAPackage.getModel();
		String buyer = screenPerformSmartCAPackage.getBuyer();
		String packageName = screenPerformSmartCAPackage.getPackageName();
		String uiSimulator = screenPerformSmartCAPackage.getUISimulator();
		String androidVersion = screenPerformSmartCAPackage.getAndroidVersion();
		String gridColumn = screenPerformSmartCAPackage.getGridRowAndColumn();
		String osVersion = screenPerformSmartCAPackage.getOSVersion();
		String smartCAType = screenPerformSmartCAPackage.getSmartCAType();
		String providingType = screenPerformSmartCAPackage.getProvidingType();

		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String txtPackageCreateSuccess = screenPerformSmartCAPackage.getTextFrom("txtPackageCreatedSuccess");
		screenPerformSmartCAPackage.clickOnOkPackageSuccessCreated();
		screenPerformListPackages.clickOnEditPackageButton();
		assertTrue(screenPerformSmartCAPackage.verifyPackageCreatedWithSuccess(model, buyer, packageName, uiSimulator,
				androidVersion, gridColumn, osVersion, smartCAType, providingType));
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
	}

	/**
	 * @testLinkId T-GPRI-3415
	 * @summary Create Smart CA Package With All Fields Requireds
	 */
	@Test
	public void test_3415_CreateSmartCAPackageWithNoSmartCAType() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TESTSMARTCA, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.NO_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF, ConstantsPackages.SW_VERSION_FROM,
				ConstantsPackages.SW_VERSION_TO);
		String model = screenPerformSmartCAPackage.getModel();
		String buyer = screenPerformSmartCAPackage.getBuyer();
		String packageName = screenPerformSmartCAPackage.getPackageName();
		String uiSimulator = screenPerformSmartCAPackage.getUISimulator();
		String androidVersion = screenPerformSmartCAPackage.getAndroidVersion();
		String gridColumn = screenPerformSmartCAPackage.getGridRowAndColumn();
		String osVersion = screenPerformSmartCAPackage.getOSVersion();
		String smartCAType = screenPerformSmartCAPackage.getSmartCAType();
		String providingType = screenPerformSmartCAPackage.getProvidingType();

		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		String txtPackageCreateSuccess = screenPerformSmartCAPackage.getTextFrom("txtPackageCreatedSuccess");
		screenPerformSmartCAPackage.clickOnOkPackageSuccessCreated();
		screenPerformListPackages.clickOnEditPackageButton();
		assertTrue(screenPerformSmartCAPackage.verifyPackageCreatedWithSuccess(model, buyer, packageName, uiSimulator,
				androidVersion, gridColumn, osVersion, smartCAType, providingType));
		assertEquals(ConstantsPackages.PACKAGE_CREATED_SUCCESS, txtPackageCreateSuccess);
	}

}
