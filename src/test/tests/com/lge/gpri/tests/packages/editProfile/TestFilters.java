package com.lge.gpri.tests.packages.editProfile;

import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_AUTO_COMMIT;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_DEFAULT_HIDDEN_ON_MOTHER;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_HAS_LINKS;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_MANDATORY;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_MANDATORY_FOR_BUYER_CHILD;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_MULTIPLE;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_TAG_TYPES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_TAG_VALUES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_TYPE;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_WARNING;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_AUTO_COMMIT;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_AUTO_COMMIT_2;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_DEFAULT_HIDDEN_ON_MOTHER;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_HAS_LINKS;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_MANDATORY;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_MANDATORY_FOR_BUYER_CHILD;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_MULTIPLE;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_NAME;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_ANDROID_VERSION;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_COTA;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_CUSTOMIZER;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_NO;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_RESOURCE;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_YES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_SMART_CA_TYPES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_TAG_TYPES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_TAG_VALUES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_TYPE;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_WARNINGS;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.MESSAGE_MATCHED_AUTO_COMMIT;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.MESSAGE_NO_RESULT_MATCHED_AUTO_COMMIT_2;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.TXT_NO_RESULT_MATCHED;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.TXT_RESULT_MATCHED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsItemInspector;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Filters on
 * Edit Profile Package.
 * 
 * @author Gabriel Costa
 * @author Cayk Lima Barreto
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestFilters extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-3115
	 * @summary Auto Commit
	 */
	@Test
	public void test_2210_AutoCommit() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_AUTO_COMMIT);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_AUTO_COMMIT);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_YES);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, FILTER_AUTO_COMMIT);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		if (resultOk) {
			screenPerformListPackages.openPage();
			screenPerformListPackages.setModel(ConstantsPackages.LGH815);
			screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
			screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.optionFilters();
			screenPerformEditPackageProfile.searchItemLibrary(FILTER_AUTO_COMMIT);
			screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_AUTO_COMMIT);
			screenPerformEditPackageProfile.elementFocus();
			screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
			screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_NO);
			screenPerformEditPackageProfile.waitLoading();
			pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
			for (String path : pathItems) {
				screenPerformEditPackageProfile.newTab("");
				screenPerformEditPackageProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, FILTER_AUTO_COMMIT);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditPackageProfile.switchTabs(0);
				if (!resultOk) {
					break;
				}
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
		for (Boolean result : noProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3116
	 * @summary Default Hide on Mother
	 */
	@Test
	public void test_2211_DefaultHideOnMother() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER,
				FILTER_OPTION_YES);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.DEFAULT_HIDE_ON_MOTHER);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		if (resultOk) {
			screenPerformListPackages.openPage();
			screenPerformListPackages.setModel(ConstantsPackages.LGH815);
			screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
			screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.optionFilters();
			screenPerformEditPackageProfile.searchItemLibrary(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
			screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
			screenPerformEditPackageProfile.elementFocus();
			screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER);
			screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER,
					FILTER_OPTION_NO);
			screenPerformEditPackageProfile.waitLoading();
			pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
			for (String path : pathItems) {
				screenPerformEditPackageProfile.newTab("");
				screenPerformEditPackageProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.DEFAULT_HIDE_ON_MOTHER);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditPackageProfile.switchTabs(0);
				if (!resultOk) {
					break;
				}
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
		for (Boolean result : noProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3117
	 * @summary Mandatory
	 */
	@Test
	public void test_2212_Mandatory() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_MANDATORY);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_MANDATORY);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY, FILTER_OPTION_YES);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		if (resultOk) {
			screenPerformListPackages.openPage();
			screenPerformListPackages.setModel(ConstantsPackages.LGH815);
			screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
			screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.optionFilters();
			screenPerformEditPackageProfile.searchItemLibrary(FILTER_MANDATORY);
			screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_MANDATORY);
			screenPerformEditPackageProfile.elementFocus();
			screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY);
			screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY, FILTER_OPTION_NO);
			screenPerformEditPackageProfile.waitLoading();
			pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
			for (String path : pathItems) {
				screenPerformEditPackageProfile.newTab("");
				screenPerformEditPackageProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditPackageProfile.switchTabs(0);
				if (!resultOk) {
					break;
				}
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
		for (Boolean result : noProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3118
	 * @summary Mandatory for Buyer Child
	 */
	@Test
	public void test_2213_MandatoryForBuyerChild() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY_FOR_BUYER_CHILD,
				FILTER_OPTION_YES);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path,
					ConstantsItemInspector.FILTER_MANDATORY_FOR_BUYER_CHILD);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		if (resultOk) {
			screenPerformListPackages.openPage();
			screenPerformListPackages.setModel(ConstantsPackages.LGH815);
			screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
			screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.optionFilters();
			screenPerformEditPackageProfile.searchItemLibrary(FILTER_MANDATORY_FOR_BUYER_CHILD);
			screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_MANDATORY_FOR_BUYER_CHILD);
			screenPerformEditPackageProfile.elementFocus();
			screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD);
			screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY_FOR_BUYER_CHILD,
					FILTER_OPTION_NO);
			screenPerformEditPackageProfile.waitLoading();
			pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
			for (String path : pathItems) {
				screenPerformEditPackageProfile.newTab("");
				screenPerformEditPackageProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path,
						ConstantsItemInspector.FILTER_MANDATORY_FOR_BUYER_CHILD);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditPackageProfile.switchTabs(0);
				if (!resultOk) {
					break;
				}
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
		for (Boolean result : noProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3119
	 * @summary Multiple
	 */
	@Test
	public void test_2214_Multiple() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.reopenPackage(nameStatus);
		resultOk = screenPerformListPackages.clickOnEditProfilePackageButton();
		if (!resultOk) {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.rejectGeneratingAPK(ConstantsPackages.LGH815);
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		resultOk = true;
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_MULTIPLE);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_MULTIPLE);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_MULTIPLE);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_MULTIPLE, FILTER_OPTION_YES);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MULTIPLE);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		if (resultOk) {
			screenPerformListPackages.openPage();
			screenPerformListPackages.setModel(ConstantsPackages.LGH815);
			screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
			screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.optionFilters();
			screenPerformEditPackageProfile.searchItemLibrary(FILTER_MULTIPLE);
			screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_MULTIPLE);
			screenPerformEditPackageProfile.elementFocus();
			screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_MULTIPLE);
			screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_MULTIPLE, FILTER_OPTION_NO);
			screenPerformEditPackageProfile.waitLoading();
			pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
			for (String path : pathItems) {
				screenPerformEditPackageProfile.newTab("");
				screenPerformEditPackageProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MULTIPLE);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditPackageProfile.switchTabs(0);
				if (!resultOk) {
					break;
				}
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
		for (Boolean result : noProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3120
	 * @summary Name
	 */
	@Test
	public void test_2215_Name() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_NAME);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_NAME);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.setTextOnFilterName(ConstantsItemInspector.HOME);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_NAME);
			resultOk = value.contains(ConstantsItemInspector.HOME);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3121
	 * @summary SmartCA Types
	 */
	@Test
	public void test_2216_SmartCATypes() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_SMART_CA_TYPES);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_SMART_CA_TYPES);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSmartCaDropDown();
		screenPerformEditPackageProfile.clickOnSelectedOptionForSmartCaTypeFilter(FILTER_OPTION_CUSTOMIZER);
		screenPerformEditPackageProfile.clickOnSmartCaDropDown();
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_SMARTCA_TYPES);
			resultOk = value.equals(ConstantsItemInspector.CUSTOMIZER);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3122
	 * @summary Tag Types
	 */
	@Test
	public void test_2217_TagTypes() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_TAG_TYPES);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_TAG_TYPES);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_TAG_TYPES);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_TAG_TYPES, FILTER_OPTION_ANDROID_VERSION);
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_TAG_TYPES);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_TAG_TYPES);
			resultOk = value.contains(ConstantsItemInspector.ANDROID_VERSION);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3123
	 * @summary Tag Values
	 */
	@Test
	public void test_2218_TagValues() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_TAG_VALUES);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_TAG_VALUES);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_TAG_VALUES);
		screenPerformEditPackageProfile.selectValueFilterOptionWithArea(CRITERIA_TAG_VALUES,
				ConstantsItemInspector.ANDROID_VERSION, ConstantsItemInspector.ANDROID_VERSION);
		ArrayList<String> allVersions = screenPerformEditPackageProfile.getAllValuesOfTagValues();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_TAG_VALUES);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_TAG_VALUES);
			String[] versions = value.split(",");
			boolean hasSomeOne = false;
			for (int i = 0; i < versions.length && resultOk; i++) {
				if (versions[i].contains(ConstantsItemInspector.ANDROID_VERSION)) {
					hasSomeOne = true;
					resultOk = allVersions
							.contains(versions[i].replace(ConstantsItemInspector.ANDROID_VERSION + " ", ""));
				}
			}
			if (!hasSomeOne) {
				resultOk = false;
			}
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3124
	 * @summary Type
	 */
	@Test
	public void test_2219_Type() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.reopenPackage(nameStatus);
		resultOk = screenPerformListPackages.clickOnEditProfilePackageButton();
		if (!resultOk) {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.rejectGeneratingAPK(ConstantsPackages.LGH815);
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		resultOk = true;
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_TYPE);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_TYPE);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_TYPE);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_TYPE, FILTER_OPTION_RESOURCE);
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_TYPE);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_TYPE);
			resultOk = value.contains(FILTER_OPTION_RESOURCE);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3125
	 * @summary Warnings
	 */
	@Test
	public void test_2220_Warnings() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_WARNINGS);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_WARNINGS);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_WARNING);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_WARNING, FILTER_OPTION_COTA);
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_WARNING);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, FILTER_WARNINGS);
			resultOk = value.contains(FILTER_OPTION_COTA);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3126
	 * @summary Has Links
	 */
	@Test
	public void test_2221_HasLinks() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_HAS_LINKS);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_HAS_LINKS);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_HAS_LINKS);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_HAS_LINKS, FILTER_OPTION_YES);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, FILTER_HAS_LINKS);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		if (resultOk) {
			screenPerformListPackages.openPage();
			screenPerformListPackages.setModel(ConstantsPackages.LGH815);
			screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
			screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.optionFilters();
			screenPerformEditPackageProfile.searchItemLibrary(FILTER_HAS_LINKS);
			screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_HAS_LINKS);
			screenPerformEditPackageProfile.elementFocus();
			screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_HAS_LINKS);
			screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_HAS_LINKS, FILTER_OPTION_NO);
			screenPerformEditPackageProfile.waitLoading();
			pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
			for (String path : pathItems) {
				screenPerformEditPackageProfile.newTab("");
				screenPerformEditPackageProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, FILTER_HAS_LINKS);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditPackageProfile.switchTabs(0);
				if (!resultOk) {
					break;
				}
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
		for (Boolean result : noProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3127
	 * @summary Auto Commit and Default Hide on Mother
	 */
	@Test
	public void test_2222_AutoCommitAndDefaultHideOnMother() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_AUTO_COMMIT);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_AUTO_COMMIT);
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_YES);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER,
				FILTER_OPTION_YES);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String valueAutoCommit = screenItemLibrary.getValueProperty(path,
					ConstantsItemInspector.FILTER_AUTO_COMMIT);
			String valueHideOnMother = screenItemLibrary.getValueProperty(path,
					ConstantsItemInspector.DEFAULT_HIDE_ON_MOTHER);
			resultOk = valueAutoCommit.equals("true");
			resultOk = resultOk && valueHideOnMother.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3128
	 * @summary Mandatory and Mandatory for Buyer Child
	 */
	@Test
	public void test_2223_MandatoryAndMandatoryForBuyerChild() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk = true;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_MANDATORY);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_MANDATORY);
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY, FILTER_OPTION_YES);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY_FOR_BUYER_CHILD,
				FILTER_OPTION_NO);
		screenPerformEditPackageProfile.waitLoading();
		pathItems = screenPerformEditPackageProfile.getFirstItemsOfEachRootDirectory();
		for (String path : pathItems) {
			screenPerformEditPackageProfile.newTab("");
			screenPerformEditPackageProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String valueMandatory = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY);
			String valueMandatoryBuyer = screenItemLibrary.getValueProperty(path,
					ConstantsItemInspector.FILTER_MANDATORY_FOR_BUYER_CHILD);
			resultOk = valueMandatory.equals("true");
			resultOk = resultOk && valueMandatoryBuyer.equals("false");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditPackageProfile.switchTabs(0);
			if (!resultOk) {
				break;
			}
		}

		for (Boolean result : yesProperty) {
			assertTrue(result);
		}
	}

	/**
	 * @testLinkId T-GPRI-3129
	 * @summary All Filters
	 */
	@Test
	public void test_2224_AllFilters() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		boolean resultSelectAllFilters = false;
		boolean resultDeselectAllFilters = false;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		resultSelectAllFilters = screenPerformEditPackageProfile.selectAllFilters();
		resultSelectAllFilters = resultSelectAllFilters
				&& screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_AUTO_COMMIT);
		resultSelectAllFilters = resultSelectAllFilters
				&& screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER);
		resultSelectAllFilters = resultSelectAllFilters
				&& screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_MANDATORY);
		resultSelectAllFilters = resultSelectAllFilters
				&& screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD);
		resultSelectAllFilters = resultSelectAllFilters
				&& screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_MULTIPLE);
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditPackageProfile.isVisibleFilterName();
		resultSelectAllFilters = resultSelectAllFilters
				&& screenPerformEditPackageProfile.isVisibleFilterSmartCaOption();
		resultSelectAllFilters = resultSelectAllFilters
				&& screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_TAG_TYPES);
		resultSelectAllFilters = resultSelectAllFilters
				&& screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_TAG_VALUES);
		resultSelectAllFilters = resultSelectAllFilters
				&& screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_TYPE);
		resultSelectAllFilters = resultSelectAllFilters
				&& screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_WARNING);
		resultSelectAllFilters = resultSelectAllFilters
				&& screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_HAS_LINKS);
		resultDeselectAllFilters = screenPerformEditPackageProfile.deselectAllFilters();
		resultDeselectAllFilters = resultDeselectAllFilters
				&& !screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_AUTO_COMMIT);
		resultDeselectAllFilters = resultDeselectAllFilters
				&& !screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER);
		resultDeselectAllFilters = resultDeselectAllFilters
				&& !screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_MANDATORY);
		resultDeselectAllFilters = resultDeselectAllFilters
				&& !screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD);
		resultDeselectAllFilters = resultDeselectAllFilters
				&& !screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_MULTIPLE);
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditPackageProfile.isVisibleFilterName();
		resultDeselectAllFilters = resultDeselectAllFilters
				&& !screenPerformEditPackageProfile.isVisibleFilterSmartCaOption();
		resultDeselectAllFilters = resultDeselectAllFilters
				&& !screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_TAG_TYPES);
		resultDeselectAllFilters = resultDeselectAllFilters
				&& !screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_TAG_VALUES);
		resultDeselectAllFilters = resultDeselectAllFilters
				&& !screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_TYPE);
		resultDeselectAllFilters = resultDeselectAllFilters
				&& !screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_WARNING);
		resultDeselectAllFilters = resultDeselectAllFilters
				&& !screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_HAS_LINKS);
		screenPerformEditPackageProfile.waitLoading();

		assertTrue(resultSelectAllFilters);
		assertTrue(resultDeselectAllFilters);
	}

	/**
	 * @testLinkId T-GPRI-3130
	 * @summary Remove filter
	 */
	@Test
	public void test_2225_RemoveFilter() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		boolean resultAutoCommitAdd = false;
		boolean resultRemoveFilter = false;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_AUTO_COMMIT);
		screenPerformEditPackageProfile.clickOnSelectedFilter(FILTER_AUTO_COMMIT);
		screenPerformEditPackageProfile.elementFocus();
		screenPerformEditPackageProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
		screenPerformEditPackageProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_YES);
		resultAutoCommitAdd = screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_AUTO_COMMIT);
		screenPerformEditPackageProfile.waitLoading();
		screenPerformEditPackageProfile.removeFilter(CRITERIA_AUTO_COMMIT);
		resultRemoveFilter = !screenPerformEditPackageProfile.isVisibleFilterOptions(CRITERIA_AUTO_COMMIT);
		screenPerformEditPackageProfile.waitLoading();

		assertTrue(resultAutoCommitAdd);
		assertTrue(resultRemoveFilter);
	}

	/**
	 * @testLinkId T-GPRI-3131
	 * @summary Search on filter
	 */
	@Test
	public void test_2226_SearchOnFilter() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		String resultAuCommit = "";
		String resultAuCommit2 = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.optionFilters();
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_AUTO_COMMIT);
		resultAuCommit = screenPerformEditPackageProfile.getText(TXT_RESULT_MATCHED);
		screenPerformEditPackageProfile.searchItemLibrary(FILTER_AUTO_COMMIT_2);
		resultAuCommit2 = screenPerformEditPackageProfile.getText(TXT_NO_RESULT_MATCHED);

		assertEquals(MESSAGE_MATCHED_AUTO_COMMIT, resultAuCommit);
		assertEquals(MESSAGE_NO_RESULT_MATCHED_AUTO_COMMIT_2, resultAuCommit2);
	}
}
