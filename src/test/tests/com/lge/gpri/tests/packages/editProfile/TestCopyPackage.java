package com.lge.gpri.tests.packages.editProfile;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsCompare;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* <pre>
* Author: Lucas Sales
* Purpose: <Type here the purpose of the class> - Lucas Sales - 16 de nov de 2017
* History: Class creation - Lucas Sales - 16 de nov de 2017
*
*</pre>
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCopyPackage extends AbstractTestScenary{
	//Blocked
	/**
	* @testLinkId T-GPRI-3165
	* @summary Same Model Buyer Values
	*/
	@Test
	public void test_2347_CopyPackageWithSameModelBuyerValues() throws Exception{
		String homeNetwork = "";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.preConditionsToTestCopyPackage();
		
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.moveToPageHeader();
		screenPerformEditPackageProfile.clickOnCopyPackageButton();
		screenPerformEditPackageProfile.clickOnCopyPackageOptionModelButton();
		screenPerformEditPackageProfile.setCopyPackageModel(ConstantsPackages.LGH815);
		screenPerformEditPackageProfile.clickOnCopyPackageOptionPackageButton();
		screenPerformEditPackageProfile.clickOnFirstElementOfListPackage();
		screenPerformEditPackageProfile.clickOnLoadPackageButton();
		screenPerformEditPackageProfile.clickOnUseBuyerValuesAllButton();
		screenPerformEditPackageProfile.clickOnApplyButton();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		homeNetwork = screenPerformEditPackageProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
		String gsm = screenPerformEditPackageProfile.getValueGivenNameItem(ConstantsPackages.GSM);
		String utms = screenPerformEditPackageProfile.getValueGivenNameItem(ConstantsPackages.UMTS);
		String lte = screenPerformEditPackageProfile.getValueGivenNameItem(ConstantsPackages.LTE);
		
		assertEquals("", homeNetwork);
		assertEquals(ConstantsPackages.GSM_2100, gsm);
		assertEquals(ConstantsPackages.UMTS_2100, utms);
		assertEquals(ConstantsPackages.LTE_7, lte);
	}
	
	/**
	* @testLinkId T-GPRI-3166
	* @summary Same Model Package Values
	* Should be Use Source Values
	*/
	@Test
	public void test_2348_CopyPackageWithSameModelPackageValues() throws Exception{
		String homeNetwork = "";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.preConditionsToTestCopyPackage();
		
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.moveToPageHeader();
		screenPerformEditPackageProfile.clickOnCopyPackageButton();
		screenPerformEditPackageProfile.clickOnCopyPackageOptionModelButton();
		screenPerformEditPackageProfile.setCopyPackageModel(ConstantsPackages.LGH815);
		screenPerformEditPackageProfile.clickOnCopyPackageOptionPackageButton();
		screenPerformEditPackageProfile.clickOnFirstElementOfListPackage();
		screenPerformEditPackageProfile.clickOnLoadPackageButton();
		screenPerformEditPackageProfile.clickOnUsePackageValuesAllButton();
		screenPerformEditPackageProfile.clickOnApplyButton();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		homeNetwork = screenPerformEditPackageProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
		String gsm = screenPerformEditPackageProfile.getValueGivenNameItem(ConstantsPackages.GSM);
		String utms = screenPerformEditPackageProfile.getValueGivenNameItem(ConstantsPackages.UMTS);
		String lte = screenPerformEditPackageProfile.getValueGivenNameItem(ConstantsPackages.LTE);
		
		assertEquals(ConstantsPackages.HOME_NETWORK_724_31, homeNetwork);
		assertEquals("", gsm);
		assertEquals("", utms);
		assertEquals("", lte);
	}
	
	/**
	* @testLinkId T-GPRI-3167
	* @summary Same Model Nothing changed
	*/
	@Test
	public void test_2349_CopyPackageWithSameModelNothingChanged() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		test_2348_CopyPackageWithSameModelPackageValues();
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.preConditionsToTestCopyPackage();
		
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.moveToPageHeader();
		screenPerformEditPackageProfile.clickOnCopyPackageButton();
		screenPerformEditPackageProfile.clickOnCopyPackageOptionModelButton();
		screenPerformEditPackageProfile.setCopyPackageModel(ConstantsPackages.LGH815);
		screenPerformEditPackageProfile.clickOnCopyPackageOptionPackageButton();
		screenPerformEditPackageProfile.clickOnFirstElementOfListPackage();
		screenPerformEditPackageProfile.clickOnLoadPackageButton();
		String txtNothingToChange = screenPerformEditPackageProfile.getMsgNothingToChange();
		
		assertEquals(ConstantsPackages.NOTHING_CHANGE, txtNothingToChange);
	}
}