package com.lge.gpri.tests.packages.editProfile;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.models.ScreenPerformListModel;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Adding a note
 * to Edit Package Profile.
 * 
 * @author Gabriel Costa
 * @author Cayk Lima Barreto
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAddingNote extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-3139
	 * @summary Change the value of an item and add a note
	 */
	@Test
	public void test_2313_ChangeTheValueOfAnItemAndAddANote() throws Exception {
		String buyer = "", model = "", note = "";
		getDriver().get(ConstantsPackages.PAGE_MODELS_URL);
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.setNameFilter(ConstantsPackages.LGH815);
		screenPerformListModel.clickOnViewPackages(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String modelName = screenPerformListPackages.getModelName();
		String buyerName = screenPerformListPackages.getBuyerName();
		String suffixBuyer = buyerName.split("\\(")[1].split("\\)")[0];
		String namePackage = screenPerformListPackages.getNamePackageConfirmation();
		if (!screenPerformListPackages.clickOnEditProfilePackageButton()) {
			screenPerformListPackages.reopenPackage("");
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
			}
		}
		if (!screenPerformEditPackageProfile.existsElement("fieldHomeNetwork")) {
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.addItem(ConstantsPackages.ITEM_HOME_NETWORK);
			screenPerformEditPackageProfile.moveToPageBottom();
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		}
	
		if (screenPerformEditPackageProfile.verifyIfValuesAreEquals("fieldHomeNetwork",
				ConstantsPackages.HOME_NETWORK_724_31) || screenPerformEditPackageProfile.verifyIfValuesIsEmpty("fieldHomeNetwork", "")) {
			screenPerformEditPackageProfile.setValue("fieldHomeNetwork", ConstantsPackages.HOME_NETWORK_724_32);
		} else {
			String attributeTextFrom = screenPerformEditPackageProfile.getAttributeTextFrom("fieldHomeNetwork");
			Integer homeNetwork = Integer.valueOf(attributeTextFrom.replaceAll(" ", ""));
			Integer integer = homeNetwork + 1;
			String newHomeNetwork = String.valueOf(integer);
			screenPerformEditPackageProfile.setValue("fieldHomeNetwork",
					newHomeNetwork.substring(0, 3) + " " + newHomeNetwork.substring(3, 5));
		}
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnItem("homeNetworkNote");
		screenPerformEditPackageProfile.setItemValueChangeNote("fieldNote", ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnSaveNote();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		Thread.sleep(1000);
		if (screenPerformEditPackageProfile.existsElement("warningModal")) {
			screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		}
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(namePackage + " " + modelName + " " + suffixBuyer);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformView.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnItemValue("pkg17HomeNetwork");
		screenPerformViewValues.clickOnItemHistory("homeNetworkHistory");
		buyer = screenPerformViewValues.getTextFrom("itemHistoryBuyer");
		model = screenPerformViewValues.getTextFrom("itemHistoryModel");
		note = screenPerformViewValues.getTextFrom("lastItemHistoryNote");

		assertEquals(ConstantsPackages.LGH815, model);
		assertEquals(ConstantsPackages.OI_BRAZIL_BOI, buyer);
		assertEquals(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING, note);
	}

	/**
	 * @testLinkId T-GPRI-3140
	 * @summary Change the value of an item and do not add a note
	 */
	@Test
	public void test_2314_ChangeTheValueOfAnItemDoNotAndAddANote() throws Exception {
		String buyer = "", model = "", note = "";
		getDriver().get(ConstantsPackages.PAGE_MODELS_URL);
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());

		screenPerformListModel.openPage();
		screenPerformListModel.setNameFilter(ConstantsPackages.LGH815);
		screenPerformListModel.clickOnViewPackages(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String modelName = screenPerformListPackages.getModelName();
		String buyerName = screenPerformListPackages.getBuyerName();
		String suffixBuyer = buyerName.split("\\(")[1].split("\\)")[0];
		String namePackage = screenPerformListPackages.getNamePackageConfirmation();
		boolean resultOk = screenPerformListPackages.clickOnEditProfilePackageButton();
		if (!resultOk) {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.rejectGeneratingAPK(ConstantsPackages.LGH815);
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		if (!screenPerformEditPackageProfile.existsElement("fieldHomeNetwork")) {
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
			screenPerformEditPackageProfile.addItem(ConstantsPackages.ITEM_HOME_NETWORK);
			screenPerformEditPackageProfile.moveToPageBottom();
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		}
		if (screenPerformEditPackageProfile.verifyIfValuesAreEquals("fieldHomeNetwork",
				ConstantsPackages.VALUE_72490)) {
			screenPerformEditPackageProfile.setValue("fieldHomeNetwork", ConstantsPackages.HOME_NETWORK_VALUE_716_17);
		} else {
			screenPerformEditPackageProfile.setValue("fieldHomeNetwork", ConstantsPackages.VALUE_72490);
		}
		screenPerformEditPackageProfile.clickOnSaveProfile();
		Thread.sleep(1000);
		if (screenPerformEditPackageProfile.existsElement("warningModal")) {
			screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		}
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(namePackage + " " + modelName + " " + suffixBuyer);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformView.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnItemValue("pkg17HomeNetwork");
		screenPerformViewValues.clickOnItemHistory("homeNetworkHistory");
		buyer = screenPerformViewValues.getTextFrom("itemHistoryBuyer");
		model = screenPerformViewValues.getTextFrom("itemHistoryModel");
		note = screenPerformViewValues.getTextFrom("lastItemHistoryNote");

		assertEquals(ConstantsPackages.LGH815, model);
		assertEquals(ConstantsPackages.OI_BRAZIL_BOI, buyer);
		assertEquals("", note);
	}
}
