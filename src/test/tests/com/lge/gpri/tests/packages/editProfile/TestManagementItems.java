package com.lge.gpri.tests.packages.editProfile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * <pre>
* Author: Lucas Sales
* Purpose: <Type here the purpose of the class> - Lucas Sales - 13 de nov de 2017
* History: Class creation - great - 13 de nov de 2017
 *
 * </pre>
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestManagementItems extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-3107
	 * @summary Add one item
	 */
	@Test
	public void test_2202_AddOneItem() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		if (screenPerformEditPackageProfile.unselectItemInSelectItemsTree("ESM info flag")) {
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
			screenPerformEditPackageProfile.clickOnSaveProfile();
			screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
			screenPerformEditPackageProfile.clickOnSaveEditedProfile();
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		}
		screenPerformEditPackageProfile.selectItemInSelectItemsTree("ESM info flag");
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		assertTrue(screenPerformEditPackageProfile.unselectItemInSelectItemsTree("ESM info flag"));
	}

	/**
	 * @testLinkId T-GPRI-3108
	 * @summary Remove one item
	 */
	@Test
	public void test_2203_RemoveOneItem() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		if (screenPerformEditPackageProfile.selectItemInSelectItemsTree("ESM info flag")) {
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
			screenPerformEditPackageProfile.clickOnSaveProfile();
			screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
			screenPerformEditPackageProfile.clickOnSaveEditedProfile();
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		}
		screenPerformEditPackageProfile.unselectItemInSelectItemsTree("ESM info flag");
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		assertTrue(screenPerformEditPackageProfile.selectItemInSelectItemsTree("ESM info flag"));
	}

	/**
	 * @testLinkId T-GPRI-3109
	 * @summary Add more than one item
	 */
	//TODO: Verify status before click on edit profile (error if apk error generator)
	@Test
	public void test_2204_AddMoreThanOneItem() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.addItemByPath("");
		screenPerformEditPackageProfile.removeItemByPath("");
		screenPerformEditPackageProfile.addItemByPath(ConstantsPackages.PATH_PLMN_DISPLAY_NAME);
		screenPerformEditPackageProfile.addItemByPath(ConstantsPackages.PATH_PLMN_MCC);
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.setValue("fieldDisplayName", ConstantsPackages.VALUE_2100);
		screenPerformEditPackageProfile.setValue("fieldPLMNMcc", ConstantsPackages.MCC_724);
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		String txtDisplayName = screenPerformEditPackageProfile.getValue("fieldDisplayName");
		String txtMCC = screenPerformEditPackageProfile.getValue("fieldPLMNMcc");

		assertEquals(ConstantsPackages.VALUE_2100, txtDisplayName);
		assertEquals(ConstantsPackages.MCC_724, txtMCC);
	}

	/**
	 * @testLinkId T-GPRI-3110
	 * @summary Remove more than one item
	 */
	@Test
	public void test_2205_RemoveMoreThanOneItem() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		if (screenPerformEditPackageProfile.selectItemInSelectItemsTree("ESM info flag")
				&& screenPerformEditPackageProfile.selectItemInSelectItemsTree("Always Available")) {
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
			screenPerformEditPackageProfile.setValueGivenNameItem("Always Available #1",
					ConstantsBuyers.HOME_NETWORK_724_32);
			screenPerformEditPackageProfile.clickOnSaveProfile();
			screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
			screenPerformEditPackageProfile.clickOnSaveEditedProfile();
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		}
		screenPerformEditPackageProfile.unselectItemInSelectItemsTree("ESM info flag");
		screenPerformEditPackageProfile.unselectItemInSelectItemsTree("Always Available");
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		assertTrue(screenPerformEditPackageProfile.selectItemInSelectItemsTree("ESM info flag"));
		assertTrue(screenPerformEditPackageProfile.selectItemInSelectItemsTree("Always Available"));
	}

	/**
	 * @testLinkId T-GPRI-3111
	 * @summary Remove more than one item
	 */
	@Test
	public void test_2206_AddAllItems() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		if (screenPerformListPackages.verifyIsStatusUnderEdition()) {
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		} else {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		}
		assertTrue(screenPerformEditPackageProfile.selectItemInSelectItemsTree("Item Library"));
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());

	}

	/**
	 * @testLinkId T-GPRI-3112
	 * @summary Uncheck all items
	 */
	@Test
	public void test_2207_UncheckAllItems() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		if (screenPerformListPackages.verifyIsStatusUnderEdition()) {
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		} else {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		}
		screenPerformEditPackageProfile.doubleClickOnElementInTree("Item Library");
		assertTrue(screenPerformEditPackageProfile.verifyAllItemsUnchecked());
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
	}

	/**
	 * @testLinkId T-GPRI-3113
	 * @summary Add Folder
	 */
	@Test
	public void test_2208_AddFolder() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		if (screenPerformListPackages.verifyIsStatusUnderEdition()) {
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		} else {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		}
		if(screenPerformEditPackageProfile.selectItemInSelectItemsTree("Carrier Aggregation")){
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
			screenPerformEditPackageProfile.setValueGivenNameItem("Inter band", "B7+B20");
			screenPerformEditPackageProfile.setValueGivenNameItem("Intra band", "B3+B7");
			screenPerformEditPackageProfile.clickOnSaveProfile();
			screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
			screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		}
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		assertTrue(screenPerformEditPackageProfile.verifyExistsElementInParameters("Inter band"));
		assertTrue(screenPerformEditPackageProfile.verifyExistsElementInParameters("Intra band"));
	}

	/**
	 * @testLinkId T-GPRI-3114
	 * @summary Remove Folder
	 */
	@Test
	public void test_2209_RemoveFolder() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		if (screenPerformListPackages.verifyIsStatusUnderEdition()) {
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		} else {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		}
		screenPerformEditPackageProfile.waitLoadingOverlay();
		if(screenPerformEditPackageProfile.isItemCheckedByPath(ConstantsPackages.PATH_CARRIER_AGGREGATION)){
			screenPerformEditPackageProfile.unselectItemInSelectItemsTree("Carrier Aggregation");
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		}
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		assertFalse(screenPerformEditPackageProfile.verifyExistsElementInParameters("Carrier Aggregation"));
	}

}