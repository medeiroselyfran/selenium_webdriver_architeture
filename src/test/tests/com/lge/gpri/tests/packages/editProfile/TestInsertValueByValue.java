package com.lge.gpri.tests.packages.editProfile;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.view.ScreenPerformViewValidator;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* <pre>
* Author: great
* Purpose: <Type here the purpose of the class> - great - 20 de nov de 2017
* History: Class creation - great - 20 de nov de 2017
*
*</pre>
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestInsertValueByValue extends AbstractTestScenary{

	
	/**
	* @testLinkId T-GPRI-3174
	* @summary Insert a invalid value after removing a invalid value
	*/
	@Test
	public void test_2359_InsertInvalidValueAfterRemovingInvalidValue() throws Exception{
		String txtWarning = "";
		String option12h = "";
		String option24hour ="";
		String option24hours="";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformViewValidator screenPerformViewValidator = new ScreenPerformViewValidator(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.openValidatorsPageOnAnotherTab();
		screenPerformViewValidator.searchValidator(ConstantsPackages.TIME_FORMAT_VALIDATOR);
		screenPerformViewValidator.clickOnTimeFormatValidatorButton();
		screenPerformViewValidator.setNewValue(ConstantsPackages.TIME_FORMAT_24_HOUR);
		screenPerformViewValidator.clickOnAddNewValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformViewValidator.switchTabs(0);
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.searchItemSelectItems("Time Format");
		screenPerformEditPackageProfile.clickOnTimeFormatButton();
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.switchTabs(1);
		screenPerformViewValidator.clickOnDeleteTimeFormatButton();
		screenPerformViewValidator.clickOnConfirmDeletedValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformEditPackageProfile.closeTab();
		screenPerformViewValidator.switchTabs(0);
		screenPerformEditPackageProfile.clickOnTimeFormatTypeButton();
		screenPerformEditPackageProfile.clickOnTimeFormatPerValueOption();
		screenPerformEditPackageProfile.clickOnTimeFormatValueOptions();
		screenPerformEditPackageProfile.select24HourOption();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.clickOnCancelSaveButton();
		txtWarning = screenPerformEditPackageProfile.getText("spanTimeFormat");
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		option12h = screenPerformEditPackageProfile.getText("option12h");
		option24hour = screenPerformEditPackageProfile.getText("option24hour");
		option24hours = screenPerformEditPackageProfile.getText("option24hours");

		assertEquals(ConstantsPackages.VALUE_NO_LONGER_ACCEPTABLE, txtWarning);
		assertEquals(ConstantsPackages.TIME_FORMAT_12_HOURS, option12h);
		assertEquals(ConstantsPackages.TIME_FORMAT_24_HOUR, option24hour);
		assertEquals(ConstantsPackages.TIME_FORMAT_24_HOURS, option24hours);
	}
	
	/**
	* @testLinkId T-GPRI-3175
	* @summary Insert a valid value after removing a invalid value
	*/
	@Test
	public void test_2360_InsertValidValueAfterRemovingInvalidValue() throws Exception{
		String option12h = "";
		String option24hours="";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformViewValidator screenPerformViewValidator = new ScreenPerformViewValidator(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.openValidatorsPageOnAnotherTab();
		screenPerformViewValidator.searchValidator(ConstantsPackages.TIME_FORMAT_VALIDATOR);
		screenPerformViewValidator.clickOnTimeFormatValidatorButton();
		screenPerformViewValidator.setNewValue(ConstantsPackages.TIME_FORMAT_24_HOURS);
		screenPerformViewValidator.clickOnAddNewValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformViewValidator.setNewValue(ConstantsPackages.TIME_FORMAT_24_HOUR);
		screenPerformViewValidator.clickOnAddNewValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformViewValidator.clickOnDeleteTimeFormatButton();
		screenPerformViewValidator.clickOnConfirmDeletedValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformEditPackageProfile.closeTab();
		screenPerformViewValidator.switchTabs(0);
				
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.searchItemSelectItems("Time Format");
		screenPerformEditPackageProfile.clickOnTimeFormatButton();
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();

		screenPerformEditPackageProfile.clickOnTimeFormatTypeButton();
		screenPerformEditPackageProfile.clickOnTimeFormatPerValueOption();
		screenPerformEditPackageProfile.clickOnTimeFormatValueOptions();
		screenPerformEditPackageProfile.select12HoursOption();
		screenPerformEditPackageProfile.clickOnSaveProfile();

		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		option12h = screenPerformEditPackageProfile.getText("option12h");
		option24hours = screenPerformEditPackageProfile.getText("option24hours");

		assertEquals(ConstantsPackages.TIME_FORMAT_12_HOURS, option12h);
		assertEquals(ConstantsPackages.TIME_FORMAT_24_HOURS, option24hours);
	}
	
	/**
	* @testLinkId T-GPRI-3176
	* @summary Insert a empty value after removing a invalid value
	*/
	@Test
	public void test_2362_InsertAEmptyValueAfterRemovingAInvalidValue() throws Exception{
		String option12h = "";
		String option24hours="";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		ScreenPerformViewValidator screenPerformViewValidator = new ScreenPerformViewValidator(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformEditPackageProfile.openValidatorsPageOnAnotherTab();
		screenPerformViewValidator.searchValidator(ConstantsPackages.TIME_FORMAT_VALIDATOR);
		screenPerformViewValidator.clickOnTimeFormatValidatorButton();
		screenPerformViewValidator.setNewValue(ConstantsPackages.TIME_FORMAT_24_HOUR);
		screenPerformViewValidator.clickOnAddNewValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformViewValidator.switchTabs(0);
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.clickOnItemLibraryButton();
		screenPerformEditPackageProfile.searchItemSelectItems("Time Format");
		screenPerformEditPackageProfile.clickOnTimeFormatButton();
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		
		screenPerformEditPackageProfile.switchTabs(1);
		screenPerformViewValidator.clickOnDeleteTimeFormatButton();
		screenPerformViewValidator.clickOnConfirmDeletedValueButton();
		screenPerformViewValidator.clickOnSaveValidatorButton();
		screenPerformViewValidator.clickOnValidatorSuccessfullySavedButton();
		screenPerformEditPackageProfile.closeTab();
		screenPerformViewValidator.switchTabs(0);

		screenPerformEditPackageProfile.clickOnTimeFormatTypeButton();
		screenPerformEditPackageProfile.clickOnTimeFormatPerValueOption();
		screenPerformEditPackageProfile.clickOnTimeFormatValueOptions();
		screenPerformEditPackageProfile.selectEmptyOption();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.clickOnWarningOkButton();
		screenPerformEditPackageProfile.select12HoursOption();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.clickOnContinueAnywayButton();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		option12h = screenPerformEditPackageProfile.getText("option12h");
		option24hours = screenPerformEditPackageProfile.getText("option24hours");

		assertEquals(ConstantsPackages.TIME_FORMAT_12_HOURS, option12h);
		assertEquals(ConstantsPackages.TIME_FORMAT_24_HOURS, option24hours);
	}
	
}