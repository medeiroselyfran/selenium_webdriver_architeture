package com.lge.gpri.tests.packages;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.buyers.ScreenPerformBuyer;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.models.ScreenPerformListModel;
import com.lge.gpri.screens.models.ScreenPerformModel;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.packages.ScreenPerformNormalPackage;
import com.lge.gpri.screens.packages.ScreenPerformSmartCAPackage;
import com.lge.gpri.screens.usermanagement.ScreenPerformRegions;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery SCENARY NAME.
*  
* @author AUTHOR NAME (can exist more than one) 
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestListPackages extends AbstractTestScenary{

	/**
	* @testLinkId T-GPRI-2911
	* @summary Load only latest package
	*/
	@Test
	public void test_1186_LoadingOnlyLatestPackage() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		boolean resultLast=true, resultAll=true;
		int numberLast, numberAll;
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		numberAll = screenPerformListPackages.getNumberOfItemsListPackage();
		if(numberAll<1){
			resultAll=false;
		}
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		numberLast = screenPerformListPackages.getNumberOfItemsListPackage();
		if(numberLast!=1){
			resultLast=false;
		}
		
		assertTrue(resultAll);
		assertTrue(resultLast);
	}
	
	/**
	* @testLinkId T-GPRI-2912
	* @summary Search
	*/
	@Test
	public void test_1188_Search() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage(ConstantsPackages.PKG2_M_MS1);
		String packageName = screenPerformListPackages.getNamePackageConfirmation();
		
		assertEquals(ConstantsPackages.PKG2_M_MS1, packageName);
		
		screenPerformListPackages.searchPackage(ConstantsPackages.PKG2_M_MS1_ABC);
		String txt = screenPerformListPackages.getTextFrom("noPackages");
		
		assertEquals(ConstantsPackages.NO_PACKAGE_AVAILABLE, txt);
	}
	
	/**
	* @testLinkId T-GPRI-2913
	* @summary Pagination 
	*/
	@Test
	public void test_1189_Pagination() throws Exception{
		boolean resultNext = false;
		boolean resultPrevious = false;
		boolean resultFistPage = false;
		boolean resultLastPage = false;
		boolean resultChoosedPage = false;
		boolean result10 = false;
		boolean result25 = false;
		boolean result50 = false;
		boolean result100 = false;
		boolean result200 = false;
		boolean result500 = false;
		boolean resultAll = false;
		int actualPage, previousPage, numberOfPages;
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OPEN_UNIFIED_LATIN_AMERICA_SCA);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.showTableElements("show10");
		int numberLines10 = screenPerformListPackages.getNumberOfItemsListPackage();
		int showingInfo10 = screenPerformListPackages.getEndEntrieShowingInfo();
		if(numberLines10<=10 && showingInfo10==10){
			result10=true;
		}
		screenPerformListPackages.showTableElements("show25");
		int numberLines25 = screenPerformListPackages.getNumberOfItemsListPackage();
		int showingInfo25 = screenPerformListPackages.getEndEntrieShowingInfo();
		if(numberLines25<=25 && showingInfo25==25){
			result25=true;
		}
		screenPerformListPackages.showTableElements("show100");
		int numberLines100 = screenPerformListPackages.getNumberOfItemsListPackage();
		int showingInfo100 = screenPerformListPackages.getEndEntrieShowingInfo();
		if(numberLines100<=100 && showingInfo100==100){
			result100=true;
		}
		screenPerformListPackages.showTableElements("show200");
		int numberLines200 = screenPerformListPackages.getNumberOfItemsListPackage();
		int showingInfo200 = screenPerformListPackages.getEndEntrieShowingInfo();
		if(numberLines200<=200 && showingInfo200==200){
			result200=true;
		}
		screenPerformListPackages.showTableElements("show500");
		int numberLines500 = screenPerformListPackages.getNumberOfItemsListPackage();
		int showingInfo500 = screenPerformListPackages.getEndEntrieShowingInfo();
		if(numberLines500<=500 && showingInfo500==500){
			result500=true;
		}
		screenPerformListPackages.showTableElements("showAll");
		int numberLinesAll = screenPerformListPackages.getNumberOfItemsListPackage();
		int showingInfoAll = screenPerformListPackages.getEndEntrieShowingInfo();
		if(numberLinesAll==screenPerformListPackages.getTotalEntriesShowingInfo() && showingInfoAll==screenPerformListPackages.getTotalEntriesShowingInfo()){
			resultAll=true;
		}
		screenPerformListPackages.showTableElements("show50");
		int numberLines50 = screenPerformListPackages.getNumberOfItemsListPackage();
		int showingInfo50 = screenPerformListPackages.getEndEntrieShowingInfo();
		if(numberLines50<=50 && showingInfo50==50){
			result50=true;
		}
		numberOfPages = screenPerformListPackages.getNumberOfPages();
		screenPerformListPackages.moveToPageBottom();
		previousPage = screenPerformListPackages.getActivePage();
		screenPerformListPackages.paginationNext();
		actualPage = screenPerformListPackages.getActivePage();
		if(numberOfPages==previousPage || previousPage<actualPage){
			resultNext = true;
		}
		previousPage = screenPerformListPackages.getActivePage();
		screenPerformListPackages.paginationPrevious();
		actualPage = screenPerformListPackages.getActivePage();
		if(1==previousPage || previousPage>actualPage){
			resultPrevious = true;
		}
		screenPerformListPackages.goToPage("lastPage");
		actualPage = screenPerformListPackages.getActivePage();
		if(actualPage==numberOfPages){
			resultLastPage = true;
		}
		screenPerformListPackages.goToPage("firstPage");
		actualPage = screenPerformListPackages.getActivePage();
		if(actualPage==1){
			resultFistPage = true;
		}
		screenPerformListPackages.goToPage("page2");
		actualPage = screenPerformListPackages.getActivePage();
		if(actualPage==2 || numberOfPages==1){
			resultChoosedPage = true;
		}
		assertTrue(result10);
		assertTrue(result25);
		assertTrue(result50);
		assertTrue(result100);
		assertTrue(result200);
		assertTrue(result500);
		assertTrue(resultAll);
		assertTrue(resultNext);
		assertTrue(resultPrevious);
		assertTrue(resultFistPage);
		assertTrue(resultLastPage);
		assertTrue(resultChoosedPage);
	}
	
	/**
	* @testLinkId T-GPRI-2914
	* @summary View Package with buyer mother
	*/
	@Test
	public void test_1190_ViewPackageWithBuyerMother () throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		HashMap<String,Boolean> listConfirmationBuyerChildren = null;
		boolean confirmationMotherBuyer=true;
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_UNIFIED_LATIN_AMERICA_AMX);
		ArrayList<String> status = screenPerformListPackages.getAllStatus();
		ArrayList<Boolean> validationStatus = new ArrayList<>();
		for(int i=0;i<status.size();i++){
			validationStatus.add(status.get(i).contains(ConstantsPackages.CONFIRMED)||status.get(i).contains("Error"));
		}
		ArrayList<String> names = screenPerformListPackages.getAllBuyersNames();
		screenPerformListPackages.clickOnViewPackage(ConstantsPackages.CLARO_UNIFIED_LATIN_AMERICA_AMX);
		screenPerformViewValues.openViewBuyerChildren();
		confirmationMotherBuyer = screenPerformViewValues.getConfirmationMotherBuyer();
		listConfirmationBuyerChildren = screenPerformViewValues.getConfirmationListBuyerChildren();
		
		assertEquals(validationStatus.get(0), confirmationMotherBuyer);
		for(int i=1;i<validationStatus.size();i++){
			Boolean inListPackage = validationStatus.get(i);
			Boolean inViewPackage = listConfirmationBuyerChildren.get(names.get(i));
			assertEquals(inListPackage, inViewPackage);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2915
	* @summary View Package with buyer single 
	*/
	@Test
	public void test_1191_ViewPackageWithBuyerSingle () throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		HashMap<String,Boolean> listConfirmationBuyerChildren = null;
		boolean confirmationMotherBuyer=true;
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		ArrayList<String> status = screenPerformListPackages.getAllStatus();
		ArrayList<Boolean> validationStatus = new ArrayList<>();
		for(int i=0;i<status.size();i++){
			validationStatus.add(status.get(i).contains(ConstantsPackages.CONFIRMED)||status.get(i).contains("Error"));
		}
		screenPerformListPackages.clickOnViewPackage(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformViewValues.openViewBuyerChildren();
		confirmationMotherBuyer = screenPerformViewValues.getConfirmationMotherBuyer();
		listConfirmationBuyerChildren = screenPerformViewValues.getConfirmationListBuyerChildren();
		
		assertEquals(validationStatus.get(0), confirmationMotherBuyer);
		assertEquals(0, listConfirmationBuyerChildren.size());
	}
	
	/**
	* @testLinkId T-GPRI-2916
	* @summary More Info with buyer mother
	*/
	@Test
	public void test_1192_MoreInfoWithBuyerMother () throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		ScreenPerformRegions screenPerformRegions = new ScreenPerformRegions(getDriver());
		
		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsPackages.LGH815);
		screenPerformListModel.clickOnEditModel(ConstantsPackages.LGH815);
		String targetDevice = screenPerformModel.getTargetDevice();
		String product = screenPerformModel.getProduct();
		ArrayList<String> swpls = screenPerformModel.getSWPLs();
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsPackages.SUFFIX_AMX);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsPackages.SUFFIX_AMX);
		String region = screenPerformBuyer.getRegion();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		
		screenPerformRegions.openPage();
		screenPerformRegions.clickOnRegion(region);
		screenPerformRegions.clickOnTabRequirementManager();
		ArrayList<String> rms = screenPerformRegions.getItemsOfRequirementManager();
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_UNIFIED_LATIN_AMERICA_AMX);
		boolean resultEdit = screenPerformListPackages.clickOnEditPackageButton();
		if(!resultEdit){
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnOKButtonAfterReopenPackage();
			screenPerformListPackages.setNewTargetDate(ConstantsPackages.TODAY);
			screenPerformListPackages.clickOnReopenAfterTargetDate();
			screenPerformListPackages.clickOnEditPackageButton();
		}
		String type = screenPerformNormalPackage.getType();
		String country = screenPerformNormalPackage.getCountryShortcut();
		String operator = screenPerformNormalPackage.getOperatorShortcut();
		String androidVersion = screenPerformNormalPackage.getAndroidVersion();
		String carrierAggregation = screenPerformNormalPackage.getCarrierAggregation();
		String gridRowColumn = screenPerformNormalPackage.getGridRowAndColumn();
		String osVersion = screenPerformNormalPackage.getOSVersion();
		String uiVersion = screenPerformNormalPackage.getUIVersion();
		String voiceOver = screenPerformNormalPackage.getVoiceOver();
		String reasonForCreation = screenPerformNormalPackage.getCreationReasons();
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.CLARO_UNIFIED_LATIN_AMERICA_AMX);
		screenPerformListPackages.clickOnMoreInfo(ConstantsPackages.CLARO_UNIFIED_LATIN_AMERICA_AMX);
		String moreInfo= screenPerformListPackages.getStringMoreInfo();
		String typeInfo = moreInfo.split("Type")[1].split("Target Device")[0].replaceAll("\n   ", "");
		String targetDeviceInfo = moreInfo.split("Target Device")[1].split("Product")[0].replaceAll("\n   ", "");
		String productInfo = moreInfo.split("Product")[1].split("CUPSS Group")[0].replaceAll("\n   ", "");
		String countryInfo = moreInfo.split("Country Shortcut")[1].split("Operator Shortcut")[0].replaceAll("\n   ", "");
		String operatorInfo = moreInfo.split("Operator Shortcut")[1].split("SWPLs")[0].replaceAll("\n   ", "");
		String[] swplsInfo = moreInfo.split("SWPLs")[1].split("LTMs")[0].replaceFirst("\n   ", "").split("\n   ");
		String[] ltmsInfo = moreInfo.split("LTMs")[1].split("RMs")[0].replaceFirst("\n   ", "").split("\n   ");
		String[] rmsInfo = moreInfo.split("RMs")[1].split("Tags")[0].replaceFirst("\n   ", "").split("\n   ");
		String androidVersionInfo = moreInfo.split("Tags")[1].split("Android Version: ")[1].split("Carrier Aggregation")[0].replaceAll("\n   ", "");
		String carrierAggregationInfo = moreInfo.split("Tags")[1].split("Carrier Aggregation: ")[1].split("Grid Row x Column")[0].replaceAll("\n   ", "");
		String gridRowColumnInfo = moreInfo.split("Tags")[1].split("Grid Row x Column: ")[1].split("OS Version")[0].replaceAll("\n   ", "");
		String osVersionInfo = moreInfo.split("Tags")[1].split("OS Version: ")[1].split("UI Version")[0].replaceAll("\n   ", "");
		String uiVersionInfo = moreInfo.split("Tags")[1].split("UI Version: ")[1].split("Voice over")[0].replaceAll("\n   ", "");
		String voiceOverInfo = moreInfo.split("Tags")[1].split("Voice over: ")[1].split("Reasons for package creation")[0].replaceAll("\n   ", "");
		String reasonForCreationInfo = moreInfo.split("Tags")[1].split("Reasons for package creation")[1].replaceAll("\n", "");
		
		assertEquals(type,typeInfo);
		assertEquals(targetDevice,targetDeviceInfo);
		assertEquals(product,productInfo);
		assertEquals(country,countryInfo);
		assertEquals(operator,operatorInfo);
		boolean resultOk = false;
		for(int i=0;i<swplsInfo.length;i++){
			for(int j=0;j<swpls.size();j++){
				if(swplsInfo[i].contains(swpls.get(j).split(" \\(")[0])){
					resultOk=true;break;
				}
			}
		}
		assertTrue(resultOk);
		resultOk = false;
		for(int i=0;i<ltmsInfo.length;i++){
			for(int j=0;j<ltms.size();j++){
				if(ltmsInfo[i].contains(ltms.get(j).split(" \\(")[0])){
					resultOk=true;break;
				}
			}
		}
		assertTrue(resultOk);
		resultOk = false;
		for(int i=0;i<rmsInfo.length;i++){
			for(int j=0;j<rms.size();j++){
				if(rmsInfo[i].contains(rms.get(j).split(" \\(")[0])){
					resultOk=true;break;
				}
			}
		}
		assertTrue(resultOk);
		assertEquals(androidVersion,androidVersionInfo);
		assertEquals(carrierAggregation,carrierAggregationInfo);
		assertEquals(gridRowColumn,gridRowColumnInfo);
		assertEquals(osVersion,osVersionInfo);
		assertEquals(uiVersion,uiVersionInfo);
		assertEquals(voiceOver,voiceOverInfo);
		assertEquals(reasonForCreation,reasonForCreationInfo);
	}

	/**
	* @testLinkId T-GPRI-2917
	* @summary More Info with buyer single 
	*/
	@Test
	public void test_1193_MoreInfoWithBuyerSingle () throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
		ScreenPerformModel screenPerformModel = new ScreenPerformModel(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		ScreenPerformRegions screenPerformRegions = new ScreenPerformRegions(getDriver());
		
		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsPackages.LGH815);
		screenPerformListModel.clickOnEditModel(ConstantsPackages.LGH815);
		String targetDevice = screenPerformModel.getTargetDevice();
		String product = screenPerformModel.getProduct();
		ArrayList<String> swpls = screenPerformModel.getSWPLs();
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsPackages.SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsPackages.SUFFIX_BOI);
		String region = screenPerformBuyer.getRegion();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		
		screenPerformRegions.openPage();
		screenPerformRegions.clickOnRegion(region);
		screenPerformRegions.clickOnTabRequirementManager();
		ArrayList<String> rms = screenPerformRegions.getItemsOfRequirementManager();
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		boolean resultEdit = screenPerformListPackages.clickOnEditPackageButton();
		if(!resultEdit){
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnOKButtonAfterReopenPackage();
			screenPerformListPackages.setNewTargetDate(ConstantsPackages.TODAY);
			screenPerformListPackages.clickOnReopenAfterTargetDate();
			screenPerformListPackages.clickOnEditPackageButton();
		}
		String type = screenPerformNormalPackage.getType();
		if(type.equals(ConstantsPackages.NORMAL)){
			String country = screenPerformNormalPackage.getCountryShortcut();
			String operator = screenPerformNormalPackage.getOperatorShortcut();
			String androidVersion = screenPerformNormalPackage.getAndroidVersion();
			String carrierAggregation = screenPerformNormalPackage.getCarrierAggregation();
			String gridRowColumn = screenPerformNormalPackage.getGridRowAndColumn();
			String osVersion = screenPerformNormalPackage.getOSVersion();
			String uiVersion = screenPerformNormalPackage.getUIVersion();
			String voiceOver = screenPerformNormalPackage.getVoiceOver();
			String reasonForCreation = screenPerformNormalPackage.getCreationReasons();
			screenPerformListPackages.openPage();
			screenPerformListPackages.setModel(ConstantsPackages.LGH815);
			screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.clickOnMoreInfo(ConstantsPackages.OI_BRAZIL_BOI);
			String moreInfo= screenPerformListPackages.getStringMoreInfo();
			String typeInfo = moreInfo.split("Type")[1].split("Target Device")[0].replaceAll("\n   ", "");
			String targetDeviceInfo = moreInfo.split("Target Device")[1].split("Product")[0].replaceAll("\n   ", "");
			String productInfo = moreInfo.split("Product")[1].split("CUPSS Group")[0].replaceAll("\n   ", "");
			String countryInfo = moreInfo.split("Country Shortcut")[1].split("Operator Shortcut")[0].replaceAll("\n   ", "");
			String operatorInfo = moreInfo.split("Operator Shortcut")[1].split("SWPLs")[0].replaceAll("\n   ", "");
			String[] swplsInfo = moreInfo.split("SWPLs")[1].split("LTMs")[0].replaceFirst("\n   ", "").split("\n   ");
			String[] ltmsInfo = moreInfo.split("LTMs")[1].split("RMs")[0].replaceFirst("\n   ", "").split("\n   ");
			String[] rmsInfo = moreInfo.split("RMs")[1].split("Tags")[0].replaceFirst("\n   ", "").split("\n   ");
			String androidVersionInfo = moreInfo.split("Tags")[1].split("Android Version: ")[1].split("Carrier Aggregation")[0].replaceAll("\n   ", "");
			String carrierAggregationInfo = moreInfo.split("Tags")[1].split("Carrier Aggregation: ")[1].split("Grid Row x Column")[0].replaceAll("\n   ", "");
			String gridRowColumnInfo = moreInfo.split("Tags")[1].split("Grid Row x Column: ")[1].split("OS Version")[0].replaceAll("\n   ", "");
			String osVersionInfo = moreInfo.split("Tags")[1].split("OS Version: ")[1].split("UI Version")[0].replaceAll("\n   ", "");
			String uiVersionInfo = moreInfo.split("Tags")[1].split("UI Version: ")[1].split("Voice over")[0].replaceAll("\n   ", "");
			String voiceOverInfo = moreInfo.split("Tags")[1].split("Voice over: ")[1].split("Reasons for package creation")[0].replaceAll("\n   ", "");
			String reasonForCreationInfo = moreInfo.split("Tags")[1].split("Reasons for package creation")[1].replaceAll("\n", "");
			
			assertEquals(type,typeInfo);
			assertEquals(targetDevice,targetDeviceInfo);
			assertEquals(product,productInfo);
			assertEquals(country,countryInfo);
			assertEquals(operator,operatorInfo);
			boolean resultOk = false;
			for(int i=0;i<swplsInfo.length;i++){
				for(int j=0;j<swpls.size();j++){
					if(swplsInfo[i].contains(swpls.get(j).split(" \\(")[0])){
						resultOk=true;break;
					}
				}
			}
			assertTrue(resultOk);
			resultOk = false;
			for(int i=0;i<ltmsInfo.length;i++){
				for(int j=0;j<ltms.size();j++){
					if(ltmsInfo[i].contains(ltms.get(j).split(" \\(")[0])){
						resultOk=true;break;
					}
				}
			}
			assertTrue(resultOk);
			resultOk = false;
			for(int i=0;i<rmsInfo.length;i++){
				for(int j=0;j<rms.size();j++){
					if(rmsInfo[i].contains(rms.get(j).split(" \\(")[0])){
						resultOk=true;break;
					}
				}
			}
			assertTrue(resultOk);
			assertEquals(androidVersion,androidVersionInfo);
			assertEquals(carrierAggregation,carrierAggregationInfo);
			assertEquals(gridRowColumn,gridRowColumnInfo);
			assertEquals(osVersion,osVersionInfo);
			assertEquals(uiVersion,uiVersionInfo);
			assertEquals(voiceOver,voiceOverInfo);
			assertEquals(reasonForCreation,reasonForCreationInfo);
		}else{
			String country = screenPerformSmartCAPackage.getCountryShortcut();
			String operator = screenPerformSmartCAPackage.getOperatorShortcut();
			String androidVersion = screenPerformSmartCAPackage.getAndroidVersion();
			String gridRowColumn = screenPerformSmartCAPackage.getGridRowAndColumn();
			String osVersion = screenPerformSmartCAPackage.getOSVersion();
			String reasonForCreation = screenPerformSmartCAPackage.getCreationReasons();
			screenPerformListPackages.openPage();
			screenPerformListPackages.setModel(ConstantsPackages.LGH815);
			screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.clickOnMoreInfo(ConstantsPackages.OI_BRAZIL_BOI);
			String moreInfo= screenPerformListPackages.getStringMoreInfo();
			String typeInfo = moreInfo.split("Type")[1].split("Representative Model Name")[0].replaceAll("\n   ", "");
			String targetDeviceInfo = moreInfo.split("Target Device")[1].split("\nProduct")[0].replaceAll("\n   ", "");
			String productInfo = moreInfo.split("Product")[1].split("\nCUPSS Group")[0].replaceAll("\n   ", "");
			String countryInfo = moreInfo.split("Country Shortcut")[1].split("\nOperator Shortcut")[0].replaceAll("\n   ", "");
			String operatorInfo = moreInfo.split("Operator Shortcut")[1].split("\nSWPLs")[0].replaceAll("\n   ", "");
			String[] swplsInfo = moreInfo.split("SWPLs")[1].split("\nLTMs")[0].replaceFirst("\n   ", "").split("\n   ");
			String[] ltmsInfo = moreInfo.split("LTMs")[1].split("\nGTMs")[0].replaceFirst("\n   ", "").split("\n   ");
			String[] rmsInfo = moreInfo.split("RMs")[1].split("\nTags")[0].replaceFirst("\n   ", "").split("\n   ");
			String androidVersionInfo = moreInfo.split("Tags")[1].split("Android Version: ")[1].split("Grid Row x Column")[0].replaceAll("\n   ", "");
			String gridRowColumnInfo = moreInfo.split("Tags")[1].split("Grid Row x Column: ")[1].split("OS Version")[0].replaceAll("\n   ", "");
			String osVersionInfo = moreInfo.split("Tags")[1].split("OS Version: ")[1].split("\nSmart CA")[0].replaceAll("\n   ", "");
			String reasonForCreationInfo = moreInfo.split("Tags")[1].split("Reasons for package creation")[1].replaceAll("\n", "");
			
			assertEquals(type.replace(" ", ""),typeInfo.replace(" ", ""));
			assertEquals(targetDevice.replace("\n", ""),targetDeviceInfo.replace("\n ", ""));
			assertEquals(product.replace("\n", ""),productInfo.replace("\n", ""));
			assertEquals(country.replace("\n", ""),countryInfo.replace("\n", ""));
			assertEquals(operator.replace("\n", ""),operatorInfo.replace("\n", ""));
			boolean resultOk = false;
			for(int i=0;i<swplsInfo.length;i++){
				for(int j=0;j<swpls.size();j++){
					if(swplsInfo[i].contains(swpls.get(j).split(" \\(")[0])){
						resultOk=true;break;
					}
				}
			}
			assertTrue(resultOk);
			resultOk = false;
			for(int i=0;i<ltmsInfo.length;i++){
				for(int j=0;j<ltms.size();j++){
					if(ltmsInfo[i].contains(ltms.get(j).split(" \\(")[0])){
						resultOk=true;break;
					}
				}
			}
			assertTrue(resultOk);
			resultOk = false;
			for(int i=0;i<rmsInfo.length;i++){
				for(int j=0;j<rms.size();j++){
					if(rmsInfo[i].contains(rms.get(j).split(" \\(")[0])){
						resultOk=true;break;
					}
				}
			}
			assertTrue(resultOk);
			assertEquals(androidVersion,androidVersionInfo);
			assertEquals(gridRowColumn,gridRowColumnInfo);
			assertEquals(osVersion,osVersionInfo);
			assertEquals(reasonForCreation,reasonForCreationInfo);
		}
	}
	
	/**
	* @testLinkId T-GPRI-2918
	* @summary Package flag
	*/
	@Test
	public void test_1194_PackageFlag () throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		boolean flagOn = screenPerformListPackages.clickOnMarkAsFinal(ConstantsPackages.OI_BRAZIL_BOI);
		boolean flagOff = screenPerformListPackages.clickOnMarkAsFinal(ConstantsPackages.OI_BRAZIL_BOI);
		
		assertTrue(flagOn);
		assertFalse(flagOff);
	}
}
