package com.lge.gpri.tests.packages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.packages.ScreenPerformSmartCAPackage;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery SCENARY NAME.
 * 
 * @author AUTHOR NAME (can exist more than one)
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestEditSmartCAPackage extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2896
	 */
	@Test
	public void test_1157_EditSmartCAPackageChangingOnlyName() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";
		String editedpackageName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage,
					ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.searchPackage("Smart");
			constructorNamePackage = screenPerformListPackages.constructorNamePackage();
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setPackageName(constructorNamePackage);
			editedpackageName = screenPerformSmartCAPackage.getPackageName();
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			packageName = screenPerformListPackages.getNamePackageConfirmation();
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setPackageName(constructorNamePackage);
			editedpackageName = screenPerformSmartCAPackage.getPackageName();
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			packageName = screenPerformListPackages.getNamePackageConfirmation();
		}
		assertEquals(editedpackageName, packageName);
	}

	/**
	 * @testLinkId T-GPRI-2897
	 */
	@Test
	public void test_1171_ChangeOnlyUiSimulator() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setUISimulator(ConstantsPackages.UI_SIMULATOR_DEFAULT_FP_KEY);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			screenPerformListPackages.clickOnEditPackageButton();
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setUISimulator(ConstantsPackages.UI_SIMULATOR_DEFAULT_FP_KEY);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			screenPerformListPackages.clickOnEditPackageButton();
		}

		assertEquals(ConstantsPackages.UI_SIMULATOR_DEFAULT_FP_KEY, screenPerformSmartCAPackage.getUISimulator());
	}

	/**
	 * @testLinkId T-GPRI-2898
	 */
	@Test
	public void test_1172_ChangeAndroidVersionForBuyerSingle() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setAndroidVersion(ConstantsPackages.ANDROID_VERSION_6);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			screenPerformListPackages.clickOnEditPackageButton();
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setAndroidVersion(ConstantsPackages.ANDROID_VERSION_6);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			screenPerformListPackages.clickOnEditPackageButton();
		}
		assertEquals(ConstantsPackages.ANDROID_VERSION_6, screenPerformSmartCAPackage.getAndroidVersion());
	}

	/**
	 * @testLinkId T-GPRI-2899
	 */
	@Test
	public void test_1184_ChangeAndroidVersionForBuyerMother() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TLF_GROUP_EUROPE_6TL);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.TLF_GROUP_EUROPE_6TL);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setAndroidVersion(ConstantsPackages.ANDROID_VERSION_6);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			screenPerformListPackages.clickOnEditPackageButton();
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setAndroidVersion(ConstantsPackages.ANDROID_VERSION_6);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			screenPerformListPackages.clickOnEditPackageButton();
		}

		assertEquals(ConstantsPackages.ANDROID_VERSION_6, screenPerformSmartCAPackage.getAndroidVersion());
	}

	/**
	 * @testLinkId T-GPRI-2900
	 */
	@Test
	public void test_1173_LeaveNameEmpty() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";
		String txtPackageEditionValidationName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setPackageName("");
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			txtPackageEditionValidationName = screenPerformSmartCAPackage
					.getTextFrom("txtPackageEditionValidationField");
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setPackageName("");
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			txtPackageEditionValidationName = screenPerformSmartCAPackage
					.getTextFrom("txtPackageEditionValidationField");
		}
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationName);
	}

	/**
	 * @testLinkId T-GPRI-2901
	 */
	@Test
	public void test_1174_LeaveReasonsEmpty() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String txtPackageEditionValidationName = "";
		String packageName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.OI_BRAZIL_BOI);
		}
		screenPerformListPackages.searchPackage("Smart");
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setCreationReasons("");
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			txtPackageEditionValidationName = screenPerformSmartCAPackage
					.getTextFrom("txtPackageEditionValidationField");
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setCreationReasons("");
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			txtPackageEditionValidationName = screenPerformSmartCAPackage
					.getTextFrom("txtPackageEditionValidationField");

		}
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationName);
	}

	/**
	 * @testLinkId T-GPRI-2902
	 */
	@Test
	public void test_1175_EditTargetDateForAfterDateThanCurrentDayForBuyerChild() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String txtTargetDate = "";
		String packageName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setTargetDate(ConstantsPackages.TARGET_DATE_26_11_18);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			screenPerformListPackages.clickOnEditPackageButton();
			txtTargetDate = screenPerformSmartCAPackage.getAttributeTextFrom("fieldTargetDate");
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setTargetDate(ConstantsPackages.TARGET_DATE_26_11_18);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			screenPerformListPackages.clickOnEditPackageButton();
			txtTargetDate = screenPerformSmartCAPackage.getAttributeTextFrom("fieldTargetDate");
		}
		assertEquals(ConstantsPackages.TARGET_DATE_26_11_18, txtTargetDate);
	}

	/**
	 * @testLinkId T-GPRI-2903
	 */
	@Test
	public void test_1176_EditTargetDateForAfterDateThanCurrentDayForBuyerMother() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";
		String nameStatus = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TLF_GROUP_EUROPE_6TL);
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			screenPerformListPackages.cleanFieldSearchPackage();
			nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.TLF_GROUP_EUROPE_6TL);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setTargetDate(ConstantsPackages.TARGET_DATE_26_11_18);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			screenPerformListPackages.clickOnEditPackageButton();
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setTargetDate(ConstantsPackages.TARGET_DATE_26_11_18);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
			screenPerformListPackages.clickOnEditPackageButton();
		}
		String txtTargetDate = screenPerformSmartCAPackage.getAttributeTextFrom("fieldTargetDate");
		assertEquals(ConstantsPackages.TARGET_DATE_26_11_18, txtTargetDate);
	}

	/**
	 * @testLinkId T-GPRI-2904
	 */
	@Test
	public void test_1177_TargetDateForBeforeDateThanCurrentDay() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setTargetDate(ConstantsPackages.TARGET_DATE_01_10_2016);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setTargetDate(ConstantsPackages.TARGET_DATE_01_10_2016);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		}
		String txtPackageEditionValidationDate = screenPerformSmartCAPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationDate);
	}

	/**
	 * @testLinkId T-GPRI-2905
	 */
	@Test
	public void test_1178_WithTheNameAlreadyExistingForTheBuyerInQuestion() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";

		screenPerformListPackages.openPage();

		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setPackageName(ConstantsPackages.TESTF_8);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setPackageName(ConstantsPackages.TESTF_8);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		}
		String txtPackageEditionValidationName = screenPerformSmartCAPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationName);

	}

	/**
	 * @testLinkId T-GPRI-2906
	 */
	@Test
	public void test_1179_EditNormalPackageWithNameWhithNameDoubleQuote() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_DOBLE_QUOTE);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_DOBLE_QUOTE);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		}
		String txtPackageEditionValidationDate = screenPerformSmartCAPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationDate);
	}

	/**
	 * @testLinkId T-GPRI-2907
	 */
	@Test
	public void test_1180_EditNormalPackageWithNameWithNameSingleQuote() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage,ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_SIMPLE_QUOTE);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_SIMPLE_QUOTE);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		}
		String txtPackageEditionValidationDate = screenPerformSmartCAPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationDate);
	}

	/**
	 * @testLinkId T-GPRI-2908
	 */
	@Test
	public void test_1181_EditNormalPackageWithNameWithNameBackslash() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_BACKSLASH);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setPackageName(ConstantsPackages.PACKAGE_NAME_BACKSLASH);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		}
		String txtPackageEditionValidationDate = screenPerformSmartCAPackage
				.getTextFrom("txtPackageEditionValidationField");
		assertEquals(ConstantsPackages.MESSAGE_FIELD_PACKAGE_EMPTY, txtPackageEditionValidationDate);
	}

	/**
	 * @testLinkId T-GPRI-2909
	 */
	@Test
	public void test_2909_ChangeProvidingType() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		String packageName = "";

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		screenPerformListPackages.searchPackage("Smart");
		if (!screenPerformListPackages.verifiExistingPackageResearched()) {
			String constructorNamePackage = screenPerformListPackages.constructorNamePackage();
			screenPerformSmartCAPackage.logicForCreateNewSmartCaPackage(nameStatus, constructorNamePackage, ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.searchPackage("Smart");
		}
		if (screenPerformListPackages.clickOnEditPackageButton()) {
			screenPerformSmartCAPackage.setProvidingType(ConstantsPackages.MANUAL_ACTIVATION);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		} else {
			screenPerformListPackages.reopenPackage(packageName);
			screenPerformListPackages.clickOnEditPackageButton();
			screenPerformSmartCAPackage.setProvidingType(ConstantsPackages.MANUAL_ACTIVATION);
			screenPerformSmartCAPackage.clickOnSaveEditedButton();
		}
		screenPerformListPackages.clickOnEditPackageButton();
		assertEquals(ConstantsPackages.MANUAL_ACTIVATION, screenPerformSmartCAPackage.getProvidingType());
	}

	/**
	 * @testLinkId T-GPRI-2910
	 */
	@Test
	public void test_1183_WithStatusConfirmed() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		screenPerformListPackages.openPage();
		ArrayList<String> createdPackage = CreateSmartCAPackage();

		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(createdPackage.get(0));
		screenPerformListPackages.searchPackage(createdPackage.get(1));
		screenPerformListPackages.clickOnConfirmPackageButton();
		screenPerformListPackages.clickOnOkConfirmSmartCA();
		screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
		assertFalse(screenPerformListPackages.clickOnEditPackageButton());
	}

	/**
	 * Method for create smart ca package
	 */
	private ArrayList<String> CreateSmartCAPackage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		ArrayList<String> infoPackage = new ArrayList<String>();

		screenPerformListPackages.clickOnCreateSmartCAButton();
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, ConstantsPackages.FIRST,
				ConstantsPackages.PACKAGE_NAME_TESTSMARTCA, ConstantsPackages.TODAY,
				ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.NORMAL_SMARTCA, ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI,
				ConstantsPackages.OPEN_SW_YES, ConstantsPackages.NT_CODE_LIST_1_FFF,
				ConstantsPackages.SW_VERSION_FROM, ConstantsPackages.SW_VERSION_TO);
		screenPerformSmartCAPackage.moveToPageHeader();
		String buyerName = screenPerformSmartCAPackage.getBuyer();
		String packageName = screenPerformSmartCAPackage.getPackageName();
		infoPackage.add(buyerName);
		infoPackage.add(packageName);
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		screenPerformSmartCAPackage.clickOnOkPackageSuccessCreated();
		return infoPackage;
	}
}
