package com.lge.gpri.tests.packages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.screens.packages.ScreenPerformNormalPackage;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery SCENARY NAME.
 * 
 * @author AUTHOR NAME (can exist more than one)
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestStateMachineNormalPackage extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2492
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_2492_ViewPermalinksWhenStatusIsCommiting() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		createNormalPackage(ConstantsPackages.LGH815, ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnConfirmPackageButton();
		screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
		screenPerformListPackages.clickOnPermalinksPackage();
		String txt = screenPerformListPackages.getTextFrom("noPermalinksFound");
		assertEquals(ConstantsPackages.NO_PERMALINKS_FOUND, txt);
	}

	/**
	 * @testLinkId T-GPRI-2493
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_2493_ConfirmPackageWithSuccess() throws Exception {
		boolean status = false;
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		createNormalPackage(ConstantsPackages.LGH815, ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnConfirmPackageButton();
		screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
		screenPerformListPackages.waitLoading();
		status = screenPerformListPackages.checkStatus();
		String branchTxt = "";
		if (status) {
			screenPerformListPackages.clickOnPermalinksPackage();
			screenPerformListPackages.clickOnPermalinkPopUp();
			screenPerformListPackages.switchTabs(1);
			branchTxt = screenPerformListPackages.getTextFrom("branchTxt");
			getDriver().close();
			screenPerformListPackages.switchTabs(0);
			assertEquals(ConstantsPackages.LG_APPS_M_RELEASE_TEST, branchTxt);
		}
		assertEquals(ConstantsPackages.LG_APPS_M_RELEASE_TEST, branchTxt);
		assertTrue(status);
	}

	/**
	 * @testLinkId T-GPRI-2494
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_2494_ScriptOfPackage() throws Exception {
		boolean result = false;
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		createNormalPackage(ConstantsPackages.LGH815, ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnConfirmPackageButton();
		screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
		screenPerformListPackages.clickOnCommitingButton();
		screenPerformListPackages.clickOnGpriMButton();
		screenPerformListPackages.checkBuilding();
		screenPerformListPackages.checkBuilding();
		screenPerformListPackages.clickOnMoreDetails();
		result = screenPerformListPackages.clickOnDownloadLog();
		assertTrue(result);
	}

	/**
	 * @testLinkId T-GPRI-2495
	 * @summary THE TITLE OF THE TEST LINK CASE BEING AUTOMATED.
	 */
	@Test
	public void test_2495_ReopenPackageConfirmed() throws Exception {
		boolean result = false;
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());

		createNormalPackage(ConstantsPackages.LGH815, ConstantsPackages.TIM_BRAZIL_BTM);
		screenPerformListPackages.clickOnConfirmPackageButton();
		screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
		result = screenPerformListPackages.checkStatus();
		if (result) {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnOKButtonAfterReopenPackage();
			String status = screenPerformListPackages.getPackageStatus();
			assertEquals(ConstantsPackages.UNDER_EDITION, status);
		}
		assertTrue(result);
	}

	public void createNormalPackage(String model, String buyer) throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(model);
		screenPerformListPackages.setBuyer(buyer);
		boolean isUnderEdition = screenPerformListPackages.clickOnRemovePackageButton();
		if (isUnderEdition) {
			screenPerformListPackages.confirmRemovePackage();
		}

		String namePackage = screenPerformListPackages.getExistingPackage();

		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F_)) {
			int numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(model, "", buyer,
						ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			} else {

				screenPerformListPackages.clickOnCreatePackageButton();
				screenPerformNormalPackage.fillFieldsPackage(model, "", buyer,
						ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1), ConstantsPackages.TODAY,
						ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
						ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
						ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
						ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
				screenPerformNormalPackage.clickOnSaveCreatedButton();
			}

		} else {
			screenPerformListPackages.waitLoading();
			screenPerformListPackages.clickOnCreatePackageButton();
			screenPerformListPackages.waitLoading();
			screenPerformNormalPackage.fillFieldsPackage(model, "", buyer, ConstantsPackages.PACKAGE_NAME_TEST_F_1,
					ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB, "1",
					ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.ANDROID_VERSION_5,
					ConstantsPackages.GRID_ROW_COLUMN_5X5, ConstantsPackages.OS_VERSION_M,
					ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
			screenPerformNormalPackage.clickOnSaveCreatedButton();
		}
		screenPerformNormalPackage.clickOnKeepConfirmationButton();
		screenPerformNormalPackage.clickOnOkPackageSuccessCreated();
	}
}
