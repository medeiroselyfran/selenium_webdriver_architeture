package com.lge.gpri.tests.packages.editProfile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.packages.ScreenPerformEditPackageProfile;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* <pre>
* Author: Lucas Sales
* Purpose: <Type here the purpose of the class> - Lucas Sales - 16 de nov de 2017
* History: Class creation - Lucas Sales - 16 de nov de 2017
*
*</pre>
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestComponents extends AbstractTestScenary{
	
	/**
	* @testLinkId T-GPRI-3132
	* @summary Only checked items
	*/
	@Test
	public void test_2227_OnlyCheckedItems() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.clickOnOnlyCheckedItems();
		assertTrue(screenPerformEditPackageProfile.verifyElementsCheckedsIsDisplayed());
		assertFalse(screenPerformEditPackageProfile.verifyElementsUncheckedsIsDisplayed());
		screenPerformEditPackageProfile.clickOnOnlyCheckedItems();
		assertTrue(screenPerformEditPackageProfile.verifyElementsUncheckedsIsDisplayed());
	}
	
	/**
	* @testLinkId T-GPRI-3133
	* @summary Search
	*/
	@Test
	public void test_2228_Search() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		boolean resultOk = screenPerformListPackages.clickOnEditProfilePackageButton();
		if(!resultOk){
			screenPerformListPackages.rejectGeneratingAPK(ConstantsPackages.OI_BRAZIL_BOI);
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.addItemByPath("");
		screenPerformEditPackageProfile.removeItemByPath("");
		screenPerformEditPackageProfile.addItemByPath(ConstantsPackages.PATH_HOME_NETWORK);
		screenPerformEditPackageProfile.searchItemSelectItems(ConstantsPackages.HOME_NETWORK);
		assertTrue(screenPerformEditPackageProfile.itemIsHighlightedInTree(ConstantsPackages.PATH_HOME_NETWORK));
		assertFalse(screenPerformEditPackageProfile.itemIsHighlightedInTree("Parameters::Network::Emergency Numbers::Always Available"));
	}
	
	/**
	* @testLinkId T-GPRI-3134
	* @summary Add one item do not apply with group multiple 
	*/
	@Test
	public void test_2229_AddOneItemDoNotApplyWithGroupMultiple() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.ENABLE_RCS);
		screenPerformEditPackageProfile.clickOnMarkItemsAsDoNotApplyButton();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		Thread.sleep(1000);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.searchItemSelectItems( ConstantsPackages.ENABLE_RCS);
		String elementClass = screenPerformEditPackageProfile.getAttributeClassOfElement("enableRcs");
		
		assertEquals(ConstantsPackages.ELEMENT_CLASS, elementClass);
	}
	
	/**
	* @testLinkId T-GPRI-3135
	* @summary Add more than one item do not apply with group multiple
	*/
	@Test
	public void test_2231_AddMoreThanOneItemDoNotApplyWithGroupMultiple() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		if(screenPerformEditPackageProfile.unselectItemInSelectItemsTree(ConstantsPackages.CUSTOM_ICON) && screenPerformEditPackageProfile.unselectItemInSelectItemsTree(ConstantsPackages.CUSTOM_LABEL)){
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		}
		screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.CUSTOM_ICON);
		screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.CUSTOM_LABEL);
		screenPerformEditPackageProfile.clickOnMarkItemsAsDoNotApplyButton();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		assertTrue(screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.CUSTOM_ICON));
		assertTrue(screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.CUSTOM_LABEL));
	}
	
	
	/**
	* @testLinkId T-GPRI-3136
	* @summary Add more than one item do not apply with group multiple and disabled one item
	*/
	@Test
	public void test_2232_AddMoreThanOneItemDoNotApplyWithGroupMultipleandDisabledOneItem() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		if(screenPerformEditPackageProfile.unselectItemInSelectItemsTree(ConstantsPackages.CUSTOM_ICON) && screenPerformEditPackageProfile.unselectItemInSelectItemsTree(ConstantsPackages.CUSTOM_LABEL)){
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		}
		screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.CUSTOM_ICON);
		screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.CUSTOM_LABEL);
		screenPerformEditPackageProfile.clickOnMarkItemsAsDoNotApplyButton();
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.unselectItemInSelectItemsTree(ConstantsPackages.CUSTOM_LABEL);
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		assertTrue(screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.CUSTOM_ICON));
		assertTrue(screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.CUSTOM_LABEL));
	}
	
	/**
	* @testLinkId T-GPRI-3137
	* @summary Add a one item as do not apply of an item not associated with a multiple group
	*/
	@Test
	public void test_2233_AddAOneItemAsDoNotApplyOfAnItemNotAssociatedWithAMultipleGroup() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		if(screenPerformEditPackageProfile.unselectItemInSelectItemsTree(ConstantsPackages.BLANK_HOMESCREEN)){
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		}
		screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.BLANK_HOMESCREEN);
		screenPerformEditPackageProfile.clickOnMarkItemsAsDoNotApplyButton();
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		assertTrue(screenPerformEditPackageProfile.isItemCheckedByPath(ConstantsPackages.PATH_BLANK_HOMESCREEN));
	}
	
	
	/**
	* @testLinkId T-GPRI-3138
	* @summary View Items
	*/
	@Test
	public void test_2234_ViewItems() throws Exception{
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(getDriver());
		
		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.OI_BRAZIL_BOI);
		screenPerformEditPackageProfile.verifyStatusAndClickOnEditProfilePackage();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		if(screenPerformEditPackageProfile.unselectItemInSelectItemsTree(ConstantsPackages.PACKAGE_ROAMING_ICON)){
			screenPerformEditPackageProfile.clickOnSaveSelectedItems();
			screenPerformEditPackageProfile.clickOnSelectItemsButton();
		}
		screenPerformEditPackageProfile.selectItemInSelectItemsTree(ConstantsPackages.PACKAGE_ROAMING_ICON);
		assertTrue(screenPerformEditPackageProfile.clickOnSaveSelectedItems());
		screenPerformEditPackageProfile.btnOptionInTree(ConstantsPackages.BTN_COLLAPSE_ALL);
		assertTrue(screenPerformEditPackageProfile.verifyClassElement(ConstantsPackages.PACKAGE_ROAMING_ICON, "ttCollapsed"));
		screenPerformEditPackageProfile.btnOptionInTree(ConstantsPackages.BTN_EXPAND_ALL);
		assertFalse(screenPerformEditPackageProfile.verifyClassElement(ConstantsPackages.PACKAGE_ROAMING_ICON, "ttCollapsed"));
	}
}