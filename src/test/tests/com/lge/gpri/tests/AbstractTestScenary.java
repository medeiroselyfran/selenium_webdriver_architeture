package com.lge.gpri.tests;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.rules.Timeout;
import org.openqa.selenium.WebDriver;

import com.lge.gpri.helpers.report.Configuration;
import com.lge.gpri.helpers.GpriDriver;
import com.lge.gpri.helpers.Preconditions;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.helpers.report.Report;
import com.lge.gpri.helpers.report.ReportManager;
import com.lge.gpri.helpers.report.ReportManagerWatcher;
import com.lge.gpri.helpers.report.TestLink;

//This class must be instantiated by all class of Test Scenario.

/**
* Test Scenery Abstract class.
* 
* <p>Description: This class must contains the driver object and methods of setUp() and tearDown().
*  
* @author Gabriel Costa do Nascimento 
*/

public abstract class AbstractTestScenary {
	public WebDriver driver;
	private int reportPos = 0;
	private Report mReport;
	public Preconditions preconditions;
	public static boolean clearResult = TestLink.clearFileResults();
	
	@Rule
	public ReportManagerWatcher reportTestWacther = new ReportManagerWatcher(ReportManager.getInstance().getReport(reportPos));
	 
	@Rule
	public TestName testName = new TestName();
	
	@Rule
    public Timeout globalTimeout = Timeout.seconds(600);
	
	@BeforeClass
    public static void beforeClass() {
		Configuration.initFromFile();
		ReportManager.getInstance().addReport(Configuration.releaseId);
		TestLink.readResultsFromFile();
    }
	
	@AfterClass
    public static void afterClass() {
		TestLink.saveResultsToFile();
    }
	
	@Before
	public void before() {
		System.setProperty(ConstantsGeneric.KEY_DRIVER, ConstantsGeneric.PATH);
		this.driver = new GpriDriver(700);
		this.driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		if(preconditions==null){
			preconditions = new Preconditions(driver);
		}
		
		this.mReport = ReportManager.getInstance().getReport(this.reportPos);
		
		String testScenaryName = this.getClass().getSimpleName();
		if (this.mReport.getTestScenary(testScenaryName) == null) {
			this.mReport.addTestScenary(testScenaryName);
		}
		this.mReport.addTestCaseCurrentTestScenary(this.testName.getMethodName());
	}

	@After
	public void after() {
		driver.quit();
		TestLink.mTestLinkIdCurrentTest = Configuration.projectLabel+this.testName.getMethodName().split("_")[1];
		this.mReport.writeResults();
	}
	
	public WebDriver getDriver() {
		return this.driver;
	}
}