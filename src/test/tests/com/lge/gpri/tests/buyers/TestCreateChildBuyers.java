package com.lge.gpri.tests.buyers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.screens.buyers.AutoProfile;
import com.lge.gpri.screens.buyers.ScreenPerformBuyer;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Create Child
 * Buyer.
 * 
 * @author Gabriel Costa do Nascimento
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCreateChildBuyers extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2689
	 * @summary Auto Profile MCC/MNC and without Global Group
	 */
	@Test
	public void test_1841_AutoProfileMCC_MNCandWithoutGlobalGroup() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_730_30));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.SECOND, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved = autoProfilesSaved.get(0).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String region = screenPerformBuyer.getRegion();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(regionSaved, region);
		assertEquals(typeAutoProfileSaved, typeAutoProfile);
		assertEquals(valueAutoProfileSaved, valueAutoProfile);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2690
	 * @summary Auto Profile MCC/MNC + GID1 and without Global Group
	 */
	@Test
	public void test_1842_AutoProfileMCC_MNC_GID1andWithoutGlobalGroup() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_GID1, ConstantsBuyers.VALUE_714_06_11345678));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.SECOND, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved = autoProfilesSaved.get(0).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String region = screenPerformBuyer.getRegion();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(regionSaved, region);
		assertEquals(typeAutoProfileSaved, typeAutoProfile);
		assertEquals(valueAutoProfileSaved, valueAutoProfile);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2691
	 * @summary Auto Profile MCC/MNC + SPN and without Global Group
	 */
	@Test
	public void test_1843_AutoProfileMCC_MNC_SPNandWithoutGlobalGroup() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_SPN, ConstantsBuyers.VALUE_714_06_Orange));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.SECOND, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved = autoProfilesSaved.get(0).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String region = screenPerformBuyer.getRegion();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(regionSaved, region);
		assertEquals(typeAutoProfileSaved, typeAutoProfile);
		assertEquals(valueAutoProfileSaved, valueAutoProfile);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2692
	 * @summary Auto Profile MCC/MNC + IMSI and without Global Group
	 */
	@Test
	public void test_1844_AutoProfileMCC_MNC_IMSIandWithoutGlobalGroup() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved
				.add(new AutoProfile(ConstantsBuyers.MCC_MNC_IMSI, ConstantsBuyers.VALUE_113_11_7140x13919191x3));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.SECOND, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved = autoProfilesSaved.get(0).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String region = screenPerformBuyer.getRegion();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(regionSaved, region);
		assertEquals(typeAutoProfileSaved, typeAutoProfile);
		assertEquals(valueAutoProfileSaved, valueAutoProfile);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2693
	 * @summary Auto Profile MCC/MNC + ICCID and without Global Group
	 */
	@Test
	public void test_1845_AutoProfileMCC_MNC_ICCIDandWithoutGlobalGroup() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_ICCID, ConstantsBuyers.VALUE_714_06_11345678));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved = autoProfilesSaved.get(0).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String region = screenPerformBuyer.getRegion();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(regionSaved, region);
		assertEquals(typeAutoProfileSaved, typeAutoProfile);
		assertEquals(valueAutoProfileSaved, valueAutoProfile);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2694
	 * @summary Auto Profile MCC/MNC and with Global Group
	 */
	@Test
	public void test_1846_AutoProfileMCC_MNCandWithGlobalGroup() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_714_06));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, ConstantsBuyers.DTAG, autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String globalGroupSaved = screenPerformBuyer.getGlobalGroup();
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved = autoProfilesSaved.get(0).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String globalGroup = screenPerformBuyer.getGlobalGroup();
		String region = screenPerformBuyer.getRegion();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(globalGroupSaved, globalGroup);
		assertEquals(regionSaved, region);
		assertEquals(typeAutoProfileSaved, typeAutoProfile);
		assertEquals(valueAutoProfileSaved, valueAutoProfile);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2695
	 * @summary Auto Profile MCC/MNC+SPN and with Global Group
	 */
	@Test
	public void test_1847_AutoProfileMCC_MNC_SPNandWithGlobalGroup() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_SPN, ConstantsBuyers.VALUE_714_06_Orange));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, ConstantsBuyers.DTAG, autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String globalGroupSaved = screenPerformBuyer.getGlobalGroup();
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved = autoProfilesSaved.get(0).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String globalGroup = screenPerformBuyer.getGlobalGroup();
		String region = screenPerformBuyer.getRegion();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(globalGroupSaved, globalGroup);
		assertEquals(regionSaved, region);
		assertEquals(typeAutoProfileSaved, typeAutoProfile);
		assertEquals(valueAutoProfileSaved, valueAutoProfile);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2696
	 * @summary Two Values of Auto Profile of the same type
	 */
	@Test
	public void test_1848_TwoValuesOfAutoProfileOfTheSameType() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_730_30));
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_731_31));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved1 = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved1 = autoProfilesSaved.get(0).getValue();
		String typeAutoProfileSaved2 = autoProfilesSaved.get(1).getType();
		String valueAutoProfileSaved2 = autoProfilesSaved.get(1).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String region = screenPerformBuyer.getRegion();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile1 = autoprofiles.get(0).getType();
		String valueAutoProfile1 = autoprofiles.get(0).getValue();
		String typeAutoProfile2 = autoprofiles.get(1).getType();
		String valueAutoProfile2 = autoprofiles.get(1).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(regionSaved, region);
		assertEquals(typeAutoProfileSaved1, typeAutoProfile1);
		assertEquals(valueAutoProfileSaved1, valueAutoProfile1);
		assertEquals(typeAutoProfileSaved2, typeAutoProfile2);
		assertEquals(valueAutoProfileSaved2, valueAutoProfile2);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2697
	 * @summary Two Auto Profile in different types
	 */
	@Test
	public void test_1849_TwoAutoProfileInDifferentTypes() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_730_30));
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_GID1, ConstantsBuyers.VALUE_714_06_11345678));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved1 = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved1 = autoProfilesSaved.get(0).getValue();
		String typeAutoProfileSaved2 = autoProfilesSaved.get(1).getType();
		String valueAutoProfileSaved2 = autoProfilesSaved.get(1).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String region = screenPerformBuyer.getRegion();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile1 = autoprofiles.get(0).getType();
		String valueAutoProfile1 = autoprofiles.get(0).getValue();
		String typeAutoProfile2 = autoprofiles.get(1).getType();
		String valueAutoProfile2 = autoprofiles.get(1).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(regionSaved, region);
		assertEquals(typeAutoProfileSaved1, typeAutoProfile1);
		assertEquals(valueAutoProfileSaved1, valueAutoProfile1);
		assertEquals(typeAutoProfileSaved2, typeAutoProfile2);
		assertEquals(valueAutoProfileSaved2, valueAutoProfile2);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2698
	 * @summary More than one LTM
	 */
	@Test
	public void test_1850_MoreThanOneLTM() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_730_30));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST, ConstantsBuyers.SECOND), Arrays.asList(""));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved = autoProfilesSaved.get(0).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved1 = ltmsSaved.get(0);
		String ltmSaved2 = ltmsSaved.get(1);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String region = screenPerformBuyer.getRegion();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm1 = ltms.get(0);
		String ltm2 = ltms.get(1);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(regionSaved, region);
		assertEquals(typeAutoProfileSaved, typeAutoProfile);
		assertEquals(valueAutoProfileSaved, valueAutoProfile);
		assertEquals(ltmSaved1, ltm1);
		assertEquals(ltmSaved2, ltm2);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2699
	 * @summary Hide on View
	 */
	@Test
	public void test_1851_HideOnView() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_730_30));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		screenPerformBuyer.checkOnHideOnView();

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved = autoProfilesSaved.get(0).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		boolean isShowing = screenPerformListBuyers.clickOnEditBuyer(suffixSaved);
		screenPerformListBuyers.clickOnShowHiddenBuyersButton();
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String region = screenPerformBuyer.getRegion();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertFalse(isShowing);
		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(regionSaved, region);
		assertEquals(typeAutoProfileSaved, typeAutoProfile);
		assertEquals(valueAutoProfileSaved, valueAutoProfile);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2700
	 * @summary Type MVNO
	 */
	@Test
	public void test_1852_TypeMVNO() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.checkMVNO();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_GID1, ConstantsBuyers.VALUE_714_06_11345678));
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String networkOwnerSaved = ConstantsBuyers.MOBILE_ISRAEL_ISM;
		screenPerformBuyer.setNetworkOwner(networkOwnerSaved);

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyerSecondDropdownBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String regionSaved = screenPerformBuyer.getRegionFirstDropdownBuyer();
		String typeAutoProfileSaved = autoProfilesSaved.get(0).getType();
		String valueAutoProfileSaved = autoProfilesSaved.get(0).getValue();
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		screenPerformBuyer.clickOnSaveBuyer();
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			screenPerformBuyer.clickOnSaveBuyer();
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String region = screenPerformBuyer.getRegion();
		String networkOwner = screenPerformBuyer.getNetworkOwner();
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(regionSaved, region);
		assertEquals(networkOwnerSaved, networkOwner);
		assertEquals(typeAutoProfileSaved, typeAutoProfile);
		assertEquals(valueAutoProfileSaved, valueAutoProfile);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2701
	 * @summary Only the type of the buyer is selected
	 */
	@Test
	public void test_1853_OnlyTheTypeOfTheBuyerIsSelected() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		boolean sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR_NETWORK_CODE);

		assertEquals(ConstantsBuyers.WARNING_NETWORK_CODE, msg);
	}

	/**
	 * @testLinkId T-GPRI-2702
	 * @summary Only the type of the buyer and buyer selected
	 */
	@Test
	public void test_1854_OnlyTheTypeOfTheBuyerAndBuyerSelected() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.setBuyer(ConstantsBuyers.SECOND);
		boolean sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR_NETWORK_CODE);

		assertEquals(ConstantsBuyers.WARNING_NETWORK_CODE, msg);
	}

	/**
	 * @testLinkId T-GPRI-2703
	 * @summary Only the type of the buyer, buyer and Global Group selected
	 */
	@Test
	public void test_1855_OnlyTheTypeOfTheBuyer_BuyerAndGlobalGroupSelected() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.setBuyer(ConstantsBuyers.SECOND);
		screenPerformBuyer.setGlobalGroup(ConstantsBuyers.DTAG);
		boolean sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR_NETWORK_CODE);

		assertEquals(ConstantsBuyers.WARNING_NETWORK_CODE, msg);
	}

	/**
	 * @testLinkId T-GPRI-2704
	 * @summary Only the type of the buyer, buyer and MCC/MNC selected
	 */
	@Test
	public void test_1856_OnlyTheTypeOfTheBuyer_BuyerAndMCC_MNCSelected() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.setBuyer(ConstantsBuyers.SECOND);
		screenPerformBuyer.addAutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_730_30);
		boolean sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR_LTM);

		assertEquals(ConstantsBuyers.WARNING_LTM_USER, msg);
	}
}
