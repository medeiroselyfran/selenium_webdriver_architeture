package com.lge.gpri.tests.buyers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.screens.buyers.AutoProfile;
import com.lge.gpri.screens.buyers.ScreenPerformBuyer;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Edit Child
 * Buyers.
 * 
 * @author AUTHOR NAME (can exist more than one)
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestEditChildBuyers extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2705
	 * @summary Remove the MCC/MNC of the Buyer when has one more MCC/MNC
	 */
	@Test
	public void test_1857_RemoveTheMCC_MNCOfTheBuyerWhenHasOneMoreMCC_MNC() throws Exception {
		boolean successfullSaving;
		boolean successfullClickEdit;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_3IR);
		successfullClickEdit = screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		if (!successfullClickEdit) {
			screenPerformListBuyers.clickOnShowDeletedBuyersButton();
			if (screenPerformListBuyers.clickOnRestoreBuyer(ConstantsBuyers.SUFFIX_3IR)) {
				screenPerformListBuyers.confirmRestoreBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			} else {
				screenPerformListBuyers.clickOnShowHiddenBuyersButton();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
				screenPerformBuyer.uncheckOnHideOnView();
				screenPerformBuyer.clickOnSaveBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			}
		}
		screenPerformBuyer.removeAllAutoProfiles();
		screenPerformBuyer.addAutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_587_965);
		screenPerformBuyer.addAutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_799_99);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		screenPerformBuyer.removeAutoProfile(ConstantsBuyers.VALUE_799_99);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();

		assertEquals(ConstantsBuyers.MCC_MNC, typeAutoProfile);
		assertEquals(ConstantsBuyers.VALUE_587_965, valueAutoProfile);
	}

	/**
	 * @testLinkId T-GPRI-2706
	 * @summary Remove Auto Profile when the Buyer Has only one Auto Profile
	 */
	@Test
	public void test_1858_RemoveAutoProfileWhenTheBuyerHasOnlyOneAutoProfile() throws Exception {
		boolean successfullSaving;
		boolean successfullClickEdit;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_3IR);
		successfullClickEdit = screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		if (!successfullClickEdit) {
			screenPerformListBuyers.clickOnShowDeletedBuyersButton();
			if (screenPerformListBuyers.clickOnRestoreBuyer(ConstantsBuyers.SUFFIX_3IR)) {
				screenPerformListBuyers.confirmRestoreBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			} else {
				screenPerformListBuyers.clickOnShowHiddenBuyersButton();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
				screenPerformBuyer.uncheckOnHideOnView();
				screenPerformBuyer.clickOnSaveBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			}
		}
		screenPerformBuyer.removeAllAutoProfiles();
		screenPerformBuyer.addAutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_587_965);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		screenPerformBuyer.removeAutoProfile(ConstantsBuyers.VALUE_587_965);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR_NETWORK_CODE);

		assertEquals(ConstantsBuyers.WARNING_NETWORK_CODE, msg);
	}

	/**
	 * @testLinkId T-GPRI-2707
	 * @summary Cancel Remove MCC/MNC
	 */
	@Test
	public void test_1859_CancelRemoveMCC_MNC() throws Exception {
		boolean successfullSaving;
		boolean successfullClickEdit;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_3IR);
		successfullClickEdit = screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		if (!successfullClickEdit) {
			screenPerformListBuyers.clickOnShowDeletedBuyersButton();
			if (screenPerformListBuyers.clickOnRestoreBuyer(ConstantsBuyers.SUFFIX_3IR)) {
				screenPerformListBuyers.confirmRestoreBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			} else {
				screenPerformListBuyers.clickOnShowHiddenBuyersButton();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
				screenPerformBuyer.uncheckOnHideOnView();
				screenPerformBuyer.clickOnSaveBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			}
		}
		screenPerformBuyer.removeAllAutoProfiles();
		screenPerformBuyer.addAutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_587_965);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		screenPerformBuyer.removeAutoProfile(ConstantsBuyers.VALUE_587_965);
		screenPerformBuyer.clickOnCancelBuyer();
		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile = autoprofiles.get(0).getType();
		String valueAutoProfile = autoprofiles.get(0).getValue();

		assertEquals(ConstantsBuyers.MCC_MNC, typeAutoProfile);
		assertEquals(ConstantsBuyers.VALUE_587_965, valueAutoProfile);
	}

	/**
	 * @testLinkId T-GPRI-2708
	 * @summary Add the MCC/MNC of the Buyer
	 */
	@Test
	public void test_1860_AddTheMCC_MNCOfTheBuyer() throws Exception {
		boolean successfullSaving;
		boolean successfullClickEdit;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_3IR);
		successfullClickEdit = screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		if (!successfullClickEdit) {
			screenPerformListBuyers.clickOnShowDeletedBuyersButton();
			if (screenPerformListBuyers.clickOnRestoreBuyer(ConstantsBuyers.SUFFIX_3IR)) {
				screenPerformListBuyers.confirmRestoreBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			} else {
				screenPerformListBuyers.clickOnShowHiddenBuyersButton();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
				screenPerformBuyer.uncheckOnHideOnView();
				screenPerformBuyer.clickOnSaveBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			}
		}
		screenPerformBuyer.removeAllAutoProfiles();
		screenPerformBuyer.addAutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_587_965);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		screenPerformBuyer.addAutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_799_99);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		ArrayList<AutoProfile> autoprofiles = screenPerformBuyer.getAutoProfiles();
		String typeAutoProfile1 = autoprofiles.get(0).getType();
		String valueAutoProfile1 = autoprofiles.get(0).getValue();
		String typeAutoProfile2 = autoprofiles.get(1).getType();
		String valueAutoProfile2 = autoprofiles.get(1).getValue();

		assertEquals(ConstantsBuyers.MCC_MNC, typeAutoProfile1);
		assertEquals(ConstantsBuyers.VALUE_587_965, valueAutoProfile1);
		assertEquals(ConstantsBuyers.MCC_MNC, typeAutoProfile2);
		assertEquals(ConstantsBuyers.VALUE_799_99, valueAutoProfile2);
	}

	/**
	 * @testLinkId T-GPRI-2709
	 * @summary Add the LTM of the Buyer
	 */
	@Test
	public void test_1861_AddTheLTMOfTheBuyer() throws Exception {
		boolean successfullSaving;
		boolean successfullClickEdit;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_3IR);
		successfullClickEdit = screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		if (!successfullClickEdit) {
			screenPerformListBuyers.clickOnShowDeletedBuyersButton();
			if (screenPerformListBuyers.clickOnRestoreBuyer(ConstantsBuyers.SUFFIX_3IR)) {
				screenPerformListBuyers.confirmRestoreBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			} else {
				screenPerformListBuyers.clickOnShowHiddenBuyersButton();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
				screenPerformBuyer.uncheckOnHideOnView();
				screenPerformBuyer.clickOnSaveBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			}
		}
		screenPerformBuyer.removeAllLTMs();
		screenPerformBuyer.addLTM(ConstantsBuyers.ALEX_RECHTMAN_COMPLETE);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI_COMPLETE);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm1 = ltms.get(0).split(" \\(")[0];
		String ltm2 = ltms.get(1).split(" \\(")[0];

		assertEquals(ConstantsBuyers.AJAY_CHEECKOLI_COMPLETE, ltm1);
		assertEquals(ConstantsBuyers.ALEX_RECHTMAN_COMPLETE, ltm2);
	}

	/**
	 * @testLinkId T-GPRI-2710
	 * @summary Remove One LTM when the Buyer Has more than one LTM
	 */
	@Test
	public void test_1862_RemoveOneLTMWhenTheBuyerHasMoreThanOneLTM() throws Exception {
		boolean successfullSaving;
		boolean successfullClickEdit;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_3IR);
		successfullClickEdit = screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		if (!successfullClickEdit) {
			screenPerformListBuyers.clickOnShowDeletedBuyersButton();
			if (screenPerformListBuyers.clickOnRestoreBuyer(ConstantsBuyers.SUFFIX_3IR)) {
				screenPerformListBuyers.confirmRestoreBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			} else {
				screenPerformListBuyers.clickOnShowHiddenBuyersButton();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
				screenPerformBuyer.uncheckOnHideOnView();
				screenPerformBuyer.clickOnSaveBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			}
		}

		screenPerformBuyer.removeAllLTMs();
		screenPerformBuyer.addLTM(ConstantsBuyers.ALEX_RECHTMAN_COMPLETE);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI_COMPLETE);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		screenPerformBuyer.removeLTM(ConstantsBuyers.ALEX_RECHTMAN_COMPLETE);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm1 = ltms.get(0).split(" \\(")[0];

		assertEquals(ConstantsBuyers.AJAY_CHEECKOLI_COMPLETE, ltm1);
		assertEquals(ltms.size(), 1);
	}

	/**
	 * @testLinkId T-GPRI-2711
	 * @summary Remove LTM when the Buyer Has only one LTM
	 */
	@Test
	public void test_1863_RemoveLTMWhenTheBuyerHasOnlyOneLTM() throws Exception {
		boolean successfullSaving;
		boolean successfullClickEdit;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_3IR);
		successfullClickEdit = screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		if (!successfullClickEdit) {
			screenPerformListBuyers.clickOnShowDeletedBuyersButton();
			if (screenPerformListBuyers.clickOnRestoreBuyer(ConstantsBuyers.SUFFIX_3IR)) {
				screenPerformListBuyers.confirmRestoreBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			} else {
				screenPerformListBuyers.clickOnShowHiddenBuyersButton();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
				screenPerformBuyer.uncheckOnHideOnView();
				screenPerformBuyer.clickOnSaveBuyer();
				screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			}
		}
		screenPerformBuyer.removeAllLTMs();
		screenPerformBuyer.addLTM(ConstantsBuyers.ALEX_RECHTMAN_COMPLETE);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
		screenPerformBuyer.removeLTM(ConstantsBuyers.ALEX_RECHTMAN_COMPLETE);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR_LTM);

		assertEquals(ConstantsBuyers.WARNING_LTM_USER, msg);
	}
}
