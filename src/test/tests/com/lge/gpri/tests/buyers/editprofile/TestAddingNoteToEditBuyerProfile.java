package com.lge.gpri.tests.buyers.editprofile;

import static com.lge.gpri.helpers.constants.ConstantsBuyers.*;
import static com.lge.gpri.helpers.constants.ConstantsView.HOME_NETWORK_VIEW;
import static com.lge.gpri.helpers.constants.ConstantsView.HOME_NETWORK_HISTORY;
import static com.lge.gpri.helpers.constants.ConstantsView.ITEM_HISTORY_NOTE;
import static com.lge.gpri.helpers.constants.ConstantsView.ITEM_HISTORY_REQUESTER;
import static com.lge.gpri.helpers.constants.ConstantsView.ITEM_HISTORY_APPROVER;
import static com.lge.gpri.helpers.constants.ConstantsUserManagement.YARA_MARTINS;
import static com.lge.gpri.helpers.constants.ConstantsUserManagement.ANDRESSA_LEITE;
import static com.lge.gpri.helpers.constants.ConstantsUserManagement.ABDUL_ANSARI;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsItemInspector;
import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.buyers.ScreenPerformBuyerApproval;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.usermanagement.ScreenUsersManagement;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Adding a note to Edit Buyer Profile.
*  
* @author Cayk Lima Barreto
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAddingNoteToEditBuyerProfile extends AbstractTestScenary{

	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3244
	* @summary An unapproved item with a note related to change 
	*/
	@Test
	public void test_2303_AnUnapprovedItemWithNoteRelatedToChange() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		String noteMsg = "";
		String itemHistoryMsg = "";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.addItem(ITEM_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.addNote(FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, CRITICAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.checkStatusValidated(SUFFIX_CLR, FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.clickOnNote();
		noteMsg = screenPerformEditBuyerProfile.getValue(FIELD_NOTE);
		String noteMsgFormated = noteMsg.split("\n")[0];
		screenPerformEditBuyerProfile.clickOnCloseNote();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_CLARO_BRAZIL_CLR);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformView.waitLoadingOverlay();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnItemValue(HOME_NETWORK_VIEW);
		screenPerformViewValues.clickOnItemHistory(HOME_NETWORK_HISTORY);
		itemHistoryMsg = screenPerformViewValues.getTextFrom(ITEM_HISTORY_NOTE);
		
		assertEquals(NOTE_MESSAGE, noteMsgFormated);
		assertEquals(NOTE_MESSAGE, itemHistoryMsg);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3245
	* @summary An approved item with a note related to change 
	*/
	@Test
	public void test_2304_AnApprovedItemWithNoteRelatedToChange() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		boolean existNote = true;
		String itemHistoryMsg = "";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.addItem(ITEM_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.addNote(FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, CRITICAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.checkStatusValidated(SUFFIX_CLR, FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE);
		screenPerformListBuyers.clickOnStatusChangeItem(SUFFIX_CLR);
		screenPerformListBuyers.clickOnStatusPendingApproval(SUFFIX_CLR);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.moveToPageBottom();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		existNote = screenPerformEditBuyerProfile.existsElement(HOME_NETWORK_NOTE);
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_CLARO_BRAZIL_CLR);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformView.waitLoadingOverlay();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnItemValue(HOME_NETWORK_VIEW);
		screenPerformViewValues.clickOnItemHistory(HOME_NETWORK_HISTORY);
		itemHistoryMsg = screenPerformViewValues.getTextFrom(ITEM_HISTORY_NOTE);
		
		assertFalse(existNote);
		assertEquals(NOTE_MESSAGE, itemHistoryMsg);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3246
	* @summary An unapproved item without a note related to change 
	*/
	@Test
	public void test_2307_AnUnapprovedItemWithoutNoteRelatedToChange() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		String noteMsg = "msg";
		boolean existNote = false;
		String itemHistoryMsg = "msg";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.addItem(ITEM_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.addNote(FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, EMPTY);
		existNote = screenPerformEditBuyerProfile.existsElement(HOME_NETWORK_NOTE);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, CRITICAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.checkStatusValidated(SUFFIX_CLR, FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, EMPTY);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.clickOnNote();
		noteMsg = screenPerformEditBuyerProfile.getValue(FIELD_NOTE);
		screenPerformEditBuyerProfile.clickOnCloseNote();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_CLARO_BRAZIL_CLR);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformView.waitLoadingOverlay();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnItemValue(HOME_NETWORK_VIEW);
		screenPerformViewValues.clickOnItemHistory(HOME_NETWORK_HISTORY);
		itemHistoryMsg = screenPerformViewValues.getTextFrom(ITEM_HISTORY_NOTE);
		
		assertTrue(existNote);
		assertEquals(EMPTY, noteMsg);
		assertEquals(EMPTY, itemHistoryMsg);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3247
	* @summary Item display for the user who did not make the change in item
	*/
	@Test
	public void test_2308_ItemDisplayForTheUserWhoDidNotMakeTheChangeInItem() throws Exception{
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		boolean existNote = false;
		boolean noteDisplayBlocked = false;
		String itemHistoryMsg = "";
		String requester = "";
		
		screenUsersManagement.openPage();
		screenUsersManagement.moveToPageBottom();
		screenUsersManagement.clickOnBlockedUsers();
		screenUsersManagement.searchUserBlocked(YARA_MARTINS);
		screenUsersManagement.checkUserOnListBlock(YARA_MARTINS);
		screenUsersManagement.searchUserBlocked(ANDRESSA_LEITE);
		screenUsersManagement.checkUserOnListBlock(ANDRESSA_LEITE);
		screenUsersManagement.clickOnUnblockSelectedUsers();
		screenUsersManagement.clickOnCloseBlockedUsers();
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.searchAndSelectUser(YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clicOnLoginAs();
		sectionPerformMenu.closeNewReleaseInfo();
		sectionPerformMenu.clickOnMenuBuyer();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.addItem(ITEM_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.addNote(FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE);
		existNote = screenPerformEditBuyerProfile.existsElement(HOME_NETWORK_NOTE);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, CRITICAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.checkStatusValidated(SUFFIX_CLR, FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE);
		sectionPerformMenu.clickOnMenuUserManagement();
		screenUsersManagement.searchAndSelectUser(ANDRESSA_LEITE);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clicOnLoginAs();
		if(screenPerformEditBuyerProfile.existsElement(WHATS_NEW)){
			sectionPerformMenu.closeNewReleaseNotes();
		}else{
			sectionPerformMenu.closeNewReleaseInfo();
		}
		sectionPerformMenu.clickOnMenuBuyer();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.clickOnNote();
		noteDisplayBlocked = screenPerformEditBuyerProfile.verifyIfElementDisplayIsBlocked(NOTE_DIALOG);
		screenPerformEditBuyerProfile.clickOnCancelNote();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_CLARO_BRAZIL_CLR);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformView.waitLoadingOverlay();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnItemValue(HOME_NETWORK_VIEW);
		screenPerformViewValues.clickOnItemHistory(HOME_NETWORK_HISTORY);
		itemHistoryMsg = screenPerformViewValues.getTextFrom(ITEM_HISTORY_NOTE);
		requester = screenPerformViewValues.getTextFrom(ITEM_HISTORY_REQUESTER);
		
		assertTrue(existNote);
		assertTrue(noteDisplayBlocked);
		assertEquals(NOTE_MESSAGE, itemHistoryMsg);
		assertEquals(YARA_MARTINS, requester);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3248
	* @summary Item approval be carried out by other LTM of buyer in question
	*/
	@Test
	public void test_2310_ItemApprovalBeCarriedOutByOtherLtmOfBuyerInQuestion() throws Exception{
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		boolean existNote = true;
		String itemHistoryMsg = "";
		String requester = "";
		String approver = "";
		
		screenUsersManagement.openPage();
		screenUsersManagement.moveToPageBottom();
		screenUsersManagement.clickOnBlockedUsers();
		screenUsersManagement.searchUserBlocked(YARA_MARTINS);
		screenUsersManagement.checkUserOnListBlock(YARA_MARTINS);
		screenUsersManagement.searchUserBlocked(ABDUL_ANSARI);
		screenUsersManagement.checkUserOnListBlock(ABDUL_ANSARI);
		screenUsersManagement.clickOnUnblockSelectedUsers();
		screenUsersManagement.clickOnCloseBlockedUsers();
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.searchAndSelectUser(YARA_MARTINS);
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clicOnLoginAs();
		sectionPerformMenu.closeNewReleaseInfo();
		sectionPerformMenu.clickOnMenuBuyer();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.addItem(ITEM_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.addNote(FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, CRITICAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.checkStatusValidated(SUFFIX_CLR, FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE);
		screenPerformListBuyers.clickOnStatusChangeItem(SUFFIX_CLR);
		sectionPerformMenu.clickOnMenuUserManagement();
		screenUsersManagement.searchAndSelectUser(ABDUL_ANSARI);
		boolean addSuccess = screenUsersManagement.addRegionOnItemReviewer(ConstantsBuyers.LATIN_AMERICA);
		if (!addSuccess) {
			screenUsersManagement.removeRegionOnItemReviewer(ConstantsBuyers.LATIN_AMERICA);
			screenUsersManagement.confirmRemoveRegion();
			screenUsersManagement.addRegionOnItemReviewer(ConstantsBuyers.LATIN_AMERICA);
		}
		screenUsersManagement.moveToPageBottom();
		addSuccess = screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_HOME_NETWORK);
		if (!addSuccess) {
			screenUsersManagement.removeItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_HOME_NETWORK);
			screenUsersManagement.confirmRemoveItem();
			screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_HOME_NETWORK);
		}
		screenUsersManagement.moveToPageHeader();
		screenUsersManagement.clicOnLoginAs();
		screenUsersManagement.waitLoading();
		if(screenPerformEditBuyerProfile.existsElement(WHATS_NEW)){
			sectionPerformMenu.closeNewReleaseNotes();
		}else{
			sectionPerformMenu.closeNewReleaseInfo();
		}
		sectionPerformMenu.clickOnMenuBuyer();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_CLR);
		screenPerformListBuyers.clickOnStatusPendingApproval(SUFFIX_CLR);
		screenPerformListBuyers.waitLoading();
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.moveToPageHeader();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.moveToPageBottom();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenUsersManagement.waitLoading();
		screenPerformBuyerApproval.clickOnConfirmSave();
		screenUsersManagement.waitLoading();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		existNote = screenPerformEditBuyerProfile.existsElement(HOME_NETWORK_NOTE);
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_CLARO_BRAZIL_CLR);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformView.waitLoadingOverlay();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnItemValue(HOME_NETWORK_VIEW);
		screenPerformViewValues.clickOnItemHistory(HOME_NETWORK_HISTORY);
		itemHistoryMsg = screenPerformViewValues.getTextFrom(ITEM_HISTORY_NOTE);
		requester = screenPerformViewValues.getTextFrom(ITEM_HISTORY_REQUESTER);
		approver = screenPerformViewValues.getTextFrom(ITEM_HISTORY_APPROVER);
		
		assertFalse(existNote);
		assertEquals(NOTE_MESSAGE, itemHistoryMsg);
		assertEquals(YARA_MARTINS, requester);
		assertEquals(ABDUL_ANSARI, approver);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3249
	* @summary Change the note of an item without approval
	*/
	@Test
	public void test_2312_ChangeTheNoteOfAnItemWithoutApproval() throws Exception{
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		String noteOne = "";
		String noteTwo = "";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.addItem(ITEM_HOME_NETWORK);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.addNote(FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE_1);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, CRITICAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.checkStatusValidated(SUFFIX_CLR, FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE_1);
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_CLARO_BRAZIL_CLR);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformView.waitLoadingOverlay();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnItemValue(HOME_NETWORK_VIEW);
		screenPerformViewValues.clickOnItemHistory(HOME_NETWORK_HISTORY);
		noteOne = screenPerformViewValues.getTextFrom(ITEM_HISTORY_NOTE);
		screenPerformViewValues.clickOnItemHistoryCloseButton();
		sectionPerformMenu.clickOnMenuBuyer();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.addNote(FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE_2);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, CRITICAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.checkStatusValidated(SUFFIX_CLR, FIELD_HOME_NETWORK, HOME_NETWORK_724_32, HOME_NETWORK_724_99, HOME_NETWORK_NOTE, NOTE_MESSAGE_2);
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_CLARO_BRAZIL_CLR);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformView.waitLoadingOverlay();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnItemValue(HOME_NETWORK_VIEW);
		screenPerformViewValues.clickOnItemHistory(HOME_NETWORK_HISTORY);
		noteTwo = screenPerformViewValues.getTextFrom(ITEM_HISTORY_NOTE);
		
		assertEquals(NOTE_MESSAGE_1, noteOne);
		assertEquals(NOTE_MESSAGE_2, noteTwo);
	}
}