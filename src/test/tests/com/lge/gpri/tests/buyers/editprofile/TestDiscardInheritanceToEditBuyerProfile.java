package com.lge.gpri.tests.buyers.editprofile;

import static com.lge.gpri.helpers.constants.ConstantsBuyers.*;
import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Discard Inheritance to Edit Buyer Profile.
*  
* @author Cayk Lima Barreto
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestDiscardInheritanceToEditBuyerProfile extends AbstractTestScenary{
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3215
	* @summary Auto Commit 
	*/
	@Test
	public void test_2268_AutoCommit() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultYes = false;
		boolean resultNo = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_AUTO_COMMIT);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_AUTO_COMMIT);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
		resultYes = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
		resultNo = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_NO);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(resultYes);
		assertTrue(resultNo);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3216
	* @summary Default Hide on Mother 
	*/
	@Test
	public void test_2269_DefaultHideOnMother() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultYes = false;
		boolean resultNo = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER);
		resultYes = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER);
		resultNo = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER, FILTER_OPTION_NO);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(resultYes);
		assertTrue(resultNo);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3217
	* @summary Mandatory 
	*/
	@Test
	public void test_2270_Mandatory() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultYes = false;
		boolean resultNo = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_MANDATORY);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_MANDATORY);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY);
		resultYes = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY);
		resultNo = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY, FILTER_OPTION_NO);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(resultYes);
		assertTrue(resultNo);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3218
	* @summary Mandatory for Buyer Child 
	*/
	@Test
	public void test_2271_MandatoryForBuyerChild() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultYes = false;
		boolean resultNo = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD);
		resultYes = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY_FOR_BUYER_CHILD, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD);
		resultNo = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY_FOR_BUYER_CHILD, FILTER_OPTION_NO);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(resultYes);
		assertTrue(resultNo);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3219
	* @summary Multiple 
	*/
	@Test
	public void test_2272_Multiple() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultYes = false;
		boolean resultNo = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_MULTIPLE);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_MULTIPLE);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MULTIPLE);
		resultYes = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MULTIPLE, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MULTIPLE);
		resultNo = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MULTIPLE, FILTER_OPTION_NO);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(resultYes);
		assertTrue(resultNo);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3220
	* @summary Name 
	*/
	@Test
	public void test_2273_Name() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean result = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_NAME);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_NAME);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.discardInheritanceSetTextOnFilterName(FILTER_NAME);
		result = screenPerformEditBuyerProfile.existsElement(ITEM_NAME);
		
		assertTrue(result);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3221
	* @summary SmartCA Type 
	*/
	@Test
	public void test_2274_SmartCAType() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean result = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_SMART_CA_TYPES);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_SMART_CA_TYPES);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSmartCaDropDownInDiscardInheritance();
		result = screenPerformEditBuyerProfile.clickOnSelectedOptionForSmartCaTypeFilterInDiscardInheritance(FILTER_OPTION_CUSTOMIZER);
		screenPerformEditBuyerProfile.waitLoading();
		screenPerformEditBuyerProfile.clickOnSmartCaDropDownInDiscardInheritance();
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(result);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3222
	* @summary Tag Types 
	*/
	@Test
	public void test_2275_TagTypes() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean result = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_TAG_TYPES);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_TAG_TYPES);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_TAG_TYPES);
		result = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_TAG_TYPES, FILTER_OPTION_ANDROID_VERSION);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(result);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3223
	* @summary Tag Values 
	*/
	@Test
	public void test_2276_TagValues() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean result = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_TAG_VALUES);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_TAG_VALUES);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_TAG_VALUES);
		result = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_TAG_VALUES, FILTER_OPTION_ANDROID_VERSION_4_4_0);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(result);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3224
	* @summary Type 
	*/
	@Test
	public void test_2277_Type() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean result = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_TYPE);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_TYPE);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_TYPE);
		result = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_TYPE, FILTER_OPTION_RESOURCE);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(result);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3225
	* @summary Warnings 
	*/
	@Test
	public void test_2278_Warnings() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean result = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_WARNINGS);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_WARNINGS);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_WARNING);
		result = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_WARNING, FILTER_OPTION_COTA);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(result);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3226
	* @summary Has Links 
	*/
	@Test
	public void test_2279_HasLinks() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultYes = false;
		boolean resultNo = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_HAS_LINKS);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_HAS_LINKS);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_HAS_LINKS);
		resultYes = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_HAS_LINKS, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_HAS_LINKS);
		resultNo = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_HAS_LINKS, FILTER_OPTION_NO);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(resultYes);
		assertTrue(resultNo);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3227
	* @summary Auto Commit and Default Hide on Mother
	*/
	@Test
	public void test_2280_AutoCommitAndDefaultHideOnMother() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultAutoCommitYes = false;
		boolean resultDefaultHideOnMotherYes = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_AUTO_COMMIT);
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
		resultAutoCommitYes = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER);
		resultDefaultHideOnMotherYes = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		
		
		assertTrue(resultAutoCommitYes);
		assertTrue(resultDefaultHideOnMotherYes);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3228
	* @summary Mandatory and Mandatory for Buyer Child
	*/
	@Test
	public void test_2281_MandatoryAndMandatoryForBuyerChild() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultMandatoryYes = false;
		boolean resultMandatoryForBuyerChildNo = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_MANDATORY);
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY);
		resultMandatoryYes = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD);
		resultMandatoryForBuyerChildNo = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY_FOR_BUYER_CHILD, FILTER_OPTION_NO);
		screenPerformEditBuyerProfile.waitLoading();
		
		
		assertTrue(resultMandatoryYes);
		assertTrue(resultMandatoryForBuyerChildNo);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3229
	* @summary All Filters
	*/
	@Test
	public void test_2282_AllFilters() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultSelectAllFilters = false;
		boolean resultDeselectAllFilters = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		resultSelectAllFilters = screenPerformEditBuyerProfile.discardInheritanceSelectAllFilters();
		screenPerformEditBuyerProfile.waitLoading();
		resultDeselectAllFilters = screenPerformEditBuyerProfile.discardInheritanceDeselectAllFilters();
		screenPerformEditBuyerProfile.waitLoading();
		
		
		assertTrue(resultSelectAllFilters);
		assertTrue(resultDeselectAllFilters);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3230
	* @summary Remove filter
	*/
	@Test
	public void test_2283_RemoveFilter() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultAutoCommitYes = false;
		boolean resultRemoveFilter = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_AUTO_COMMIT);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedFilter(FILTER_AUTO_COMMIT);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
		resultAutoCommitYes = screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		resultRemoveFilter = screenPerformEditBuyerProfile.discardInheritanceRemoveFilter(CRITERIA_AUTO_COMMIT);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(resultAutoCommitYes);
		assertTrue(resultRemoveFilter);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3231
	* @summary Search on filter
	*/
	@Test
	public void test_2284_SearchOnFilter() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		String resultAuCommit = "";
		String resultAuCommit2 = "";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceOptionFilters();
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_AUTO_COMMIT);
		resultAuCommit = screenPerformEditBuyerProfile.getText(DISCARD_INHERITANCE_TXT_RESULT_MATCHED);
		screenPerformEditBuyerProfile.dicardInheritanceSearchItemLibrary(FILTER_AUTO_COMMIT_2);
		resultAuCommit2 = screenPerformEditBuyerProfile.getText(DISCARD_INHERITANCE_TXT_NO_RESULT_MATCHED);
		
		assertEquals(MESSAGE_MATCHED_AUTO_COMMIT, resultAuCommit);
		assertEquals(MESSAGE_NO_RESULT_MATCHED_AUTO_COMMIT_2, resultAuCommit2);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3232
	* @summary Enable one item for all children
	*/
	@Test
	public void test_2286_EnableOneItemForAllChildren() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		boolean checked = false;
		boolean alwaysAvailable = true;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_O2U);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2U, ITEM_ALWAYS_AVAILABLE, FIELD_ALWAYS_AVAILABLE, VALUE_19);
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(ALL_CHILDREN);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(ALL_CHILDREN);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.addItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		checked = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceCloseButton();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_TLF_GROUP_EUROPE);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		alwaysAvailable = screenPerformViewValues.existsElement(ALWAYS_AVAILABLE);
		
		assertTrue(checked);
		assertFalse(alwaysAvailable);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3233
	* @summary Disable one item for all children
	*/
	@Test
	public void test_2287_DisableOneItemForAllChildren() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		boolean checked = true;
		boolean alwaysAvailable = false;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_O2U);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2U, ITEM_ALWAYS_AVAILABLE, FIELD_ALWAYS_AVAILABLE, VALUE_19);
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(ALL_CHILDREN);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(ALL_CHILDREN);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		checked = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceCloseButton();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_TLF_GROUP_EUROPE);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		alwaysAvailable = screenPerformViewValues.existsElement(ALWAYS_AVAILABLE);
		
		assertFalse(checked);
		assertTrue(alwaysAvailable);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3234
	* @summary Enable more than one item for all children
	*/
	@Test
	public void test_2288_EnableMoreThanOneItemForAllChildren() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		boolean checkedAlwaysAvailable = false;
		boolean checkedAdditionalNumber = false;
		boolean alwaysAvailable = true;
		boolean additionalNumber = true;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_O2U);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2U, ITEM_ALWAYS_AVAILABLE, FIELD_ALWAYS_AVAILABLE, VALUE_19);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2U, ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD, FIELD_ADDITIONAL_NUMBER_WITH_SIM_CARD, VALUE_123);
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(ALL_CHILDREN);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(ALL_CHILDREN);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.addItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.addItem(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		checkedAlwaysAvailable = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ALWAYS_AVAILABLE);
		checkedAdditionalNumber = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceCloseButton();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_TLF_GROUP_EUROPE);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		alwaysAvailable = screenPerformViewValues.existsElement(ALWAYS_AVAILABLE);
		additionalNumber = screenPerformViewValues.existsElement(ADDITIONAL_NUMBER_WITH_SIM_CARD);
		
		assertTrue(checkedAlwaysAvailable);
		assertTrue(checkedAdditionalNumber);
		assertFalse(alwaysAvailable);
		assertFalse(additionalNumber);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3235
	* @summary Disable more than one item for all children
	*/
	@Test
	public void test_2289_DisableMoreThanOneItemForAllChildren() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		boolean checkedAlwaysAvailable = true;
		boolean checkedAdditionalNumber = true;
		boolean alwaysAvailable = false;
		boolean additionalNumber = false;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_O2U);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2U, ITEM_ALWAYS_AVAILABLE, FIELD_ALWAYS_AVAILABLE, VALUE_19);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2U, ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD, FIELD_ADDITIONAL_NUMBER_WITH_SIM_CARD, VALUE_123);
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(ALL_CHILDREN);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(ALL_CHILDREN);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.removeItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(ALL_CHILDREN);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(ALL_CHILDREN);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		checkedAlwaysAvailable = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ALWAYS_AVAILABLE);
		checkedAdditionalNumber = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceCloseButton();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_TLF_GROUP_EUROPE);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		alwaysAvailable = screenPerformViewValues.existsElement(ALWAYS_AVAILABLE);
		additionalNumber = screenPerformViewValues.existsElement(ADDITIONAL_NUMBER_WITH_SIM_CARD);
		
		assertFalse(checkedAdditionalNumber);
		assertFalse(checkedAlwaysAvailable);
		assertTrue(alwaysAvailable);
		assertTrue(additionalNumber);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3236
	* @summary Enable one item for one buyer child
	*/
	@Test
	public void test_2290_EnableOneItemForOneBuyerChild() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		boolean checkedAlwaysAvailable = false;
		boolean alwaysAvailable = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.addItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		checkedAlwaysAvailable = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceCloseButton();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_TLF_GROUP_EUROPE);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		alwaysAvailable = screenPerformViewValues.existsElement(UNITED_KINGDOM_ALWAYS_AVAILABLE);
		
		assertTrue(checkedAlwaysAvailable);
		assertFalse(alwaysAvailable);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3237
	* @summary Disable one item for one buyer child
	*/
	@Test
	public void test_2291_DisableOneItemForOneBuyerChild() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		boolean checkedAlwaysAvailable = true;
		boolean childAlwaysAvailable = false;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_O2U);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2U, ITEM_ALWAYS_AVAILABLE, FIELD_ALWAYS_AVAILABLE, VALUE_19);
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		checkedAlwaysAvailable = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceCloseButton();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_TLF_GROUP_EUROPE);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		childAlwaysAvailable = screenPerformViewValues.existsElement(UNITED_KINGDOM_ALWAYS_AVAILABLE);
		
		assertFalse(checkedAlwaysAvailable);
		assertTrue(childAlwaysAvailable);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3238
	* @summary Enable more than one item for one buyer child
	*/
	@Test
	public void test_2292_EnableMoreThanOneItemForOneBuyerChild() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		boolean checkedAlwaysAvailable = false;
		boolean checkedAdditionalNumber = false;
		boolean childAlwaysAvailable = true;
		boolean childAdditionalNumber = true;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_O2U);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2U, ITEM_ALWAYS_AVAILABLE, FIELD_ALWAYS_AVAILABLE, VALUE_19);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2U, ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD, FIELD_ADDITIONAL_NUMBER_WITH_SIM_CARD, VALUE_123);
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.addItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.addItem(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		checkedAlwaysAvailable = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ALWAYS_AVAILABLE);
		checkedAdditionalNumber = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceCloseButton();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_TLF_GROUP_EUROPE);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		childAlwaysAvailable = screenPerformViewValues.existsElement(UNITED_KINGDOM_ALWAYS_AVAILABLE);
		childAdditionalNumber = screenPerformViewValues.existsElement(UNITED_KINGDOM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		
		assertTrue(checkedAlwaysAvailable);
		assertTrue(checkedAdditionalNumber);
		assertFalse(childAlwaysAvailable);
		assertFalse(childAdditionalNumber);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3239
	* @summary Disable more than one item for one buyer child
	*/
	@Test
	public void test_2293_DisableMoreThanOneItemForOneBuyerChild() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		boolean checkedAlwaysAvailable = true;
		boolean checkedAdditionalNumber = true;
		boolean childAlwaysAvailable = false;
		boolean childAdditionalNumber = false;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_O2U);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2U, ITEM_ALWAYS_AVAILABLE, FIELD_ALWAYS_AVAILABLE, VALUE_19);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2U, ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD, FIELD_ADDITIONAL_NUMBER_WITH_SIM_CARD, VALUE_123);
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		checkedAlwaysAvailable = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ALWAYS_AVAILABLE);
		checkedAdditionalNumber = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceCloseButton();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_TLF_GROUP_EUROPE);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		childAlwaysAvailable = screenPerformViewValues.existsElement(UNITED_KINGDOM_ALWAYS_AVAILABLE);
		childAdditionalNumber = screenPerformViewValues.existsElement(UNITED_KINGDOM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		
		assertFalse(checkedAlwaysAvailable);
		assertFalse(checkedAdditionalNumber);
		assertTrue(childAlwaysAvailable);
		assertTrue(childAdditionalNumber);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3240
	* @summary Enable more than one item for one buyer child and with another buyer daughter on the view
	*/
	@Test
	public void test_2294_EnableMoreThanOneItemForOneBuyerChildAndWithAnotherBuyerDaughterOnTheView() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		boolean checkedAlwaysAvailable = false;
		boolean checkedAdditionalNumber = false;
		boolean childAlwaysAvailable = false;
		boolean childAdditionalNumber = false;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_O2D);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2D, ITEM_ALWAYS_AVAILABLE, FIELD_ALWAYS_AVAILABLE, VALUE_19);
		screenPerformEditBuyerProfile.verifyChild(SUFFIX_O2D, ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD, FIELD_ADDITIONAL_NUMBER_WITH_SIM_CARD, VALUE_123);
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.removeItem(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.addItem(ITEM_ALWAYS_AVAILABLE);
		screenPerformEditBuyerProfile.addItem(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOkButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceOptionChildren();
		screenPerformEditBuyerProfile.setFildChildren(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceSelectedChild(BUYER_02_UNITED_KINGDOM_O2U);
		screenPerformEditBuyerProfile.moveTo(ITEM_ALWAYS_AVAILABLE);
		checkedAlwaysAvailable = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ALWAYS_AVAILABLE);
		checkedAdditionalNumber = screenPerformEditBuyerProfile.boxIsChecked(ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceCloseButton();
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_GERMANY_O2D);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.selectAllAutoProfiles();
		screenPerformViewValues.clickOnViewPackageButton();
		childAlwaysAvailable = screenPerformViewValues.existsElement(GERMANY_ALWAYS_AVAILABLE);
		childAdditionalNumber = screenPerformViewValues.existsElement(GERMANY_ADDITIONAL_NUMBER_WITH_SIM_CARD);
		
		assertTrue(checkedAlwaysAvailable);
		assertTrue(checkedAdditionalNumber);
		assertTrue(childAlwaysAvailable);
		assertTrue(childAdditionalNumber);
	}
	
	/**
	* @throws Exception 
	* @testLinkId T-GPRI-3241
	* @summary Enable show only checked items
	*/
	@Test
	public void test_2295_EnableShowOnlyCheckedItems() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean onlyCheckedItems = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		onlyCheckedItems = screenPerformEditBuyerProfile.clickOnDiscardInheritanceOnlyCheckedItemsButton();
		
		assertTrue(onlyCheckedItems);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3242
	* @summary Search Valid
	*/
	@Test
	public void test_2296_SearchValid() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean highlightedItem = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceSearchItems(NAME);
		highlightedItem = screenPerformEditBuyerProfile.checkItemClass(ITEM_NAME, ITEM_HIGHLIGHTED);
		
		assertTrue(highlightedItem);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3243
	* @summary Search Invalid
	*/
	@Test
	public void test_2297_SearchInvalid() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean itemNotHighlighted = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_6TL);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_6TL);
		screenPerformEditBuyerProfile.moveToPageHeader();
		screenPerformEditBuyerProfile.clickOnDiscardInheritanceButton();
		screenPerformEditBuyerProfile.discardInheritanceSearchItems(NAMI);
		itemNotHighlighted = screenPerformEditBuyerProfile.checkItemClass(ITEM_NAME, ITEM_HIGHLIGHTED);
		
		assertFalse(itemNotHighlighted);
	}
}

