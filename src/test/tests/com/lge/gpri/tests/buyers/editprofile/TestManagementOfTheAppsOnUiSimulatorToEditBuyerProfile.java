package com.lge.gpri.tests.buyers.editprofile;

import static com.lge.gpri.helpers.constants.ConstantsBuyers.*;
import static com.lge.gpri.helpers.constants.ConstantsView.APP_INSTAGRAM_VIEW;
import static com.lge.gpri.helpers.constants.ConstantsView.APP_FACEBOOK_VIEW;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.view.ScreenPerformView;
import com.lge.gpri.screens.view.ScreenPerformViewValues;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Management of the Apps on UI Simulator to Edit Buyer Profile.
*  
* @author Cayk Lima Barreto
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestManagementOfTheAppsOnUiSimulatorToEditBuyerProfile extends AbstractTestScenary{

	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3286
	* @summary Add apps in different positions from buyer
	*/
	@Test
	public void test_2315_AddAppsInDifferentPositionsFromBuyer() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		boolean resultApp1OnEditProfile = false;
		boolean resultApp2OnEditProfile = false;
		boolean resultApp1OnView = false;
		boolean resultApp2OnView = false;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.clickOnEditorModeButton();
		screenPerformEditBuyerProfile.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformEditBuyerProfile.clickOnLockUnlockButton();
		screenPerformEditBuyerProfile.clickAppPositionOneButton();
		screenPerformEditBuyerProfile.addApp(INSTAGRAM);
		screenPerformEditBuyerProfile.clickOnSaveAppsButton();
		screenPerformEditBuyerProfile.clickOnEditorModeButton();
		screenPerformEditBuyerProfile.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformEditBuyerProfile.clickOnLockUnlockButton();
		screenPerformEditBuyerProfile.clickAppPositionTwoButton();
		screenPerformEditBuyerProfile.addApp(FACEBOOK);
		screenPerformEditBuyerProfile.clickOnSaveAppsButton();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformEditBuyerProfile.clickOnLockUnlockButton();
		resultApp1OnEditProfile = screenPerformEditBuyerProfile.existsElement(APP_INSTAGRAM);
		resultApp2OnEditProfile = screenPerformEditBuyerProfile.existsElement(APP_FACEBOOK);
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_CLARO_BRAZIL_CLR);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnCustomizationButton();
		resultApp1OnView = screenPerformViewValues.checkApp1GridValues();
		resultApp2OnView = screenPerformViewValues.checkApp2GridValues();
		
		assertTrue(resultApp1OnEditProfile);
		assertTrue(resultApp2OnEditProfile);
		assertTrue(resultApp1OnView);
		assertTrue(resultApp2OnView);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3288
	* @summary Add apps in same position from buyer
	*/
	@Test
	public void test_2319_AddAppsInSamePositionFromBuyer() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		boolean resultApp1OnEditProfile = false;
		boolean resultApp2OnEditProfile = false;
		boolean resultCollision = false;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.clickOnEditorModeButton();
		screenPerformEditBuyerProfile.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformEditBuyerProfile.clickOnLockUnlockButton();
		screenPerformEditBuyerProfile.clickAppPositionOneButton();
		screenPerformEditBuyerProfile.addApp(INSTAGRAM);
		screenPerformEditBuyerProfile.clickOnSaveAppsButton();
		screenPerformEditBuyerProfile.clickOnEditorModeButton();
		screenPerformEditBuyerProfile.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformEditBuyerProfile.clickOnLockUnlockButton();
		screenPerformEditBuyerProfile.clickAppPositionTwoButton();
		screenPerformEditBuyerProfile.addApp(FACEBOOK);
		screenPerformEditBuyerProfile.clickOnSaveAppsButton();
		screenPerformEditBuyerProfile.setAppGridRow(APP_FACEBOOK_ROW, VALUE_1);
		screenPerformEditBuyerProfile.setAppGridColumn(APP_FACEBOOK_COLUMN, VALUE_1);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformEditBuyerProfile.clickOnLockUnlockButton();
		resultApp1OnEditProfile = screenPerformEditBuyerProfile.existsElement(APP_INSTAGRAM);
		resultApp2OnEditProfile = screenPerformEditBuyerProfile.existsElement(APP_FACEBOOK);
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_CLARO_BRAZIL_CLR);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformViewValues.clickOnLockUnlockButton();
		resultCollision = screenPerformViewValues.checkCollisionBetweenApps(APP_INSTAGRAM_VIEW, APP_FACEBOOK_VIEW);
		
		assertTrue(resultApp1OnEditProfile);
		assertTrue(resultApp2OnEditProfile);
		assertTrue(resultCollision);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3289
	* @summary Add apps without position from buyer
	*/
	@Test
	public void test_2320_AddAppsWithoutPositionFromBuyer() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		String warningMsg = "";
		String fieldEmptyMsg = "";
		boolean warning = false;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.clickOnEditorModeButton();
		screenPerformEditBuyerProfile.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformEditBuyerProfile.clickOnLockUnlockButton();
		screenPerformEditBuyerProfile.clickAppPositionOneButton();
		screenPerformEditBuyerProfile.addApp(INSTAGRAM);
		screenPerformEditBuyerProfile.clickOnSaveAppsButton();
		screenPerformEditBuyerProfile.clickOnCustomizationButton();
		screenPerformEditBuyerProfile.setAppGridRow(APP_INSTAGRAM_ROW, EMPTY);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		warningMsg = screenPerformEditBuyerProfile.getTextFrom(WARNING_CANNOT_SAVE_BUYER);
		warning = screenPerformEditBuyerProfile.clickOnCannotSaveBuyerOkButton();
		fieldEmptyMsg = screenPerformEditBuyerProfile.getTextFrom(APP_INSTAGRAM_ROW_EMPTY);
		
		assertTrue(warning);
		assertEquals(MSG_WARNING_CANNOT_SAVE_BUYER, warningMsg);
		assertEquals(MSG_WARNING_FIELD_EMPTY, fieldEmptyMsg);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3287
	* @summary Edit positions of the apps to not overlap from buyer
	*/
	@Test
	public void test_2317_EditPositionsOfTheAppsToNotOverpalFromBuyer() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());
		ScreenPerformView screenPerformView = new ScreenPerformView(getDriver());
		ScreenPerformViewValues screenPerformViewValues = new ScreenPerformViewValues(getDriver());
		boolean collision = false;
		boolean collisionAfterEdit = true;
		boolean collisionOnViewPage = true;
		
		screenPerformListBuyers.openPage();
		screenPerformEditBuyerProfile.onlyOneItemOnLibrary(SUFFIX_CLR);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.clickOnEditorModeButton();
		screenPerformEditBuyerProfile.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformEditBuyerProfile.clickOnLockUnlockButton();
		screenPerformEditBuyerProfile.clickAppPositionOneButton();
		screenPerformEditBuyerProfile.addApp(INSTAGRAM);
		screenPerformEditBuyerProfile.clickOnSaveAppsButton();
		screenPerformEditBuyerProfile.clickOnEditorModeButton();
		screenPerformEditBuyerProfile.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformEditBuyerProfile.clickOnLockUnlockButton();
		screenPerformEditBuyerProfile.clickAppPositionTwoButton();
		screenPerformEditBuyerProfile.addApp(FACEBOOK);
		screenPerformEditBuyerProfile.clickOnSaveAppsButton();
		screenPerformEditBuyerProfile.setAppGridRow(APP_FACEBOOK_ROW, VALUE_1);
		screenPerformEditBuyerProfile.setAppGridColumn(APP_FACEBOOK_COLUMN, VALUE_1);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformEditBuyerProfile.clickOnLockUnlockButton();
		collision = screenPerformEditBuyerProfile.checkCollisionBetweenApps(APP_INSTAGRAM, APP_FACEBOOK);
		screenPerformEditBuyerProfile.clickOnCustomizationButton();
		screenPerformEditBuyerProfile.setAppGridRow(APP_FACEBOOK_ROW, VALUE_2);
		screenPerformEditBuyerProfile.setAppGridColumn(APP_FACEBOOK_COLUMN, VALUE_1);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_CLR);
		screenPerformEditBuyerProfile.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformEditBuyerProfile.clickOnLockUnlockButton();
		collisionAfterEdit = screenPerformEditBuyerProfile.checkCollisionBetweenApps(APP_INSTAGRAM, APP_FACEBOOK);
		sectionPerformMenu.clickOnMenuView();
		screenPerformView.addSearch(BUYER_CLARO_BRAZIL_CLR);
		screenPerformView.selectFirstItemResultSearch();
		screenPerformViewValues.moveToPageHeader();
		screenPerformViewValues.clickOnRevisionButton();
		screenPerformViewValues.clickOnLastRevisionDropdown();
		screenPerformViewValues.clickOnViewPackageButton();
		screenPerformViewValues.clickOnUiStartButton();
		Thread.sleep(2000);
		screenPerformViewValues.clickOnLockUnlockButton();
		collisionOnViewPage = screenPerformViewValues.checkCollisionBetweenApps(APP_INSTAGRAM_VIEW, APP_FACEBOOK_VIEW);
		
		assertTrue(collision);
		assertFalse(collisionAfterEdit);
		assertFalse(collisionOnViewPage);
	}
}