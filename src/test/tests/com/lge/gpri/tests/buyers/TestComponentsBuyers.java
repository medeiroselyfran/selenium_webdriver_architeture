package com.lge.gpri.tests.buyers;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.buyers.ScreenPerformBuyer;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Components Buyers.
*  
* @author Gabriel Costa do Nascimento
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestComponentsBuyers extends AbstractTestScenary{

	/**
	* @testLinkId T-GPRI-3259
	* @summary Remove buyer
	*/
	@Test
	public void test_3259_RemoveBuyer() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_3IR);
		boolean notExists = screenPerformListBuyers.noBuyersAvailable();
		if(notExists){
			screenPerformListBuyers.clickOnShowDeletedBuyersButton();
			screenPerformListBuyers.clickOnRestoreBuyer(ConstantsBuyers.SUFFIX_3IR);
			screenPerformListBuyers.confirmRestoreBuyer();
		}
		screenPerformListBuyers.clickOnShowDeletedBuyersButton();
		screenPerformListBuyers.clickOnRemoveBuyer(ConstantsBuyers.SUFFIX_3IR);
		screenPerformListBuyers.confirmRemoveBuyer();
		
		notExists = screenPerformListBuyers.noBuyersAvailable();
		assertTrue(notExists);
	}
	
	/**
	* @testLinkId T-GPRI-3261
	* @summary Show Deleted Buyers
	*/
	@Test
	public void test_3261_ShowDeletedBuyers() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		
		test_3259_RemoveBuyer();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_3IR);
		boolean notExists1 = screenPerformListBuyers.noBuyersAvailable();
		screenPerformListBuyers.clickOnShowDeletedBuyersButton();
		boolean notExists2 = screenPerformListBuyers.noBuyersAvailable();
		if(notExists2){
			screenPerformListBuyers.clickOnShowHiddenBuyersButton();
			screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			screenPerformBuyer.uncheckOnHideOnView();
			boolean sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);;
			screenPerformListBuyers.clickOnShowHiddenBuyersButton();
		}
		notExists2 = screenPerformListBuyers.noBuyersAvailable();
		
		assertTrue(notExists1);
		assertFalse(notExists2);
	}
	
	/**
	* @testLinkId T-GPRI-3262
	* @summary Show Hidden Buyers
	*/
	@Test
	public void test_3262_ShowHiddenBuyers() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_3IR);
		boolean notExists = screenPerformListBuyers.noBuyersAvailable();
		if(notExists){
			//deleted or hide
			screenPerformListBuyers.clickOnShowDeletedBuyersButton();
			notExists = screenPerformListBuyers.noBuyersAvailable();
			if(notExists){
				//is hide, but is deleted?
				screenPerformListBuyers.clickOnShowHiddenBuyersButton();
				screenPerformListBuyers.clickOnShowDeletedBuyersButton();
				notExists = screenPerformListBuyers.noBuyersAvailable();
				if(notExists){
					//is delete too!
					screenPerformListBuyers.clickOnShowDeletedBuyersButton();
					screenPerformListBuyers.clickOnRestoreBuyer(ConstantsBuyers.SUFFIX_3IR);
					screenPerformListBuyers.confirmRestoreBuyer();
					screenPerformListBuyers.clickOnShowDeletedBuyersButton();
				}
				screenPerformListBuyers.clickOnShowHiddenBuyersButton();
			}else{
				//is deleted
				screenPerformListBuyers.clickOnRestoreBuyer(ConstantsBuyers.SUFFIX_3IR);
				screenPerformListBuyers.confirmRestoreBuyer();
				screenPerformListBuyers.clickOnShowDeletedBuyersButton();
			}
		}
		notExists = screenPerformListBuyers.noBuyersAvailable();
		if(!notExists){
			//is not hide
			screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_3IR);
			screenPerformBuyer.checkOnHideOnView();
			boolean sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);;
		}
		boolean notExists1 = screenPerformListBuyers.noBuyersAvailable();
		screenPerformListBuyers.clickOnShowHiddenBuyersButton();
		boolean notExists2 = screenPerformListBuyers.noBuyersAvailable();
		
		assertTrue(notExists1);
		assertFalse(notExists2);
	}
	
	/**
	* @testLinkId T-GPRI-3263
	* @summary Show only My Buyers
	*/
	@Test
	public void test_3263_OnlyMyBuyers() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_LGU);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_LGU);
		screenPerformBuyer.addLTM(ConstantsBuyers.ZUNSU_KIM);
		boolean sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_KOR);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_KOR);
		screenPerformBuyer.addLTM(ConstantsBuyers.ZUNSU_KIM);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);;
		
		screenPerformListBuyers.getDriver().get(ConstantsGeneric.LINK_LOGIN_ZUNSU_KIM);
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.removeSuffixFilter();
		screenPerformListBuyers.clickOnShowOnlyMyBuyersButton();
		ArrayList<String> buyers = screenPerformListBuyers.getBuyersInPage();
		boolean contains1 = buyers.contains(ConstantsBuyers.SUFFIX_KOR);
		boolean contains2 = buyers.contains(ConstantsBuyers.SUFFIX_LGU);
		
		assertTrue(contains1);
		assertTrue(contains2);
	}
	
	/**
	* @testLinkId T-GPRI-3264
	* @summary Filter with all filter selected
	*/
	@Test
	public void test_3264_FilterWithAllFilterSelected() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		String region = ConstantsBuyers.EUROPE;
		String suffix = ConstantsBuyers.SUFFIX_3IR;
		String country = ConstantsBuyers.IRELAND;
		String operator = ConstantsBuyers.OPERATOR_3;
		String type = ConstantsBuyers.CHILD;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.selectAllBuyerFilter();
		screenPerformListBuyers.setRegionFilter(region);
		screenPerformListBuyers.setSuffixFilter(suffix);
		screenPerformListBuyers.setCountryFilter(country);
		screenPerformListBuyers.setOperatorFilter(operator);
		screenPerformListBuyers.setTypeFilter(type);
		screenPerformListBuyers.clickOnShowHiddenBuyersButton();
		screenPerformListBuyers.clickOnShowDeletedBuyersButton();
		
		HashMap<String,ArrayList<String>> buyers = screenPerformListBuyers.getBuyerLinesResultFilter();
		for(Entry<String, ArrayList<String>> buyer: buyers.entrySet()){
			String regionItem = buyer.getValue().get(0);
			String suffixItem = buyer.getValue().get(1);
			String countryItem = buyer.getValue().get(2);
			String operatorItem = buyer.getValue().get(3);
			String typeItem = buyer.getValue().get(4);
			if(!regionItem.contains(region) || !suffixItem.contains(suffix) || !countryItem.contains(country) || !operatorItem.contains(operator) || !typeItem.contains(type)){
				resultOk=false;
				break;
			}
		}
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3265
	* @summary Filter with only Region, Suffix and Country
	*/
	@Test
	public void test_3265_FilterWithOnlyRegionSuffixAndCountry() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		String region = ConstantsBuyers.EUROPE;
		String suffix = ConstantsBuyers.SUFFIX_3IR;
		String country = ConstantsBuyers.IRELAND;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.selectAllBuyerFilter();
		screenPerformListBuyers.setRegionFilter(region);
		screenPerformListBuyers.setSuffixFilter(suffix);
		screenPerformListBuyers.setCountryFilter(country);
		screenPerformListBuyers.clickOnShowHiddenBuyersButton();
		screenPerformListBuyers.clickOnShowDeletedBuyersButton();
		
		HashMap<String,ArrayList<String>> buyers = screenPerformListBuyers.getBuyerLinesResultFilter();
		for(Entry<String, ArrayList<String>> buyer: buyers.entrySet()){
			String regionItem = buyer.getValue().get(0);
			String suffixItem = buyer.getValue().get(1);
			String countryItem = buyer.getValue().get(2);
			if(!regionItem.contains(region) || !suffixItem.contains(suffix) || !countryItem.contains(country)){
				resultOk=false;
				break;
			}
		}
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3266
	* @summary Filter only Country, Operator and Status
	*/
	@Test
	public void test_3266_FilterOnlyCountryOperatorAndStatus() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		String operator = ConstantsBuyers.OPERATOR_3;
		String type = ConstantsBuyers.CHILD;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.selectAllBuyerFilter();
		screenPerformListBuyers.setOperatorFilter(operator);
		screenPerformListBuyers.setTypeFilter(type);
		screenPerformListBuyers.clickOnShowHiddenBuyersButton();
		screenPerformListBuyers.clickOnShowDeletedBuyersButton();
		
		HashMap<String,ArrayList<String>> buyers = screenPerformListBuyers.getBuyerLinesResultFilter();
		for(Entry<String, ArrayList<String>> buyer: buyers.entrySet()){
			String operatorItem = buyer.getValue().get(3);
			String typeItem = buyer.getValue().get(4);
			if(!operatorItem.contains(operator) || !typeItem.contains(type)){
				resultOk=false;
				break;
			}
		}
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3267
	* @summary Filter only Suffix and Status
	*/
	@Test
	public void test_3267_FilterOnlySuffixAndStatus() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		String suffix = ConstantsBuyers.SUFFIX_3IR;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.selectAllBuyerFilter();
		screenPerformListBuyers.setSuffixFilter(suffix);
		screenPerformListBuyers.clickOnShowHiddenBuyersButton();
		screenPerformListBuyers.clickOnShowDeletedBuyersButton();
		
		HashMap<String,ArrayList<String>> buyers = screenPerformListBuyers.getBuyerLinesResultFilter();
		for(Entry<String, ArrayList<String>> buyer: buyers.entrySet()){
			String suffixItem = buyer.getValue().get(1);
			if(!suffixItem.contains(suffix)){
				resultOk=false;
				break;
			}
		}
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3268
	* @summary Filter only Suffix
	*/
	@Test
	public void test_3268_FilterOnlySuffix() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		String suffix = ConstantsBuyers.SUFFIX_3IR;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.selectAllBuyerFilter();
		screenPerformListBuyers.setSuffixFilter(suffix);
		screenPerformListBuyers.clickOnShowHiddenBuyersButton();
		screenPerformListBuyers.clickOnShowDeletedBuyersButton();
		
		HashMap<String,ArrayList<String>> buyers = screenPerformListBuyers.getBuyerLinesResultFilter();
		for(Entry<String, ArrayList<String>> buyer: buyers.entrySet()){
			String suffixItem = buyer.getValue().get(1);
			if(!suffixItem.contains(suffix)){
				resultOk=false;
				break;
			}
		}
		assertTrue(resultOk);
	}
}
