package com.lge.gpri.tests.buyers.editprofile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Management Items to Edit Buyer Profile.
*  
* @author Cayk Lima Barreto
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestManagementItemsToEditBuyerProfile extends AbstractTestScenary{

	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3183
	* @summary Add one item 
	*/
	@Test
	public void test_2235_AddOneItem() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		String result = "";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("BOI");
		screenPerformListBuyers.clickOnEditProfile("BOI");
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.addItem("chkBandBlockItem");
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.setValue("fieldBandBlock", "12");
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile("BOI");
		result = screenPerformEditBuyerProfile.getValue("fieldBandBlock");
		
		assertEquals("12", result);
		
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3184
	* @summary Remove one item 
	*/
	@Test
	public void test_2236_RemoveOneItem() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean result = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("BOI");
		screenPerformListBuyers.clickOnEditProfile("BOI");
		screenPerformEditBuyerProfile.verifyIfExistItems();
		result = screenPerformEditBuyerProfile.removeItem("chkBandBlockItem");
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile("BOI");
		
		assertTrue(result);
		
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3185
	* @summary Add more than one item 
	*/
	@Test
	public void test_2237_AddMoreThanOneItem() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		String gsm = "";
		String umts = "";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("BOI");
		screenPerformListBuyers.clickOnEditProfile("BOI");
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.addItem("itemGsm");
		screenPerformEditBuyerProfile.addItem("itemUmts");
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.setValue("fieldGsm", "2100");
		screenPerformEditBuyerProfile.setValue("fieldUmts", "7");
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile("BOI");
		gsm = screenPerformEditBuyerProfile.getValue("fieldGsm");
		umts = screenPerformEditBuyerProfile.getValue("fieldUmts");
		
		assertEquals("2100", gsm);
		assertEquals("7", umts);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3186
	* @summary Remove more than one item 
	*/
	@Test
	public void test_2238_RemoveMoreThanOneItem() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean removeGsm = true;
		boolean removeUmts= true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("BOI");
		screenPerformListBuyers.clickOnEditProfile("BOI");
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.clickOnFrequencyBandsButton();
		screenPerformEditBuyerProfile.clickOnFrequencyBandsButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile("BOI");
		removeGsm = screenPerformEditBuyerProfile.existsElement("fieldGsm");
		removeUmts = screenPerformEditBuyerProfile.existsElement("fieldUmts");
		
		assertFalse(removeGsm);
		assertFalse(removeUmts);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3187
	* @summary Add all items 
	*/
	@Test
	public void test_2239_AddAllItems() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		String result = "";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("BOI");
		screenPerformListBuyers.clickOnEditProfile("BOI");
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.clickOnSelectItemsButton();
		result = screenPerformEditBuyerProfile.getAttributeClassOfElement("boxItemLibrary");
		
		assertEquals("jstree-icon jstree-checkbox jstree-undetermined", result);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3188
	* @summary Uncheck all items 
	*/
	@Test
	public void test_2240_UncheckAllItems() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		String result = "";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("BOI");
		screenPerformListBuyers.clickOnEditProfile("BOI");
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.clickOnAddItemToBuyerWithNoItemButton();
		result = screenPerformEditBuyerProfile.getAttributeClassOfElement("btnItemLibrary");
		
		assertEquals("jstree-anchor", result);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3189
	* @summary Add Folder
	*/
	@Test
	public void test_2241_AddFolder() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		String interBand = "";
		String intraBand = "";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("BOI");
		screenPerformListBuyers.clickOnEditProfile("BOI");
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.addItem("itemInterBand");
		screenPerformEditBuyerProfile.addItem("itemIntraBand");
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.setValue("fieldInterBand", "B7+B20, B3+B7, B3+B20");
		screenPerformEditBuyerProfile.setValue("fieldIntraBand", "B7+B20, B3+B7, B3+B20");
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile("BOI");
		interBand = screenPerformEditBuyerProfile.getValue("fieldInterBand");
		intraBand = screenPerformEditBuyerProfile.getValue("fieldIntraBand");
		
		assertEquals("B7+B20, B3+B7, B3+B20", interBand);
		assertEquals("B7+B20, B3+B7, B3+B20", intraBand);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3190
	* @summary Add Folder
	*/
	@Test
	public void test_2242_RemoveFolder() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean interBand = true;
		boolean intraBand = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("BOI");
		screenPerformListBuyers.clickOnEditProfile("BOI");
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.removeItem("itemInterBand");
		screenPerformEditBuyerProfile.removeItem("itemIntraBand");
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile("BOI");
		interBand = screenPerformEditBuyerProfile.existsElement("fieldInterBand");
		intraBand = screenPerformEditBuyerProfile.existsElement("fieldIntraBand");
		
		assertFalse(interBand);
		assertFalse(intraBand);
	}
}