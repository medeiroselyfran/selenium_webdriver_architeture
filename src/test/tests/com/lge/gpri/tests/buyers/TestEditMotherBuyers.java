package com.lge.gpri.tests.buyers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.screens.buyers.AutoProfile;
import com.lge.gpri.screens.buyers.ScreenPerformBuyer;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Edit Mother
 * Buyers.
 * 
 * @author AUTHOR NAME (can exist more than one)
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestEditMotherBuyers extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2731
	 * @summary Edit for two buyers children with same MCC/MNC
	 */
	@Test
	public void test_1883_EditForTwoBuyersChildrenWithSameMCC_MNC() throws Exception {
		boolean successfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_730_30));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
			successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(successfullSaving);
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
			successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(successfullSaving);
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		String buyerMother = screenPerformBuyer.getBuyer();
		String suffixMother = buyerMother.split("\\(")[1].replace(")", "");
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixMother);
		screenPerformListBuyers.clickOnEditBuyer(suffixMother);
		screenPerformBuyer.addChildBuyer(buyer2);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertFalse(successfullSaving);

		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);

		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2732
	 * @summary Edit for three buyers children with same MCC/MNC
	 */
	@Test
	public void test_1884_EditForThreeBuyersChildrenWithSameMCC_MNC() throws Exception {
		boolean successfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_730_30));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
			successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(successfullSaving);
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
			successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(successfullSaving);
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer3 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix3 = buyer3.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer3 = oldNickNameOperator + " " + suffix3 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix3);
			successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(successfullSaving);
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		String buyerMother = screenPerformBuyer.getBuyer();
		String suffixMother = buyerMother.split("\\(")[1].replace(")", "");
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixMother);
		screenPerformListBuyers.clickOnEditBuyer(suffixMother);
		screenPerformBuyer.addChildBuyer(buyer2);
		screenPerformBuyer.addChildBuyer(buyer3);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertFalse(successfullSaving);

		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_3;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);
		expected = expected.replace("SUFFIX3", suffix3);

		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2733
	 * @summary Edit for two buyers children with same MCC/MNC + GID1
	 */
	@Test
	public void test_1885_EditForTwoBuyersChildrenWithSameMCC_MNC_GID1() throws Exception {
		boolean successfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_GID1, ConstantsBuyers.VALUE_714_06_11345678));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
			successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(successfullSaving);
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
			successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(successfullSaving);
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		String buyerMother = screenPerformBuyer.getBuyer();
		String suffixMother = buyerMother.split("\\(")[1].replace(")", "");
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixMother);
		screenPerformListBuyers.clickOnEditBuyer(suffixMother);
		screenPerformBuyer.addChildBuyer(buyer2);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertFalse(successfullSaving);

		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);

		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2734
	 * @summary Edit for two buyers children with same MCC/MNC + SPN
	 */
	@Test
	public void test_1886_EditForTwoBuyersChildrenWithSameMCC_MNC_SPN() throws Exception {
		boolean successfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_SPN, ConstantsBuyers.VALUE_714_06_Orange));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!successfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
				successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
				assertTrue(successfullSaving);
			}
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			screenPerformBuyer.moveToPageHeader();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
			successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(successfullSaving);
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		String buyerMother = screenPerformBuyer.getBuyer();
		String suffixMother = buyerMother.split("\\(")[1].replace(")", "");
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!successfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
				successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			}
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixMother);
		screenPerformListBuyers.clickOnEditBuyer(suffixMother);
		screenPerformBuyer.addChildBuyer(buyer2);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertFalse(successfullSaving);

		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);

		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2735
	 * @summary Edit for two buyers children with same MCC/MNC + IMSI
	 */
	@Test
	public void test_1887_EditForTwoBuyersChildrenWithSameMCC_MNC_IMSI() throws Exception {
		boolean successfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved
				.add(new AutoProfile(ConstantsBuyers.MCC_MNC_IMSI, ConstantsBuyers.VALUE_113_11_7140x13919191x3));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!successfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
				successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
				assertTrue(successfullSaving);
			}
		}
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!successfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
				successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			}
		}
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		String buyerMother = screenPerformBuyer.getBuyer();
		String suffixMother = buyerMother.split("\\(")[1].replace(")", "");
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixMother);
		screenPerformListBuyers.clickOnEditBuyer(suffixMother);
		screenPerformBuyer.addChildBuyer(buyer2);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertFalse(successfullSaving);

		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);

		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2736
	 * @summary Edit for two buyers children with same MCC/MNC + ICCID
	 */
	@Test
	public void test_1888_EditForTwoBuyersChildrenWithSameMCC_MNC_ICCID() throws Exception {
		boolean successfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_ICCID, ConstantsBuyers.VALUE_714_06_11345678));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!successfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
				successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			}
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!successfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
				successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			}
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		String buyerMother = screenPerformBuyer.getBuyer();
		String suffixMother = buyerMother.split("\\(")[1].replace(")", "");
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixMother);
		screenPerformListBuyers.clickOnEditBuyer(suffixMother);
		screenPerformBuyer.addChildBuyer(buyer2);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertFalse(successfullSaving);

		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);

		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2737
	 * @summary Edit for two buyers children with same MCC/MNC + GID1 of type
	 *          MVNO
	 */
	@Test
	public void test_1889_EditForTwoBuyersChildrenWithSameMCC_MNC_GID1OfTypeMVNO() throws Exception {
		boolean successfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_GID1, ConstantsBuyers.VALUE_714_06_11345678));
		String networkOwnerSaved = ConstantsBuyers.MOBILE_ISRAEL_ISM;

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.checkMVNO();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		screenPerformBuyer.setNetworkOwner(networkOwnerSaved);
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!successfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
				successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			}
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.checkMVNO();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		screenPerformBuyer.setNetworkOwner(networkOwnerSaved);
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!successfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
				successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
				assertTrue(successfullSaving);
			}
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		String buyerMother = screenPerformBuyer.getBuyer();
		String suffixMother = buyerMother.split("\\(")[1].replace(")", "");
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(successfullSaving);

		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixMother);
		screenPerformListBuyers.clickOnEditBuyer(suffixMother);
		screenPerformBuyer.addChildBuyer(buyer2);
		successfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertFalse(successfullSaving);

		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);

		assertEquals(expected, msg);
	}
}
