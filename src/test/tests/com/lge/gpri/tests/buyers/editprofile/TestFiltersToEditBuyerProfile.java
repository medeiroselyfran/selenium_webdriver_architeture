package com.lge.gpri.tests.buyers.editprofile;

import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_AUTO_COMMIT;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_DEFAULT_HIDDEN_ON_MOTHER;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_HAS_LINKS;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_MANDATORY;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_MANDATORY_FOR_BUYER_CHILD;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_MULTIPLE;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_SMART_CA_TYPES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_TAG_TYPES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_TAG_VALUES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_TYPE;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITERIA_WARNING;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_AUTO_COMMIT;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_AUTO_COMMIT_2;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_DEFAULT_HIDDEN_ON_MOTHER;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_HAS_LINKS;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_MANDATORY;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_MANDATORY_FOR_BUYER_CHILD;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_MULTIPLE;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_NAME;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_ANDROID_VERSION;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_COTA;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_CUSTOMIZER;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_NO;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_RESOURCE;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_OPTION_YES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_SMART_CA_TYPES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_TAG_TYPES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_TAG_VALUES;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_TYPE;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.FILTER_WARNINGS;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.MESSAGE_MATCHED_AUTO_COMMIT;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.MESSAGE_NO_RESULT_MATCHED_AUTO_COMMIT_2;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.SUFFIX_BOI;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.TXT_NO_RESULT_MATCHED;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.TXT_RESULT_MATCHED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsItemInspector;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Filters to Edit Buyer Profile.
*  
* @author Cayk Lima Barreto
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestFiltersToEditBuyerProfile extends AbstractTestScenary{

	/**
	* @testLinkId T-GPRI-3191
	* @summary Auto Commit 
	*/
	@Test
	public void test_2243_AutoCommit() throws Exception {
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_AUTO_COMMIT);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_AUTO_COMMIT);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, FILTER_AUTO_COMMIT);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformListBuyers.openPage();
			screenPerformListBuyers.addSuffixFilter();
			screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
			screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
			screenPerformEditBuyerProfile.verifyIfExistItems();
			screenPerformEditBuyerProfile.optionFilters();
			screenPerformEditBuyerProfile.searchItemLibrary(FILTER_AUTO_COMMIT);
			screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_AUTO_COMMIT);
			screenPerformEditBuyerProfile.elementFocus();
			screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
			screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_NO);
			screenPerformEditBuyerProfile.waitLoading();
			pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformEditBuyerProfile.newTab("");
				screenPerformEditBuyerProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, FILTER_AUTO_COMMIT);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditBuyerProfile.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3192
	* @summary Default Hide on Mother 
	*/
	@Test
	public void test_2244_DefaultHideOnMother() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.DEFAULT_HIDE_ON_MOTHER);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformListBuyers.openPage();
			screenPerformListBuyers.addSuffixFilter();
			screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
			screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
			screenPerformEditBuyerProfile.verifyIfExistItems();
			screenPerformEditBuyerProfile.optionFilters();
			screenPerformEditBuyerProfile.searchItemLibrary(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
			screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
			screenPerformEditBuyerProfile.elementFocus();
			screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER);
			screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER, FILTER_OPTION_NO);
			screenPerformEditBuyerProfile.waitLoading();
			pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformEditBuyerProfile.newTab("");
				screenPerformEditBuyerProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.DEFAULT_HIDE_ON_MOTHER);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditBuyerProfile.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3193
	* @summary Mandatory 
	*/
	@Test
	public void test_2245_Mandatory() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_MANDATORY);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_MANDATORY);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformListBuyers.openPage();
			screenPerformListBuyers.addSuffixFilter();
			screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
			screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
			screenPerformEditBuyerProfile.verifyIfExistItems();
			screenPerformEditBuyerProfile.optionFilters();
			screenPerformEditBuyerProfile.searchItemLibrary(FILTER_MANDATORY);
			screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_MANDATORY);
			screenPerformEditBuyerProfile.elementFocus();
			screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY);
			screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY, FILTER_OPTION_NO);
			screenPerformEditBuyerProfile.waitLoading();
			pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformEditBuyerProfile.newTab("");
				screenPerformEditBuyerProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditBuyerProfile.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3194
	* @summary Mandatory for Buyer Child 
	*/
	@Test
	public void test_2246_MandatoryForBuyerChild() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY_FOR_BUYER_CHILD, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY_FOR_BUYER_CHILD);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformListBuyers.openPage();
			screenPerformListBuyers.addSuffixFilter();
			screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
			screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
			screenPerformEditBuyerProfile.verifyIfExistItems();
			screenPerformEditBuyerProfile.optionFilters();
			screenPerformEditBuyerProfile.searchItemLibrary(FILTER_MANDATORY_FOR_BUYER_CHILD);
			screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_MANDATORY_FOR_BUYER_CHILD);
			screenPerformEditBuyerProfile.elementFocus();
			screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD);
			screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY_FOR_BUYER_CHILD, FILTER_OPTION_NO);
			screenPerformEditBuyerProfile.waitLoading();
			pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformEditBuyerProfile.newTab("");
				screenPerformEditBuyerProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY_FOR_BUYER_CHILD);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditBuyerProfile.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3195
	* @summary Multiple 
	*/
	@Test
	public void test_2247_Multiple() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_MULTIPLE);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_MULTIPLE);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MULTIPLE);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MULTIPLE, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MULTIPLE);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformListBuyers.openPage();
			screenPerformListBuyers.addSuffixFilter();
			screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
			screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
			screenPerformEditBuyerProfile.verifyIfExistItems();
			screenPerformEditBuyerProfile.optionFilters();
			screenPerformEditBuyerProfile.searchItemLibrary(FILTER_MULTIPLE);
			screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_MULTIPLE);
			screenPerformEditBuyerProfile.elementFocus();
			screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MULTIPLE);
			screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MULTIPLE, FILTER_OPTION_NO);
			screenPerformEditBuyerProfile.waitLoading();
			pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformEditBuyerProfile.newTab("");
				screenPerformEditBuyerProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MULTIPLE);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditBuyerProfile.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3196
	* @summary Name 
	*/
	@Test
	public void test_2248_Name() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_NAME);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_NAME);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.setTextOnFilterName(ConstantsItemInspector.HOME);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_NAME);
			resultOk = value.contains(ConstantsItemInspector.HOME);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3197
	* @summary SmartCA Types 
	*/
	@Test
	public void test_2249_SmartCATypes() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_SMART_CA_TYPES);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_SMART_CA_TYPES);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSmartCaDropDown();
		screenPerformEditBuyerProfile.clickOnSelectedOptionForSmartCaTypeFilter(FILTER_OPTION_CUSTOMIZER);
		screenPerformEditBuyerProfile.waitLoading();
		screenPerformEditBuyerProfile.clickOnSmartCaDropDown();
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_SMARTCA_TYPES);
			resultOk = value.equals(ConstantsItemInspector.CUSTOMIZER);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3198
	* @summary Tag Types 
	*/
	@Test
	public void test_2250_TagTypes() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_TAG_TYPES);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_TAG_TYPES);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_TAG_TYPES);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_TAG_TYPES, FILTER_OPTION_ANDROID_VERSION);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_TAG_TYPES);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_TAG_TYPES);
			resultOk = value.contains(ConstantsItemInspector.ANDROID_VERSION);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3199
	* @summary Tag Values 
	*/
	@Test
	public void test_2251_TagValues() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_TAG_VALUES);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_TAG_VALUES);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_TAG_VALUES);
		screenPerformEditBuyerProfile.selectValueFilterOptionWithArea(CRITERIA_TAG_VALUES, ConstantsItemInspector.ANDROID_VERSION, ConstantsItemInspector.ANDROID_VERSION);
		ArrayList<String> allVersions = screenPerformEditBuyerProfile.getAllValuesOfTagValues();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_TAG_VALUES);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_TAG_VALUES);
			String[] versions = value.split(",");
			boolean hasSomeOne = false;
			for(int i=0;i<versions.length&&resultOk;i++){
				if(versions[i].contains(ConstantsItemInspector.ANDROID_VERSION)){
					hasSomeOne = true;
					resultOk = allVersions.contains(versions[i].replace(ConstantsItemInspector.ANDROID_VERSION+" ",""));
				}
			}
			if(!hasSomeOne){resultOk = false;}
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3200
	* @summary Type 
	*/
	@Test
	public void test_2252_Type() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_TYPE);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_TYPE);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_TYPE);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_TYPE, FILTER_OPTION_RESOURCE);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_TYPE);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_TYPE);
			resultOk = value.contains(FILTER_OPTION_RESOURCE);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3201
	* @summary Warnings 
	*/
	@Test
	public void test_2253_Warnings() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_WARNINGS);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_WARNINGS);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_WARNING);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_WARNING, FILTER_OPTION_COTA);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_WARNING);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, FILTER_WARNINGS);
			resultOk = value.contains(FILTER_OPTION_COTA);
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3202
	* @summary Has Links
	*/
	@Test
	public void test_2254_HasLinks() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		ArrayList<Boolean> noProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_HAS_LINKS);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_HAS_LINKS);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_HAS_LINKS);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_HAS_LINKS, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String value = screenItemLibrary.getValueProperty(path, FILTER_HAS_LINKS);
			resultOk = value.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		if(resultOk){
			screenPerformListBuyers.openPage();
			screenPerformListBuyers.addSuffixFilter();
			screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
			screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
			screenPerformEditBuyerProfile.verifyIfExistItems();
			screenPerformEditBuyerProfile.optionFilters();
			screenPerformEditBuyerProfile.searchItemLibrary(FILTER_HAS_LINKS);
			screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_HAS_LINKS);
			screenPerformEditBuyerProfile.elementFocus();
			screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_HAS_LINKS);
			screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_HAS_LINKS, FILTER_OPTION_NO);
			screenPerformEditBuyerProfile.waitLoading();
			pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
			for(String path: pathItems){
				screenPerformEditBuyerProfile.newTab("");
				screenPerformEditBuyerProfile.switchTabs(1);
				screenItemLibrary.openPage();
				String value = screenItemLibrary.getValueProperty(path, FILTER_HAS_LINKS);
				resultOk = value.equals("false");
				noProperty.add(resultOk);
				screenItemLibrary.getDriver().close();
				screenPerformEditBuyerProfile.switchTabs(0);
				if(!resultOk){
					break;
				}
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
		for(Boolean result: noProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3203
	* @summary Auto Commit and Default Hide on Mother
	*/
	@Test
	public void test_2255_AutoCommitAndDefaultHideOnMother() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_AUTO_COMMIT);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_AUTO_COMMIT);
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String valueAutoCommit = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_AUTO_COMMIT);
			String valueHideOnMother = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.DEFAULT_HIDE_ON_MOTHER);
			resultOk = valueAutoCommit.equals("true");
			resultOk = resultOk&&valueHideOnMother.equals("true");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3204
	* @summary Mandatory and Mandatory for Buyer Child
	*/
	@Test
	public void test_2256_MandatoryAndMandatoryForBuyerChild() throws Exception{
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ArrayList<String> pathItems;
		ArrayList<Boolean> yesProperty = new ArrayList<>();
		boolean resultOk=true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_MANDATORY);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_MANDATORY);
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY, FILTER_OPTION_YES);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_MANDATORY_FOR_BUYER_CHILD, FILTER_OPTION_NO);
		screenPerformEditBuyerProfile.waitLoading();
		pathItems = screenPerformEditBuyerProfile.getFirstItemsOfEachRootDirectory();
		for(String path: pathItems){
			screenPerformEditBuyerProfile.newTab("");
			screenPerformEditBuyerProfile.switchTabs(1);
			screenItemLibrary.openPage();
			String valueMandatory = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY);
			String valueMandatoryBuyer = screenItemLibrary.getValueProperty(path, ConstantsItemInspector.FILTER_MANDATORY_FOR_BUYER_CHILD);
			resultOk = valueMandatory.equals("true");
			resultOk = resultOk&&valueMandatoryBuyer.equals("false");
			yesProperty.add(resultOk);
			screenItemLibrary.getDriver().close();
			screenPerformEditBuyerProfile.switchTabs(0);
			if(!resultOk){
				break;
			}
		}
		
		for(Boolean result: yesProperty){
			assertTrue(result);
		}
	}
	
	/**
	* @testLinkId T-GPRI-3205
	* @summary All Filters
	*/
	@Test
	public void test_2257_AllFilters() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultSelectAllFilters = false;
		boolean resultDeselectAllFilters = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		resultSelectAllFilters = screenPerformEditBuyerProfile.selectAllFilters();
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_AUTO_COMMIT); 
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER); 
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_MANDATORY); 
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD); 
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_MULTIPLE); 
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterName();
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_SMART_CA_TYPES); 
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_TAG_TYPES); 
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_TAG_VALUES); 
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_TYPE); 
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_WARNING); 
		resultSelectAllFilters = resultSelectAllFilters && screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_HAS_LINKS);
		resultDeselectAllFilters = screenPerformEditBuyerProfile.deselectAllFilters();
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_AUTO_COMMIT); 
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_DEFAULT_HIDDEN_ON_MOTHER); 
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_MANDATORY); 
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_MANDATORY_FOR_BUYER_CHILD); 
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_MULTIPLE); 
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterName();
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterSmartCaOption(); 
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_TAG_TYPES); 
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_TAG_VALUES); 
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_TYPE); 
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_WARNING); 
		resultDeselectAllFilters = resultDeselectAllFilters && !screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_HAS_LINKS);
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(resultSelectAllFilters);
		assertTrue(resultDeselectAllFilters);
	}
	
	/**
	* @testLinkId T-GPRI-3206
	* @summary Remove filter
	*/
	@Test
	public void test_2258_RemoveFilter() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultAutoCommitAdd = false;
		boolean resultRemoveFilter = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_AUTO_COMMIT);
		screenPerformEditBuyerProfile.clickOnSelectedFilter(FILTER_AUTO_COMMIT);
		screenPerformEditBuyerProfile.elementFocus();
		screenPerformEditBuyerProfile.clickOnSelectedFilterOptions(CRITERIA_AUTO_COMMIT);
		screenPerformEditBuyerProfile.clickOnSelectedFilterOption(CRITERIA_AUTO_COMMIT, FILTER_OPTION_YES);
		resultAutoCommitAdd = screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_AUTO_COMMIT); 
		screenPerformEditBuyerProfile.waitLoading();
		screenPerformEditBuyerProfile.removeFilter(CRITERIA_AUTO_COMMIT);
		resultRemoveFilter = !screenPerformEditBuyerProfile.isVisibleFilterOptions(CRITERIA_AUTO_COMMIT); 
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(resultAutoCommitAdd);
		assertTrue(resultRemoveFilter);
	}
	
	/**
	* @testLinkId T-GPRI-3207
	* @summary Search on filter
	*/
	@Test
	public void test_2259_SearchOnFilter() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		String resultAuCommit = "";
		String resultAuCommit2 = "";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.optionFilters();
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_AUTO_COMMIT);
		resultAuCommit = screenPerformEditBuyerProfile.getText(TXT_RESULT_MATCHED);
		screenPerformEditBuyerProfile.searchItemLibrary(FILTER_AUTO_COMMIT_2);
		resultAuCommit2 = screenPerformEditBuyerProfile.getText(TXT_NO_RESULT_MATCHED);
		
		assertEquals(MESSAGE_MATCHED_AUTO_COMMIT, resultAuCommit);
		assertEquals(MESSAGE_NO_RESULT_MATCHED_AUTO_COMMIT_2, resultAuCommit2);
	}
}

