package com.lge.gpri.tests.buyers.editprofile;

import static com.lge.gpri.helpers.constants.ConstantsBuyers.*;
import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.tests.AbstractTestScenary;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Components to Edit Buyer Profile.
*  
* @author Cayk Lima Barreto
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestComponentsToEditBuyerProfile extends AbstractTestScenary{
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3208
	* @summary Only checked items 
	*/
	@Test
	public void test_2260_OnlyCheckedItems() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean itemName = false;
		boolean itemIap = false;
		boolean itemHomeNetwork = false;
		boolean itemBandBlock = false;
		boolean itemHplmn = false;
		boolean checked = false;
		boolean unchecked = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.clickOnItemLibraryButton();
		screenPerformEditBuyerProfile.addItem(ITEM_NAME);
		screenPerformEditBuyerProfile.addItem(ITEM_IAPN_PROTOCOL_TYPE);
		screenPerformEditBuyerProfile.addItem(ITEM_HOME_NETWORK);
		screenPerformEditBuyerProfile.addItem(ITEM_BAND_BLOCK);
		screenPerformEditBuyerProfile.addItem(ITEM_HPLMN_TIMER);
		checked = screenPerformEditBuyerProfile.clickOnOnlyCheckedItemsButton();
		itemName = screenPerformEditBuyerProfile.checkItemClass(ITEM_NAME, ITEM_CHECKED);
		itemIap = screenPerformEditBuyerProfile.checkItemClass(ITEM_IAPN_PROTOCOL_TYPE, ITEM_CHECKED);
		itemHomeNetwork = screenPerformEditBuyerProfile.checkItemClass(ITEM_HOME_NETWORK, ITEM_CHECKED);
		itemBandBlock = screenPerformEditBuyerProfile.checkItemClass(ITEM_BAND_BLOCK, ITEM_CHECKED);
		itemHplmn = screenPerformEditBuyerProfile.checkItemClass(ITEM_HPLMN_TIMER, ITEM_CHECKED);
		unchecked = screenPerformEditBuyerProfile.clickOnOnlyCheckedItemsButton();
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(checked);
		assertTrue(itemName);
		assertTrue(itemIap);
		assertTrue(itemHomeNetwork);
		assertTrue(itemBandBlock);
		assertTrue(itemHplmn);
		assertTrue(unchecked);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3209
	* @summary Search 
	*/
	@Test
	public void test_2261_Search() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean highlightedItem = false;
		boolean itemNotHighlighted = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.searchItemSelectItems(NAME);
		highlightedItem = screenPerformEditBuyerProfile.checkItemClass(ITEM_NAME, ITEM_HIGHLIGHTED);
		screenPerformEditBuyerProfile.searchItemSelectItems(NAMI);
		itemNotHighlighted = screenPerformEditBuyerProfile.checkItemClass(ITEM_NAME, ITEM_HIGHLIGHTED);
		
		assertTrue(highlightedItem);
		assertFalse(itemNotHighlighted);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3210
	* @summary Add one item do not apply with group multiple 
	*/
	@Test
	public void test_2262_AddOneItemDoNotApplyWithGroupMultiple() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean itemChecked = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.moveTo(ITEM_CUSTOM_ICON);
		screenPerformEditBuyerProfile.addItem(ITEM_CUSTOM_ICON);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnMarkItemsAsDoNotApplyButton();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		itemChecked = screenPerformEditBuyerProfile.checkItemClass(ITEM_CUSTOM_ICON, ITEM_CHECKED);
		
		assertFalse(itemChecked);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3211
	* @summary Add more than one item do not apply with group multiple
	*/
	@Test
	public void test_2263_AddMoreThanOneItemDoNotApplyWithGroupMultiple() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean itemIconChecked = true;
		boolean itemLabelChecked = true;
		boolean checked = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.moveTo(ITEM_CUSTOM_ICON);
		screenPerformEditBuyerProfile.addItem(ITEM_CUSTOM_ICON);
		checked = screenPerformEditBuyerProfile.boxIsChecked(ITEM_CUSTOM_LABEL);
		screenPerformEditBuyerProfile.addItem(ITEM_CUSTOM_LABEL);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnMarkItemsAsDoNotApplyButton();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		if(checked){
			screenPerformEditBuyerProfile.clickOnCustomizationButton();
			screenPerformEditBuyerProfile.clickOnBoxDoNotApplyForItemCustomLabel();
		}
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		itemIconChecked = screenPerformEditBuyerProfile.checkItemClass(ITEM_CUSTOM_ICON, ITEM_CHECKED);
		itemLabelChecked = screenPerformEditBuyerProfile.checkItemClass(ITEM_CUSTOM_LABEL, ITEM_CHECKED);
		
		assertFalse(itemIconChecked);
		assertFalse(itemLabelChecked);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3212
	* @summary Add more than one item do not apply with group multiple and disabled one item
	*/
	@Test
	public void test_2264_AddMoreThanOneItemDoNotApplyWithGroupMultipleAndDisabledOneItem() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean itemIconChecked = true;
		boolean itemLabelChecked = false;
		boolean checked = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.moveTo(ITEM_CUSTOM_ICON);
		screenPerformEditBuyerProfile.addItem(ITEM_CUSTOM_ICON);
		checked = screenPerformEditBuyerProfile.boxIsChecked(ITEM_CUSTOM_LABEL);
		screenPerformEditBuyerProfile.addItem(ITEM_CUSTOM_LABEL);
		screenPerformEditBuyerProfile.moveToPageBottom();
		screenPerformEditBuyerProfile.clickOnMarkItemsAsDoNotApplyButton();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.clickOnCustomizationButton();
		if(!checked){
			screenPerformEditBuyerProfile.clickOnBoxDoNotApplyForItemCustomLabel();
		}
		screenPerformEditBuyerProfile.setValue(FIELD_CUSTOM_LABEL, TEST);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.clickOnSelectItemsButton();
		itemIconChecked = screenPerformEditBuyerProfile.checkItemClass(ITEM_CUSTOM_ICON, ITEM_CHECKED);
		itemLabelChecked = screenPerformEditBuyerProfile.checkItemClass(ITEM_CUSTOM_LABEL, ITEM_CHECKED);
		
		assertFalse(itemIconChecked);
		assertTrue(itemLabelChecked);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3213
	* @summary Add one item as do not apply of a item not associated with a multiple group
	*/
	@Test
	public void test_2265_AddOneItemAsDoNotApplyOfAItemNotAssociatedWithAMultipleGroup() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		String itemValue = "";
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		screenPerformEditBuyerProfile.addItem(ITEM_BAND_BLOCK);
		screenPerformEditBuyerProfile.clickOnMarkItemsAsDoNotApplyButton();
		screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
		screenPerformEditBuyerProfile.setValue(FIELD_BAND_BLOCK, VALUE_123);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		itemValue = screenPerformEditBuyerProfile.getValue(FIELD_BAND_BLOCK);
		
		assertEquals(VALUE_123, itemValue);
	}
	
	/**
	* @throws Exception 
	 * @testLinkId T-GPRI-3214
	* @summary View Items
	*/
	@Test
	public void test_2266_ViewItems() throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean expand = false;
		boolean collapse = false;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditProfile(SUFFIX_BOI);
		screenPerformEditBuyerProfile.verifyIfExistItems();
		collapse = screenPerformEditBuyerProfile.clickOnCollapseAllNodes();
		screenPerformEditBuyerProfile.waitLoading();
		expand = screenPerformEditBuyerProfile.clickExpandAllNodesButton();
		screenPerformEditBuyerProfile.waitLoading();
		
		assertTrue(expand);
		assertTrue(collapse);
	}
}

