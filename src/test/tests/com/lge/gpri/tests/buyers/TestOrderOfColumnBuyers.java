package com.lge.gpri.tests.buyers;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.tests.AbstractTestScenary;

import edu.emory.mathcs.backport.java.util.Arrays;

/**
* Test Scenery class.
* 
* <p>Description: This class must contains the tests for the scenery Order Of Column Buyers.
*  
* @author Gabriel Costa do Nascimento
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestOrderOfColumnBuyers extends AbstractTestScenary{
	
	/**
	* @testLinkId T-GPRI-3269
	* @summary Decreasing for column Status
	*/
	@Test
	public void test_3269_DecreasingForColumnStatus() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		@SuppressWarnings("unchecked")
		ArrayList<String> order = new ArrayList<>(Arrays.asList(new String[]{
				"Validated","Under Edition","Under Edition / Needs Review","Needs Review","Ready for Approval","Ready for Approval / Needs Review"}));
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addRegionFilter();
		screenPerformListBuyers.setRegionFilter(ConstantsBuyers.EUROPE);
		screenPerformListBuyers.setDecreasingStatusOrder();
		String previous = order.get(5);
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.STATUS);
			for(String buyer: buyers){
				String actual = buyer.split("\n")[0];
				int posPrevious = order.indexOf(previous);
				int posActual = order.indexOf(actual);
				if(posPrevious<posActual){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3270
	* @summary Increasing for column Status
	*/
	@Test
	public void test_3270_IncreasingForColumnStatus() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		@SuppressWarnings("unchecked")
		ArrayList<String> order = new ArrayList<>(Arrays.asList(new String[]{
				"Validated","Under Edition","Under Edition / Needs Review","Needs Review","Ready for Approval","Ready for Approval / Needs Review"}));
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addRegionFilter();
		screenPerformListBuyers.setRegionFilter(ConstantsBuyers.EUROPE);
		screenPerformListBuyers.setIncreasingStatusOrder();
		String previous = order.get(0);
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.STATUS);
			for(String buyer: buyers){
				String actual = buyer.split("\n")[0];
				int posPrevious = order.indexOf(previous);
				int posActual = order.indexOf(actual);
				if(posPrevious>posActual){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3271
	* @summary Decreasing for column Region
	*/
	@Test
	public void test_3271_DecreasingForColumnRegion() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setDecreasingRegionOrder();
		String previous = "Z";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.REGION);
			for(String buyer: buyers){
				String actual = buyer.toUpperCase();
				if(previous.compareTo(actual)<0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3272
	* @summary Increasing for column Region
	*/
	@Test
	public void test_3272_IncreasingForColumnRegion() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setIncreasingRegionOrder();
		String previous = "A";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.REGION);
			for(String buyer: buyers){
				String actual = buyer.toUpperCase();
				if(previous.compareTo(actual)>0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3273
	* @summary Decreasing for column Suffix
	*/
	@Test
	public void test_3273_DecreasingForColumnSuffix() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setDecreasingSuffixOrder();
		String previous = "ZZZ";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.SUFFIX);
			for(String buyer: buyers){
				String actual = buyer.toUpperCase();
				if(previous.compareTo(actual)<0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3274
	* @summary Increasing for column Suffix
	*/
	@Test
	public void test_3274_IncreasingForColumnSuffix() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setIncreasingSuffixOrder();
		String previous = "000";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.SUFFIX);
			for(String buyer: buyers){
				String actual = buyer.toUpperCase();
				if(previous.compareTo(actual)>0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3275
	* @summary Decreasing for column Country
	*/
	@Test
	public void test_3275_DecreasingForColumnCountry() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setDecreasingCountryOrder();
		String previous = "ZZZ";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.COUNTRY);
			for(String buyer: buyers){
				String actual = buyer.toUpperCase();
				if(previous.compareTo(actual)<0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3276
	* @summary Increasing for column Country
	*/
	@Test
	public void test_3276_IncreasingForColumnCountry() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setIncreasingCountryOrder();
		String previous = "000";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.COUNTRY);
			for(String buyer: buyers){
				String actual = buyer.toUpperCase();
				if(previous.compareTo(actual)>0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3277
	* @summary Decreasing for column Operator
	*/
	@Test
	public void test_3277_DecreasingForColumnOperator() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setDecreasingOperatorOrder();
		String previous = "ZZZ";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.OPERATOR);
			for(String buyer: buyers){
				String actual = buyer.toUpperCase();
				if(previous.compareTo(actual)<0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3278
	* @summary Increasing for column Operator
	*/
	@Test
	public void test_3278_IncreasingForColumnOperator() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setIncreasingOperatorOrder();
		String previous = "000";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.OPERATOR);
			for(String buyer: buyers){
				String actual = buyer.toUpperCase();
				if(previous.compareTo(actual)>0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3279
	* @summary Decreasing for column Type
	*/
	@Test
	public void test_3279_DecreasingForColumnType() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setDecreasingTypeOrder();
		String previous = "ZZZ";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.TYPE);
			for(String buyer: buyers){
				String actual = buyer.split(" ")[0].toUpperCase();
				if(previous.compareTo(actual)<0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3280
	* @summary Increasing for column Type
	*/
	@Test
	public void test_3280_IncreasingForColumnType() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setIncreasingTypeOrder();
		String previous = "000";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.TYPE);
			for(String buyer: buyers){
				String actual = buyer.split(" ")[0].toUpperCase();
				if(previous.compareTo(actual)>0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3281
	* @summary Decreasing for column Global Group
	*/
	@Test
	public void test_3281_DecreasingForColumnGlobalGroup() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setDecreasingGlobalGroupOrder();
		String previous = "ZZZ";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.GLOBAL_GROUP);
			for(String buyer: buyers){
				String actual = buyer.split(" ")[0].toUpperCase();
				if(actual.equals("NONE")){
					actual = previous;
				}
				if(previous.compareTo(actual)<0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
	
	/**
	* @testLinkId T-GPRI-3282
	* @summary Increasing for column Global Group
	*/
	@Test
	public void test_3282_IncreasingForColumnGlobalGroup() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		boolean resultOk = true;

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter("A");
		screenPerformListBuyers.setIncreasingGlobalGroupOrder();
		String previous = "000";
		do{
			ArrayList<String> buyers = screenPerformListBuyers.getColumnResultFilter(ConstantsBuyers.GLOBAL_GROUP);
			for(String buyer: buyers){
				String actual = buyer.split(" ")[0].toUpperCase();
				if(actual.equals("NONE")){
					actual = previous;
				}
				if(previous.compareTo(actual)>0){
					resultOk=false;
					break;
				}
				previous = actual;
			}
			if(!resultOk){break;}
		}while(screenPerformListBuyers.paginateNext());
		
		assertTrue(resultOk);
	}
}
