package com.lge.gpri.tests.buyers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.screens.buyers.AutoProfile;
import com.lge.gpri.screens.buyers.ScreenPerformBuyer;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Create Mother
 * Buyers.
 * 
 * @author Gabriel Costa do Nascimento
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCreateMotherBuyers extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2712
	 * @summary Without Global Group with one child and one LTM
	 */
	@Test
	public void test_1864_WithoutGlobalGroupWithOneChildAndOneLTM() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		screenPerformListBuyers.openPage();

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsMotherBuyerFromNewBuyer(ConstantsBuyers.RANDOM, ConstantsBuyers.RANDOM,
				ConstantsBuyers.RANDOM, ConstantsBuyers.EMPTY,
				Arrays.asList(ConstantsBuyers.BUYER_019MOBILE_ISRAEL_ISM),
				Arrays.asList(ConstantsBuyers.AJAY_CHEECKOLI), Arrays.asList(ConstantsBuyers.EMPTY));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String buyerChildSaved = ConstantsBuyers.BUYER_019MOBILE_ISRAEL_ISM;
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		ArrayList<String> buyerChildren = screenPerformBuyer.getBuyerChildren();
		String buyerChild = buyerChildren.get(0);
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(buyerChildSaved, buyerChild);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2713
	 * @summary With Buyer existing without Global Group with one child and one
	 *          LTM
	 */
	@Test
	public void test_1865_WithBuyerExistingWithoutGlobalGroupWithOneChildAndOneLTM() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		screenPerformListBuyers.openPage();

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsMotherBuyerFromExistBuyer(ConstantsBuyers.FIRST, ConstantsBuyers.EMPTY,
				Arrays.asList(ConstantsBuyers.BUYER_AIRTEL_INDIA_ART),
				Arrays.asList(ConstantsBuyers.AJAY_CHEECKOLI), Arrays.asList(ConstantsBuyers.EMPTY));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String buyerChildSaved = ConstantsBuyers.BUYER_AIRTEL_INDIA_ART;
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		ArrayList<String> buyerChildren = screenPerformBuyer.getBuyerChildren();
		String buyerChild = buyerChildren.get(0);
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(buyerChildSaved, buyerChild);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2714
	 * @summary With Buyer existing without Global Group with two child and one
	 *          LTM
	 */
	@Test
	public void test_1866_WithBuyerExistingWithoutGlobalGroupWithTwoChildAndOneLTM() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		screenPerformListBuyers.openPage();

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsMotherBuyerFromExistBuyer(ConstantsBuyers.FIRST, ConstantsBuyers.EMPTY,
				Arrays.asList(ConstantsBuyers.BUYER_AIRTEL_INDIA_ART, ConstantsBuyers.BUYER_AIRCEL_INDIA_AAI),
				Arrays.asList(ConstantsBuyers.AJAY_CHEECKOLI), Arrays.asList(ConstantsBuyers.EMPTY));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String buyerChildSaved1 = ConstantsBuyers.BUYER_AIRCEL_INDIA_AAI;
		String buyerChildSaved2 = ConstantsBuyers.BUYER_AIRTEL_INDIA_ART;
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved = ltmsSaved.get(0);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		ArrayList<String> buyerChildren = screenPerformBuyer.getBuyerChildren();
		String buyerChild1 = buyerChildren.get(0);
		String buyerChild2 = buyerChildren.get(1);
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm = ltms.get(0);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(buyerChildSaved1, buyerChild1);
		assertEquals(buyerChildSaved2, buyerChild2);
		assertEquals(ltmSaved, ltm);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2715
	 * @summary With Buyer existing without Global Group with one child and two
	 *          LTM
	 */
	@Test
	public void test_1867_WithBuyerExistingWithoutGlobalGroupWithOneChildAndTwoLTM() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		screenPerformListBuyers.openPage();

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsMotherBuyerFromExistBuyer(ConstantsBuyers.FIRST, ConstantsBuyers.EMPTY,
				Arrays.asList(ConstantsBuyers.BUYER_AIRTEL_INDIA_ART),
				Arrays.asList(ConstantsBuyers.AJAY_CHEECKOLI, ConstantsBuyers.ALEX_RECHTMAN_COMPLETE),
				Arrays.asList(ConstantsBuyers.EMPTY));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String buyerChildSaved = ConstantsBuyers.BUYER_AIRTEL_INDIA_ART;
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved1 = ltmsSaved.get(0);
		String ltmSaved2 = ltmsSaved.get(1);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		ArrayList<String> buyerChildren = screenPerformBuyer.getBuyerChildren();
		String buyerChild = buyerChildren.get(0);
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm1 = ltms.get(0);
		String ltm2 = ltms.get(1);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(buyerChildSaved, buyerChild);
		assertEquals(ltmSaved1, ltm1);
		assertEquals(ltmSaved2, ltm2);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2716
	 * @summary With Buyer existing with Global Group with one child and two LTM
	 */
	@Test
	public void test_1868_WithBuyerExistingWithGlobalGroupWithOneChildAndTwoLTM() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		screenPerformListBuyers.openPage();

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsMotherBuyerFromExistBuyer(ConstantsBuyers.FIRST, ConstantsBuyers.ORANGE,
				Arrays.asList(ConstantsBuyers.BUYER_AIRTEL_INDIA_ART),
				Arrays.asList(ConstantsBuyers.AJAY_CHEECKOLI, ConstantsBuyers.ALEX_RECHTMAN_COMPLETE),
				Arrays.asList(ConstantsBuyers.EMPTY));

		String typeSaved = screenPerformBuyer.getType();
		String buyerSaved = screenPerformBuyer.getBuyer();
		String nicknameSaved = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String suffixSaved = buyerSaved.split("\\(")[1].replace(")", "");
		String globalGroupSaved = screenPerformBuyer.getGlobalGroup();
		String buyerChildSaved = ConstantsBuyers.BUYER_AIRTEL_INDIA_ART;
		ArrayList<String> ltmsSaved = screenPerformBuyer.getLTMs();
		String ltmSaved1 = ltmsSaved.get(0);
		String ltmSaved2 = ltmsSaved.get(1);
		ArrayList<String> gtamsSaved = screenPerformBuyer.getGTAMs();
		String gtamSaved = gtamsSaved.get(0);

		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyerSaved = oldNickNameOperator + " " + suffixSaved + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffixSaved);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffixSaved);
		screenPerformListBuyers.clickOnEditBuyer(suffixSaved);

		String type = screenPerformBuyer.getType();
		String buyer = screenPerformBuyer.getBuyer();
		String nickname = buyerSaved.split("\\(")[0].substring(0, buyerSaved.split("\\(")[0].length() - 1);
		String globalGroup = screenPerformBuyer.getGlobalGroup();
		ArrayList<String> buyerChildren = screenPerformBuyer.getBuyerChildren();
		String buyerChild = buyerChildren.get(0);
		ArrayList<String> ltms = screenPerformBuyer.getLTMs();
		String ltm1 = ltms.get(0);
		String ltm2 = ltms.get(1);
		ArrayList<String> gtams = screenPerformBuyer.getGTAMs();
		String gtam = gtams.get(0);

		assertEquals(typeSaved, type);
		assertEquals(buyerSaved, buyer);
		assertEquals(nicknameSaved, nickname);
		assertEquals(globalGroupSaved, globalGroup);
		assertEquals(buyerChildSaved, buyerChild);
		assertEquals(ltmSaved1, ltm1);
		assertEquals(ltmSaved2, ltm2);
		assertEquals(gtamSaved, gtam);
	}

	/**
	 * @testLinkId T-GPRI-2717
	 * @summary With only type buyer is selected
	 */
	@Test
	public void test_1869_WithOnlyTypeBuyerIsSelected() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR_CHILDREN);

		assertEquals(ConstantsBuyers.WARNING_CHILDREN, msg);
	}

	/**
	 * @testLinkId T-GPRI-2718
	 * @summary Only Buyer Mother Selected
	 */
	@Test
	public void test_1870_OnlyBuyerMotherSelected() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR_CHILDREN);

		assertEquals(ConstantsBuyers.WARNING_CHILDREN, msg);
	}

	/**
	 * @testLinkId T-GPRI-2719
	 * @summary With Buyer existing selecting only Buyer mother and Child Buyer
	 */
	@Test
	public void test_1871_WithBuyerExistingSelectingOnlyBuyermotherAndChildBuyer() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		screenPerformBuyer.addChildBuyer(ConstantsBuyers.BUYER_AIRTEL_INDIA_ART);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR_LTM);

		assertEquals(ConstantsBuyers.WARNING_LTM_USER, msg);
	}

	/**
	 * @testLinkId T-GPRI-2720
	 * @summary With Buyer existing selecting only Global Group
	 */
	@Test
	public void test_1872_WithBuyerExistingSelectingOnlyGlobalGroup() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setGlobalGroup(ConstantsBuyers.DTAG);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR_CHILDREN);

		assertEquals(ConstantsBuyers.WARNING_CHILDREN, msg);
	}

	/**
	 * @testLinkId T-GPRI-2721
	 * @summary With Buyer existing selecting only LTM
	 */
	@Test
	public void test_1873_WithBuyerExistingSelectingOnlyLTM() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setGlobalGroup(ConstantsBuyers.DTAG);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI_COMPLETE);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		String msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR_CHILDREN);

		assertEquals(ConstantsBuyers.WARNING_CHILDREN, msg);
	}

	/**
	 * @testLinkId T-GPRI-2722
	 * @summary Without selecting any option window "New Buyer"
	 */
	@Test
	public void test_1874_WithoutSelectingAnyOptionWindowNewBuyer() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.createNewBuyer(ConstantsBuyers.EMPTY, ConstantsBuyers.EMPTY, ConstantsBuyers.EMPTY);
		String msg = screenPerformBuyer.getErrorNewBuyer();

		assertEquals(ConstantsBuyers.ERROR_OPERATOR_IS_MANDATORY, msg);
	}

	/**
	 * @testLinkId T-GPRI-2723
	 * @summary Without selecting only Operator in the window "New Buyer"
	 */
	@Test
	public void test_1875_WithoutSelectingOnlyOperatorInTheWindowNewBuyer() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.createNewBuyer(ConstantsBuyers.RANDOM, ConstantsBuyers.EMPTY, ConstantsBuyers.EMPTY);
		String msg = screenPerformBuyer.getErrorNewBuyer();

		assertEquals(ConstantsBuyers.ERROR_COUNTRY_IS_MANDATORY, msg);
	}

	/**
	 * @testLinkId T-GPRI-2724
	 * @summary With two children buyers with same MCC/MNC
	 */
	@Test
	public void test_1876_WithTwoChildrenBuyersWithSameMCC_MNC() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_730_30));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addChildBuyer(buyer2);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertFalse(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);

		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);
		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2725
	 * @summary With three children buyers with same MCC/MNC
	 */
	@Test
	public void test_1877_WithThreeChildrenBuyersWithSameMCC_MNC() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_730_30));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer3 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix3 = buyer3.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer3 = oldNickNameOperator + " " + suffix3 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix3);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addChildBuyer(buyer2);
		screenPerformBuyer.addChildBuyer(buyer3);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertFalse(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);

		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_3;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);
		expected = expected.replace("SUFFIX3", suffix3);
		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2726
	 * @summary With two children buyers with same MCC/MNC + GID1
	 */
	@Test
	public void test_1878_WithTwoChildrenBuyersWithSameMCC_MNC_GID1() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_GID1, ConstantsBuyers.VALUE_714_06_11345678));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addChildBuyer(buyer2);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertFalse(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);

		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);
		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2727
	 * @summary With two children buyers with same MCC/MNC + SPN
	 */
	@Test
	public void test_1879_WithTwoChildrenBuyersWithSameMCC_MNC_SPN() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg = "";

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_SPN, ConstantsBuyers.VALUE_714_06_Orange));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!sucessfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
				sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
				assertTrue(sucessfullSaving);
			}
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!sucessfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
				sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
				assertTrue(sucessfullSaving);
			}
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addChildBuyer(buyer2);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!sucessfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer2 = suffix2 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(suffix2);
				sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			}
		}
		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);
		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2728
	 * @summary With two children buyers with same MCC/MNC + IMSI
	 */
	@Test
	public void test_1880_WithTwoChildrenBuyersWithSameMCC_MNC_IMSI() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg = "";

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved
				.add(new AutoProfile(ConstantsBuyers.MCC_MNC_IMSI, ConstantsBuyers.VALUE_113_11_7140x13919191x3));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!sucessfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
				sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
				assertTrue(sucessfullSaving);
			}
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!sucessfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
				sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
				assertTrue(sucessfullSaving);
			}
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addChildBuyer(buyer2);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!sucessfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer2 = suffix2 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(suffix2);
				sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			}
		}
		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);
		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2729
	 * @summary With two children buyers with same MCC/MNC + ICCID
	 */
	@Test
	public void test_1881_WithTwoChildrenBuyersWithSameMCC_MNC_ICCID() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_ICCID, ConstantsBuyers.VALUE_714_06_11345678));

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addChildBuyer(buyer2);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!sucessfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer2 = suffix2 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(suffix2);
				sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			}
		}

		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);
		assertEquals(expected, msg);
	}

	/**
	 * @testLinkId T-GPRI-2730
	 * @summary With two children buyers with same MCC/MNC + GID1 of type MVNO
	 */
	@Test
	public void test_1882_WithTwoChildrenBuyersWithSameMCC_MNC_GID1OfTypeMVNO() throws Exception {
		boolean sucessfullSaving;
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		String msg;

		screenPerformListBuyers.openPage();
		ArrayList<AutoProfile> autoProfilesSaved = new ArrayList<>();
		autoProfilesSaved.add(new AutoProfile(ConstantsBuyers.MCC_MNC_ICCID, ConstantsBuyers.VALUE_714_06_11345678));
		String networkOwnerSaved = ConstantsBuyers.MOBILE_ISRAEL_ISM;

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.checkMVNO();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		screenPerformBuyer.setNetworkOwner(networkOwnerSaved);
		String buyer1 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix1 = buyer1.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer1 = oldNickNameOperator + " " + suffix1 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix1);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}

		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.checkMVNO();
		screenPerformBuyer.fillFieldsChildBuyer(ConstantsBuyers.FIRST, "", autoProfilesSaved,
				Arrays.asList(ConstantsBuyers.FIRST), Arrays.asList(""));
		screenPerformBuyer.setNetworkOwner(networkOwnerSaved);
		String buyer2 = screenPerformBuyer.getBuyerFirstDropdownBuyer();
		String suffix2 = buyer2.split("\\(")[1].replace(")", "");
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
			screenPerformBuyer.closePopupError();
			String oldNickNameOperator = screenPerformBuyer.getNicknameOperator();
			String NickNameCountry = screenPerformBuyer.getNicknameCountry();
			buyer2 = oldNickNameOperator + " " + suffix2 + " " + NickNameCountry;
			screenPerformBuyer.changeNicknameOperator(oldNickNameOperator + " " + suffix2);
			sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			assertTrue(sucessfullSaving);
		}

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.clickOnNewBuyerButton();
		screenPerformBuyer.selectType(ConstantsBuyers.MOTHER);
		screenPerformBuyer.setBuyer(ConstantsBuyers.FIRST);
		screenPerformBuyer.addChildBuyer(buyer1);
		screenPerformBuyer.addChildBuyer(buyer2);
		screenPerformBuyer.addLTM(ConstantsBuyers.AJAY_CHEECKOLI);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		if (!sucessfullSaving) {
			msg = screenPerformBuyer.getErrorMsg(ConstantsBuyers.ERROR);
			if (msg.equals(ConstantsBuyers.ERROR_BUYER_NICKNAME_EXIST)) {
				screenPerformBuyer.closePopupError();
				screenPerformBuyer.moveToPageHeader();
				String NickNameCountry = screenPerformBuyer.getNicknameCountry();
				buyer2 = suffix2 + " " + NickNameCountry;
				screenPerformBuyer.changeNicknameOperator(suffix2);
				sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
			}
		}

		String expected = ConstantsBuyers.WARNING_NEWTWORK_CODE_CONFLICT_2;
		expected = expected.replace("SUFFIX1", suffix1);
		expected = expected.replace("SUFFIX2", suffix2);
		assertEquals(expected, msg);
	}
}
