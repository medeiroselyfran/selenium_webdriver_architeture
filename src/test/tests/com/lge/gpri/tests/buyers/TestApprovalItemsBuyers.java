package com.lge.gpri.tests.buyers;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsCompare;
import com.lge.gpri.helpers.constants.ConstantsItemInspector;
import com.lge.gpri.helpers.constants.ConstantsItemLibrary;
import com.lge.gpri.helpers.constants.ConstantsUserManagement;
import com.lge.gpri.screens.SectionPerformMenu;
import com.lge.gpri.screens.buyers.ScreenPerformBuyer;
import com.lge.gpri.screens.buyers.ScreenPerformBuyerApproval;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;
import com.lge.gpri.screens.itemLibrary.ScreenItemLibrary;
import com.lge.gpri.screens.usermanagement.ScreenUsersManagement;
import com.lge.gpri.tests.AbstractTestScenary;

/**
 * Test Scenery class.
 * 
 * <p>
 * Description: This class must contains the tests for the scenery Approval
 * Items Buyers.
 * 
 * @author Gabriel Costa do Nascimento
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestApprovalItemsBuyers extends AbstractTestScenary {

	/**
	 * @testLinkId T-GPRI-2738
	 * @summary Configure GTAM of Buyer
	 */
	@Test
	public void test_1890_ConfigureGTAMOfBuyer() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		ArrayList<String> gtams;
		boolean resultOk = false;

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyer.clickOnGTAMTab();
		screenPerformBuyer.removeAllGTAMs();
		screenPerformBuyer.addGTAM(ConstantsBuyers.HAEYEON_PARK);
		boolean sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyer.clickOnGTAMTab();
		gtams = screenPerformBuyer.getGTAMs();
		String split = gtams.get(0).split("\\(")[0];
		if (split.contains(ConstantsBuyers.HAEYEON_PARK)) {
			resultOk = true;
		}
		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2739
	 * @summary Arrive in status Waiting GTAM for Edit
	 */
	@Test
	public void test_1891_ArriveInStatusWaitingGTAMForEdit() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		boolean resultOk = false;

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyer.removeAllAutoProfiles();
		screenPerformBuyer.addAutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_799_99);
		screenPerformBuyer.addAutoProfile(ConstantsBuyers.MCC_MNC, ConstantsBuyers.VALUE_587_965);
		boolean sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		screenPerformListBuyers.clickOnEditBuyer(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyer.removeAutoProfile(ConstantsBuyers.VALUE_799_99);
		sucessfullSaving = screenPerformBuyer.clickOnSaveBuyer();
		assertTrue(sucessfullSaving);
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			resultOk = true;
		}

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2740
	 * @summary Arrive in status Waiting GTAM for Edit Profile
	 */
	@Test
	public void test_1892_ArriveInStatusWaitingGTAMForEditProfile() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		boolean resultOk = false;

		screenPerformEditBuyerProfile.preconditionItem(ConstantsBuyers.SUFFIX_BOI, ConstantsCompare.HOME_NETWORK_1,
				ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
			screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
			screenPerformBuyerApproval.clickOnApproveAll();
			screenPerformBuyerApproval.clickOnSaveButton();
			screenPerformBuyerApproval.clickOnConfirmSave();
		}
		screenPerformListBuyers.clickOnEditProfile(ConstantsBuyers.SUFFIX_BOI);
		screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1,
				ConstantsBuyers.HOME_NETWORK_724_99);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
				ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			resultOk = true;
		}

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2741
	 * @summary Log with GTAM
	 */
	@Test
	public void test_1893_LogWithGTAM() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		SectionPerformMenu sectionPerformMenu = new SectionPerformMenu(getDriver());

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.GTAM);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.HAEYEON_PARK, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsBuyers.HAEYEON_PARK);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.HAEYEON_PARK, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsBuyers.HAEYEON_PARK);
		screenUsersManagement.removeAllRolesExcept(ConstantsUserManagement.GTAM);
		screenUsersManagement.clicOnLoginAs();
		sectionPerformMenu.closeNewReleaseInfo();
		sectionPerformMenu.clickOnExpandSideMenuButton();
		String user = sectionPerformMenu.getUserLogged();
		boolean resultOk = false;
		if (user.contains(ConstantsBuyers.HAEYEON_PARK)) {
			resultOk = true;
		}
		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2742
	 * @summary Approve item with status Waiting GTAM with user GTAM of buyer
	 */
	@Test
	public void test_1894_ApproveItemWithStatusWaitingGTAMWithUserGTAMOfBuyer() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		boolean resultOk = true;

		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.HOME_NETWORK_1,
				ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);

		test_1890_ConfigureGTAMOfBuyer();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.UNDER_EDITION)) {
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		} else if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsBuyers.SUFFIX_BOI);
			String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
			if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
				item = ConstantsBuyers.HOME_NETWORK_724_32;
			} else {
				item = ConstantsBuyers.HOME_NETWORK_724_99;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		}

		test_1893_LogWithGTAM();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
		status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL) || status.contains(ConstantsBuyers.GTAM)) {
			resultOk = false;
		}

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2743
	 * @summary Approve item with status Waiting GTAM with user GTAM of another
	 *          buyer
	 */
	@Test
	public void test_1895_ApproveItemWithStatusWaitingGTAMWithUserGTAMOfAnotherBuyer() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = true;

		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.HOME_NETWORK_1,
				ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);

		test_1890_ConfigureGTAMOfBuyer();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.UNDER_EDITION)) {
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		} else if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsBuyers.SUFFIX_BOI);
			String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
			if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
				item = ConstantsBuyers.HOME_NETWORK_724_32;
			} else {
				item = ConstantsBuyers.HOME_NETWORK_724_99;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		}

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.GTAM);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.JIHYEON_KIM, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsBuyers.JIHYEON_KIM);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.JIHYEON_KIM, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsBuyers.JIHYEON_KIM);
		screenUsersManagement.removeAllRolesExcept(ConstantsUserManagement.GTAM);
		screenUsersManagement.clicOnLoginAs();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		resultOk = screenPerformListBuyers.isStatusPendingApprovalDisabled(ConstantsBuyers.SUFFIX_BOI);

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2744
	 * @summary Approve item with status Waiting GTAM with user Local Master of
	 *          buyer
	 */
	@Test
	public void test_1896_ApproveItemWithStatusWaitingGTAMWithUserLocalMasterOfBuyer() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = true;

		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.HOME_NETWORK_1,
				ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);

		test_1890_ConfigureGTAMOfBuyer();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.UNDER_EDITION)) {
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		} else if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsBuyers.SUFFIX_BOI);
			String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
			if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
				item = ConstantsBuyers.HOME_NETWORK_724_32;
			} else {
				item = ConstantsBuyers.HOME_NETWORK_724_99;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		}

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.LOCAL_MASTER);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.EVAN_KIM, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsBuyers.EVAN_KIM);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.EVAN_KIM, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsBuyers.EVAN_KIM);
		boolean addSuccess = screenUsersManagement.addRegionOnLocalMaster(ConstantsBuyers.LATIN_AMERICA);
		if (!addSuccess) {
			screenUsersManagement.removeRegionOnLocalMaster(ConstantsBuyers.LATIN_AMERICA);
			screenUsersManagement.confirmRemove();
			screenUsersManagement.addRegionOnLocalMaster(ConstantsBuyers.LATIN_AMERICA);
		}
		screenUsersManagement.removeAllRolesExcept(ConstantsUserManagement.LOCAL_MASTER);
		screenUsersManagement.clicOnLoginAs();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
		status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (!status.contains(ConstantsBuyers.VALIDATED)) {
			resultOk = false;
		}

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2745
	 * @summary Approve item with status Waiting GTAM with user Global Master of
	 *          buyer
	 */
	@Test
	public void test_1897_ApproveItemWithStatusWaitingGTAMWithUserGlobalMasterOfBuyer() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = true;

		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.HOME_NETWORK_1,
				ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);

		test_1890_ConfigureGTAMOfBuyer();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.UNDER_EDITION)) {
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		} else if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsBuyers.SUFFIX_BOI);
			String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
			if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
				item = ConstantsBuyers.HOME_NETWORK_724_32;
			} else {
				item = ConstantsBuyers.HOME_NETWORK_724_99;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		}

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.GLOBAL_MASTER);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.CARLOS_TONETO, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsBuyers.CARLOS_TONETO);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.CARLOS_TONETO, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsBuyers.CARLOS_TONETO);
		screenUsersManagement.removeAllRolesExcept(ConstantsUserManagement.GLOBAL_MASTER);
		screenUsersManagement.clicOnLoginAs();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
		status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (!status.contains(ConstantsBuyers.VALIDATED)) {
			resultOk = false;
		}

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2746
	 * @summary Associate Item Reviewer with Region and Item
	 */
	@Test
	public void test_1898_AssociateItemReviewerWithRegionAndItem() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = true;

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.ITEM_REVIEWER);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.ABDUL_ANSARI, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsBuyers.ABDUL_ANSARI);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.ABDUL_ANSARI, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsBuyers.ABDUL_ANSARI);
		screenUsersManagement.removeAllRolesExcept(ConstantsUserManagement.ITEM_REVIEWER);
		boolean addSuccess = screenUsersManagement.addRegionOnItemReviewer(ConstantsBuyers.LATIN_AMERICA);
		if (!addSuccess) {
			screenUsersManagement.removeRegionOnItemReviewer(ConstantsBuyers.LATIN_AMERICA);
			screenUsersManagement.confirmRemoveRegion();
			screenUsersManagement.addRegionOnItemReviewer(ConstantsBuyers.LATIN_AMERICA);
		}
		addSuccess = screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_HOME_NETWORK);
		if (!addSuccess) {
			screenUsersManagement.removeItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_HOME_NETWORK);
			screenUsersManagement.confirmRemoveItem();
			screenUsersManagement.addItemOnItemReviewer(ConstantsItemInspector.PATH_ITEM_HOME_NETWORK);
		}

		ArrayList<String> regions = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.ITEM_REVIEWER, 1);
		if (!regions.contains(ConstantsBuyers.LATIN_AMERICA)) {
			resultOk = false;
		}
		ArrayList<String> items = screenUsersManagement.getItemsOfItemReviewer();
		if (!items.contains("[Item] " + ConstantsItemInspector.PATH_ITEM_HOME_NETWORK)) {
			resultOk = false;
		}

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2747
	 * @summary Associate RM with Region
	 */
	@Test
	public void test_1899_AssociateRMWithRegion() throws Exception {
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = true;

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.RM);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.GC_KIM, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsBuyers.GC_KIM);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.GC_KIM, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsBuyers.GC_KIM);
		screenUsersManagement.removeAllRolesExcept(ConstantsUserManagement.RM);
		boolean addSuccess = screenUsersManagement.addRegionOnRM(ConstantsBuyers.LATIN_AMERICA);
		if (!addSuccess) {
			screenUsersManagement.removeRegionOnRM(ConstantsBuyers.LATIN_AMERICA);
			screenUsersManagement.confirmRemoveRegion();
			screenUsersManagement.addRegionOnRM(ConstantsBuyers.LATIN_AMERICA);
		}

		ArrayList<String> regions = screenUsersManagement.getItemsOfRole(ConstantsUserManagement.RM, 1);
		if (!regions.contains(ConstantsBuyers.LATIN_AMERICA)) {
			resultOk = false;
		}

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2748
	 * @summary Approve item with status Waiting GTAM with user Item Reviewer
	 */
	@Test
	public void test_1900_ApproveItemWithStatusWaitingGTAMwithUserItemReviewer() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = true;

		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.HOME_NETWORK_1,
				ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);

		test_1890_ConfigureGTAMOfBuyer();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.UNDER_EDITION)) {
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		} else if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsBuyers.SUFFIX_BOI);
			String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
			if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
				item = ConstantsBuyers.HOME_NETWORK_724_32;
			} else {
				item = ConstantsBuyers.HOME_NETWORK_724_99;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		}

		test_1898_AssociateItemReviewerWithRegionAndItem();
		screenUsersManagement.clicOnLoginAs();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		resultOk = screenPerformBuyerApproval.clickOnConfirmSave();

		assertFalse(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2749
	 * @summary Approve item with status Waiting GTAM with user RM
	 */
	@Test
	public void test_1901_ApproveItemWithStatusWaitingGTAMwithUserRM() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = true;

		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.HOME_NETWORK_1,
				ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);

		test_1890_ConfigureGTAMOfBuyer();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.UNDER_EDITION)) {
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		} else if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsBuyers.SUFFIX_BOI);
			String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
			if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
				item = ConstantsBuyers.HOME_NETWORK_724_32;
			} else {
				item = ConstantsBuyers.HOME_NETWORK_724_99;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		}

		test_1899_AssociateRMWithRegion();
		screenUsersManagement.clicOnLoginAs();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		resultOk = screenPerformBuyerApproval.clickOnConfirmSave();

		assertFalse(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2750
	 * @summary Approve item with status Waiting GTAM with user GTAM with user Item
	 *          Reviewer
	 */
	@Test
	public void test_1902_ApproveItemWithStatusWaitingGTAMWithUserGTAMAndUserItemReviewer() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = true;

		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.HOME_NETWORK_1,
				ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);

		test_1890_ConfigureGTAMOfBuyer();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.UNDER_EDITION)) {
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		} else if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsBuyers.SUFFIX_BOI);
			String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
			if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
				item = ConstantsBuyers.HOME_NETWORK_724_32;
			} else {
				item = ConstantsBuyers.HOME_NETWORK_724_99;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		}

		test_1898_AssociateItemReviewerWithRegionAndItem();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.GTAM);
		screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.ABDUL_ANSARI, profiles);
		boolean addSuccess = screenUsersManagement.addModelOnGTAM(ConstantsBuyers.OI_BRAZIL_BOI);
		if (!addSuccess) {
			screenUsersManagement.removeModelOnGTAM(ConstantsBuyers.OI_BRAZIL_BOI);
			screenUsersManagement.confirmRemove();
			screenUsersManagement.addModelOnGTAM(ConstantsBuyers.OI_BRAZIL_BOI);
		}
		screenUsersManagement.clicOnLoginAs();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
		status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (!status.contains(ConstantsBuyers.VALIDATED)) {
			resultOk = false;
		}

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2751
	 * @summary Approve item with status Waiting GTAM with user GTAM with user RM
	 */
	@Test
	public void test_1903_ApproveItemWithStatusWaitingGTAMWithUserGTAMwithGTAMAndUserRM() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = true;

		screenPerformEditBuyerProfile.preconditionItem(ConstantsCompare.SUFFIX_BOI, ConstantsCompare.HOME_NETWORK_1,
				ConstantsCompare.HOME_NETWORK_724_32, ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, false);

		test_1890_ConfigureGTAMOfBuyer();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.UNDER_EDITION)) {
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		} else if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsBuyers.SUFFIX_BOI);
			String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
			if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
				item = ConstantsBuyers.HOME_NETWORK_724_32;
			} else {
				item = ConstantsBuyers.HOME_NETWORK_724_99;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		}

		test_1899_AssociateRMWithRegion();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.GTAM);
		screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.GC_KIM, profiles);
		boolean addSuccess = screenUsersManagement.addModelOnGTAM(ConstantsBuyers.OI_BRAZIL_BOI);
		if (!addSuccess) {
			screenUsersManagement.removeModelOnGTAM(ConstantsBuyers.OI_BRAZIL_BOI);
			screenUsersManagement.confirmRemove();
			screenUsersManagement.addModelOnGTAM(ConstantsBuyers.SUFFIX_BOI);
		}
		screenUsersManagement.clicOnLoginAs();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
		status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL) || status.contains(ConstantsBuyers.GTAM)) {
			resultOk = false;
		}

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2752
	 * @summary Approve item with status 1 pending approval with user Item Reviewer
	 */
	@Test
	public void test_1904_ApprovItemWithStatusOnePendingApprovalWithUserItemReviewer() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		boolean resultOk = true;

		test_1894_ApproveItemWithStatusWaitingGTAMWithUserGTAMOfBuyer();

		screenUsersManagement.openPage();
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.clicOnLoginAs();
		test_1898_AssociateItemReviewerWithRegionAndItem();
		screenUsersManagement.clicOnLoginAs();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (!status.contains(ConstantsBuyers.VALIDATED)) {
			resultOk = false;
		}

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2753
	 * @summary Approve item with status 1 pending approval with user RM when item
	 *          not must have Reviewer
	 */
	@Test
	public void test_1905_ApprovItemWithStatusOnePendingApprovalWithUserRMWhenItemNotMustHaveReviewer()
			throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		ScreenItemLibrary screenItemLibrary = new ScreenItemLibrary(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultOk = true;

		screenItemLibrary.openPage();
		boolean exists = screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_ITEM_TO_APPROVAL);
		if (exists) {
			screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_ITEM_TO_APPROVAL);
			screenItemLibrary.selectOptionRightClick(ConstantsItemLibrary.DELETE);
			screenItemLibrary.confirmDelete();
		}
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_NETWORK);
		screenItemLibrary.selectOptionRightClick(ConstantsItemLibrary.NEW_ITEM);
		screenItemLibrary.renameItem(ConstantsItemLibrary.ITEM_TO_APPROVAL);

		screenPerformEditBuyerProfile.preconditionItem(ConstantsBuyers.SUFFIX_BOI,
				ConstantsItemLibrary.ITEM_TO_APPROVAL, ConstantsItemLibrary.ITEM_TO_APPROVAL_123,
				ConstantsItemLibrary.PATH_ITEM_TO_APPROVAL, false);

		test_1890_ConfigureGTAMOfBuyer();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.UNDER_EDITION)) {
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		} else if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsBuyers.SUFFIX_BOI);
			String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsItemLibrary.ITEM_TO_APPROVAL);
			if (item.equals(ConstantsItemLibrary.ITEM_TO_APPROVAL_123)) {
				item = ConstantsItemLibrary.ITEM_TO_APPROVAL_321;
			} else {
				item = ConstantsItemLibrary.ITEM_TO_APPROVAL_123;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsItemLibrary.ITEM_TO_APPROVAL, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		}
		test_1893_LogWithGTAM();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		screenUsersManagement.openPage();
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.clicOnLoginAs();
		test_1899_AssociateRMWithRegion();
		screenUsersManagement.clicOnLoginAs();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
		status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (!status.contains(ConstantsBuyers.VALIDATED)) {
			resultOk = false;
		}

		assertTrue(resultOk);
	}

	/**
	 * @testLinkId T-GPRI-2754
	 * @summary Approve item with status 1 pending approval with user Item Reviewer
	 *          only the region
	 */
	@Test
	public void test_1906_ApprovItemWithStatusOnePendingApprovalWithUserItemReviewerOnlyTheRegion() throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());
		ScreenUsersManagement screenUsersManagement = new ScreenUsersManagement(getDriver());
		ScreenItemLibrary screenItemLibrary = new ScreenItemLibrary(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		boolean resultOk = true;

		screenItemLibrary.openPage();
		boolean exists = screenItemLibrary.selectItemInTree(ConstantsItemLibrary.PATH_ITEM_TO_APPROVAL);
		if (exists) {
			screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_ITEM_TO_APPROVAL);
			screenItemLibrary.selectOptionRightClick(ConstantsItemLibrary.DELETE);
			screenItemLibrary.confirmDelete();
		}
		screenItemLibrary.clickRightOnItem(ConstantsItemLibrary.PATH_NETWORK);
		screenItemLibrary.selectOptionRightClick(ConstantsItemLibrary.NEW_ITEM);
		screenItemLibrary.renameItem(ConstantsItemLibrary.ITEM_TO_APPROVAL);

		screenPerformEditBuyerProfile.preconditionItem(ConstantsBuyers.SUFFIX_BOI,
				ConstantsItemLibrary.ITEM_TO_APPROVAL, ConstantsItemLibrary.ITEM_TO_APPROVAL_123,
				ConstantsItemLibrary.PATH_ITEM_TO_APPROVAL, false);

		test_1890_ConfigureGTAMOfBuyer();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
		String status = screenPerformListBuyers.getStatusApproval(ConstantsBuyers.SUFFIX_BOI);
		if (status.contains(ConstantsBuyers.UNDER_EDITION)) {
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		} else if (!status.contains(ConstantsBuyers.READY_FOR_APPROVAL)) {
			screenPerformListBuyers.clickOnEditProfile(ConstantsBuyers.SUFFIX_BOI);
			String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsItemLibrary.ITEM_TO_APPROVAL);
			if (item.equals(ConstantsItemLibrary.ITEM_TO_APPROVAL_123)) {
				item = ConstantsItemLibrary.ITEM_TO_APPROVAL_321;
			} else {
				item = ConstantsItemLibrary.ITEM_TO_APPROVAL_123;
			}
			screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsItemLibrary.ITEM_TO_APPROVAL, item);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
					ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			screenPerformListBuyers.clickOnStatusChangeItem(ConstantsBuyers.SUFFIX_BOI);
		}
		test_1893_LogWithGTAM();
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();

		screenUsersManagement.openPage();
		screenUsersManagement.searchAndSelectUser(ConstantsUserManagement.YARA_MARTINS);
		screenUsersManagement.clicOnLoginAs();

		screenUsersManagement.openPage();
		List<String> profiles = new ArrayList<String>();
		profiles.add(ConstantsUserManagement.ITEM_REVIEWER);
		String msg = screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.ABDUL_ANSARI, profiles);
		screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		if (msg.equals(ConstantsUserManagement.WARNING_BLOCKED)) {
			screenUsersManagement.clickOnBlockedUsers();
			screenUsersManagement.checkUserOnListBlock(ConstantsBuyers.ABDUL_ANSARI);
			screenUsersManagement.clickOnUnblockSelectedUsers();
			screenUsersManagement.clickOnCloseBlockedUsers();
			screenUsersManagement.associateUserToProfileGetMsg(ConstantsBuyers.ABDUL_ANSARI, profiles);
			screenUsersManagement.closeErrorMsg(ConstantsBuyers.ERROR);
		}
		screenUsersManagement.searchAndSelectUser(ConstantsBuyers.ABDUL_ANSARI);
		screenUsersManagement.removeAllRolesExcept(ConstantsUserManagement.ITEM_REVIEWER);
		boolean addSuccess = screenUsersManagement.addRegionOnItemReviewer(ConstantsBuyers.LATIN_AMERICA);
		if (!addSuccess) {
			screenUsersManagement.removeRegionOnItemReviewer(ConstantsBuyers.LATIN_AMERICA);
			screenUsersManagement.confirmRemoveRegion();
			screenUsersManagement.addRegionOnItemReviewer(ConstantsBuyers.LATIN_AMERICA);
		}
		screenUsersManagement.removeAllItemsOnItemReviewer();
		screenUsersManagement.clicOnLoginAs();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(ConstantsBuyers.SUFFIX_BOI);
		screenPerformListBuyers.clickOnStatusPendingApproval(ConstantsBuyers.SUFFIX_BOI);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
		resultOk = screenPerformBuyerApproval.clickOnConfirmSave();

		assertFalse(resultOk);
	}
}
