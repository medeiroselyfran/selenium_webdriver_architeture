package com.lge.gpri.helpers.constants;


/**
* Constants class.
* 
* <p>Description: This class provides the constants needed for the tests execution.
*  
* @author AUTHOR NAME (can exist more than one)
* 
*/

public class ConstantsItemInspector {

	/**
	 * Model LGH815
	 */
	public static final String LGH815 = "LGH815";
	
	/**
	 * Android Version
	 */
	public static final String ANDROID_VERSION = "Android Version";

	/**
	 * Item Library Auto Commit
	 */
	public static final String AUTO_COMMIT = "Auto Commit";
	
	/**
	 * Profile type NVG/PVG
	 */
	public static final String NVG_PVG = "NVG/PVG";
	
	/**
	 * User alberto.decaceres
	 */
	public static final String ALBERTO_DECACERES = "alberto.decaceres";
	
	/**
	 * User abhai.singh
	 */
	public static final String ABHAI_SINGH = "abhai.singh";
	
	/**
	 * User donato.antonucci GTM Complete
	 */
	public static final String DONATO_ANTONUCCI_COMPLETE = "donato.antonucci";

	/**
	 * Region Europe
	 */
	public static final String EUROPE = "EUROPE";
	
	/**
	 * Region Canada
	 */
	public static final String CANADA = "CANADA";
	
	/**
	 * Item Name
	 */
	public static final String NAME = "Name";
	
	/**
	 * String Home Network 
	 */
	public static final String HOME_NETWORK = "Home Network";

	/**
	 * Model LGP970
	 */
	public static final String LGP970 = "LGP970"; 
	
	/**
	 * Operator 012Mobile
	 */
	public static final String OPERATOR_012MOBILE = "012Mobile";
	
	/**
	 * Type Field Select
	 */
	public static final String TYPE_FIELD_SELECT = "btn-group bootstrap-select show-tick criteria-select";
	
	/**
	 * Type Field Text Normal
	 */
	public static final String TYPE_FIELD_TEXT_NORMAL = "criteria-text form-control";
	
	/**
	 * Type Field Select Multi
	 */
	public static final String TYPE_FIELD_SELECT_MULTI = "btn-group bootstrap-select show-tick criteria-select-multi";
	
	/**
	 * Type Field Select Multi Groups
	 */
	public static final String TYPE_FIELD_SELECT_MULTI_GROUPS = "btn-group bootstrap-select show-tick criteria-select-multi with-groups";
	
	/**
	 * Country Brazil
	 */
	public static final String BRAZIL = "Brazil";
	
	/**
	 * Filter Auto Commit
	 */
	public static final String FILTER_AUTO_COMMIT = "Auto Commit";
	
	/**
	 * Filter Default hidden on Mother
	 */
	public static final String FILTER_DEFAULT_HIDDEN_ON_MOTHER = "Default hidden on Mother";
	
	/**
	 * Filter Default Hide on Mother
	 */
	public static final String FILTER_DEFAULT_HIDE_ON_MOTHER = "Default Hide on Mother";
	
	/**
	 * Filter Default Hide On Mother
	 */
	public static final String DEFAULT_HIDE_ON_MOTHER = "Default Hide On Mother";
	
	/**
	 * Filter Mandatory
	 */
	public static final String FILTER_MANDATORY = "Mandatory";
	
	/**
	 * Filter Mandatory for Buyer Child
	 */
	public static final String FILTER_MANDATORY_FOR_BUYER_CHILD = "Mandatory for Buyer Child";
	
	/**
	 * Filter Multiple
	 */
	public static final String FILTER_MULTIPLE = "Multiple";
	
	/**
	 * Filter Name
	 */
	public static final String FILTER_NAME = "Name";
	
	/**
	 * Filter SmartCA Types
	 */
	public static final String FILTER_SMARTCA_TYPES = "SmartCA Types";
	
	/**
	 * Filter Tag Types
	 */
	public static final String FILTER_TAG_TYPES = "Tag Types";
	
	/**
	 * Filter Tag Values
	 */
	public static final String FILTER_TAG_VALUES = "Tag Values";
	
	/**
	 * Filter Type
	 */
	public static final String FILTER_TYPE = "Type";
	
	/**
	 * Filter Warnings
	 */
	public static final String FILTER_WARNINGS = "Warnings";
	
	/**
	 * Filter Has Links
	 */
	public static final String FILTER_HAS_LINK = "Has Links";

	/**
	 * Path to item inspector iaAPN ProtocolType
	 */
	public static final String PATH_IAAPN_PROTOCOL_TYPE = "Parameters::Network::iaAPN ProtocolType";
	
	/**
	 * item iaAPN ProtocolType
	 */
	public static final String IAAPN_PROTOCOL_TYPE = "iaAPN ProtocolType";

	/**
	 * Path item Name
	 */
	public static final String PATH_ITEM_NAME = "Parameters::Network::Name";
	
	/**
	 * Path item Home Network
	 */
	public static final String PATH_ITEM_HOME_NETWORK = "Parameters::Network::Home Network";
	
	/**
	 * Path item LTE Mode
	 */
	public static final String PATH_ITEM_LTE_MODE = "Parameters::Network::LTE Mode";
	
	/**
	 * Item LTE Mode
	 */
	public static final String ITEM_LTE_MODE = "LTE Mode";
	
	/*
	/**
	 * Parameter Yes of Filters in Item Inspector
	 */
	public static final String YES = "Yes";

	/**
	 * Parameter No of Filters in Item Inspector
	 */
	public static final String NO = "No";

	/**
	 * Parameter Home
	 */
	public static final String HOME = "Home";

	/**
	 * Parameter Customizer
	 */
	public static final String CUSTOMIZER = "Customizer";

	/**
	 * Parameter Resource
	 */
	public static final String RESOURCE = "Resource";

	/**
	 * Parameter COTA
	 */
	public static final String COTA = "COTA";

	/**
	 * Message Alert Item Inspector
	 */
	public static final String ALERT_ITEM_INSPECTOR = "Please, select at least one item and at least one option among Countries, Operatores, Models and Managers";

	/**
	 * Search BLC valid
	 */
	public static final String BLC = "BLC";
	
	/**
	 * Search BLCG invalid
	 */
	public static final String BLCG = "BLCG";
	
	/**
	 * URL
	 */
	public static final String URL = "http://136.166.96.136:8204/Views/ItemInspector/Default.aspx";
}
