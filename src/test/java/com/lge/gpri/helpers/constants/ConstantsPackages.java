package com.lge.gpri.helpers.constants;

/**
 * Constants class.
 * 
 * <p>
 * Description: This class provides the constants needed for the tests
 * execution.
 * 
 * @author AUTHOR NAME (can exist more than one)
 * 
 */

public class ConstantsPackages {

	/**
	 * Button Create Package
	 */
	public static final String BUTTON_CREATE_PACKAGE = "btnCreatePackage";

	/**
	 * Button Edit Package
	 */
	public static final String BUTTON_EDIT_PACKAGE = "btnEditPackage";

	/**
	 * Buyer Oi Brazil (BOI)
	 */
	public static final String OI_BRAZIL_BOI = "Oi Brazil (BOI)";

	/**
	 * Buyer Airtel India (ART)
	 */
	public static final String AIRTEL_INDIA_ART = "Airtel India (ART)";

	/**
	 * Buyer Lg Default Values Germany
	 */
	public static final String LG_DEFAULT_VALUES_GERMANY_LGD = "Lg Default Values Germany (LGD)";

	/**
	 * Buyer suffix (BOI)
	 */
	public static final String SUFFIX_BOI = "BOI";

	/**
	 * Buyer Tim Brazil (BTM)
	 */
	public static final String TIM_BRAZIL_BTM = "Tim Brazil (BTM)";

	/**
	 * Buyer OPEN Europe (EUU)
	 */
	public static final String OPEN_EUROPE_EUU = "OPEN Europe (EUU)";

	/**
	 * Buyer Open Europe
	 */
	public static final String NEXTEL_BRAZIL = "Nextel Brazil (BNT)";

	/**
	 * Name Last Package
	 */
	public static final String TESTF_8 = "testf 8";

	/**
	 * First Buyer
	 */
	public static final String FIRST = "FIRST";

	/**
	 * Param Second
	 */
	public static final String SECOND = "SECOND";

	/**
	 * Buyer TLF group Europe (6TL)
	 */
	public static final String TLF_GROUP_EUROPE_6TL = "TLF group Europe (6TL)";

	/**
	 * Buyer Open Unified Latin America (SCA)
	 */
	public static final String OPEN_UNIFIED_LATIN_AMERICA_SCA = "Open Unified Latin America (SCA)";

	/**
	 * Buyer Claro Unified Latin America (AMX)
	 */
	public static final String CLARO_UNIFIED_LATIN_AMERICA_AMX = "Claro Unified Latin America (AMX)";

	/**
	 * Buyer Claro Colombia (CMC)
	 */
	public static final String CLARO_COLOMBIA_CMC = "Claro Colombia (CMC)";

	/**
	 * Buyer Suffix (AMX)
	 */
	public static final String SUFFIX_AMX = "AMX";

	/**
	 * Buyer Claro Argentina (CTI)
	 */
	public static final String CLARO_ARGENTINA_CLI = "Claro Argentina (CTI)";

	/**
	 * Buyer Claro Chile (CLA)
	 */
	public static final String CLARO_CHILE_CLA = "Claro Chile (CLA)";

	/**
	 * Buyer Claro Brazil (CLR)
	 */
	public static final String CLARO_BRAZIL_CLR = "Claro Brazil (CLR)";

	/**
	 * Buyer OPEN Israel (ISR)
	 */
	public static final String OPEN_ISRAEL_ISR = "OPEN Israel (ISR)";

	/**
	 * Model LGH815
	 */
	public static final String LGH815 = "LGH815";

	/**
	 * Android Version 6.0
	 */
	public static final String ANDROID_VERSION_6 = "6.0";

	/**
	 * Android Version 5.0
	 */
	public static final String ANDROID_VERSION_5 = "5.0";

	/**
	 * Android Version 5.1
	 */
	public static final String ANDROID_VERSION_5_1 = "5.1";

	/**
	 * Carrier Aggregation
	 */
	public static final String CARRIER_AGGREGATION = "Carrier Aggregation";

	/**
	 * Grid Row x Column 5 x 5
	 */
	public static final String GRID_ROW_COLUMN_5X5 = "5 x 5";

	/**
	 * Grid Row x Column 5 x 4
	 */
	public static final String GRID_ROW_COLUMN_5X4 = "5 x 4";

	/**
	 * OS Version N
	 */
	public static final String OS_VERSION_N = "N";

	/**
	 * OS Version M
	 */
	public static final String OS_VERSION_M = "M";

	/**
	 * OS Version O
	 */
	public static final String OS_VERSION_O = "O";

	/**
	 * OS Version KitKat
	 */
	public static final String OS_VERSION_KITKAT = "KitKat";

	/**
	 * OS Version Lollipop
	 */
	public static final String OS_VERSION_LOLLIPOP = "Lollipop";

	/**
	 * UI Version UI 4.2
	 */
	public static final String UI_VERSION_4_2 = "UI 4.2";

	/**
	 * UI Version UI 5.1
	 */
	public static final String UI_VERSION_5_1 = "UI 5.1";

	/**
	 * Voice Over None
	 */
	public static final String VOICE_OVER_NONE = "None";

	/**
	 * UI Simulator Default FP Key
	 */
	public static final String UI_SIMULATOR_DEFAULT_FP_KEY = "Default FP Key";

	/**
	 * UI Simulator Default Android GB
	 */
	public static final String UI_SIMULATOR_DEFAULT_ANDROID_GB = "Default Android GB";
	/**
	 * Package Name Test2
	 */
	public static final String PACKAGE_NAME_TEST2 = "[Normal] PKG22-M-VoWiFi-AVISO9-MS1";

	/**
	 * Package Name Test
	 */
	public static final String PACKAGE_NAME_TEST = "Test";
	/**
	 * Create Package Name Test
	 */
	public static final String PACKAGE_NAME_TEST_F_1 = "testf 1";

	/**
	 * Create Package Name Test
	 */
	public static final String PACKAGE_NAME_TEST_F_ = "testf ";
	
	/**
	 * Create Package Name Test
	 */
	public static final String PACKAGE_NAME_TEST_F = "testf";

	/**
	 * Package Name SmartCATest
	 */
	public static final String PACKAGE_NAME_SMARTCATEST = "SmartCATest";

	/**
	 * Package Name TestSmartCA
	 */
	public static final String PACKAGE_NAME_TESTSMARTCA = "TestSmartCA";
	
	/**
	 * Package Name TestSmartCA
	 */
	public static final String PACKAGE_NAME_TESTSMARTCA_ = "TestSmartCA 1";

	/**
	 * Package Name With Double Quote
	 */
	public static final String PACKAGE_NAME_WITH_DOUBLE_QUOTE = "\"Test";

	/**
	 * Package Name With Single Quote
	 */
	public static final String PACKAGE_NAME_WITH_SINGLE_QUOTE = "'Test";

	/**
	 * Package Name With Backslash
	 */
	public static final String PACKAGE_NAME_WITH_BACKSLASH = "\\Test";

	/**
	 * Package Name PKG2-M-MS1
	 */
	public static final String PKG2_M_MS1 = "PKG2-M-MS1";

	/**
	 * Package Name PKG2-M-MS1_ABC
	 */
	public static final String PKG2_M_MS1_ABC = "PKG2-M-MS1_ABC";

	/**
	 * Package Name Test Status
	 */
	public static final String PACKAGE_NAME_TEST_STATUS = "Test Status";

	/**
	 * Package Name Test State Machine
	 */
	public static final String PACKAGE_NAME_TEST_STATE_MACHINE = "Test State Machine";

	/**
	 * Create Reasons Only for test
	 */
	public static final String CREATE_REASONS_ONLY_FOR_TEST = "Only for test";
	/**
	 * Target Date 01/10/2016
	 */
	public static final String TARGET_DATE_01_10_2016 = "01/10/2016";

	/**
	 * Target Date 28/10/2017
	 */
	public static final String TARGET_DATE_28_10_2017 = "28/10/2017";

	/**
	 * Target Date 26/11/2017
	 */
	public static final String TARGET_DATE_30_10_2018 = "30/10/2018";

	/**
	 * Providing Type Auto activation with WIFI
	 */
	public static final String AUTO_ACTIVATION_WITH_WIFI = "Auto activation with WIFI";

	/**
	 * Providing Type Manual activation
	 */
	public static final String MANUAL_ACTIVATION = "Manual activation";

	/**
	 * Open SW Yes
	 */
	public static final String OPEN_SW_YES = "Yes";

	/**
	 * NT Code List "1","FFF,FFF,FFFFFFFF,FFFFFFFF,FF"
	 */
	public static final String NT_CODE_LIST_1_FFF = "\"1\",\"FFF,FFF,FFFFFFFF,FFFFFFFF,FF\"";
	
	/**
	 * SW Version From 55
	 */
	public static final String SW_VERSION_FROM = "55";
	
	/**
	 * SW Version To 2
	 */
	public static final String SW_VERSION_TO = "2";

	/**
	 * NT Code List Maxinum Supported
	 */
	public static final String NT_CODE_MAXIMUN_SUPPORTED = "\"42\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\",\"123,45F,FFFFFFFF,FFFFFFFF,FF\"";

	/**
	 * Package Name Error Message
	 */
	public static final String PACKAGE_NAME_ERROR = "The name cannot have any of the following characters: \" (double quote) or ' (single quote) or \\ (backslash).";

	/**
	 * Package created with success
	 */
	public static final String PACKAGE_CREATED_SUCCESS = "The package was successfully created.";

	/**
	 * No package available
	 */
	public static final String NO_PACKAGE_AVAILABLE = "No packages available for this model and buyer.";
	
	/**
	 * 
	 */
	public static final String NO_PACKAGE_AVAILABLE_COMBO = "No results matched";

	/**
	 * Paginate button to the next elements
	 */
	public static final String PAGINATE_BUTTON_NEXT = "paginate_button next";

	/**
	 * Paginate button to the previous elements
	 */
	public static final String PAGINATE_BUTTON_PREVIOUS = "paginate_button previous";

	/**
	 * Message No permalinks found for this package
	 */
	public static final String NO_PERMALINKS_FOUND = "No permalinks found for this package.";

	/**
	 * Status Under Edition
	 */
	public static final String UNDER_EDITION = "Under Edition";

	/**
	 * Status Commiting
	 */
	public static final String COMMITING = "Commiting";

	/**
	 * Status Commit Error
	 */
	public static final String COMMIT_ERROR = "Commit Error";

	/**
	 * Status Confirmed
	 */
	public static final String CONFIRMED = "Confirmed";

	/**
	 * Branch LG Apps 1 Release Test
	 */
	public static final String LG_APPS_M_RELEASE_TEST = "LG_apps_m_release_test";
	/**
	 * Class check
	 */
	public static final String CLASS_CHECK = "check";
	/**
	 * Grid Row x Column 4 x 4
	 */
	public static final String GRID_ROW_COLUMN_4X4 = "4 x 4";

	/**
	 * Frenquency Band UMTS
	 */
	public static final String FREQUENCY_BAND_UMTS = "umts";

	/**
	 * Path Frenquency Band UMTS
	 */
	public static final String PATH_FREQUENCY_BAND_UMTS = "Parameters::Network::Frequency Bands::UMTS";
	
	/**
	 * Path PLMN DISPLAY NAME
	 */
	public static final String PATH_PLMN_DISPLAY_NAME = "Parameters::Network::PLMN::Display Name";
	
	/**
	 * Path PLMN RAT
	 */
	public static final String PATH_PLMN_MCC = "Parameters::Network::PLMN::MCC";

	/**
	 * Frenquency Band LTE
	 */
	public static final String FREQUENCY_BAND_LTE = "lte";
	

	/**
	 * Path Frenquency Band LTE
	 */
	public static final String PATH_FREQUENCY_BAND_LTE = "Parameters::Network::Frequency Bands::LTE";

	/**
	 * Frenquency Band LTE
	 */
	public static final String FREQUENCY_BAND_GSM = "gsm";
	
	/**
	 * Value 2100
	 */
	public static final String VALUE_2100 = "2100";

	/**
	 * Value 7
	 */
	public static final String VALUE_7 = "7";

	/**
	 * Value 724 90
	 */
	public static final String VALUE_72490 = "724 90";

	/**
	 * Value 724 89
	 */
	public static final String VALUE_724_89 = "724 89";

	/**
	 * Message Only for testing
	 */
	public static final String MESSAGE_ONLY_FOR_TESTING = "Only for testing";
	/**
	 * Package [Normal] Test Statusf1
	 */
	public static final String PACKAGE_NORMAL_TEST_STATUSF1 = "[Normal] Test Statusf1";

	/**
	 * Show 10 results in table of status history screen
	 */
	public static final String DISPLAY10 = "display10";
	/**
	 * Item Library Auto Commit
	 */
	public static final String AUTO_COMMIT = "Auto Commit";

	/**
	 * Item Library Mandatory for Group
	 */
	public static final String MANDATORY_FOR_GROUP = "Mandatory for Group";

	/**
	 * Item Library Auto Commit 2
	 */
	public static final String AUTO_COMMIT_2 = "Auto Commit 2";

	/**
	 * No Result Found for Auto Commit 2
	 */
	public static final String NO_RESULTS_MATCHED_AUTO_COMMIT_2 = "No results matched \"Auto Commit 2\"";

	/**
	 * Enable RCS
	 */
	public static final String ENABLE_RCS = "Enable RCS";

	/**
	 * Element Class
	 */
	public static final String ELEMENT_CLASS = "jstree-anchor jstree-search";

	/**
	 * Page Models Url
	 */
	public static final String PAGE_MODELS_URL = "http://136.166.96.136:8204/Views/Models/";

	/**
	 * PKG17-M-VoWiFi-AVISO6 (CLR LGH815)
	 */
	public static final String PKG17_M_VOWIFI_AVIS06 = "PKG17-M-VoWiFi-AVISO6-MS1";

	/**
	 * test_ism_815
	 */
	public static final String TEST_ISM_815 = "test_ism_815";

	/**
	 * Buyer 019Mobile Israel (ISM)
	 */
	public static final String MOBILE_ISRAEL_ISM = "019Mobile Israel (ISM)";

	/**
	 * Buyer ETB Colombia (ETB)
	 */
	public static final String ETB_COLOMBIA = "ETB Colombia (ETB)";
	/**
	 * Message Package Already Existing
	 */
	public static final String PACKAGE_ALREADY_EXISTING = "Already exists one package with name";
	/**
	 * Buyer MEO Portugal (TMP)
	 */
	public static final String MEO_PORTUGAL = "MEO Portugal (TMP)";
	/**
	 * Buyer WIND Italy (WIN)
	 */
	public static final String WIND_ITALY = "WIND Italy (WIN)";
	/**
	 * Combobox Select a Buyer
	 */
	public static final String SELECT_BUYER = "Select a Buyer...";
	/**
	 * Buyer Usa United Status (USA)
	 */
	public static final String USA_UNITED_STATUS = "Usa United States (USA)";
	/**
	 * Buyer Cellcom Israel (CCO)
	 */
	public static final String CELLCOM_ISRAEL = "Cellcom Israel (CCO)";

	/**
	 * Package VP2 - 07/03/2016
	 */
	public static final String VP2 = "VP2 - 07/03/2016";

	/**
	 * Package NOSU_VP1 - 27/11/2016
	 */
	public static final String NOSU_VP1 = "NOSU_VP1 - 27/11/2016";

	/**
	 * Value Home Network 716 17
	 */
	public static final String HOME_NETWORK_VALUE_716_17 = "716 17";

	/**
	 * Package Name Test More Than One Buyer
	 */
	public static final String PACKAGE_NAME_TEST_MORE_THAN_ONE_BUYER = "Test More Than One Buyer";

	/**
	 * Buyer VODACOM South Africa (VDC)
	 */
	public static final String TATA_INDIA = "Tata India (IDT)";

	/**
	 * Button Close Pop-up
	 */
	public static final String BUTTON_CLOSE_POPUP = "buttonCloseOnCreationPackageError";

	/**
	 * Package Name Error Single Quote / Double Quota Message
	 */
	public static final String WITHOUT_NAME = "";

	/**
	 * NT Code List "1f"
	 */
	public static final String NT_CODE_INVALID_FORMAT = "1f";

	/**
	 * NT Code List Message Error
	 */
	public static final String NT_CODE_MESSAGE_ERROR = "Invalid format. Example: \"1\",\"FFF,FFF,FFFFFFFF,FFFFFFFF,FF\".";

	/**
	 * NT Code Message Maximum Supported
	 */
	public static final String NT_CODE_MESSAGE_MAXIMUN_SUPPORTED = "\"42\" is larger than the maximum supported (35 ~ 41).";

	/**
	 * Model LGH840
	 */
	public static final String LGH840 = "LGH840";

	/**
	 * Package Name Test3
	 */
	public static final String PACKAGE_NAME_TEST3 = "Test3";

	/**
	 * Target Date 26/11/2018
	 */
	public static final String TARGET_DATE_26_11_18 = "26-11-2018";

	/**
	 * Target Date 26/11/18
	 */
	public static final String TARGET_DATE_26_11_18_OTHER_FORMAT = "26-11-18";

	/**
	 * Message name Package Empty
	 */
	public static final Object MESSAGE_FIELD_PACKAGE_EMPTY = "Sorry! Some fields have invalid values. Please fix them and try again.";

	/**
	 * Package Name Duplicate
	 */
	public static final String PACKAGE_NAME_DUPLICATE = "PKG8-N";

	/**
	 * Package Name Double Quote
	 */
	public static final String PACKAGE_NAME_DOBLE_QUOTE = "\"Test";

	/**
	 * Package Name Simple Quote
	 */
	public static final String PACKAGE_NAME_SIMPLE_QUOTE = "\'Test";

	/**
	 * Package Name Backslash
	 */
	public static final String PACKAGE_NAME_BACKSLASH = "Test\\";

	/**
	 * Package Country ShortCut
	 */
	public static final String PACKAGE_COUNTRY_SHORTCUT = "Test";

	/**
	 * Home NetWork 724 31
	 */
	public static final String HOME_NETWORK_724_31 = "724 31";

	/**
	 * Home NetWork 724 32
	 */
	public static final String HOME_NETWORK_724_32 = "724 32";

	/**
	 * LTE 4G
	 */
	public static final String LTE_4G = "4G";

	/**
	 * Validator TimeFormat_Validator
	 */
	public static final String TIME_FORMAT_VALIDATOR = "TimeFormat_Validator";

	/**
	 * TimeFormat 12-hours
	 */
	public static final String TIME_FORMAT_12_HOURS = "12-hours";

	/**
	 * TimeFormat 24-hour
	 */
	public static final String TIME_FORMAT_24_HOUR = "24-hour";

	/**
	 * TimeFormat 24-hours
	 */
	public static final String TIME_FORMAT_24_HOURS = "24-hours";

	/**
	 * Message Value no longer acceptable, please choose another one
	 */
	public static final String VALUE_NO_LONGER_ACCEPTABLE = "Value no longer acceptable, please choose another one";

	/**
	 * Item Home Network
	 */
	public static final String ITEM_HOME_NETWORK = "homeNetwork";

	/**
	 * Parameter All
	 */
	public static final String ALL = "All";

	/**
	 * Parameter All
	 */
	public static final String CONFIRMED_BY = "Confirmed";

	/**
	 * URL
	 */
	public static final String URL = "http://136.166.96.136:8204/Views/Packages/List.aspx#/";

	/**
	 * Normal type
	 */
	public static final String NORMAL = "Normal";

	/**
	 * Smart CA type
	 */
	public static final String SMART_CA = "SmartCA";

	/**
	 * Today param
	 */
	public static final String TODAY = "TODAY";

	/**
	 * Test 43
	 */
	public static final String TEST_43 = "Test43";

	/**
	 * Test 41
	 */
	public static final String TEST_41 = "Test41";

	/**
	 * No permalinks found for this package.
	 */
	public static final Object TXT_NO_PERMALINKS_FOUND = "No permalinks found for this package.";

	/**
	 * Autocommit [TEST]ApkOverlay2_M
	 */
	public static final String AUTOCOMMIT_APK_OVERLAY2_M = "[TEST]ApkOverlay2_M";

	/**
	 * [TEST]GPRI_M
	 */
	public static final String AUTOCOMMIT_GPRI_M = "[TEST]GPRI_M";

	/**
	 * Error on Engine process execution.
	 */
	public static final Object TXT_ERROR_ENGINE = "Error on Engine process execution.";

	/**
	 * Exception caught during execution of fetch command
	 */
	public static final Object TXT_ERROR_COMMITING = "Exception caught during execution of fetch command";

	/**
	 * Validating
	 */
	public static final String VALIDATING = "Validating";

	/**
	 * Building
	 */
	public static final String BUILDING = "Building";

	/**
	 * OS Version:M
	 */
	public static final Object RESTRICTION_OS_M = "OS Version:M";

	/**
	 * UI Version:UI 4.2
	 */
	public static final Object RESTRICTION_UI_4_2_5_0 = "UI Version:UI 4.2,UI 5.0";

	/**
	 * Message for No histories available.
	 */
	public static final String NO_HISTORIES_AVAILABLE = "No histories available.";

	/**
	 * Invalid Search
	 */
	public static final String INVALID_SEARCH = "Confirmy";

	/**
	 * Valid Search
	 */
	public static final String VALID_SEARCH = "Confirm";

	/**
	 * Instagram
	 */
	public static final String INSTAGRAM = "Instagram";

	/**
	 * Facebook
	 */
	public static final String FACEBOOK = "Facebook";

	/**
	 * App Facebook
	 */
	public static final String APP_FACEBOOK = "appFacebook";

	/**
	 * App Instagram
	 */
	public static final String APP_INSTAGRAM = "appInstagram";

	/**
	 * gridColumnApp2
	 */
	public static final String APP_2_COLUMN = "gridColumnApp2";

	/**
	 * gridRowApp2
	 */
	public static final String APP_2_ROW = "gridRowApp2";

	/**
	 * gridRowApp1
	 */
	public static final String APP_1_ROW = "gridRowApp1";

	/**
	 * Warning cannot save package
	 */
	public static final String WARNING_CANNOT_SAVE_PACKAGE = "warningCannotSaveBuyerMsg";

	/**
	 * app1RowEmptyMsg
	 */
	public static final String APP_1_ROW_EMPTY = "app1RowEmptyMsg";

	/**
	 * Message Warning cannot save package
	 */
	public static final String MSG_WARNING_CANNOT_SAVE_PACKAGE = "Cannot save the package. Please check the red marked fields.";

	/**
	 * Message Field Empty
	 */
	public static final String MSG_WARNING_FIELD_EMPTY = "This field is empty";

	/**
	 * Value 1
	 */
	public static final String VALUE_1 = "1";

	/**
	 * Value 2
	 */
	public static final String VALUE_2 = "2";

	/**
	 * Value Empty
	 */
	public static final String EMPTY = "";

	/**
	 * Nothing changed.
	 */
	public static final Object NOTHING_CHANGE = "Nothing changed.";

	/**
	 * Valid name Item in tree
	 */
	public static final String CUSTOM_ICON = "Custom Icon";

	/**
	 * Valid name Item in tree
	 */
	public static final String CUSTOM_LABEL = "Custom Label";

	/**
	 * Valid name Item in tree
	 */
	public static final String HOME_NETWORK = "Home Network";

	/**
	 * Valid name Item in tree
	 */
	public static final String SHOW_PLMN = "Show PLMN";

	/**
	 * Valied name Intem tree
	 */
	public static final String LTE_MODE = "LTE Mode";

	/**
	 * Valid name package
	 */
	public static final String PACKAGE_ROAMING_ICON = "Roaming Icon";

	/**
	 * Valid path network
	 */
	public static final String PATH_HOME_NETWORK = "Parameters::Network::Home Network";

	/**
	 * Valid path network
	 */
	public static final String PATH_SHOW_PLMN = "Parameters::Network::PLMN/SPN::Results::Show PLMN";

	/**
	 * Name button collapse all
	 */
	public static final String BTN_COLLAPSE_ALL = "Collapse All";

	/**
	 * Name button expand all
	 */
	public static final String BTN_EXPAND_ALL = "Expand All";

	/**
	 * Valid name item in tree
	 */
	public static final String BLANK_HOMESCREEN = "Blank Homescreen";

	/**
	 * Path Blank Homescreen
	 */
	public static final String PATH_BLANK_HOMESCREEN = "Customization::Homescreen::Android::Blank Homescreen";

	/**
	 * GSM used in Item
	 */
	public static final String GSM = "GSM";

	/**
	 * GSM value used in Item
	 */
	public static final String GSM_2100 = "2100";

	/**
	 * UMTS used in Item
	 */
	public static final String UMTS = "UMTS";

	/**
	 * UMTS value used in Item
	 */
	public static final String UMTS_2100 = "2100";

	/**
	 * LTE used in Item
	 */
	public static final String LTE = "LTE";

	/**
	 * LTE value used in Item
	 */
	public static final String LTE_7 = "7";

	/**
	 * Customization::Hotseat::Android::Item::Folder
	 */
	public static final String PATH_CUSTOMIZATION_HOTSEAT_ANDROID_ITEM_FOLDER = "Customization::Hotseat::Android::Item::Folder";

	/**
	 * Customization::Hotseat::Android::Folder
	 */
	public static final String PATH_CUSTOMIZATION_HOTSEAT_ANDROID_FOLDER = "Customization::Hotseat::Android::Folder";

	/**
	 * Customization::Hotseat::Android::Folder::Name
	 */
	public static final String PATH_CUSTOMIZATION_HOTSEAT_ANDROID_FOLDER_NAME = "Customization::Hotseat::Android::Folder::Name";

	/**
	 * Customization::Hotseat::Android::Folder::Folder #1::Name
	 */
	public static final String PATH_FOLDER_1_NAME = "Customization::Hotseat::Android::Folder::Folder #1::Name";

	/**
	 * Customization::Hotseat::Android::Folder::Folder #var::Name
	 */
	public static final String PATH_FOLDER_VAR_NAME = "Customization::Hotseat::Android::Folder::Folder #%%s::Name";

	/**
	 * Customization::Hotseat::Android::Folder::Folder #var
	 */
	public static final String PATH_FOLDER_VAR = "Customization::Hotseat::Android::Folder::Folder #%%s";

	/**
	 * Customization::Hotseat::Android::Folder
	 */
	public static final String PATH_FOLDER = "Customization::Hotseat::Android::Folder";

	/**
	 * Customization::Hotseat::Android::Item::Item #1::Folder
	 */
	public static final String PATH_ITEM_1_FOLDER = "Customization::Hotseat::Android::Item::Item #1::Folder";

	/**
	 * Folder Test
	 */
	public static final String FOLDER_TEST = "FolderTest";

	/**
	 * Folder Test 2
	 */
	public static final String FOLDER_TEST_2 = "FolderTest 2";

	/**
	 * Path Parameters::Network::Carrier Aggregation
	 */
	public static final String PATH_CARRIER_AGGREGATION = "Parameters::Network::Carrier Aggregation";

	public static final String VIVO_BRAZIL_VIV = "VIVO Brazil (VIV)";

	/**
	 * Select tab customization
	 */
	public static final String CUSTOMIZATION = "Customization";

	/**
	 * Select tab Parameters
	 */
	public static final String PARAMETERS = "Parameters";
	/**
	 * Normal SmartCA Type
	 */
	public static final String NORMAL_SMARTCA = "Normal SmartCA";

	/**
	 * Small SmartCA Type
	 */
	public static final String SMALL_SMARTCA = "Small SmartCA";
	
	/**
	 * No SmartCA Type
	 */
	public static final String NO_SMARTCA = "No SmartCA";

	/**
	 * control-label error
	 */
	public static final String CONTROL_LABEL_ERROR = "control-label error";
	
	/**
	 * Pop Up Confirm SmartCA
	 */
	public static final String CONFIRM_SMARTCA_POPUP = "Confirm Smart CA";
	
	/**
	 * MCC 724
	 */
	public static final String MCC_724 = "724";
}
