package com.lge.gpri.helpers.constants;


/**
* Constants class.
* 
* <p>Description: This class provides the constants needed for the tests execution.
*  
* @author AUTHOR NAME (can exist more than one)
* 
*/

public class ConstantsItemLibrary {

	/**
	 * Buyer Tim Brazil (BTM)
	 */
	public static final String TIM_BRAZIL_BTM = "Tim Brazil (BTM)";
	
	/**
	 * Buyer Oi Brazil (BOI)
	 */
	public static final String OI_BRAZIL_BOI = "Oi Brazil (BOI)";

	/**
	 * Model LGH815
	 */
	public static final String LGH815 = "LGH815";

	/**
	 * Type Field Select
	 */
	public static final String TYPE_FIELD_SELECT = "btn-group bootstrap-select show-tick criteria-select";
	
	/**
	 * Type Field Text Normal
	 */
	public static final String TYPE_FIELD_TEXT_NORMAL = "criteria-text form-control";
	
	/**
	 * Type Field Select Multi
	 */
	public static final String TYPE_FIELD_SELECT_MULTI = "btn-group bootstrap-select show-tick criteria-select-multi";
	
	/**
	 * Type Field Select Multi Groups
	 */
	public static final String TYPE_FIELD_SELECT_MULTI_GROUPS = "btn-group bootstrap-select show-tick criteria-select-multi with-groups";
	
	/**
	 * Filter Name
	 */
	public static final String FILTER_NAME = "Name";
	
	/**
	 * Filter SmartCA Types
	 */
	public static final String FILTER_SMARTCA_TYPES = "SmartCA Types";
	
	/**
	 * Filter Tag Types
	 */
	public static final String FILTER_TAG_TYPES = "Tag Types";
	
	/**
	 * Filter Tag Values
	 */
	public static final String FILTER_TAG_VALUES = "Tag Values";
	
	/**
	 * Filter Type
	 */
	public static final String FILTER_TYPE = "Type";
	
	/**
	 * Filter Warnings
	 */
	public static final String FILTER_WARNINGS = "Warnings";
	
	/**
	 * Filter Has Links
	 */
	public static final String FILTER_HAS_LINK = "Has Links";

	/**
	 * Path to item inspector iaAPN ProtocolType
	 */
	public static final String PATH_IAAPN_PROTOCOL_TYPE = "Parameters::Network::iaAPN ProtocolType";
	
	/**
	 * Item Roaming Icon
	 */
	public static final String PATH_ROAMING_ICON = "Parameters::Network::Roaming Icon";
	
	/**
	 * Path item Name 0
	 */
	public static final String PATH_ITEM_NAME = "Parameters::Network::Name";
	
	/**
	 * Path item LTE Mode
	 */
	public static final String PATH_ITEM_LTE_MODE = "Parameters::Network::LTE Mode";
	
	/**
	 * Path item itemToApproval
	 */
	public static final String PATH_ITEM_TO_APPROVAL = "Parameters::Network::ItemToApproval";
	
	/**
	 * Item itemToApproval
	 */
	public static final String ITEM_TO_APPROVAL = "ItemToApproval";
	
	/**
	 * Item itemToApproval 123
	 */
	public static final String ITEM_TO_APPROVAL_123 = "123";
	
	/**
	 * Item itemToApproval 321
	 */
	public static final String ITEM_TO_APPROVAL_321 = "321";
	
	/**
	 * Path item Network
	 */
	public static final String PATH_NETWORK= "Parameters::Network";
	
	/**
	 * Item LTE Mode
	 */
	public static final String ITEM_LTE_MODE = "LTE Mode";

	/**
	 * Delete option right click
	 */
	public static final String DELETE = "Delete";
	
	/**
	 * New Item option right click
	 */
	public static final String NEW_ITEM = "New Item";
	
	/**
	 * Date Format
	 */
	public static final String DD_MM_YYYY_HH_MM_SS_A = "dd/MM/yyyy hh:mm:ss a";

}
