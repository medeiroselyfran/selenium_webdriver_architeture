package com.lge.gpri.helpers.constants;


/**
* Constants class.
* 
* <p>Description: This class provides the constants needed for the tests execution.
*  
* @author AUTHOR NAME (can exist more than one)
* 
*/

public class ConstantsBuyers {

	/**
	 * Buyer Suffix 3IR
	 */
	public static final String SUFFIX_3IR = "3IR";
	
	/**
	 * Buyer Suffix LGU
	 */
	public static final String SUFFIX_LGU = "LGU";
	
	/**
	 * Buyer Suffix KOR
	 */
	public static final String SUFFIX_KOR = "KOR";
	
	/**
	 * Buyer Suffix BOI
	 */
	public static final String SUFFIX_BOI = "BOI";
	
	/**
	 * Buyer Suffix 6TL
	 */
	public static final String SUFFIX_6TL = "6TL";
	
	/**
	 * Suffix O2U
	 */
	public static final String SUFFIX_O2U = "O2U";
	
	/**
	 * Suffix O2D
	 */
	public static final String SUFFIX_O2D = "O2D";
	
	/**
	 * Suffix CLR
	 */
	public static final String SUFFIX_CLR = "CLR";
	
	/**
	 * Test
	 */
	public static final String TEST = "Test";
	
	/**
	 * Message Only for testing
	 */
	public static final String MESSAGE_ONLY_FOR_TESTING = "Only for testing";

	/**
	 * String reference to ascending order in columns
	 */
	public static final String ASC_SORT = "sorting_asc";
	
	/**
	 * String reference to order in columns
	 */
	public static final String SORT = "sorting";
	
	/**
	 * Item Library Mandatory for Group
	 */
	public static final String MANDATORY_FOR_GROUP = "Mandatory for Group";
	
	/**
	 * Buyer 019Mobile Israel (ISM)
	 */
	public static final String MOBILE_ISRAEL_ISM = "019Mobile Israel (ISM)";
	
	/**
	 * User alex.rechtman (LGEYK GSM)
	 */
	public static final String ALEX_RECHTMAN_COMPLETE = "alex.rechtman";
	
	/**
	 * User ajay.cheeckoli (LGEGF MC Group)
	 */
	public static final String AJAY_CHEECKOLI_COMPLETE = "ajay.cheeckoli";
	
	/**
	 * User ajay.cheeckoli
	 */
	public static final String AJAY_CHEECKOLI = "ajay.cheeckoli";

	/**
	 * Error A buyer with the same nickname already exists.
	 */
	public static final Object ERROR_BUYER_NICKNAME_EXIST = "A buyer with the same nickname already exists.";
	
	/**
	 * Error This buyer must have at least one network code.
	 */
	public static final Object WARNING_NETWORK_CODE = "This buyer must have at least one network code.";
	
	/**
	 * Error This buyer must have at least one network code.
	 */
	public static final Object WARNING_LTM_USER = "This buyer must have at least one LTM user.";
	
	/**
	 * Error A mother buyer must have at least one child buyer.
	 */
	public static final Object WARNING_CHILDREN = "A mother buyer must have at least one child buyer.";
	
	/**
	 * The set of Buyers [SUFFIX1, SUFFIX2] has network codes that conflicts in this Mother.
	 */
	public static final String WARNING_NEWTWORK_CODE_CONFLICT_2 = "The set of Buyers [SUFFIX1, SUFFIX2] has network codes that conflicts in this Mother.";
	
	/**
	 * The set of Buyers [SUFFIX1, SUFFIX2, SUFFIX3] has network codes that conflicts in this Mother.
	 */
	public static final String WARNING_NEWTWORK_CODE_CONFLICT_3 = "The set of Buyers [SUFFIX1, SUFFIX2, SUFFIX3] has network codes that conflicts in this Mother.";
	
	/**
	 * Parameter All
	 */
	public static final String ALL = "All";
	
	/**
	 * Parameter FIRST
	 */
	public static final String FIRST = "FIRST";
	
	/**
	 * Parameter SECOND
	 */
	public static final String SECOND = "SECOND";

	/**
	 * No LTM users associated with this buyer in buyer creation/edition
	 */
	public static final Object NO_LTM_ASSOCIATED = "No LTM users associated with this buyer.";
	
	/**
	 * No GTAM users associated with this buyer in buyer creation/edition
	 */
	public static final Object NO_GTAM_ASSOCIATED = "No GTAM users associated with this buyer.";
	
	/**
	 * No network codes associated with this buyer in buyer creation/edition
	 */
	public static final Object NO_NETWORK_CODE_ASSOCIATED = "No network codes associated with this buyer.";
	
	/**
	 * No children buyers associated with this buyer creation/edition
	 */
	public static final Object NO_CHILDREN_BUYER_ASSOCIATED = "No children buyers associated with this buyer.";

	/**
	 * MCC/MNC
	 */
	public static final String MCC_MNC = "MCC/MNC";
	
	/**
	 * MCC/MNC+GID1
	 */
	public static final String MCC_MNC_GID1 = "MCC/MNC+GID1";
	
	/**
	 * MCC/MNC+SPN
	 */
	public static final String MCC_MNC_SPN = "MCC/MNC+SPN";
	
	/**
	 * MCC/MNC+IMSI
	 */
	public static final String MCC_MNC_IMSI = "MCC/MNC+IMSI";
	
	/**
	 * MCC/MNC+ICCID
	 */
	public static final String MCC_MNC_ICCID = "MCC/MNC+ICCID";

	/**
	 * Value 730 30
	 */
	public static final String VALUE_730_30 = "730 30";
	
	/**
	 * Value 731 31
	 */
	public static final String VALUE_731_31 = "731 31";
	
	/**
	 * Value 714 06+11345678
	 */
	public static final String VALUE_714_06_11345678 = "714 06+11345678";
	
	/**
	 * Value 714 06+Orange
	 */
	public static final String VALUE_714_06_Orange = "714 06+Orange";
	
	/**
	 * Value 714 06
	 */
	public static final String VALUE_714_06 = "714 06";
	
	/**
	 * Value 799 99
	 */
	public static final String VALUE_799_99 = "799 99";
	
	/**
	 * Value 587 965
	 */
	public static final String VALUE_587_965 = "587 965";
	
	/**
	 * Value 113 11+7140x13919191x3
	 */
	public static final String VALUE_113_11_7140x13919191x3 = "113 11+7140x13919191x3";
	
	/**
	 * Value 123
	 */
	public static final String VALUE_123 = "123";
	

	/**
	 * Value 19
	 */
	public static final String VALUE_19 = "19";
	
	/**
	 * Value 1
	 */
	public static final String VALUE_1 = "1";
	
	/**
	 * Value 2
	 */
	public static final String VALUE_2 = "2";

	/**
	 * Parameter of Global Group Buyer Creation DTAG
	 */
	public static final String DTAG = "DTAG";

	/**
	 * Error
	 */
	public static final String ERROR = "Error";
	
	/**
	 * Network Code Error
	 */
	public static final String ERROR_NETWORK_CODE = "Network Code";
	
	/**
	 * Children Error
	 */
	public static final String ERROR_CHILDREN = "Children";
	
	/**
	 * LTM Error
	 */
	public static final String ERROR_LTM = "LTM";
	
	/**
	 * Option Random
	 */
	public static final String RANDOM = "RANDOM";
	
	/**
	 * Option Empty
	 */
	public static final String EMPTY = "";
	
	/**
	 * Normal
	 */
	public static final String NORMAL = "normal";
	
	/**
	 * Critical
	 */
	public static final String CRITICAL = "critical";

	/**
	 * Buyer 019Mobile Israel (ISM)
	 */
	public static final String BUYER_019MOBILE_ISRAEL_ISM = "019Mobile Israel (ISM)";

	/**
	 * Buyer Orange Egypt (OEE)
	 */
	public static final String BUYER_ORANGE_EGYPT_OEE = "Orange Egypt (OEE)";
	
	/**
	 * TLF group Europe
	 */
	public static final String BUYER_TLF_GROUP_EUROPE = "TLF group Europe";
	
	/**
	 * TLF Suffix
	 */
	public static final String SUFFIX_TLF = "TLF";
	
	/**
	 * Suffix (OEE)
	 */
	public static final String SUFFIX_OEE = "OEE";

	/**
	 * Buyer Airtel India (ART)
	 */
	public static final String BUYER_AIRTEL_INDIA_ART = "Airtel India (ART)";
	
	/**
	 * Buyer O2 United Kingdom (O2U)
	 */
	public static final String BUYER_02_UNITED_KINGDOM_O2U = "O2 United Kingdom (O2U)";
	
	/**
	 * Buyer Aircel_Ap_Kk&Kerala India (AAI)
	 */
	public static final String BUYER_AIRCEL_INDIA_AAI = "Aircel_Ap_Kk&Kerala India (AAI)";
	
	/**
	 * Buyer Oi Brazil (BOI)
	 */
	public static final String OI_BRAZIL_BOI = "Oi Brazil (BOI)";
	
	/**
	 * Buyer Claro Brazil (CLR)
	 */
	public static final String BUYER_CLARO_BRAZIL_CLR = "Claro Brazil CLR";

	/**
	 * Buyer O2D Germany (O2D)
	 */
	public static final String BUYER_GERMANY_O2D = "O2D Germany O2D";
	
	/**
	 * Global Group Orange
	 */
	public static final String ORANGE = "Orange";

	/**
	 * Mother
	 */
	public static final String MOTHER = "Mother";
	
	/**
	 * Child
	 */
	public static final String CHILD = "Child";
	
	/**
	 * Error The operator field is mandatory.
	 */
	public static final String ERROR_OPERATOR_IS_MANDATORY = "The operator field is mandatory.";
	
	/**
	 * Error The country field is mandatory.
	 */
	public static final String ERROR_COUNTRY_IS_MANDATORY = "The country field is mandatory.";

	/**
	 * Gtam haeyeon.park
	 */
	public static final String HAEYEON_PARK_COMPLETE = "haeyeon.park";
	
	/**
	 * Ltam zunsu.kim
	 */
	public static final String ZUNSU_KIM_COMPLETE = "zunsu.kim";
	
	/**
	 * Ltam zunsu.kim
	 */
	public static final String ZUNSU_KIM = "zunsu.kim";
	
	/**
	 * Gtam haeyeon.park
	 */
	public static final String HAEYEON_PARK = "haeyeon.park";
	
	/**
	 * Gtam jihyeon.kim
	 */
	public static final String JIHYEON_KIM= "jihyeon.kim";
	
	/**
	 * Gtam evan.kim
	 */
	public static final String EVAN_KIM= "evan.kim";
	
	/**
	 * GLobal Master carlos.toneto
	 */
	public static final String CARLOS_TONETO= "carlos.toneto";
	
	/**
	 * User abdul.ansari
	 */
	public static final String ABDUL_ANSARI= "abdul.ansari";
	
	/**
	 * User gc.kim
	 */
	public static final String GC_KIM= "gc.kim";
	
	/**
	 * Ready for Approval
	 */
	public static final String READY_FOR_APPROVAL = "Ready for Approval";
	
	/**
	 * Validated Status
	 */
	public static final String VALIDATED = "Validated";
	
	/**
	 * Ready for Approval
	 */
	public static final String UNDER_EDITION = "Under Edition";

	/**
	 * Home NetWork 724 99
	 */
	public static final String HOME_NETWORK_724_99 = "724 99";
	
	/**
	 * Home NetWork 724 32
	 */
	public static final String HOME_NETWORK_724_32 = "724 32";

	/**
	 * GTAM
	 */
	public static final CharSequence GTAM = "GTAM";

	/**
	 * Field Custom Label
	 */
	public static final String FIELD_CUSTOM_LABEL = "fieldCustomLabel";
	
	/**
	 * Field Home Network
	 */
	public static final String FIELD_HOME_NETWORK = "fieldHomeNetwork";
	
	/**
	 * Field Additional number with SIM card
	 */
	public static final String FIELD_ADDITIONAL_NUMBER_WITH_SIM_CARD = "fieldAdditionalNumbersWithSimCard";
	
	/**
	 * Field Always Available
	 */
	public static final String FIELD_ALWAYS_AVAILABLE = "fieldAlwaysAvailable";
	
	/**
	 * Field Band Block
	 */
	public static final String FIELD_BAND_BLOCK = "fieldBandBlock";
	
	/**
	 * Field Note
	 */
	public static final String FIELD_NOTE = "fieldNote";
	
	/**
	 * Item Name
	 */
	public static final String ITEM_NAME = "itemName";
	
	/**
	 * Item iaPN Protocol Type
	 */
	public static final String ITEM_IAPN_PROTOCOL_TYPE = "itemIaApnProtocolType";
	
	/**
	 * Item Home Network
	 */
	public static final String ITEM_HOME_NETWORK = "itemHomeNetwork";
	
	/**
	 * Item Band Block
	 */
	public static final String ITEM_BAND_BLOCK = "chkBandBlockItem";
	
	/**
	 * Item HPLMN Timer
	 */
	public static final String ITEM_HPLMN_TIMER = "itemHplmnTimerTime";
	
	/**
	 * Item Custom Icon
	 */
	public static final String ITEM_CUSTOM_ICON = "itemCustomIcon";
	
	/**
	 * Item Custom Label
	 */
	public static final String ITEM_CUSTOM_LABEL = "itemCustomLabel";
	
	/**
	 * Item Always Available
	 */
	public static final String ITEM_ALWAYS_AVAILABLE = "itemAlwaysAvailable";
	
	/**
	 * Item Additional Number with SIM Card
	 */
	public static final String ITEM_ADDITIONAL_NUMBER_WITH_SIM_CARD = "itemAdditionalNumbersWithSimCard";
	
	/**
	 * Filter Auto Commit
	 */
	public static final String FILTER_AUTO_COMMIT = "Auto Commit";
	
	/**
	 * Filter Auto Commit 2
	 */
	public static final String FILTER_AUTO_COMMIT_2 = "Auto Commit 2";
	
	/**
	 * Filter Default hidden on Mother
	 */
	public static final String FILTER_DEFAULT_HIDDEN_ON_MOTHER = "Default hidden on Mother";
	
	/**
	 * Filter Mandatory
	 */
	public static final String FILTER_MANDATORY = "Mandatory";
	
	/**
	 * Filter Mandatory for Buyer Child
	 */
	public static final String FILTER_MANDATORY_FOR_BUYER_CHILD = "Mandatory for Buyer Child";
	
	/**
	 * Filter Multiple
	 */
	public static final String FILTER_MULTIPLE = "Multiple";
	
	/**
	 * Filter Name
	 */
	public static final String FILTER_NAME = "Name";
	
	/**
	 * Filter SmartCA Types
	 */
	public static final String FILTER_SMART_CA_TYPES = "SmartCA Types";
	
	/**
	 * Filter Tag Types
	 */
	public static final String FILTER_TAG_TYPES = "Tag Types";
	
	/**
	 * Filter Tag Values
	 */
	public static final String FILTER_TAG_VALUES = "Tag Values";
	
	/**
	 * Filter Type
	 */
	public static final String FILTER_TYPE = "Type";
	
	/**
	 * Filter Warnings
	 */
	public static final String FILTER_WARNINGS = "Warnings";
	
	/**
	 * Filter Has Links
	 */
	public static final String FILTER_HAS_LINKS = "Has Links";
	
	/**
	 * Criteria Auto Commit
	 */
	public static final String CRITERIA_AUTO_COMMIT = "criteria-auto-commit";
	
	/**
	 * Criteria Default hidden on Mother
	 */
	public static final String CRITERIA_DEFAULT_HIDDEN_ON_MOTHER = "criteria-hidden-mother";
	
	/**
	 * Criteria Mandatory
	 */
	public static final String CRITERIA_MANDATORY = "criteria-mandatory";
	
	/**
	 * Criteria Mandatory for Buyer Child
	 */
	public static final String CRITERIA_MANDATORY_FOR_BUYER_CHILD = "criteria-mandatory-child";
	
	/**
	 * Criteria Multiple
	 */
	public static final String CRITERIA_MULTIPLE = "criteria-multiple";
	
	/**
	 * Criteria SmartCA Types
	 */
	public static final String CRITERIA_SMART_CA_TYPES = "criteria-smartca-type";
	
	/**
	 * Criteria Tag Types
	 */
	public static final String CRITERIA_TAG_TYPES = "criteria-tag-types";
	
	/**
	 * Criteria Tag Values
	 */
	public static final String CRITERIA_TAG_VALUES = "criteria-tag-values";
	
	/**
	 * Criteria Type
	 */
	public static final String CRITERIA_TYPE = "criteria-type";
	
	/**
	 * Criteria Warnings
	 */
	public static final String CRITERIA_WARNING = "criteria-warning";
	
	/**
	 * Criteria Has Links
	 */
	public static final String CRITERIA_HAS_LINKS = "criteria-haslinks";
	
	/**
	 * Filter Option Yes
	 */
	public static final String FILTER_OPTION_YES = "Yes";
	
	/**
	 * Filter Option No
	 */
	public static final String FILTER_OPTION_NO = "No";
	
	/**
	 * Filter Option Customizer
	 */
	public static final String FILTER_OPTION_CUSTOMIZER = "Customizer";
	
	/**
	 * Filter Option Android Version
	 */
	public static final String FILTER_OPTION_ANDROID_VERSION = "Android Version";
	
	/**
	 * Filter Option Android Version 4.4.0
	 */
	public static final String FILTER_OPTION_ANDROID_VERSION_4_4_0 = "4.4.0";
	
	/**
	 * Filter Option Resource
	 */
	public static final String FILTER_OPTION_RESOURCE = "Resource";
	
	/**
	 * Filter Option COTA
	 */
	public static final String FILTER_OPTION_COTA = "COTA 1";
	
	/**
	 * Text Result Matched
	 */
	public static final String TXT_RESULT_MATCHED = "txtResultMatched";
	
	/**
	 * Text Discard Inheritance Result Matched
	 */
	public static final String DISCARD_INHERITANCE_TXT_RESULT_MATCHED = "discardInheritanceTxtResultMatched";
	
	/**
	 * Text No Result Matched
	 */
	public static final String TXT_NO_RESULT_MATCHED = "txtNoResultMatched";
	
	/**
	 * Text Discard Inheritance No Result Matched
	 */
	public static final String DISCARD_INHERITANCE_TXT_NO_RESULT_MATCHED = "discardInheritanceTxtNoResultMatched";
	
	/**
	 * Message result matched with Auto Commit
	 */
	public static final String MESSAGE_MATCHED_AUTO_COMMIT = "Auto Commit";
	
	/**
	 * Message no result matched with Auto Commit 2
	 */
	public static final String MESSAGE_NO_RESULT_MATCHED_AUTO_COMMIT_2 = "No results matched \"Auto Commit 2\"";
	
	/**
	 * Class of a item
	 */
	public static final String ITEM_CLASS = "jstree-anchor";
	
	/**
	 * Class of a checked item
	 */
	public static final String ITEM_CHECKED = "jstree-checked";
	
	/**
	 * Class of a highlighted item
	 */
	public static final String ITEM_HIGHLIGHTED = "jstree-search";
	
	/**
	 * Name
	 */
	public static final String NAME = "Name";
	
	/**
	 * Nami
	 */
	public static final String NAMI = "Nami";
	
	/**
	 * ALL CHILDREN
	 */
	public static final String ALL_CHILDREN = "ALL CHILDREN";
	
	/**
	 * alwaysAvailable
	 */
	public static final String ALWAYS_AVAILABLE = "alwaysAvailable";
	
	/**
	 * unitedKingdomAlwaysAvailable
	 */
	public static final String UNITED_KINGDOM_ALWAYS_AVAILABLE = "unitedKingdomAlwaysAvailable";
	
	/**
	 * unitedKingdomAdditionalNumbersWithSimCard
	 */
	public static final String UNITED_KINGDOM_ADDITIONAL_NUMBER_WITH_SIM_CARD = "unitedKingdomAdditionalNumbersWithSimCard";
	
	/**
	 * germanyAlwaysAvailable
	 */
	public static final String GERMANY_ALWAYS_AVAILABLE = "germanyAlwaysAvailable";
	
	/**
	 * germanyAdditionalNumbersWithSimCard
	 */
	public static final String GERMANY_ADDITIONAL_NUMBER_WITH_SIM_CARD = "germanyAdditionalNumbersWithSimCard";
	
	/**
	 * additionalNumbersWithSimCard
	 */
	public static final String ADDITIONAL_NUMBER_WITH_SIM_CARD = "additionalNumbersWithSimCard";
	
	/**
	 * Name Always Available
	 */
	public static final String NAME_ALWAYS_AVAILABLE = "Always Available #";

	/**
	 * Latin America Region
	 */
	public static final String LATIN_AMERICA = "LATIN AMERICA";

	/**
	 * EUROPE
	 */
	public static final String EUROPE = "EUROPE";

	/**
	 * Ireland
	 */
	public static final String IRELAND = "Ireland";

	/**
	 * Operator 3
	 */
	public static final String OPERATOR_3 = "3";

	/**
	 * Region
	 */
	public static final String REGION = "Region";
	
	/**
	 * Suffix
	 */
	public static final String SUFFIX = "Suffix";

	/**
	 * Country
	 */
	public static final String COUNTRY = "Country";
	
	/**
	 * Operator
	 */
	public static final String OPERATOR = "Operator";
	
	/**
	 * Type
	 */
	public static final String TYPE = "Type";
	
	/**
	 * Global
	 */
	public static final String GLOBAL_GROUP = "Global Group";
	
	/**
	 * Status
	 */
	public static final String STATUS = "Status";
	
	/**
	 * homeNetworkNote
	 */
	public static final String HOME_NETWORK_NOTE = "homeNetworkNote";
	
	/**
	 * Note Message New Note Test 51
	 */
	public static final String NOTE_MESSAGE = "New Note Test 51";
	
	/**
	 * Note Message Note 1
	 */
	public static final String NOTE_MESSAGE_1 = "Note 1";
	
	/**
	 * Note Message Note 2
	 */
	public static final String NOTE_MESSAGE_2 = "Note 2";
	
	/**
	 * noteDialog
	 */
	public static final String NOTE_DIALOG = "noteDialog";
	
	/**
	 * Facebook
	 */
	public static final String FACEBOOK = "Facebook";
	
	/**
	 * Instagram
	 */
	public static final String INSTAGRAM = "Instagram";
	
	/**
	 * App Facebook
	 */
	public static final String APP_FACEBOOK = "appFacebook";
	
	/**
	 *  App Instagram
	 */
	public static final String APP_INSTAGRAM = "appInstagram";
	
	/**
	 *  appFacebookColumn
	 */
	public static final String APP_FACEBOOK_COLUMN = "appFacebookColumn";
	
	/**
	 *  appFacebookRow
	 */
	public static final String APP_FACEBOOK_ROW = "appFacebookRow";
	
	/**
	 *  appInstagramRow
	 */
	public static final String APP_INSTAGRAM_ROW = "appInstagramRow";
	
	/**
	 *  Warning cannot save buyer
	 */
	public static final String WARNING_CANNOT_SAVE_BUYER = "warningCannotSaveBuyerMsg";
	
	/**
	 *  appInstagramRowEmptyMsg
	 */
	public static final String APP_INSTAGRAM_ROW_EMPTY = "appInstagramRowEmptyMsg";
	
	/**
	 *  Message Warning cannot save buyer
	 */
	public static final String MSG_WARNING_CANNOT_SAVE_BUYER = "Cannot save the Buyer. Please check the red marked fields.";
	
	/**
	 *  Message Field Empty
	 */
	public static final String MSG_WARNING_FIELD_EMPTY = "This field is empty";
	
	/**
	 *  Dialog What's new
	 */
	public static final String WHATS_NEW = "whatsNew";
}
