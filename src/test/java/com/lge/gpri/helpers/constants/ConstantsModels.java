package com.lge.gpri.helpers.constants;


/**
* Constants class.
* 
* <p>Description: This class provides the constants needed for the tests execution.
*  
* @author AUTHOR NAME (can exist more than one)
* 
*/

public class ConstantsModels {

	/**
	 * Carrier Aggregation
	 */
	public static final String CARRIER_AGGREGATION = "Carrier Aggregation";
	/**
	 * Model 4GB3
	 */
	public static final String MODEL_4GB3 = "4GB3";
	
	/**
	 * Model LG-G350
	 */
	public static final String MODEL_LG_G350 = "LG-G350";
	
	/**
	 * Model LG-K350
	 */
	public static final String MODEL_LG_A270 = "LG-A270";
	
	/**
	 * Model 16GB2
	 */
	public static final String MODEL_16GB2 = "16GB2";
	
	/**
	 * Model 32GB
	 */
	public static final String MODEL_64GB2 = "64GB2";
	
	
	/**
	 * Invalid Model.
	 */
	public static final String INVALID_MODEL = "ÇPInvalid";
	
	/**
	 * Message for No Models Available
	 */
	public static final String NO_MODELS_AVAILABLE = "No models available.";

	/**
	 * Grid Row x Column 4 x 4
	 */
	public static final String GRID_ROW_COLUMN_4X4 = "4 x 4";
	
	/**
	 * Grid Row x Column 3 x 2
	 */
	public static final String GRID_ROW_COLUMN_3X2 = "3 x 2";

	/**
	 * Tag Grid Row x Column
	 */
	public static final String TAG_GRID_ROW_X_COLUMN = "Grid Row x Column";

	/**
	 * UI Version UI 4.0
	 */
	public static final String UI_VERSION_4_0 = "UI 4.0";
	
	/**
	 * Representative Name
	 */
	public static final String REPRESENTATIVE_NAME_TEST_MODEL = "Teste Model";
	/**
	 * Chipset
	 */
	public static final String QCT_ATLAS = "QCT Atlas";

	/**
	 * Hotseat Icon Range
	 */
	public static final String HOTSEAT_ICON_RANGE_4 = "4 Icons";
	
	/**
	 * Hotseat Icon Range
	 */
	public static final String HOTSEAT_ICON_RANGE_7 = "7 Icons";
	
	/**
	 * Tag Grid Row x Column
	 */
	public static final String TAG_HOTSEAT_ICON_RANGE = "Hotseat Icon Range";

	/**
	 * Icon Size
	 */
	public static final String ICON_SIZE_72x72 = "72 x 72";
	
	/**
	 * Icon Size
	 */
	public static final String ICON_SIZE_136x136 = "136 x 136";
	
	/**
	 * Tag Icon Size
	 */
	public static final String TAG_ICON_SIZE = "Icon Size";

	/**
	 * Radio
	 */
	public static final String RADIO_3G = "3G";

	/**
	 * RAM Memory
	 */
	public static final String RAM_MEMORY_1GB = "1GB";

	/**
	 * Screen Size
	 */
	public static final String SCREEN_SIZE_800x480 = "800 x 480";
	
	/**
	 * Tag Screen Size
	 */
	public static final String TAG_SCREEN_SIZE = "Screen Size";

	/**
	 * Second Screen Icon Range
	 */
	public static final String SECOND_SCREEN_ICON_RANGE_4 = "4";

	/**
	 * SIM Card Slots
	 */
	public static final String SINGLE_SIM = "Single SIM";

	/**
	 * Technology
	 */
	public static final String ANDROID_PHONE = "Android Phone";

	/**
	 * Wallpaper Size
	 */
	public static final String WALLPAPER_SIZE_1024X600 = "1024x600";
	
	/**
	 * Region Latin America
	 */
	public static final String REGION_LATIN_AMERICA = "LATIN AMERICA";
	
	/**
	 * Region ASIA & MIDDLE EAST & AFRICA
	 */
	public static final String REGION_ASIA_MIDDLE_EAST_AFRICA = "ASIA & MIDDLE EAST & AFRICA";
	
	/**
	 * Option NO PACKAGE 
	 */
	public static final String NO_PACKAGE = "NO PACKAGE";
	
	/**
	 * SWPL
	 */
	public static final String SWPL_JAESUK_LEE_ALL = "jaesuk.lee";

	/**
	 * SWPL
	 */
	public static final String SWPL_HUGO_KANG_ALL = "hugo.kang";
	
	/**
	 * SWPL
	 */
	public static final String SWPL_JAESUK_LEE = "jaesuk.lee";

	/**
	 * SWPL
	 */
	public static final String SWPL_HUGO_KANG = "hugo.kang";
	
	/**
	 * SWPL
	 */
	public static final String SWPL_MANSOO_KIM = "mansoo.kim";
	
	/**
	 * SWPL
	 */
	public static final String SWPL_WOOJAE_LEE = "woojae.lee";
	
	/**
	 * SWPL
	 */
	public static final String SWPL_CHANGSOO_KIM = "changsoo.kim";
	
	/**
	 * SWPL
	 */
	public static final String SWPL_MANSOO_KIM_ALL = "mansoo.kim";
	
	/**
	 * SWPL
	 */
	public static final String SWPL_WOOJAE_LEE_ALL = "woojae.lee";
	
	/**
	 * SWPL
	 */
	public static final String SWPL_CHANGSOO_KIM_ALL = "changsoo.kim";
	
	/**
	 * SWMM
	 */
	public static final String SWMM_ALVIN_SHIN = "alvin.shin";

	/**
	 * SWMM
	 */
	public static final String SWMM_FOX_LEE = "fox.lee";
	
	/**
	 * SWMM
	 */
	public static final String SWMM_ALVIN_SHIN_ALL = "alvin.shin";

	/**
	 * SWMM
	 */
	public static final String SWMM_FOX_LEE_ALL = "fox.lee";
	
	/**
	 * SWMM
	 */
	public static final String SWMM_ABHAI_SINGH = "abhai.singh";
	
	/**
	 * SWMM
	 */
	public static final String SWMM_ABHAI_SINGH_ALL = "abhai.singh";
	
	/**
	 * Model created with success
	 */
	public static final String MODEL_CREATED_SUCCESS = "Model created successfully.";

	/**
	 * Model created with success
	 */
	public static final String MODEL_EDITED_SUCCESS = "Model edited successfully.";
	
	/**
	 * Message Only for testing
	 */
	public static final String MESSAGE_ONLY_FOR_TESTING = "Only for testing";
	
	/**
	 * Invalid Region
	 */
	public static final String INVALID_REGION = "Invalid Region";
	
	/**
	 * Message for invalid search
	 */
	public static final String INVALID_SEARCH = "No results matched \"REGION\"";
	
	/**
	 * Option FIRST
	 */
	public static final String FIRST = "FIRST";
	
	/**
	 * target device
	 */
	public static final String TARGET_DEVICE = "target device";
	
	/**
	 * target device 16GB2
	 */
	public static final String TARGET_DEVICE_16GB2 = "targetdevice16GB2";
	
	/**
	 * product
	 */
	public static final String PRODUCT = "product";
	
	/**
	 * cupss group
	 */
	public static final String CUPSS_GROUP = "cupss group";
	
	/**
	 * Message for Empty Field
	 */
	public static final String MODEL_EMPTY_FIELD = "Some required fields have no value. Please, fill this fields and save again.";
	
	/**
	 * No users added.
	 */
	public static final Object NO_USER_ADDED = "No users added.";

}
