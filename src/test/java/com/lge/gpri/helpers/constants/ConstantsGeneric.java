package com.lge.gpri.helpers.constants;


/**
* Constants class.
* 
* <p>Description: This class provides the constants needed for the tests execution.
*  
* @author AUTHOR NAME (can exist more than one)
* 
*/

public class ConstantsGeneric {

	/**
	 * Name User
	 */
	public static final String USER = System.getenv("username");
	
	/**
	 * Time for waiting
	 */
	public static final int TIME = 1;
	
	/**
	 * Time more for waiting
	 */
	public static final int TIME_LONG = 5;
	
	/**
	 * Chrome Driver Path
	 */
	public static final String PATH = "src\\test\\resources\\chromedriver.exe";
	
	/**
	 * Screenshots Path
	 */
	public static final String SCREENSHOTS_PATH = "C:\\Users\\" + USER + "\\Documents\\Screenshots\\";

	/**
	 * Web Browser
	 */
	public static final String KEY_DRIVER = "webdriver.chrome.driver";
	
	/**
	 * Configuration File Path for Buyers
	 */
	public static final String PATH_FILE_BUYER = "src\\test\\resources\\buyermap.properties";
	
	/**
	 * Configuration File Path for Models
	 */
	public static final String PATH_FILE_MODEL = "src\\test\\resources\\modelmap.properties";
	
	/**
	 * Configuration File Path for Packages
	 */
	public static final String PATH_FILE_PACKAGE = "src\\test\\resources\\packagemap.properties";
	
	/**
	 * Configuration File Path for View
	 */
	public static final String PATH_FILE_VIEW = "src\\test\\resources\\viewmap.properties";
	
	/**
	 * Configuration File Path Globals
	 */
	public static final String PATH_FILE_GLOBAL = "src\\test\\resources\\globalmap.properties";
	
    /**
	 * Configuration File Path for Packages
	 */
	public static final String PATH_FILE_ITEM_LIBRARY = "src\\test\\resources\\itemlibrarymap.properties";
	/**
	 * Configuration File Path Menu
	 */
	public static final String PATH_FILE_MENU = "src\\test\\resources\\menumap.properties";
	
	/**
	 * Configuration File Path for Compare
	 */
	public static final String PATH_FILE_COMPARE = "src\\test\\resources\\comparemap.properties";
	
	/**
	 * Configuration File Path for Item Inspector
	 */
	public static final String PATH_FILE_ITEM_INSPECTOR = "src\\test\\resources\\iteminspectormap.properties";

	/**
	 * Configuration File Path for User Management
	 */
	public static final String PATH_FILE_USER_MANAGEMENT = "src\\test\\resources\\usermanagementmap.properties";
	
	/**
	 * Configuration File Path for Dashboard Home screen
	 */
	public static final String PATH_FILE_DASHBOARD = "src\\test\\resources\\dashboardmap.properties";
	
	/**
	 * Link View Page
	 */
	public static final String LINK_SITE_VIEW_PAGE = "http://136.166.96.136:8204/Views/View_GPRI/Search.aspx#/";
	
	/**
	 * Link Buyers Page
	 */
	public static final String LINK_SITE_BUYERS_PAGE = "http://136.166.96.136:8204/Views/Buyers/";
	
	/**
	 * Link Buyers Page Count zunsu.kim
	 */
	public static final String LINK_LOGIN_ZUNSU_KIM = "http://136.166.96.136:8204/Views/Shared/Login.aspx?sabun=106569";

	/**
	 * Configuration File Path for Tags
	 */
	public static final String PATH_FILE_TAGS = "src\\test\\resources\\tagsmap.properties";
	
	/**
	 * URL Follows
	 */
	public static final String URL_FOLLOWS = "http://136.166.96.136:8204/Views/Follows/";
	
	/**
	 * URL Help
	 */
	public static final String URL_HELP = "http://136.166.96.136:8204/Views/Help/";
}
