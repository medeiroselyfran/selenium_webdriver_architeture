package com.lge.gpri.helpers.constants;


/**
* Constants class.
* 
* <p>Description: This class provides the constants needed for the tests execution.
*  
* @author AUTHOR NAME (can exist more than one)
* 
*/

public class ConstantsUserManagement {

	/**
	 * Create Reasons Only for test
	 */
	public static final String CREATE_REASONS_ONLY_FOR_TEST = "Only for test";
	
	/**
	 * Profile type NVG/PVG
	 */
	public static final String NVG_PVG = "NVG/PVG";
	
	/**
	 * Profile type GTAM
	 */
	public static final String GTAM = "GTAM";
	
	/**
	 * Profile type Local Master
	 */
	public static final String LOCAL_MASTER = "Local Master";
	
	/**
	 * Profile type Global Master
	 */
	public static final String GLOBAL_MASTER = "Global Master";
	
	/**
	 * Profile type Item Reviewer
	 */
	public static final String ITEM_REVIEWER = "Item Reviewer";
	
	/**
	 * Profile type RM
	 */
	public static final String RM = "RM";
	
	/**
	 * User alberto.decaceres
	 */
	public static final String ALBERTO_DECACERES = "alberto.decaceres";
	
	/**
	 * Invalid User
	 */
	public static final String USER_INVALID = "InvalidUser";
	
	/**
	 * User ben.chu
	 */
	public static final String BEN_CHU = "ben.chu";
	
	/**
	 * User yara.martins
	 */
	public static final String YARA_MARTINS = "yara.martins";
	
	/**
	 * User andressa.leite
	 */
	public static final String ANDRESSA_LEITE = "andressa.leite";

	/**
	 * User alex.rechtman
	 */
	public static final String ALEX_RECHMAN = "alex.rechtman";
	
	/**
	 * User abdul.ansari
	 */
	public static final String ABDUL_ANSARI = "abdul.ansari";
	
	/**
	 * Region Europe
	 */
	public static final String EUROPE = "EUROPE";
	
	/**
	 * Buyer Region ASIA & MIDDLE EAST & AFRICA
	 */
	public static final String ASIA_MIDDLE_EAST_AFRICA = "ASIA & MIDDLE EAST & AFRICA";

	/**
	 * Date 26/06/16
	 */
	public static final String DATE_26_06_16 = "26/06/16";
	
	/**
	 * User fernando.campos
	 */
	public static final String FERNANDO_RIBEIRO = "fernando.ribeiro";
	
	/**
	 * User blocked warning
	 */
	public static final String WARNING_BLOCKED = "You cannot add roles to a blocked user.";

	/**
	 * Role swpl
	 */
	public static final String SWPL = "SW P/L";
	
	/**
	 * Role swmm
	 */
	public static final String SWMM = "SW MM";

	/**
	 * 
	 */
	public static final String CHINA_SOUTHEAST_ASIA = "CHINA & SOUTHEAST ASIA";
	
	/**
	 * No PVGs available.
	 */
	public static final String NO_PVG_AVAILABLE = "No PVGs available.";
}
