package com.lge.gpri.helpers.constants;


/**
* Constants class.
* 
* <p>Description: This class provides the constants needed for the tests execution.
*  
* @author AUTHOR NAME (can exist more than one)
* 
*/

public class ConstantsView {

	/**
	 * Buyer Oi Brazil (BOI)
	 */
	public static final String OI_BRAZIL_BOI = "Oi Brazil (BOI)";
	
	/**
	 * Buyer TLF group Europe (6TL)
	 */
	public static final String TLF_GROUP_EUROPE_6TL = "TLF group Europe (6TL)";
	
	/**
	 * Buyer Suffix 6TL
	 */
	public static final String SUFFIX_6TL = "6TL";
	
	/**
	 * Model LGH815
	 */
	public static final String LGH815 = "LGH815";
	
	/**
	 * Model LGX240YK
	 */
	public static final String LGX240YK = "LGX240YK";
	
	/**
	 * PACKAGE LGX240YK_MOS_Australia_Open
	 */
	public static final String PACKAGE_LGX240YK = "LGX240YK_MOS_Australia_Open";
	/**
	 * UI Simulator Default Android GB
	 */
	public static final String UI_SIMULATOR_DEFAULT_ANDROID_GB = "Default Android GB";
	/**
	 * Package Name Test ABCDE
	 */
	public static final String PACKAGE_NAME_ABCDE = "abcde";

	/**
	 * Package Name PKG3-N-MS1 (BOI LGH870)
	 */
	public static final String PACKAGE_NAME_PKG3_N_MS1 = "PKG3-N-MS1 (BOI LGH870)";

	/**
	 * Search Invalid BOI Brasil
	 */
	public static final String SEARCH_INVALID_BOI_BRASIL = "BOI Brasil";
	/**
	 * Package Name Test3 View
	 */
	public static final String PACKAGE_NAME_TEST_VIEW = "Test3 (6TL LGH815)";
	/**
	 * Status Under Edition
	 */
	public static final String UNDER_EDITION = "Under Edition";
	/**
	 * Status Not Confirmed
	 */
	public static final String NOT_CONFIRMED = "Not Confirmed";
	
	/**
	 * Status Not Confirmed
	 */
	public static final String CONFIRMED = "Confirmed";
	
	/**
	 * Message Only for testing
	 */
	public static final String MESSAGE_ONLY_FOR_TESTING = "Only for testing";
	/**
	 * Model 16GB2 
	 */
	public static final String MODEL_16GB2 = "16GB2";
	
	/**
	 * Buyer Advinne South Africa (ADS)
	 */
	public static final String ADVINNE_SOUTH_AFRICA_ADS = "Advinne South Africa (ADS)";
	
	/**
	 * Buyer Advinne South Africa
	 */
	public static final String ADVINNE_SOUTH_AFRICA = "Advinne South Africa";
	
	/**
	 * Country South Africa
	 */
	public static final String SOUTH_AFRICA = "South Africa";
	
	/**
	 * Operator Advinne
	 */
	public static final String ADVINNE = "Advinne";

	/**
	 * Suffix ADS
	 */
	public static final String SUFFIX_ADS = "ADS";
	
	/**
	 * Suffix IDT
	 */
	public static final String SUFFIX_IDT = "IDT";
	
	/**
	 * User changhwan.lee SW PL
	 */
	public static final String CHANGHWAN_LEE = "changhwan.lee";
	
	/**
	 * User alvin.shin SW MM
	 */
	public static final String ALVIN_SHIN = "alvin.shin";

	/**
	 * User alex.rechtman LTM
	 */
	public static final String ALEX_RECHTMAN = "alex.rechtman";
	
	/**
	 * User kj.shin GTAM
	 */
	public static final String KJ_SHIN = "kj.shin";
	
	/**
	 * User donato.antonucci GTM
	 */
	public static final String DONATO_ANTONUCCI = "donato.antonucci";

	/**
	 * Buyer Region ASIA & MIDDLE EAST & AFRICA
	 */
	public static final String ASIA_MIDDLE_EAST_AFRICA = "ASIA & MIDDLE EAST & AFRICA";
	
	/**
	 * Buyer TELEFONICA Spain (TLF)
	 */
	public static final String TELEFONICA_SPAIN_TLF = "TELEFONICA Spain (TLF)";
	
	/**
	 * Buyer O2D Germany (O2D)
	 */
	public static final String O2D_GERMANY_O2D = "O2D Germany (O2D)";
	
	/**
	 * Buyer O2 United Kingdom (O2U)
	 */
	public static final String O2_UNITED_KINGDOM_O2U = "O2 United Kingdom (O2U)";

	/**
	 * Package PKG3-M-VoLTE/VoWiFi-MS1 (NTP LGH815)
	 */
	public static final String PKG_3M_VOLTE_VOWIFI_MS1_NTP_LGH815 = "PKG3-M-VoLTE VoWiFi-MS1 (NTP LGH815)";
	
	/**
	 * Package PKG3-M-VoLTE/VoWiFi
	 */
	public static final String PKG3_M_VOLTE_VOWIFI = "[Normal] PKG3-M-VoLTE/VoWiFi (11/08/2016)";
	
	/**
	 * Package PKG5-M-VoLTE/VoWiFi-MS1
	 */
	public static final String PKG5_M_VOLTE_VOWIFI_MS1 = "[Normal] PKG5-M-VoLTE/VoWiFi-MS1";
	
	/**
	 * String Home Network 
	 */
	public static final String HOME_NETWORK = "Home Network";
	
	/**
	 * Name Home Network #1
	 */
	public static final String HOME_NETWORK_1 = "Home Network #1";
	
	/**
	 * Value Home Network 716 17
	 */
	public static final String HOME_NETWORK_VALUE_716_17 = "716 17";
	
	/**
	 * Value Invalid Home Network 716 16
	 */
	public static final String HOME_NETWORK_INVALID_VALUE_716_16 = "716 16";
	
	/**
	 * Value Home Network 120 01
	 */
	public static final String HOME_NETWORK_VALUE_120_01 = "120 01";
	
	/**
	 * Buyer VODACOM South Africa (VDC)
	 */
	public static final String TATA_INDIA = "Tata India (IDT)";

	/**
	 * Model LGH220
	 */
	public static final String LGH220 = "LGH220"; 
	
	/**
	 * Search Zealand
	 */
	public static final String SEARCH_ZEALAND = "Zealand";
	
	/**
	 * Result New Zealand
	 */
	public static final String RESULT_NEW_ZEALAND = "New Zealand";
	
	/**
	 * Search BYT
	 */
	public static final String SEARCH_BYT = "BYT";
	
	/**
	 * Result BOUYGUES France (BYT)
	 */
	public static final String RESULT_BOUYGUES_FRANCE_BYT = "BOUYGUES France (BYT)";
	
	/**
	 * Package Name Test3
	 */
	public static final String PACKAGE_NAME_TEST3 = "Test3";
	
	/**
	 * Package Follow Advinne South Africa (ADS) + 16GB2
	 */
	public static final String PACKAGE_ADS_16GB2 = "Advinne South Africa (ADS) + 16GB2";
	
	/**
	 * Buyer ENTEL Peru (NTP)
	 */
	public static final String BUYER_ENTEL_PERU_NTP = "ENTEL Peru (NTP)";
	
	/**
	 * Buyer Suffix ENTEL Peru (NTP)
	 */
	public static final String SUFFIX_NTP = "NTP";

	/**
	 * Error A buyer with the same nickname already exists.
	 */
	public static final Object ERROR_BUYER_NICKNAME_EXIST = "A buyer with the same nickname already exists.";
	
	/**
	 * Error
	 */
	public static final String ERROR = "Error";
	
	/**
	 * URL
	 */
	public static final String URL = "http://136.166.96.136:8204/Views/View_GPRI/Search.aspx#/";
	
	/**
	 * homeNetwork
	 */
	public static final String HOME_NETWORK_VIEW = "homeNetwork";
	
	/**
	 * homeNetworkHistory
	 */
	public static final String HOME_NETWORK_HISTORY = "homeNetworkHistory";
	
	/**
	 * itemHistoryNote
	 */
	public static final String ITEM_HISTORY_NOTE = "itemHistoryNote";
	
	/**
	 * itemHistoryRequester
	 */
	public static final String ITEM_HISTORY_REQUESTER = "itemHistoryRequester";
	
	/**
	 * itemHistoryApprover
	 */
	public static final String ITEM_HISTORY_APPROVER = "itemHistoryApprover";
	
	/**
	 * appInstagram
	 */
	public static final String APP_INSTAGRAM_VIEW= "appInstagram";
	
	/**
	 * You must select at least one item
	 */
	public static final String ERROR_MESSAGE_EXPORT_WITHOUT_ITEM = "You must select at least one item";
	
	/**
	 * appFacebook
	 */
	public static final String APP_FACEBOOK_VIEW = "appFacebook";
}