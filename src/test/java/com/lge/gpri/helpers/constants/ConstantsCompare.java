package com.lge.gpri.helpers.constants;


/**
* Constants class.
* 
* <p>Description: This class provides the constants needed for the tests execution.
*  
* @author AUTHOR NAME (can exist more than one)
* 
*/

public class ConstantsCompare {

	/**
	 * Buyer Oi Brazil (BOI)
	 */
	public static final String OI_BRAZIL_BOI = "Oi Brazil (BOI)";
	
	/**
	 * Buyer Suffix (BOI)
	 */
	public static final String SUFFIX_BOI = "BOI";
	
	/**
	 * Buyer Suffix (TIM)
	 */
	public static final String SUFFIX_TIM = "TIM";
	
	/**
	 * Buyer Tim Italy (TIM)
	 */
	public static final String TIM_ITALY_TIM = "TIM Italy (TIM)";
	/**
	 * Model LGH815
	 */
	public static final String LGH815 = "LGH815";
	
	/**
	 * Model LGA270
	 */
	public static final String LGA270 = "LGA270";
	
	/**
	 * Item Library Auto Commit
	 */
	public static final String AUTO_COMMIT = "Auto Commit";
	
	/**
	 * Model 16GB2 
	 */
	public static final String MODEL_16GB2 = "16GB2";
	
	/**
	 * Buyer Advinne South Africa (ADS)
	 */
	public static final String ADVINNE_SOUTH_AFRICA_ADS = "Advinne South Africa (ADS)";
	
	/**
	 * Item Name
	 */
	public static final String NAME = "Name";
	
	/**
	 * Item Name #0
	 */
	public static final String NAME_0 = "Name #0";
	
	/**
	 * Value Item Name Advinne
	 */
	public static final String NAME_ADVINNE = "ADVINNE";
	
	/**
	 * Name Home Network #1
	 */
	public static final String HOME_NETWORK_1 = "Home Network #1";

	/**
	 * Name Home Network
	 */
	public static final String HOME_NETWORK = "Home Network";

	/**
	 * Buyer Djuice Ukraine (DJU)
	 */
	public static final String DJUICE_UKRAINE_DJU = "Djuice Ukraine (DJU)";
	
	/**
	 * Buyer ENTEL Peru (NTP)
	 */
	public static final String BUYER_ENTEL_PERU_NTP = "ENTEL Peru (NTP)";
	
	/**
	 * Buyer 012Mobile Israel (MOP)
	 */
	public static final String BUYER_012MOBILE_ISRAEL_MOP = "012Mobile Israel (MOP)";
	
	/**
	 * Home NetWork 724 32
	 */
	public static final String HOME_NETWORK_724_32 = "724 32";
	
	/**
	 * Home NetWork 724 35
	 */
	public static final String HOME_NETWORK_724_35 = "724 35";
	
	/**
	 * Path item Name 0
	 */
	public static final String PATH_ITEM_NAME_0 = "Parameters::Network::Name #0";
	
	/**
	 * Path item Name
	 */
	public static final String PATH_ITEM_NAME = "Parameters::Network::Name";
	
	/**
	 * Path item Band Block
	 */
	public static final String PATH_ITEM_BAND_BLOCK = "Parameters::Network::Band Block";
	
	/**
	 * Path item Home Network 1
	 */
	public static final String PATH_ITEM_HOME_NETWORK_1 = "Parameters::Network::Home Network #1";
	
	/*
	/**
	 * Item Library Band Block
	 */
	public static final String BAND_BLOCK = "Band Block";
	
	/**
	 * Item Library Band Block Value 50
	 */
	public static final String BAND_BLOCK_VALUE_50 = "50";
	
	/**
	 * Item Library Band Block Value 51
	 */
	public static final String BAND_BLOCK_VALUE_51 = "51";

	/**
	 * Parameter All
	 */
	public static final String ALL = "All";
	
	/**
	 * Parameter LAST
	 */
	public static final String LAST = "LAST";
	
	/**
	 * Parameter EMPTY
	 */
	public static final String EMPTY = "";
	
	/**
	 * URL
	 */
	public static final String URL = "http://136.166.96.136:8204/Views/Compare_GPRI/";
}
