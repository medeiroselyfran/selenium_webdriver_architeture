package com.lge.gpri.helpers;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;

public class ObjectMap {
	
	Properties properties;
	
	public ObjectMap(String mapFile){
		properties = new Properties();
		try{
			FileInputStream fileIn = new FileInputStream(mapFile);
			properties.load(fileIn);
			fileIn.close();
		}catch(IOException e){
			System.out.println(e.getMessage());
		}
	}
	
	public By getLocator(String logicalElementName) throws Exception{
		String locator = properties.getProperty(logicalElementName);
		
		String locatorType = locator.split(">>")[0];
		String locatorValue = locator.split(">>")[1];
		
		if(locatorType.toLowerCase().equals("id"))
			return By.id(locatorValue);
		else if(locatorType.toLowerCase().equals("name"))
			return By.name(locatorValue);
		else if(locatorType.toLowerCase().equals("classname") || locatorType.toLowerCase().equals("class"))
			return By.className(locatorValue);
		else if(locatorType.toLowerCase().equals("tagname") || locatorType.toLowerCase().equals("tag"))
			return By.tagName(locatorValue);
		else if(locatorType.toLowerCase().equals("linktext") || locatorType.toLowerCase().equals("link"))
			return By.linkText(locatorValue);
		else if(locatorType.toLowerCase().equals("partiallinktext"))
			return By.partialLinkText(locatorValue);
		else if(locatorType.toLowerCase().equals("cssselector"))
			return By.cssSelector(locatorValue);
		else if(locatorType.toLowerCase().equals("xpath"))
			return By.xpath(locatorValue);
		else
			throw new Exception("Type " + locatorType + " not defined!");
	}
	
	/**
	 * Method to get the xpath of that logicalElementName given
	 * replacing a variable given in the determined space ("%%s")
	 * For a while this variables can only be used for xpath type
	 * 
	 */
	public By getLocatorVar(String logicalElementName, String... var) throws Exception{
		String locator = properties.getProperty(logicalElementName);
		
		
		String locatorType = locator.split(">>")[0];
		String locatorValue = locator.split(">>")[1];
		
		for(int i=0;i<var.length;i++){
			locatorValue = locatorValue.replaceFirst("%%s",var[i]);
		}
		
		if(locatorType.toLowerCase().equals("id"))
			return By.id(locatorValue);
		else if(locatorType.toLowerCase().equals("name"))
			return By.name(locatorValue);
		else if(locatorType.toLowerCase().equals("classname") || locatorType.toLowerCase().equals("class"))
			return By.className(locatorValue);
		else if(locatorType.toLowerCase().equals("tagname") || locatorType.toLowerCase().equals("tag"))
			return By.tagName(locatorValue);
		else if(locatorType.toLowerCase().equals("linktext") || locatorType.toLowerCase().equals("link"))
			return By.linkText(locatorValue);
		else if(locatorType.toLowerCase().equals("partiallinktext"))
			return By.partialLinkText(locatorValue);
		else if(locatorType.toLowerCase().equals("cssselector"))
			return By.cssSelector(locatorValue);
		else if(locatorType.toLowerCase().equals("xpath")){
			return By.xpath(locatorValue);
		}
		else
			throw new Exception("Type " + locatorType + " not defined!");
	}
	
	public String getElement(String elementPage){
		String element = properties.getProperty(elementPage);
		return element;
	}
}

