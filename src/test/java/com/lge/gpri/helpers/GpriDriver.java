package com.lge.gpri.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GpriDriver extends ChromeDriver {
	private int speed;

	public GpriDriver(int speed) {
		this.speed = speed;
	}

	@Override
	public WebElement findElement(By by) {
		try {
			Thread.sleep(speed);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return by.findElement((SearchContext) this);
	}
}
