package com.lge.gpri.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.lge.gpri.helpers.constants.ConstantsGeneric;

/**
* Util class
* 
* <p>Description: This class must provide methods for the test executions.
*  
* @author AUTHOR NAME (can exist more than one)
* 
*/

public class Util {
	
	public static Properties getPropertiesFile(String fileName) {
		Properties propertieFile = new Properties();
		
		try{
			propertieFile = new Properties();
			ClassLoader classLoader = ObjectMap.class.getClassLoader();
			File propertiesFile = new File(classLoader.getResource(fileName).getFile());
			FileInputStream fileIn = new FileInputStream(propertiesFile);
			propertieFile.load(fileIn);
			fileIn.close();
		}catch(IOException e){
			System.out.println(e.getMessage());
		}
		
		return propertieFile;
	}
	
	public static File getResultFile() {
		return new File("results/results.json");
	}
	
	public static void saveScreenshot(WebDriver driver, String testName){
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshot, new File(ConstantsGeneric.SCREENSHOTS_PATH + testName + System.currentTimeMillis() + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String currentDay() {
		return new SimpleDateFormat("dd/MM/yy").format(Calendar.getInstance().getTime());	
	}
	
	public static String sumDate (int dias,Date data) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		calendar.add(Calendar.DATE, dias);
		String output = calendar.toString();
		return output;         
	}
}
