package com.lge.gpri.helpers.report;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import testlink.api.java.client.TestLinkAPIResults;

public class ReportManagerWatcher extends TestWatcher {
	
	private Report mReport;
	
	public ReportManagerWatcher(Report report) {
		mReport = report;
	}
	
	@Override
	protected void starting(Description description) {
		super.starting(description);
	}
	
	@Override
	protected void succeeded(Description description) {
		super.succeeded(description);
		mReport.setResultCurrentTest(Result.SUCCESS, description.toString(), "");
		
		try {
			TestLink.addResultCurrentTest(TestLinkAPIResults.TEST_PASSED);
		} catch (Exception e) {
			System.out.println("The variable \"mTestLinkIdCurrentTest\" from TestLink class must be setted.");
			e.printStackTrace();
		}
		
		mReport.writeResults();
	}
	
	@Override
    protected void failed(Throwable e, Description description) {
		super.failed(e, description);
		
		StackTraceElement[] stackTraceElements = e.getStackTrace();
		
		if (stackTraceElements[0].toString().contains("Assert.fail")) {
			mReport.setResultCurrentTest(Result.ASSERT_ERROR, description.toString(), "");
			//TestLink.addResultCurrentTest(TestLinkAPIResults.TEST_FAILED); //If failed dont send to testlink
		} else if (stackTraceElements[0].toString().contains("org.junit.AssumptionViolatedException")) {
			System.out.println("Assumpition Error");
			return;
		} else {
			String stackTrace = formatStackTrace(e.getStackTrace());
			mReport.setResultCurrentTest(Result.EXCEPTION, description.toString(), stackTrace);
			//TestLink.addResultCurrentTest(TestLinkAPIResults.TEST_FAILED); //If failed dont send to testlink
		}
		
		mReport.writeResults();
    }
	
	@Override
	protected void finished(Description description) {
		super.finished(description);
	}
	
	@Override
	protected void skipped(org.junit.AssumptionViolatedException e, Description description) {
		super.skipped(e, description);


		mReport.setResultCurrentTest(Result.SKIPPED, description.toString(), "");
		
		mReport.writeResults();
	}
	
	private String formatStackTrace(StackTraceElement[] stackTrace) {
        StringBuilder buffer = new StringBuilder();
        for (StackTraceElement element : stackTrace) {
            buffer.append(element).append("<br>");
        }
        return buffer.toString();
    }
}
