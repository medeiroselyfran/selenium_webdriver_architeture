package com.lge.gpri.helpers.report;

import java.util.ArrayList;

public class ReportManager {
	private ArrayList<Report> mReports;
	private static ReportManager reportManagerInstance;
	
	private ReportManager() {
		mReports = new ArrayList<Report>();
	}
	
	public static ReportManager getInstance() {
		if (reportManagerInstance == null) {
			reportManagerInstance = new ReportManager();
		}
		return reportManagerInstance;
	}
	
	public Report addReport(String reportName) {
		
		for (Report report : mReports) {
			if (report.getReportName().equals(reportName)) {
				return null;
			}
		}
		
		Report newReport = new Report(reportName);
		mReports.add(newReport);
		return newReport;
	}
	
	public Report getReport(int pos) {
		return mReports.get(pos);
	}
	
	public void writeResultsAllReports() {
		for (Report report : mReports) {
			report.writeResults();
		}
	}
	
	public void writeResultReport(int pos) {
		mReports.get(pos).writeResults();
	}
}
