package com.lge.gpri.helpers.report;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.filechooser.FileSystemView;

import com.fasterxml.jackson.databind.ObjectMapper;
//import testlink.api.java.client.TestLinkAPIClient;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

public class TestLink {
	private static HashMap<String, String> mTestCaseResults = new HashMap<String, String>();
	public static String mTestLinkIdCurrentTest;
	public static String timeInitExecution = (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss")).format(new Timestamp(System.currentTimeMillis()));
	
	
	public static void main(String[] args) throws InterruptedException, IOException {
		sendResultsToTestLink();
	}
	
	@SuppressWarnings({ "unused", "rawtypes" })
	public static void sendResultsToTestLink() throws InterruptedException, IOException {        
		readResultsFromFile();
		Configuration.initFromFile();
		int totalTestCases = mTestCaseResults.size();
		boolean verifyBeforeSending = false;
		int partSize = 100;
		Thread testsOfPart[] = new Thread[partSize];
		int[] countTestsSent = new int[] {0};
		int[] countPartsSent = new int[] {0};
		int[] testsAlreadyPerformed = new int[] {0};
		long startTime = 0,endTime=0,partTime=0,totalTime=0;

		System.out.println("Project: "+Configuration.projectName);
		System.out.println("Plan: "+Configuration.testPlanName);
		System.out.println("Build: "+Configuration.buildName);
		System.out.println();
		System.out.println("Total test cases to send: "+totalTestCases+" ("+(totalTestCases/partSize+1)+" parts of size "+partSize+" each)");
		System.out.print("Do you really want to send these tests? (y/n)\n>");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String response = br.readLine();
		if(!response.equals("y")) {
			System.out.println("Canceled.");
			return;
		}
		System.out.println();

		final TestLinkAPIClient testLinkAPIClient = new TestLinkAPIClient(Configuration.devKey, Configuration.urlTestLink);
		boolean test = testLinkAPIClient.isConnected;
		
		Iterator<Entry<String, String>> it = mTestCaseResults.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			int positionInPart = countTestsSent[0]%partSize;	//posicao relativa do test no pacote a ser enviado
		
			if(positionInPart==0) {
				countPartsSent[0]++;
				System.out.print(getNow()+" - Sending part "+countPartsSent[0]+"/"+(totalTestCases/partSize+1)+"...");
				startTime = System.nanoTime();
				testsAlreadyPerformed[0] = 0;
			}
			testsOfPart[positionInPart] = new Thread() {
				@Override
				public void run() {
					try {
						//System.out.println(sdf.format(cal.getTime())+" - Preparing test "+(countTestsSent[0]+1)+" of lote "+(totalTestCases/partSize+1));
						String status;
						if(verifyBeforeSending) {
							TestLinkAPIResults test = testLinkAPIClient.getLastExecutionResult(Configuration.projectName,Configuration.testPlanName, (String) pair.getKey());
							status = (String) test.getData(0).get("status");
						}else {
							status = null;
						}
						if(status==null||!status.equals("p")) {
							testLinkAPIClient.reportTestCaseResult(
								Configuration.projectName, 
								Configuration.testPlanName, 
								(String) pair.getKey(), 
								Configuration.buildName, 
								null, 
								(String) pair.getValue());
						}else {
							testsAlreadyPerformed[0]++;
						}
						//System.out.println("Test "+(countTestsSent[0]+1)+" sent to TestLink.");
					} catch (TestLinkAPIException e) {
						e.printStackTrace();
					}
				}
			};
		
			testsOfPart[positionInPart].start();
			Thread.sleep(100);
			countTestsSent[0]++;
		
			//
			if(positionInPart==partSize-1){
				for(int i=0;i<partSize;i++){
					try {
						testsOfPart[i].join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				endTime   = System.nanoTime();
				partTime = (endTime - startTime)/1000000000;
				totalTime+=partTime;
				System.out.print(" (Done) ("+partTime+"s)");
				if(testsAlreadyPerformed[0]!=0) {
					System.out.println(" ("+testsAlreadyPerformed[0]+" tests already performed)");
				}else {
					System.out.println();
				}
			}
	
			if(countTestsSent[0]==totalTestCases){
				for(int i=0;i<positionInPart;i++){
					try {
						testsOfPart[i].join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				endTime   = System.nanoTime();
				partTime = (endTime - startTime)/1000000000;
				totalTime+=partTime;
				Thread.sleep(1000);
				System.out.print(" (Done) ("+partTime+"s)");
				if(testsAlreadyPerformed[0]!=0) {
					System.out.println(" ("+testsAlreadyPerformed[0]+" tests already performed)");
				}else {
					System.out.println();
				}
				
				System.out.println(getNow()+" - Finished! (Total "+totalTime+"s)");
			}
		}
		
		//clearFileResults();
	}
	
	public static boolean clearFileResults() {
		try {
			PrintWriter writer = new PrintWriter(getResultFile());
			writer.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static File getResultFile() {
		return new File("results\\results.json");
	}
	
	public static File getResultBackupFile() throws IOException {
		String reportPath = FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "\\" + Configuration.releaseId + "\\Testlink results (json)";
		File directorio = new File(reportPath);
		if(!directorio.exists()) {
			directorio.mkdirs();
		}
		String resultPath = reportPath+"\\results_"+timeInitExecution+".json";
		File result = new File(resultPath);
		if(result.exists()) {
			return result;
		}else {
			result.createNewFile();
			return result;
		}
		
	}
	
	public static void addResult(String testId, String result) {
		mTestCaseResults.put(testId, result);
	}
	
	public static void addResultCurrentTest(String result) {
		if (mTestLinkIdCurrentTest == null) {
			return;
		}
		mTestCaseResults.put(mTestLinkIdCurrentTest, result);
	}
	
	@SuppressWarnings("unchecked")
	public static void readResultsFromFile() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			File resultsFile = getResultFile();

			if (resultsFile.length() != 0) {
				mTestCaseResults = mapper.readValue(resultsFile, HashMap.class);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void saveResultsToFile() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			//System.out.println(mapper.writeValueAsString(mTestCaseResults));
			mapper.writeValue(getResultFile(), mTestCaseResults);
			mapper.writeValue(getResultBackupFile(), mTestCaseResults);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getNow() {
		Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
	}
}
