package com.lge.gpri.helpers.report;


import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.swing.filechooser.FileSystemView;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Report {
	private ExtentHtmlReporter mHtmlReporter;
	private ExtentReports mExtentReport;
	private ExtentTest mCurrentTestScenary;
	private ExtentTest mCurrentTestCase;
	private Map<String, ExtentTest> mTestScenaries;
	private String reportName;
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	
	public Report(String projectName) {
		String reportPath = FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "\\" + Configuration.releaseId + "\\Extent (html)";
		String junitExportPath = FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "\\" + Configuration.releaseId + "\\Junit exports (xml)";
		File directorio = new File(reportPath);
		if(!directorio.exists()) {
			directorio.mkdirs();
		}
		File directorioExport = new File(junitExportPath);
		if(!directorioExport.exists()) {
			directorioExport.mkdirs();
		}
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		reportName = projectName;
		String reportFileName = reportPath + "\\" + reportName + "_" + sdf.format(timestamp) + ".html" ;
		System.out.println("Report will be saved in the following directory: " + reportFileName);
		
		mHtmlReporter = new ExtentHtmlReporter(reportFileName);
		mHtmlReporter.config().setReportName(projectName);
		mHtmlReporter.config().setDocumentTitle(projectName);
		mHtmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
		mHtmlReporter.config().setChartVisibilityOnOpen(true);
		mHtmlReporter.config().setTheme(Theme.STANDARD);
		mHtmlReporter.config().setEncoding("utf-8");
		mHtmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		
		mExtentReport = new ExtentReports();
		mExtentReport.setAnalysisStrategy(AnalysisStrategy.SUITE);
        mExtentReport.attachReporter(mHtmlReporter);
        
        mTestScenaries = new HashMap<String, ExtentTest>();
	}
	
	public String getReportName() {
		return this.reportName;
	}
	
	public ExtentTest getTestScenary(String testScenaryName) {
		return mTestScenaries.get(testScenaryName);
	}
	
	public Map<String, ExtentTest> getTestScenaries() {
		return mTestScenaries;
	}
	
	public void addTestScenary(String testScenaryName) {
		mCurrentTestScenary = mExtentReport.createTest(testScenaryName);
		mTestScenaries.put(testScenaryName, mCurrentTestScenary);
	}
	
	public void addTestCaseCurrentTestScenary(String testCaseName) {
		mCurrentTestCase = mCurrentTestScenary.createNode(testCaseName);
	}
	
	public void setResultCurrentTest(Result result, String description, String stackTrace) {
		switch (result) {
			case SUCCESS:
				mCurrentTestCase.pass("Description: " + description);
				break;
			case EXCEPTION:
				mCurrentTestCase.fatal("Description: " + description);
				mCurrentTestCase.info("Stack Trace: " + stackTrace);
				break;
			case ASSERT_ERROR:
				mCurrentTestCase.fail("Description: " + description);
				break;
			case SKIPPED:
				mCurrentTestCase.skip("Description: " + description);
				break;
			default:
				break;
		}
	}
	
	public void writeResults() {
		mExtentReport.flush();
	}
}
