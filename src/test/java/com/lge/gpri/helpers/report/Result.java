package com.lge.gpri.helpers.report;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public enum Result implements Serializable {
    SUCCESS,
    EXCEPTION,
    ASSERT_ERROR,
    SKIPPED;

    private static List<Result> resultList = Arrays.asList(
    		Result.SUCCESS,
    		Result.EXCEPTION,
    		Result.ASSERT_ERROR,
    		Result.SKIPPED
    );

    public static List<Result> getReasons() {
        return resultList;
    }

    @Override
    public String toString() {
        switch (this) {
            case SUCCESS:
                return "The test case passed.";
            case EXCEPTION:
                return "The test case failed due to an exception.";
            case ASSERT_ERROR:
                return "The test case failed due to an assertiton error.";
            case SKIPPED:
            	return "The test case was skipped.";
            default:
                return "Unknown";
        }
    }
}