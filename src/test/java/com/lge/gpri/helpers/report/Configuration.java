package com.lge.gpri.helpers.report;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configuration {
	public static String urlTestLink;
	public static String devKey;
	public static String testPlanName;
	public static String buildName;
	public static String projectName;
	public static String projectLabel;
	
	public static String releaseId;
	
	public static void initFromFile() {
		Properties properties = getPropertiesFile("configuration.properties");
		urlTestLink = properties.getProperty("urlTestLink");
		devKey = properties.getProperty("devKey");
		testPlanName = properties.getProperty("testPlanName");
		buildName = properties.getProperty("buildName");
		projectName = properties.getProperty("projectName");
		projectLabel = properties.getProperty("projectLabel");
		releaseId = "Automation "+projectName+" - "+testPlanName+" - "+buildName;
	}
	
	public static Properties getPropertiesFile(String fileName) {
		Properties propertieFile = new Properties();
		
		try{
			InputStream input = new FileInputStream("src\\test\\resources\\configuration.properties");
			propertieFile.load(input);
			input.close();
		}catch(IOException e){
			System.out.println(e.getMessage());
		}
		
		return propertieFile;
	}
}
