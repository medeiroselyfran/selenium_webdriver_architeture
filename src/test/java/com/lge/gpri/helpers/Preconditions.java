package com.lge.gpri.helpers;

import org.openqa.selenium.WebDriver;

import com.lge.gpri.screens.models.ScreenPerformListModel;

public class Preconditions {
	
	public WebDriver driver;
	
	public Preconditions(WebDriver driver) {
		this.driver = driver;
	}
	
	public String preconditionRestoreModel16GB2() {
		String value = "";
		try {
			ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(driver);
			screenPerformListModel.preRestoreModel16GB2();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}
}
