package com.lge.gpri.screens.dashboard;

import org.openqa.selenium.WebDriver;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
 * Perform Home Screen Dashboard page class.
 * 
 * <p>
 * Description: Methods to realize the tests.
 * 
 * @author Gabriel Costa do Nascimento
 */

public class ScreenPerformDashboard extends AbstractScreen {

	public ScreenPerformDashboard(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_DASHBOARD);
	}

	public void openPage() {
		getDriver().get("http://136.166.96.136:8204/");
		getDriver().manage().window().maximize();
	}

	public void clickOnActivityStream() throws Exception {
		clickOnButton(map.getLocator("chkActivityStream"));
	}

	public boolean verifyShowActivityStreamDashboard() throws Exception {
		return getDriver().findElement(map.getLocator("activityStreamDashboard")).isDisplayed();
	}

	public void clickOnCloseInWhatsNewPopUp() throws Exception {
		clickOnButton(map.getLocator("whatsNewBtnClose"));
	}
}
