package com.lge.gpri.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;

/**
* Perform Menu page class.
* 
* <p>Description: Methods to perform in Menu
*  
* @author Gabriel Costa do Nascimento
*/

public class SectionPerformMenu extends AbstractScreen{
	
	public SectionPerformMenu(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_MENU);
	}
	
	public void closeNewReleaseInfo(){
		try{
			if(existsElement(By.xpath("/html/body/div/div/div/button[text()='Close']"))){
				WebElement close = getDriver().findElement(By.xpath("/html/body/div/div/div/button[text()='Close']"));
				close.click();
			}
		}catch (Exception e) {
		}
	}
	
	public void closeNewReleaseInfoByXButton(){
		try{
			if(existsElement(By.xpath("/html/body/div[5]/div[1]/button"))){
				WebElement close = getDriver().findElement(By.xpath("/html/body/div[5]/div[1]/button"));
				close.click();
			}
		}catch (Exception e) {
		}
	}
	
	public void closeNewReleaseNotes(){
		try{
			elementFocus();
			WebElement close = getDriver().findElement(By.xpath("//*[@id=\"dlgWhatsNew\"]/following-sibling::div/div/button[text()='Close']"));
			close.click();
		}catch (Exception e) {
		}
	}
	
	public void clickOnMenuView() throws Exception{
		waitLoading();
		waitLoadingOverlay();
		clickOnButton(map.getLocator("btnView"));
	}
	
	public void clickOnMenuBatchEdit() throws Exception {
		clickOnButton(map.getLocator("btnTools"));
		clickOnButton(map.getLocator("subBatchEdit"));
	}
	
	public void clickOnMenuCompare() throws Exception {
		clickOnButton(map.getLocator("btnTools"));
		clickOnButton(map.getLocator("subCompare"));
	}
	
	public void clickOnMenuItemInspector() throws Exception {
		clickOnButton(map.getLocator("btnTools"));
		clickOnButton(map.getLocator("subItemInspector"));
	}
	
	public void clickOnMenuBuyer() throws Exception {
		clickOnButton(map.getLocator("btnProducts"));
		clickOnButton(map.getLocator("subBuyers"));
	}
	
	public void clickOnMenuModel() throws Exception {
		clickOnButton(map.getLocator("btnProducts"));
		clickOnButton(map.getLocator("subModels"));
	}
	
	public void clickOnMenuPackage() throws Exception{
		clickOnButton(map.getLocator("btnProducts"));
		clickOnButton(map.getLocator("subPackages"));
	}
	
	public void clickOnMenuRequests() throws Exception {
		clickOnButton(map.getLocator("btnExpresso"));
		clickOnButton(map.getLocator("subRequests"));
	}
	
	public void clickOnMenuPermalinks() throws Exception {
		clickOnButton(map.getLocator("btnExpresso"));
		clickOnButton(map.getLocator("subPermalinks"));
	}
	
	public void clickOnMenuDetails() throws Exception {
		clickOnButton(map.getLocator("btnExpresso"));
		clickOnButton(map.getLocator("subDetails"));
	}
	
	public void clickOnMenuUserManagement() throws Exception {
		clickOnButton(map.getLocator("btnReports"));
		clickOnButton(map.getLocator("subUserManagement"));
	}
	
	public void clickOnMenuReviewerHistory() throws Exception {
		clickOnButton(map.getLocator("btnReports"));
		clickOnButton(map.getLocator("subReviewerHistory"));
	}
	
	public void clickOnMenuItemChangeLog() throws Exception {
		clickOnButton(map.getLocator("btnReports"));
		clickOnButton(map.getLocator("subItemChangeLog"));
	}
	
	public void clickOnMenuPackagesReport() throws Exception {
		clickOnButton(map.getLocator("btnReports"));
		clickOnButton(map.getLocator("subPackagesReport"));
	}
	
	public void clickOnMenuPackagesChangeLog() throws Exception {
		clickOnButton(map.getLocator("btnReports"));
		clickOnButton(map.getLocator("subPackagesChangeLog"));
	}
	
	public void clickOnMenuApprovalPendingItems() throws Exception {
		clickOnButton(map.getLocator("btnReports"));
		clickOnButton(map.getLocator("subApprovalPendingItems"));
	}
	
	public void clickOnMenuActiveSessions() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subActiveSessions"));
	}
	
	public void clickOnMenuApplicationList() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subApplicationList"));
	}

	public void clickOnMenuCountryAndOperatorShortcuts() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subCountryAndOperatorShortcuts"));
	}
	
	public void clickOnMenuGlobalGroups() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subGlobalGroups"));
	}
	
	public void clickOnMenuItemLibrary() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subItemLibrary"));
	}
	
	public void clickOnMenuNotifications() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subNotifications"));
	}
	
	public void clickOnMenuRegions() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subRegions"));
	}
	
	public void clickOnMenuTags() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subTags"));
	}
	
	public void clickOnMenuValidators() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subValidators"));
	}
	
	public void clickOnMenuWarnings() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subWarnings"));
	}
	
	public void clickOnMenuConfiguration() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subConfiguration"));
	}
	
	public void clickOnMenuAutocommits() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subAutocommits"));
	}
	
	public void clickOnMenuSmartCAs() throws Exception {
		clickOnButton(map.getLocator("btnConfigurations"));
		clickOnButton(map.getLocator("subSmartCAs"));
	}
	
	public void clickOnMenuHelp() throws Exception{
		clickOnButton(map.getLocator("btnHelp"));
	}
	
	public void clickOnExpandSideMenuButton() throws Exception{
		clickOnButton(map.getLocator("btnExpandSideMenu"));
		Thread.sleep(1000);
	}
	
	public void clickOnFollows() throws Exception{
		clickOnButton(map.getLocator("itemFollows"));
	}
	
	public void clickOnItemLibrary() throws Exception{
		clickOnButton(map.getLocator("btnSettings"));
		clickOnButton(map.getLocator("optionItemLibrary"));
	}
	
	public void clickOnTags() throws Exception{
		clickOnButton(map.getLocator("btnSettings"));
		clickOnButton(map.getLocator("optionTags"));
	}
	
	public String getUserLogged() throws Exception{
		return getDriver().findElement(map.getLocator("userLogged")).getText();
	}
	
	public String getCurrentURL(){
		return getDriver().getCurrentUrl();
	}
	
}