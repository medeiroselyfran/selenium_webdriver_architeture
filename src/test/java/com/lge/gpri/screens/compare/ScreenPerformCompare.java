package com.lge.gpri.screens.compare;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
* Perform Compare page class.
* 
* <p>Description: Methods to realize the tests.
*  
* @author Gabriel Costa do Nascimento
*/

public class ScreenPerformCompare extends AbstractScreen{
	
	public ScreenPerformCompare(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_COMPARE);
	}
	
	public void openPage() {
		getDriver().get("http://136.166.96.136:8204/Views/Compare_GPRI/");
		getDriver().manage().window().maximize();
	}

	/**
	 * This methods fill all fields in PRI
	 * Obs.: for select all autoProfiles the entry autoProfiles should be Arrays.asList("All")
	 */
	public void fillPriOne(String buyer, String model, String _package, String revision, List<String> autoProfiles) throws Exception { 
		fillPriGeneric("PRI One",buyer,model,_package,revision,autoProfiles);
	}
	public void fillPriTwo(String buyer, String model, String _package, String revision, List<String> autoProfiles) throws Exception { 
		fillPriGeneric("PRI Two",buyer,model,_package,revision,autoProfiles);
	}
	public void fillPriThree(String buyer, String model, String _package, String revision, List<String> autoProfiles) throws Exception { 
		fillPriGeneric("PRI Three",buyer,model,_package,revision,autoProfiles);
	}
	private void fillPriGeneric(String priType, String buyer, String model, String _package, String revision, List<String> autoProfiles) throws Exception {
		setFieldInSomePri(priType,"Buyer",buyer);
		setFieldInSomePri(priType,"Model",model);
		setFieldInSomePri(priType,"Package",_package);
		setFieldInSomePri(priType,"Revision",revision);
		setAutoProfileInSomePri(priType,autoProfiles);
	}
	
	/**
	 * This methods fill the field specified
	 */
	public void setBuyerPriOne(String entry) throws Exception { 
		setFieldInSomePri("PRI One","Buyer",entry);
	}
	public void setModelPriOne(String entry) throws Exception { 
		setFieldInSomePri("PRI One","Model",entry);
	}
	public void setPackagePriOne(String entry) throws Exception { 
		setFieldInSomePri("PRI One","Package",entry);
	}
	public void setRevisionPriOne(String entry) throws Exception { 
		setFieldInSomePri("PRI One","Revision",entry);
	}
	public void setAutoProfilePriOne(List<String> entries) throws Exception { 
		setAutoProfileInSomePri("PRI One",entries);
	}
	public void setBuyerPriTwo(String entry) throws Exception { 
		setFieldInSomePri("PRI Two","Buyer",entry);
	}
	public void setModelPriTwo(String entry) throws Exception { 
		setFieldInSomePri("PRI Two","Model",entry);
	}
	public void setPackagePriTwo(String entry) throws Exception { 
		setFieldInSomePri("PRI Two","Package",entry);
	}
	public void setRevisionPriTwo(String entry) throws Exception { 
		setFieldInSomePri("PRI Two","Revision",entry);
	}
	public void setAutoProfilePriTwo(List<String> entries) throws Exception { 
		setAutoProfileInSomePri("PRI Two",entries);
	}
	public void setBuyerPriThree(String entry) throws Exception { 
		setFieldInSomePri("PRI Three","Buyer",entry);
	}
	public void setModelPriThree(String entry) throws Exception { 
		setFieldInSomePri("PRI Three","Model",entry);
	}
	public void setPackagePriThree(String entry) throws Exception { 
		setFieldInSomePri("PRI Three","Package",entry);
	}
	public void setRevisionPriThree(String entry) throws Exception { 
		setFieldInSomePri("PRI Three","Revision",entry);
	}
	public void setAutoProfilePriThree(List<String> entries) throws Exception { 
		setAutoProfileInSomePri("PRI Three",entries);
	}
	private void setFieldInSomePri(String priType, String nameField, String entry) throws Exception {
		String priContentPath = getPathFromBy(map.getLocatorVar("priContent", priType));
		String optionPath = priContentPath + getPathFromBy(map.getLocatorVar("optionInPriContentRelative",nameField));
		String fieldPath = priContentPath + getPathFromBy(map.getLocatorVar("fieldInPriContentRelative",nameField));
		By optionLocator = By.xpath(optionPath);
		By fieldLocator = By.xpath(fieldPath);
		clickOnButton(optionLocator);
		if(!entry.equals("LAST")){
			if(entry.isEmpty()){
				clickOnButton(optionLocator);
			}else{
				setTextFieldDropdown(fieldLocator, entry);
			}
		}else{
			String pathRelativeLastItem = getPathFromBy(map.getLocator("lastItemSelect"));
			String pathToLastItem = fieldPath+pathRelativeLastItem;
			By lastLocator = By.xpath(pathToLastItem);
			clickOnButton(lastLocator);
		}
		
	}
	
	/**
	 * To select all autoProfiles the entries param should be Arrays.asList("All")
	 */
	private void setAutoProfileInSomePri(String priType, List<String> entries) throws Exception {
		String priContentPath = getPathFromBy(map.getLocatorVar("priContent", priType));
		String optionPath = priContentPath + getPathFromBy(map.getLocatorVar("optionInPriContentRelative","Auto Profile"));
		By optionLocator = By.xpath(optionPath);
		if(entries.get(0).equals("All")){
			clickOnButton(optionLocator);
			String selectAllPath = priContentPath + getPathFromBy(map.getLocatorVar("selectAllAutoProfileInPriContentRelative","Auto Profile"));
			By selectAllLocator = By.xpath(selectAllPath);
			clickOnButton(selectAllLocator);
		}else{
			String fieldPath = priContentPath + getPathFromBy(map.getLocatorVar("fieldInPriContentRelative","Auto Profile"));
			String deselectAllPath = priContentPath + getPathFromBy(map.getLocatorVar("deselectAllAutoProfileInPriContentRelative","Auto Profile"));
			By fieldLocator = By.xpath(fieldPath);
			By deselectAllLocator = By.xpath(deselectAllPath);
			clickOnButton(optionLocator);
			clickOnButton(deselectAllLocator);
			for(int i=0;i<entries.size();i++){
				setTextFieldDropdown(fieldLocator, entries.get(i));
			}
		}
		clickOnButton(optionLocator);
	}

	public boolean clickOnCompare() throws Exception {
		return clickOnButton(map.getLocator("btnCompare"));
	}
	
	public boolean clickOnClear() throws Exception {
		return clickOnButton(map.getLocator("btnClear"));
	}
	
	/**
	 * Return the xpath of /tr element of the tree table given the path of the element
	 * The given path should be the same when right-clicked on the item and then copy the path
	 */
	private String getXPathInTreeTable(String pathUnic) throws Exception{
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocatorVar("tabTableBody",path[0]));
		for(int i=1;i<path.length-1;i++){
			pathToElement+=getPathFromBy(map.getLocatorVar("hierarchyTreeRelative",path[i]));
		}
		pathToElement+=getPathFromBy(map.getLocatorVar("childTreeRelative", path[path.length-1]));
		return pathToElement;
	}

	public String getNameOnResult(String pathToItem) throws Exception {
		waitLoading();
		String pathTrItem = getXPathInTreeTable(pathToItem);
		String pathToName = pathTrItem+getPathFromBy(map.getLocator("nameItemRelative"));
		By locatorItem = By.xpath(pathToName);
		if(existsElement(locatorItem)){
			return getDriver().findElement(locatorItem).getText();
		}
		return null;
	}

	public String getValuePriOneOnResult(String pathToItem) throws Exception {
		waitLoading();
		String pathTrItem = getXPathInTreeTable(pathToItem);
		String pathToValue = pathTrItem+getPathFromBy(map.getLocator("valuePriOneItemRelative"));
		By locatorItem = By.xpath(pathToValue);
		if(existsElement(locatorItem)){
			return getDriver().findElement(locatorItem).getText();
		}
		return null;
	}

	public String getValuePriTwoOnResult(String pathToItem) throws Exception {
		waitLoading();
		String pathTrItem = getXPathInTreeTable(pathToItem);
		String pathToValue = pathTrItem+getPathFromBy(map.getLocator("valuePriTwoItemRelative"));
		By locatorItem = By.xpath(pathToValue);
		if(existsElement(locatorItem)){
			return getDriver().findElement(locatorItem).getText();
		}
		return null;
	}

	public String getValuePriThreeOnResult(String pathToItem) throws Exception {
		waitLoading();
		String pathTrItem = getXPathInTreeTable(pathToItem);
		String pathToValue = pathTrItem+getPathFromBy(map.getLocator("valuePriThreeItemRelative"));
		By locatorItem = By.xpath(pathToValue);
		if(existsElement(locatorItem)){
			return getDriver().findElement(locatorItem).getText();
		}
		return null;
	}

	/**
	 * When there is not result items in Parameters this class return true
	 */
	public boolean isResultParametersDifEmpty() throws Exception {
		waitLoadingOverlay();
		String pathToTableResult = getPathFromBy(map.getLocator("tableResultVisible"));
		if(existsElement(By.xpath(pathToTableResult))){
			return false;
		}
		return true;
	}

	/**
	 * To copy parameters from PRI One to PRI Two,
	 * the follow methods do the same changing PRI number
	 */
	public void copyParametersOneTwo() throws Exception {
		clickOnButton(map.getLocator("btnCopyParametersOneTwo"));
	}
	public void copyParametersTwoOne() throws Exception {
		clickOnButton(map.getLocator("btnCopyParametersTwoOne"));
	}
	public void copyParametersTwoThree() throws Exception {
		clickOnButton(map.getLocator("btnCopyParametersTwoThree"));
	}
	public void copyParametersThreeTwo() throws Exception {
		clickOnButton(map.getLocator("btnCopyParametersThreeTwo"));
	}

	public void clickOnOnlyAutoCommit() throws Exception {
		clickOnButton(map.getLocator("onlyAutoCommit"));
	}

	public void clickOnOnlyDiffs() throws Exception {
		clickOnButton(map.getLocator("onlyDiffs"));
	}

	public void openOptions() throws Exception {
		moveToPageHeader();
		clickOnButton(map.getLocator("btnOptions"));
	}

	public void clickOnPermanentLinkOption() throws Exception {
		clickOnButton(map.getLocator("permanentLinkOption"));
	}
	
	public boolean downloadAsSpreadsheet() throws Exception{
		boolean result = false;
		clickOnButton(map.getLocator("downloadAsSpreadsheet"));
		waitLoading();
		moveToPageBottom();
		WebElement export = getDriver().findElement(map.getLocator("btnExport"));
		if(export != null){
			export.click();
			result = true;
		}
		return result;
	}

	public void clickOnExpandAll() throws Exception {
		clickOnButton(map.getLocator("btnExpandAll"));
		Thread.sleep(500);
	}

	public void clickOnCollapseAll() throws Exception {
		clickOnButton(map.getLocator("btnCollapseAll"));
		Thread.sleep(500);
	}

	public void clickOnCenterValues() throws Exception {
		clickOnButton(map.getLocator("btnCenterValues"));
		Thread.sleep(1000);
	}
	
	public void setNameFilter(String entry) throws Exception {
		waitLoading();
		setTextField(map.getLocatorVar("fieldNameFilter"), entry);
		getDriver().findElement(map.getLocatorVar("fieldNameFilter")).sendKeys(Keys.ENTER);
	}
	
	public void setValueFilter(String entry) throws Exception {
		waitLoading();
		setTextField(map.getLocatorVar("fieldValueFilter"), entry);
		getDriver().findElement(map.getLocatorVar("fieldValueFilter")).sendKeys(Keys.ENTER);
	}

	public boolean isParametersCollapsed() throws Exception {
		return getDriver().findElement(map.getLocator("parametersCollapsed")).getAttribute("class").contains("ttCollapsed");
	}
	
	public boolean isCompareButtonClickable() throws Exception {
		try{
			return clickOnCompare();
		}catch (Exception e) {
			return false;
		}
	}
}
