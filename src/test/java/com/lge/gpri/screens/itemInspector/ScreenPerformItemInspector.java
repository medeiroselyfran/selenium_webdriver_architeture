package com.lge.gpri.screens.itemInspector;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipFile;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsItemInspector;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

/**
* Perform Item Inspector page class.
* 
* <p>Description: Methods to realize the tests.
*  
* @author Gabriel Costa do Nascimento
*/

public class ScreenPerformItemInspector extends AbstractScreen{
	
	public ScreenPerformItemInspector(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_ITEM_INSPECTOR);
	}
	
	public void openPage() {
		getDriver().get("http://136.166.96.136:8204/Views/ItemInspector/Default.aspx");
		getDriver().manage().window().maximize(); 
	}

	/**
	 * The type param should be the same of option in "Select some filters" 
	 */
	public void selectSomeFilter(String type) throws Exception {
		waitLoading();
		if(!type.isEmpty()){
			clickOnButton(map.getLocator("optionSelectSomeFilters"));
			WebElement criteriaList = getDriver().findElement(map.getLocator("listFilter"));
			List<WebElement> criteriaListElements = criteriaList.findElements(By.xpath("li"));
			for(WebElement element: criteriaListElements){
            	if (element.getText().equals(type)) {
            		if(!element.getAttribute("class").equals("selected")){
            			element.click();
            			clickOnButton(map.getLocator("optionSelectSomeFilters"));
            		}
                    return;
                }
            }
		}
	}
	
	/**
	 * The type param should be the same of label showed
	 * The area param is optional, for the case of Tag Values (eg. "Grid Row x Column")
	 * Obs: The filter "Default hidden on Mother" must be set with name "Default Hide on Mother"
	 */
	public void setInFilterValue(String type, String entry, String... area) throws Exception {
		//3 tipos: simple drodown, field, search dropdown
		String pathFilterBase = getPathFromBy(map.getLocatorVar("filterContentBase", type));
		String classField = getDriver().findElement(By.xpath(pathFilterBase)).getAttribute("class");
		if(classField.equals(ConstantsItemInspector.TYPE_FIELD_SELECT)){
			String pathToOptionRelative = getPathFromBy(map.getLocatorVar("fieldRelative",entry));
			clickOnButton(By.xpath(pathFilterBase+"/button"));
			clickOnButton(By.xpath(pathFilterBase+pathToOptionRelative));
		}else if(classField.equals(ConstantsItemInspector.TYPE_FIELD_TEXT_NORMAL)){
			setTextField(By.xpath(pathFilterBase), entry);
		}else if(classField.equals(ConstantsItemInspector.TYPE_FIELD_SELECT_MULTI)){
			clickOnButton(By.xpath(pathFilterBase+"/button"));
			setTextFieldDropdown(By.xpath(pathFilterBase+"/div/div/input"), entry);
			clickOnButton(By.xpath(pathFilterBase+"/button"));
		}else if(classField.equals(ConstantsItemInspector.TYPE_FIELD_SELECT_MULTI_GROUPS)){
			clickOnButton(By.xpath(pathFilterBase+"/button"));
			setTextFieldDropdownWithArea(By.xpath(pathFilterBase+"/div/div/input"), area[0], entry);
			clickOnButton(By.xpath(pathFilterBase+"/button"));
		}
	}

	/**
	 * Select item given the path
	 * The given path should be the same when right-clicked on the item and then copy the path
	 * To select a folder, the principle is the same
	 * To select root folder(Item Library) let the path void
	 */
	public void selectItemInTree(String pathUnic) throws Exception{
		waitLoading();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for(int i=0;i<path.length&&!pathUnic.isEmpty();i++){
			pathToElement+=getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative",path[i]));
		}
		boolean isChecked = getDriver().findElement(By.xpath(pathToElement)).getAttribute("aria-selected").equals("true");
		pathToElement+=getPathFromBy(map.getLocator("checkBoxItemLibraryRelative"));
		if(!isChecked){
			clickOnButton(By.xpath(pathToElement));
		}
	}
	
	public void moreInfoRightClickItem(String pathUnic) throws Exception{
		waitLoading();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for(int i=0;i<path.length&&!pathUnic.isEmpty();i++){
			pathToElement+=getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative",path[i]));
		}
		pathToElement+=getPathFromBy(map.getLocator("checkBoxItemLibraryRelative"));
		rightClickOnButton(By.xpath(pathToElement));
		clickOnButton(map.getLocator("moreInfoRightClick"));
	}

	public void selectContryOptions(String entry) throws Exception {
		clickOnButton(map.getLocatorVar("optionOptions","Countries"));
		setTextFieldDropdown(map.getLocatorVar("fieldOptions","Countries"), entry);
		clickOnButton(map.getLocatorVar("optionOptions","Countries"));
	}

	public void selectAllContriesOptions() throws Exception {
		clickOnButton(map.getLocatorVar("optionOptions","Countries"));
		clickOnButton(map.getLocatorVar("selectAllOptions","Countries"));
		clickOnButton(map.getLocatorVar("optionOptions","Countries"));
	}

	public void selectOperatorOptions(String entry) throws Exception {
		clickOnButton(map.getLocatorVar("optionOptions","Operators"));
		setTextFieldDropdown(map.getLocatorVar("fieldOptions","Operators"), entry);
		clickOnButton(map.getLocatorVar("optionOptions","Operators"));
	}

	public void selectAllOperatorsOptions() throws Exception {
		clickOnButton(map.getLocatorVar("optionOptions","Operators"));
		clickOnButton(map.getLocatorVar("selectAllOptions","Operators"));
		clickOnButton(map.getLocatorVar("optionOptions","Operators"));
	}

	public void selectModelOptions(String entry) throws Exception {
		clickOnButton(map.getLocatorVar("optionOptions","Models"));
		setTextFieldDropdown(map.getLocatorVar("fieldOptions","Models"), entry);
		clickOnButton(map.getLocatorVar("optionOptions","Models"));
	}

	public void selectAllModelsOptions() throws Exception {
		clickOnButton(map.getLocatorVar("optionOptions","Models"));
		clickOnButton(map.getLocatorVar("selectAllOptions","Models"));
		clickOnButton(map.getLocatorVar("optionOptions","Models"));
	}

	public void selectGTMLTMOptions(String entry) throws Exception {
		clickOnButton(map.getLocatorVar("optionOptions","GTMs/LTMs"));
		setTextFieldDropdown(map.getLocatorVar("fieldOptions","GTMs/LTMs"), entry);
		clickOnButton(map.getLocatorVar("optionOptions","GTMs/LTMs"));
	}

	public void selectAllGTMsLTMsOptions() throws Exception {
		clickOnButton(map.getLocatorVar("optionOptions","GTMs/LTMs"));
		clickOnButton(map.getLocatorVar("selectAllOptions","GTMs/LTMs"));
		clickOnButton(map.getLocatorVar("optionOptions","GTMs/LTMs"));
	}

	public void clickOnShow() throws Exception {
		clickOnButton(map.getLocator("btnShow"));
		waitLoadingOverlay();
	}

	public boolean valuesTableIsVisible() throws Exception {
		waitLoading();
		return existsElement(map.getLocator("entriePanel"));
	}
	
	public boolean exportExcel() throws Exception{
		boolean result = false;
		clickOnButton(map.getLocator("btnDownloadAsSpreadsheet"));
		waitLoading();
		while(existsElement(map.getLocator("progressBar"))){
			result= true;
			Thread.sleep(1000);
		}
		if(existsElement(map.getLocator("errorPopup"))){
			result = false;
		}
		Thread.sleep(1000);
		return result;
	}

	public void hideOptions() throws Exception {
		waitLoading();
		WebElement hideOption = getDriver().findElement(map.getLocator("btnHideOptions"));
		if(hideOption.getAttribute("class").contains("expanded")){
			clickOnButton(map.getLocator("btnHideOptions"));
			Thread.sleep(1000);
		}
	}
	
	public void showOptions() throws Exception {
		waitLoading();
		WebElement hideOption = getDriver().findElement(map.getLocator("btnHideOptions"));
		if(!hideOption.getAttribute("class").contains("expanded")){
			clickOnButton(map.getLocator("btnHideOptions"));
			Thread.sleep(1000);
		}
	}
	
	public boolean optionsPanelIsOpen() throws Exception{
		return getDriver().findElement(map.getLocator("optionsPanel")).getAttribute("aria-expanded").equals("true");
	}
	
	public boolean filtersPanelIsOpen() throws Exception{
		return getDriver().findElement(map.getLocator("filtersPanel")).getAttribute("aria-expanded").equals("true");
	}
	
	public void hideFilters() throws Exception {
		waitLoading();
		WebElement hideOption = getDriver().findElement(map.getLocator("btnHideFilters"));
		if(hideOption.getAttribute("class").contains("expanded")){
			clickOnButton(map.getLocator("btnHideFilters"));
			Thread.sleep(1000);
		}
	}
	
	public void showFilters() throws Exception {
		waitLoading();
		WebElement hideOption = getDriver().findElement(map.getLocator("btnHideFilters"));
		if(!hideOption.getAttribute("class").contains("expanded")){
			clickOnButton(map.getLocator("btnHideFilters"));
			Thread.sleep(1000);
		}
	}
	
	public void showEntries(String numberOfElements) throws Exception{
		waitLoading();
		By showLocator = map.getLocator("optionShowEntries");
		boolean exists = existsElement(showLocator);
		if(exists&&!numberOfElements.isEmpty()){
			clickOnButton(map.getLocator("optionShowEntries"));
			clickOnButton(map.getLocatorVar("numberShowEntries", numberOfElements));
			clickOnButton(map.getLocator("optionShowEntries"));
		}
	}
	
	public void waitResultItems() throws Exception{
		waitElementToBeVisible(map.getLocator("entriePanel"));
	}
	
	public void paginateNext() throws Exception{
		clickOnButton(map.getLocator("btnNext"));
	}
	
	public void paginatePrevious() throws Exception{
		clickOnButton(map.getLocator("btnPrevious"));
	}
	
	public void goToLastPage() throws Exception{
		clickOnButton(map.getLocator("btnLast"));
	}
	
	public void goToFirstPage() throws Exception{
		clickOnButton(map.getLocator("btnFirst"));
	}
	
	public void clickOnVisiblePage(String choosedPage) throws Exception{
		clickOnButton(map.getLocatorVar("btnPageChoosed",choosedPage));
	}
	
	public void goToChoosedPage(String choosedPage) throws Exception{
		waitLoading();
		if(!choosedPage.isEmpty()){
			goToFirstPage();
			String page = getDriver().findElement(map.getLocator("activePageTable")).getText();
			while(!page.equals(choosedPage)){
				paginateNext();
				waitLoading();
				page = getDriver().findElement(map.getLocator("activePageTable")).getText();
			}
		}
	}
	
	public int getActivePage() throws Exception{
		return Integer.parseInt(getDriver().findElement(map.getLocator("activePageTable")).getText().replace(",", ""));
	}
	
	public int getNumberOfPages() throws NumberFormatException, Exception{
		return getDriver().findElements(map.getLocator("numberOfPages")).size()+1;
	}

	/**
	 * This method search specified entry and return the number of results.
	 * If is no result return 0
	 */
	public int searchInResult(String search) throws Exception {
		setTextField(map.getLocator("searchEntries"), search);
		waitLoadingOverlay();
		return getTotalEntriesShowingInfo();
	}
	
	/**
	 * This methods return respectively the number in the show info in result table
	 * "Showing N to N of N entries"
	 */
	public int getStartEntrieShowingInfo() throws NumberFormatException, Exception{
		return Integer.parseInt(getDriver().findElement(map.getLocator("txtShowingEntriesInfo")).getText().split(" ")[1].replace(",", ""));
	}
	
	public int getEndEntrieShowingInfo() throws NumberFormatException, Exception{
		return Integer.parseInt(getDriver().findElement(map.getLocator("txtShowingEntriesInfo")).getText().split(" ")[3].replace(",", ""));
	}
	
	public int getTotalEntriesShowingInfo() throws NumberFormatException, Exception{
		return Integer.parseInt(getDriver().findElement(map.getLocator("txtShowingEntriesInfo")).getText().split(" ")[5].replace(",", ""));
	}
	
	public int getLineNumberEntriesShow() throws Exception {
		return getDriver().findElements(map.getLocator("linesEntries")).size();
	}
	
	public String getPathFromAlert() throws Exception {
		Thread.sleep(500);
		Alert alert = getDriver().switchTo().alert();
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_C);
		r.keyRelease(KeyEvent.VK_C);
		r.keyRelease(KeyEvent.VK_CONTROL);
		Thread.sleep(100);
		String path = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
		alert.accept();
		Thread.sleep(100);
		return path;
	}

	public ArrayList<String> getFirstItemsOfEachRootDirectory() throws Exception {
		ArrayList<String> paths = new ArrayList<>();
		boolean resultOk;
		By itemParameters = By.xpath("//*[@id='g:0']/ul/li[1]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemCustomization = By.xpath("//*[@id='g:0']/ul/li[2]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemProfiles = By.xpath("//*[@id='g:0']/ul/li[3]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemLibrary = By.xpath("//*[@id='g:0']/ul/li[4]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemSmartCA = By.xpath("//*[@id='g:0']/ul/li[5]//li/a/parent::li[contains(@id,'i:')]/a");
		
		resultOk = rightClickOnButton(itemParameters);
		if(resultOk){
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}
		
		resultOk = rightClickOnButton(itemCustomization);
		if(resultOk){
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}
		
		resultOk = rightClickOnButton(itemProfiles);
		if(resultOk){
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}
		
		resultOk = rightClickOnButton(itemLibrary);
		if(resultOk){
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}
		
		resultOk = rightClickOnButton(itemSmartCA);
		if(resultOk){
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}
		
		return paths;
	}

	/**
	 * return all the values of a given tag in filter tag values
	 * obs: the filter must be selected
	 */
	public ArrayList<String> getAllValuesOfTagValues(String area) throws Exception {
		String pathFilterBase = getPathFromBy(map.getLocatorVar("filterContentBase", ConstantsItemInspector.FILTER_TAG_VALUES));
		String values = getDriver().findElement(By.xpath(pathFilterBase+"/button")).findElement(By.xpath("span")).getText();
		ArrayList<String> result = new ArrayList<>(Arrays.asList(values.split(", ")));
		return result;
	}

	public String getMessageAlert() throws Exception {
		return getDriver().findElement(map.getLocatorVar("msgPopup","Alert")).getText();
	}
	
	/**
	 * The file name must be the concatenate of items, in the order of selection, separate by ", ".
	 * If the number of items is 4, must be: "item1, item2, item3 and other"
	 * If the number of items is more than 4 <n>, must be: "item1, item2, item3 and <n>-3 others", 
	 */
	public String getContentExcel (String fileName) throws Exception {
		String content = "";
		int count=0;
		Workbook workbook = Workbook.getWorkbook(getFileFromZipDownloaded(fileName));
		Sheet sheet = workbook.getSheet(0);
		Cell cell = sheet.getCell(1, 2);
		while(cell.getContents().contains("Item:")){
			content+= cell.getContents()+"\n";
			count++;
			cell = sheet.getCell(1, 2+count);
		}
		workbook.close();
		return content;
	}
	
	public File getFileFromZipDownloaded (String fileName) throws Exception {
		File zipFile = new File("C:/Users/"+ConstantsGeneric.USER+"/Downloads/"+fileName+".zip");
		ZipFile zip = new ZipFile(zipFile);
		InputStream is = zip.getInputStream(zip.getEntry(fileName+".xls"));
	    byte[] buffer = new byte[is.available()];
	    is.read(buffer);
	    File targetFile = File.createTempFile("src/test/resources/excelTemp", "xls");
	    targetFile.deleteOnExit();
	    OutputStream outStream = new FileOutputStream(targetFile);
	    outStream.write(buffer);
	    outStream.close();
	    zip.close();
		return targetFile;
	}

	public void deleteFileDownloaded(String fileName) throws IOException {
		Path path = Paths.get("C:/Users/"+ConstantsGeneric.USER+"/Downloads/"+fileName+".zip");
		Files.delete(path);
	}

	/**
	 * If params region equals "", return all reviews of all regions
	 */
	public String getReviewersFromMoreInfo(String region) throws Exception {
		String reviewers = getDriver().findElement(map.getLocator("reviewerMoreInfo")).getText();
		if(!region.equals("") && reviewers.contains(region)){
			reviewers = reviewers.substring(reviewers.indexOf(region), reviewers.length());
		}
		String[] revs = reviewers.split("\n ");
		String finalRevs = "";
		for(int i=1; i<revs.length; i++){
			if(revs[i].contains(")")){
				finalRevs = finalRevs + revs[i];
			}else{
				break;
			}
		}
		return finalRevs;
	}
}
