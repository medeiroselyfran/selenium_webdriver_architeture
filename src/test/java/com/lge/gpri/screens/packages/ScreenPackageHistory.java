package com.lge.gpri.screens.packages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
* Perform package history page class.
* 
* <p>Description: Methods to test in the package history.
*  
* @author AUTHOR
*/

public class ScreenPackageHistory extends AbstractScreen{
	
	public ScreenPackageHistory(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_PACKAGE);
		PageFactory.initElements(getDriver(), this);
	}
	
	public boolean AmountRecordStatusHistoryTable() throws Exception {
		boolean istrue = false;
		List<String> currentTab = new ArrayList<String>(getDriver().getWindowHandles());
		WebDriver newTab = getDriver().switchTo().window(currentTab.get(1));
		WebElement statusHistoryTableTbody = newTab.findElement(map.getLocator("statusHistoryTableTbody"));
		List<WebElement> allRowsStatusHistoryTable = statusHistoryTableTbody.findElements(By.tagName("tr"));
		String valueShow = newTab.findElement(map.getLocator("numberOfElementDisplayInTable")).getAttribute("value");
		String[] firstPart = newTab.findElement(map.getLocator("footOfTableInfo")).getText().split("Showing 1 - ");
		String secondPart = firstPart[1];
		String[] split = secondPart.split("of 111");
		String showTotalRecords = split[0];
		boolean firstValue = valueShow.equals(String.valueOf(allRowsStatusHistoryTable.size()));
		boolean secondValue = valueShow.equals(showTotalRecords.trim());
		if (firstValue && secondValue) {
			istrue = true;
		}
		return istrue;
	}

	public boolean showTableElementsForStatusHistoryPage(String numberOfElements) throws Exception {
		boolean result = false;
		waitLoading();
		if (!numberOfElements.isEmpty()) {
			getDriver().findElement(map.getLocator("numberOfElementDisplayInTable")).click();
			getDriver().findElement(map.getLocator(numberOfElements)).click();
			result = true;
		}
		return result;
	}

	public String getSecondOldStatus() throws Exception {
		return getTextFrom("secondtOldStatus");
	}
	
	public String getSecondNewStatus() throws Exception {
		return getTextFrom("secondNewStatus");
	}
	
	public String getFirstOldStatus() throws Exception {
		return getTextFrom("firstOldStatus");
	}
	
	public String getFirstNewStatus() throws Exception {
		return getTextFrom("firstNewStatus");
	}
	
	public void clickOnFirstPermalinks() throws Exception{
		clickOnButton(map.getLocator("permalinksAction"));
	}
	
	public void clickOnFirstDetails() throws Exception{
		clickOnButton(map.getLocator("detailsAction"));
	}
	
	public String getMsgPackagePermalinks() throws Exception{
		waitLoading();
		waitLoadingOverlay();
		return getDriver().findElement(map.getLocatorVar("msgPopup","Package Permalinks")).getText();
	}

	public void viewRequestDetailsFromAutocommit(String autocommit) throws Exception {
		clickOnButton(map.getLocatorVar("detailsAutocommit", autocommit));
	}

	public String getDetailsAutocommitFromStatus(String status) throws Exception {
		Thread.sleep(1000);
		return getDriver().findElement(map.getLocatorVar("detailsStatusAutocommit", status)).getText();
	}

	public boolean allValidatingErrorAutocommit() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		int numberErrors = getDriver().findElements(map.getLocator("containsValidationError")).size();
		int numberTotal = getDriver().findElements(map.getLocator("numberOfAutocommits")).size();
		return numberErrors==numberTotal;
	}
	
	public void showTableElements(String numberOfElements) throws Exception {
		waitLoading();
		if (!numberOfElements.isEmpty()) {
			clickOnButton(map.getLocator("numberOfLinesShow"));
			clickOnButton(map.getLocatorVar("optionNumberOfLinesShow", numberOfElements));
			clickOnButton(map.getLocator("numberOfLinesShow"));
		}
	}
	
	public int getLinesQuantity() throws Exception {
		int quantityLines = getDriver().findElements(map.getLocator("listAllLines")).size();
		return quantityLines;
	}
	
	public String[] getShowQuantityOfLinesOnTheFootTable() {
		String infoModelsShow = getDriver().findElement(By.id("status-history-table_info")).getText();
		String[] split = infoModelsShow.split(" - ");
		String[] modelsQuantity = split[1].split(" of ");
		return modelsQuantity;
	}
	
	public String getMessageForNoHistoriesAvailable() throws Exception {
		return getDriver().findElement(map.getLocator("txtNoHistoriesAvailable")).getText();
	}

	public void setFilter(String entry) throws Exception {
		waitLoading();
		if (!entry.isEmpty()) {
			setTextField(map.getLocator("searchInputHistory"), entry);
		}
	}

	public int getLinesQuantityWiothSearch(String search) throws Exception {
		return getDriver().findElements(map.getLocatorVar("linesContainsSearch", search)).size();
	}
	
	public boolean paginateNext() throws Exception{
		waitLoading();
		moveToPageBottom();	
		boolean enabled = !getDriver().findElement(map.getLocator("btnNextHistory")).findElement(By.xpath("parent::li")).getAttribute("class").contains("disabled");
		if(enabled){
			clickOnButton(map.getLocator("btnNextHistory"));
		}
		return enabled;
	}

	public void paginatePrevious() throws Exception {
		waitLoading();
		moveToPageBottom();
		clickOnButton(map.getLocator("btnPreviousHistory"));
	}

	public void goToLastPage() throws Exception {
		waitLoading();
		moveToPageBottom();
		clickOnButton(map.getLocator("btnLastHistory"));
	}

	public void goToFirstPage() throws Exception {
		waitLoading();
		moveToPageBottom();
		clickOnButton(map.getLocator("btnFirstHistory"));
	}

	public void goToChoosedPage(String choosedPage) throws Exception {
		waitLoading();
		moveToPageBottom();
		if (!choosedPage.isEmpty()) {
			goToFirstPage();
			String page = getDriver().findElement(map.getLocator("activePageTableHistory")).getText();
			while (!page.equals(choosedPage)) {
				paginateNext();
				waitLoading();
				page = getDriver().findElement(map.getLocator("activePageTableHistory")).getText();
			}
		}
	}
	
	public int getActivePage() throws Exception{
		return Integer.parseInt(getDriver().findElement(map.getLocator("activePageTableHistory")).getText().replace(",", ""));
	}
	
	public int getNumberOfPages() throws NumberFormatException, Exception{
		return getDriver().findElements(map.getLocator("numberOfPagesHistory")).size()+1;
	}
	
	public String getNumberOfLastPage() throws NumberFormatException, Exception{
		return getDriver().findElement(map.getLocator("numberOfLastPage")).getText();
	}
}
