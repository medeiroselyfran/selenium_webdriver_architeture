package com.lge.gpri.screens.packages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.AbstractScreen;

/**
 * Perform create and edit normal packages page class.
 * 
 * <p>
 * Description: Methods to create and edit a package.
 * 
 * @author Cayk Lima Barreto
 */

public abstract class ScreenPerformAbstractPackage extends AbstractScreen {

	public ScreenPerformAbstractPackage(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_PACKAGE);
	}

	public void setValueFromAnotherPackage() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("anotherPackage"));
	}

	public void setModel(String model) throws Exception {
		waitLoading();
		if (!model.isEmpty()) {
			clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Model and Type", "Model"));
			setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Model and Type", "Model"), model);
		}
	}

	public String getModel() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocatorVar("inputGenericOnCreatePackage", "Model and Type", "Model"))
				.getAttribute("value").split("\n")[0];
	}

	public void setTypeNormalPackage() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("radioNormalPackage"));
	}

	public void setTypeSmartCAPackage() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("radioSmartCAPackage"));
	}

	public String getType() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		WebElement normalRadio = getDriver().findElement(map.getLocator("radioNormalPackage"));
		WebElement smartRadio = getDriver().findElement(map.getLocator("radioSmartCAPackage"));
		if (normalRadio.isSelected()) {
			return ConstantsPackages.NORMAL;
		} else if (smartRadio.isSelected()) {
			return ConstantsPackages.SMART_CA;
		}
		return null;
	}

	public void setOption(String option) throws Exception {
		waitLoading();
		if (!option.isEmpty())
			clickOnButton(map.getLocator(option));
	}

	/**
	 * If param buyer is "FIRST" the buyer selected will be the first active of the
	 * list
	 */
	public void setBuyerValues(String buyer) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Values", "Buyer"));
		if (buyer.equals("FIRST")) {
			clickOnButton(map.getLocatorVar("firstItemActiveOnDropdownGenericOnCreatePackage", "Values", "Buyer"));
		} else if (buyer.equals("SECOND")) {
			clickOnButton(map.getLocatorVar("secondItemActiveOnDropdownGenericOnCreatePackage", "Values", "Buyer"));
		} else {
			setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Values", "Buyer"), buyer);
		}
	}

	public void setModelValuesFromAnotherPackage(String model) throws Exception {
		waitLoading();
		if (!model.isEmpty()) {
			clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Values", "Model"));
			if (model.equals("FIRST")) {
				clickOnButton(map.getLocatorVar("firstItemActiveOnDropdownGenericOnCreatePackage", "Values", "Model"));
			} else {
				setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Values", "Model"), model);
			}
		}
	}

	/**
	 * If param buyer is "FIRST" the buyer selected will be the first active of the
	 * list
	 */
	public void setBuyerValuesFromAnotherPackage(String buyer) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage2", "Values", "Buyer"));
		if (buyer.equals("FIRST")) {
			clickOnButton(map.getLocatorVar("firstItemActiveOnDropdownGenericOnCreatePackage2", "Values", "Buyer"));
		} else {
			setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage2", "Values", "Buyer"), buyer);
		}
	}

	public void setPackageValuesFromAnotherPackage(String name) throws Exception {
		waitLoading();
		if (!name.isEmpty()) {
			clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Values", "Package"));
			if (name.equals("FIRST")) {
				clickOnButton(
						map.getLocatorVar("firstItemActiveOnDropdownGenericOnCreatePackage", "Values", "Package"));
			} else {
				setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Values", "Package"), name);
			}
		}
	}

	public String getBuyer() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocatorVar("optionGererciOnCreatePackage", "Values", "Buyer")).getText()
				.split("\n")[0];
	}

	public String getBuyerFirstDropdownBuyer() throws Exception {
		waitLoadingOverlay();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Values", "Buyer"));
		String buyer = getDriver()
				.findElement(map.getLocatorVar("firstItemActiveOnDropdownGenericOnCreatePackage", "Values", "Buyer"))
				.getText();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Values", "Buyer"));
		return buyer;
	}

	public void setPackageName(String packageName) throws Exception {
		setTextField(map.getLocator("fieldPackageName"), packageName);
	}

	public String getPackageName() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("fieldPackageName")).getAttribute("value").split("\n")[0];
	}

	public void setTargetDate(String targetDate) throws Exception {
		setTextDate(map.getLocator("fieldTargetDate"), targetDate);
	}

	public String getTargetDate() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("fieldTargetDate")).getAttribute("value").split("\n")[0];
	}

	public void setUISimulator(String uiSimulatorName) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Basic Informations", "UI Simulator Template"));
		setTextFieldDropdown(
				map.getLocatorVar("inputGenericOnCreatePackage", "Basic Informations", "UI Simulator Template"),
				uiSimulatorName);
	}

	public String getUISimulator() throws Exception {
		waitLoading();
		return getDriver().findElement(
				map.getLocatorVar("optionGererciOnCreatePackage", "Basic Informations", "UI Simulator Template"))
				.getText().split("\n")[0];
	}

	public void setCupssGroup(String cupssGroup) throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionGenericCupssGroup"));
		clickOnButton(map.getLocatorVar("inputGenericCupssGroup", cupssGroup));
	}

	public String getCupssGroup() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionGenericCupssGroup")).getText();
	}

	public void setCreationReasons(String createReasons) throws Exception {
		setTextField(map.getLocator("fieldCreationReasons"), createReasons);
	}

	public String getCreationReasons() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("fieldCreationReasons")).getAttribute("value").split("\n")[0];
	}

	public void setAndroidVersion(String androidVersion) throws Exception {
		if (androidVersion != null && !androidVersion.trim().isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "Android Version"));
			setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Tag Values", "Android Version"),
					androidVersion);
		}
	}

	public String getAndroidVersion() throws Exception {
		waitLoading();
		return getDriver()
				.findElement(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "Android Version"))
				.getText().split("\n")[0];
	}

	public void setCarrierAggregation(String carrier) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "Carrier Aggregation"));
		setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Tag Values", "Carrier Aggregation"),
				carrier);
	}

	public String getCarrierAggregation() throws Exception {
		waitLoading();
		return getDriver()
				.findElement(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "Carrier Aggregation"))
				.getText().split("\n")[0];
	}

	public void setGridRowAndColumn(String rowColumn) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "Grid Row x Column"));
		setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Tag Values", "Grid Row x Column"),
				rowColumn);
	}

	public String getGridRowAndColumn() throws Exception {
		waitLoading();
		return getDriver()
				.findElement(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "Grid Row x Column"))
				.getText().split("\n")[0];
	}

	public void setOSVersion(String osVersion) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "OS Version"));
		setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Tag Values", "OS Version"), osVersion);
	}

	public String getOSVersion() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "OS Version"))
				.getText().split("\n")[0];
	}

	public void setUIVersion(String uiVersion) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "UI Version"));
		setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Tag Values", "UI Version"), uiVersion);
	}

	public String getUIVersion() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "UI Version"))
				.getText().split("\n")[0];
	}

	public void setVoiceOver(String voiceOver) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "Voice over"));
		setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Tag Values", "Voice over"), voiceOver);
	}

	public String getVoiceOver() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocatorVar("optionGererciOnCreatePackage", "Tag Values", "Voice over"))
				.getText().split("\n")[0];
	}

	public void setCountryShortcut(String countryShortcut) throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("btnEditCountryShortcut"));
		setTextField(map.getLocator("fieldCountryShortcut"), countryShortcut);
	}

	public String getCountryShortcut() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("fieldCountryShortcut")).getAttribute("value").split("\n")[0];
	}

	public String getOperatorShortcut() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("fieldOperatorShorcut")).getAttribute("value").split("\n")[0];
	}

	public String getModelOnPackageEditPage() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocatorVar("optionGererciOnCreatePackage", "Model and Type", "Model"))
				.getAttribute("title");
	}

	public String getBuyerOnPackageEditPage() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocatorVar("optionGererciOnCreatePackage", "Buyer", "Buyer"))
				.getAttribute("title");
	}

	public boolean verifyErrorOnOsVersionField() throws Exception {
		waitLoading();
		moveToPageHeader();
		By by = map.getLocatorVar("optionGererciFieldName", "Tag Values", "OS Version");
		return ConstantsPackages.CONTROL_LABEL_ERROR.equals(getDriver().findElement(by).getAttribute("class"));
	}

	public void clickOnSaveCreatedButton() throws Exception {
		savePackage(map.getLocator(ConstantsPackages.BUTTON_CREATE_PACKAGE));
	}

	public void clickOnSaveEditedButton() throws Exception {
		savePackage(map.getLocator(ConstantsPackages.BUTTON_EDIT_PACKAGE));
	}

	public void clickOnKeepConfirmationButton() throws Exception {
		clickOnButton(map.getLocator("btnKeepConfirmation"));
	}

	public void clickOnConfirmationSucessOkButton() throws Exception {
		clickOnButton(map.getLocator("btnConfirmationSucessOk"));
	}

	public void clickOnClosePopup() throws Exception {
		savePackage(map.getLocator(ConstantsPackages.BUTTON_CLOSE_POPUP));
	}

	public boolean clickOnOkPackageSuccessCreated() throws Exception {
		boolean result;
		waitLoading();
		elementFocus();
		result = clickOnButton(map.getLocatorVar("btnYesPopup", "Success"));
		return result;
	}

	public void clickOnCancelPackage() throws Exception {
		clickOnButton(map.getLocator("btnCancelPackage"));
	}

	public void clickOnConfirmCancelPackage() throws Exception {
		clickOnButton(map.getLocator("YesCancelPackage"));
	}

	public void selectAllAutoProfileCodes() throws Exception {
		clickOnButton(map.getLocator("allAutoProfileCodes"));
	}

	public void setDinamicTagValues() throws Exception {
		List<WebElement> findElements = getDriver().findElements(map.getLocator("quantityFieldsTagValues"));
		for (int i = 0; i < findElements.size(); i++) {
			By locatorVar = map.getLocatorVar("fillDinamicTagValues", String.valueOf(i+1));
			getDriver().findElement(locatorVar).click();
		}
	}
}
