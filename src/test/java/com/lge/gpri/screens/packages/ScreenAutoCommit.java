package com.lge.gpri.screens.packages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
* Perform package history page class.
* 
* <p>Description: Methods to test in the package history.
*  
* @author Gabriel Costa
*/

public class ScreenAutoCommit extends AbstractScreen{
	
	public ScreenAutoCommit(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_PACKAGE);
	}

	public String getCommitUrlFromRestricitons(List<Object> list) throws Exception {
		String[] keys = new String[list.size()];
		String[] values = new String[list.size()];
		for(int i=0;i<list.size();i++){
			keys[i]=((String)list.get(i)).split(":")[0];
			values[i]=((String)list.get(i)).split(":")[1];
		}
		String pathBase = getPathFromBy(map.getLocator("autoCommitBase"));
		String pathBack = getPathFromBy(map.getLocator("autoCommitBack"));
		String pathCurrentTable = getPathFromBy(map.getLocator("autoCommitTable"));
		String pathCommitUrl = getPathFromBy(map.getLocator("autoCommitCommitUrl"));
		String pathToCommitUrl = pathBase;
		for(int i=0;i<list.size();i++){
			String pathAutoRestriciton = getPathFromBy(map.getLocatorVar("autoCommitRestriction",keys[i],values[i]));
			pathToCommitUrl+=pathAutoRestriciton+pathBack;
		}
		pathToCommitUrl+=pathCurrentTable+pathCommitUrl;
		String commitUrl = getDriver().findElement(By.xpath(pathToCommitUrl)).getAttribute("value");
		return commitUrl;
	}
	
	public void setCommitUrlFromRestricitons(String input, List<Object> list) throws Exception {
		String[] keys = new String[list.size()];
		String[] values = new String[list.size()];
		for(int i=0;i<list.size();i++){
			keys[i]=((String)list.get(i)).split(":")[0];
			values[i]=((String)list.get(i)).split(":")[1];
		}
		String pathBase = getPathFromBy(map.getLocator("autoCommitBase"));
		String pathBack = getPathFromBy(map.getLocator("autoCommitBack"));
		String pathCurrentTable = getPathFromBy(map.getLocator("autoCommitTable"));
		String pathCommitUrl = getPathFromBy(map.getLocator("autoCommitCommitUrl"));
		String pathToCommitUrl = pathBase;
		for(int i=0;i<list.size();i++){
			String pathAutoRestriciton = getPathFromBy(map.getLocatorVar("autoCommitRestriction",keys[i],values[i]));
			pathToCommitUrl+=pathAutoRestriciton+pathBack;
		}
		pathToCommitUrl+=pathCurrentTable+pathCommitUrl;
		setTextField(By.xpath(pathToCommitUrl), input);
	}

	public void saveChanges() throws Exception {
		elementFocus();
		clickOnButton(map.getLocator("btnSaveAutoCommit"));
	}

	public void confirmSaveChanges() throws Exception {
		clickOnButton(globalMap.getLocatorVar("popupButton","Save Changes","Yes"));
	}
}
