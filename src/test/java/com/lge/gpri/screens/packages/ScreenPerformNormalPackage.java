package com.lge.gpri.screens.packages;

import org.openqa.selenium.WebDriver;

import com.lge.gpri.helpers.constants.ConstantsPackages;


/**
* Perform create and edit normal packages page class.
* 
* <p>Description: Methods to create and edit a package.
*  
* @author Cayk Lima Barreto
*/

public class ScreenPerformNormalPackage extends ScreenPerformAbstractPackage{
	
	public ScreenPerformNormalPackage(WebDriver driver) {
		super(driver);
	}
	
	public void fillFieldsPackage(String model, String option, String buyer, String packageName, String targetDate, String uiSimulator, String cupssGroup, String creationReasons, String
			androidVersion, String gridRowColumn, String osVersion, String uiVersion, String voiceOver) throws Exception{
		
		setModel(model);
		setOption(option);
		setBuyerValues(buyer);
		setPackageName(packageName);
		setTargetDate(targetDate);
		setUISimulator(uiSimulator);
		setCreationReasons(creationReasons);
		setCupssGroup(cupssGroup);
		setAndroidVersion(androidVersion);
		setGridRowAndColumn(gridRowColumn);
		setOSVersion(osVersion);
		setUIVersion(uiVersion);
		setVoiceOver(voiceOver);
	}
	
	public void fillFieldsFromAnotherPackage(String model, String modelAnother, String buyerAnother, String packageNameAnother, String packageName, String targetDate, String uiSimulator, String cupssGroup, String creationReasons, String
			androidVersion, String carrierAggregation, String gridRowColumn, String osVersion, String uiVersion, String voiceOver) throws Exception{
		setModel(model);
		setValueFromAnotherPackage();
		setModelValuesFromAnotherPackage(modelAnother);
		setBuyerValuesFromAnotherPackage(buyerAnother);
		setPackageValuesFromAnotherPackage(packageNameAnother);
		setPackageName(packageName);
		setTargetDate(targetDate);
		setUISimulator(uiSimulator);
		setCupssGroup(cupssGroup);
		setCreationReasons(creationReasons);
		setAndroidVersion(androidVersion);
		setCarrierAggregation(carrierAggregation);
		setGridRowAndColumn(gridRowColumn);
		setOSVersion(osVersion);
		setUIVersion(uiVersion);
		setVoiceOver(voiceOver);
		
	}

	public void clickOnSaveCreatedButton() throws Exception {
		savePackage(map.getLocator(ConstantsPackages.BUTTON_CREATE_PACKAGE));
	}
	
	public void clickOnSaveEditedButton() throws Exception {
		savePackage(map.getLocator(ConstantsPackages.BUTTON_EDIT_PACKAGE));
	}

	public void clickOnKeepConfirmationButton() throws Exception {
		clickOnButton(globalMap.getLocatorVar("popupButton","Create Packages","Keep Confirmations"));
	}
	
	public void clickOnOpenAllBuyers() throws Exception {
		clickOnButton(globalMap.getLocatorVar("popupButton","Create Packages","Open all Buyers"));
	}
	
	public void clickOnOpenOnlyBuyersThatChanged() throws Exception {
		clickOnButton(globalMap.getLocatorVar("popupButton","Create Packages","Open only Buyers that changed"));
	}

	public void clickOnConfirmationSucessOkButton() throws Exception {
		clickOnButton(map.getLocator("btnConfirmationSucessOk"));
	}
	
	public void clickOnClosePopup() throws Exception {
		savePackage(map.getLocator(ConstantsPackages.BUTTON_CLOSE_POPUP));
	}
	
	public boolean clickOnOkPackageSuccessCreated() throws Exception {
		boolean result;
		waitLoading();
		elementFocus();
		result = clickOnButton(map.getLocatorVar("btnYesPopup","Success"));
		return result;
	}

	public void clickOnCancelPackage() throws Exception {
		clickOnButton(map.getLocator("btnCancelPackage"));
	}

	public void clickOnConfirmCancelPackage() throws Exception {
		clickOnButton(map.getLocator("YesCancelPackage"));
	}
}
