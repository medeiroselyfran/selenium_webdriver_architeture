package com.lge.gpri.screens.packages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsCompare;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.AbstractScreen;
import com.lge.gpri.screens.buyers.ScreenPerformBuyerApproval;
import com.lge.gpri.screens.buyers.ScreenPerformEditBuyerProfile;
import com.lge.gpri.screens.buyers.ScreenPerformListBuyers;

/**
 * Perform list of packages page class.
 * 
 * <p>
 * Description: Methods to realize the tests.
 * 
 * @author Cayk Lima Barreto Refactor methods and create new metods - Fernando
 *         Pinheiro - 03/11/17 Refactor methods and create new metods - Fernando
 *         Pinheiro - 07/11/17
 */

public class ScreenPerformListPackages extends AbstractScreen {

	String status = "";
	String[] nameStatus;

	public ScreenPerformListPackages(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_PACKAGE);
	}

	public void openPage() {
		getDriver().get("http://136.166.96.136:8204/Views/Packages/List.aspx#/");
		getDriver().manage().window().maximize();
	}

	public void setModel(String model) throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionModel"));
		setTextFieldDropdown(map.getLocator("fieldModel"), model);
	}

	public String getModelName() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModel")).getAttribute("title");
	}

	public boolean setBuyer(String buyer) throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionBuyer"));
		boolean setTextFieldDropdown = setTextFieldDropdown(map.getLocator("fieldBuyer"), buyer);
		waitLoading();
		return setTextFieldDropdown;
	}

	public String getBuyerName() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionBuyer")).getText();
	}

	public void selectFirstBuyer(String buyer) throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionBuyer"));
		if (buyer.equals("FIRST")) {
			clickOnButton(map.getLocator("fieldFirstBuyerActiveOnPackageListScreen"));
		} else {
			setTextFieldDropdown(map.getLocator("fieldBuyer"), buyer);
		}
	}

	public void searchPackage(String packageName) throws Exception {
		setTextField(map.getLocator("fieldSearchPackages"), packageName);
		Thread.sleep(1000);
	}

	public void cleanFieldSearchPackage() throws Exception {
		By by = map.getLocator("fieldSearchPackages");
		while (!verifiExistingPackageResearched()) {
			getDriver().findElement(by).sendKeys(Keys.BACK_SPACE);
		}
		Thread.sleep(1000);
	}

	public boolean paginationNext() throws Exception {
		waitLoading();
		moveToPageBottom();
		return clickOnButton(map.getLocator("btnNextPackages"));
	}

	public boolean paginationPrevious() throws Exception {
		waitLoading();
		moveToPageBottom();
		return clickOnButton(map.getLocator("btnPreviousPackages"));
	}

	public boolean showTableElements(String numberOfElements) throws Exception {
		boolean result = false;
		waitLoading();
		if (!numberOfElements.isEmpty()) {
			WebElement optionNumberOfElementsShow = getDriver()
					.findElement(map.getLocator("optionNumberOfElementsShow"));
			optionNumberOfElementsShow.click();
			WebElement optionSelected = getDriver().findElement(map.getLocator(numberOfElements));
			optionSelected.click();
			result = true;
		}
		return result;
	}

	public boolean goToPage(String selectedPage) throws Exception {
		boolean result = false;
		waitLoading();
		if (!selectedPage.isEmpty()) {
			WebElement page = getDriver().findElement(map.getLocator(selectedPage));
			page.click();
			result = true;
		}
		return result;
	}

	public boolean checkStatus() throws Exception {
		waitLoading();
		boolean result = false;
		String status = "";
		String statusTxt = "";
		String[] split;
		do {
			status = getDriver().findElement(map.getLocator("pacakgeStatus")).getText();
			split = status.split("\n");
			statusTxt = split[0];
			Thread.sleep(50);
		} while (statusTxt.equals(ConstantsPackages.COMMITING));
		if (statusTxt.equals(ConstantsPackages.CONFIRMED)) {
			result = true;
		}
		return result;
	}

	/**
	 * Its returns all status of buyers in the page list packages, most used to get
	 * mother and child buyers from a package
	 */
	public ArrayList<String> getAllStatus() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		ArrayList<String> statusBuyers = new ArrayList<>();
		List<WebElement> status = getDriver().findElements(map.getLocator("pacakgesStatus"));
		for (WebElement s : status) {
			statusBuyers.add(s.getText());
		}
		return statusBuyers;
	}

	/**
	 * Its returns all revision status of buyers in the page list packages, most
	 * used to get mother and child buyers from a package
	 */
	public ArrayList<String[]> getAllRevison() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		ArrayList<String[]> revisionsBuyers = new ArrayList<>();
		List<WebElement> revisions = getDriver().findElements(map.getLocator("pacakgesRevisions"));
		for (WebElement s : revisions) {
			revisionsBuyers.add(s.getText().split(" / "));
		}
		return revisionsBuyers;
	}

	/**
	 * Its returns all names of buyers in the page list packages, most used to get
	 * mother and child buyers from a package
	 */
	public ArrayList<String> getAllBuyersNames() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		ArrayList<String> namesBuyers = new ArrayList<>();
		List<WebElement> names = getDriver().findElements(map.getLocator("pacakgeBuyersNames"));
		for (WebElement name : names) {
			namesBuyers.add(name.getText());
		}
		return namesBuyers;
	}

	public boolean checkBuilding() throws Exception {
		waitLoading();
		boolean result = false;
		String statusBuilding;
		do {
			waitLoading();
			statusBuilding = getDriver().findElement(map.getLocator("buildingClass")).getAttribute("class");
			Thread.sleep(50);
		} while (!statusBuilding.equals(ConstantsPackages.CLASS_CHECK));

		if (statusBuilding.equals(ConstantsPackages.CLASS_CHECK)) {
			result = true;
		}
		return result;
	}

	public void clickOnLoadOnlyTheLatestPackageButton() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("onlyLatestPackage"));
	}

	public void clickOnGpriMButton() throws Exception {
		Thread.sleep(10000);
		WebElement btnGpriM = getDriver().findElement(map.getLocator("btnGpriM"));
		btnGpriM.click();
	}

	public void acessPackageHistoryForRelatedPackage(String packageName) throws Exception {
		WebElement packageList = getDriver().findElement(map.getLocator("tbodyPackages"));
		List<WebElement> allRow = packageList.findElements(By.tagName("tr"));
		for (int i = 0; i <= allRow.size(); i++) {
			if (ConstantsPackages.PACKAGE_NORMAL_TEST_STATUSF1.equals(allRow.get(i).getText())) {
				allRow.get(i + 1).findElement(map.getLocator("btnPackageHistory")).click();
				break;
			}
		}
	}

	public void rejectChildBuyers() throws Exception {
		openPage();
		setModel(ConstantsPackages.LGH815);
		setBuyer(ConstantsPackages.TLF_GROUP_EUROPE_6TL);
		waitLoadingOverlay();
		do {
			if (existsElement(By.xpath("//*[@id='tbodyPackages']/tr/td[8]/button[@uititle='Confirm']"))) {
				getDriver().findElement(By.xpath("//*[@id='tbodyPackages']/tr/td[8]/button[@uititle='Confirm']"))
						.click();
				waitLoadingOverlay();
				clickOnOKButtonAfterConfirmPackage();
				clickOnOkConfirmSmartCA();
				waitLoadingOverlay();
			} else if (existsElement(By.xpath("//*[@id='tbodyPackages']/tr/td[8]/button[@uititle='Reject']"))) {
				getDriver().findElement(By.xpath("//*[@id='tbodyPackages']/tr/td[8]/button[@uititle='Reject']"))
						.click();
				setTextField(map.getLocator("rejectMessage"), ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
				clickOnButton(map.getLocatorVar("btnYesPopup", "Reject Package"));
				waitLoadingOverlay();
			}
			Thread.sleep(1000);
		} while (getDriver().findElements(By.xpath("//*[@id='tbodyPackages']/tr/td[8]/button[@uititle='Reject']"))
				.size() != 0
				|| getDriver().findElements(By.xpath("//*[@id='tbodyPackages']/tr/td[8]/button[@uititle='Confirm']"))
						.size() != 0
				|| existsElement(By.xpath("//*[@id='tbodyPackages']/tr/td[7]/div[@class='progress']")));
	}

	public void clickOnCreatePackageButton() throws Exception {
		moveToPageHeader();
		clickOnButton(map.getLocator("btnNewPackage"));
	}

	public void clickOnCreateSmartCAButton() throws Exception {
		clickOnButton(map.getLocator("btnNewSmartCA"));
	}

	public String getNamePackageConfirmation() throws Exception {
		waitElementToBeClickable(map.getLocator("tablePackages"));
		String txt = getDriver().findElement(map.getLocator("packageNameFromTable")).getText();
		String[] split = txt.split("] ");
		if (split.length > 1) {
			return split[1];
		} else {
			return "";
		}
	}
	
	public String getPackageType() throws Exception {
		waitElementToBeClickable(map.getLocator("tablePackages"));
		String txt = getDriver().findElement(map.getLocator("packageNameFromTable")).getText();
		String[] split = txt.split("]");
		if (split.length > 1) {
			return split[0].substring(1);
		} else {
			return "";
		}
	}

	public String getStatusPackageConfirmation() throws Exception {
		waitElementToBeClickable(map.getLocator("tablePackages"));
		String txt = getDriver().findElement(map.getLocator("packageStatusFromTable")).getText();
		return txt;
	}

	/**
	 * @throws Exception
	 * 
	 */
	public String getMessageForNoPackagesAvailable() throws Exception {
		return getDriver().findElement(map.getLocator("txtNoPackageAvailable")).getText();
	}
	
	public String getMessageForNoPackagesAvailableInCombo() throws Exception {
		return getDriver().findElement(map.getLocator("txtNoresultsPackage")).getText();
	}

	public boolean clickOnEditPackageButton() throws Exception {
		return clickOnButton(map.getLocator("editButton"));
	}

	public void selectReopenPackage(String date) throws Exception {
		clickOnReopenPackageButton();
		clickOnExtendPackageDate();
		selectNewExtendTargetDate(date);
		clickOnReopenAfterTargetDate();
	}

	public void selectNewExtendTargetDate(String date) throws Exception {
		waitLoading();
		if (getDriver().findElement(map.getLocator("newTargetDate")).isDisplayed()) {
			clickOnButton(map.getLocator("newTargetDate"));
			if (!date.equals(ConstantsPackages.TODAY)) {
				String day = date.split("/")[0];
				int month = Integer.parseInt(date.split("/")[1]);
				int year = Integer.parseInt(date.split("/")[2]);
				String actualMonthString = getDriver().findElement(map.getLocator("monthOfExtendTargetDate")).getText();
				int actualYear = Integer
						.parseInt(getDriver().findElement(map.getLocator("yearExtendTargetDate")).getText());
				String[] months = { "January", "February", "March", "April", "May", "June", "July", "August",
						"September", "October", "November", "December" };
				int actualMonth = 0;
				for (int i = 0; i < 12; i++) {
					if (actualMonthString.equals(months[i])) {
						actualMonth = i + 1;
					}
				}
				int jumps = (2000 + year - actualYear) * 12 + month - actualMonth;
				By jumpMonthButton = null;
				if (jumps > 0) {
					jumpMonthButton = map.getLocator("nextMonth");
				} else {
					jumpMonthButton = map.getLocator("previousMonth");
				}
				for (int i = 0; i < Math.abs(jumps); i++) {
					getDriver().findElement(jumpMonthButton).click();
				}
				clickOnButton(map.getLocatorVar("dayExtendTargetDate", day));
			} else {
				waitLoading();
				getDriver().findElement(map.getLocator("newTargetDate")).sendKeys(Keys.ENTER);
			}
		}
	}

	public boolean clickOnEditProfilePackageButton() throws Exception {
		boolean resultOk;
		waitLoading();
		waitLoadingOverlay();
		resultOk = clickOnButton(map.getLocator("btnEditProfile"));
		if (resultOk) {
			waitLoadingOverlay();
		}
		return resultOk;
	}

	public boolean clickOnEditProfilePackageButtonFrom(String suffix) throws Exception {
		boolean resultOk;
		waitLoadingOverlay();
		resultOk = clickOnButton(map.getLocatorVar("btnEditProfileGeneric", suffix));
		if (resultOk) {
			waitLoadingOverlay();
		}
		return resultOk;
	}

	public boolean clickOnConfirmPackageButton() throws Exception {
		return clickOnButton(map.getLocator("confirm"));
	}

	public boolean clickOnRemovePackageButton() throws Exception {
		waitLoadingOverlay();
		return clickOnButton(map.getLocator("remove"));
	}

	public boolean confirmRemovePackage() throws Exception {
		boolean result;
		waitLoading();
		elementFocus();
		result = clickOnButton(map.getLocatorVar("btnYesPopup", "Delete Package"));
		return result;
	}

	public boolean cancelRemovePackage() throws Exception {
		boolean result;
		waitLoading();
		elementFocus();
		result = clickOnButton(globalMap.getLocatorVar("popupButton", "Delete Packages", "No"));
		return result;
	}

	public void clickOnOKButtonAfterConfirmPackage() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		elementFocus();
		clickOnButton(map.getLocatorVar("btnYesPopup", "Confirm Package"));
	}

	public void clickOnReopenPackageButton() throws Exception {
		clickOnButton(map.getLocator("btnReopenPackage"));
	}

	public void clickOnOKButtonAfterReopenPackage() throws Exception {
		elementFocus();
		clickOnButton(map.getLocator("btnYesReopenPackage"));
	}

	public void clickOnCommitingButton() throws Exception {
		clickOnButton(map.getLocator("btnCommiting"));
	}

	public void clickOnPackageHistoryButton() throws Exception {
		clickOnButton(map.getLocator("btnPackageHistory"));
	}

	public void clickOnPermalinksPackage() throws Exception {
		clickOnButton(map.getLocator("packagePermalinks"));
	}

	public void clickOnPermalinkPopUp() throws Exception {
		clickOnButton(map.getLocator("permaLink"));
	}

	public String getPackageStatus() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("status")).getText();
	}

	public void clickOnMoreDetails() throws Exception {
		clickOnButton(map.getLocator("btnMoreDetails"));
	}

	public boolean clickOnDownloadLog() throws Exception {
		return clickOnButton(map.getLocator("btnDownloadLog"));
	}

	public void clickOnConfirmBuyerGiven(String buyer) throws Exception {
		boolean exists = existsElement(map.getLocatorVar("confirmPackageGiven", buyer));
		if (exists) {
			clickOnButton(map.getLocatorVar("confirmPackageGiven", buyer));
		}
	}

	public void clickOnOkConfirmSmartCA() throws Exception {
		elementFocus();
		clickOnButton(map.getLocatorVar("btnYesPopup", "Confirm Smart CA"));
	}

	public boolean isDisplayedPopUpConfirmSmartCA() throws Exception {
		boolean isDisplayed = false;
		if (existsElement(map.getLocatorVar("btnYesPopup", "Confirm Smart CA"))) {
			isDisplayed = getDriver().findElement(map.getLocatorVar("btnYesPopup", "Confirm Smart CA")).isDisplayed();
		}
		return isDisplayed;
	}

	public void setBuyerConfirmed(String buyer) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		if (existsElement(map.getLocatorVar("confirmPackageGiven", buyer))) {
			clickOnButton(map.getLocatorVar("confirmPackageGiven", buyer));
			clickOnOKButtonAfterConfirmPackage();
			clickOnOkConfirmSmartCA();
		}
	}

	public void setBuyerUnconfirmed(String buyer) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		// if(!existsElement(map.getLocatorVar("rejectPackageGiven", buyer))){
		// rejectGeneratingAPK(buyer);
		// confirmRejectPackage(buyer);
		// }
		while (!existsElement(map.getLocatorVar("reopenPackageGiven", buyer))
				&& !existsElement(map.getLocatorVar("confirmPackageGiven", buyer))) {
			Thread.sleep(1000);
		}
		if (!existsElement(map.getLocatorVar("confirmPackageGiven", buyer))) {
			clickOnButton(map.getLocatorVar("reopenPackageGiven", buyer));
			clickOnExtendPackageDate();
			selectNewExtendTargetDate(ConstantsPackages.TODAY);
			clickOnReopenAfterTargetDate();
			// clickOnButton(map.getLocatorVar("reopenPackageGiven", buyer));
			// elementFocus();
			// clickOnButton(map.getLocator("btnYesReopenPackage"));
		}
	}

	public void confirmRejectPackage(String buyer) throws Exception {
		setTextField(map.getLocator("rejectMessage"), ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		clickOnButton(map.getLocatorVar("btnYesPopup", "Reject Package"));
	}

	public void clickOnExtendPackageDate() throws Exception {
		clickOnButton(map.getLocatorVar("btnYesPopup", "Reopen Package"));
	}

	public void selectExtendedDateToday() throws Exception {
		try {
			clickOnButton(map.getLocator("fieldExtendedDate"));
			clickOnButton(map.getLocator("dateToday"));
			clickOnButton(map.getLocatorVar("btnYesPopup", "Extend target date and reopen"));
		} catch (Exception e) {
			return;
		}
	}

	public void rejectGeneratingAPK(String buyer) throws Exception {
		boolean resultOk = true;
		resultOk = clickOnButton(map.getLocatorVar("rejectPackageGiven", buyer));
		if (resultOk) {
			setTextField(map.getLocator("rejectMessage"), ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
			clickOnButton(map.getLocatorVar("btnYesPopup", "Reject Package"));
		}
	}
	
	public void rejectPackage() throws Exception {
		boolean resultOk = true;
		resultOk = clickOnButton(map.getLocator("btnRejectPackage"));
		if (resultOk) {
			setTextField(map.getLocator("rejectMessage"), ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
			clickOnButton(map.getLocatorVar("btnYesPopup", "Reject Package"));
		}
	}

	public boolean existsElement(String element) throws Exception {
		boolean result = existsElement(map.getLocator(element));
		return result;
	}

	public String getExistingPackage() throws Exception {
		openPage();
		String namePackage = "";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		namePackage = screenPerformListPackages.getTextFrom("namePackage").replaceFirst("] ", "---").split("---")[1];
		return namePackage;
	}

	public String verifyExistingPackageToGetPackageName(String model, String buyer, String packageName) throws Exception {
		openPage();
		String namePackage = "";
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		screenPerformListPackages.setModel(model);
		screenPerformListPackages.setBuyer(buyer);
		screenPerformListPackages.clickOnComboPackage();
		screenPerformListPackages.selectShowAllPackages();
		screenPerformListPackages.searchPackage(packageName);
		if (!ConstantsPackages.NO_PACKAGE_AVAILABLE.contains(getMessageForNoPackagesAvailable())) {
			String partname = screenPerformListPackages.getTextFrom("namePackage").split("]")[1];
			namePackage = partname.replaceFirst(" ", "---").split("---")[1];
		}
		return namePackage;
	}

	public boolean verifiExistingPackageResearched() throws Exception {
		boolean isOk = false;
		if (!ConstantsPackages.NO_PACKAGE_AVAILABLE.equals(getMessageForNoPackagesAvailable())) {
			isOk = true;
		}
		return isOk;
	}

	public void ToogleAll() throws Exception {
		waitLoadingOverlay();
		clickOnButton(map.getLocator("confirmPackages"));
		clickOnButton(map.getLocator("checkToogleAll"));
		clickOnButton(map.getLocator("confirmOnConfirmPackages"));
	}

	public String getPackageStatusMother() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("statusMother")).getText();
	}

	public String getPackageStatusChildOne() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("statusChildOne")).getText();
	}

	public String getPackageStatusChildTwo() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("statusChildTwo")).getText();
	}

	public int getNumberOfItemsListPackage() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		return getDriver().findElements(map.getLocator("tableLinesListPackage")).size();
	}

	/**
	 * This methods return respectively the number in the show info in result table
	 * "Showing N to N of N entries"
	 */
	public int getStartEntrieShowingInfo() throws NumberFormatException, Exception {
		return Integer.parseInt(getDriver().findElement(map.getLocator("txtShowingEntriesInfo")).getText().split(" ")[1]
				.replace(",", ""));
	}

	public int getEndEntrieShowingInfo() throws NumberFormatException, Exception {
		return Integer.parseInt(getDriver().findElement(map.getLocator("txtShowingEntriesInfo")).getText().split(" ")[3]
				.replace(",", ""));
	}

	public int getTotalEntriesShowingInfo() throws NumberFormatException, Exception {
		return Integer.parseInt(getDriver().findElement(map.getLocator("txtShowingEntriesInfo")).getText().split(" ")[5]
				.replace(",", ""));
	}

	public int getActivePage() throws Exception {
		return Integer.parseInt(getDriver().findElement(map.getLocator("activePageTable")).getText().replace(",", ""));
	}

	public int getNumberOfPages() throws NumberFormatException, Exception {
		return Integer.parseInt(getDriver().findElement(map.getLocator("numberOfPages")).getText());
	}

	public void clickOnViewPackage(String buyerName) throws Exception {
		moveToPageHeader();
		clickOnButton(map.getLocatorVar("viewPackage", buyerName));
	}

	public void clickOnMoreInfo(String packageName) throws Exception {
		clickOnButton(map.getLocatorVar("moreInfoPackage", packageName));
	}

	public boolean clickOnMarkAsFinal(String buyerName) throws Exception {
		clickOnButton(map.getLocatorVar("markAsFinalPackage", buyerName));
		waitLoadingOverlay();
		String classContent = getDriver().findElement(map.getLocatorVar("markAsFinalPackage", buyerName))
				.getAttribute("class");
		return classContent.contains("final");
	}

	public String getStringMoreInfo() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		String infos = "";
		List<WebElement> items = getDriver().findElements(map.getLocator("stringMoreInfo"));
		for (WebElement info : items) {
			infos += info.getText();
		}
		return infos;
	}

	public boolean verifyIsStatusUnderEdition() throws Exception {
		try {
			getDriver().findElement(map.getLocator("statusUnderEdition"));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public void setNewTargetDate(String date) throws Exception {
		clickOnButton(map.getLocator("newTargetDate"));
		if (date.equals(ConstantsPackages.TODAY)) {
			Actions action = new Actions(getDriver());
			action.sendKeys(Keys.ENTER).build().perform();
		}
	}

	public boolean clickOnReopenAfterTargetDate() throws Exception {
		Thread.sleep(500);
		return clickOnButton(map.getLocatorVar("btnYesPopup", "Extend target date and reopen"));
	}

	/**
	 * May be called any time
	 */
	public void downgradeRevisionOfBuyerHomeNetwork(String suffix) throws Exception {
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformBuyerApproval screenPerformBuyerApproval = new ScreenPerformBuyerApproval(getDriver());

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffix);
		screenPerformListBuyers.clickOnEditProfile(suffix);
		String item = screenPerformEditBuyerProfile.getValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1);
		if (item.equals(ConstantsBuyers.HOME_NETWORK_724_99)) {
			item = ConstantsBuyers.HOME_NETWORK_724_32;
		} else {
			item = ConstantsBuyers.HOME_NETWORK_724_99;
		}
		screenPerformEditBuyerProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1, item);
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING,
				ConstantsBuyers.NORMAL);
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();

		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(suffix);
		screenPerformListBuyers.clickOnStatusChangeItem(suffix);
		screenPerformListBuyers.clickOnStatusPendingApproval(suffix);
		screenPerformBuyerApproval.clickOnCloseRevisionCommentsButton();
		screenPerformBuyerApproval.clickOnApproveAll();
		screenPerformBuyerApproval.clickOnSaveButton();
		screenPerformBuyerApproval.clickOnConfirmSave();
	}

	/**
	 * After make the search
	 */
	public void upgradeRevisonOfBuyer(String suffix) throws Exception {
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());

		clickOnEditProfilePackageButtonFrom(suffix);
		screenPerformEditBuyerProfile.updateRevision();
		screenPerformEditBuyerProfile.selectAllUpdatePackage();
		screenPerformEditBuyerProfile.applyUpdatePackage();
		screenPerformEditBuyerProfile.clickOnSaveProfile();
		screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, "");
		screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
	}

	public boolean selectTabCustomizationOrParameters(String tabName) throws Exception {
		By opt = map.getLocatorVar("selectTab", tabName);
		return clickOnButton(opt);
	}

	public boolean verifyIfDisplayVerticalPropagationPopUp() throws Exception {
		boolean isDisplayed = false;
		if (existsElement(globalMap.getLocatorVar("errorMsg", "Pending Vertical Propagation"))) {
			isDisplayed = true;
		}
		return isDisplayed;
	}

	public void clickOnApplyVerticalPropagationInPopUp() throws Exception {
		By btn = map.getLocatorVar("optionPopUpPendingVerticalPropagation", "Apply Propagation");
		clickOnButton(btn);
	}

	public void clickOnNotApplyVerticalPropagationInPopUp() throws Exception {
		By btn = map.getLocatorVar("optionPopUpPendingVerticalPropagation", "Do Not Apply Propagation");
		clickOnButton(btn);
	}

	public void clickOnCancelReopenInPopUp() throws Exception {
		By btn = map.getLocatorVar("optionPopUpPendingVerticalPropagation", "Cancel reopen");
		clickOnButton(btn);
	}

	public boolean verifyPackageStatus(String buyer, String status) throws Exception {
		boolean isOk = false;
		setBuyer(buyer);
		if (status.equals(getTextFrom("status").split(" ")[0])) {
			clickOnConfirmPackageButton();
			clickOnOKButtonAfterConfirmPackage();
			if (isDisplayedPopUpConfirmSmartCA()) {
				clickOnOkConfirmSmartCA();
			}
			isOk = true;
		}
		return isOk;
	}
	
	public void waitProgressBarToBeVisible() throws Exception {
		WebDriverWait wait = new WebDriverWait(getDriver(), ConstantsGeneric.TIME_LONG);
		By by = map.getLocator("progressGeneratingApk");
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}
	
	public void existElementsProgressBar() throws Exception {
		waitProgressBarToBeVisible();
		while(existsElement(map.getLocator("progressGeneratingApk"))) {
			Thread.sleep(1000);
		}
	}
	
	public String constructorNamePackage() throws Exception {
		int numberPackage = 0;
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		String partialName = screenPerformListPackages.getNamePackageConfirmation().split(" ")[0];
		String namePackageConfirmation = screenPerformListPackages.getNamePackageConfirmation();
		String[] splitName = namePackageConfirmation.split(" ");
		if (splitName.length == 1) {
			numberPackage = 0;
		} else {
			numberPackage = Integer.parseInt(splitName[1]);
		}
		return partialName + " " + (numberPackage + 1);
	}
	
	public void clickOnComboPackage() throws Exception {
		By cmbPackage = map.getLocator("packageCombo");
		getDriver().findElement(cmbPackage).click();
	}
	
	public void performSearchInPackageCombo(String packageName) throws Exception {
		setTextField(map.getLocator("fieldToInputTextForSearch"), packageName);
	}
	
	public void selectShowAllPackages() throws Exception {
		getDriver().findElement(map.getLocator("showAllPackagesOption")).click();
		waitLoading();
	}
	
	public String getNameSecondPackage() throws Exception {
		String text = getDriver().findElement(map.getLocator("secondPackageOfListPackage")).getText();
		return text.replaceFirst("] ", "---").split("---")[1];
	}

}
