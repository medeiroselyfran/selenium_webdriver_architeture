package com.lge.gpri.screens.packages;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsCompare;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.AbstractScreen;
import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;

/**
 * Perform edit package profile page class.
 * 
 * <p>
 * Description: Methods to test the package profile.
 * 
 * @author Cayk Lima Barreto
 */

public class ScreenPerformEditPackageProfile extends AbstractScreen {

	public ScreenPerformEditPackageProfile(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_PACKAGE);
	}

	public boolean addItem(String item) throws Exception {
		waitLoadingOverlay();
		boolean result = false;
		if (!boxIsChecked(item)) {
			result = clickOnButton(map.getLocator(item));
		} else {
			result = true;
		}

		return result;
	}

	public boolean verifyAllItemsUnchecked() throws Exception {
		boolean isOk = false;
		boolean allItemsUnchecked = ("jstree-anchor"
				.equals(getDriver().findElement(map.getLocator("folderItemLibrary")).getAttribute("class"))
				|| "jstree-anchor jstree-hovered"
						.equals(getDriver().findElement(map.getLocator("folderItemLibrary")).getAttribute("class")))
				&& "jstree-icon jstree-checkbox"
						.equals(getDriver().findElement(map.getLocator("itemLibraryBtn")).getAttribute("class"));
		boolean someItemsChecked = "jstree-anchor"
				.equals(getDriver().findElement(map.getLocator("folderItemLibrary")).getAttribute("class"))
				&& "jstree-icon jstree-checkbox jstree-undetermined"
						.equals(getDriver().findElement(map.getLocator("itemLibraryBtn")).getAttribute("class"));
		boolean allItemsChecked = "jstree-anchor jstree-checked"
				.equals(getDriver().findElement(map.getLocator("folderItemLibrary")).getAttribute("class"));

		if (allItemsUnchecked) {
			isOk = true;
		} else if (someItemsChecked || allItemsChecked) {
			isOk = false;
		}
		return isOk;
	}

	public boolean addItemByPath(String pathUnic) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for (int i = 0; i < path.length && !pathUnic.isEmpty(); i++) {
			pathToElement += getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative", path[i]));
		}
		boolean isChecked = getDriver().findElement(By.xpath(pathToElement)).getAttribute("aria-selected")
				.equals("true");
		pathToElement += getPathFromBy(map.getLocator("checkBoxItemLibraryRelative"));
		if (!isChecked) {
			return clickOnButton(By.xpath(pathToElement));
		}
		return true;
	}

	public boolean removeItemByPath(String pathUnic) throws Exception {
		waitLoading();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for (int i = 0; i < path.length && !pathUnic.isEmpty(); i++) {
			pathToElement += getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative", path[i]));
		}
		boolean isChecked = getDriver().findElement(By.xpath(pathToElement)).getAttribute("aria-selected")
				.equals("true");
		pathToElement += getPathFromBy(map.getLocator("checkBoxItemLibraryRelative"));
		if (isChecked) {
			return clickOnButton(By.xpath(pathToElement));
		}
		return true;
	}

	public boolean removeItem(String item) throws Exception {
		waitLoadingOverlay();
		boolean result = false;
		if (boxIsChecked(item)) {
			result = clickOnButton(map.getLocator(item));
		} else {
			result = true;
		}
		return result;
	}

	public void setValue(String fieldName, String value) throws Exception {
		if (!fieldName.isEmpty() && !value.isEmpty()) {
			setTextField(map.getLocator(fieldName), value);
		}
	}

	public void setValueGivenNameItem(String nameItem, String value) throws Exception {
		if (!nameItem.isEmpty() && !value.isEmpty()) {
			String name = nameItem.split("#")[0];
			if (nameItem.split("#").length > 1) {
				String number = nameItem.split("#")[1];
				clickOnButton(map.getLocatorVar("selectValueGivenItemAndNumber", name + "#", number));
				clickOnButton(map.getLocatorVar("optionValueGivenItemAndNumber", name + "#", number));
				setTextField(map.getLocatorVar("fieldGivenItemAndNumber", name + "#", number), value);
			} else {
				setTextField(map.getLocatorVar("fieldGivenItem", nameItem), value);
			}
		}
	}

	public String getValue(String fieldName) throws Exception {
		if (!fieldName.isEmpty()) {
			return getDriver().findElement(map.getLocator(fieldName)).getAttribute("value");
		}
		return null;
	}

	public String getValueGivenNameItem(String nameItem) throws Exception {
		if (!nameItem.isEmpty()) {
			String name = nameItem.split("#")[0];
			if (nameItem.split("#").length > 1) {
				String number = nameItem.split("#")[1];
				if (existsElement(map.getLocatorVar("fieldGivenItemAndNumber", name + "#", number))) {
					return getDriver().findElement(map.getLocatorVar("fieldGivenItemAndNumber", name + "#", number))
							.getAttribute("value");
				}
			} else {
				if (existsElement(map.getLocatorVar("fieldGivenItem", nameItem))) {
					return getDriver().findElement(map.getLocatorVar("fieldGivenItem", nameItem)).getAttribute("value");
				}
			}
		}
		return "";
	}

	public void setMessageRevisionComment(String message) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		String display = getDriver().findElement(map.getLocator("dlgRevisionComment")).getAttribute("style");
		String none = display.split(" ")[1];
		if (!none.equals("none;")) {
			setTextField(map.getLocator("txtRevisionComment"), message);
		}
	}

	public void searchItemSelectItems(String item) throws Exception {
		setTextField(map.getLocator("fieldSearch"), item);
	}

	public void searchItemLibrary(String item) throws Exception {
		waitLoading();
		if (!item.isEmpty()) {
			WebElement fieldElement = getDriver().findElement(map.getLocator("fieldItemLibrary"));
			fieldElement.clear();
			fieldElement.sendKeys(item);
			waitFieldToBeFilled(map.getLocator("fieldItemLibrary"), item);
		}
	}

	public void setItemLibrary(String item) throws Exception {
		setTextFieldDropdown(map.getLocator("fieldItemLibrary"), item);
	}

	public boolean clickOnSelectItemsButton() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		boolean resultOk = false;
		try {
			resultOk = clickOnButton(map.getLocator("btnSelectItems"));
		} catch (Exception e) {
		}
		if (!resultOk) {
			clickOnNoItemAddedSelectItems();
		}
		return resultOk;
	}

	public boolean clickOnNoItemAddedSelectItems() throws Exception {
		boolean resultOk = false;
		try {
			resultOk = clickOnButton(map.getLocator("noItemAddedSelectItem"));
		} catch (Exception e) {
		}
		return resultOk;
	}

	public boolean clickOnSaveSelectedItems() throws Exception {
		return clickOnButton(map.getLocator("btnSaveSelectedItems"));
	}

	public void clickOnSaveProfile() throws Exception {
		moveToPageBottom();
		clickOnButton(map.getLocator("btnSaveProfile"));
	}

	public void clickOnSaveEditedProfile() throws Exception {
		moveToPageBottom();
		if (existsElement("btnSaveEditedProfile")) {
			clickOnButton(map.getLocator("btnSaveEditedProfile"));
			waitLoadingOverlay();
		}
	}

	public void clickOnItem(String item) throws Exception {
		clickOnButton(map.getLocator(item));
	}

	public void clickOnMarkItemsAsDoNotApplyButton() throws Exception {
		clickOnButton(map.getLocator("btnMarkItemsAsDoNotApply"));
	}

	public void clickOnAndroid(String value) throws Exception {
		waitLoadingOverlay();
		// *[@id="values"]//tr/td/div[text()="LTE
		// Mode"]/parent::td/following-sibling::td//div[@class="modified"]/a
		By opt = map.getLocatorVar("selectAndroidApp", value);
		if (existsElement(opt)) {
			clickOnButton(opt);
		}
	}

	public String getAttributeClassOfElement(String element) throws Exception {
		return getDriver().findElement(map.getLocator(element)).getAttribute("class");
	}

	public void setItemValueChangeNote(String item, String message) throws Exception {
		setTextField(map.getLocator("fieldNote"), message);
	}

	public void clickOnSaveNote() throws Exception {
		clickOnButton(map.getLocator("btnSaveNote"));
	}

	public void setValueItemPerValue(String optionValue, String value) throws Exception {
		By locatorOption = map.getLocator(optionValue);
		String pathToOption = getPathFromBy(locatorOption);
		String pathToPerValue = getPathFromBy(map.getLocator("relativePerValueParameters"));
		String pathToInput = getPathFromBy(map.getLocator("relativeFieldValueParameters"));
		By locatorPerValue = By.xpath(pathToOption + pathToPerValue);
		By locatorInput = By.xpath(pathToOption + pathToInput);
		clickOnButton(locatorOption);
		clickOnButton(locatorPerValue);
		setTextField(locatorInput, value);
	}

	public String getValueFromFieldText(String field) throws Exception {
		return getDriver().findElement(map.getLocator(field)).getAttribute("value");
	}

	public String getText(String field) throws Exception {
		return getDriver().findElement(map.getLocator(field)).getText();
	}

	public void clickOnUiStartButton() throws Exception {
		if (existsElement("btnUiStart")) {
			WebElement element = getDriver().findElement(map.getLocator("btnUiStart"));
			clickOnButtonByJavaScript(element);
		}
	}

	public void clickOnLockUnlockButton() throws Exception {
		if (existsElement("uiSimulatorLockImage")) {
			WebElement element = getDriver().findElement(map.getLocator("btnLockUnlock"));
			clickOnButtonByJavaScript(element);
		}
	}

	public void clickOnEditorModeButton() throws Exception {
		String btnEditorClass = getDriver().findElement(map.getLocator("btnUiSimulatorEditor")).getAttribute("class");
		if (!btnEditorClass.equals("btn btn-icon active")) {
			WebElement element = getDriver().findElement(map.getLocator("btnUiSimulatorEditor"));
			clickOnButtonByJavaScript(element);
		}
	}

	public void clickAppPositionOneButton() throws Exception {
		JavascriptExecutor executor = (JavascriptExecutor) getDriver();
		executor.executeScript(
				"$('.uiSimulator_adder').eq(0).find('div:eq(1)').trigger({ type: 'mouseup', which: 1 })");
	}

	public void clickAppPositionTwoButton() throws Exception {
		JavascriptExecutor executor = (JavascriptExecutor) getDriver();
		executor.executeScript(
				"$('.uiSimulator_adder').eq(1).find('div:eq(1)').trigger({ type: 'mouseup', which: 1 })");
	}

	public void clickOnSelectApplicationLink() throws Exception {
		By linkToClick = map.getLocator("linkToAddAplication");
		if (existsElement(linkToClick)) {
			getDriver().findElement(linkToClick).click();
		}
	}

	public void searchApp(String app) throws Exception {
		WebElement frame = getDriver().findElement(map.getLocator("frameAppPool"));
		getDriver().switchTo().frame(frame);
		WebElement search = getDriver().findElement(map.getLocator("searchAppOnApplicationPool"));
		search.clear();
		search.sendKeys(app);
		getDriver().switchTo().defaultContent();
	}

	public void clickOnInstagramApp() throws Exception {
		WebElement frame = getDriver().findElement(map.getLocator("frameAppPool"));
		getDriver().switchTo().frame(frame);
		Thread.sleep(500);
		clickOnButton(map.getLocator("instagram"));
		getDriver().switchTo().defaultContent();
	}

	public void clickOnSaveAppsButton() throws Exception {
		clickOnButton(map.getLocator("btnSaveApps"));
	}

	public void clickOnCustomizationButton() throws Exception {
		clickOnButton(map.getLocator("btnCustomization"));
	}

	public boolean checkApp1GridValues() throws Exception {
		boolean result = false;
		String appColumn = getDriver().findElement(map.getLocator("gridColumnApp1")).getAttribute("value");
		String appRow = getDriver().findElement(map.getLocator("gridRowApp1")).getAttribute("value");

		if (appColumn.equals("1") && appRow.equals("1")) {
			result = true;
		}
		return result;
	}

	public boolean checkApp2GridValues() throws Exception {
		boolean result = false;
		waitLoading();
		String appColumn = getDriver().findElement(map.getLocator("gridColumnApp2")).getAttribute("value");
		String appRow = getDriver().findElement(map.getLocator("gridRowApp2")).getAttribute("value");

		if (appColumn.equals("1") && appRow.equals("3")) {
			result = true;
		}
		return result;
	}

	public void setAppGridRow(String app, String value) throws Exception {
		waitLoading();
		WebElement row = getDriver().findElement(map.getLocator(app));
		row.clear();
		row.sendKeys(value);
	}

	public void setAppGridColumn(String app, String value) throws Exception {
		waitLoading();
		WebElement column = getDriver().findElement(map.getLocator(app));
		column.clear();
		column.sendKeys(value);
	}

	public boolean checkCollisionBetweenApps(String app1, String app2) throws Exception {
		boolean result = false;
		waitLoading();
		String classApp1 = getDriver().findElement(map.getLocator(app1)).getAttribute("class");
		String[] classesApp1 = classApp1.split(" ");
		String classApp2 = getDriver().findElement(map.getLocator(app2)).getAttribute("class");
		String[] classesApp2 = classApp2.split(" ");

		if (classesApp1.length > 3 && classesApp1[3].equals("uiSimulator_homeScreenCollision") && classesApp2.length > 3
				&& classesApp2[3].equals("uiSimulator_homeScreenCollision")) {
			result = true;
		}

		return result;
	}

	public void clickOnSyncButton() throws Exception {
		clickOnButtonWithoutWaiting(map.getLocator("btnSync"));
	}

	public void clickOnUseBuyerValuesAllButton() throws Exception {
		clickOnButton(map.getLocator("btnUseBuyerValuesAll"));
	}

	public void clickOnUsePackageValuesAllButton() throws Exception {
		clickOnButton(map.getLocator("btnUsePackageValuesAll"));
	}

	public void clickOnApplyButton() throws Exception {
		clickOnButtonWithoutWaiting(map.getLocator("btnApply"));
	}

	public void clickOnUseBuyerValuesParametersButton() throws Exception {
		clickOnButton(map.getLocator("btnUseBuyerValuesParameters"));
	}

	public void clickOnUsePackageValuesCustomizationButton() throws Exception {
		clickOnButton(map.getLocator("btnUsePackageValuesCustomization"));
	}

	public void clickOnOkSyncButton() throws Exception {
		waitLoading();
		WebElement btn = getDriver().findElement(map.getLocator("btnOkSyncMessage"));
		btn.click();
	}

	public void clickOnUpdateToButton() throws Exception {
		clickOnButtonWithoutWaiting(map.getLocator("btnUpdatePackage"));
	}

	public void clickOnCopyPackageButton() throws Exception {
		clickOnButtonWithoutWaiting(map.getLocator("btnCopyPackage"));
	}

	public void clickOnCopyPackageOptionModelButton() throws Exception {
		waitLoading();
		WebElement btn = getDriver().findElement(map.getLocator("copyPackageOptionModel"));
		Thread.sleep(500);
		btn.click();
	}

	public void setCopyPackageModel(String model) throws Exception {
		setTextFieldDropdown(map.getLocator("copyPacakgeFieldModel"), model);
	}

	public void clickOnCopyPackageOptionPackageButton() throws Exception {
		waitLoading();
		WebElement btn = getDriver().findElement(map.getLocator("copyPackageOptionPackage"));
		Thread.sleep(500);
		btn.click();
	}

	public void setCopyPackageFieldPackage(String pckage) throws Exception {
		setTextFieldDropdown(map.getLocator("copyPackageFieldPackage"), pckage);
	}

	public void clickOnFirstElementOfListPackage() throws Exception {
		clickOnButton(map.getLocator("firtElementOfPackageList"));
	}

	public void clickOnLoadPackageButton() throws Exception {
		waitLoading();
		WebElement btn = getDriver().findElement(map.getLocator("btnLoadPackage"));
		Thread.sleep(500);
		btn.click();
	}

	public void openValidatorsPageOnAnotherTab() throws Exception {
		openPageOnAnotherTab("http://136.166.96.136:8204/Views/Admin/Validator");
	}

	public void clickOnItemLibraryButton() throws Exception {
		clickOnButton(map.getLocator("itemLibraryBtn"));
	}

	public void clickOnTimeFormatButton() throws Exception {
		Thread.sleep(1000);
		clickOnButton(map.getLocator("timeFormatBtn"));
	}

	public void clickOnTimeFormatValueOptions() throws Exception {
		clickOnButton(map.getLocator("timeFormatOptions"));
	}

	public void select24HourOption() throws Exception {
		clickOnButton(map.getLocator("option24hour"));
	}

	public void select12HoursOption() throws Exception {
		clickOnButton(map.getLocator("option12h"));
	}

	public void selectEmptyOption() throws Exception {
		clickOnButton(map.getLocator("optionEmpty"));
	}

	public void clickOnCancelSaveButton() throws Exception {
		clickOnButton(map.getLocator("btnCancelSave"));
	}

	public void clickOnWarningOkButton() throws Exception {
		Thread.sleep(1000);
		clickOnButton(map.getLocator("btnOkWarning"));
	}

	public void clickOnContinueAnywayButton() throws Exception {
		clickOnButton(map.getLocator("btnContinueAnyway"));
	}

	public void clickOnTimeFormatTypeButton() throws Exception {
		clickOnButton(map.getLocator("btnTimeFormatType"));
	}

	public void clickOnTimeFormatPerProfileOption() throws Exception {
		clickOnButton(map.getLocator("timeFormatPerProfile"));
	}

	public void clickOnTimeFormatPerProfile72402Options() throws Exception {
		waitLoading();
		String selectedOption = new Select(getDriver().findElement(map.getLocator("btnTimeFormatPerProfile72402")))
				.getFirstSelectedOption().getText();
		if (!selectedOption.equals("24-hour")) {
			clickOnButton(map.getLocator("btnTimeFormatPerProfile72402"));
		}
	}

	public void clickOnEmptyOptionPerProfile72402() throws Exception {
		clickOnButton(map.getLocator("optionEmptyPerProfile72402"));
	}

	public void clickOnEmptyOptionPerProfile72403() throws Exception {
		clickOnButton(map.getLocator("optionEmptyPerProfile72403"));
	}

	public void clickOnOption24HourPerProfile72402() throws Exception {
		clickOnButton(map.getLocator("option24HourPerProfile72402"));
	}

	public void clickOnOption12HoursPerProfile72402() throws Exception {
		clickOnButton(map.getLocator("option12HoursPerProfile72402"));
	}

	public void clickOnTimeFormatPerProfile72403Options() throws Exception {
		waitLoading();
		String selectedOption = new Select(getDriver().findElement(map.getLocator("btnTimeFormatPerProfile72403")))
				.getFirstSelectedOption().getText();
		if (!selectedOption.equals("24-hour")) {
			clickOnButton(map.getLocator("btnTimeFormatPerProfile72403"));
		}
	}

	public void clickOnOption24HourPerProfile72403() throws Exception {
		clickOnButton(map.getLocator("option24HourPerProfile72403"));
	}

	public void clickOnTimeFormatPerProfile72404Options() throws Exception {
		waitLoading();
		String selectedOption = new Select(getDriver().findElement(map.getLocator("btnTimeFormatPerProfile72404")))
				.getFirstSelectedOption().getText();
		if (!selectedOption.equals("24-hour")) {
			clickOnButton(map.getLocator("btnTimeFormatPerProfile72404"));
		}
	}

	public void clickOnOption24HourPerProfile72404() throws Exception {
		clickOnButton(map.getLocator("option24HourPerProfile72404"));
	}

	public void clickOnTimeFormatPerValueOption() throws Exception {
		clickOnButton(map.getLocator("timeFormatPerValue"));
	}

	public void clickOnCheckBoxProfile72402(String flag) throws Exception {
		waitLoading();
		boolean isEnabled = getDriver().findElement(map.getLocator("btnTimeFormatPerProfile72402")).isEnabled();
		if (flag.equals("Enable") && !isEnabled) {
			clickOnButton(map.getLocator("checkBoxProfile72402"));
		}
		if (flag.equals("Disable") && isEnabled) {
			clickOnButton(map.getLocator("checkBoxProfile72402"));
		}
	}

	public void clickOnCheckBoxProfile72403(String flag) throws Exception {
		waitLoading();
		boolean isEnabled = getDriver().findElement(map.getLocator("btnTimeFormatPerProfile72403")).isEnabled();
		if (flag.equals("Enable") && !isEnabled) {
			clickOnButton(map.getLocator("checkBoxProfile72403"));
		}
		if (flag.equals("Disable") && isEnabled) {
			clickOnButton(map.getLocator("checkBoxProfile72403"));
		}
	}

	public void clickOnCheckBoxProfile72404(String flag) throws Exception {
		waitLoading();
		boolean isEnabled = getDriver().findElement(map.getLocator("btnTimeFormatPerProfile72404")).isEnabled();
		if (flag.equals("Enable") && !isEnabled) {
			clickOnButton(map.getLocator("checkBoxProfile72404"));
		}
		if (flag.equals("Disable") && isEnabled) {
			clickOnButton(map.getLocator("checkBoxProfile72404"));
		}
	}

	public String getSelectedOption(String element) throws Exception {
		waitLoading();
		String selectedOption = new Select(getDriver().findElement(map.getLocator(element))).getFirstSelectedOption()
				.getText();
		if (selectedOption == null) {
			return "";
		}
		return selectedOption;
	}

	public ArrayList<String> getOptions(String element) throws Exception {
		waitLoading();
		Select sel = new Select(getDriver().findElement(map.getLocator(element)));
		ArrayList<String> options = new ArrayList<String>();
		for (int i = 1; i < sel.getOptions().size(); i++) {
			options.add(sel.getOptions().get(i).getText());
		}

		return options;
	}

	public boolean verifyOptions(String element) throws Exception {
		boolean result = false;
		ArrayList<String> optionsPerProfile = new ArrayList<String>();
		optionsPerProfile.add("12-hours");
		optionsPerProfile.add("24-hour");
		optionsPerProfile.add("24-hours");
		ArrayList<String> options = getOptions(element);
		if (options.containsAll(optionsPerProfile)) {
			result = true;
		}
		return result;
	}

	public void clickOnOkButtonWarningCannotSavePackage() throws Exception {
		Thread.sleep(1000);
		clickOnButton(map.getLocator("btnOkWarningCannotSavePackage"));
	}

	public boolean existsElement(String element) throws Exception {
		boolean result = existsElement(map.getLocator(element));
		return result;
	}
	
	public void moveTo(String element) throws Exception {
		moveTo(map.getLocator(element));
	}

	public void setNameFilter(String entry) throws Exception {
		waitLoading();
		try {
			setTextField(map.getLocatorVar("fieldNameFilter"), entry);
			waitLoading();
			getDriver().findElement(map.getLocatorVar("fieldNameFilter")).sendKeys(Keys.ENTER);
		} catch (Exception e) {
		}
	}

	public void setValueFilter(String entry) throws Exception {
		waitLoading();
		setTextField(map.getLocatorVar("fieldValueFilter"), entry);
		waitLoading();
		getDriver().findElement(map.getLocatorVar("fieldValueFilter")).sendKeys(Keys.ENTER);
	}

	public boolean itemIsVisible(String item) throws Exception {
		String itemName = item.split("#")[0];
		if (item.split("#").length > 1) {
			itemName = itemName + "#";
		}
		waitLoading();
		return existsElement(map.getLocatorVar("itemParametersVisible", itemName));
	}

	public void warningContinueAnyway() throws Exception {
		clickOnButton(map.getLocatorVar("btnYesPopup", "Warning"));
	}

	public void clickOnOnlyCheckedItems() throws Exception {
		waitLoading();
		WebElement element = getDriver().findElement(map.getLocator("btnOnlyCheckedItems"));
		clickOnButtonByJavaScript(element);
	}

	public void clickOnCollapseAllNodesButton() throws Exception {
		clickOnButton(map.getLocator("btnCollapseAllNodes"));
	}

	public void clickOnExpandAllNodesButton() throws Exception {
		clickOnButton(map.getLocator("btnExpandAllNodes"));
	}

	public void clickOnAddNewNameItemButton() throws Exception {
		clickOnButton(map.getLocator("btnAddNewNameItem"));
	}

	public void clickOnRemoveNameItemButton() throws Exception {
		clickOnButton(map.getLocator("btnRemoveNameItem"));
	}

	public void clickOnConfirmRemoveItemButton() throws Exception {
		clickOnButton(map.getLocator("btnConfirmRemoveItem"));
	}

	public void checkBoxItemNamePerProfile() throws Exception {
		clickOnButton(map.getLocator("checkBoxNamePerProfile"));
	}

	public void closeTab() throws Exception {
		getDriver().close();
	}

	/**
	 * This method set the value of item in a package give the buyer, the model and
	 * the path of item in item library If item parameter is "All" and value is ""
	 * the items will be cleaned
	 */
	public void preconditionItem(String model, String buyer, String item, String value, String path,
			boolean onlyThisItem) throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(model);
		screenPerformListPackages.setBuyer(buyer);
		boolean resultOk = screenPerformListPackages.clickOnEditProfilePackageButton();
		if (!resultOk) {
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.rejectGeneratingAPK(buyer);
			if (screenPerformListPackages.verifyIfDisplayVerticalPropagationPopUp()) {
				screenPerformListPackages.clickOnApplyVerticalPropagationInPopUp();
				screenPerformListPackages.waitLoadingOverlay();
			}
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		if (item.equals(ConstantsPackages.ALL)) {
			resultOk = screenPerformEditPackageProfile.clickOnNoItemAddedSelectItems();
			if (!resultOk) {
				screenPerformEditPackageProfile.clickOnSelectItemsButton();
				screenPerformEditPackageProfile.addItemByPath("");
				screenPerformEditPackageProfile.removeItemByPath("");
				screenPerformEditPackageProfile.moveToPageBottom();
				screenPerformEditPackageProfile.clickOnSaveSelectedItems();
				screenPerformEditPackageProfile.clickOnSaveProfile();
				screenPerformEditPackageProfile.warningContinueAnyway();
				screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
				screenPerformEditPackageProfile.clickOnSaveEditedProfile();
			}
		} else {
			if (onlyThisItem) {
				resultOk = screenPerformEditPackageProfile.clickOnSelectItemsButton();
				if (!resultOk) {
					screenPerformEditPackageProfile.clickOnNoItemAddedSelectItems();
				}
				String pathGroup = path.substring(0, path.lastIndexOf("::"));
				ArrayList<String> pathsToMandatoryItems = getMandatoryItemsInSelectItemsGivenGroup(pathGroup);
				if (pathsToMandatoryItems.size() != 0) {
					newTab("");
					switchTabs(1);
					for (String parhItem : pathsToMandatoryItems) {
						screenItemLibrary.preconditionItem(parhItem, ConstantsPackages.MANDATORY_FOR_GROUP, false);
					}
					getDriver().close();
					switchTabs(0);
					refreshPage();
					resultOk = screenPerformEditPackageProfile.clickOnSelectItemsButton();
					if (!resultOk) {
						screenPerformEditPackageProfile.clickOnNoItemAddedSelectItems();
					}
				}
				screenPerformEditPackageProfile.addItemByPath("");
				screenPerformEditPackageProfile.removeItemByPath("");
				screenPerformEditPackageProfile.addItemByPath(path.split(" #")[0]);
				screenPerformEditPackageProfile.moveToPageBottom();
				screenPerformEditPackageProfile.clickOnSaveSelectedItems();
			} else {
				screenPerformEditPackageProfile.setNameFilter(item.split("#")[0]);
				if (!screenPerformEditPackageProfile.itemIsVisible(item)) {
					resultOk = screenPerformEditPackageProfile.clickOnSelectItemsButton();
					if (!resultOk) {
						screenPerformEditPackageProfile.clickOnNoItemAddedSelectItems();
					}
					screenPerformEditPackageProfile.addItemByPath(path.split(" #")[0]);
					screenPerformEditPackageProfile.moveToPageBottom();
					screenPerformEditPackageProfile.clickOnSaveSelectedItems();
				}
			}
			String valueActual = screenPerformEditPackageProfile.getValueGivenNameItem(item);
			if (!valueActual.equals(value)) {
				screenPerformEditPackageProfile.setValueGivenNameItem(item, value);
			}
			screenPerformEditPackageProfile.setNameFilter("");
			screenPerformEditPackageProfile.setEmptyValuesRandomly();
			Thread.sleep(1000);
			screenPerformEditPackageProfile.clickOnSaveProfile();
			screenPerformEditPackageProfile.warningContinueAnyway();
			screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
			screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		}
	}

	public String[] preConditionsToTestCopyPackage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformEditPackageProfile screenPerformEditPackageProfile = new ScreenPerformEditPackageProfile(
				getDriver());
		ScreenPerformNormalPackage screenPerformNormalPackage = new ScreenPerformNormalPackage(getDriver());

		String namePackage1, namePackage2;

		screenPerformListPackages.openPage();
		screenPerformListPackages.setModel(ConstantsPackages.LGH815);
		screenPerformListPackages.setBuyer(ConstantsPackages.TIM_BRAZIL_BTM);

		String namePackage = screenPerformListPackages.getExistingPackage();
		int numberPackage = 1;

		if (screenPerformListPackages.verifyIsStatusUnderEdition()) {
			screenPerformListPackages.clickOnConfirmPackageButton();
			screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
			waitLoadingOverlay();
		}

		if (namePackage.contains(ConstantsPackages.PACKAGE_NAME_TEST_F_)) {
			numberPackage = Integer.parseInt(namePackage.split(" ")[1]);
			String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			namePackage1 = ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 1);
			if (nameStatus.equals("Under")) {
				screenPerformListPackages.clickOnConfirmPackageButton();
				screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
			}
		} else {
			namePackage1 = ConstantsPackages.PACKAGE_NAME_TEST_F_1;
		}

		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.TIM_BRAZIL_BTM,
				namePackage1, ConstantsPackages.TODAY, "", "3", ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.ANDROID_VERSION_5, ConstantsPackages.GRID_ROW_COLUMN_5X5,
				ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnOkPackageSuccessCreated();

		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.addItemByPath("");
		screenPerformEditPackageProfile.removeItemByPath("");
		screenPerformEditPackageProfile.addItem(ConstantsPackages.FREQUENCY_BAND_LTE);
		screenPerformEditPackageProfile.addItem(ConstantsPackages.FREQUENCY_BAND_UMTS);
		screenPerformEditPackageProfile.addItem(ConstantsPackages.FREQUENCY_BAND_GSM);
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.setValueGivenNameItem(ConstantsPackages.GSM, ConstantsPackages.GSM_2100);
		screenPerformEditPackageProfile.setValueGivenNameItem(ConstantsPackages.UMTS, ConstantsPackages.UMTS_2100);
		screenPerformEditPackageProfile.setValueGivenNameItem(ConstantsPackages.LTE, ConstantsPackages.LTE_7);
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		screenPerformListPackages.clickOnConfirmPackageButton();
		screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();

		namePackage2 = ConstantsPackages.PACKAGE_NAME_TEST_F_ + (numberPackage + 2);
		screenPerformListPackages.clickOnCreatePackageButton();
		screenPerformNormalPackage.fillFieldsPackage(ConstantsPackages.LGH815, "", ConstantsPackages.TIM_BRAZIL_BTM,
				namePackage2, ConstantsPackages.TODAY, "", "2", ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST,
				ConstantsPackages.ANDROID_VERSION_5, ConstantsPackages.GRID_ROW_COLUMN_5X5,
				ConstantsPackages.OS_VERSION_M, ConstantsPackages.UI_VERSION_4_2, ConstantsPackages.VOICE_OVER_NONE);
		screenPerformNormalPackage.clickOnSaveCreatedButton();
		screenPerformNormalPackage.clickOnOkPackageSuccessCreated();

		screenPerformListPackages.clickOnEditProfilePackageButton();
		screenPerformEditPackageProfile.clickOnSelectItemsButton();
		screenPerformEditPackageProfile.addItemByPath("");
		screenPerformEditPackageProfile.removeItemByPath("");
		screenPerformEditPackageProfile.addItemByPath(ConstantsPackages.PATH_HOME_NETWORK);
		screenPerformEditPackageProfile.moveToPageBottom();
		screenPerformEditPackageProfile.clickOnSaveSelectedItems();
		screenPerformEditPackageProfile.setValueGivenNameItem(ConstantsCompare.HOME_NETWORK_1,
				ConstantsPackages.HOME_NETWORK_724_31);
		Thread.sleep(500);
		screenPerformEditPackageProfile.clickOnSaveProfile();
		screenPerformEditPackageProfile.setMessageRevisionComment(ConstantsPackages.MESSAGE_ONLY_FOR_TESTING);
		screenPerformEditPackageProfile.clickOnSaveEditedProfile();
		return new String[] { namePackage1, namePackage2 };
	}

	private ArrayList<String> getMandatoryItemsInSelectItemsGivenGroup(String pathUnic) throws Exception {
		waitLoading();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for (int i = 0; i < path.length && !pathUnic.isEmpty(); i++) {
			pathToElement += getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative", path[i]));
		}
		pathToElement += getPathFromBy(map.getLocator("mandatoryItemLibraryRelative"));
		List<WebElement> itemsMandatory = getDriver().findElements(By.xpath(pathToElement));
		ArrayList<String> paths = new ArrayList<>();
		for (WebElement element : itemsMandatory) {
			String name = element.getText().split("\\(")[0];
			paths.add(pathUnic + "::" + name);
		}
		return paths;
	}

	public void setEmptyValuesRandomly() throws Exception {
		String value = "111 11";
		waitLoading();
		if (existsElement(map.getLocator("inputsEditProfile"))) {
			List<WebElement> emptyInputs = getDriver().findElements(map.getLocator("inputsEditProfile"));
			for (WebElement input : emptyInputs) {
				input.click();
			}
			waitLoading();
			if (existsElement(map.getLocator("emptyInputsEditProfile"))) {
				emptyInputs = getDriver().findElements(map.getLocator("emptyInputsEditProfile"));
				if (emptyInputs.size() != 0) {
					for (WebElement input : emptyInputs) {
						// String regex = input.getAttribute("regex");
						// if(!regex.equals("")){
						// Xeger generator = new Xeger(regex);
						// value = generator.generate();
						// }
						input.sendKeys(value);
					}
				}
			}
		}
	}

	/**
	 * This method selects one item in select items as long as this element is not
	 * repeated in the tree
	 * 
	 * @param element
	 * @return
	 * @throws Exception
	 */
	public boolean selectItemInSelectItemsTree(String element) throws Exception {
		waitLoading();
		List<WebElement> opts = getDriver().findElements(map.getLocatorVar("selectAnyElementInSelectItems", element));
		if (opts.size() > 0) {
			waitElementToBeClickable(map.getLocatorVar("selectAnyElementInSelectItems", element));
			opts.get(0).click();
			return true;
		}
		return false;
	}

	public boolean isItemCheckedByPath(String pathUnic) throws Exception {
		waitLoadingOverlay();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for (int i = 0; i < path.length && !pathUnic.isEmpty(); i++) {
			pathToElement += getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative", path[i]));
		}
		boolean isChecked = getDriver().findElement(By.xpath(pathToElement)).findElement(By.xpath("a"))
				.getAttribute("class").contains("checked");
		return isChecked;
	}

	/**
	 * This method unselect one item in select items as long as this element is not
	 * repeated in the tree
	 * 
	 * @param element
	 * @return
	 * @throws Exception
	 */
	public boolean unselectItemInSelectItemsTree(String element) throws Exception {
		try {
			waitLoading();
			WebElement opc = getDriver().findElement(map.getLocatorVar("unselectAnyElementInSelectItems", element));
			waitElementToBeClickable(map.getLocatorVar("unselectAnyElementInSelectItems", element));
			opc.click();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * This method select one item in select items as long as this element is not
	 * repeated in the tree
	 * 
	 * @throws Exception
	 */
	public boolean doubleClickOnElementInTree(String element) {
		try {
			WebElement opt = getDriver().findElement(map.getLocatorVar("getElementInTreeCheckedOrNo", element));
			Actions actions = new Actions(getDriver());
			actions.click(opt);
			actions.build().perform();
			actions.click(opt);
			actions.build().perform();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Verify if there is an element on the screen
	 * 
	 * @param elementName
	 * @return
	 * @throws Exception
	 */
	public boolean verifyExistsElementInParameters(String elementName) throws Exception {
		waitLoadingOverlay();
		By test = map.getLocatorVar("elementInTreeParameters", elementName);
		return existsElement(test);
	}

	public void optionFilters() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionFilters"));
	}

	public boolean clickOnSelectedFilter(String filter) throws Exception {
		return clickOnButton(map.getLocatorVar("choosedFilter", filter));
	}

	public boolean clickOnSelectedFilterOptions(String criteria) throws Exception {
		return clickOnButton(map.getLocatorVar("selectedFilterOptions", criteria));
	}

	public boolean isVisibleFilterOptions(String criteria) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		return getDriver().findElement(map.getLocatorVar("selectedFilterOptions", criteria)).isDisplayed();
	}

	public boolean isVisibleFilterSmartCaOption() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		return getDriver().findElement(map.getLocator("fieldDropDownSmartCaType")).isDisplayed();
	}

	public boolean clickOnSelectedFilterOption(String criteria, String option) throws Exception {
		return clickOnButton(map.getLocatorVar("selectedFilterOption", criteria, option));
	}

	public boolean clickOnSelectedOptionForSmartCaTypeFilter(String option) throws Exception {
		return clickOnButton(map.getLocatorVar("optionsToSmartCaTypesFilter", option));
	}

	public boolean clickOnSmartCaDropDown() throws Exception {
		return clickOnButton(map.getLocator("fieldDropDownSmartCaType"));
	}

	public boolean selectValueFilterOptionWithArea(String criteria, String option, String area) throws Exception {
		return setTextFieldDropdownWithArea(map.getLocatorVar("fieldCritertia", criteria), area, option);
	}

	public ArrayList<String> getAllValuesOfTagValues() throws Exception {
		String pathFilterBase = getPathFromBy(
				map.getLocatorVar("selectedFilterOptions", ConstantsBuyers.CRITERIA_TAG_VALUES));
		String values = getDriver().findElement(By.xpath(pathFilterBase)).findElement(By.xpath("span")).getText();
		ArrayList<String> result = new ArrayList<>(Arrays.asList(values.split(", ")));
		return result;
	}

	public void setTextOnFilterName(String text) throws Exception {
		setTextField(map.getLocator("fieldNameOnFilterName"), text);
	}

	public boolean isVisibleFilterName() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		return getDriver().findElement(map.getLocator("fieldNameOnFilterName")).isDisplayed();
	}

	public boolean selectAllFilters() throws Exception {
		waitLoading();
		return clickOnButton(map.getLocator("selectAllFilters"));
	}

	public boolean deselectAllFilters() throws Exception {
		waitLoading();
		return clickOnButton(map.getLocator("deselectAllFilters"));
	}

	public boolean removeFilter(String filterCriteria) throws Exception {
		waitLoading();
		boolean result = false;
		WebElement element = getDriver().findElement(map.getLocatorVar("btnRemoveFilter", filterCriteria));
		if (element != null) {
			element.click();
			result = true;
		}
		return result;
	}

	public String getBuyerValuesFromSyncPage() throws Exception {
		waitLoading();
		String value = getDriver().findElement(map.getLocator("buyerValueSyncHomeNetwork")).getText().split(": ")[1];
		return value.substring(0, value.length());
	}

	public String getPackageValuesFromSyncPage() throws Exception {
		waitLoading();
		String value = getDriver().findElement(map.getLocator("packageValueSyncHomeNetwork")).getText().split(": ")[1];
		return value.substring(0, value.length());
	}

	public String getHomeNetworkValuesFromPackageSyncPage() throws Exception {
		waitLoading();
		String value = getDriver().findElement(map.getLocator("txtHomeNetworkChanged")).getAttribute("value")
				.split(": ")[0];
		return value.substring(0, value.length());
	}

	public String getMsgSynchronizePackage() throws Exception {
		waitLoading();
		if (existsElement(globalMap.getLocatorVar("errorMsg", " Synchronize Package"))) {
			return getDriver().findElement(globalMap.getLocatorVar("errorMsg", " Synchronize Package")).getText();
		}
		return "";
	}

	public ArrayList<String> getFirstItemsOfEachRootDirectory() throws Exception {
		ArrayList<String> paths = new ArrayList<>();
		boolean resultOk;
		By itemParameters = By.xpath("//*[@id='g:0']/ul/li[1]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemCustomization = By.xpath("//*[@id='g:0']/ul/li[2]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemProfiles = By.xpath("//*[@id='g:0']/ul/li[3]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemLibrary = By.xpath("//*[@id='g:0']/ul/li[4]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemSmartCA = By.xpath("//*[@id='g:0']/ul/li[5]//li/a/parent::li[contains(@id,'i:')]/a");

		resultOk = rightClickOnButton(itemParameters);
		if (resultOk) {
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}

		resultOk = rightClickOnButton(itemCustomization);
		if (resultOk) {
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}

		resultOk = rightClickOnButton(itemProfiles);
		if (resultOk) {
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}

		resultOk = rightClickOnButton(itemLibrary);
		if (resultOk) {
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}

		resultOk = rightClickOnButton(itemSmartCA);
		if (resultOk) {
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}

		return paths;
	}

	public String getPathFromAlert() throws Exception {
		Thread.sleep(500);
		Alert alert = getDriver().switchTo().alert();
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_C);
		r.keyRelease(KeyEvent.VK_C);
		r.keyRelease(KeyEvent.VK_CONTROL);
		Thread.sleep(100);
		String path = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
		alert.accept();
		Thread.sleep(100);
		return path;
	}

	/**
	 * Verify if all elements checked are displayed
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean verifyElementsCheckedsIsDisplayed() throws Exception {
		List<WebElement> getElementsChecked = getDriver()
				.findElements(map.getLocator("getElementsCheckedInSelectItems"));

		for (WebElement checked : getElementsChecked) {
			if (!checked.isDisplayed()) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Verify if all elements unchecked are displayed
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean verifyElementsUncheckedsIsDisplayed() throws Exception {
		List<WebElement> elementsInSelectItems = getDriver()
				.findElements(map.getLocator("getElementsUncheckedInSelectItems"));
		for (WebElement e : elementsInSelectItems) {
			if (!e.isDisplayed()) {
				return false;
			}
		}
		return true;
	}

	public boolean itemIsHighlightedInTree(String pathToItem) throws Exception {
		waitLoading();
		String[] path = pathToItem.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for (int i = 0; i < path.length && !pathToItem.isEmpty(); i++) {
			pathToElement += getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative", path[i]));
		}
		pathToElement += getPathFromBy(map.getLocator("nameItemLibraryRelative"));
		return getDriver().findElement(By.xpath(pathToElement)).getAttribute("class").contains("jstree-search");
	}

	public boolean verifyClassElement(String elementName, String nameClass) throws Exception {
		By element = map.getLocatorVar("getElementInTreeOfParameters", elementName);

		if (existsElement(element)) {
			WebElement pack = getDriver().findElement(element);
			if (pack.getAttribute("class").contains(nameClass))
				return true;
		}
		return false;
	}

	public boolean btnOptionInTree(String optionName) throws Exception {
		By opt = map.getLocatorVar("btnOptionsInTreeEditPackage", optionName);
		return clickOnButton(opt);
	}

	public void onlyOneItemOnLibrary() throws Exception {
		ScreenPerformListPackages listPackages = new ScreenPerformListPackages(getDriver());
		listPackages.setModel("LGH815");
		listPackages.setBuyer("Claro Brazil (CLR)");
		listPackages.clickOnEditProfilePackageButton();
		clickOnSelectItemsButton();
		clickOnItemLibraryButton();
		clickOnItemLibraryButton();
		addItem("homeNetwork");
		clickOnSaveSelectedItems();
		setValueItemPerProfile(ConstantsCompare.PATH_ITEM_HOME_NETWORK_1, "724 99");
		clickOnSaveProfile();
		setMessageRevisionComment("Only for testing");
		clickOnSaveEditedProfile();
	}

	public void addApp(String app) throws Exception {
		WebElement frame = getDriver().findElement(map.getLocator("frameAppPool"));
		getDriver().switchTo().frame(frame);
		WebElement search = getDriver().findElement(map.getLocator("searchAppOnApplicationPool"));
		search.clear();
		search.sendKeys(app);
		Thread.sleep(1000);
		clickOnButton(map.getLocatorVar("selectedApp", app));
		getDriver().switchTo().defaultContent();
	}

	public boolean clickOnCannotSaveBuyerOkButton() throws Exception {
		waitLoading();
		return clickOnButton(map.getLocator("btnOkWarningCannotSaveBuyer"));
	}

	public String addItemToFolder(String pathUnic) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocatorVar("rootTableItem", path[0]));
		for (int i = 1; i < path.length && !pathUnic.isEmpty(); i++) {
			pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItem", path[i]));
		}
		String input = pathToElement
				+ getPathFromBy(map.getLocatorVar("hierarchyTableItem", path[path.length - 1] + " #"));
		input += getPathFromBy(map.getLocator("inputPositionTableItem"));
		pathToElement += getPathFromBy(map.getLocator("addItemTableItem"));
		clickOnButton(By.xpath(pathToElement));
		String attribute = getDriver().findElement(By.xpath(input)).getAttribute("value");
		return attribute;
	}

	public boolean removeItemInFolder(String pathUnic) throws Exception {
		boolean result = false;
		waitLoading();
		waitLoadingOverlay();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocatorVar("rootTableItem", path[0]));
		for (int i = 1; i < path.length - 1 && !pathUnic.isEmpty(); i++) {
			pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItem", path[i]));
		}
		String nameItem = path[path.length - 1].split(" #")[0] + " #";
		String numberItem = path[path.length - 1].split(" #")[1];
		pathToElement += getPathFromBy(map.getLocatorVar("removeItemTableItem", nameItem, numberItem));
		result = clickOnButton(By.xpath(pathToElement));
		result = result && confirmRemovetableItem();
		waitLoadingOverlay();
		return result;
	}

	public boolean confirmRemovetableItem() throws Exception {
		return clickOnButton(globalMap.getLocatorVar("popupButton", "Remove Item", "Ok"));
	}

	/**
	 * Set Option to Per profile and set the value (input or dropdown) given the
	 * path
	 */
	public void setValueItemPerProfile(String pathUnic, String value) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocatorVar("rootTableItem", path[0]));
		for (int i = 1; i < path.length && !pathUnic.isEmpty(); i++) {
			if (!path[i].contains("#")) {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItem", path[i]));
			} else {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItemNumeric",
						path[i].split(" #")[0] + " #", path[i].split(" #")[1]));
			}
		}
		String pathToOption = getPathFromBy(map.getLocator("relativeOptionParemetersGeneric"));
		String pathToPerProfile = getPathFromBy(map.getLocator("relativePerProfileParemeters"));
		By locatorOption = By.xpath(pathToElement + pathToOption);
		By locatorPerProfile = By.xpath(pathToElement + pathToOption + pathToPerProfile);
		moveTo(map.getLocator("lastTableItemEditProfile"));
		clickOnButton(locatorOption);
		moveTo(locatorPerProfile);
		clickOnButton(locatorPerProfile);

		String pathToFieldSelect = getPathFromBy(map.getLocator("relativeFieldSelectParemetersGeneric"));
		String pathToFieldInput = getPathFromBy(map.getLocator("relativeFieldInputParemetersGeneric"));
		By locatorFieldSelect = By.xpath(pathToElement + pathToOption + pathToFieldSelect);
		By locatorFieldInput = By.xpath(pathToElement + pathToOption + pathToFieldInput);
		if (existsElement(locatorFieldSelect)) {
			WebElement findElements = getDriver().findElement(locatorFieldSelect);
			List<WebElement> findElements2 = findElements.findElements(By.tagName("option"));
			for (WebElement element : findElements2) {
				if (value.equals(element.getText())) {
					element.click();
					break;
				}
			}
		} else if (existsElement(locatorFieldInput)) {
			setTextField(locatorFieldInput, value);
		}
		Thread.sleep(1000);
	}

	/**
	 * Set Option to Value and set the value (input or dropdown) given the path
	 * 
	 * @throws Exception
	 */
	public void setValueItemValue(String pathUnic, String value) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocatorVar("rootTableItem", path[0]));
		for (int i = 1; i < path.length && !pathUnic.isEmpty(); i++) {
			if (!path[i].contains("#")) {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItem", path[i]));
			} else {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItemNumeric",
						path[i].split(" #")[0] + " #", path[i].split(" #")[1]));
			}
		}
		String pathToOption = getPathFromBy(map.getLocator("relativeOptionParemetersGeneric"));
		String pathToValue = getPathFromBy(map.getLocator("relativePerValueParameters"));
		By locatorOption = By.xpath(pathToElement + pathToOption);
		By locatorValue = By.xpath(pathToElement + pathToOption + pathToValue);
		moveTo(map.getLocator("lastTableItemEditProfile"));
		clickOnButton(locatorOption);
		moveTo(locatorValue);
		clickOnButton(locatorValue);

		String pathToFieldSelect = getPathFromBy(map.getLocator("relativeFieldSelectParemetersGeneric"));
		String pathToFieldInput = getPathFromBy(map.getLocator("relativeFieldInputParemetersGeneric"));
		By locatorFieldSelect = By.xpath(pathToElement + pathToOption + pathToFieldSelect);
		By locatorFieldInput = By.xpath(pathToElement + pathToOption + pathToFieldInput);
		if (existsElement(locatorFieldSelect)) {
			WebElement findElements = getDriver().findElement(locatorFieldSelect);
			List<WebElement> findElements2 = findElements.findElements(By.tagName("option"));
			for (WebElement element : findElements2) {
				if (value.equals(element.getText())) {
					element.click();
					break;
				}
			}
		} else if (existsElement(locatorFieldInput)) {
			setTextField(locatorFieldInput, value);
		}
		Thread.sleep(1000);
	}

	/**
	 * Returns the value (input or dropdown) given the path
	 * 
	 * @throws Exception
	 */
	public String getValueItem(String pathUnic) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocatorVar("rootTableItem", path[0]));
		for (int i = 1; i < path.length && !pathUnic.isEmpty(); i++) {
			if (!path[i].contains("#")) {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItem", path[i]));
			} else {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItemNumeric",
						path[i].split(" #")[0] + " #", path[i].split(" #")[1]));
			}
		}
		String pathToOption = getPathFromBy(map.getLocator("relativeOptionParemetersGeneric"));
		String pathToFieldSelect = getPathFromBy(map.getLocator("relativeFieldSelectParemetersGeneric"));
		String pathToFieldInput = getPathFromBy(map.getLocator("relativeFieldInputParemetersGeneric"));
		By locatorOption = By.xpath(pathToElement + pathToOption);
		moveTo(map.getLocator("lastTableItemEditProfile"));
		moveTo(locatorOption);
		By locatorFieldSelect = By.xpath(pathToElement + pathToOption + pathToFieldSelect);
		By locatorFieldInput = By.xpath(pathToElement + pathToOption + pathToFieldInput);
		if (existsElement(locatorFieldSelect)) {
			return getDriver().findElement(locatorFieldSelect).getAttribute("value");
		} else if (existsElement(locatorFieldInput)) {
			return getDriver().findElement(locatorFieldInput).getAttribute("value");
		}
		return "";
	}

	/**
	 * Returns the message above the field input of item (in the case of field is
	 * empty)
	 */
	public String getMsgWarningItem(String pathUnic) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocatorVar("rootTableItem", path[0]));
		for (int i = 1; i < path.length && !pathUnic.isEmpty(); i++) {
			if (!path[i].contains("#")) {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItem", path[i]));
			} else {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItemNumeric",
						path[i].split(" #")[0] + " #", path[i].split(" #")[1]));
			}
		}
		String pathToOption = getPathFromBy(map.getLocator("relativeOptionParemetersGeneric"));
		String pathToFieldSelect = getPathFromBy(map.getLocator("relativeFieldSelectParemetersGeneric"));
		String pathToFieldInput = getPathFromBy(map.getLocator("relativeFieldInputParemetersGeneric"));
		By locatorOption = By.xpath(pathToElement + pathToOption);
		moveTo(map.getLocator("lastTableItemEditProfile"));
		moveTo(locatorOption);
		By locatorFieldSelect = By.xpath(pathToElement + pathToOption + pathToFieldSelect);
		By locatorFieldInput = By.xpath(pathToElement + pathToOption + pathToFieldInput);
		if (existsElement(locatorFieldSelect)) {
			return getDriver().findElement(locatorFieldSelect)
					.findElement(By.xpath("following-sibling::span[@class='errorMessage']")).getText();
		} else if (existsElement(locatorFieldInput)) {
			return getDriver().findElement(locatorFieldInput)
					.findElement(By.xpath("following-sibling::span[@class='errorMessage']")).getText();
		}
		return "";
	}

	public String getMSgWaringMarkedFields() throws Exception {
		waitLoading();
		if (existsElement(globalMap.getLocatorVar("errorMsg", " Warning"))) {
			return getDriver().findElement(globalMap.getLocatorVar("errorMsg", " Warning")).getText();
		}
		return "";
	}

	public boolean clickOkWarningFields() throws Exception {
		return clickOnButton(globalMap.getLocatorVar("popupButton", " Warning", "Ok"));
	}

	public void checkItemsAsDoNotApplyButton(String pathUnic) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocatorVar("rootTableItem", path[0]));
		for (int i = 1; i < path.length && !pathUnic.isEmpty(); i++) {
			if (!path[i].contains("#")) {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItem", path[i]));
			} else {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItemNumeric",
						path[i].split(" #")[0] + " #", path[i].split(" #")[1]));
			}
		}
		String pathToCheckDoNotApply = getPathFromBy(map.getLocator("relativeDoNotApplyParemetersGeneric"));
		By locatorCheckDoNotApply = By.xpath(pathToElement + pathToCheckDoNotApply);
		moveTo(map.getLocator("lastTableItemEditProfile"));
		moveTo(locatorCheckDoNotApply);
		if (!getDriver().findElement(locatorCheckDoNotApply).isSelected()) {
			clickOnButton(locatorCheckDoNotApply);
		}
	}

	public boolean clickOkSortIndex() throws Exception {
		return clickOnButton(globalMap.getLocatorVar("popupButton", "Sort Indexes", "Sort"));
	}

	public void sortFolder(String pathUnic) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocatorVar("rootTableItem", path[0]));
		for (int i = 1; i < path.length && !pathUnic.isEmpty(); i++) {
			if (!path[i].contains("#")) {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItem", path[i]));
			} else {
				pathToElement += getPathFromBy(map.getLocatorVar("hierarchyTableItemNumeric",
						path[i].split(" #")[0] + " #", path[i].split(" #")[1]));
			}
		}
		String pathToSort = getPathFromBy(map.getLocator("sortFolderTableItem"));
		By locatorSort = By.xpath(pathToElement + pathToSort);
		moveTo(map.getLocator("lastTableItemEditProfile"));
		moveTo(locatorSort);
		clickOnButton(locatorSort);
		clickOkSortIndex();
	}

	public void verifyIfPackageHasItemsAndClickToSelectItems() throws Exception {
		waitLoading();
		if (existsElement("btnAddItemToPackageWithNoItem")) {
			clickOnButton(map.getLocator("btnAddItemToPackageWithNoItem"));
		} else {
			clickOnSelectItemsButton();
		}
	}

	public void removeAllItemsOnProfilePackages() throws Exception {
		By btnRemove = map.getLocator("btnRemoveItemProfilePackage");
		List<WebElement> itemsToRemove = getDriver().findElements(btnRemove);
		if (existsElement("btnRemoveItemProfilePackage")) {
			for (int i = 0; i < itemsToRemove.size(); i++) {
				itemsToRemove.get(i).click();
				clickOnConfirmRemoveItemButton();
			}
		}
	}

	public void clickOkOkPopUpSynchronizePackageWithoutChange() throws Exception {
		if (existsElement(globalMap.getLocatorVar("errorMsg", " Synchronize Package"))) {
			clickOnButton(map.getLocator("btnOkOnSynchronizePackagePopUp"));
		}
	}

	public void clickOkOkPopUpUpdatePackage() throws Exception {
		if (existsElement(globalMap.getLocatorVar("errorMsg", " Update Package"))) {
			clickOnButton(map.getLocator("btnOkOnUpdatePackagePopUp"));
		}
	}

	public String getMsgNothingToChange() throws Exception {
		return getDriver().findElement(globalMap.getLocatorVar("errorMsg", " Copy Package")).getText();
	}

	public void verifyStatusAndClickOnEditProfilePackage() throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		String nameStatus = screenPerformListPackages.getTextFrom("status").split(" ")[0];
		if (nameStatus.equals("Under")) {
			screenPerformListPackages.clickOnEditProfilePackageButton();
		} else if ("APK".contains(nameStatus)) {
			screenPerformListPackages.selectReopenPackage(ConstantsPackages.TARGET_DATE_26_11_18_OTHER_FORMAT);
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
	}
}