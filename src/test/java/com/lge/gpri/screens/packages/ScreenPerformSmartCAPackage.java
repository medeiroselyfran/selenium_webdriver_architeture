package com.lge.gpri.screens.packages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.lge.gpri.helpers.constants.ConstantsPackages;

/**
 * Perform create and edit samrt CA page class.
 * 
 * <p>
 * Description: Methods to create and edit a Smart CA package.
 * 
 * @author Cayk Lima Barreto
 */

public class ScreenPerformSmartCAPackage extends ScreenPerformAbstractPackage {

	public ScreenPerformSmartCAPackage(WebDriver driver) {
		super(driver);
	}

	/*
	 * public void fillFieldsPackage(String model, String buyer, String packageName,
	 * String targetDate, String uiSimulator, String creationReasons, String
	 * androidVersion, String gridRowColumn, String osVersion, String smartCAType,
	 * String providingType, String openSW, String ntCode) throws Exception {
	 * 
	 * setModel(model); setBuyerValues(buyer); setPackageName(packageName);
	 * setTargetDate(targetDate); setUISimulator(uiSimulator);
	 * setCreationReasons(creationReasons); setAndroidVersion(androidVersion);
	 * setGridRowAndColumn(gridRowColumn); setOSVersion(osVersion);
	 * moveToPageBottom(); setSmartCAType(smartCAType, providingType);
	 * setOpenSW(openSW); setNTCodeList(ntCode); }
	 */

	public void fillFieldsPackage(String model, String buyer, String packageName, String targetDate, String uiSimulator,
			String creationReasons, String smartCAType, String providingType, String openSW, String ntCode,
			String swVersionFrom, String swVersionTo) throws Exception {

		setModel(model);
		setBuyerValues(buyer);
		setPackageName(packageName);
		setTargetDate(targetDate);
		setUISimulator(uiSimulator);
		setCreationReasons(creationReasons);
		setDinamicTagValues();
		moveToPageBottom();
		setSmartCAType(smartCAType, providingType);
		setOpenSW(openSW);
		setNTCodeList(openSW, ntCode);
		setSwVersionFrom(swVersionFrom);
		setSwVersionTo(swVersionTo);
	}

	public void logicForCreateNewSmartCaPackage(String nameStatus, String namePackage, String buyer) throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformSmartCAPackage screenPerformSmartCAPackage = new ScreenPerformSmartCAPackage(getDriver());
		screenPerformListPackages.cleanFieldSearchPackage();
		if (null == namePackage) {
			namePackage = ConstantsPackages.PACKAGE_NAME_TESTSMARTCA_;
		}
		if (nameStatus.equals("Under")) {
			screenPerformListPackages.clickOnConfirmPackageButton();
			screenPerformListPackages.clickOnOKButtonAfterConfirmPackage();
			if (screenPerformListPackages.isDisplayedPopUpConfirmSmartCA()) {
				screenPerformListPackages.clickOnOkConfirmSmartCA();
				screenPerformListPackages.waitLoading();
				screenPerformListPackages.existElementsProgressBar();
			}
			String nameStatusNew = screenPerformListPackages.getTextFrom("status").split(" ")[0];
			if ("APK".contains(nameStatusNew)) {
				screenPerformListPackages.rejectPackage();
			}
			screenPerformListPackages.waitLoading();
			screenPerformListPackages.clickOnCreateSmartCAButton();
			fillFieldsPackage(screenPerformSmartCAPackage, namePackage, buyer);
		} else if ("APK".contains(nameStatus)) {
			screenPerformListPackages.rejectPackage();
			screenPerformListPackages.clickOnCreateSmartCAButton();
			fillFieldsPackage(screenPerformSmartCAPackage, namePackage, buyer);
		} else {
			screenPerformListPackages.clickOnCreateSmartCAButton();
			fillFieldsPackage(screenPerformSmartCAPackage, namePackage, buyer);
		}
		screenPerformSmartCAPackage.clickOnSaveCreatedButton();
		screenPerformSmartCAPackage.clickOnOkPackageSuccessCreated();
	}

	private void fillFieldsPackage(ScreenPerformSmartCAPackage screenPerformSmartCAPackage, String namePackage,
			String buyer) throws Exception {
		screenPerformSmartCAPackage.fillFieldsPackage(ConstantsPackages.LGH815, buyer, namePackage,
				ConstantsPackages.TODAY, ConstantsPackages.UI_SIMULATOR_DEFAULT_ANDROID_GB,
				ConstantsPackages.CREATE_REASONS_ONLY_FOR_TEST, ConstantsPackages.NORMAL_SMARTCA,
				ConstantsPackages.AUTO_ACTIVATION_WITH_WIFI, ConstantsPackages.OPEN_SW_YES,
				ConstantsPackages.NT_CODE_LIST_1_FFF,
				ConstantsPackages.SW_VERSION_FROM, ConstantsPackages.SW_VERSION_TO);
	}

	public boolean verifyPackageCreatedWithSuccess(String model, String buyer, String packageName, String uiSimulator,
			String androidVersion, String gridRowColumn, String osVersion, String smartCAType, String providingType)
			throws Exception {
		return model.equals(getModelOnPackageEditPage()) && buyer.equals(getBuyerOnPackageEditPage())
				&& packageName.equals(getPackageName()) && uiSimulator.equals(getUISimulator())
				&& androidVersion.equals(getAndroidVersion()) && gridRowColumn.equals(getGridRowAndColumn())
				&& osVersion.equals(getOSVersion()) && smartCAType.equals(getSmartCAType())
				&& providingType.equals(getProvidingType());
	}

	public void setSmartCAType(String type, String providingType) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Smart CA Values", "SmartCA Type"));
		setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Smart CA Values", "SmartCA Type"), type);
		if ("Normal SmartCA".equals(type)) {
			setProvidingType(providingType);
		} else if ("Small SmartCA".equals(type)) {
			setProvidingType(providingType);
		} else if ("".equals(type)) {
			clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Smart CA Values", "SmartCA Type"));
		}
	}

	public String getSmartCAType() throws Exception {
		waitLoading();
		moveToPageBottom();
		return getDriver()
				.findElement(map.getLocatorVar("optionGererciOnCreatePackage", "Smart CA Values", "SmartCA Type"))
				.getText().split("\n")[0];
	}

	public void setProvidingType(String providingType) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Smart CA Values", "Providing Type"));
		setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Smart CA Values", "Providing Type"),
				providingType);
	}

	public String getProvidingType() throws Exception {
		waitLoading();
		return getDriver()
				.findElement(map.getLocatorVar("optionGererciOnCreatePackage", "Smart CA Values", "Providing Type"))
				.getText().split("\n")[0];
	}

	public void setOpenSW(String openSW) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionGererciOnCreatePackage", "Smart CA Values", "Open SW"));
		setTextFieldDropdown(map.getLocatorVar("inputGenericOnCreatePackage", "Smart CA Values", "Open SW"), openSW);
	}

	public void setNTCodeList(String openSW, String ntCodeList) throws Exception {
		if (ConstantsPackages.OPEN_SW_YES.equals(openSW)) {
			setTextField(map.getLocator("fieldNTCodeList"), ntCodeList);
		}
	}

	public void setSwVersionFrom(String swVersionFrom) throws Exception {
		setTextField(map.getLocator("txtSwVersionFrom"), swVersionFrom);
	}

	public void setSwVersionTo(String swVersionTo) throws Exception {
		setTextField(map.getLocator("txtSwVersionTo"), swVersionTo);
	}

	public boolean verifyErrorOnSmartCATypeField() throws Exception {
		waitLoading();
		moveToPageBottom();
		By by = map.getLocatorVar("optionGererciFieldName", "Smart CA Values", "SmartCA Type");
		return ConstantsPackages.CONTROL_LABEL_ERROR.equals(getDriver().findElement(by).getAttribute("class"));
	}

	public void clickOnSaveCreatedButton() throws Exception {
		savePackage(map.getLocator(ConstantsPackages.BUTTON_CREATE_PACKAGE));
	}

	public void clickOnSaveEditedButton() throws Exception {
		savePackage(map.getLocator(ConstantsPackages.BUTTON_EDIT_PACKAGE));
	}

	public void clickOnClosePopup() throws Exception {
		savePackage(map.getLocator(ConstantsPackages.BUTTON_CLOSE_POPUP));
	}
}