package com.lge.gpri.screens.itemLibrary;

import org.openqa.selenium.WebDriver;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
* Perform Change Log page class.
* 
* <p>Description: Methods to realize the tests on Cnage Log screen.
*  
* @author Gabriel Costa do Nascimento
*/


public class ScreenChangeLog extends AbstractScreen{

	public ScreenChangeLog(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_ITEM_LIBRARY);
	}
	
	public String getDateFirstLog() throws Exception {
		return getDriver().findElement(map.getLocator("dateChangeLog")).getText();
	}
	
	public String getLoginFirstLog() throws Exception {
		return getDriver().findElement(map.getLocator("loginChangeLog")).getText();
	}
	
	public String getDescriptionFirstLog() throws Exception {
		return getDriver().findElement(map.getLocator("descriptionChangeLog")).getText();
	}
}