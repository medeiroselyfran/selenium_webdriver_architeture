package com.lge.gpri.screens.itemLibrary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
* <pre>
* Author: great
* Purpose: <Type here the purpose of the class> - great - 4 de out de 2017
* History: Class creation - great - 4 de out de 2017
*
*</pre>
*/

public class ScreenTags extends AbstractScreen{

	public ScreenTags(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_TAGS);
	}
	
	public void addNewTagType(String name) throws Exception{
		By field = map.getLocator("fieldAddTagType");
		setTextField(field, name);
		By btn = map.getLocator("btnAddNewTag");
		clickOnButton(btn);
		waitLoadingOverlay();
		String msg = getErrorMsg(" Error");
		if(msg.contains("Already exists a Tag Type with the same 'Type Name'.")){
			closeErrorMsg(" Error");
		}
	}
	
	public void addValueInNewTag(String... value) throws Exception{
		By valueTag = map.getLocator("newTagValue");
		By btn = map.getLocator("btnAddNewValueTag");
		for(String elements : value){		
			setTextField(valueTag, elements);
			clickOnButton(btn);
		}
	}
	
	public boolean confirmAlertSucces() throws Exception{
		By alert = map.getLocator("alertSuccess");
		waitElementToBeVisible(alert);
		By btn = map.getLocator("btnOkAlertSuccess");
		return clickOnButton(btn);
	}
	
	public boolean clickOnSave() throws Exception{
		By btn = map.getLocator("btnSave");
		clickOnButton(btn);
		return confirmAlertSucces();
	}
	
	public void clickOnElementInList(String value) throws Exception{
		By opt = map.getLocatorVar("getElementListTags", value);
		if(existsElement(opt))
			clickOnButton(opt);
	}
	
}