package com.lge.gpri.screens.itemLibrary;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.helpers.constants.ConstantsItemLibrary;
import com.lge.gpri.screens.AbstractScreen;

/**
 * <pre>
* Author: great
* Purpose: <Type here the purpose of the class> - great - 13 de set de 2017
* History: Class creation - great - 13 de set de 2017
 *
 * </pre>
 */

public class ScreenItemLibrary extends AbstractScreen {

	public ScreenItemLibrary(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_ITEM_LIBRARY);
	}

	public void openPage() {
		getDriver().get("http://136.166.96.136:8204/Views/Admin/ItemLibrary/#Group/0");
		getDriver().manage().window().maximize();
	}

	public void rightClickOnNetwork() throws Exception {
		WebElement element = getDriver().findElement(map.getLocator("folderNetwork"));
		Actions action = new Actions(getDriver()).contextClick(element);
		action.build().perform();
	}

	public boolean verifyOptionsInScreen(String option) throws Exception {
		By optionOnList = map.getLocatorVar("listOptionsRightClick", option);

		return existsElement(optionOnList);
	}

	public boolean clickOnNewItem() throws Exception {
		By newItem = map.getLocatorVar("listOptionsRightClick", "New Item");

		if (existsElement(newItem)) {
			getDriver().findElement(newItem).click();
			return true;
		}
		return false;
	}

	public void putTextOnFieldSearch(String text) throws Exception {
		By field = map.getLocator("fieldSearch");
		setTextField(field, text);
		waitLoadingOverlay();
	}

	public boolean selectItemInTree(String pathUnic) throws Exception {
		waitLoading();
		Thread.sleep(300);
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for (int i = 0; i < path.length && !pathUnic.isEmpty(); i++) {
			pathToElement += getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative", path[i]));
		}
		pathToElement += getPathFromBy(map.getLocator("checkBoxItemLibraryRelative"));
		return clickOnButton(By.xpath(pathToElement));
	}

	public void selectOption(String nameOption) throws Exception {
		waitLoadingOverlay();
		By op = map.getLocatorVar("propertiesOptions", nameOption);
		WebElement option = getDriver().findElement(op);
		option.click();
	}

	public void selectOptionFolder(String nameOption) throws Exception {
		waitLoadingOverlay();
		By op = map.getLocatorVar("propertiesFolderGroupInfo", nameOption);
		WebElement option = getDriver().findElement(op);
		option.click();
	}

	public boolean clickOnButtonSave() throws Exception {
		waitLoadingOverlay();
		By btnSave = map.getLocator("btnSave");
		boolean result = clickOnButton(btnSave);
		By alertField = map.getLocator("alertSuccess");
		waitElementToBeVisible(alertField);
		return result;
	}

	public void fillTagType(String optionTagType) throws Exception {
		waitLoading();
		By searchInput = map.getLocator("fillTagType");
		setTextField(searchInput, optionTagType);
		clickOnButton(searchInput);
		getDriver().findElement(searchInput).sendKeys(Keys.ENTER);
	}

	public void fillTagValue(String optionTagValue) throws Exception {
		waitLoading();
		By searchInput = map.getLocator("fillTagValue");
		setTextField(searchInput, optionTagValue);
		clickOnButton(searchInput);
		getDriver().findElement(searchInput).sendKeys(Keys.ENTER);
	}

	public void fillRegion(String region) throws Exception {
		waitLoading();
		By searchInput = map.getLocator("fillRegions");
		setTextField(searchInput, region);
		clickOnButton(searchInput);
		getDriver().findElement(searchInput).sendKeys(Keys.ENTER);
	}

	public void fillCheckProcedure(String value) throws Exception {
		getDriver().switchTo().frame(getDriver().findElement(map.getLocator("iframeCheckProcedure")));
		By textArea = map.getLocator("fieldCheckProcedure");
		getDriver().findElement(textArea).clear();
		WebElement fieldElement = getDriver().findElement(textArea);
		Actions action = new Actions(getDriver());
		action.sendKeys(fieldElement, value);
		action.build().perform();
		getDriver().switchTo().defaultContent();
	}
	
	public void removeCheckProcedure() throws Exception{
		getDriver().switchTo().frame(getDriver().findElement(map.getLocator("iframeCheckProcedure")));
		By textArea = map.getLocator("fieldCheckProcedure");
		getDriver().findElement(textArea).clear();
		getDriver().switchTo().defaultContent();
	}

	public void removeTagOrRegion(String option) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("removeTag", option));
	}
	
	public void removeTagValue(String option) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("removeTag", option));
	}
	
	public void selectCheckProcedure() throws Exception {
		By checkProcedure = map.getLocator("tabCheckProcedure");
		clickOnButton(checkProcedure);
	}

	public void selectReviewers() throws Exception {
		By checkProcedure = map.getLocator("tabReviewers");
		clickOnButton(checkProcedure);
	}

	public void clickOnSelectReviewers() throws Exception {
		By selectReviewers = map.getLocator("selectReviewers");
		clickOnButton(selectReviewers);
	}

	public boolean selectReviewerItem(String name) throws Exception {
		By searchInput = map.getLocator("fieldSearchReviewrs");
		By btnAddReviewer = map.getLocator("btnAddReviewer");
		WebElement fieldElement = getDriver().findElement(searchInput);
		fieldElement.clear();
		fieldElement.sendKeys(name);
		waitFieldToBeFilled(searchInput, name);
		Thread.sleep(300);
		fieldElement.sendKeys(Keys.ENTER);
		WebElement addReviewer = getDriver().findElement(btnAddReviewer);
		if (addReviewer.isEnabled()) {
			addReviewer.click();
			return true;
		} else {
			return false;
		}
	}
	public boolean selectReviewerFolder(String name) throws Exception {
		By searchInput = map.getLocator("fieldSearchReviewrsFolder");
		By btnAddReviewer = map.getLocator("btnAddReviewer");
		WebElement fieldElement = getDriver().findElement(searchInput);
		fieldElement.clear();
		fieldElement.sendKeys(name);
		waitFieldToBeFilled(searchInput, name);
		Thread.sleep(300);
		fieldElement.sendKeys(Keys.ENTER);
		WebElement addReviewer = getDriver().findElement(btnAddReviewer);
		if (addReviewer.isEnabled()) {
			addReviewer.click();
			return true;
		} else {
			return false;
		}
	}

	public boolean removeReviewer(String name) throws Exception {
		By reviewer = map.getLocatorVar("removeReviewer", name);
		return clickOnButton(reviewer);
	}

	public void clickRightOnItem(String pathUnic) throws Exception {
		waitLoading();
		Thread.sleep(500);
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for (int i = 0; i < path.length && !pathUnic.isEmpty(); i++) {
			pathToElement += getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative", path[i]));
		}
		pathToElement += getPathFromBy(map.getLocator("checkBoxItemLibraryRelative"));

		WebElement item = getDriver().findElement(By.xpath(pathToElement));

		Actions action = new Actions(getDriver()).contextClick(item);
		action.build().perform();
	}

	public void selectOptionRightClick(String option) throws Exception {
		By optionOnList = map.getLocatorVar("listOptionsRightClick", option);
		clickOnButton(optionOnList);
	}

	public void selectOptionOnFolderRightClick(String option) throws Exception {
		By optionOnList = map.getLocatorVar("listOptionRighClickOnFolder", option);
		clickOnButton(optionOnList);
	}

	public void confirmOptionDeleteItemAndFolder() throws Exception {
		By delete = map.getLocator("btnDelete");
		WebElement btnDelete = getDriver().findElement(delete);
		btnDelete.click();
	}

	public void clickOnAbaLink() throws Exception {
		By abaLinks = map.getLocator("abaLinks");
		clickOnButton(abaLinks);
	}
	
	public void clickOnTabReviewers() throws Exception{
		waitLoadingOverlay();
		By tabReviews = map.getLocator("tabReviewers");
		clickOnButton(tabReviews);
	}

	public void addLinkInAbaLink(String link) throws Exception {
		By fieldSelectLink = map.getLocator("fieldSelectLink");
		clickOnButton(fieldSelectLink);
		By input = map.getLocator("inputLink");
		WebElement fieldElement = getDriver().findElement(input);
		fieldElement.clear();
		fieldElement.sendKeys(link);
		waitFieldToBeFilled(input, link);
		Thread.sleep(300);
		fieldElement.sendKeys(Keys.ENTER);
		By btnAddLink = map.getLocator("btnAddLink");
		clickOnButton(btnAddLink);
	}

	public boolean removeLinks(String name) throws Exception {
		By link = map.getLocatorVar("removeLinks", name);
		return clickOnButton(link);
	}

	public void renameItem(String newName) throws Exception {
		Actions action = new Actions(getDriver());
		action.sendKeys(newName, Keys.ENTER).build().perform();
	}

	public boolean renameItemForAnExistingName(String newName) throws Exception {
		Actions action = new Actions(getDriver());
		action.sendKeys(newName, Keys.ENTER).build().perform();
		By modal = map.getLocator("modalSameName");
		waitElementToBeVisible(modal);
		By btnClose = map.getLocator("btnCloseInModal");
		return clickOnButton(btnClose);
	}

	public boolean copyPath(String expectedPath) throws Exception {
		Thread.sleep(500);
		Alert alert = getDriver().switchTo().alert();
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_X);
		r.keyRelease(KeyEvent.VK_X);
		r.keyRelease(KeyEvent.VK_CONTROL);
		String path = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
		alert.accept();
		if (path.equals(expectedPath))
			return true;
		else
			return false;

	}

	public boolean itemIsHighlightedInTree(String pathToItem) throws Exception {
		waitLoading();
		String[] path = pathToItem.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for (int i = 0; i < path.length && !pathToItem.isEmpty(); i++) {
			pathToElement += getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative", path[i]));
		}
		pathToElement += getPathFromBy(map.getLocator("nameItemLibraryRelative"));
		return getDriver().findElement(By.xpath(pathToElement)).getAttribute("class")
				.equals("jstree-anchor jstree-search");
	}

	/**
	 * The type param should be the same of label showed The area param is
	 * optional, for the case of Tag Values (eg. "Grid Row x Column") Obs: The
	 * filter "Default hidden on Mother" must be set with name "Default Hide on
	 * Mother"
	 */
	public void setInFilterValue(String type, String entry, String... area) throws Exception {
		// 3 tipos: simple drodown, field, search dropdown
		String pathFilterBase = getPathFromBy(map.getLocatorVar("filterContentBase", type));
		String classField = getDriver().findElement(By.xpath(pathFilterBase)).getAttribute("class");
		if (classField.equals(ConstantsItemLibrary.TYPE_FIELD_SELECT)) {
			String pathToOptionRelative = getPathFromBy(map.getLocatorVar("fieldRelative", entry));
			clickOnButton(By.xpath(pathFilterBase + "/button"));
			clickOnButton(By.xpath(pathFilterBase + pathToOptionRelative));
		} else if (classField.equals(ConstantsItemLibrary.TYPE_FIELD_TEXT_NORMAL)) {
			setTextField(By.xpath(pathFilterBase), entry);
		} else if (classField.equals(ConstantsItemLibrary.TYPE_FIELD_SELECT_MULTI)) {
			clickOnButton(By.xpath(pathFilterBase + "/button"));
			setTextFieldDropdown(By.xpath(pathFilterBase + "/div/div/input"), entry);
			clickOnButton(By.xpath(pathFilterBase + "/button"));
		} else if (classField.equals(ConstantsItemLibrary.TYPE_FIELD_SELECT_MULTI_GROUPS)) {
			clickOnButton(By.xpath(pathFilterBase + "/button"));
			setTextFieldDropdownWithArea(By.xpath(pathFilterBase + "/div/div/input"), area[0], entry);
			clickOnButton(By.xpath(pathFilterBase + "/button"));
		}
	}

	public void selectSmartCAType(String option) throws Exception {
		clickOnButton(map.getLocator("fieldSmartCAType"));
		clickOnButton(map.getLocatorVar("itemSmartCAType", option));
	}

	/**
	 * The type param should be the same of option in "Select some filters"
	 */
	public void selectSomeFilter(String type) throws Exception {
		waitLoading();
		if (!type.isEmpty()) {
			clickOnButton(map.getLocator("optionSelectSomeFilters"));
			WebElement criteriaList = getDriver().findElement(map.getLocator("listFilter"));
			List<WebElement> criteriaListElements = criteriaList.findElements(By.xpath("li"));
			for (WebElement element : criteriaListElements) {
				if (element.getText().equals(type)) {
					if (!element.getAttribute("class").equals("selected")) {
						element.click();
						clickOnButton(map.getLocator("optionSelectSomeFilters"));
					}
					return;
				}
			}
		}
	}

	public void selectOptionCOTA() throws Exception {
		By optionCOTA = map.getLocator("optionCOTA");
		clickOnButton(optionCOTA);
	}

	public void fillConfiguration(String option, String nameTagType, String valueTagValue, String region,
			String valueCheckProcedure, String nameReviewer) throws Exception {
		selectOptionFolder(option);
		selectOptionCOTA();
		fillTagType(nameTagType);
		fillTagValue(valueTagValue);
		fillRegion(region);
		selectCheckProcedure();
		fillCheckProcedure(valueCheckProcedure);
		clickOnTabReviewers();
		clickOnSelectReviewers();
		selectReviewerFolder(nameReviewer);
	}
	
	public boolean confirmDelete() throws Exception{
		waitLoading();
		By btnConfirm = map.getLocatorVar("btnYesPopup"," Delete Item?");
		return clickOnButton(btnConfirm);
	}
	

	public void preconditionItem(String pathItem, String property, boolean checked) throws Exception {
		pathItem=pathItem.split(" #")[0];

		openPage();
		selectItemInTree(pathItem);
		if(checked){
			setCheckedProperty(property);
		}else{
			setUncheckedProperty(property);
		}
		clickOnButtonSave();
	}

	private void setCheckedProperty(String property) throws Exception {
		if(!getDriver().findElement(map.getLocatorVar("chkPropertiesOptions", property)).isSelected()){
			selectOption(property);
		}
	}
	
	private void setUncheckedProperty(String property) throws Exception {
		if(getDriver().findElement(map.getLocatorVar("chkPropertiesOptions", property)).isSelected()){
			selectOption(property);
		}
	}

	public String getValueProperty(String path, String property) throws Exception {
		selectItemInTree(path);
		if(property.equals(ConstantsItemLibrary.FILTER_NAME)){
			return getDriver().findElement(map.getLocator("propertiesName")).getAttribute("value");
		}else if(property.equals(ConstantsItemLibrary.FILTER_TYPE)){
			for(WebElement elem : getDriver().findElements(map.getLocator("propertiesType"))){
				boolean isSelected = elem.isSelected();
				if(isSelected){
					return elem.findElement(By.xpath("parent::label")).getText();
				}
			}
			return "false";
		}else if(property.equals(ConstantsItemLibrary.FILTER_SMARTCA_TYPES)){
			return getDriver().findElement(map.getLocator("propertiesSmartCAType")).getText();
		}else if(property.equals(ConstantsItemLibrary.FILTER_TAG_TYPES)){
			String types="";
			for(WebElement elem : getDriver().findElements(map.getLocator("tagTypes"))){
				types+=elem.findElement(By.xpath("span")).getText()+",";
			}
			return types;
		}else if(property.equals(ConstantsItemLibrary.FILTER_TAG_VALUES)){
			String values="";
			for(WebElement elem : getDriver().findElements(map.getLocator("tagValues"))){
				values+=elem.findElement(By.xpath("span")).getText()+",";
			}
			return values;
		}else if(property.equals(ConstantsItemLibrary.FILTER_WARNINGS)){
			for(WebElement elem : getDriver().findElements(map.getLocator("propertiesWarnings"))){
				boolean isSelected = elem.isSelected();
				if(isSelected){
					return elem.findElement(By.xpath("parent::label")).getText();
				}
			}
			return "false";
		}else if(property.equals(ConstantsItemLibrary.FILTER_HAS_LINK)){
			try{
				getDriver().findElement(map.getLocator("hasNoLink"));
				return "false";
			}catch (Exception e) {
				return "true";
			}
		}else{
			WebElement prop = getDriver().findElement(map.getLocatorVar("chkPropertiesOptions", property));
			String checked = prop.getAttribute("checked");
			if(checked!=null){
				return checked;
			}
			String disable = prop.getAttribute("disabled");
			if(disable!=null&&disable.equals("true")){
				selectItemInTree(path.substring(0, path.lastIndexOf("::")));
				prop = getDriver().findElement(map.getLocatorVar("chkPropertiesOptionsGroup", property));
				checked = prop.getAttribute("checked");
				if(checked!=null){
					return checked;
				}
			}
			return getDriver().findElement(map.getLocatorVar("chkPropertiesOptions", property)).isSelected()+"";
		}
	}
}
