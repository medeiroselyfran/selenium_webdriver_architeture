package com.lge.gpri.screens;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.helpers.constants.ConstantsModels;
import com.lge.gpri.helpers.constants.ConstantsPackages;
import com.lge.gpri.screens.models.ScreenPerformListModel;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;

//This class must be instantiated by all class pages.

/**
 * Test Scenery Abstract class.
 * 
 * <p>
 * Description: This class must holds the instance of the driver used in the
 * page and.
 * 
 * @author AUTHOR NAME (can exist more than one)
 */

public abstract class AbstractScreen {
	private WebDriver driver;
	public static ObjectMap globalMap = new ObjectMap(ConstantsGeneric.PATH_FILE_GLOBAL);
	protected ObjectMap map;

	public AbstractScreen(WebDriver driver) {
		this.driver = driver;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public ObjectMap getMap() {
		return map;
	}

	public void newTab(String link) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("$(window.open('" + link + "'))");
	}

	public WebElement getElement(By locator) {
		WebDriverWait waiting = new WebDriverWait(driver, 10, 100);
		return waiting.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}

	public void waitElementToBeVisible(By element) {
		WebDriverWait wait = new WebDriverWait(driver, ConstantsGeneric.TIME_LONG);
		wait.until(ExpectedConditions.visibilityOfElementLocated(element));
	}
	
	public void waitElementToBeClickable(By element) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, ConstantsGeneric.TIME);
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} catch (Exception e) {
		}
	}

	public Boolean waitLoading() throws Exception {
		Boolean until = new WebDriverWait(driver, 100).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				return (Boolean) js.executeScript("return jQuery.active == 0");
			}
		});
		waitLoadingOverlay();
		return until;
	}

	public void moveToPageBottom() {
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public void moveToPageHeader() throws InterruptedException {
		Thread.sleep(2000);
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollTo(0,0)");
	}

	public void moveTo(By element) {
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		WebElement webElement = driver.findElement(element);
		jsx.executeScript("arguments[0].scrollIntoView(true);", webElement);
	}

	public void moveTo(WebElement element) {
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public void moveToRight() {
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollTo(2000,0)");
	}

	public void savePackage(By saveType) throws Exception {
		moveToPageBottom();
		clickOnButton(saveType);
		Thread.sleep(200);
	}

	public boolean clickOnButton(By btn) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		boolean result = false;
		if (existsElement(btn)) {
			if (!isAttribtuePresent(btn, "disabled")) {
				waitElementToBeClickable(btn);
			}
			// Thread.sleep(500);
			WebElement button = driver.findElement(btn);
			if (button != null) {
				try {
					button.click();
					result = true;
				} catch (Exception e) {
					moveTo(btn);
					try {
						button.click();
						result = true;
					} catch (Exception e2) {
						result = false;
					}
				}
			}
		}
		Thread.sleep(300);
		return result;
	}

	public void clickOnButtonByJavaScript(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) getDriver();
		executor.executeScript("arguments[0].click()", element);
	}

	public boolean clickOnButtonWithoutWaiting(By btn) {
		boolean result = false;
		if (existsElement(btn)) {
			WebElement button = driver.findElement(btn);
			button.click();
			result = true;
		}
		return result;
	}

	public boolean rightClickOnButton(By btn) throws Exception {
		waitLoading();
		boolean result = false;
		if (existsElement(btn)) {
			try {
				waitElementToBeClickable(btn);
				Thread.sleep(500);
				WebElement button = driver.findElement(btn);
				if (button != null) {
					Actions action = new Actions(driver);
					action.contextClick(button).perform();
					result = true;
				}
			} catch (Exception e) {
			}
		}
		return result;
	}

	public void switchTabs(int tabNumber) {
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(tabNumber));
	}

	public void elementFocus() throws Exception {
		WebElement focus = driver.findElement(By.xpath("/html/body"));
		focus.click();
	}

	/**
	 * Method to wait a field to be filled
	 */
	public void waitFieldToBeFilled(By locator, String entry) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, ConstantsGeneric.TIME);
		wait.until(ExpectedConditions.attributeContains(locator, "value", entry));
	}

	/**
	 * Method to wait the list item, that match with entry, in dropdown to be
	 * activated The first its verify if exist an element in dropdown with contains
	 * the text entry If exist more than one option its verify exactly if the text
	 * entry match If its not the case then
	 */
	public void waitDropdownItemToBeActive(By locator, String entry) throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, ConstantsGeneric.TIME);
			String pathOfField = getPathFromBy(locator);
			String pathToListItemOfField = getPathFromBy(
					globalMap.getLocatorVar("listItemDropdownRelativeContains", entry));
			By locatorListItem = By.xpath(pathOfField + pathToListItemOfField);
			wait.until(ExpectedConditions.attributeContains(locatorListItem, "class", "active"));
		} catch (Exception e) {
		}
	}

	public String getPathFromBy(By locator) {
		return locator.toString().replace("By.xpath: ", "");
	}

	public void setTextField(By field, String entry) throws Exception {
		waitLoading();
		WebElement fieldElement = driver.findElement(field);
		if (!entry.isEmpty()) {
			fieldElement.clear();
			fieldElement.sendKeys(entry);
			waitFieldToBeFilled(field, entry);
			fieldElement.sendKeys(Keys.ENTER);
		} else {
			fieldElement.clear();
		}
	}

	public void setTextFieldView(By field, String entry) throws Exception {
		waitLoading();
		WebElement fieldElement = driver.findElement(field);
		if (!entry.isEmpty()) {
			fieldElement.clear();
			fieldElement.sendKeys(entry);
			waitFieldToBeFilled(field, entry);
		} else {
			fieldElement.clear();
		}
	}

	public void setTextDate(By field, String entry) throws Exception {
		waitLoading();
		WebElement fieldElement = driver.findElement(field);
		if (!entry.isEmpty()) {
			fieldElement.clear();
			if (entry.equals(ConstantsPackages.TODAY)) {
				fieldElement.sendKeys(Keys.ENTER);
			} else {
				fieldElement.sendKeys(entry);
				waitFieldToBeFilled(field, entry);
				fieldElement.sendKeys(Keys.TAB);
			}
		} else {
			fieldElement.clear();
		}
	}

	public boolean setTextFieldDropdown(By string, String entry) throws Exception {
		if (!entry.isEmpty()) {
			waitLoading();
			WebElement fieldElement = driver.findElement(string);
			fieldElement.clear();
			fieldElement.sendKeys(entry);
			waitFieldToBeFilled(string, entry);
			waitDropdownItemToBeActive(string, entry);
			return selectItemDropdown(string, entry);
		}
		return false;
	}

	public boolean setTextFieldDropdownWithArea(By field, String area, String entry) throws Exception {
		waitLoading();
		if (!entry.isEmpty()) {
			WebElement fieldElement = driver.findElement(field);
			fieldElement.clear();
			fieldElement.sendKeys(entry);
			waitFieldToBeFilled(field, entry);
			waitDropdownItemToBeActive(field, entry);
			return selectItemDropdownWithArea(field, area, entry);
		}
		return false;
	}

	/**
	 * Method to select a list item in dropdown after you have filled in the field.
	 * Here you are sure that the correct item will be selected, because the list
	 * will be searched for the item
	 */
	public boolean selectItemDropdown(By locator, String entry) throws Exception {
		String pathOfField = getPathFromBy(locator);
		String pathToList = getPathFromBy(globalMap.getLocatorVar("listDropdownRelativeDirect", entry));
		By locatorList = By.xpath(pathOfField + pathToList);
		List<WebElement> options = driver.findElements(locatorList);
		if (options.size() != 0) {
			for (WebElement opt : options) {
				// if(opt.getText().replace(" ", "").equals(entry.replace(" ",
				// ""))){
				if (opt.getText().contains(entry)) {
					options.get(0).click();
					return true;
				}
			}
			return false;
		} else {
			return false;
		}
	}

	@SuppressWarnings("serial")
	public class ItemNotFoundException extends Exception {
		public ItemNotFoundException() {
			super();
		}

		public ItemNotFoundException(String message) {
			super(message);
		}

		public ItemNotFoundException(String message, Throwable cause) {
			super(message, cause);
		}

		public ItemNotFoundException(Throwable cause) {
			super(cause);
		}
	}

	public boolean selectItemDropdownWithArea(By locator, String area, String entry) throws Exception {
		String pathOfField = getPathFromBy(locator);
		String pathToList = getPathFromBy(globalMap.getLocatorVar("listDropdownRelativeDirectArea", area, entry));
		By locatorList = By.xpath(pathOfField + pathToList);
		List<WebElement> options = driver.findElements(locatorList);
		if (options.size() != 0) {
			options.get(0).click();
			return true;
		} else {
			return false;
		}
	}

	public void selectItemDropdownOneByOne(By locator) throws Exception {
		String pathOfField = getPathFromBy(locator);
		String pathToList = getPathFromBy(globalMap.getLocator("listDropdownRelative"));
		By locatorList = By.xpath(pathOfField + pathToList);

		WebElement list = driver.findElement(locatorList);
		List<WebElement> listItemElements = list.findElements(By.xpath("li[not(contains(@class,'hidden'))]"));
		for (WebElement element : listItemElements) {
			if (!element.getAttribute("class").equals("selected")) {
				waitLoading();
				Thread.sleep(500);
				element.click();
			}
		}
	}

	public void deselectItemDropdownOneByOne(By locator) throws Exception {
		String pathOfField = getPathFromBy(locator);
		String pathToList = getPathFromBy(globalMap.getLocator("listDropdownRelative"));
		By locatorList = By.xpath(pathOfField + pathToList);

		WebElement list = driver.findElement(locatorList);
		List<WebElement> listItemElements = list.findElements(By.xpath("li[not(contains(@class,'hidden'))]"));
		for (WebElement element : listItemElements) {
			if (element.getAttribute("class").equals("selected")) {
				waitLoading();
				Thread.sleep(500);
				element.click();
			}
		}
	}

	public boolean existsElement(By element) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
		boolean exists = driver.findElements(element).size() != 0;
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		return exists;
	}

	/**
	 * Generic method to get text of a give element text type
	 */
	public String getTextFrom(String element) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		if (existsElement(getMap().getLocator(element))) {
			return getDriver().findElement(getMap().getLocator(element)).getText();
		}
		return null;
	}

	/**
	 * Generic method to get text of a give element text type
	 */
	public String getAttributeTextFrom(String element) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		if (existsElement(getMap().getLocator(element))) {
			return getDriver().findElement(getMap().getLocator(element)).getAttribute("value");
		}
		return null;
	}

	public int getPositionNode(By locator) throws Exception {
		waitLoading();
		String xpath = locator.toString().replace("By.xpath: ", "");
		String count = xpath + "/preceding-sibling::*";
		By locatorPreceding = By.xpath(count);
		if (existsElement(locatorPreceding)) {
			return getDriver().findElements(locatorPreceding).size() + 1;
		} else {
			return 1;
		}
	}

	/**
	 * wait the loading overlay to gone
	 */
	public void waitLoadingOverlay() throws Exception {
		while (existsElement(globalMap.getLocator("loadingoverlay"))) {
			Thread.sleep(100);
		}
	}

	/**
	 * F5
	 */
	public void refreshPage() {
		driver.navigate().refresh();
	}

	public boolean isAttribtuePresent(By element, String attribute) {
		Boolean result = false;
		try {
			String value = getDriver().findElement(element).getAttribute(attribute);
			if (value != null) {
				result = true;
			}
		} catch (Exception e) {
		}

		return result;
	}

	public void openPageOnAnotherTab(String url) throws Exception {
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.open();");

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.get(url);

	}

	public boolean boxIsChecked(String elementName) throws Exception {
		boolean result = false;
		String elementClass = driver.findElement(map.getLocator(elementName)).getAttribute("class");
		if (elementClass.equals("jstree-anchor jstree-checked")) {
			result = true;
		}
		return result;
	}

	public boolean verifyIfValuesAreEquals(String fieldName, String value) throws Exception {
		boolean result = false;
		if (!fieldName.trim().isEmpty() && !value.trim().isEmpty()) {
			String fieldValue = driver.findElement(map.getLocator(fieldName)).getAttribute("value");
			if (fieldValue.equals(value)) {
				result = true;
			}
		}
		return result;
	}
	
	public boolean verifyIfValuesIsEmpty(String fieldName, String value) throws Exception {
		boolean result = false;
		if (!fieldName.trim().isEmpty() && value.trim().isEmpty()) {
			String fieldValue = driver.findElement(map.getLocator(fieldName)).getAttribute("value");
			if (fieldValue.equals(value)) {
				result = true;
			}
		}
		return result;
	}

	public boolean selectRandomtInDropdown(By field, By list, String entry) throws Exception {
		waitLoading();
		if (!entry.isEmpty()) {
			WebElement fieldElement = getDriver().findElement(field);
			fieldElement.clear();
			fieldElement.sendKeys(entry);
			waitFieldToBeFilled(field, entry);
			return selectRadnomItemDropdown(field, list, entry);
		}
		return false;
	}

	public boolean selectRadnomItemDropdown(By field, By listLocator, String entry) throws Exception {
		By locatorList = listLocator;
		WebElement list = getDriver().findElement(locatorList);
		List<WebElement> listItemElements = list.findElements(By.xpath("li[not(contains(@class,'hidden'))]"));
		Random rand = new Random();

		int position = rand.nextInt(3);
		if (position > listItemElements.size()) {
			position = rand.nextInt(listItemElements.size());
		}
		try {
			listItemElements.get(position).click();
			waitLoadingOverlay();
			getDriver().findElement(field).sendKeys(Keys.ENTER);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String getErrorMsg(String nameError) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		if (existsElement(globalMap.getLocatorVar("errorMsg", nameError))) {
			return getDriver().findElement(globalMap.getLocatorVar("errorMsg", nameError)).getText();
		}
		return "";
	}

	public boolean closeErrorMsg(String nameError) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		return clickOnButton(globalMap.getLocatorVar("closeError", nameError));
	}

	public String getElementXPath(WebElement element) {
		return (String) ((JavascriptExecutor) driver).executeScript(
				"gPt=function(c){if(c.id!==''){return'id(\"'+c.id+'\")'}if(c===document.body){return c.tagName}var a=0;var e=c.parentNode.childNodes;for(var b=0;b<e.length;b++){var d=e[b];if(d===c){return gPt(c.parentNode)+'/'+c.tagName+'['+(a+1)+']'}if(d.nodeType===1&&d.tagName===c.tagName){a++}}};return gPt(arguments[0]).toLowerCase();",
				element);
	}

	public boolean showErrorPleaseTryAgain() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		if (existsElement(globalMap.getLocatorVar("errorMsg", " Error"))) {
			return true;
		}
		return false;
	}

	public void preRestoreModel16GB2() throws Exception {
		ScreenPerformListModel screenPerformListModel = new ScreenPerformListModel(getDriver());
			
		screenPerformListModel.openPage();
		screenPerformListModel.addNameFilter();
		screenPerformListModel.setNameFilter(ConstantsModels.MODEL_16GB2);
		boolean resultOk = screenPerformListModel.clickOnEditModel(ConstantsModels.MODEL_16GB2);
		if (!resultOk) {
			screenPerformListModel.clickOnShowDeletedModelsButton();
			screenPerformListModel.clickOnRestoreModel(ConstantsModels.MODEL_16GB2);
			screenPerformListModel.confirmRestoreModel();
			screenPerformListModel.clickOnShowDeletedModelsButton();
		}
	}

	public void reopenPackage(String nameStatus) throws Exception {
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		if (!nameStatus.equals("Under")) {
			screenPerformListPackages.selectReopenPackage(ConstantsPackages.TARGET_DATE_26_11_18_OTHER_FORMAT);
		}
	}
}
