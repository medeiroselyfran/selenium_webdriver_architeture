package com.lge.gpri.screens.models;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.helpers.constants.ConstantsModels;
import com.lge.gpri.screens.AbstractScreen;

/**
 * Perform create and edit models page class.
 * 
 * <p>
 * Description: Methods to create and edit a Model.
 * 
 * @author Gabriel Costa do Nascimento
 */

public class ScreenPerformModel extends AbstractScreen {

	public ScreenPerformModel(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_MODEL);
	}

	/*public void fillFieldsModel(String modelName, String representativeName, String targetDevice, String product,
			String carrierAggregation, String chipset, String gridRowColumn, String hotseatIconRange, String iconSize,
			String radio, String ramMemory, String screenSize, String secondStringIconRange, String simCardSlot,
			String technology, String uiVersion, String wallpaperSize, List<String> swpls, List<String> swmms)
			throws Exception {

		setModelName(modelName);
		setRepresentativeName(representativeName);
		setTargetDevice(targetDevice);
		setProduct(product);

		setCarrierAggregation(carrierAggregation);
		setChipset(chipset);
		setGridRowAndColumn(gridRowColumn);
		setHotseatIconRange(hotseatIconRange);
		setIconSize(iconSize);
		setRadio(radio);
		setRAMMemory(ramMemory);
		setScreenSize(screenSize);
		setSecondStringIconRange(secondStringIconRange);
		setSIMCardSlot(simCardSlot);
		setTechnology(technology);
		setUIVersion(uiVersion);
		setWallpaperSize(wallpaperSize);

		for (String swpl : swpls) {
			addSWPL(swpl);
		}
		for (String swmm : swmms) {
			addSWMM(swmm);
		}

	}*/

	public void fillFieldsModel(String modelName, String representativeName, String targetDevice, String product,
			List<String> swpls, List<String> swmms) throws Exception {

		setModelName(modelName);
		setRepresentativeName(representativeName);
		setTargetDevice(targetDevice);
		setProduct(product);
		setDinamicTagValues();

		for (String swpl : swpls) {
			addSWPL(swpl);
		}
		for (String swmm : swmms) {
			addSWMM(swmm);
		}

	}

	public void setModelName(String modelName) throws Exception {
		if (!modelName.isEmpty()) {
			waitLoading();
			if (modelName.equals(ConstantsModels.FIRST)) {
				clickOnButton(map.getLocator("fieldModelName"));
				clickOnButton(map.getLocator("itemModelFirst"));
			} else {
				setTextField(map.getLocator("fieldModelName"), modelName);
			}
		}
	}

	public String getModelName() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("fieldModelName")).getAttribute("value").split("\n")[0];
	}

	public String getModelFirst() throws Exception {
		return getDriver().findElement(map.getLocator("fieldModelName")).getAttribute("value");
	}

	public void setRepresentativeName(String representativeName) throws Exception {
		if (!representativeName.isEmpty()) {
			waitLoading();
			setTextField(map.getLocator("fieldRepresentativeName"), representativeName);
		}
	}
	
	public String getRepresentativeName() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("fieldRepresentativeName")).getAttribute("value").split("\n")[0];
	}

	public void setTargetDevice(String targetDevice) throws Exception {
		if (!targetDevice.isEmpty()) {
			waitLoading();
			setTextField(map.getLocator("fieldTargetDevice"), targetDevice);
		}
	}

	public String getTargetDevice() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("fieldTargetDevice")).getAttribute("value").split("\n")[0];
	}

	public void setProduct(String product) throws Exception {
		if (!product.isEmpty()) {
			waitLoading();
			setTextField(map.getLocator("fieldProduct"), product);
		}
	}

	public String getProduct() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("fieldProduct")).getAttribute("value").split("\n")[0];
	}

	public void setAppsDrawer(String carrierAggregation) throws Exception {
		if (!carrierAggregation.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelCarrierAggregation"));
			setTextFieldDropdown(map.getLocator("fieldModelCarrierAggregation"), carrierAggregation);
		}
	}

	public String getAppsDrawer() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelCarrierAggregation")).getText().split("\n")[0];
	}

	public void setCarrierAggregation(String carrierAggregation) throws Exception {
		if (!carrierAggregation.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelCarrierAggregation"));
			setTextFieldDropdown(map.getLocator("fieldModelCarrierAggregation"), carrierAggregation);
		}
	}

	public String getCarrierAggregation() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelCarrierAggregation")).getText().split("\n")[0];
	}

	public void setChipset(String chipset) throws Exception {
		if (!chipset.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelChipset"));
			setTextFieldDropdown(map.getLocator("fieldModelChipset"), chipset);
		}
	}

	public String getChipset() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelChipset")).getText().split("\n")[0];
	}

	public void setGridRowAndColumn(String rowColumn) throws Exception {
		if (!rowColumn.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelGridRowColumn"));
			setTextFieldDropdown(map.getLocator("fieldModelGridRowColumn"), rowColumn);
		}
	}

	public String getGridRowAndColumn() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelGridRowColumn")).getText().split("\n")[0];
	}

	public void setHotseatIconRange(String hotseatIconRange) throws Exception {
		if (!hotseatIconRange.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelHotseatIconRange"));
			setTextFieldDropdown(map.getLocator("fieldModelHotseatIconRange"), hotseatIconRange);
		}
	}

	public String getHotseatIconRange() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelHotseatIconRange")).getText().split("\n")[0];
	}

	public void setIconSize(String iconSize) throws Exception {
		if (!iconSize.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelIconSize"));
			setTextFieldDropdown(map.getLocator("fieldModelIconSize"), iconSize);
		}
	}
	
	public String getIconSize() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelIconSize")).getText().split("\n")[0];
	}

	public void setRadio(String radio) throws Exception {
		if (!radio.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelRadio"));
			setTextFieldDropdown(map.getLocator("fieldModelRadio"), radio);
		}
	}

	public String getRadio() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelRadio")).getText().split("\n")[0];
	}

	public void setRAMMemory(String ramMemory) throws Exception {
		if (!ramMemory.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelRamMemory"));
			setTextFieldDropdown(map.getLocator("fieldModelRamMemory"), ramMemory);
		}
	}

	public String getRAMMemory() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelRamMemory")).getText().split("\n")[0];
	}

	public void setScreenSize(String screenSize) throws Exception {
		if (!screenSize.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelScreenSize"));
			setTextFieldDropdown(map.getLocator("fieldModelScreenSize"), screenSize);
		}
	}

	public String getScreenSize() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelScreenSize")).getText().split("\n")[0];
	}

	public void setSecondStringIconRange(String secondStringIconRange) throws Exception {
		if (!secondStringIconRange.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelSecondStringIconRange"));
			setTextFieldDropdown(map.getLocator("fieldModelSecondStringIconRange"), secondStringIconRange);
		}
	}

	public String getSecondStringIconRange() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelSecondStringIconRange")).getText().split("\n")[0];
	}

	public void setSIMCardSlot(String simCardSlot) throws Exception {
		if (!simCardSlot.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelSimCardSlot"));
			setTextFieldDropdown(map.getLocator("fieldModelSimCardSlot"), simCardSlot);
		}
	}

	public String getSIMCardSlot() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelSimCardSlot")).getText().split("\n")[0];
	}

	public void setTechnology(String technology) throws Exception {
		if (!technology.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelTechnology"));
			setTextFieldDropdown(map.getLocator("fieldModelTechnology"), technology);
		}
	}

	public String getTechnology() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelTechnology")).getText().split("\n")[0];
	}

	public void setUIVersion(String uiVersion) throws Exception {
		if (!uiVersion.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelUIVersion"));
			setTextFieldDropdown(map.getLocator("fieldModelUIVersion"), uiVersion);
		}
	}

	public String getUIVersion() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelUIVersion")).getText().split("\n")[0];
	}

	public void setWallpaperSize(String wallpaperSize) throws Exception {
		if (!wallpaperSize.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelWallpaperSize"));
			setTextFieldDropdown(map.getLocator("fieldModelWallpaperSize"), wallpaperSize);
		}
	}

	public String getWallpaperSize() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionModelWallpaperSize")).getText().split("\n")[0];
	}

	public void addSWPL(String swpl) throws Exception {
		if (!swpl.isEmpty()) {
			waitLoading();
			moveToPageHeader();
			clickOnButton(map.getLocator("optionModelSWPL"));
			setTextFieldDropdown(map.getLocator("fieldModelSWPL"), swpl);
			clickOnButton(map.getLocator("addSWPL"));
		}
	}

	public void removeSWPL(String swpl) throws Exception {
		if (!swpl.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocatorVar("removeSWPL", swpl));
		}
	}

	public void addSWMM(String swmm) throws Exception {
		if (!swmm.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionModelSWMM"));
			setTextFieldDropdown(map.getLocator("fieldModelSWMM"), swmm);
			clickOnButton(map.getLocator("addSWMM"));
		}
	}

	public void removeSWMM(String swmm) throws Exception {
		if (!swmm.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocatorVar("removeSWMM", swmm));
		}
	}

	public ArrayList<String> getSWPLs() throws Exception {
		List<WebElement> swpls = getDriver().findElements(map.getLocator("swpls"));
		ArrayList<String> swplsArray = new ArrayList<>();
		for (WebElement swpl : swpls) {
			swplsArray.add(swpl.getText());
		}
		if (swplsArray.get(0).equals(ConstantsModels.NO_USER_ADDED)) {
			swplsArray = new ArrayList<>();
			swplsArray.add("");
		}
		return swplsArray;
	}

	public ArrayList<String> getSWMMs() throws Exception {
		List<WebElement> swmms = getDriver().findElements(map.getLocator("swmms"));
		ArrayList<String> swmmsArray = new ArrayList<>();
		for (WebElement swmm : swmms) {
			swmmsArray.add(swmm.getText());
		}
		if (swmmsArray.get(0).equals(ConstantsModels.NO_USER_ADDED)) {
			swmmsArray = new ArrayList<>();
			swmmsArray.add("");
		}
		return swmmsArray;
	}

	public void clickOnSaveModel() throws Exception {
		waitLoading();
		WebElement saveButton = getDriver().findElement(map.getLocator("btnCreateModel"));
		moveToPageBottom();
		waitElementToBeClickable(map.getLocator("btnCreateModel"));
		saveButton.click();
	}

	public void clickOnCreateModelOkButton() throws Exception {
		clickOnButton(map.getLocator("buttonOkOnCreatingModel"));
	}

	public void setDinamicTagValues() throws Exception {
		List<WebElement> findElements = getDriver().findElements(map.getLocator("quantityFieldsTagValues"));
		for (int i = 0; i < findElements.size(); i++) {
			By locatorVar = map.getLocatorVar("fillDinamicTags", String.valueOf(i + 1));
			getDriver().findElement(locatorVar).click();
		}
	}

}
