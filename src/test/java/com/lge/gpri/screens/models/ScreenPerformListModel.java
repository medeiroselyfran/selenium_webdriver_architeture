package com.lge.gpri.screens.models;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
 * Perform list of models page class.
 * 
 * <p>
 * Description: Methods to realize the tests on list of models screen.
 * 
 * @author Gabriel Costa do Nascimento
 */

public class ScreenPerformListModel extends AbstractScreen {

	public ScreenPerformListModel(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_MODEL);
	}

	public void openPage() {
		getDriver().get("http://136.166.96.136:8204/Views/Models/");
		getDriver().manage().window().maximize();
	}

	public void clickOnNewModelButton() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("btnNewModel"));
		waitLoading();
	}

	public void clickOnShowDeletedModelsButton() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("showDeletedModels"));
	}

	public void clickOnShowOnlyMyModelsButton() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("showOnlyMyModels"));
	}

	public void showTableElements(String numberOfElements) throws Exception {
		waitLoading();
		if (!numberOfElements.isEmpty()) {
			clickOnButton(map.getLocator("numberOfModelsShow"));
			clickOnButton(map.getLocatorVar("optionNumberOfModelsShow", numberOfElements));
			clickOnButton(map.getLocator("numberOfModelsShow"));
		}
	}

	public void selectAllCriteriaFilter() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionCriteriaFilter"));
		clickOnButton(map.getLocator("selectAllCriteriaFilter"));
		clickOnButton(map.getLocator("optionCriteriaFilter"));
	}

	public void deselectAllCriteriaFilter() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionCriteriaFilter"));
		clickOnButton(map.getLocator("deselectAllCriteriaFilter"));
		clickOnButton(map.getLocator("optionCriteriaFilter"));
	}

	@SuppressWarnings("unused")
	private void addCriteriaFilter(String criteria) throws Exception {
		waitLoading();
		if (!criteria.isEmpty()) {
			clickOnButton(map.getLocator("optionCriteriaFilter"));
			WebElement criteriaList = getDriver().findElement(map.getLocator("listCriteriaFilter"));
			List<WebElement> criteriaListElements = criteriaList.findElements(By.xpath("li"));
			for (WebElement element : criteriaListElements) {
				if (element.getText().equals(criteria)) {
					if (!element.getAttribute("class").equals("selected")) {
						element.click();
					}
					return;
				}
			}
			clickOnButton(map.getLocator("optionCriteriaFilter"));
		}
	}

	@SuppressWarnings("unused")
	private void removeCriteriaFilter(String criteria) throws Exception {
		waitLoading();
		if (!criteria.isEmpty()) {
			clickOnButton(map.getLocatorVar("btnRemoveCriteria", criteria));
		}
	}

	@SuppressWarnings("unused")
	private void setCriteriaFilter(String criteria, String entry) throws Exception {
		waitLoading();
		if (!criteria.isEmpty() && !entry.isEmpty()) {
			setTextField(map.getLocatorVar("fieldCriteriaFilter", criteria), entry);
		}
	}

	@SuppressWarnings("unused")
	private void selectAllSpecificCriteriaFilter(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionCriteriaFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("selectAllSpecificCriteriaFilter", criteria));
		clickOnButton(map.getLocatorVar("optionCriteriaFilterDropdown", criteria));
	}

	@SuppressWarnings("unused")
	private void selectAllSpecificCriteriaFilterOneByOne(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionCriteriaFilterDropdown", criteria));
		selectItemDropdownOneByOne(map.getLocatorVar("fieldCriteriaFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("optionCriteriaFilterDropdown", criteria));
	}

	@SuppressWarnings("unused")
	private void deselectAllSpecificCriteriaFilter(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionCriteriaFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("deselectAllSpecificCriteriaFilter", criteria));
		clickOnButton(map.getLocatorVar("optionCriteriaFilterDropdown", criteria));
	}

	@SuppressWarnings("unused")
	private void deselectAllSpecificCriteriaFilterOneByOne(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionCriteriaFilterDropdown", criteria));
		deselectItemDropdownOneByOne(map.getLocatorVar("fieldCriteriaFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("optionCriteriaFilterDropdown", criteria));
	}

	public void selectAllModelFilter() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionModelFilter"));
		clickOnButton(map.getLocator("selectAllModelFilter"));
		clickOnButton(map.getLocator("optionModelFilter"));
	}

	public void deselectAllModelFilter() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionModelFilter"));
		clickOnButton(map.getLocator("deselectAllModelFilter"));
		clickOnButton(map.getLocator("optionModelFilter"));
	}

	public void addNameFilter() throws Exception {
		addModelFilter("Name");
	}

	public void addRepresentativeNameFilter() throws Exception {
		addModelFilter("Representative Name");
	}

	public void addTargetDeviceFilter() throws Exception {
		addModelFilter("Target Device");
	}

	public void addRegionFilter() throws Exception {
		addModelFilter("Region");
	}

	public void addTagsFilter() throws Exception {
		addModelFilter("Tags");
	}

	private void addModelFilter(String criteria) throws Exception {
		waitLoading();
		if (!criteria.isEmpty()) {
			clickOnButton(map.getLocator("optionModelFilter"));
			WebElement criteriaList = getDriver().findElement(map.getLocator("listModelFilter"));
			List<WebElement> criteriaListElements = criteriaList.findElements(By.xpath("li"));
			for (WebElement element : criteriaListElements) {
				if (element.getText().equals(criteria)) {
					if (!element.getAttribute("class").equals("selected")) {
						element.click();
					}
					return;
				}
			}
			clickOnButton(map.getLocator("optionModelFilter"));
		}
	}

	public void removeNameFilter() throws Exception {
		removeModelFilter("Name");
	}

	public void removeRepresentativeNameFilter() throws Exception {
		removeModelFilter("Representative Name");
	}

	public void removeTargetDeviceFilter() throws Exception {
		removeModelFilter("Target Device");
	}

	public void removeRegionFilter() throws Exception {
		removeModelFilter("Region");
	}

	public void removeTagsFilter() throws Exception {
		removeModelFilter("Tags");
	}

	private void removeModelFilter(String criteria) throws Exception {
		waitLoading();
		if (!criteria.isEmpty()) {
			clickOnButton(map.getLocatorVar("btnRemoveModelFilter", criteria));
		}
	}

	/**
	 * Before calling these methods(setFilter) it is necessary to call addFilter
	 * method first
	 */
	public void setNameFilter(String entry) throws Exception {
		setModelFilter("Name", entry);
	}

	public void setRepresentativeNameFilter(String entry) throws Exception {
		setModelFilter("Representative Name", entry);
	}

	public void setTargetDeviceFilter(String entry) throws Exception {
		setModelFilter("Target Device", entry);
	}

	private void setModelFilter(String criteria, String entry) throws Exception {
		waitLoading();
		if (!criteria.isEmpty() && !entry.isEmpty()) {
			setTextField(map.getLocatorVar("fieldModelFilter", criteria), entry);
		}
	}

	public void setRegionFilter(String entry) throws Exception {
		waitLoading();
		String criteria = "Region";
		if (!criteria.isEmpty() && !entry.isEmpty()) {
			clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
			setTextFieldDropdown(map.getLocatorVar("fieldModelFilterDropdown", criteria), entry);
			clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
		}
	}

	public int verifyDisplayRegionInFilter(String entry) throws Exception {
		waitLoading();
		String criteria = "Region";
		if (!criteria.isEmpty() && !entry.isEmpty()) {
			clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
			setTextFieldDropdown(map.getLocatorVar("fieldModelFilterDropdown", criteria), entry);
		}
		return getDriver().findElements(map.getLocator("resultSearchFilterRegionCriteria")).size();
	}
	
	public String verifySearchForInvalidRegion(String entry) throws Exception {
		waitLoading();
		String criteria = "Region";
		if (!criteria.isEmpty() && !entry.isEmpty()) {
			clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
			setTextFieldDropdown(map.getLocatorVar("fieldModelFilterDropdown", criteria), entry);
		}
		return getDriver().findElement(map.getLocator("resultSearchFilterRegionCriteria")).getText();
	}

	public void setTagFilter(String area, String entry) throws Exception {
		waitLoading();
		String criteria = "Tags";
		if (!criteria.isEmpty() && !entry.isEmpty()) {
			clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
			setTextFieldDropdownWithArea(map.getLocatorVar("fieldModelFilterDropdown", criteria), area, entry);
			clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
		}
	}

	public void selectAllRegionFilter() throws Exception {
		selectAllSpecificModelFilter("Region");
	}

	public void selectAllRegionFilterOneByOne() throws Exception {
		selectAllSpecificModelFilterOneByOne("Region");
	}

	public void selectAllTagsFilter() throws Exception {
		selectAllSpecificModelFilter("Tags");
	}

	private void selectAllSpecificModelFilter(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("selectAllSpecificModelFilter", criteria));
		clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
	}

	private void selectAllSpecificModelFilterOneByOne(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
		selectItemDropdownOneByOne(map.getLocatorVar("fieldModelFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
	}

	public void deselectAllRegionFilter() throws Exception {
		deselectAllSpecificModelFilter("Region");
	}

	public void deselectAllRegionFilterOneByOne() throws Exception {
		deselectAllSpecificModelFilterOneByOne("Region");
	}

	public void deselectAllTagsFilter() throws Exception {
		deselectAllSpecificModelFilter("Tags");
	}

	private void deselectAllSpecificModelFilter(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("deselectAllSpecificModelFilter", criteria));
		clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
	}

	private void deselectAllSpecificModelFilterOneByOne(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
		deselectItemDropdownOneByOne(map.getLocatorVar("fieldModelFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("optionModelFilterDropdown", criteria));
	}

	public void clickOnMoreInfo(String ModelName) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("moreInfoTblModels", ModelName));
	}

	public boolean clickOnEditModel(String ModelName) throws Exception {
		waitLoading();
		return clickOnButton(map.getLocatorVar("btnEditModel", ModelName));
	}

	public void clickOnViewPackages(String ModelName) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("btnViewPackages", ModelName));
	}

	public boolean clickOnRemoveModel(String ModelName) throws Exception {
		waitLoading();
		return clickOnButton(map.getLocatorVar("btnRemoveModel", ModelName));
	}

	public void clickOnRestoreModel(String ModelName) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("btnRestoreModel", ModelName));
	}

	public boolean confirmRemoveModel() throws Exception {
		boolean result;
		waitLoading();
		elementFocus();
		result = clickOnButton(map.getLocatorVar("btnYesPopup", "Remove Model?"));
		return result;
	}

	public void confirmRestoreModel() throws Exception {
		waitLoading();
		elementFocus();
		clickOnButton(map.getLocator("btnYesRestoreModel"));
	}

	public void paginateNext() throws Exception {
		waitLoading();
		moveToPageBottom();
		clickOnButton(map.getLocator("btnNextModels"));
	}
	
	public boolean existNextPage() throws Exception {
		waitLoading();
		moveToPageBottom();
		return clickOnButton(map.getLocator("btnNextModels"));
	}

	public void paginatePrevious() throws Exception {
		waitLoading();
		moveToPageBottom();
		clickOnButton(map.getLocator("btnPreviousModels"));
	}

	public void goToLastPage() throws Exception {
		waitLoading();
		moveToPageBottom();
		clickOnButton(map.getLocator("btnLastModels"));
	}

	public void goToFirstPage() throws Exception {
		waitLoading();
		moveToPageBottom();
		clickOnButton(map.getLocator("btnFirstModels"));
	}

	public void goToChoosedPage(String choosedPage) throws Exception {
		waitLoading();
		moveToPageBottom();
		if (!choosedPage.isEmpty()) {
			goToFirstPage();
			String page = getDriver().findElement(map.getLocator("activePageTableModels")).getText();
			while (!page.equals(choosedPage)) {
				paginateNext();
				waitLoading();
				page = getDriver().findElement(map.getLocator("activePageTableModels")).getText();
			}
		}
	}

	/**
	 * 
	 * @param users
	 * @return
	 * @throws Exception
	 */
	public boolean getSWPLsFromMoreInfo(List<String> users) throws Exception {
		boolean isOk = false;
		for (int i = 0; i < users.size(); i++) {
			String currentUser = users.get(i) + "@lge.com";
			By user = map.getLocatorVar("listSWPLs", currentUser);
			try {
				getDriver().findElement(user);
			} catch (Exception e) {
				return isOk;
			}
		}
		return isOk = true;
	}
	
	public boolean getSWMMsFromMoreInfo(List<String> users) throws Exception {
		boolean isOk = false;
		for (int i = 0; i < users.size(); i++) {
			String currentUser = users.get(i) + "@lge.com";
			By user = map.getLocatorVar("listSWMMs", currentUser);
			try {
				getDriver().findElement(user);
			} catch (Exception e) {
				return isOk;
			}
		}
		return isOk = true;
	}

	/**
	 * 
	 * @param users
	 * @return
	 * @throws Exception
	 */
	public boolean getSWMMFromMoreInfo(List<String> users) throws Exception {
		boolean isOk = false;
		for (int i = 0; i < users.size(); i++) {
			String currentUser = users.get(i) + "@lge.com";
			By user = map.getLocatorVar("listSWMMs", currentUser);
			try {
				getDriver().findElement(user);
			} catch (Exception e) {
				return isOk;
			}
		}
		return isOk = true;
	}

	/**
	 * 
	 */
	public String getValueTagFromMoreInfo(String tag) throws Exception {
		String tagValue = "";
		By allTags = map.getLocator("listTags");
		try {
			String tags = getDriver().findElement(allTags).getText();
			String[] value = tags.split(tag)[1].split("\\n");
			tagValue = value[0].replace(": ", "");
		} catch (Exception e) {
			return tagValue;
		}
		return tagValue;
	}

	/**
	 * Method for get quantity of models displayed on table
	 * 
	 * @return Quantity of models
	 * @throws Exception
	 */
	public int getModelsQuantity() throws Exception {
		int quantityModels = getDriver().findElements(map.getLocator("listAllModels")).size();
		return quantityModels;
	}

	/**
	 * @throws Exception
	 * 
	 */
	public WebElement getDesiredLineOfTable(String nameColumn) throws Exception {
		WebElement model = null;
		WebElement desiredcolumn = getColumnTable(nameColumn);
		List<WebElement> allModels = getModelOfTable();
		for (int i = 0; i <= allModels.size(); i++) {
			if (nameColumn.equals(desiredcolumn.getText())) {
				model = allModels.get(i);
				break;
			}
		}
		return model;
	}

	/**
	 * @throws Exception
	 * 
	 */
	public WebElement getColumnTable(String nameColumn) throws Exception {
		WebElement column = null;
		waitLoading();
		List<WebElement> allTitleTable = getDriver().findElements(map.getLocator("tableHeader"));
		for (int i = 0; i < allTitleTable.size(); i++) {
			if (allTitleTable.get(i).getText().equals(nameColumn)) {
				column = allTitleTable.get(i);
			}
		}
		return column;
	}

	/**
	 * @throws Exception
	 * 
	 */
	public List<WebElement> getModelOfTable() throws Exception {
		List<WebElement> allModels = getDriver().findElements(map.getLocator("listAllModels"));
		return allModels;
	}

	/**
	 * @throws Exception
	 * 
	 */
	public String getMessageForNoModelsAvailable() throws Exception {
		waitLoading();
		if(existsElement(map.getLocator("txtNoModelsAvailable"))){
			return getDriver().findElement(map.getLocator("txtNoModelsAvailable")).getText();
		}else{
			return "";
		}
	}

	/**
	 * Method for return quantity of models is Display on the Foot Table
	 * 
	 * @return
	 */
	public String[] getShowQuantityOfModelsOnTheFootTable() {
		String infoModelsShow = getDriver().findElement(By.id("tblModels_info")).getText();
		String[] split = infoModelsShow.split(" - ");
		String[] modelsQuantity = split[1].split("of");
		return modelsQuantity;
	}

	/**
	 * 
	 * @param screenPerformListModel
	 * @return
	 * @throws Exception
	 */
	public boolean verifyTag(String tagName, String tagValue) throws Exception {
		boolean isTrue = false;
		List<WebElement> allModelsOfTable = getModelOfTable();
		for (int i = 0; i < allModelsOfTable.size(); i++) {
			String[] modelName = allModelsOfTable.get(i).getText().split(" ");
			clickOnMoreInfo(modelName[0]);
			Thread.sleep(4000);
			if (tagValue.equals(getValueTagFromMoreInfo(tagName))) {
				isTrue = true;
				getDriver().findElement(map.getLocator("btnCloseMoreInfo")).click();
			} else {
				isTrue = false;
				break;
			}
		}
		return isTrue;
	}
	
	public boolean verifyNoPackageRelatedToPackage(ScreenPerformListModel screenPerformListModel) throws Exception {
		boolean isEmpty = false;
		String empty = "";
		List<WebElement> modelOfTable = screenPerformListModel.getModelOfTable();
		for(int i = 0; i < modelOfTable.size(); i++){
			if(empty.equals(modelOfTable.get(i).findElements(By.tagName("td")).get(6).getText())){
				isEmpty = true;
			}
		}
		return isEmpty;
	}
	
	/**
	 * @throws Exception 
	 * 
	 */
	public String getModelNameBasedOnRepresentativeNameInModelTable(String representativeName) throws Exception {
		return getElement(map.getLocatorVar("modelNameValueBasedOnRepresentativeName", representativeName)).getAttribute("name");
	}
	
	/**
	 * @throws Exception 
	 * 
	 */
	public boolean filterOnlySameTargetDevice(String targetDevice) throws Exception {
		String infoModelsShow = getElement(map.getLocator("tblModels_info")).getText();
		String[] split = infoModelsShow.split(" - ");
		String modelsQuantity = split[1].split("of")[0];
		
		int size = getDriver().findElements(map.getLocatorVar("qtdeModelsBasedOnTargetDevice", targetDevice)).size();
		return String.valueOf(size).equals(modelsQuantity.trim());
	}

}
