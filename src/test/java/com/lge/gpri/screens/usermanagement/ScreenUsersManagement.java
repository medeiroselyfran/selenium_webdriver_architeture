package com.lge.gpri.screens.usermanagement;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.helpers.constants.ConstantsUserManagement;
import com.lge.gpri.screens.AbstractScreen;

/**
 * Perform User Management.
 * 
 * <p>
 * Description: Methods to perform User Management
 * 
 * @author Elyfran
 * @author Gabriel Costa do Nascimento
 */

public class ScreenUsersManagement extends AbstractScreen {
	private ObjectMap map;

	public ScreenUsersManagement(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_USER_MANAGEMENT);
	}

	public void openPage() {
		getDriver().get("http://136.166.96.136:8204/Views/Admin/User/#");
		getDriver().manage().window().maximize();
	}

	public void associateUserToProfile(String user, List<String> profileName) throws Exception {
		fillNewUserField(user);
		Thread.sleep(2000);
		for (int i = 0; i < profileName.size(); i++) {
			setProfileAddNewUser(profileName.get(0));

			String msg = getErrorMsg("Error");
			if (!msg.isEmpty()) {
				closeErrorMsg("Error");
				searchAndSelectUser(user);
				removeRole(profileName.get(i));
				confirmRemoveRole();
				Thread.sleep(2000);
				moveToPageHeader();
				setProfileAddNewUser(profileName.get(0));
			}
		}
	}

	public String associateUserToProfileGetMsg(String user, List<String> profileName) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		getDriver().findElement(map.getLocator("newUserLogin")).clear();
		getDriver().findElement(map.getLocator("newUserLogin")).sendKeys(user);
		waitFieldToBeFilled(map.getLocator("newUserLogin"), user);
		waitElementToBeVisible(map.getLocator("listUsers"));
		for (int i = 0; i < profileName.size(); i++) {
			getDriver().findElement(map.getLocator("newUserRole")).click();
			setTextFieldDropdown(map.getLocator("searchbox"), profileName.get(i));
			getDriver().findElement(map.getLocator("btnAddNewuser")).click();
		}
		String msg = getErrorMsg("Error");
		if (!msg.isEmpty()) {
			return msg;
		}
		return "";
	}

	public void setProfileAddNewUser(String profileName) throws Exception, InterruptedException {
		getDriver().findElement(map.getLocator("newUserRole")).click();
		setTextFieldDropdown(map.getLocator("searchbox"), profileName);
		getDriver().findElement(map.getLocator("btnAddNewuser")).click();
	}

	public void clearUser() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		getDriver().findElement(map.getLocator("newUserLogin")).clear();
	}

	public void fillNewUserField(String user) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		getDriver().findElement(map.getLocator("newUserLogin")).clear();
		getDriver().findElement(map.getLocator("newUserLogin")).sendKeys(user);
	}

	public String verifyAssociateUserWithError(String user, List<String> profileName, String nameError)
			throws Exception {
		waitLoading();
		waitLoadingOverlay();
		getDriver().findElement(map.getLocator("newUserLogin")).clear();
		getDriver().findElement(map.getLocator("newUserLogin")).sendKeys(user);
		Thread.sleep(2000);
		for (int i = 0; i < profileName.size(); i++) {
			setProfileAddNewUser(profileName.get(0));
		}
		String msg = getErrorMsg(nameError);
		if (!msg.isEmpty()) {
			return msg;
		}
		return "";
	}

	public String getErrorMsg(String nameError) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		if (existsElement(map.getLocatorVar("errorMsg", nameError))) {
			return getDriver().findElement(map.getLocatorVar("errorMsg", nameError)).getText();
		}
		return "";
	}

	public boolean closeErrorMsg(String nameError) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		return clickOnButton(map.getLocatorVar("closeError", nameError));
	}

	public void clickOnGPRICarrer() throws Exception {
		clickOnButton(map.getLocator("career"));
	}

	public boolean verifyCustomMomentisClosed() throws Exception {
		boolean isTrue = true;
		if (existsElement(map.getLocator("btnCancelCustomMoment"))) {
			WebElement findElement = getDriver().findElement(map.getLocator("btnCancelCustomMoment"));
			if (findElement.isDisplayed()) {
				isTrue = false;
			}
		}
		return isTrue;
	}

	public boolean verifyGPRICareerisClosed() throws Exception {
		boolean isTrue = false;
		WebElement findElement = getDriver().findElement(map.getLocator("btnCloseGPRICarrer"));
		if (!findElement.isDisplayed()) {
			isTrue = true;
		}
		return isTrue;
	}

	public void selectDataCustomMoment(String date) throws Exception {
		String day = date.split("/")[0];
		int month = Integer.parseInt(date.split("/")[1]);
		int year = Integer.parseInt(date.split("/")[2]);
		clickOnDateFieldCustomMoment();
		String actualMonthString = getDriver().findElement(map.getLocator("monthCustomMoment")).getText();
		int actualYear = Integer.parseInt(getDriver().findElement(map.getLocator("yearCustomMoment")).getText());
		String[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		int actualMonth = 0;
		for (int i = 0; i < 12; i++) {
			if (actualMonthString.equals(months[i])) {
				actualMonth = i + 1;
			}
		}
		int jumps = (2000 + year - actualYear) * 12 + month - actualMonth;
		By jumpMonthButton = null;
		if (jumps > 0) {
			jumpMonthButton = map.getLocator("nextMonth");
		} else {
			jumpMonthButton = map.getLocator("previousMonth");
		}
		for (int i = 0; i < Math.abs(jumps); i++) {
			getDriver().findElement(jumpMonthButton).click();
		}
		clickOnButton(map.getLocatorVar("dayCustomMoment", day));
		clickOnButton(map.getLocator("btnDoneDate"));
	}

	public boolean verifyGPRICarrerForAssociateUserWithProfile(String profileName) throws Exception {
		boolean isTrue = false;
		WebElement findElement = getDriver().findElement(map.getLocator("dataTables_scrollBody"));
		List<WebElement> allRows = findElement.findElements(By.tagName("tr"));
		String text = "Add as " + profileName + " by yara.martins";
		for (int i = 1; i <= allRows.size(); i++) {
			if (text.equals(allRows.get(i).findElements(By.tagName("td")).get(1).getText())) {
				isTrue = true;
				getDriver().findElement(map.getLocator("btnVerifyGPRICarrer")).click();
				break;
			}
		}
		return isTrue;
	}

	public boolean verifyGPRICarrerForCustomMoment(String message) throws Exception {
		boolean isTrue = false;
		waitLoading();
		WebElement findElement = getDriver().findElement(map.getLocator("dataTables_scrollBody"));
		WebElement firstRow = findElement.findElements(By.tagName("tr")).get(1);
		if (message.equals(firstRow.findElements(By.tagName("td")).get(1).getText())) {
			isTrue = true;
			getDriver().findElement(map.getLocator("btnVerifyGPRICarrer")).click();
		}
		return isTrue;
	}

	public boolean setRegionAssociatedToUser(String region) throws Exception {
		boolean isTrue = false;
		WebElement findElement = getDriver().findElement(map.getLocator("listInfo"));
		List<WebElement> findElements = findElement.findElements(By.tagName("div"));
		for (int i = 0; i < findElements.size(); i++) {
			if (region.equals(findElements.get(i).getText())) {
				isTrue = true;
				break;
			}

		}
		waitLoadingOverlay();
		getDriver().findElement(map.getLocator("pvgRegions")).click();
		if ("true".equals(getDriver().findElement(map.getLocator("pvgRegions")).getAttribute("aria-expanded"))) {
			getDriver().findElement(map.getLocator("fieldRegion")).sendKeys(region);
			getDriver().findElement(map.getLocator("btnAddRegions")).click();
			isTrue = true;
		}

		return isTrue;
	}

	/**
	 * This method click on user that have one of the profiles given If this not
	 * found, these profile will be added
	 */
	public void getUserAndRoles(String username, List<String> profileName) throws Exception {
		WebElement tableUsers = getDriver().findElement(map.getLocator("allUsers"));
		List<WebElement> allUsers = tableUsers.findElements(By.tagName("a"));
		for (int i = 0; i < allUsers.size(); i++) {
			String[] split = allUsers.get(i).getText().split("\n");
			if (username.equals(split[0].toString().trim())) {
				boolean contains = false;
				for (int j = 0; j < profileName.size(); j++) {
					if (split[1].toString() != null && split[1].toString().contains(profileName.get(j).toString())) {
						contains = true;
						break;
					}
				}
				if (contains) {
					Thread.sleep(500);
					allUsers.get(i).click();
					break;
				} else {
					associateUserToProfile(username, profileName);
					break;
				}
			}
		}
	}

	public boolean verifyIfRegionIsAssociated(String region) throws Exception {
		boolean isTrue = false;
		WebElement regionName = getDriver().findElement(map.getLocatorVar("regionsNameOfNVGPVG", region));
		if (region.equals(regionName.getText())) {
			isTrue = true;
		}
		return isTrue;
	}

	/**
	 * Similar to getUserAndRoles(), but not considering profile names
	 */
	public boolean searchAndSelectUser(String username) throws Exception {
		By fieldInputUserName = map.getLocator("inputUsersAndRoles");
		By user = map.getLocatorVar("userItem", username);
		getDriver().findElement(fieldInputUserName).clear();
		getDriver().findElement(fieldInputUserName).sendKeys(username);
		try {
			getDriver().findElement(user).click();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean removeRegionNVGPVG(String role) throws Exception {
		return clickOnButton(map.getLocatorVar("removeRegionNVGPVG", role));
	}

	public boolean confirmRemoveRegion() throws Exception {
		elementFocus();
		return clickOnButton(map.getLocator("btnConfirmRemove"));
	}

	public boolean confirmRemoveItem() throws Exception {
		elementFocus();
		return clickOnButton(map.getLocator("btnConfirmRemove"));
	}

	public boolean cancelRemoveRegion() throws Exception {
		elementFocus();
		return clickOnButton(map.getLocator("btnCancelRemove"));
	}

	public void clickOnAddCustomMoment() throws Exception {
		clickOnButton(map.getLocator("btnAddCustomMoment"));
	}

	public void fillCustomMomentCarrer(String date, String description) throws Exception {
		selectDataCustomMoment(date);
		setTextField(map.getLocator("fieldDescriptionCustomMoment"), description);
	}
	
	public void clickOnDateFieldCustomMoment() throws Exception {
		clickOnButton(map.getLocator("fieldDateCustomMoment"));
	}

	public void clickOnSaveCustomMoment() throws Exception {
		clickOnButton(map.getLocator("btnSaveCustomMoment"));
	}

	public void clickOnCancelCustomMoment() throws Exception {
		clickOnButton(map.getLocator("btnCancelCustomMoment"));
	}

	public void clickOnCloseCustomMoment() throws Exception {
		clickOnButton(map.getLocator("btnCloseCustomMoment"));
	}

	public boolean clickOnRemoveCustomMoment(String description) throws Exception {
		return clickOnButton(map.getLocatorVar("btnRemoveCustomMoment", description));
	}

	public void clickOnConfirmRemoveCustomMoment() throws Exception {
		clickOnButton(map.getLocator("btnConfirmCustomMoment"));
	}

	public void clickOnCancelRemoveCustomMoment() throws Exception {
		clickOnButton(map.getLocator("btnCancelCustomMoment"));
	}

	public void searchGPRICarrer(String entry) throws Exception {
		setTextField(map.getLocator("fieldSearchCustomMoment"), entry);
	}

	public void closeGPRICarrer() throws Exception {
		clickOnButton(map.getLocator("XcloseGPRICarrer"));
	}

	public void clickOnCloseGPRICarrerButton() throws Exception {
		clickOnButton(map.getLocator("btnCloseGPRICarrer"));
	}

	public void clickOnBlockedUsers() throws Exception {
		clickOnButton(map.getLocator("btnBlockedUsers"));
	}

	/**
	 * This method just fill the field in search for some name or login and
	 * return if the user name appears and is available to selected
	 */
	public boolean selectUserToBlock(String user) throws Exception {
		boolean result = true;
		elementFocus();
		setTextField(map.getLocator("fieldBlockUser"), user);
		int count = 20;
		while (getDriver().findElement(map.getLocator("listUserField")).getAttribute("style").contains("display: none")
				&& count > 0) {
			Thread.sleep(100);
			count--;
		}
		if (count == 0) {
			result = false;
		}
		return result;
	}

	public void clearUserFieldToBlock() throws Exception {
		setTextField(map.getLocator("fieldBlockUser"), "");
	}

	/**
	 * This method click on Block Button and returns false if an error occurs
	 */
	public boolean clickOnBlock() throws Exception {
		boolean result = true;
		clickOnButton(map.getLocator("btnBlock"));
		Thread.sleep(1000);
		if (existsElement(map.getLocator("errorBlockingUser"))) {
			result = false;
		}
		return result;
	}

	/**
	 * 
	 */
	public void verifyIfUserBlock(String user) throws Exception {
		clickOnButton(map.getLocator("btnBlock"));
		Thread.sleep(1000);
		if (existsElement(map.getLocator("errorBlockingUser"))) {
			closeErrorMsg("Error");
			checkUserOnListBlock(user);
			clickOnUnblockSelectedUsers();
			clickOnBlock();
		}
	}

	public int searchUserBlocked(String user) throws Exception {
		setTextField(map.getLocator("fieldBlockedUsers"), user);
		return getDriver().findElements(map.getLocator("showUserBlockedInList")).size();
	}

	public void clickOnCloseBlockedUsers() throws Exception {
		clickOnButton(map.getLocator("btnCloseBlockedUsers"));
	}

	public void checkUserOnListBlock(String user) throws Exception {
		clickOnButton(map.getLocatorVar("chkUserToUnblock", user));
	}

	public void clickOnUnblockSelectedUsers() throws Exception {
		clickOnButton(map.getLocator("btnUnblockSelectedUsers"));
	}

	public boolean clickOnXCloseBlockedUsers() throws Exception {
		return clickOnButton(map.getLocator("btnXCloseBlockedUsers"));
	}

	public void clicOnLoginAs() throws Exception {
		clickOnButton(map.getLocator("btnLoginAs"));
	}

	public boolean addRegionOnNVGPVG(String region) throws Exception {
		boolean resultOk;
		clickOnButton(map.getLocator("optionsNVGPVGRegions"));
		resultOk = setTextFieldDropdown(map.getLocator("fieldInputRegionNVGPVG"), region);
		clickOnButton(map.getLocator("btnAddRegionsNVGPVG"));
		return resultOk;
	}

	public boolean addRegionOnItemReviewer(String region) throws Exception {
		boolean resultOk;
		clickOnButton(map.getLocator("optionRegionsItemReviewer"));
		resultOk = setTextFieldDropdown(map.getLocator("fieldRegionsItemReviewer"), region);
		clickOnButton(map.getLocator("btnAddRegionsItemReviewer"));
		return resultOk;
	}

	public boolean addItemOnItemReviewer(String item) throws Exception {
		boolean resultOk;
		clickOnButton(map.getLocator("optionItemsItemReviewer"));
		resultOk = setTextFieldDropdown(map.getLocator("fieldItemsItemReviewer"), item);
		clickOnButton(map.getLocator("btnAddItemsItemReviewer"));
		return resultOk;
	}

	public boolean removeRegionOnItemReviewer(String region) throws Exception {
		boolean resultOk;
		resultOk = clickOnButton(map.getLocatorVar("removeRegionsItemReviewer", region));
		return resultOk;
	}

	public boolean removeItemOnItemReviewer(String item) throws Exception {
		boolean resultOk;
		resultOk = clickOnButton(map.getLocatorVar("removeItemsItemReviewer", "[Item] " + item));
		return resultOk;
	}

	public boolean addRegionOnRM(String region) throws Exception {
		boolean resultOk;
		clickOnButton(map.getLocator("optionRegionsRM"));
		resultOk = setTextFieldDropdown(map.getLocator("fieldRegionsRM"), region);
		clickOnButton(map.getLocator("btnAddRegionsRM"));
		return resultOk;
	}

	public boolean removeRegionOnRM(String region) throws Exception {
		boolean resultOk;
		resultOk = clickOnButton(map.getLocatorVar("removeRegionsRM", region));
		return resultOk;
	}

	public boolean addRegionOnLocalMaster(String region) throws Exception {
		boolean resultOk;
		clickOnButton(map.getLocator("optionRegionsLocalMaster"));
		resultOk = setTextFieldDropdown(map.getLocator("fieldRegionsLocalMaster"), region);
		clickOnButton(map.getLocator("btnAddRegionsLocalMaster"));
		return resultOk;
	}

	public boolean removeRegionOnLocalMaster(String region) throws Exception {
		boolean resultOk;
		resultOk = clickOnButton(map.getLocatorVar("removeRegionsLocalMaster", region));
		return resultOk;
	}

	public void removeAllRegionsOnItemReviewer() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		if (existsElement(map.getLocator("regionsNameItemReviewer"))) {
			List<WebElement> regions = getDriver().findElements(map.getLocator("regionsNameItemReviewer"));
			for (int i = 0; i < regions.size(); i++) {
				removeRegionOnItemReviewer(regions.get(i).getText());
				confirmRemoveRegion();
			}
		}
	}

	public void removeAllItemsOnItemReviewer() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		if (existsElement(map.getLocator("getlistItemsOnItemReviewer"))) {
			List<WebElement> items = getDriver().findElements(map.getLocator("getlistItemsOnItemReviewer"));
			for (int i = 0; i < items.size(); i++) {
				String name = items.get(i).getText().split("] ")[1];
				removeItemOnItemReviewer(name);
				waitLoading();
				clickOnButton(map.getLocatorVar("btnYesPopup", " Remove User Permission?"));
			}
		}
	}

	public boolean addModelOnGTAM(String model) throws Exception {
		boolean resultOk;
		clickOnButton(map.getLocator("optionModelGTAM"));
		resultOk = setTextFieldDropdown(map.getLocator("fieldModelGTAM"), model);
		clickOnButton(map.getLocator("btnAddModelGTAM"));
		return resultOk;
	}

	public boolean removeModelOnGTAM(String model) throws Exception {
		boolean resultOk;
		resultOk = clickOnButton(map.getLocatorVar("removeModelGTAM", model));
		return resultOk;
	}

	public boolean confirmRemove() throws Exception {
		elementFocus();
		return clickOnButton(map.getLocator("btnConfirmRemove"));
	}

	/**
	 * This method returns a list of items in a role of user given the section
	 * of role eg. the role "SW P/L" has the section of regions(1) and
	 * models(2), if we want regions then the parameters should be ("SW P/L", 1)
	 */
	public ArrayList<String> getItemsOfRole(String role, int section) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		ArrayList<String> array = new ArrayList<>();
		List<WebElement> items = getDriver()
				.findElements(map.getLocatorVar("getlistContentItems", role, (section * 2) + ""));

		for (WebElement item : items) {
			array.add(item.getText());
		}
		return array;
	}

	public ArrayList<String> getAllRoleOfThisUser() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		ArrayList<String> array = new ArrayList<>();
		List<WebElement> items = getDriver().findElements(map.getLocator("rolesOfUser"));
		for (WebElement item : items) {
			array.add(item.getText());
		}
		return array;
	}

	public boolean verifyIfProfileIsAssociatedWithUser() throws Exception {
		boolean isTrue = false;
		ArrayList<String> allRoleOfThisUser = getAllRoleOfThisUser();
		for (int i = 0; i < allRoleOfThisUser.size(); i++) {
			if (ConstantsUserManagement.NVG_PVG.equals(allRoleOfThisUser.get(i))) {
				isTrue = true;
				break;
			}
		}
		return isTrue;
	}

	public ArrayList<String> getItemsOfItemReviewer() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		ArrayList<String> array = new ArrayList<>();
		List<WebElement> items = getDriver().findElements(map.getLocator("getlistItemsOnItemReviewer"));

		for (WebElement item : items) {
			array.add(item.getText());
		}
		return array;
	}

	public void removeRole(String role) throws Exception {
		clickOnButton(map.getLocatorVar("removeRole", role));
	}

	public void removeAllRolesExcept(String role) throws Exception {
		waitLoadingOverlay();
		ArrayList<String> array = getAllRoleOfThisUser();
		for (String r : array) {
			if (!r.equals(role)) {
				removeRole(r);
				confirmRemoveRole();
			}
		}
		Thread.sleep(100);
	}

	private void confirmRemoveRole() throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("btnYesPopup", " Remove User Role?"));
	}
}
