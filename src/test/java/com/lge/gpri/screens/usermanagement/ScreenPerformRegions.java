package com.lge.gpri.screens.usermanagement;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

import edu.emory.mathcs.backport.java.util.Arrays;

/**
* Perform Regions class.
* 
* <p>Description: Methods to perform Regions
*  
* @author Gabriel Costa do Nascimento
*/

public class ScreenPerformRegions extends AbstractScreen{
	private ObjectMap map;
	
	@SuppressWarnings("unchecked")
	ArrayList<String> tabs = new ArrayList<>(Arrays.asList(new String[]{
			"Country","Local Master","Requirement Manager",
			"Software Project Leader","Item Reviewers","NVG/PVG"
	}));

	public ScreenPerformRegions(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_USER_MANAGEMENT);
	}
	
	public void openPage() {
		getDriver().get("http://136.166.96.136:8204/Views/Admin/Region/#/");
		getDriver().manage().window().maximize(); 
	}

	public void searchRegion(String entry) throws Exception {
		setTextField(map.getLocator("fieldSearchRegions"), entry);
	}

	public void clickOnRegion(String region) throws Exception {
		clickOnButton(map.getLocatorVar("listItemRegion", region));
	}

	public void clickOnTabNVGPVG() throws Exception {
		clickOnButton(map.getLocatorVar("tabDetails", "NVG/PVG"));
	}
	
	public void clickOnTabRequirementManager() throws Exception {
		clickOnButton(map.getLocatorVar("tabDetails", "Requirement Manager"));
	}
	
	public ArrayList<String> getItemsOfCountry() throws Exception{
		return getItemsOfTab((tabs.indexOf("Country")+1)+"");
	}
	
	public ArrayList<String> getItemsOfLocalMaster() throws Exception{
		return getItemsOfTab((tabs.indexOf("Local Master")+1)+"");
	}
	
	public ArrayList<String> getItemsOfRequirementManager() throws Exception{
		return getItemsOfTab((tabs.indexOf("Requirement Manager")+1)+"");
	}
	
	public ArrayList<String> getItemsOfSoftwareProjectLeader() throws Exception{
		return getItemsOfTab((tabs.indexOf("Software Project Leader")+1)+"");
	}
	
	public ArrayList<String> getItemsOfItemReviewers() throws Exception{
		return getItemsOfTab((tabs.indexOf("Item Reviewers")+1)+"");
	}
	
	public ArrayList<String> getItemsOfNVG_PVG() throws Exception{
		return getItemsOfTab((tabs.indexOf("NVG/PVG")+1)+"");
	}
	
	public ArrayList<String> getItemsOfTab(String tabNumer) throws Exception{
		ArrayList<String> names = new ArrayList<>();
		List<WebElement> items = getDriver().findElements(map.getLocatorVar("itemsOfTab", tabNumer));
		for(WebElement item:items){
			names.add(item.getText());
		}
		return names;
	}

	public boolean selectUserNVGPVG(String user) throws Exception {
		clickOnButton(map.getLocator("optionUserNVGPVG"));
		boolean setTextFieldDropdown = setTextFieldDropdown(map.getLocator("fieldUserNVGPVG"), user);
		clickOnButton(map.getLocator("btnSelectAllUsersNVGPVG"));
		return setTextFieldDropdown;
	}
	
	public boolean verifyIfUserIsAssociatedWithRegion(String user) throws Exception {
		return getDriver().findElement(map.getLocatorVar("userDisplayedInTable", user)).isDisplayed();
	}
	
	public void addUserNVGPVGButton() throws Exception {
		clickOnButton(map.getLocator("btnAddUserNVGPVG"));
	}
	
	public boolean clickOnSaveRegion() throws Exception {
		return clickOnButton(map.getLocator("btnSaveRegion"));
	}

	public void searchUserNVGPVG(String entry) throws Exception {
		setTextField(map.getLocator("fieldSearchUserNVGPVG"), entry);
	}

	public void removeUserNVGPVG(String user) throws Exception {
		clickOnButton(map.getLocatorVar("removeItemOnTab", "pvgs", user));
	}

	public void confirmRemove() throws Exception {
		clickOnButton(map.getLocator("confirmRemove"));
	}
	
	public void cancelRemove() throws Exception {
		clickOnButton(map.getLocator("cancelRemove"));
	}
	
	public void clickOkSavedRegionButton() throws Exception {
		clickOnButton(map.getLocator("btnOkSavedRegion"));
	}
}
