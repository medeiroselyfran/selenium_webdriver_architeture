package com.lge.gpri.screens.view;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
* Perform Follow page class.
* 
* <p>Description: Methods to test the GPRI View.
*  
* @author Gabriel Costa do Nascimento
*/

public class ScreenPerformFollow extends AbstractScreen{
	
	public ScreenPerformFollow(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_VIEW);
	}
	
	public void openPage() {
		getDriver().get("http://136.166.96.136:8204/Views/Follows/");
		getDriver().manage().window().maximize(); 
	}

	public void clickOnBuyersTab() throws Exception {
		clickOnButton(map.getLocatorVar("followTab", "Buyers"));
	}
	
	public void clickOnModelsTab() throws Exception {
		clickOnButton(map.getLocatorVar("followTab", "Models"));
	}
	
	public void clickOnPackagesTab() throws Exception {
		clickOnButton(map.getLocatorVar("followTab", "Packages"));
	}
	
	public void clickOnItemsTab() throws Exception {
		clickOnButton(map.getLocatorVar("followTab", "Items"));
	}

	/**
	 * Return the xpath of /div element item of the follow tab given the path of the element
	 */
	public String getItemXPathInPackagesTab(String tab, String item) throws Exception {
		return getPathFromBy(map.getLocatorVar("followItemInTab",tab,item));
	}
	
	public String getNameItemOnPackagesTab(String name) throws Exception {
		waitLoading();
		String pathNameItem = getItemXPathInPackagesTab("tabs-packages",name)+"/span";
		By locatorNameItem = By.xpath(pathNameItem);
		if(existsElement(locatorNameItem)){
			return getDriver().findElement(locatorNameItem).getText();
		}
		return null;
	}
	
	public void removeItemOnPackagesTab(String name) throws Exception {
		waitLoading();
		String pathRemoveItem = getItemXPathInPackagesTab("tabs-packages",name)+"/div";
		By locatorRemoveItem = By.xpath(pathRemoveItem);
		if(existsElement(locatorRemoveItem)){
			clickOnButton(locatorRemoveItem);
		}
	}
}

