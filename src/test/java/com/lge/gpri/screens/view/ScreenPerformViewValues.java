package com.lge.gpri.screens.view;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
* Perform view values page class.
* 
* <p>Description: Methods to test the GPRI View Values.
*  
* @author Cayk Lima Barreto
* @author Gabriel Costa do Nascimento
*/

public class ScreenPerformViewValues extends AbstractScreen{
	
	public ScreenPerformViewValues(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_VIEW);
	}
	
	public boolean waitExport() throws Exception{
		boolean result = false, donwloading=false;
		waitLoading();
		while(existsElement(map.getLocator("progressBar"))){
			Thread.sleep(500);
			donwloading = true;
		}
		if(donwloading&&!existsElement(map.getLocator("errorPopup"))){
			result = true;
		}
		Thread.sleep(1000);
		return result;
	}
	
	/**
	 * Allows the Item History to be clickable
	 */
	public void clickOnItemValue(String item) throws Exception {
		clickOnButton(map.getLocator(item));
	}
	
	public void clickOnExportButton() throws Exception{
		clickOnButton(map.getLocator("btnExport"));
	}
	
	/**
	 * Before call this method the clickOnItemValue() must be called
	 */
	public void clickOnItemHistory(String item) throws Exception {
		WebElement element = getDriver().findElement(map.getLocator(item));
		clickOnButtonByJavaScript(element);
	}
	
	public void clickOnRevisionButton() throws Exception {
		moveToPageHeader();
		clickOnButton(map.getLocator("optionRevision"));
	}

	/**
	 * Before call this method the clickOnRevisionButton() must be called
	 */
	public void clickOnLastRevisionDropdown() throws Exception {
		clickOnButton(map.getLocator("revisionSelected"));
	}
	public boolean clickOnRevisionDropdown(String number) throws Exception {
		return setTextFieldDropdown(map.getLocator("fieldRevision"), number);
	}
	
	public void clickOnViewPackageButton() throws Exception {
		waitLoadingOverlay();
		By locatorButtonView = map.getLocator("btnViewPackage");
		WebElement btnView = getDriver().findElement(locatorButtonView);
		if(btnView.isDisplayed()){
			clickOnButton(locatorButtonView);
			waitLoading();
		}
	}
	
	public void clickOnOptionsMenu() throws Exception {
		moveToPageHeader();
		clickOnButton(map.getLocator("btnOptionsMenu"));
	}
	
	/**
	 * Before call this method the clickOnOptionsMenu() must be called
	 * The option given should be the same of the text in options
	 */
	public boolean selectOption(String option) throws Exception{
		return clickOnButton(map.getLocatorVar("itemOption",option));
	}
	
	public void openViewBuyerChildren() throws Exception{
		moveToPageHeader();
		By locatorBtnView = map.getLocator("btnViewConfirmationBuyerChildren");
		WebElement btnView = getDriver().findElement(locatorBtnView);
		if(btnView.getAttribute("class").equals("gpri-img pill_down")){
			clickOnButton(locatorBtnView);
		}
	}
	
	public boolean openViewBuyerChildrenIsVisible() throws Exception{
		By locatorBtnView = map.getLocator("btnViewConfirmationBuyerChildren");
		return clickOnButton(locatorBtnView);
	}
	
	public void closeViewBuyerChildren() throws Exception{
		moveToPageHeader();
		By locatorBtnView = map.getLocator("btnViewConfirmationBuyerChildren");
		WebElement btnView = getDriver().findElement(locatorBtnView);
		if(btnView.getAttribute("class").equals("gpri-img pill_up")){
			clickOnButton(locatorBtnView);
		}
	}
	
	/**
	 * Before call this method the openViewBuyerChildren() must be called
	 * This method get the list of children in View Values Page returning
	 * a map list with the name of buyer child and a boolean showing if the buyer
	 * is confirmed or not. 
	 */
	public HashMap<String, Boolean> getConfirmationListBuyerChildren() throws Exception {
		HashMap<String,Boolean> listBuyerChildren = new HashMap<String, Boolean>();
		waitLoading();
		WebElement listChildren = getDriver().findElement(map.getLocator("listViewConfirmationBuyerChildren"));
		List<WebElement> listItemElements = listChildren.findElements(By.xpath("span"));
        for(WebElement element: listItemElements){
        	WebElement confirmedCheck=null;
        	try{
	        	getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
	        	confirmedCheck = element.findElement(By.xpath("i"));
	        	getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        	}catch(Exception e){}
        	boolean confirmed = false;
        	if(confirmedCheck!=null){
        		confirmed = confirmedCheck.getAttribute("class").equals("gpri-img tick_on");
        	}
        	String nameChild = element.findElement(By.xpath("child::*[2]")).getText();
        	listBuyerChildren.put(nameChild, confirmed);
        }
        
		return listBuyerChildren;
	}
	
	/**
	 * Verify if the Mother Buyer is confirmed
	 */
	public boolean getConfirmationMotherBuyer() throws Exception {
		WebElement confimedBuyer = getDriver().findElement(map.getLocator("confirmationMotherBuyer"));
		return confimedBuyer.getAttribute("class").equals("gpri-img tick_on");
	}
	
	public void setPackage(String packageName) throws Exception{
		if(!packageName.isEmpty()){
			waitLoading();
			moveToPageHeader();
			clickOnButton(map.getLocator("optionPackage"));
			setTextFieldDropdown(map.getLocator("fieldPackage"), packageName);
		}
	}

	/**
	 * Show if the table of items of package is being displayed
	 */
	public boolean packageItemsTableIsShowing() throws Exception {
		if(existsElement(map.getLocator("itemsOfPackageTable"))){
			return true;
		}
		return false;
	}
	
	public void selectAllAutoProfiles() throws Exception{
		waitLoading();
		moveToPageHeader();
		clickOnButton(map.getLocator("optionAutoProfiles"));
		clickOnButton(map.getLocator("selectAllAutoProfile"));
		clickOnButton(map.getLocator("optionAutoProfiles"));
	}
	
	public void deselectAllAutoProfiles() throws Exception{
		waitLoading();
		moveToPageHeader();
		clickOnButton(map.getLocator("optionAutoProfiles"));
		clickOnButton(map.getLocator("deselectAllAutoProfile"));
		clickOnButton(map.getLocator("optionAutoProfiles"));
	}

	public void setNameFilter(String entry) throws Exception {
		waitLoading();
		setTextField(map.getLocatorVar("fieldNameFilter"), entry);
		getDriver().findElement(map.getLocatorVar("fieldNameFilter")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
	}
	
	public void setValueFilter(String entry) throws Exception {
		waitLoading();
		setTextField(map.getLocatorVar("fieldValueFilter"), entry);
		getDriver().findElement(map.getLocatorVar("fieldValueFilter")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
	}
	
	/**
	 * Return the xpath of /tr element of the tree table given the path of the element
	 * The given path should be the same when right-clicked on the item and then copy the path
	 */
	public String getXPathInTreeTable(String pathUnic) throws Exception{
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocatorVar("tabTableBody",path[0]));
		for(int i=1;i<path.length-1;i++){
			pathToElement+=getPathFromBy(map.getLocatorVar("hierarchyTreeRelative",path[i]));
		}
		pathToElement+=getPathFromBy(map.getLocatorVar("childTreeRelative", path[path.length-1]));
		return pathToElement;
	}

	public String getNameItemOnTreeTable(String pathToItem) throws Exception {
		waitLoading();
		String pathTrItem = getXPathInTreeTable(pathToItem);
		String pathToName = pathTrItem+getPathFromBy(map.getLocator("nameItemRelative"));
		By locatorItem = By.xpath(pathToName);
		if(existsElement(locatorItem)){
			return getDriver().findElement(locatorItem).getText();
		}
		return null;
	}
	
	public String getValueItemOnTreeTable(String pathToItem) throws Exception {
		waitLoading();
		String pathTrItem = getXPathInTreeTable(pathToItem);
		String pathToValue = pathTrItem+getPathFromBy(map.getLocator("valueItemRelative"));
		By locatorItem = By.xpath(pathToValue);
		if(existsElement(locatorItem)){
			return getDriver().findElement(locatorItem).getText();
		}
		return null;
	}
	
	public void clickOnExpandAllButton() throws Exception {
		clickOnButton(map.getLocator("btnExpandAll"));
		waitLoading();
	}
	
	public void clickOnCollapseAllButton() throws Exception {
		clickOnButton(map.getLocator("btnCollapseAll"));
		waitLoading();
	}
	
	public void setEnableShowOnlyChangedAfterCreationButton() throws Exception {
		By locatorButton = map.getLocator("btnShowOnlyChangedAfterCreation");
		boolean enable = getDriver().findElement(locatorButton).getAttribute("class").contains("active");
		if(!enable){
			clickOnButton(locatorButton);
			waitLoading();
		}
		
	}
	
	public void setDisableShowOnlyChangedAfterCreationButton() throws Exception {
		By locatorButton = map.getLocator("btnShowOnlyChangedAfterCreation");
		boolean enable = getDriver().findElement(locatorButton).getAttribute("class").contains("active");
		if(enable){
			clickOnButton(locatorButton);
			waitLoading();
		}
	}
	
	public void clickOnCenterValuesButton() throws Exception {
		clickOnButton(map.getLocator("btnCenterValues"));
		waitLoading();
	}
	
	public boolean checkApp1GridValues() throws Exception {
		boolean result = false;
		String appColumn = getDriver().findElement(map.getLocator("gridColumnApp1")).getText();
		String appRow = getDriver().findElement(map.getLocator("gridRowApp1")).getText();
		
		if(appColumn.equals("1") && appRow.equals("1")){
			result = true;
		}
		return result;
	}
	
	public boolean checkApp2GridValues() throws Exception {
		boolean result = false;
		String appColumn = getDriver().findElement(map.getLocator("gridColumnApp2")).getText();
		String appRow = getDriver().findElement(map.getLocator("gridRowApp2")).getText();
		
		if(appColumn.equals("1") && appRow.equals("3")){
			result = true;
		}
		return result;
	}

	public void clickOnCustomizationButton() throws Exception {
		clickOnButton(map.getLocator("btnCustomization"));
	}
	
	public void clickOnUiStartButton() throws Exception {
		WebElement element = getDriver().findElement(map.getLocator("btnStarUiSimulator"));
		clickOnButtonByJavaScript(element);
	}

	public void clickOnLockUnlockButton() throws Exception {
		WebElement element = getDriver().findElement(map.getLocator("btnLockUnlockUi"));
		clickOnButtonByJavaScript(element);
		
	}
	
	public boolean checkCollisionBetweenApps(String app1, String app2) throws Exception{
		boolean result = false;
		String classApp1 = getDriver().findElement(map.getLocator(app1)).getAttribute("class");
		String[] classesApp1 = classApp1.split(" ");
		String classApp2 = getDriver().findElement(map.getLocator(app2)).getAttribute("class");
		String[] classesApp2 = classApp2.split(" ");
		
		if(classesApp1.length > 3 && classesApp1[3].equals("uiSimulator_homeScreenCollision") && classesApp2.length > 3 && classesApp2[3].equals("uiSimulator_homeScreenCollision")){
			result = true;
		}
		
		return result;
	}

	public boolean isParametersCollapsed() throws Exception {
		return getDriver().findElement(map.getLocator("parametersCollapsed")).getAttribute("class").contains("ttCollapsed");
	}

	public boolean isRevisionCommentsVisible() throws Exception {
		return getDriver().findElement(map.getLocator("revisionComments")).isDisplayed();
	}
	
	public boolean isModelPackageInfoVisible() throws Exception {
		return getDriver().findElement(map.getLocator("modelPackageInfo")).isDisplayed();
	}
	
	public boolean isAppCounterVisible() throws Exception {
		return getDriver().findElement(map.getLocator("appCounter")).isDisplayed();
	}

	public void clickOnItemLibraryCheck() throws Exception {
		clickOnButton(map.getLocator("chkItemLibrary"));
	}
	
	public boolean existsElement(String name) throws Exception{
		return existsElement(map.getLocator(name));
	}
	
	public void clickOnItemHistoryCloseButton() throws Exception {
		clickOnButton(map.getLocator("itemHistoryClose"));
	}
}

