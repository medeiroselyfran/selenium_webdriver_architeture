package com.lge.gpri.screens.view;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
* Perform view page class.
* 
* <p>Description: Methods to test the GPRI View.
*  
* @author Cayk Lima Barreto
* @author Gabriel Costa do Nascimento
*/

public class ScreenPerformView extends AbstractScreen{
	
	boolean loopSearch = true;
	int countSearch = 0;
	public ScreenPerformView(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_VIEW);
	}
	
	public void openPage() {
		getDriver().get(ConstantsGeneric.LINK_SITE_VIEW_PAGE);
		getDriver().manage().window().maximize(); 
	}
	
	/**
	 * If the search do not correspond, the search will repeat 5 times
	 */
	public String[] addSearch(String item) throws Exception{
		waitLoading();
		By searchInput = map.getLocator("viewSearch");
		setTextFieldView(searchInput, item);
		Thread.sleep(500);
		clickOnButton(searchInput);
		if(!existsElement(map.getLocator("firstResultSearch"))){
			clickOnButton(searchInput);
		}
		waitLoading();
		WebElement list = getDriver().findElement(map.getLocator("listResultSearch"));
		waitLoading();
		List<WebElement> listElements = list.findElements(By.tagName("li"));
		String[] results = new String[listElements.size()];
		for(int i=0;i<listElements.size();i++){
			results[i]=listElements.get(i).getText();
		}
		if(existsElement(map.getLocator("firstResultSearch"))){
			waitLoading();
			String firstResult = getDriver().findElement(map.getLocator("firstResultSearch")).getText();
			String[] subItems = item.split(" ");
			for(int i=0;i<subItems.length;i++){
				if(!firstResult.contains(subItems[i])){
					elementFocus();
					if(loopSearch){
						if(countSearch==5){
							countSearch=0;
							loopSearch = false;
							return addSearch(item);
						}
						countSearch++;
						results = addSearch(item);
						
					}else{
						loopSearch = true;
						results = new String[]{null};
					}
					countSearch=0;
					return results;
				}
			}
		}
		waitLoading();
		getDriver().findElement(searchInput).sendKeys(Keys.ENTER);
		return results;
	}
	
	public void removeSearch(String item) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("removeSearch",item));
	}

	public boolean selectFirstItemResultSearch() throws Exception {
		return clickOnButton(map.getLocator("viewSelectedPackage"));
	}
	
	public boolean searchIsClean() throws Exception{
		return !existsElement(map.getLocator("elementsInSearch"));
	}
	
	public String getAttributeFirstItem(String attributeName) throws Exception{
		waitLoading();
		waitLoadingOverlay();
		int position = getPositionNode(map.getLocatorVar("attributeNameCard", "1", attributeName));
		waitLoading();
		return getDriver().findElement(map.getLocatorVar("attributeValueCard", "1", position+"")).getText();
	}
	
	public String getAttributeOfCardItem(String attributeName, int cardPosition) throws Exception{
		waitLoading();
		int position = getPositionNode(map.getLocatorVar("attributeNameCard", cardPosition+"", attributeName));
		waitLoading();
		return getDriver().findElement(map.getLocatorVar("attributeValueCard", cardPosition+"", position+"")).getText();
	}

	public int getPositionItemWithName(String name) throws Exception {
		return getPositionNode(map.getLocatorVar("cardWithName", name));
	}
	
	public String getFieldValueCard(String field) throws Exception {
		field = "field" + field;
		By by = map.getLocatorVar("packageValuePerFieldCard", field);
		return getDriver().findElement(by).getText();
	}
	
	public String getPackageNameCard() throws Exception {
		By by = map.getLocator("packageNameCard");
		return getDriver().findElement(by).getText();
	}
	
	public String getPackageStatusCard() throws Exception {
		By by = map.getLocator("packageStatusCard");
		return getDriver().findElement(by).getText();
	}
	
	public String getPackageBuyerCard() throws Exception {
		By by = map.getLocator("packageBuyerNameCard");
		return getDriver().findElement(by).getText();
	}
	
	public String getValuePerFieldOfCardBasedOnModel(String model, String fieldName) throws Exception {
		fieldName = "field" + fieldName;
		By by = map.getLocatorVar("valueCardPerField", model, fieldName);
		return getDriver().findElement(by).getText();
	}
}

