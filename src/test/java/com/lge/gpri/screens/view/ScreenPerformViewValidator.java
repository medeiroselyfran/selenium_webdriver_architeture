package com.lge.gpri.screens.view;

import org.openqa.selenium.WebDriver;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
* Perform view validator page class.
* 
* <p>Description: Methods to test the GPRI View Validator.
*  
* @author Cayk Lima Barreto
*/

public class ScreenPerformViewValidator extends AbstractScreen{
	public ScreenPerformViewValidator(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_VIEW);
	}
	
	public void searchValidator(String validator) throws Exception{
		setTextField( map.getLocator("fieldSearchValidator"), validator);
	}
	
	public void setNewValue(String value) throws Exception{
		setTextField( map.getLocator("fieldAddValue"), value);
	}
	
	public void clickOnTimeFormatValidatorButton() throws Exception {
		clickOnButton(map.getLocatorVar("timeFormatValidator"));
	}
	
	public void clickOnAddNewValueButton() throws Exception {
		clickOnButton(map.getLocatorVar("btnAddValue"));
	}
	
	public void clickOnSaveValidatorButton() throws Exception {
		clickOnButton(map.getLocatorVar("btnSaveValidator"));
	}
	
	public void clickOnValidatorSuccessfullySavedButton() throws Exception {
		Thread.sleep(1000);
		elementFocus();
		clickOnButton(map.getLocatorVar("btnValidatorSaved"));
	}
	
	public void clickOnDeleteTimeFormatButton() throws Exception {
		clickOnButton(map.getLocatorVar("btnDeleteValue"));
	}
	
	public void clickOnConfirmDeletedValueButton() throws Exception {
		clickOnButton(map.getLocatorVar("btnConfirmDeleteValue"));
	}
}

