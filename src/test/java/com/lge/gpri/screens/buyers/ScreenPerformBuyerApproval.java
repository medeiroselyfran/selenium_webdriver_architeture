package com.lge.gpri.screens.buyers;

import org.openqa.selenium.WebDriver;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
* <pre>
* Author: Gabriel Costa do Nascimento
* Purpose: Methods to realize the tests buyers approval screen - Gabriel Costa do Nascimento - 25 aug 2017
* History: 	Class creation - Gabriel Costa do Nascimento - 25 aug 2017
* 
* </pre>
*/

/**
* Perform creation and edition of buyers page class.
* 
* <p>Description: Methods to create and edit a Buyer
*  
* @author Gabriel Costa do Nascimento
*/

public class ScreenPerformBuyerApproval extends AbstractScreen{
	
	public ScreenPerformBuyerApproval(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_BUYER);
	}
	
	public void clickOnCloseRevisionCommentsButton() throws Exception{
		waitLoading();
		elementFocus();
		clickOnButton(map.getLocator("btnCloseRevisionComments"));
	}
	
	public void clickOnApproveFirstOfTable() throws Exception{
		waitLoading();
		clickOnButton(map.getLocator("btnApprove"));
	}
	
	public void clickOnApproveAutoProfile(String value) throws Exception{
		waitLoading();
		clickOnButton(map.getLocatorVar("btnApproveAutoProfile", value));
	}
	
	public void clickOnApproveAll() throws Exception{
		clickOnButton(map.getLocator("btnApproveAll"));
	}
	
	public void clickOnApproveFirstAutoProfile() throws Exception{
		waitLoading();
		clickOnButton(map.getLocator("btnApproveFirstAutoProfile"));
	}
	
	public void clickOnSaveButton() throws Exception{
		waitLoading();
		waitLoadingOverlay();
		clickOnButton(map.getLocator("btnSave"));
	}
	
	public void clickOnCancelButton() throws Exception{
		waitLoading();
		clickOnButton(map.getLocator("btnCancel"));
	}
	
	public boolean clickOnConfirmSave() throws Exception{
		waitLoading();
		waitLoadingOverlay();
		return clickOnButton(map.getLocatorVar("btnYesPopup","Success"));
	}
	
	
}
