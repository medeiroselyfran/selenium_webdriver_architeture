package com.lge.gpri.screens.buyers;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;
import com.lge.gpri.screens.itemLibrary.ScreenPerformItemLibrary;
import com.lge.gpri.screens.packages.ScreenPerformListPackages;

/**
* Perform edit buyer profile page class.
* 
* <p>Description: Methods to test the buyer profile.
*  
* @author Gabriel Costa, Cayk Lima Barreto
*/

public class ScreenPerformEditBuyerProfile extends AbstractScreen{
	
	public ScreenPerformEditBuyerProfile(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_BUYER);
	}
	
	public boolean addItem(String item) throws Exception{
		waitLoadingOverlay();
		boolean result = false;
		if(!boxIsChecked(item)){
			result = clickOnButton(map.getLocator(item));
		}
		return result;
	}
	
	public boolean removeItem(String item) throws Exception{
		waitLoadingOverlay();
		boolean result = false;
		if(boxIsChecked(item)){
			result = clickOnButton(map.getLocator(item));
		}
		return result;
	}
	
	public void setValue(String fieldName, String value) throws Exception{
		if(!fieldName.isEmpty() && !value.isEmpty()){
			setTextField( map.getLocator(fieldName), value);
		}
	}
	
	public String getValue(String fieldName) throws Exception {
		if(!fieldName.isEmpty()){
			return getDriver().findElement(map.getLocator(fieldName)).getAttribute("value");
		}
		return null;
	}
	
	public void setMessageRevisionComment(String message, String flag) throws Exception{
		waitLoading();
		if(existsElement("dlgRevisionComment")){
			String display = getDriver().findElement(map.getLocator("dlgRevisionComment")).getAttribute("style");
			String none = display.split(" ")[1];
			if(!none.equals("none;")){
				if(flag.equals("normal")){
					clickOnNormalChangeButton();
				}
				else if(flag.equals("critical")){
					clickOnCriticalChangeButton();
				}
				setTextField( map.getLocator("txtRevisionComment"), message);
			}
		}
	}
	
	public void searchItemSelectItems(String item) throws Exception{
		setTextField( map.getLocator("fieldSearch"), item);
	}
	
	public void discardInheritanceSearchItems(String item) throws Exception{
		setTextField( map.getLocator("discardInheritanceSearchItems"), item);
	}
	
	public void optionFilters() throws Exception{
		waitLoading();
		clickOnButton(map.getLocator("optionFilters"));
	}
	
	public void discardInheritanceOptionFilters() throws Exception{
		waitLoading();
		clickOnButton(map.getLocator("discardInheritanceOptionFilters"));
	}
	
	public boolean selectAllFilters() throws Exception{
		waitLoading();
		return clickOnButton(map.getLocator("selectAllFilters"));
	}
	
	public boolean discardInheritanceSelectAllFilters() throws Exception{
		waitLoading();
		return clickOnButton(map.getLocator("discardInheritanceSelectAllFilters"));
	}
	
	public boolean deselectAllFilters() throws Exception{
		waitLoading();
		return clickOnButton(map.getLocator("deselectAllFilters"));
	}
	
	public boolean discardInheritanceDeselectAllFilters() throws Exception{
		waitLoading();
		return clickOnButton(map.getLocator("discardInheritanceDeselectAllFilters"));
	}
	
	public boolean removeFilter(String filterCriteria) throws Exception{
		waitLoading();
		boolean result = false;
		WebElement element = getDriver().findElement(map.getLocatorVar("btnRemoveFilter", filterCriteria));
		if(element != null){
			element.click();
			result = true;
		}
		return result;
	}
	
	public boolean discardInheritanceRemoveFilter(String filterCriteria) throws Exception{
		waitLoading();
		boolean result = false;
		WebElement element = getDriver().findElement(map.getLocatorVar("discardInheritanceBtnRemoveFilter", filterCriteria));
		if(element != null){
			element.click();
			result = true;
		}
		return result;
	}
	
	public void searchItemLibrary(String item) throws Exception{
		waitLoading();
		if(!item.isEmpty()){
			WebElement fieldElement = getDriver().findElement(map.getLocator("fieldItemLibrary"));
			fieldElement.clear();
			fieldElement.sendKeys(item);
			waitFieldToBeFilled(map.getLocator("fieldItemLibrary"),item);
		}
	}
	
	public void dicardInheritanceSearchItemLibrary(String item) throws Exception{
		waitLoading();
		if(!item.isEmpty()){
			WebElement fieldElement = getDriver().findElement(map.getLocator("discardInheritanceFieldItemLibrary"));
			fieldElement.clear();
			fieldElement.sendKeys(item);
			waitFieldToBeFilled(map.getLocator("discardInheritanceFieldItemLibrary"),item);
		}
	}
	
	public boolean clickOnSelectedFilter(String filter) throws Exception {
		return clickOnButton(map.getLocatorVar("choosedFilter", filter));
	}
	
	public boolean clickOnDiscardInheritanceSelectedFilter(String filter) throws Exception {
		return clickOnButton(map.getLocatorVar("discardInheritanceChoosedFilter", filter));
	}
	
	public boolean clickOnSelectedFilterOptions(String criteria) throws Exception {
		return clickOnButton(map.getLocatorVar("selectedFilterOptions", criteria));
	}
	
	public boolean selectValueFilterOptionWithArea(String criteria, String option, String area) throws Exception {
		return setTextFieldDropdownWithArea(map.getLocatorVar("fieldCritertia", criteria), area, option);
	}
	
	public ArrayList<String> getAllValuesOfTagValues() throws Exception {
		String pathFilterBase = getPathFromBy(map.getLocatorVar("selectedFilterOptions", ConstantsBuyers.CRITERIA_TAG_VALUES));
		String values = getDriver().findElement(By.xpath(pathFilterBase)).findElement(By.xpath("span")).getText();
		ArrayList<String> result = new ArrayList<>(Arrays.asList(values.split(", ")));
		return result;
	}
	
	public boolean clickOnSelectedFilterOption(String criteria, String option) throws Exception {
		return clickOnButton(map.getLocatorVar("selectedFilterOption", criteria, option));
	}
	
	public boolean clickOnSelectedOptionForSmartCaTypeFilter(String option) throws Exception {
		return clickOnButton(map.getLocatorVar("optionsToSmartCaTypesFilter", option));
	}
	
	public boolean clickOnSmartCaDropDown() throws Exception {
		return clickOnButton(map.getLocator("fieldDropDownSmartCaType"));
	}
	
	public boolean clickOnSmartCaDropDownInDiscardInheritance() throws Exception {
		return clickOnButton(map.getLocator("fieldDropDownSmartCaTypeInDiscardInheritance"));
	}
	
	public boolean clickOnSelectedOptionForSmartCaTypeFilterInDiscardInheritance(String option) throws Exception {
		return clickOnButton(map.getLocatorVar("optionsToSmartCaTypesFilterInDiscardInheritance", option));
	}
	
	public boolean isVisibleFilterOptions(String criteria) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		return getDriver().findElement(map.getLocatorVar("selectedFilterOptions", criteria)).isDisplayed();
	}
	
	public boolean isVisibleFilterSmartCaOption() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		return getDriver().findElement(map.getLocator("fieldDropDownSmartCaType")).isDisplayed();
	}
	
	public void setTextOnFilterName(String text) throws Exception {
		setTextField( map.getLocator("fieldNameOnFilterName"), text);
	}
	
	public boolean isVisibleFilterName() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		return getDriver().findElement(map.getLocator("fieldNameOnFilterName")).isDisplayed();
	}
	
	public void discardInheritanceSetTextOnFilterName(String text) throws Exception {
		setTextField( map.getLocator("discardInheritanceFieldNameOnFilterName"), text);
	}
	
	public void setItemLibrary(String item) throws Exception{
		setTextFieldDropdown( map.getLocator("fieldItemLibrary"), item);
	}

	public boolean clickOnSelectItemsButton() throws Exception {
		return clickOnButton(map.getLocator("btnSelectItems"));
	}

	public void clickOnSaveSelectedItems() throws Exception {
		clickOnButton(map.getLocator("btnSaveSelectedItems"));
	}

	public boolean clickOnSaveProfile() throws Exception {
		return clickOnButton(map.getLocator("btnSaveProfile"));
	}
	
	public boolean clickOnCancelButton() throws Exception {
		return clickOnButton(map.getLocator("btnCancel"));
	}
	
	public boolean clickOnConfirmCancelButton() throws Exception {
		return clickOnButton(map.getLocator("btnConfirmCancel"));
	}

	public void clickOnSaveEditedProfile() throws Exception {
		clickOnButton(map.getLocator("btnSaveEditedProfile"));
	}

	public void clickOnItem(String item) throws Exception {
		waitElementToBeClickable(map.getLocator(item));
		clickOnButton(map.getLocator(item));
	}

	public void clickOnMarkItemsAsDoNotApplyButton() throws Exception {
		clickOnButton(map.getLocator("btnMarkItemsAsDoNotApply"));
	}

	public String getAttributeClassOfElement(String element) throws Exception {
		return getDriver().findElement(map.getLocator(element)).getAttribute("class");
	}

	public void clickOnNote() throws Exception {
		clickOnButton(map.getLocator("homeNetworkNote"));
	}
	
	public void setItemValueChangeNote(String item, String message) throws Exception {
		clickOnButton(map.getLocator(item));
		setTextField( map.getLocator("fieldNote"), message);
	}

	public void clickOnSaveNote() throws Exception {
		clickOnButton(map.getLocator("btnSaveNote"));
	}
	
	public void clickOnCloseNote() throws Exception {
		clickOnButton(map.getLocator("btnCloseNote"));
	}
	
	public void clickOnCancelNote() throws Exception {
		clickOnButton(map.getLocator("btnCancelNote"));
	}
	
	public void addNote(String field, String value1, String value2, String fieldNote, String note) throws Exception{
		waitLoading();
		if(verifyIfValuesAreEquals(field, value1)){
			setValue(field, value2);
		}else{
			setValue(field, value1);
		}
		setItemValueChangeNote(fieldNote, note);
		clickOnSaveNote();
	}
	
	public void setValueItemPerProfile(String optionValue, String value) throws Exception {
		By locatorOption = map.getLocator(optionValue);
		String pathToOption = getPathFromBy(locatorOption);
		String pathToPerProfile = getPathFromBy(map.getLocator("relativePerProfileParemeters"));
		String pathToInput = getPathFromBy(map.getLocator("relativeFieldParemeters"));
		By locatorPerProfile = By.xpath(pathToOption+pathToPerProfile);
		By locatorInput = By.xpath(pathToOption+pathToInput);
		clickOnButton(locatorOption);
		clickOnButton(locatorPerProfile);
		setTextField(locatorInput, value);
	}
	
	public String getValueFromFieldText(String field) throws Exception {
		return getDriver().findElement(map.getLocator(field)).getAttribute("value");
	}
	
	public String getText(String field) throws Exception {
		return getDriver().findElement(map.getLocator(field)).getText();
	}

	public void checkItemLibrary(String item) throws Exception {
		waitLoading();
		if(!getDriver().findElement(map.getLocator(item)).getAttribute("class").contains("jstree-checked")){
			clickOnButton(map.getLocator(item));
		}
	}

	public void checkNormalChange() throws Exception {
		waitLoading();
		if(!getDriver().findElement(map.getLocator("btnNormalChange")).getAttribute("class").contains("checked")){
			clickOnButton(map.getLocator("btnNormalChange"));
		}
	}
	
	/**
	 * This method set the value of item in a package give the buyer, the model and the path of item in item library
	 * If item parameter is "All" and value is "" the items will be cleaned
	 */
	public void preconditionItem(String buyerSuffix, String item, String value, String path, boolean onlyThisItem) throws Exception{
		ScreenPerformListBuyers screenPerformListBuyers = new ScreenPerformListBuyers(getDriver());
		ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
		ScreenPerformListPackages screenPerformListPackages = new ScreenPerformListPackages(getDriver());
		ScreenPerformItemLibrary screenItemLibrary = new ScreenPerformItemLibrary(getDriver());
		
		screenPerformListBuyers.openPage();
		screenPerformListBuyers.addSuffixFilter();
		screenPerformListBuyers.setSuffixFilter(buyerSuffix);
		boolean resultOk = screenPerformListBuyers.clickOnEditProfile(buyerSuffix);
		if(!resultOk){
			screenPerformListPackages.clickOnReopenPackageButton();
			screenPerformListPackages.clickOnExtendPackageDate();
			screenPerformListPackages.selectExtendedDateToday();
			screenPerformListPackages.rejectGeneratingAPK(buyerSuffix);
			screenPerformListPackages.clickOnEditProfilePackageButton();
		}
		if(item.equals(ConstantsBuyers.ALL)){
			resultOk = screenPerformEditBuyerProfile.clickOnNoItemAddedSelectItems();
			if(!resultOk){
				screenPerformEditBuyerProfile.clickOnSelectItemsButton();
				screenPerformEditBuyerProfile.addItemByPath("");
				screenPerformEditBuyerProfile.removeItemByPath("");
				screenPerformEditBuyerProfile.moveToPageBottom();
				screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
				screenPerformEditBuyerProfile.clickOnSaveProfile();
				screenPerformEditBuyerProfile.warningContinueAnyway();
				screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
				screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
			}
		}else{
			if(onlyThisItem){
				resultOk = screenPerformEditBuyerProfile.clickOnSelectItemsButton();
				if(!resultOk){
					screenPerformEditBuyerProfile.clickOnNoItemAddedSelectItems();
				}
				String pathGroup = path.substring(0, path.lastIndexOf("::"));
				ArrayList<String> pathsToMandatoryItems = getMandatoryItemsInSelectItemsGivenGroup(pathGroup);
				if(pathsToMandatoryItems.size()!=0){
					newTab("");
					switchTabs(1);
					for(String parhItem:pathsToMandatoryItems){
						screenItemLibrary.preconditionItem(parhItem, ConstantsBuyers.MANDATORY_FOR_GROUP, false);
					}
					getDriver().close();
					switchTabs(0);
					refreshPage();
					resultOk = screenPerformEditBuyerProfile.clickOnSelectItemsButton();
					if(!resultOk){
						screenPerformEditBuyerProfile.clickOnNoItemAddedSelectItems();
					}
				}
				screenPerformEditBuyerProfile.addItemByPath("");
				screenPerformEditBuyerProfile.removeItemByPath("");
				screenPerformEditBuyerProfile.addItemByPath(path.split(" #")[0]);
				screenPerformEditBuyerProfile.moveToPageBottom();
				screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
			}else{
				screenPerformEditBuyerProfile.setNameFilter(item.split("#")[0]);
				if(!screenPerformEditBuyerProfile.itemIsVisible(item)){
					resultOk = screenPerformEditBuyerProfile.clickOnSelectItemsButton();
					if(!resultOk){
						screenPerformEditBuyerProfile.clickOnNoItemAddedSelectItems();
					}
					screenPerformEditBuyerProfile.addItemByPath(path.split(" #")[0]);
					screenPerformEditBuyerProfile.moveToPageBottom();
					screenPerformEditBuyerProfile.clickOnSaveSelectedItems();
				}
			}
			String valueActual = screenPerformEditBuyerProfile.getValueGivenNameItem(item);
			if(!valueActual.equals(value)){
				screenPerformEditBuyerProfile.setValueGivenNameItem(item, value);
			}
			screenPerformEditBuyerProfile.setNameFilter("");
			screenPerformEditBuyerProfile.setEmptyValuesRandomly();
			Thread.sleep(1000);
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			resultOk = screenPerformEditBuyerProfile.warningContinueAnyway();
			if(!valueActual.equals(value)){
				screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
				screenPerformEditBuyerProfile.checkNormalChange();
				screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
				screenPerformEditBuyerProfile.clickOnSaveProfile();
			}
			screenPerformEditBuyerProfile.setMessageRevisionComment(ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING, ConstantsBuyers.NORMAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		}
	}
	
	public boolean clickOnNoItemAddedSelectItems() throws Exception {
		boolean resultOk = false;
		try{
			resultOk = clickOnButton(map.getLocator("noItemAddedSelectItem"));
		}catch (Exception e) {}
		return resultOk;
	}

	private ArrayList<String> getMandatoryItemsInSelectItemsGivenGroup(String pathUnic) throws Exception {
		waitLoading();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for(int i=0;i<path.length&&!pathUnic.isEmpty();i++){
			pathToElement+=getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative",path[i]));
		}
		pathToElement+=getPathFromBy(map.getLocator("mandatoryItemLibraryRelative"));
		List<WebElement> itemsMandatory = getDriver().findElements(By.xpath(pathToElement));
		ArrayList<String> paths = new ArrayList<>();
		for(WebElement element: itemsMandatory){
			String name = element.getText().split("\\(")[0];
			paths.add(pathUnic+"::"+name);
		}
		return paths;
	}

	public void setEmptyValuesRandomly() throws Exception {
		String value = "111 11";
		if(existsElement(map.getLocator("inputsEditProfile"))){
			List<WebElement> emptyInputs = getDriver().findElements(map.getLocator("inputsEditProfile"));
			for(WebElement input: emptyInputs){
				input.click();
			}
			if(existsElement(map.getLocator("emptyInputsEditProfile"))){
				emptyInputs = getDriver().findElements(map.getLocator("emptyInputsEditProfile"));
				if(emptyInputs.size()!=0){
					for(WebElement input: emptyInputs){
		//				String regex = input.getAttribute("regex");
		//				if(!regex.equals("")){
		//					Xeger generator = new Xeger(regex);
		//					value = generator.generate();
		//				}
						input.sendKeys(value);
					}
				}
			}
		}
	}
	
	public boolean addItemByPath(String pathUnic) throws Exception{
		waitLoadingOverlay();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for(int i=0;i<path.length&&!pathUnic.isEmpty();i++){
			pathToElement+=getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative",path[i]));
		}
		boolean isChecked = getDriver().findElement(By.xpath(pathToElement)).getAttribute("aria-selected").equals("true");
		pathToElement+=getPathFromBy(map.getLocator("checkBoxItemLibraryRelative"));
		if(!isChecked){
			return clickOnButton(By.xpath(pathToElement));
		}
		return true;
	}
	
	public boolean removeItemByPath(String pathUnic) throws Exception{
		waitLoading();
		String[] path = pathUnic.split("::");
		String pathToElement = getPathFromBy(map.getLocator("rootItemLibrary"));
		for(int i=0;i<path.length&&!pathUnic.isEmpty();i++){
			pathToElement+=getPathFromBy(map.getLocatorVar("hierarchyItemLibraryRelative",path[i]));
		}
		boolean isChecked = getDriver().findElement(By.xpath(pathToElement)).getAttribute("aria-selected").equals("true");
		pathToElement+=getPathFromBy(map.getLocator("checkBoxItemLibraryRelative"));
		if(isChecked){
			return clickOnButton(By.xpath(pathToElement));
		}
		return true;
	}
	
	public void setNameFilter(String entry) throws Exception {
		waitLoading();
		try{
			setTextField(map.getLocatorVar("fieldNameFilter"), entry);
			getDriver().findElement(map.getLocatorVar("fieldNameFilter")).sendKeys(Keys.ENTER);
		}catch (Exception e) {
		}
	}
	
	public boolean itemIsVisible(String item) throws Exception {
		String itemName = item.split("#")[0];
		if(item.split("#").length>1){
			itemName = itemName+"#";
		}
		return existsElement(map.getLocatorVar("itemParametersVisible",itemName));
	}
	
	public void setValueGivenNameItem(String nameItem, String value) throws Exception{
		if(!nameItem.isEmpty() && !value.isEmpty()){
			String name = nameItem.split("#")[0];
			if(nameItem.split("#").length>1){
				String number = nameItem.split("#")[1];
				clickOnButton(map.getLocatorVar("selectValueGivenItemAndNumber", name+"#",number));
				clickOnButton(map.getLocatorVar("optionValueGivenItemAndNumber", name+"#",number));
				setTextField(map.getLocatorVar("fieldGivenItemAndNumber",name+"#",number),value);
			}else{
				setTextField(map.getLocatorVar("fieldGivenItem",nameItem), value);
			}
		}
	}
	
	public String getValueGivenNameItem(String nameItem) throws Exception {
		if(!nameItem.isEmpty()){
			String name = nameItem.split("#")[0];
			if(nameItem.split("#").length>1){
				String number = nameItem.split("#")[1];
				if(existsElement(map.getLocatorVar("fieldGivenItemAndNumber",name+"#",number))){
					return getDriver().findElement(map.getLocatorVar("fieldGivenItemAndNumber",name+"#",number)).getAttribute("value");
				}
			}else{
				if(existsElement(map.getLocatorVar("fieldGivenItem",nameItem))){
					return getDriver().findElement(map.getLocatorVar("fieldGivenItem",nameItem)).getAttribute("value");
				}
			}
		}
		return "";
	}
	
	public boolean warningContinueAnyway() throws Exception {
		return clickOnButton(map.getLocatorVar("btnYesPopup", "Warning"));
	}
	
	public void clickOnItemLibraryButton() throws Exception {
		clickOnButton(map.getLocator("btnItemLibrary"));
	}
	
	public void clickOnNormalChangeButton() throws Exception {
		String isSelected = getDriver().findElement(map.getLocator("btnNormalChange")).getAttribute("class");
		if(!isSelected.equals("btn btn-approve normalChange checked")){
			clickOnButton(map.getLocator("btnNormalChange"));
		}
	}
	
	public void clickOnCriticalChangeButton() throws Exception {
		String isSelected = getDriver().findElement(map.getLocator("btnCriticalChange")).getAttribute("class");
		if(!isSelected.equals("btn btn-reject criticalChange checked")){
			clickOnButton(map.getLocator("btnCriticalChange"));
		}
	}
	
	public void clickOnAddItemToBuyerWithNoItemButton() throws Exception {
		clickOnButton(map.getLocator("btnAddItemToBuyerWithNoItem"));
	}
	
	public void clickOnFrequencyBandsButton() throws Exception {
		clickOnButton(map.getLocator("itemFrequencyBands"));
	}
	
	public void clickOnCustomizationButton() throws Exception {
		clickOnButton(map.getLocator("btnCustomization"));
	}
	
	public void clickOnBoxDoNotApplyForItemCustomLabel() throws Exception {
		clickOnButton(map.getLocator("boxDoNotApplyForItemCustomLabel"));
	}
	
	public boolean clickExpandAllNodesButton() throws Exception {
		return clickOnButton(map.getLocator("btnExpandAllNodes"));
	}
	
	public boolean clickOnCollapseAllNodes() throws Exception {
		return clickOnButton(map.getLocator("btnCollapseAllNodes"));
	}
	
	public boolean clickOnDiscardInheritanceButton() throws Exception {
		return clickOnButton(map.getLocator("btnDiscardInheritance"));
	}
	
	public boolean clickOnDiscardInheritanceOptionChildren() throws Exception {
		return clickOnButton(map.getLocator("discardInheritanceOptionChildren"));
	}
	
	public boolean clickOnDiscardInheritanceSelectedChild(String child) throws Exception {
		return clickOnButton(map.getLocatorVar("discardInheritanceSelectedChild", child));
	}
	
	public boolean clickOnDiscardInheritanceOkButton() throws Exception {
		return clickOnButton(map.getLocator("discardInheritanceBtnOk"));
	}
	
	public boolean clickOnDiscardInheritanceCloseButton() throws Exception {
		return clickOnButton(map.getLocator("discardInheritanceBtnClose"));
	}
	
	public boolean clickOnCloseMissingBuyerChildMandatoryItemsButton() throws Exception {
		return clickOnButton(map.getLocator("closeMissingBuyerChildMandatoryItems"));
	}
	
	public void setFildChildren(String message) throws Exception {
		waitLoading();
		if(!message.isEmpty()){
			WebElement fieldElement = getDriver().findElement(map.getLocator("discardInheritanceFieldChildren"));
			fieldElement.clear();
			fieldElement.sendKeys(message);
			waitFieldToBeFilled(map.getLocator("discardInheritanceFieldChildren"), message);
		}
	}
	
	public boolean clickOnOnlyCheckedItemsButton() throws Exception {
		boolean result = false;
		WebElement btnOnlyCheckedItems = getDriver().findElement(map.getLocator("btnOnlyCheckedItems"));
		if(btnOnlyCheckedItems != null){
			btnOnlyCheckedItems.click();
			result = true;
		}
		return result;
	}
	
	public boolean clickOnDiscardInheritanceOnlyCheckedItemsButton() throws Exception {
		boolean result = false;
		waitLoading();
		waitLoadingOverlay();
		WebElement btnOnlyCheckedItems = getDriver().findElement(map.getLocator("discardInheritanceBtnOnlyCheckedItems"));
		if(btnOnlyCheckedItems != null){
			btnOnlyCheckedItems.click();
			result = true;
		}
		return result;
	}
	
	public boolean existsElement(String element) throws Exception{
		boolean result = existsElement(map.getLocator(element));
		return result;
	}
	
	public void moveTo(String element) throws Exception{
		JavascriptExecutor jsx = (JavascriptExecutor) getDriver();
		WebElement webElement = getDriver().findElement(map.getLocator(element));
		jsx.executeScript("arguments[0].scrollIntoView(true);", webElement);
	}
	
	/**
	 * Verify if children have the item, if not, then add the item and its value.
	 */
	public boolean verifyChild(String child, String item, String fieldName, String value){
		ScreenPerformListBuyers listBuyers = new ScreenPerformListBuyers(getDriver());
		boolean result = false;
		
		try {
			listBuyers.setSuffixFilter(child);
			listBuyers.clickOnEditProfile(child);
			if(existsElement("msgMissingBuyerChildMandatoryItems")){
				clickOnCloseMissingBuyerChildMandatoryItemsButton();
				setValue("fieldHomeNetwork", "724 99");
			}
			clickOnSelectItemsButton();
			moveTo(item);
			addItem(item);
			clickOnSaveSelectedItems();
			setValue(fieldName, value);
			clickOnSaveProfile();
			setMessageRevisionComment("Only for testing", ConstantsBuyers.NORMAL);
			clickOnSaveEditedProfile();
			result = true;
		} catch (Exception e) {}
		return result;
	}
	
	public boolean checkItemClass(String item, String expectedClass) throws Exception{
		boolean result = false;
		String itemClass = "";
		itemClass = getAttributeClassOfElement(item);
		if(itemClass != null && !itemClass.trim().isEmpty()){
			String elements[] = itemClass.split(" ");
			for(int i=0; i<elements.length; i++){
				if(elements[i].equals(expectedClass)){
					result = true;
					return result;
				}
			}
		}
		return result;
	}
	
	public void clickOnHideOnMother(String nameItem) throws Exception{
		By opt = map.getLocatorVar("optionHideOnMother", nameItem);
		clickOnButton(opt);
	}
	
	public boolean verifyCheckBoxInHideOnMother(String nameItem) throws Exception{
		By checkBox = map.getLocatorVar("verifyCheckboxInHideOnMother", nameItem);
		if(existsElement(checkBox)){
			WebElement field = getDriver().findElement(checkBox);
			if(field.isSelected()){
				return true;
			}
		}
		return false;
	}
	
	public boolean verifyPresenceUploadFile(String value) throws Exception{
		waitLoadingOverlay();
		try{
			getDriver().findElement(map.getLocatorVar("optionUploadFile", value));
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean verifyOptionsTagType(String nameElement, String tagType){
		try {
			getDriver().findElement(map.getLocatorVar("optionsTagTypeAnyElement", nameElement, tagType));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean verifyIfElementDisplayIsBlocked(String elementName) throws Exception{
		boolean result = false;
		WebElement element = getDriver().findElement(map.getLocator(elementName));
		if(element != null){
			String style;
			style = element.getAttribute("style").split(";")[0];
			if(style != null && style.equals("display: block")){
				result = true;
				return result;
			}
		}
		return result;
	}
	
	public void onlyOneItemOnLibrary(String child) throws Exception{
		ScreenPerformListBuyers listBuyers = new ScreenPerformListBuyers(getDriver());
		listBuyers.setSuffixFilter(child);
		listBuyers.clickOnEditProfile(child);
		if(existsElement("msgMissingBuyerChildMandatoryItems")){
			clickOnCloseMissingBuyerChildMandatoryItemsButton();
		}
		clickOnSelectItemsButton();
		clickOnItemLibraryButton();
		clickOnItemLibraryButton();
		addItem("itemHomeNetwork");
		clickOnSaveSelectedItems();
		setValue("fieldHomeNetwork", "724 99");
		clickOnSaveProfile();
		setMessageRevisionComment("Only for testing", ConstantsBuyers.NORMAL);
		clickOnSaveEditedProfile();
	}
	
	public void clickOnUiStartButton() throws Exception {
		if(existsElement("btnUiStart")){
			WebElement element = getDriver().findElement(map.getLocator("btnUiStart"));
			clickOnButtonByJavaScript(element);
		}
	}
	
	public void clickOnEditorModeButton() throws Exception {
		String btnEditorClass = getDriver().findElement(map.getLocator("btnUiSimulatorEditor")).getAttribute("class");
		if(!btnEditorClass.equals("btn btn-icon active")){
			WebElement element = getDriver().findElement(map.getLocator("btnUiSimulatorEditor"));
			clickOnButtonByJavaScript(element);
		}
	}
	
	public void clickOnLockUnlockButton() throws Exception {
		if(existsElement("uiSimulatorLockImage")){
			WebElement element = getDriver().findElement(map.getLocator("btnLockUnlock"));
			clickOnButtonByJavaScript(element);
		}
	}
	
	public void clickAppPositionOneButton() throws Exception {
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("$('.uiSimulator_adder').eq(0).find('div:eq(1)').trigger({ type: 'mouseup', which: 1 })");
	}
	
	public void clickAppPositionTwoButton() throws Exception {
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("$('.uiSimulator_adder').eq(1).find('div:eq(1)').trigger({ type: 'mouseup', which: 1 })");
	}
	
	public void addApp(String app) throws Exception {
		WebElement frame = getDriver().findElement(map.getLocator("frameAppPool"));
		getDriver().switchTo().frame(frame);
		WebElement search = getDriver().findElement(map.getLocator("searchAppOnApplicationPool"));
		search.clear();
		search.sendKeys(app);
		Thread.sleep(1000);
		clickOnButton(map.getLocatorVar("selectedApp", app));
		getDriver().switchTo().defaultContent();
	}
	
	public void clickOnSaveAppsButton() throws Exception {
		clickOnButton(map.getLocator("btnSaveApps"));
	}
	
	public void setAppGridRow(String app, String value) throws Exception {
		WebElement row = getDriver().findElement(map.getLocator(app));
		row.clear();
		row.sendKeys(value);
	}
	
	public void setAppGridColumn(String app, String value) throws Exception {
		WebElement column = getDriver().findElement(map.getLocator(app));
		column.clear();
		column.sendKeys(value);
	}
	
	public boolean checkCollisionBetweenApps(String app1, String app2) throws Exception{
		boolean result = false;
		String classApp1 = getDriver().findElement(map.getLocator(app1)).getAttribute("class");
		String[] classesApp1 = classApp1.split(" ");
		String classApp2 = getDriver().findElement(map.getLocator(app2)).getAttribute("class");
		String[] classesApp2 = classApp2.split(" ");
		
		if(classesApp1.length > 3 && classesApp1[3].equals("uiSimulator_homeScreenCollision") && classesApp2.length > 3 && classesApp2[3].equals("uiSimulator_homeScreenCollision")){
			result = true;
		}
		return result;
	}
	
	public boolean clickOnCannotSaveBuyerOkButton() throws Exception {
		waitLoading();
		return clickOnButton(map.getLocator("btnOkWarningCannotSaveBuyer"));
	}

	public void updateRevision() throws Exception {
		moveToPageHeader();
		clickOnButton(map.getLocator("btnUpdatePackage"));
		clickOnButton(globalMap.getLocatorVar("popupButton", " Update Package","Ok"));
	}

	public void selectAllUpdatePackage() throws Exception {
		waitLoadingOverlay();
		clickOnButton(map.getLocator("btnUseOnlySourceValue"));
	}

	public void applyUpdatePackage() throws Exception {
		clickOnButton(globalMap.getLocatorVar("popupButton","Update Package","Apply"));
	}
	
	public ArrayList<String> getFirstItemsOfEachRootDirectory() throws Exception {
		ArrayList<String> paths = new ArrayList<>();
		boolean resultOk;
		By itemParameters = By.xpath("//*[@id='g:0']/ul/li[1]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemCustomization = By.xpath("//*[@id='g:0']/ul/li[2]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemProfiles = By.xpath("//*[@id='g:0']/ul/li[3]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemLibrary = By.xpath("//*[@id='g:0']/ul/li[4]//li/a/parent::li[contains(@id,'i:')]/a");
		By itemSmartCA = By.xpath("//*[@id='g:0']/ul/li[5]//li/a/parent::li[contains(@id,'i:')]/a");
		
		resultOk = rightClickOnButton(itemParameters);
		if(resultOk){
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}
		
		resultOk = rightClickOnButton(itemCustomization);
		if(resultOk){
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}
		
		resultOk = rightClickOnButton(itemProfiles);
		if(resultOk){
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}
		
		resultOk = rightClickOnButton(itemLibrary);
		if(resultOk){
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}
		
		resultOk = rightClickOnButton(itemSmartCA);
		if(resultOk){
			clickOnButton(map.getLocator("copyPathRightClick"));
			paths.add(getPathFromAlert());
		}
		
		return paths;
	}
	
	public String getPathFromAlert() throws Exception {
		Thread.sleep(500);
		Alert alert = getDriver().switchTo().alert();
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_C);
		r.keyRelease(KeyEvent.VK_C);
		r.keyRelease(KeyEvent.VK_CONTROL);
		Thread.sleep(100);
		String path = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
		alert.accept();
		Thread.sleep(100);
		return path;
	}
	
	public void verifyIfExistItems() throws Exception{
		if(existsElement("btnAddItemToBuyerWithNoItem")){
			clickOnAddItemToBuyerWithNoItemButton();
		}else{
			clickOnSelectItemsButton();
		}
	}
		
	public boolean verifyExistsElementInParameters(String elementName) throws Exception{
		waitLoadingOverlay();
		By test = map.getLocatorVar("elementInTreeParameters", elementName);
		return existsElement(test);
	}
	
	public void verifyIfPackageHasItemsAndClickToSelectItems() throws Exception{
		if(existsElement("btnAddItemToPackageWithNoItem")){
			clickOnButton(map.getLocator("btnAddItemToPackageWithNoItem"));
		}else{
			clickOnSelectItemsButton();
		}
	}
	
	public boolean verifyIfBuyerWithoutItems() throws Exception {
		boolean isOk = false;
		if(existsElement("messageToBuyerWithoutItems")) {
			isOk = true;
		}
		return isOk;
	}
}

