package com.lge.gpri.screens.buyers;

import static com.lge.gpri.helpers.constants.ConstantsBuyers.CRITICAL;
import static com.lge.gpri.helpers.constants.ConstantsBuyers.MESSAGE_ONLY_FOR_TESTING;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
 * Perform list of buyers page class.
 * 
 * <p>
 * Description: Methods to realize the tests on list of buyers screen.
 * 
 * @author Gabriel Costa do Nascimento
 */

public class ScreenPerformListBuyers extends AbstractScreen {

	public ScreenPerformListBuyers(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_BUYER);
	}

	public void openPage() {
		getDriver().get(ConstantsGeneric.LINK_SITE_BUYERS_PAGE);
		getDriver().manage().window().maximize();
	}

	public void clickOnNewBuyerButton() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("btnNewBuyer"));
	}

	public boolean clickOnShowDeletedBuyersButton() throws Exception {
		waitLoading();
		return clickOnButton(map.getLocator("showDeletedBuyers"));
	}

	public void clickOnShowHiddenBuyersButton() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("showHiddenBuyers"));
	}

	public void clickOnShowOnlyMyBuyersButton() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("showOnlyMyBuyers"));
	}

	public void showTableElements(String numberOfElements) throws Exception {
		waitLoading();
		if (!numberOfElements.isEmpty()) {
			clickOnButton(map.getLocator("numberOfBuyersShow"));
			clickOnButton(map.getLocatorVar("optionNumberOfBuyersShow", numberOfElements));
			clickOnButton(map.getLocator("numberOfBuyersShow"));
		}
	}

	public void selectAllBuyerFilter() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionBuyerFilter"));
		clickOnButton(map.getLocator("selectAllBuyerFilter"));
		clickOnButton(map.getLocator("optionBuyerFilter"));
	}

	public void deselectAllBuyerFilter() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionBuyerFilter"));
		clickOnButton(map.getLocator("deselectAllBuyerFilter"));
		clickOnButton(map.getLocator("optionBuyerFilter"));
	}

	public void addSuffixFilter() throws Exception {
		addBuyerFilter("Suffix");
	}

	public void addCountryFilter() throws Exception {
		addBuyerFilter("Country");
	}

	public void addOperatorFilter() throws Exception {
		addBuyerFilter("Operator");
	}

	public void addGlobalGroupFilter() throws Exception {
		addBuyerFilter("Global Group");
	}

	public void addRegionFilter() throws Exception {
		addBuyerFilter("Region");
	}

	public void addTypeFilter() throws Exception {
		addBuyerFilter("Type");
	}

	private void addBuyerFilter(String criteria) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		if (!criteria.isEmpty()) {
			clickOnButton(map.getLocator("optionBuyerFilter"));
			WebElement criteriaList = getDriver().findElement(map.getLocator("listBuyerFilter"));
			List<WebElement> criteriaListElements = criteriaList.findElements(By.xpath("li"));
			for (WebElement element : criteriaListElements) {
				if (element.getText().equals(criteria)) {
					if (!element.getAttribute("class").equals("selected")) {
						element.click();
					}
					clickOnButton(map.getLocator("optionBuyerFilter"));
					return;
				}
			}
			clickOnButton(map.getLocator("optionBuyerFilter"));
		}
	}

	public void removeSuffixFilter() throws Exception {
		removeBuyerFilter("Suffix");
	}

	public void removeCountryFilter() throws Exception {
		removeBuyerFilter("Country");
	}

	public void removeOperatorFilter() throws Exception {
		removeBuyerFilter("Operator");
	}

	public void removeGlobalGroupFilter() throws Exception {
		removeBuyerFilter("Global Group");
	}

	public void removeRegionFilter() throws Exception {
		removeBuyerFilter("Region");
	}

	public void removeTypeFilter() throws Exception {
		removeBuyerFilter("Type");
	}

	private void removeBuyerFilter(String criteria) throws Exception {
		waitLoading();
		if (!criteria.isEmpty()) {
			clickOnButton(map.getLocatorVar("btnRemoveBuyerFilter", criteria));
		}
	}

	public String getRegionOfBuyer() throws Exception {
		return getDriver().findElement(map.getLocator("regionNameOfBuyer")).getText();
	}

	/**
	 * Before calling these methods(setFilter) it is necessary to call addFilter
	 * method first
	 */
	public void setSuffixFilter(String entry) throws Exception {
		setBuyerFilter("Suffix", entry);
	}

	public void setCountryFilter(String entry) throws Exception {
		setBuyerFilter("Country", entry);
	}

	public void setOperatorFilter(String entry) throws Exception {
		setBuyerFilter("Operator", entry);
	}

	public void setGlobalGroupFilter(String entry) throws Exception {
		setBuyerFilter("Global Group", entry);
	}

	private void setBuyerFilter(String criteria, String entry) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		if(!criteria.isEmpty()&&!entry.isEmpty()){
			setTextField(map.getLocatorVar("fieldBuyerFilter", criteria), entry);
		}
	}

	public void setRegionFilter(String entry) throws Exception {
		waitLoading();
		String criteria = "Region";
		if (!criteria.isEmpty() && !entry.isEmpty()) {
			clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
			setTextFieldDropdown(map.getLocatorVar("fieldBuyerFilterDropdown", criteria), entry);
			clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
		}
	}

	public void setTypeFilter(String entry) throws Exception {
		waitLoading();
		String criteria = "Type";
		if (!criteria.isEmpty() && !entry.isEmpty()) {
			clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
			setTextFieldDropdown(map.getLocatorVar("fieldBuyerFilterDropdown", criteria), entry);
			clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
		}
	}

	public void selectAllRegionFilter() throws Exception {
		selectAllSpecificBuyerFilter("Region");
	}

	public void selectAllRegionFilterOneByOne() throws Exception {
		selectAllSpecificBuyerFilterOneByOne("Region");
	}

	public void selectAllTypeFilter() throws Exception {
		selectAllSpecificBuyerFilter("Type");
	}

	private void selectAllSpecificBuyerFilter(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("selectAllSpecificBuyerFilter", criteria));
		clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
	}

	private void selectAllSpecificBuyerFilterOneByOne(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
		selectItemDropdownOneByOne(map.getLocatorVar("fieldBuyerFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
	}

	public void deselectAllRegionFilter() throws Exception {
		deselectAllSpecificBuyerFilter("Region");
	}

	public void deselectAllRegionFilterOneByOne() throws Exception {
		deselectAllSpecificBuyerFilterOneByOne("Region");
	}

	public void deselectAllTypeFilter() throws Exception {
		deselectAllSpecificBuyerFilter("Type");
	}

	private void deselectAllSpecificBuyerFilter(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("deselectAllSpecificBuyerFilter", criteria));
		clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
	}

	private void deselectAllSpecificBuyerFilterOneByOne(String criteria) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
		deselectItemDropdownOneByOne(map.getLocatorVar("fieldBuyerFilterDropdown", criteria));
		clickOnButton(map.getLocatorVar("optionBuyerFilterDropdown", criteria));
	}

	public void clickOnMoreInfo(String suffix) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("moreInfoTblBuyers", suffix));
	}

	public void clickOnViewBuyer(String suffix) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("viewBuyerTblBuyers", suffix));
	}

	public boolean clickOnEditBuyer(String suffix) throws Exception {
		waitLoading();
		waitLoadingOverlay();
		return clickOnButton(map.getLocatorVar("btnEditBuyer", suffix));
	}

	public boolean clickOnEditProfile(String suffix) throws Exception {
		boolean resultOk;
		waitLoading();
		waitLoadingOverlay();
		resultOk = clickOnButton(map.getLocatorVar("btnEditProfile", suffix));
		selectFirstBuyerToEdit();
		return resultOk;
	}

	public void clickOVerticalPropagation(String suffix) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("btnVerticalPropagation", suffix));
	}

	public void clickOnHideOnMother(String suffix) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("btnHideOnMother", suffix));
	}

	public boolean clickOnRemoveBuyer(String suffix) throws Exception {
		boolean result;
		waitLoading();
		result = clickOnButton(map.getLocatorVar("btnRemoveBuyer", suffix));
		return result;
	}

	public boolean clickOnRestoreBuyer(String suffix) throws Exception {
		waitLoading();
		return clickOnButton(map.getLocatorVar("btnRestoreBuyer", suffix));
	}

	public boolean confirmRemoveBuyer() throws Exception {
		boolean result;
		waitLoading();
		elementFocus();
		result = clickOnButton(map.getLocatorVar("btnYesPopup", "Remove Buyer"));
		return result;
	}

	public void confirmRestoreBuyer() throws Exception {
		waitLoading();
		elementFocus();
		clickOnButton(map.getLocatorVar("btnYesPopup", "Restore Buyer"));
	}
	
	public boolean paginateNext() throws Exception{
		waitLoading();
		moveToPageBottom();	
		boolean enabled = !getDriver().findElement(map.getLocator("btnNextBuyers")).findElement(By.xpath("parent::li")).getAttribute("class").contains("disabled");
		if(enabled){
			clickOnButton(map.getLocator("btnNextBuyers"));
		}
		return enabled;
	}

	public void paginatePrevious() throws Exception {
		waitLoading();
		moveToPageBottom();
		clickOnButton(map.getLocator("btnPreviousBuyers"));
	}

	public void goToLastPage() throws Exception {
		waitLoading();
		moveToPageBottom();
		clickOnButton(map.getLocator("btnLastBuyers"));
	}

	public void goToFirstPage() throws Exception {
		waitLoading();
		moveToPageBottom();
		clickOnButton(map.getLocator("btnFirstBuyers"));
	}

	public void goToChoosedPage(String choosedPage) throws Exception {
		waitLoading();
		moveToPageBottom();
		if (!choosedPage.isEmpty()) {
			goToFirstPage();
			String page = getDriver().findElement(map.getLocator("activePageTableBuyers")).getText();
			while (!page.equals(choosedPage)) {
				paginateNext();
				waitLoading();
				page = getDriver().findElement(map.getLocator("activePageTableBuyers")).getText();
			}
		}
	}

	public void setDecreasingRegionOrder() throws Exception {
		setDecreasingGeneralOrder("btnSortingRegion");
	}

	public void setIncreasingRegionOrder() throws Exception {
		setIncreasingGeneralOrder("btnSortingRegion");
	}

	public void setDecreasingSuffixOrder() throws Exception {
		setDecreasingGeneralOrder("btnSortingSuffix");
	}

	public void setIncreasingSuffixOrder() throws Exception {
		setIncreasingGeneralOrder("btnSortingSuffix");
	}

	public void setDecreasingCountryOrder() throws Exception {
		setDecreasingGeneralOrder("btnSortingCountry");
	}

	public void setIncreasingCountryOrder() throws Exception {
		setIncreasingGeneralOrder("btnSortingCountry");
	}

	public void setDecreasingOperatorOrder() throws Exception {
		setDecreasingGeneralOrder("btnSortingOperator");
	}

	public void setIncreasingOperatorOrder() throws Exception {
		setIncreasingGeneralOrder("btnSortingOperator");
	}

	public void setDecreasingTypeOrder() throws Exception {
		setDecreasingGeneralOrder("btnSortingType");
	}

	public void setIncreasingTypeOrder() throws Exception {
		setIncreasingGeneralOrder("btnSortingType");
	}

	public void setDecreasingGlobalGroupOrder() throws Exception {
		setDecreasingGeneralOrder("btnSortingGlobalGroup");
	}

	public void setIncreasingGlobalGroupOrder() throws Exception {
		setIncreasingGeneralOrder("btnSortingGlobalGroup");
	}

	public void setDecreasingStatusOrder() throws Exception {
		setDecreasingGeneralOrder("btnSortingStatus");
	}

	public void setIncreasingStatusOrder() throws Exception {
		setIncreasingGeneralOrder("btnSortingStatus");
	}

	private void setDecreasingGeneralOrder(String locator) throws Exception {
		waitLoading();
		String orderType = getDriver().findElement(map.getLocator(locator)).getAttribute("class");
		if (orderType.equals(ConstantsBuyers.SORT)) {
			clickOnButton(map.getLocator(locator));
		}
		waitLoading();
		orderType = getDriver().findElement(map.getLocator(locator)).getAttribute("class");
		if (orderType.equals(ConstantsBuyers.ASC_SORT)) {
			clickOnButton(map.getLocator(locator));
		}
	}

	private void setIncreasingGeneralOrder(String locator) throws Exception {
		waitLoading();
		String orderType = getDriver().findElement(map.getLocator(locator)).getAttribute("class");
		if (!orderType.equals(ConstantsBuyers.ASC_SORT)) {
			clickOnButton(map.getLocator(locator));
		}
	}

	public void clickOnStatusChangeItem(String suffix) throws Exception {
		waitLoading();
		clickOnButton(map.getLocatorVar("approvalItemLink", suffix));
		waitLoading();
		clickOnButton(map.getLocator("btnConfirmApproval"));
	}

	public void clickOnStatusPendingApproval(String suffix) throws Exception {
		waitLoading();
		waitElementToBeClickable(map.getLocatorVar("approvalItemLink", suffix));
		clickOnButton(map.getLocatorVar("approvalItemLink", suffix));
	}

	public boolean isStatusPendingApprovalDisabled(String suffix) throws Exception {
		waitLoading();
		waitElementToBeClickable(map.getLocatorVar("approvalItemLink", suffix));
		WebElement text = getDriver().findElement(map.getLocatorVar("approvalItemLink", suffix));
		if (text.getAttribute("class").equals("pendingsDisabled")) {
			return true;
		}
		return false;
	}

	public String getStatusApproval(String suffix) throws Exception {
		waitLoading();
		waitElementToBeVisible(map.getLocatorVar("statusApproval", suffix));
		return getDriver().findElement(map.getLocatorVar("statusApproval", suffix)).getText();
	}

	public String getRegionConfirmation() throws Exception {
		waitElementToBeClickable(map.getLocator("tableBuyers"));
		String txt = getDriver().findElement(map.getLocator("buyerRegionFromTable")).getText();
		return txt;
	}

	public String getBuyerConfirmation() throws Exception {
		waitElementToBeClickable(map.getLocator("tableBuyers"));
		String operator = getDriver().findElement(map.getLocator("buyerOperatorFromTable")).getText();
		String country = getDriver().findElement(map.getLocator("buyerCountryFromTable")).getText();
		return operator + " " + country;
	}

	public boolean noBuyersAvailable() throws Exception {
		waitLoadingOverlay();
		return existsElement(map.getLocator("noBuyersAvailable"));
	}

	public boolean restoreOrUnhideBuyer(String suffix) throws Exception {
		ScreenPerformBuyer screenPerformBuyer = new ScreenPerformBuyer(getDriver());
		addSuffixFilter();
		setSuffixFilter(suffix);
		if (noBuyersAvailable()) {
			clickOnShowDeletedBuyersButton();
			if (noBuyersAvailable()) {
				clickOnRestoreBuyer(suffix);
				confirmRestoreBuyer();
				if (noBuyersAvailable()) {
					clickOnShowHiddenBuyersButton();
					clickOnEditBuyer(suffix);
					Thread.sleep(500);
					screenPerformBuyer.uncheckMVNO();
					screenPerformBuyer.checkOnHideOnView();
					return screenPerformBuyer.clickOnSaveBuyer();
				}
			}
		}
		return false;
	}

	public void selectBuyerToEdit(String buyer) throws Exception {
		clickOnButton(map.getLocator("optionBuyerToEdit"));
		setTextFieldDropdown(map.getLocator("fieldBuyerToEdit"), buyer);
		clickOnButton(map.getLocatorVar("btnYesPopup", "Edit Profiles"));
	}

	public void selectFirstBuyerToEdit() throws Exception {
		clickOnButton(map.getLocatorVar("btnYesPopup","Edit Profiles"));
	}

	public ArrayList<String> getBuyersInPage() throws Exception {
		waitLoadingOverlay();
		ArrayList<String> suffixes = new ArrayList<>();
		List<WebElement> buyers = getDriver().findElements(map.getLocator("suffixListBuyers"));
		for(WebElement buyer: buyers){
			suffixes.add(buyer.getText());
		}
		return suffixes;
	}

	public HashMap<String, ArrayList<String>> getBuyerLinesResultFilter() throws Exception {
		waitLoadingOverlay();
		HashMap<String, ArrayList<String>> buyers = new HashMap<>();
		List<WebElement> lines = getDriver().findElements(map.getLocator("linesBuyers"));
		for(WebElement line: lines){
			String region = line.findElement(By.xpath("td[2]")).getText();
			String suffix = line.findElement(By.xpath("td[3]")).getText();
			String country = line.findElement(By.xpath("td[4]")).getText();
			String operator = line.findElement(By.xpath("td[5]")).getText();
			String type = line.findElement(By.xpath("td[6]")).getText();
			String globalGroup = line.findElement(By.xpath("td[7]")).getText();
			String status = line.findElement(By.xpath("td[8]")).getText();
			ArrayList<String> items = new ArrayList<>();
			items.add(region);
			items.add(suffix);
			items.add(country);
			items.add(operator);
			items.add(type);
			items.add(globalGroup);
			items.add(status);
			buyers.put(suffix, items);
		}
		return buyers;
	}
	
	public ArrayList<String> getColumnResultFilter(String column) throws Exception {
		waitLoadingOverlay();
		ArrayList<String> buyers = new ArrayList<>();
		List<WebElement> lines = getDriver().findElements(map.getLocator("linesBuyers"));
		for(WebElement line: lines){
			String columnChoosed = "";
			if(column.equals(ConstantsBuyers.REGION)){
				columnChoosed = line.findElement(By.xpath("td[2]")).getText();
			}else if(column.equals(ConstantsBuyers.SUFFIX)){
				columnChoosed = line.findElement(By.xpath("td[3]")).getText();
			}else if(column.equals(ConstantsBuyers.COUNTRY)){
				columnChoosed = line.findElement(By.xpath("td[4]")).getText();
			}else if(column.equals(ConstantsBuyers.OPERATOR)){
				columnChoosed = line.findElement(By.xpath("td[5]")).getText();
			}else if(column.equals(ConstantsBuyers.TYPE)){
				columnChoosed = line.findElement(By.xpath("td[6]")).getText();
			}else if(column.equals(ConstantsBuyers.GLOBAL_GROUP)){
				columnChoosed = line.findElement(By.xpath("td[7]")).getText();
			}else if(column.equals(ConstantsBuyers.STATUS)){
				columnChoosed = line.findElement(By.xpath("td[8]")).getText();
			}
			buyers.add(columnChoosed);
		}
		return buyers;
	}
	
	public void checkStatusValidated(String sufix, String field, String value1, String value2, String fieldNote, String note) throws Exception{
		if(getStatusApproval(sufix).equals("Validated")){
			ScreenPerformEditBuyerProfile screenPerformEditBuyerProfile = new ScreenPerformEditBuyerProfile(getDriver());
			clickOnEditProfile(sufix);
			screenPerformEditBuyerProfile.addNote(field, value1, value2, fieldNote, note);
			screenPerformEditBuyerProfile.moveToPageBottom();
			screenPerformEditBuyerProfile.clickOnSaveProfile();
			screenPerformEditBuyerProfile.setMessageRevisionComment(MESSAGE_ONLY_FOR_TESTING, CRITICAL);
			screenPerformEditBuyerProfile.clickOnSaveEditedProfile();
		}
	}
}
