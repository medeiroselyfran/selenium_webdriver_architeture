package com.lge.gpri.screens.buyers;

public class AutoProfile {
	public String type;
	public String value;
	
	public AutoProfile(String t, String v){
		type = t;
		value = v;
	}
	
	public String getType(){
		return type;
	}
	
	public String getValue(){
		return value;
	}
}
