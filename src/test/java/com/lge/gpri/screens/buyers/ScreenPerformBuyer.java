package com.lge.gpri.screens.buyers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lge.gpri.helpers.ObjectMap;
import com.lge.gpri.helpers.constants.ConstantsBuyers;
import com.lge.gpri.helpers.constants.ConstantsGeneric;
import com.lge.gpri.screens.AbstractScreen;

/**
 * Perform creation and edition of buyers page class.
 * 
 * <p>
 * Description: Methods to create and edit a Buyer
 * 
 * @author Gabriel Costa do Nascimento
 */

public class ScreenPerformBuyer extends AbstractScreen {

	public ScreenPerformBuyer(WebDriver driver) {
		super(driver);
		map = new ObjectMap(ConstantsGeneric.PATH_FILE_BUYER);
	}

	public void fillFieldsMotherBuyerFromNewBuyer(String operator, String country, String suffix, String globalGroup,
			List<String> childrenBuyers, List<String> ltms, List<String> gtams) throws Exception {

		selectType(ConstantsBuyers.MOTHER);
		createNewBuyer(operator, country, suffix);
		setGlobalGroup(globalGroup);

		for (String child : childrenBuyers) {
			addChildBuyer(child);
		}
		for (String ltm : ltms) {
			addLTM(ltm);
		}
		for (String gtam : gtams) {
			addGTAM(gtam);
		}
	}

	public void fillFieldsMotherBuyerFromExistBuyer(String suffix, String globalGroup, List<String> childrenBuyers,
			List<String> ltms, List<String> gtams) throws Exception {
		selectType(ConstantsBuyers.MOTHER);
		setBuyer(suffix);
		setGlobalGroup(globalGroup);

		for (String child : childrenBuyers) {
			addChildBuyer(child);
		}
		for (String ltm : ltms) {
			addLTM(ltm);
		}
		for (String gtam : gtams) {
			addGTAM(gtam);
		}
	}

	public void fillFieldsChildBuyer(String suffix, String globalGroup, ArrayList<AutoProfile> autoProfile,
			List<String> ltms, List<String> gtams) throws Exception {
		selectType(ConstantsBuyers.CHILD);
		setBuyer(suffix);
		setGlobalGroup(globalGroup);

		for (AutoProfile entry : autoProfile) {
			waitLoadingOverlay();
			addAutoProfile(entry.getType(), entry.getValue());
		}
		for (String ltm : ltms) {
			addLTM(ltm);
		}
		for (String gtam : gtams) {
			addGTAM(gtam);
		}
	}

	public void selectType(String type) throws Exception {
		waitLoading();
		if (!type.isEmpty()) {
			if (type.equals(ConstantsBuyers.MOTHER)) {
				clickOnButton(map.getLocator("radioMotherTypeBuyerInfo"));
			} else if (type.equals(ConstantsBuyers.CHILD)) {
				clickOnButton(map.getLocator("radioChildTypeBuyerInfo"));
			}
		}
	}

	public String getType() throws Exception {
		waitLoading();
		WebElement motherRadio = getDriver().findElement(map.getLocator("radioMotherTypeBuyerInfo"));
		WebElement childRadio = getDriver().findElement(map.getLocator("radioChildTypeBuyerInfo"));
		if (motherRadio.isSelected()) {
			return ConstantsBuyers.MOTHER;
		} else if (childRadio.isSelected()) {
			return ConstantsBuyers.CHILD;
		}
		return null;
	}

	public void selectNewBuyer() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("optionBuyerInfo"));
		clickOnButton(map.getLocator("newBuyerOnOptionBuyer"));
	}

	public void createNewBuyer(String operator, String country, String suffix) throws Exception {
		boolean resultOk = false;
		boolean first = false;
		Random rand = new Random();
		String randEntry;

		selectNewBuyer();
		if (!operator.equals(ConstantsBuyers.RANDOM) && !country.equals(ConstantsBuyers.RANDOM)
				&& !suffix.equals(ConstantsBuyers.RANDOM)) {

			setTextField(map.getLocator("fieldCountryInfo"), country);
			setTextField(map.getLocator("fieldSuffixInfo"), suffix);
			clickOnButton(map.getLocator("btnCreateNewBuyer"));
		} else {
			while (!resultOk) {
				if (operator.equals(ConstantsBuyers.RANDOM)) {
					randEntry = Character.toString(((char) (rand.nextInt(90 - 65) + 65)));
					if (!first) {
						selectRandomtInDropdown(map.getLocator("fieldOperatorInfo"), By.xpath("/html/body/ul[1]"),
								randEntry);
						first = true;
					} else {
						selectRandomtInDropdown(map.getLocator("fieldOperatorInfo"), By.xpath("/html/body/ul[2]"),
								randEntry);
					}
				} else {
					setTextField(map.getLocator("fieldOperatorInfo"), operator);
				}
				if (country.equals(ConstantsBuyers.RANDOM)) {
					randEntry = Character.toString(((char) (rand.nextInt(90 - 65) + 65)));
					selectRandomtInDropdown(map.getLocator("fieldCountryInfo"), By.xpath("/html/body/ul[2]"),
							randEntry);
				} else {
					setTextField(map.getLocator("fieldCountryInfo"), country);
				}
				if (suffix.equals(ConstantsBuyers.RANDOM)) {
					String c1 = Character.toString(((char) (rand.nextInt(90 - 65) + 65)));
					String c2 = Character.toString(((char) (rand.nextInt(90 - 65) + 65)));
					String c3 = Character.toString(((char) (rand.nextInt(90 - 65) + 65)));
					String randSuffix = c1 + c2 + c3;
					setTextField(map.getLocator("fieldSuffixInfo"), randSuffix);
				} else {
					setTextField(map.getLocator("fieldSuffixInfo"), suffix);
				}
				clickOnButton(map.getLocator("btnCreateNewBuyer"));
				waitLoading();
				if (getDriver().findElement(map.getLocator("errorCreatingNewBuyer")).isDisplayed()) {
					resultOk = false;
				} else {
					resultOk = true;
				}
				if (!operator.equals(ConstantsBuyers.RANDOM) || !country.equals(ConstantsBuyers.RANDOM)
						|| !suffix.equals(ConstantsBuyers.RANDOM)) {
					resultOk = true;
				}
			}
		}
	}

	public String getErrorNewBuyer() throws Exception {
		waitLoadingOverlay();
		return getDriver().findElement(map.getLocator("errorCreatingNewBuyer")).getText();
	}

	/**
	 * If param suffix is Constants.FIRST the buyer selected will be the first
	 * of the list
	 */
	public void setBuyer(String suffix) throws Exception {
		if (!suffix.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionBuyerInfo"));
			if (suffix.equals(ConstantsBuyers.FIRST)) {
				clickOnButton(map.getLocator("fieldBuyerFirst"));
			} else if (suffix.equals(ConstantsBuyers.SECOND)){
				clickOnButton(map.getLocator("fieldBuyerSecond"));
			} else {
				setTextFieldDropdown(map.getLocator("fieldBuyerInfo"), suffix);
			}
		}
	}

	public void setGlobalGroup(String globalGroup) throws Exception {
		if (!globalGroup.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionGlobalGroupBuyerInfo"));
			setTextFieldDropdown(map.getLocator("fieldGlobalGroupBuyerInfo"), globalGroup);
		}
	}

	public void setNetworkOwner(String networkOwnerSuffix) throws Exception {
		if (!networkOwnerSuffix.isEmpty()) {
			waitLoading();
			checkMVNO();
			clickOnButton(map.getLocator("optionNetworkOwnerBuyerInfo"));
			setTextFieldDropdown(map.getLocator("fieldNetworkOwnerBuyerInfo"), networkOwnerSuffix);
		}
	}

	public void checkMVNO() throws Exception {
		waitLoading();
		boolean isSelected = getDriver().findElement(By.id("chkMvno")).isSelected();
		if (!isSelected) {
			clickOnButton(map.getLocator("chkMvno"));
		}
	}

	public void uncheckMVNO() throws Exception {
		waitLoading();
		boolean isSelected = getDriver().findElement(By.id("chkMvno")).isSelected();
		if (isSelected) {
			clickOnButton(map.getLocator("chkMvno"));
		}
	}

	public void checkOnHideOnView() throws Exception {
		waitLoading();
		boolean isSelected = getDriver().findElement(By.id("chkHidden")).isSelected();
		if (!isSelected) {
			clickOnButton(map.getLocator("chkHidden"));
		}
	}

	public void uncheckOnHideOnView() throws Exception {
		waitLoading();
		boolean isSelected = getDriver().findElement(By.id("chkHidden")).isSelected();
		if (isSelected) {
			clickOnButton(map.getLocator("chkHidden"));
		}
	}

	public void addChildBuyer(String childrenBuyer) throws Exception {
		if (!childrenBuyer.isEmpty()) {
			waitLoading();
			waitLoadingOverlay();
			clickOnButton(map.getLocator("optionBuyerChild"));
			setTextFieldDropdown(map.getLocator("fieldBuyerChild"), childrenBuyer);
			clickOnButton(map.getLocator("btnAddBuyerChild"));
		}
	}

	public void removeChildBuyer(String btnRemoveBuyerChild) throws Exception {
		if (!btnRemoveBuyerChild.isEmpty()) {
			waitLoading();
			waitLoadingOverlay();
			clickOnButton(map.getLocatorVar("btnRemoveBuyerChild", btnRemoveBuyerChild));
			waitLoading();
			waitLoadingOverlay();
			clickOnButton(map.getLocator("btnConfirmRemove"));
		}
	}

	public void showTableChildBuyerElements(String numberOfElements) throws Exception {
		waitLoading();
		if (!numberOfElements.isEmpty()) {
			clickOnButton(map.getLocator("numberOfChildrenBuyersShow"));
			clickOnButton(map.getLocatorVar("optionNumberOfChildrenBuyersShow", numberOfElements));
			clickOnButton(map.getLocator("numberOfChildrenBuyersShow"));
		}
	}

	public void searchChildBuyer(String entry) throws Exception {
		setTextField(map.getLocator("fieldFilterChildrenBuyers"), entry);
	}

	public void addAutoProfile(String type, String value) throws Exception {
		if (!type.isEmpty() && !value.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocator("optionAutoProfileType"));
			setTextFieldDropdown(map.getLocator("fieldAutoProfileType"), type);
			setTextField(map.getLocator("fieldAutoProfileValue"), value);
			clickOnButton(map.getLocator("btnAddAutoProfile"));
		}
	}

	public void editAutoProfile(String value, String iccid, String carrierByiccid, String carrierBySim,
			String subCarrier) throws Exception {
		if (!value.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocatorVar("btnEditInfoAutoProfile", value));
			waitLoading();
			setTextField(map.getLocator("fieldICCID"), iccid);
			setTextField(map.getLocator("fieldCarrierByICCID"), carrierByiccid);
			setTextField(map.getLocator("fieldCarrierBySIM"), carrierBySim);
			setTextField(map.getLocator("fieldSubCarrier"), subCarrier);
			elementFocus();
			clickOnButton(map.getLocator("btnOkExtraInfoAutoProfile"));
		}
	}

	public void removeAutoProfile(String value) throws Exception {
		if (!value.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocatorVar("btnRemoveAutoProfile", value));
			waitLoading();
			clickOnButton(map.getLocatorVar("btnYesPopup", "Remove Network Code"));
		}
	}

	public void removeAllAutoProfiles() throws Exception {
		ArrayList<AutoProfile> autoProfiles = getAutoProfiles();
		for (AutoProfile auto : autoProfiles) {
			removeAutoProfile(auto.getValue());
		}
	}

	public void showTableAutoProfileElements(String numberOfElements) throws Exception {
		waitLoading();
		if (!numberOfElements.isEmpty()) {
			clickOnButton(map.getLocator("numberOfAutoProfilesShow"));
			clickOnButton(map.getLocatorVar("optionNumberOfAutoProfilesShow", numberOfElements));
			clickOnButton(map.getLocator("numberOfAutoProfilesShow"));
		}
	}

	public void searchAutoProfile(String value) throws Exception {
		setTextField(map.getLocator("fieldFilterAutoProfiles"), value);
	}

	public void clickOnLTMTab() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("ltmTabSectionBuyer"));
	}

	public void addLTM(String ltm) throws Exception {
		if (!ltm.isEmpty()) {
			waitLoading();
			clickOnLTMTab();
			clickOnButton(map.getLocator("optionLtm"));
			if (ltm.equals(ConstantsBuyers.FIRST)) {
				clickOnButton(map.getLocatorVar("ltmNumber", "1"));
			} else if (ltm.equals(ConstantsBuyers.SECOND)) {
				clickOnButton(map.getLocatorVar("ltmNumber", "2"));
			} else {
				setTextFieldDropdown(map.getLocator("fieldLtm"), ltm);
			}
			clickOnButton(map.getLocator("btnAddLtm"));
		}
	}

	public void removeLTM(String ltm) throws Exception {
		if (!ltm.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocatorVar("btnRemoveLtm", ltm));
			waitLoading();
			clickOnButton(map.getLocatorVar("btnYesPopup", "Remove"));
		}
	}

	public void removeAllLTMs() throws Exception {
		ArrayList<String> ltms = getLTMs();
		for (String ltm : ltms) {
			removeLTM(ltm);
		}
	}

	public void showTableLTMElements(String numberOfElements) throws Exception {
		waitLoading();
		if (!numberOfElements.isEmpty()) {
			clickOnButton(map.getLocator("numberOfLtmsShow"));
			clickOnButton(map.getLocatorVar("optionNumberOfLtmsShow", numberOfElements));
			clickOnButton(map.getLocator("numberOfLtmsShow"));
		}
	}

	public void searchLTM(String ltm) throws Exception {
		setTextField(map.getLocator("fieldFilterLtms"), ltm);
	}

	public void clickOnGTAMTab() throws Exception {
		waitLoading();
		clickOnButton(map.getLocator("gtamTabSectionBuyer"));
	}

	public void addGTAM(String gtam) throws Exception {
		if (!gtam.isEmpty()) {
			waitLoading();
			clickOnGTAMTab();
			clickOnButton(map.getLocator("optionGtam"));
			setTextFieldDropdown(map.getLocator("fieldGtam"), gtam);
			clickOnButton(map.getLocator("btnAddGtam"));
		}
	}

	public void removeGTAM(String gtam) throws Exception {
		if (!gtam.isEmpty()) {
			waitLoading();
			clickOnButton(map.getLocatorVar("btnRemoveGtam", gtam));
			waitLoading();
			clickOnButton(map.getLocatorVar("btnYesPopup", "Remove"));
		}
	}

	public void removeAllGTAMs() throws Exception {
		ArrayList<String> gtams = getGTAMs();
		for (String gtam : gtams) {
			removeGTAM(gtam);
		}
	}

	public void showTableGTAMElements(String numberOfElements) throws Exception {
		waitLoading();
		if (!numberOfElements.isEmpty()) {
			clickOnButton(map.getLocator("numberOfGtamsShow"));
			clickOnButton(map.getLocatorVar("optionNumberOfGtamsShow", numberOfElements));
			clickOnButton(map.getLocator("numberOfGtamsShow"));
		}
	}

	public void searchGTAM(String gtam) throws Exception {
		setTextField(map.getLocator("fieldFilterGtams"), gtam);
	}

	public boolean clickOnSaveBuyer() throws Exception {
		waitLoading();
		waitLoadingOverlay();
		WebElement saveButton = getDriver().findElement(map.getLocator("btnSaveBuyer"));
		moveToPageBottom();
		waitElementToBeClickable(map.getLocator("btnSaveBuyer"));
		saveButton.click();
		waitLoadingOverlay();
		if (showErrorPleaseTryAgain()) {
			return false;
		}
		return true;
	}

	public void clickOnCancelBuyer() throws Exception {
		waitLoading();
		WebElement cancelButton = getDriver().findElement(map.getLocator("btnCancelBuyer"));
		moveToPageBottom();
		waitElementToBeClickable(map.getLocator("btnCancelBuyer"));
		cancelButton.click();
		waitLoading();
		if (existsElement(map.getLocatorVar("btnCancelEditPageGeneric", "Yes"))) {
			clickOnButton(map.getLocatorVar("btnCancelEditPageGeneric", "Yes"));
		}
	}

	public String getBuyerFirstDropdownBuyer() throws Exception {
		waitLoadingOverlay();
		clickOnButton(map.getLocator("optionBuyerInfo"));
		waitLoading();
		String buyer = getDriver().findElement(map.getLocator("fieldBuyerFirst")).getText();
		clickOnButton(map.getLocator("optionBuyerInfo"));
		return buyer;
	}
	
	public String getBuyerSecondDropdownBuyer() throws Exception {
		waitLoadingOverlay();
		clickOnButton(map.getLocator("optionBuyerInfo"));
		waitLoading();
		String buyer = getDriver().findElement(map.getLocator("fieldBuyerSecond")).getText();
		clickOnButton(map.getLocator("optionBuyerInfo"));
		return buyer;
	}

	public String getRegionFirstDropdownBuyer() throws Exception {
		waitLoadingOverlay();
		clickOnButton(map.getLocator("optionBuyerInfo"));
		waitLoading();
		String region = getDriver().findElement(map.getLocator("fieldRegionFirst")).getText();
		clickOnButton(map.getLocator("optionBuyerInfo"));
		return region;
	}
	
	public String getErrorMsg(String nameError) throws Exception {
		waitLoading();
		if (existsElement(map.getLocatorVar("errorMsg", nameError))) {
			return getDriver().findElement(map.getLocatorVar("errorMsg", nameError)).getText();
		}
		return "";
	}

	public String getNicknameOperator() throws Exception {
		clickOnButton(map.getLocator("btnEditNickname"));
		waitLoading();
		String operator = getDriver().findElement(map.getLocator("fieldNicknameOperator")).getAttribute("value");
		clickOnButton(map.getLocator("closePopupNicknameEdit"));
		return operator;
	}

	public String getNicknameCountry() throws Exception {
		clickOnButton(map.getLocator("btnEditNickname"));
		waitLoading();
		String country = getDriver().findElement(map.getLocator("fieldNicknameCountry")).getAttribute("value");
		clickOnButton(map.getLocator("closePopupNicknameEdit"));
		return country;
	}

	public void closePopupError() throws Exception {
		clickOnButton(map.getLocator("closePopupError"));
	}

	public void changeNicknameOperator(String entry) throws Exception {
		clickOnButton(map.getLocator("btnEditNickname"));
		setTextField(map.getLocator("fieldNicknameOperator"), entry);
		clickOnButton(map.getLocator("saveNicknameEdit"));
	}

	public ArrayList<String> getLTMs() throws Exception {
		waitLoading();
		List<WebElement> ltms = getDriver().findElements(map.getLocator("ltms"));
		ArrayList<String> ltmsArray = new ArrayList<>();
		for (WebElement ltm : ltms) {
			ltmsArray.add(ltm.getText());
		}
		if (ltmsArray.get(0).equals(ConstantsBuyers.NO_LTM_ASSOCIATED)) {
			ltmsArray = new ArrayList<>();
			ltmsArray.add("");
		}
		return ltmsArray;
	}

	public ArrayList<String> getGTAMs() throws Exception {
		waitLoading();
		List<WebElement> gtams = getDriver().findElements(map.getLocator("gtams"));
		ArrayList<String> gtamsArray = new ArrayList<>();
		for (WebElement gtam : gtams) {
			gtamsArray.add(gtam.getText());
		}
		if (gtamsArray.get(0).equals(ConstantsBuyers.NO_GTAM_ASSOCIATED)) {
			gtamsArray = new ArrayList<>();
			gtamsArray.add("");
		}
		return gtamsArray;
	}

	public ArrayList<AutoProfile> getAutoProfiles() throws Exception {
		waitLoading();
		List<WebElement> autoProfiles = getDriver().findElements(map.getLocator("autoProfiles"));
		ArrayList<AutoProfile> autoArray = new ArrayList<>();
		for (WebElement auto : autoProfiles) {
			String type = auto.findElement(By.xpath("td[1]")).getText();
			String value = auto.findElement(By.xpath("td[2]")).getText();
			autoArray.add(new AutoProfile(type, value));
		}
		if (autoArray.get(0).equals(ConstantsBuyers.NO_NETWORK_CODE_ASSOCIATED)) {
			autoArray = new ArrayList<>();
			autoArray.add(new AutoProfile("", ""));
		}
		return autoArray;
	}

	public ArrayList<String> getBuyerChildren() throws Exception {
		waitLoading();
		List<WebElement> buyers = getDriver().findElements(map.getLocator("buyerChildren"));
		ArrayList<String> buyersArray = new ArrayList<>();
		for (WebElement buyer : buyers) {
			buyersArray.add(buyer.getText());
		}
		if (buyersArray.get(0).equals(ConstantsBuyers.NO_CHILDREN_BUYER_ASSOCIATED)) {
			buyersArray = new ArrayList<>();
			buyersArray.add("");
		}
		return buyersArray;
	}

	public String getBuyer() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionBuyerInfo")).getText().split("\n")[0];
	}

	public String getGlobalGroup() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionGlobalGroupBuyerInfo")).getText().split("\n")[0];
	}

	public String getRegion() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("regionBuyerInfo")).getAttribute("value").split("\n")[0];
	}

	public String getNetworkOwner() throws Exception {
		waitLoading();
		return getDriver().findElement(map.getLocator("optionNetworkOwnerBuyerInfo")).getText().split("\n")[0];
	}
}
