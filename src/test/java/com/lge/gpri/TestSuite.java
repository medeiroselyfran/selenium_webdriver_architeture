package com.lge.gpri;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.lge.gpri.tests.buyers.TestApprovalItemsBuyers;
import com.lge.gpri.tests.buyers.TestComponentsBuyers;
import com.lge.gpri.tests.buyers.TestCreateChildBuyers;
import com.lge.gpri.tests.buyers.TestCreateMotherBuyers;
import com.lge.gpri.tests.buyers.TestEditChildBuyers;
import com.lge.gpri.tests.buyers.TestEditMotherBuyers;
import com.lge.gpri.tests.buyers.TestOrderOfColumnBuyers;
import com.lge.gpri.tests.buyers.editprofile.TestAddingNoteToEditBuyerProfile;
import com.lge.gpri.tests.buyers.editprofile.TestComponentsToEditBuyerProfile;
import com.lge.gpri.tests.buyers.editprofile.TestDiscardInheritanceToEditBuyerProfile;
import com.lge.gpri.tests.buyers.editprofile.TestFiltersToEditBuyerProfile;
import com.lge.gpri.tests.buyers.editprofile.TestManagementItemsToEditBuyerProfile;
import com.lge.gpri.tests.buyers.editprofile.TestManagementOfTheAppsOnUiSimulatorToEditBuyerProfile;
import com.lge.gpri.tests.compare.TestCompare;
import com.lge.gpri.tests.itemInspector.TestItemInspectorComponents;
import com.lge.gpri.tests.itemInspector.TestItemInspectorExportExcel;
import com.lge.gpri.tests.itemInspector.TestItemInspectorFilters;
import com.lge.gpri.tests.itemInspector.TestItemInspectorInformation;
import com.lge.gpri.tests.itemInspector.TestViewItemReviewerMoreInfo;
import com.lge.gpri.tests.itemLibrary.TestChangeLog;
import com.lge.gpri.tests.itemLibrary.TestFilter;
import com.lge.gpri.tests.itemLibrary.TestManagementGroup;
import com.lge.gpri.tests.itemLibrary.TestManagementItem;
import com.lge.gpri.tests.itemLibrary.TestPropertySmartCA;
import com.lge.gpri.tests.itemLibrary.TestSearchOfTree;
import com.lge.gpri.tests.models.TestComponentsModel;
import com.lge.gpri.tests.models.TestCreateModel;
import com.lge.gpri.tests.models.TestEditModel;
import com.lge.gpri.tests.models.TestFilterModel;
import com.lge.gpri.tests.packages.TestComponentsPackageHistory;
import com.lge.gpri.tests.packages.TestCreateNormalPackage;
import com.lge.gpri.tests.packages.TestCreateSmartCAPackage;
import com.lge.gpri.tests.packages.TestEditNormalPackage;
import com.lge.gpri.tests.packages.TestEditSmartCAPackage;
import com.lge.gpri.tests.packages.TestListPackages;
import com.lge.gpri.tests.packages.TestNormalPackageStatusHistory;
import com.lge.gpri.tests.packages.TestRemoveNormalPackage;
import com.lge.gpri.tests.packages.TestRemoveSmartCAPackage;
import com.lge.gpri.tests.packages.TestStateMachineNormalPackage;
import com.lge.gpri.tests.packages.editProfile.TestAddingNote;
import com.lge.gpri.tests.packages.editProfile.TestChangeFolderNameByProfile;
import com.lge.gpri.tests.packages.editProfile.TestChangeFolderNameByValue;
import com.lge.gpri.tests.packages.editProfile.TestComponents;
import com.lge.gpri.tests.packages.editProfile.TestCopyPackage;
import com.lge.gpri.tests.packages.editProfile.TestFilters;
import com.lge.gpri.tests.packages.editProfile.TestInsertValueByPerProfile;
import com.lge.gpri.tests.packages.editProfile.TestInsertValueByValue;
import com.lge.gpri.tests.packages.editProfile.TestManagementItems;
import com.lge.gpri.tests.packages.editProfile.TestManagementOfTheAppsOnUiSimulator;
import com.lge.gpri.tests.packages.editProfile.TestSync;
import com.lge.gpri.tests.packages.editProfile.TestUpdateTo;
import com.lge.gpri.tests.usermanagement.TestAccessUserNVG_PVG;
import com.lge.gpri.tests.usermanagement.TestAddProfileNVG_PVG;
import com.lge.gpri.tests.usermanagement.TestAddProfileSWMM;
import com.lge.gpri.tests.usermanagement.TestAssociateRegion;
import com.lge.gpri.tests.usermanagement.TestBlockedUsers;
import com.lge.gpri.tests.usermanagement.TestComponentsGPRICarrier;
import com.lge.gpri.tests.view.TestSearchOnView;
import com.lge.gpri.tests.view.TestValuesForBuyersComponents;
import com.lge.gpri.tests.view.TestValuesForBuyersViewConfirmation;
import com.lge.gpri.tests.view.TestValuesForPackagesComponents;
import com.lge.gpri.tests.view.TestValuesForPackagesViewConfirmation;
import com.lge.gpri.tests.view.TestViewOptions;

/**
* Test suite class
* 
* <p>Description: This class is responsible for execute all the test class.
*  
* @author AUTHOR NAME (can exist more than one)
* 
*/
@RunWith(Suite.class)
@Suite.SuiteClasses({
	//---View
	TestSearchOnView.class,
	TestValuesForPackagesViewConfirmation.class,
	TestValuesForPackagesComponents.class,
	TestValuesForBuyersViewConfirmation.class,
	TestValuesForBuyersComponents.class,
	TestViewOptions.class,
	//---Compare
	TestCompare.class,
	//---Item Inspector
	TestItemInspectorFilters.class,
	TestItemInspectorInformation.class,
	TestItemInspectorExportExcel.class,
	TestItemInspectorComponents.class,
	TestViewItemReviewerMoreInfo.class,
	//---User Management
	TestAddProfileNVG_PVG.class,
	TestAddProfileSWMM.class,
	TestAssociateRegion.class,
	TestBlockedUsers.class,
	TestAccessUserNVG_PVG.class,
	TestComponentsGPRICarrier.class,
	//---Item Library
	TestManagementItem.class,
	TestManagementGroup.class,
	TestChangeLog.class,
	TestFilter.class,
	TestPropertySmartCA.class,
	TestSearchOfTree.class,
	//---Packages
	TestCreateNormalPackage.class,
	TestEditNormalPackage.class,
	TestRemoveNormalPackage.class,
	TestStateMachineNormalPackage.class,
	TestNormalPackageStatusHistory.class,
	TestComponentsPackageHistory.class,
	TestCreateSmartCAPackage.class,
	TestEditSmartCAPackage.class,
	TestRemoveSmartCAPackage.class,
	TestListPackages.class,
	//---Package Edit Profile
	TestManagementItems.class,
	TestFilters.class,
	TestComponents.class,
	TestAddingNote.class,
	TestChangeFolderNameByProfile.class,
	TestChangeFolderNameByValue.class,
	TestManagementOfTheAppsOnUiSimulator.class,
	TestSync.class,
	TestUpdateTo.class,
	TestCopyPackage.class,
	TestInsertValueByValue.class,
	TestInsertValueByPerProfile.class,
	//---Models
	TestFilterModel.class,
	TestEditModel.class,
	TestCreateModel.class,
	TestComponentsModel.class,
	//---Buyers
	TestCreateChildBuyers.class,
	TestEditChildBuyers.class,
	TestCreateMotherBuyers.class,
	TestEditMotherBuyers.class,
	TestApprovalItemsBuyers.class,
	TestComponentsBuyers.class,
	TestOrderOfColumnBuyers.class,
	//Buyers Edit Profile
	TestManagementItemsToEditBuyerProfile.class,
	TestFiltersToEditBuyerProfile.class,
	TestComponentsToEditBuyerProfile.class,
	TestDiscardInheritanceToEditBuyerProfile.class,
	TestAddingNoteToEditBuyerProfile.class,
	TestManagementOfTheAppsOnUiSimulatorToEditBuyerProfile.class,
})
public class TestSuite {

}
